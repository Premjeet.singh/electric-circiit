#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 26 09:55:19 2021

@author: vipluv.aga
"""

import json, re
import logging
import os
import datetime as dt
import numpy as np
import pandas as pd
import uuid
import pickle
import math
import importlib
import base64
import ast
from middlewareauth import jwt_authenticated

from flask import Flask, request, jsonify
from google.cloud import storage, datastore
from SLDMaker import SLDMaker

import sentry_sdk
from sentry_sdk.integrations.flask import FlaskIntegration
from sentry_sdk.integrations.sqlalchemy import SqlalchemyIntegration

sentry_sdk.init(
    dsn="https://508d4d25eaa447c38445f1f0bd8ac634@o434693.ingest.sentry.io/5392060",
    integrations=[FlaskIntegration(), SqlalchemyIntegration()],
    traces_sample_rate=1.0
)

if __name__ == '__main__':

    Envvar = {"Environ":
                  {"BUCKET_NAME": "solbucket",
                   "PV_DB_FILENAME": "CECModules09-2020.pkl",
                   "PVS_DB_FILENAME": "PvSystModules2021-07.pkl",
                   "BATT_DB_FILENAME": "BatteryModel-01-2021.pkl",
                   "INV_DB_FILENAME": "BigInvCEC07-2021.pkl",
                   "NASA_22YR_FILENAME": "22yr_nasa_monthly.pkl",
                   "MANUAL_PV_DB_FILENAME": "ManualPVModules2020-11.pkl",
                   "USER_UPLOADS_FOLDER": "UserUploads/",
                   "FETCHED_WEATHER_FOLDER": "FetchedWeather/",
                   "AUXFILES_FOLDER": "AuxFiles/",
                   "RESULTS_FOLDER": "Results/",
                   "BOM_FOLDER": "BoM/",
                   "ELECPARAMS": "elec_design_params.json",
                   "FINPARAMS": "finparameter_inputs.json",
                   "SOLARCALCLEADS": "CalcLeads/",
                   "GOOGLE_CLOUD_PROJECT": "solex-mvp-2",
                   "SQLALCHEMY_DATABASE_URI": "mysql+pymysql://root:default123@/refdata?unix_socket=/cloudsql/solex-mvp-2:europe-west6:component-cost",
                   "DB_NAME": "ref_agrola",

                   }}
    for var in Envvar['Environ']:
        os.environ[var] = Envvar['Environ'][var]

BUCKET_NAME = os.environ['BUCKET_NAME']
PV_DB = os.environ['PV_DB_FILENAME']
PVS_DB = os.environ['PVS_DB_FILENAME']
INV_DB = os.environ['INV_DB_FILENAME']
BATT_DB = os.environ['BATT_DB_FILENAME']
ELECPARAMS = os.environ['ELECPARAMS']
FINPARAMS = os.environ['FINPARAMS']
ALLOWED_EXTENSIONS = {'txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif', 'csv'}
BOM = os.environ['BOM_FOLDER']
USER_UPLOADS = os.environ['USER_UPLOADS_FOLDER']
FETCHED_WEATHER = os.environ['FETCHED_WEATHER_FOLDER']
AUXFILES = os.environ['AUXFILES_FOLDER']
RESULTS = os.environ['RESULTS_FOLDER']
MANUALPV_DB = os.environ['MANUAL_PV_DB_FILENAME']
DBNAME = os.environ['DB_NAME']

app = Flask(__name__)


@app.before_first_request
def _load_db():
    global PVDF, INVDF, ELPARM, FINPARM, PVSYSDF, BATTDF
    #    Electrical parameters
    client = storage.Client()
    bucket = client.get_bucket(BUCKET_NAME)
    blob = bucket.get_blob(AUXFILES + ELECPARAMS)
    e = blob.download_as_string()
    ELPARM = json.loads(e)

    #   Financial parameters
    blob = bucket.get_blob(AUXFILES + FINPARAMS)
    f = blob.download_as_string()
    FINPARM = json.loads(f)

    #    Full CEC Inverter Dataframe
    blob = bucket.get_blob(INV_DB)
    inv = blob.download_as_string()
    INVDF = pickle.loads(inv)
    INVDF = INVDF.loc[:, ~INVDF.columns.duplicated()]

    #   Full CEC PV Dataframe
    blob = bucket.get_blob(PV_DB)
    p = blob.download_as_string()
    PVDF = pickle.loads(p)

    PVDF = PVDF[~PVDF.index.duplicated()]
    PVDF = PVDF.loc[:, ~PVDF.columns.duplicated()]

    blob = bucket.get_blob(PVS_DB)
    pvs = blob.download_as_string()
    PVSYSDF = pickle.loads(pvs)
    # PVSYSDF.loc['Wp'] = PVSYSDF.loc['I_mp_ref']*PVSYSDF.loc['V_mp_ref']
    PVSYSDF = PVSYSDF.loc[:, ~PVSYSDF.columns.duplicated()]
    ## Battery Load

    BATTDF = pickle.loads(bucket.get_blob(BATT_DB).download_as_string())


@app.after_request
def add_corsheader(response):
    whitelistpatterns = [re.compile(".*solextron.*\.herokuapp\.com\/?.*"),
                         re.compile(".*solex-mvp-2.*\.appspot\.com\/?.*"),
                         re.compile(".*localhost.*"),
                         re.compile(".*solextron\.com\/?.*"),
                         re.compile(".*\.run\.*app\/?.*")]

    # whitelist =  ["http://localhost","https://3dscene-dot-solex-mvp-2.appspot.com","http://localhost:4200", "http://localhost:8080",
    #           "https://solex-ui-ng-dot-solex-mvp-2.appspot.com","https://solcalcmap-dot-solex-mvp-2.appspot.com","http://localhost:4200/",
    #           "http://solextron-new.herokuapp.com","https://solextron-new.herokuapp.com","https://solextron-solar-calculator.herokuapp.com",
    #           "https://solex-ui-ng-dot-solex-mvp-2.oa.r.appspot.com"]

    weborigin = request.environ.get('HTTP_ORIGIN')
    referer = request.environ.get('HTTP_REFERER')

    if len([weborigin for Wlist in whitelistpatterns if Wlist.match(str(weborigin))]) > 0:
        response.headers["Access-Control-Allow-Origin"] = weborigin
    elif len([referer for Wlist in whitelistpatterns if Wlist.match(str(referer))]) > 0:
        response.headers["Access-Control-Allow-Origin"] = referer
    else:
        response.headers["Access-Control-Allow-Origin"] = "http://localhost:4200"
    response.headers["Access-Control-Allow-Credentials"] = "true"
    response.headers["Access-Control-Allow-Headers"] = "authorization, content-type"
    response.headers["Access-Control-Allow-Methods"] = "GET,POST,OPTIONS"

    return response


@app.route('/sldmaker', methods=['POST'])
@jwt_authenticated
def sldmaker():
    selected_lang = "en"
    lang = request.args.get("lang")
    if lang:
        selected_lang = lang
    ds = datastore.Client()
    gcs = storage.Client()
    bucket = gcs.get_bucket(BUCKET_NAME)
    message = {}
    OutputDic = {}
    req_json = request.get_json()
    phase_name = req_json.get("phase")
    component = req_json.get("component")
    userinput = req_json
    userinput['email'] = request.email

    parent_key = ds.key('users', userinput['email'])
    user_entity = ds.get(key=parent_key)
    if user_entity:
        salesperson_name = user_entity['userdetails']['firstname'] + user_entity['userdetails']['lastname']
    entity = ds.get(key=ds.key('Project', userinput['project_id'], parent=parent_key))
    print("entiyy", entity['projectdetails']['owner'],user_entity['userdetails']['firstname'])
    project_name = entity["projectdetails"]["projectname"]
    company_name = entity["projectdetails"]["customerinfo"]["companyname"]
    project_address = entity["projectdetails"]["projectaddress"]
    print("sawks",salesperson_name)
    project_created_date = entity["created"]
    PV_NAME = entity['FinalSummary']['OverallSummary']['PVName']
    if not entity:
        message.update({"error": "No project with username {} and project {} exists".format(userinput['email'],
                                                                                            userinput['project_id'])})
        return jsonify(message), 400

    uniqueid = 'Uid_' + userinput['email'] + '_Pid_' + userinput['project_id']
    version = entity['Location']['countrycode']
    if entity.get('StringArrayConfig', {}) == {}:
        return jsonify({"error": "String Array config not valid"}), 400

    def str2array(s):
        # Remove space after [
        s = re.sub('\[ +', '[', s.strip())
        # Replace commas and spaces
        s = re.sub('[,\s]+', ', ', s)
        return np.array(ast.literal_eval(s), dtype=object)

    def str_to_list(s):
        new_s = str(s).replace("'", "")
        l = np.array(ast.literal_eval(new_s), dtype=object).tolist()
        return l

    Array4SLD = {"String Config": {}}
    INV_NAME = {}
    if 'SelectedConfig_mu' in entity:
        StringArrayConfigEnt = json.loads(json.dumps(entity.get('StringArrayConfig', {})))
        Array4SLD["String Config"] = {}
        for key, value in StringArrayConfigEnt.items():
            if not [*value["StringConfig"]]:
                continue

            temp_var11 = [*value["StringConfig"]][0]
            temp_var21 = len(value["StringConfig"][temp_var11])
            INV_NAME[temp_var21] = key.replace(' ', '_')
            temp2 = value["StringConfig"]
            for k, v in temp2.items():
                if int(k) in Array4SLD["String Config"]:
                    Array4SLD["String Config"][int(k) + 100] = str_to_list(v)
                else:
                    Array4SLD["String Config"][int(k)] = str_to_list(v)
    else:
        StringArrayConfigEnt = json.loads(json.dumps(entity.get('StringArrayConfig', {})))
        Array4SLD = {"String Config": {int(k): str2array(v).tolist() for k, v in StringArrayConfigEnt.items()}}
        temp_var1 = entity['FinalSummary']['OverallSummary']['InvName']
        if not [*value["StringConfig"]]:
            return jsonify({"error": "Invalid string: {}".format(StringArrayConfigEnt)})

        abc = [*Array4SLD["String Config"]][0]
        INV_NAME[len(Array4SLD["String Config"][abc])] = temp_var1.replace(' ', '_')
        # Array4SLD = {"String Config": {1: [[9, 9, 9, 9, 9], [10, 10], [11], [12], [13], [15], [18], [19]],
        #                          2:[[8],[19],[20],[29],[30],[31],[32]],
        #                          3:[[9,9],[5,5,5],[8],[12],[10]],
        #                          # 4:[[9,9]],

        #                          }}

    if 'SelectedConfig_mu' in entity:
        PVName = entity['SelectedConfig_mu']['Config']['PVName']
        InvName = list(entity['SelectedConfig_mu']['Config']['InvCombo'].keys())

    elif 'FinalSummary' in entity:
        PVName = entity['FinalSummary']['OverallSummary']['PVName'].replace(' ', '_')
        InvName = [entity['FinalSummary']['OverallSummary']['InvName'].replace(' ', '_')]
    elif 'SelectedConfig' in entity:
        PVName = entity['SelectedConfig']['Config']['PVName']
        InvName = [entity['SelectedConfig']['Config']['InvName'].replace(' ', '_')]
    # elif 'SelectedConfig_mu' in entity:
    #     PVName = entity['SelectedConfig_mu']['Config']['PVName']
    #     InvName = list(INV_NAME.values())
    else:
        return jsonify({"error": "Inverter and PV names not avaialble"}), 400

    PV_MODULE = "350W"
    if PVName in PVDF:
        PVData = PVDF.loc[:, PVName]
    elif PVName in PVSYSDF:
        PVData = PVSYSDF.loc[:, PVName]
    else:

        return jsonify({"error": "PV Module name not found"})
    PV_MODULE = PVData.Wp
    # elif blobpv:
    #     USERPVDF = pickle.loads(blobpv.download_as_string())
    #     if PVName in USERPVDF:
    #         PVModuleData = USERPVDF.loc[:,PVName]
    #         PV_MODULE = PVModuleData.Wp

    INVData = None
    max_inputs = {}
    INVData_KW = {}
    inverter_current = {}
    for inv in InvName:
        INVData = INVDF.loc[:, inv]
        max_inputs[inv] = INVData.Nb_MPPT
        INVData_KW[inv] = str(INVData.Paco / 1000) + "kW"
        inverter_current[inv] = INVData.Idcmax

    local_svg_file_name = uniqueid + "_SLD.svg"
    local_dxf_file_name = uniqueid + "{}_SLD.dxf"
    local_pdf_file_name = uniqueid + "_SLD.pdf"
    svg_filename2save = RESULTS + uniqueid + "_SLD.svg"
    dxf_filename2save = RESULTS + uniqueid + "{}_SLD.dxf"
    pdf_filename2save = RESULTS + uniqueid + "_SLD.pdf"

    ## Initialise and run the function
    sld_obj = SLDMaker(Array4SLD, PVData.to_dict(), INVData.to_dict())
    EN_VERSION_COUNTRY_CODE = ["IND", "ARE", "NGA", "POL", "BEL", "GBR"]
    CH_VERSION_COUNTRY_CODE = ["CHE", "DEU", "AUT"]
    if version == "BRA":
        if component:
            component = component[0]
        # success = sld_obj.makebraziliansld(bucket, local_svg_file_name,
        #     local_pdf_file_name, project_name, PV_NAME, INV_NAME,
        #     selected_lang, PV_MODULE, INVData_KW, max_inputs)
        if phase_name == '1-phase' and component == "transformer":
            local_dxf_file_name = local_dxf_file_name.format("1t")
            dxf_filename2save = dxf_filename2save.format("1t")
            success = sld_obj.maketrafouni_braziliansld(local_dxf_file_name, project_name, INV_NAME, selected_lang,
                                                        max_inputs, inverter_current)
        elif phase_name == '2-phase' and component == "transformer":
            local_dxf_file_name = local_dxf_file_name.format("2t")
            dxf_filename2save = dxf_filename2save.format("2t")
            success = sld_obj.maketrafobi_braziliansld(local_dxf_file_name, project_name, INV_NAME, selected_lang,
                                                       max_inputs, inverter_current)
        elif phase_name == '3-phase' and component == "transformer":
            local_dxf_file_name = local_dxf_file_name.format("3t")
            dxf_filename2save = dxf_filename2save.format("3t")
            success = sld_obj.maketrafotri_braziliansld(local_dxf_file_name, project_name, INV_NAME, selected_lang,
                                                        max_inputs, inverter_current)
        elif phase_name == '1-phase' and component != "transformer":
            local_dxf_file_name = local_dxf_file_name.format("1wt")
            dxf_filename2save = dxf_filename2save.format("1wt")
            success = sld_obj.makenotrafouni_braziliansld(local_dxf_file_name, project_name, INV_NAME, selected_lang,
                                                          max_inputs, inverter_current)
        elif phase_name == '2-phase' and component != "transformer":
            local_dxf_file_name = local_dxf_file_name.format("2wt")
            dxf_filename2save = dxf_filename2save.format("2wt")
            success = sld_obj.makenotrafobi_braziliansld(local_dxf_file_name, project_name, INV_NAME, selected_lang,
                                                         max_inputs, inverter_current)
        elif phase_name == '3-phase' and component != "transformer":
            local_dxf_file_name = local_dxf_file_name.format("2wt")
            dxf_filename2save = dxf_filename2save.format("2wt")
            success = sld_obj.makenotrafotri_braziliansld(local_dxf_file_name, project_name, INV_NAME, selected_lang,
                                                          max_inputs, inverter_current)
        else:
            local_dxf_file_name = local_dxf_file_name.format("t")
            dxf_filename2save = dxf_filename2save.format("t")
            success = sld_obj.makenotrafotri_braziliansld(local_dxf_file_name, project_name, INV_NAME, selected_lang,
                                                          max_inputs, inverter_current)

    elif version in CH_VERSION_COUNTRY_CODE:
        local_dxf_file_name = local_dxf_file_name.format("")
        dxf_filename2save = dxf_filename2save.format("")
        success = sld_obj.make_chversion_chsld(local_dxf_file_name, project_name, INV_NAME, selected_lang, PV_NAME,
                                               project_address, company_name, salesperson_name, project_created_date,
                                               list(component),
                                               max_inputs)

    elif version in EN_VERSION_COUNTRY_CODE:
        local_dxf_file_name = local_dxf_file_name.format("")
        dxf_filename2save = dxf_filename2save.format("")
        success = sld_obj.make_enversion_chsld(local_dxf_file_name, project_name, INV_NAME, selected_lang, PV_NAME,
                                               project_address, company_name, salesperson_name, project_created_date,
                                               list(component),
                                               max_inputs)

    else:
        local_dxf_file_name = local_dxf_file_name.format("")
        dxf_filename2save = dxf_filename2save.format("")
        # success = sld_obj.makesld(bucket, local_svg_file_name, local_pdf_file_name, project_name, PV_NAME, INV_NAME,
        #                           selected_lang, PV_MODULE, INVData_KW, max_inputs)
        success = sld_obj.makedxfsld(bucket, local_dxf_file_name, project_name, PV_NAME, INV_NAME,
                                     selected_lang, PV_MODULE, INVData_KW, max_inputs)

    if len(success) > 0:
        blob = bucket.blob(dxf_filename2save)
        blob.upload_from_filename(local_dxf_file_name)
        os.remove(local_dxf_file_name)
        # blob = bucket.blob(pdf_filename2save)
        # blob.upload_from_filename(local_pdf_file_name)
        # os.remove(local_pdf_file_name)
        urltodownload = "https://www.googleapis.com/storage/v1/b/" + BUCKET_NAME + "/o/" + \
                        dxf_filename2save.replace('/', '%2F') + "?alt=media"
        OutputDic.update({"urltodownload": urltodownload})
    else:
        OutputDic.update({"urltodownload": {}})

    return jsonify(OutputDic), 200


if __name__ == '__main__':

    Envvar = {"Environ":
                  {"BUCKET_NAME": "solbucket",
                   "PV_DB_FILENAME": "CECModules09-2020.pkl",
                   "PVS_DB_FILENAME": "PvSystModules2021-07.pkl",
                   "BATT_DB_FILENAME": "BatteryModel-01-2021.pkl",
                   "INV_DB_FILENAME": "BigInvCEC07-2021.pkl",
                   "NASA_22YR_FILENAME": "22yr_nasa_monthly.pkl",
                   "MANUAL_PV_DB_FILENAME": "ManualPVModules2020-11.pkl",
                   "USER_UPLOADS_FOLDER": "UserUploads/",
                   "FETCHED_WEATHER_FOLDER": "FetchedWeather/",
                   "AUXFILES_FOLDER": "AuxFiles/",
                   "RESULTS_FOLDER": "Results/",
                   "BOM_FOLDER": "BoM/",
                   "ELECPARAMS": "elec_design_params.json",
                   "FINPARAMS": "finparameter_inputs.json",
                   "SOLARCALCLEADS": "CalcLeads/",
                   "GOOGLE_CLOUD_PROJECT": "solex-mvp-2",
                   "SQLALCHEMY_DATABASE_URI": "mysql+pymysql://root:default123@/refdata?unix_socket=/cloudsql/solex-mvp-2:europe-west6:component-cost",
                   "DB_NAME": "ref_agrola",

                   }}
    for var in Envvar['Environ']:
        os.environ[var] = Envvar['Environ'][var]
    # os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = os.path.join("/Users","vipluv.aga","Code","auth",
    #                                                             "gcloudauthkeys","solex-mvp-2-e18a10831f31.json")
    credential_path = r"C:\Users\dell\Downloads\application_default_credentials.json"
    # project=r"solex-mvp-2"
    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = credential_path
    os.environ['SQLALCHEMY_LOCALDB_URI'] = "mysql+pymysql://root:default123@127.0.0.1:3306/refdata"
    os.environ['DB_HOST'] = '127.0.0.1'

    os.environ['FLASK_ENV'] = 'development'

    app.run(host='127.0.0.1', port=8080, debug=True, use_reloader=True)