# importing the JSON, matplotlib,
# sys to exit the script and the SchemDraw_PV and elements for the drawing

import json  # for writing, saving and opening JSON files
import sys
from sldcircuitsdxf.utilities import draw_diagram,draw_diagram_brazilian
import ezdxf
from ezdxf.addons import Importer
from ezdxf.math import Vec3
import random
import os



def Ec_dxf(res,pvdata, dxf_file2_name, ProjName, PvName, InvName, lang, Wp, paco, Nb_Mppt):
    values = list(res.values())
    count = 0
    res_data = {}
    max_parallels = []
    for key in values[0].keys():
        count += 1
        count_inverter_input = 0
        count_parallel = 0
        data_list = []
        pvamp = 0
        sum_pvolt = 0
        pvvolt =0
        count_list_len = []
        for data in values[0][key]:
            count_inverter_input += 1
            no_of_input = len(values[0][key])
            count_list_len.append(len(data))
            for num in data:
                count_parallel += 1
                data_list.append(num)
            pvamp += (count_parallel *pvdata['I_mp_ref'])
            sum_pvolt +=(count_parallel*pvdata['V_mp_ref'])
        data_list.append(count_inverter_input)
        data_list.append(count_list_len)
        max_parallels.append(count_inverter_input)
        data_list.append(count_parallel)
        res_data[count] = data_list
        pvvolt=sum_pvolt/count_parallel
        pvdata['Mp'] =count_parallel
        pvdata['NumBB']=len(values[0].keys())
        pvdata['Bat']=1
        pvdata['BatName']='battery'
        pvdata['PVName']='PVName'
        pvdata['Cable']='Cable'
        invdata= {
            'MPPTin':count_inverter_input,
            'InvName':'InvName',
            'InvEff':0.67,
        }

        # with open('inputs.json', 'w') as json_input_file:
        #     json.dump(Out, json_input_file)
        with open('pvdata.json', 'w') as json_input_file:
            json.dump(pvdata, json_input_file)
        with open('invdata.json', 'w') as json_input_file:
            json.dump(invdata, json_input_file)

        data = open("language_translation.json").read()
        lang_json_data = json.loads(data)
        Electricalscheme = lang_json_data[lang]['electricalscheme']
        PVmodule = lang_json_data[lang]['pvmodule']
        InverterDCACtype = lang_json_data[lang]['Inverter DCAC type']
        CombinerBox = lang_json_data[lang]['combinerbox']
        CombinerBoxAllBB = lang_json_data[lang]['combinerboxforbb']
        Inputs = lang_json_data[lang]['inputs']
        Outputs = lang_json_data[lang]['outputs']
        V = lang_json_data[lang]['V']
        A = lang_json_data[lang]['A']
        In = lang_json_data[lang]['in']
        Out = lang_json_data[lang]['out']
        BB = lang_json_data[lang]['BB']
        pv = lang_json_data[lang]['pv']
        cabletype = lang_json_data[lang]['cabletype']
        gridconnection = lang_json_data[lang]['gridconnection']
        GND_name = lang_json_data[lang]['GND']
        InverterNr = lang_json_data[lang]['InverterNr']
        MaxInputs = lang_json_data[lang]['MaxInputs']
        UsedInputs = lang_json_data[lang]['UsedInputs']
        # -----------------------------------------------------------------------------

        # opening the JSON file to be able to read its content

        str_data = open("pvdata.json").read()
        pv_json_data = json.loads(str_data)
        str_data = open("invdata.json").read()
        inv_json_data = json.loads(str_data)

        # -----------------------------------------------------------------------------

        # reading the values in the JSON file
        # and connecting them to the correct variable

        Mp = pv_json_data['Mp']
        Mp = int(Mp)

        NumBB = pv_json_data['NumBB']
        NumBB = int(NumBB)

        Bat = pv_json_data['Bat']
        Bat = int(Bat)

        MPPTin = count_inverter_input
        MPPTin = int(MPPTin)

        PVVolt = pv_json_data['V_mp_ref']
        PVVolt = float(PVVolt)

        PVAmp = pv_json_data['I_mp_ref']
        PVAmp = float(PVAmp)

        BatName = pv_json_data['BatName']

        InvEff = inv_json_data['InvEff']
        InvEff = float(InvEff)

        Cable = pv_json_data['Cable']


        # -----------------------------------------------------------------------------

        # defining parameter needed in loops

        ir = 0
        ip = 0
        global iNb,dots_list,ns,w_l
        iNb = 0
        ns = 0
        w_l = 0
        inv_list= []
        dots_list = []
        doc = ezdxf.new('R2010')

        # Create a block with the name 'FLAG'
        flag = doc.blocks.new(name='FLAG')
        flag2 = doc.blocks.new(name='FLAG2')
        flag_pv = doc.blocks.new(name='PV')
        PH = doc.blocks.new(name='PH')
        PH_labels = doc.blocks.new(name='PH_labels')
        PH.add_line((0, 0), (0.17, 0))
        DASHED = doc.blocks.new(name='DASHED')
        Line = doc.blocks.new(name='LINE')
        Line_down = doc.blocks.new(name='LINE_DOWN')
        grid_connection = doc.blocks.new(name='GRIDCONNECTION')
        grid_connection_label = doc.blocks.new(name='GRID_labels')
        grid_connection.add_lwpolyline([(0, 0.3), (6, 0.3), (6, 0), (0, 0), (0, 0.3)])
        grid_connection.add_line((3, 0), (3, 0.4))
        grid_connection_label.add_attdef('grid_name', (-3.5, 0.1), dxfattribs={'height': 0.2, 'color': 3})
        bb_label = doc.blocks.new(name='BB_labels')
        project_name_label = doc.blocks.new(name='project_name_label')
        ground_name_label = doc.blocks.new(name='ground_name_label')
        bb_label.add_attdef('bb_label', (-2, -1.6), dxfattribs={'height': 0.3, 'color': 3})
        DASHED.add_line((0.5, 0.5), (0.6, 0.5))
        DASHED.add_line((0.9, 0.5), (1, 0.5))
        DASHED.add_line((1.3, 0.5), (1.4, 0.5))
        DASHED.add_line((1.7, 0.5), (1.8, 0.5))
        DASHED.add_line((2.1, 0.5), (2.2, 0.5))
        DASHED.add_line((2.5, 0.5), (2.6, 0.5))
        DASHED.add_line((2.9, 0.5), (3, 0.5))
        DASHED.add_line((3.3, 0.5), (3.4, 0.5))
        DASHED.add_line((3.7, 0.5), (3.8, 0.5))
        DASHED.add_line((4, 0.5), (4.1, 0.5))
        DASHED.add_line((4.4, 0.5), (4.5, 0.5))
        DASHED.add_attdef('NAME3', (5, 0.39), dxfattribs={'height': 0.2, 'color': 3})
        inverter = doc.blocks.new(name='INVERTER')
        inverter_labels = doc.blocks.new(name='inverter_labels')
        inverter_label = doc.blocks.new(name='inverter_label')
        points = [(0, 0), (1, 0), (1, 0.2), (0, 0.2), (0, 0)]
        inverter.add_lwpolyline(points)
        inverter.add_line((1, 0.2), (0, 0))
        inverter.add_line((0.3, 0.15), (0.35, 0.15))
        inverter.add_line((0.3, 0.13), (0.35, 0.13))
        inverter.add_line((0.5, 0), (0.5, -0.1))
        fit_points = [(0, 0, 0), (750, 500, 0), (1750, 500, 0), (2250, 1250, 0)]
        inverter.add_arc((0.6, 0.08), radius=0.01, start_angle=0, end_angle=180)
        inverter.add_arc((0.62, 0.0801), radius=0.01, start_angle=180, end_angle=0)
        combinerbox = doc.blocks.new(name='COMBINERBOX')
        combinerbox.add_circle((0.5, 0.25), radius=0.2)  # add a CIRCLE entity, not required
        # msp.add_lwpolyline([(-0.2, 1), (1, 1)])
        # msp.add_line((1, 1), (1, 0))
        combiner_points = [(0, 0), (1, 0), (1, 0.5), (0, 0.5), (0, 0)]
        combinerbox.add_lwpolyline(combiner_points)
        combinerbox.add_line((0.5, 0.5), (0.5, 0.7))
        combinerbox.add_line((0.5, 0), (0.5, -0.2))
        inverter.add_attdef('inverter_names', (1.2, 0.2), dxfattribs={'height': 0.2, 'color': 3})
        circle = doc.blocks.new(name='CIRCLE')
        circle.add_circle((0.25, 1.25), .05, dxfattribs={'color': 7})
        GND = doc.blocks.new(name='GND')
        GND.add_line((0, 2), (0, 2.1))
        GND.add_line((-0.08, 2), (0.08, 2))
        GND.add_line((-0.05, 1.99), (0.05, 1.99))
        GND.add_line((-0.03, 1.98), (0.03, 1.98))
        points = [(0, 0), (0.5, 0), (0.5, 1), (0, 1), (0, 0)]
        flag.add_lwpolyline(points)  # the flag symbol as 2D polyline
        flag.add_line((0, 1), (0.3, 0.6))
        flag.add_line((0.5, 1), (0.3, 0.6))
        flag.add_line((0.25, -0.2), (0.25, 0))
        flag2.add_lwpolyline(points)  # the flag symbol as 2D polyline
        flag2.add_line((0, 1), (0.3, 0.6))
        flag2.add_line((0.5, 1), (0.3, 0.6))
        flag2.add_line((0.25, -0.2), (0.25, 0))
        flag2.add_line((0.25, -0.6), (0.25, -0.5))
        flag2.add_line((0.25, -1), (0.25, -0.9))
        flag2.add_line((0.25, 1), (0.25, 1.2))
        flag2.add_circle((0.25, 1.25), .05, dxfattribs={'color': 7})
        flag_pv.add_lwpolyline(points)  # the flag symbol as 2D polyline
        flag_pv.add_line((0, 1), (0.3, 0.6))
        flag_pv.add_line((0.5, 1), (0.3, 0.6))
        flag_pv.add_line((0.25, -0.2), (0.25, 0))
        # flag.add_line((0.25, -0.6), (0.25, -0.5))
        # flag_pv.add_line((0.25, -1), (0.25, -0.9))
        flag_pv.add_line((0.25, 1), (0.25, 1.2))
        flag_pv.add_circle((0.25, 1.25), .05, dxfattribs={'color': 7})
        flag.add_attdef('NAME', (-1.9, 2.3), dxfattribs={'height': 0.3, 'color': 3})
        flag2.add_attdef('NAME2', (-2.5, -2), dxfattribs={'height': 0.3, 'color': 3})
        flag_pv.add_attdef('pvname', (1, 0.39), dxfattribs={'height': 0.2, 'color': 3})
        project_name_label.add_attdef('projects_name', (0.5, 2), dxfattribs={'height': 0.5, 'color': 3})
        GND.add_attdef('grounds_name', (-0.9, 2), dxfattribs={'height': 0.2, 'color': 3})
        combinerbox.add_attdef('combinerboxname', (1.2, 0.2), dxfattribs={'height': 0.2, 'color': 3})
        combinerbox.add_attdef('combinerboxname', (1.2, 0.2), dxfattribs={'height': 0.2, 'color': 3})
        PH_labels.add_attdef('cable_name', (3.5, 0), dxfattribs={'height': 0.2, 'color': 3})

        Line.add_line((0, 0), (1, 0))
        Line_down.add_line((0, 0), (0, 1))
        Line.add_attdef('projectname', (-2, 1.5), dxfattribs={'height': 0.3, 'color': 3})
        # flag.add_circle((0, 0), .4, dxfattribs={'color': 2})
        msp = doc.modelspace()

        # Calculating the maximum Voltage and current at different positions

        VL1 = round(pvvolt)  # Labeling Voltage After BB
        VL2 = round(VL1)
        VL3 = 230  # Labeling Voltage After INV

        AL1 = round(pvamp)  # Labeling the current of one string
        AL2 = round(count_parallel * pvamp)  # Labeling the added up current of one BB
        AL3 = round(AL2 * VL2 * InvEff / VL3)  # Labeling the current of all BB

        # -----------------------------------------------------------------------------

        # One way to change the scaling:
        l1 = 2  # size of elements and lines;

        # -----------------------------------------------------------------------------
        # To avoid inputs, which do not make sense and do not work within he program

        if Mp <= 0:
            sys.exit("The variable Mp has an unexpected value (0 or below)!")
        elif NumBB <= 0:
            sys.exit("The variable NumBB has an unexpected value (0 or below)!")
        elif MPPTin <= 0:
            sys.exit("The variable MPPTin has an unexpected value (0 or below)!")
        elif Bat > 2:
            sys.exit("The variable Bat has an unexpected value (NOT 0, 1, 2)!")
        elif Bat < 0:
            sys.exit("The variable Bat has an unexpected value (NOT 0, 1, 2)!")
        else:
            pass

        # -----------------------------------------------------------------------------

        # starting the drawing with d = schem.Drawing()

        # d = schem.Drawing()
        # d.push()  # fixes the drawing point [0 / 0]. Return with d.pop() below.

        def bb(data_raw,key,count_list,count_parallelss,max,total_mpptin,total_dict_length,raw,point):  # function
            global ir, iNb,ns,w_l,w_ls,grid_pos
            MPPTin = key
            counts = count_parallelss
            maximum=max
            # adding the PV modules:
            if Mp:
                # d.add(e.LINE, d='right', l=l1)
                # d.push()
                sir = 1
                end_point = []
                num = 0
                count_parallel = 1
                pv_number = 0
                pv_numbers = 0
                end_stand = []
                frequency = {}

                # iterating over the list
                for item in data_raw:
                    # checking the element in dictionary
                    if item in frequency:
                        # incrementing the counr
                        frequency[item] += 1
                    else:
                        # initializing the count
                        frequency[item] = 1
                end_points ={}
                end_two={}
                # global dots_list
                # dots_list=[]
                temp = 0
                temp2 = 0
                temp7 = 0
                temp6 = 0
                sum_count_list = count_list.copy()
                frequency_count_list = count_list.copy()

                for key in range(1, len(sum_count_list) - 1 + 1):
                    sum_count_list[key] = sum_count_list[key] + sum_count_list[key - 1]
                x_count = 0
                y_count = 0
                # print("len of keys",len(frequency.keys()))
                # print("data raw",data_raw)
                if len(frequency.keys()) == 1 and MPPTin > 1:
                    # import pdb
                    # pdb.set_trace();
                    x_count += 0.78
                    y_count += 1
                    temp = 0
                    temp2 = 0
                    temp6 = 0
                    temp7 = 0
                    x_count = 0
                    y_count = 0
                    c = 0
                    sum_count_list = count_list.copy()
                    frequency_count_list = count_list.copy()
                    for key in range(1, len(sum_count_list) - 1 + 1):
                        sum_count_list[key] = sum_count_list[key] + sum_count_list[key - 1]

                    for data in data_raw:
                        # import pdb;
                        # pdb.set_trace()
                        temp += 1
                        temp6 += 1
                        if raw == 1:
                            x_count += 0.78
                            y_count += 1
                        else:
                            # point =(0.78,1)+ dots_list[-1]
                            # print("pointsss",point)
                            x_count = 0.78 + point[0]
                            y_count = 1 + point[1]
                            x_count += 0.02
                            y_count += 1
                        # if pv_number ==2:
                        #     import pdb
                        #     pdb.set_trace();
                        if temp == sum_count_list[temp2]:
                            temp = 0
                        if temp6 == frequency_count_list[temp7]:
                            temp6 = 0
                        pv_number += 1
                        c += 1
                        if temp7 + 1 < len(frequency_count_list):
                            if len(frequency_count_list) >= 2:
                                if pv_number == (sum_count_list[temp2] + 1) and frequency_count_list[
                                    temp7] >= 5 and frequency_count_list[temp7 + 1] == 4:
                                    count_parallel = 1
                                if pv_number == (sum_count_list[temp2] + 1) and frequency_count_list[
                                    temp7] >= 5 and frequency_count_list[temp7 + 1] == 5:
                                    count_parallel = 1
                                if pv_number == (sum_count_list[temp2] + 1) and frequency_count_list[
                                    temp7] >= 5 and frequency_count_list[temp7 + 1] > 5:
                                    count_parallel = 1

                            if frequency_count_list[temp7] <= 3 and frequency_count_list[temp7 + 1] > 3:
                                count_parallel = 0
                            if (pv_number) == sum_count_list[temp2] + 1 and frequency_count_list[temp7] >= 3 and \
                                    frequency_count_list[temp7 + 1] > 3 and (frequency_count_list[temp7] >= 5 and (
                                    frequency_count_list[temp7 + 1] != 4 and frequency_count_list[temp7 + 1] != 5 and
                                    frequency_count_list[temp7] < 5)):
                                count_parallel = 0
                        if count_parallel >= 3 and sum_count_list[temp7] >= 3 and frequency_count_list[temp7] <= 3:
                            pv_numbers = pv_number - 1
                            count_parallel = 1
                        if frequency_count_list[temp7] >= 3 and (frequency_count_list[temp7] + 1) == count_parallel:
                            pv_numbers = pv_number - 1
                            count_parallel = 1
                        if count_parallel <= 3:
                            if data > 0:
                                x_pos = x_count
                                values = {
                                    'NAME': pv+ str(pv_number) + '-' + '1',
                                    'XPOS': x_pos,
                                    'YPOS': 0
                                }
                                point = (x_count, 0)
                                # Every flag has a different scaling and a rotation of -15 deg.
                                random_scale = 0.5 + random.random() * 2.0
                                # Add a block reference to the block named 'FLAG' at the coordinates 'point'.
                                blockref = msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })
                                dots_list.append(blockref.dxf.insert)
                                blockref.add_auto_attribs(values)
                                point = (x_count, 0.6)
                                block_s = msp.add_blockref('FLAG2', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })
                                x_pos = x_count + 0.5
                                values = {
                                    'NAME2': pv + str(pv_number) + '-' + str(data),
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                block_s.add_auto_attribs(values)
                            if count_parallel == 3 and frequency_count_list[temp7] > 3:
                                point = (x_count, 0)
                                # Every flag has a different scaling and a rotation of -15 deg.
                                random_scale = 0.5 + random.random() * 2.0
                                # Add a block reference to the block named 'FLAG' at the coordinates 'point'.
                                msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })
                                point = (x_count, 0.6)
                                msp.add_blockref('FLAG2', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })

                                point = (x_count, 0.35)
                                msp.add_blockref('DASHED', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })
                                x_count += 0.05
                                point = (x_count, -0.06)
                                msp.add_blockref('PH', point, dxfattribs={
                                    'xscale': 4.9,
                                    'yscale': 0.1,
                                    'rotation': 0
                                })
                                x_count += 0.8
                                point = (x_count, 0)
                                x_pos = x_count
                                values = {
                                    'NAME': pv + str(pv_number+(frequency_count_list[temp7]-3))+ '-' + '1',
                                    'XPOS': x_pos,
                                    'YPOS': 0
                                }
                                blockref = msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })
                                blockref.add_auto_attribs(values)
                                point = (x_count, 0.6)
                                x_pos = x_count + 0.5
                                values = {
                                    'NAME2': pv + str(pv_number+(frequency_count_list[temp7]-3)) + '-' + str(data),
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                block_s = msp.add_blockref('FLAG2', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })
                                block_s.add_auto_attribs(values)

                            if pv_number != len(data_raw):
                                if frequency_count_list[temp7] <= 3 and frequency_count_list[temp7] != 1:
                                    if pv_number != sum_count_list[temp2]:
                                        x_count += 0.05
                                        point = (x_count, -0.06)
                                        ENDPV=msp.add_blockref('PH', point, dxfattribs={
                                            'xscale': 4.9,
                                            'yscale': 0.1,
                                            'rotation': 0
                                        })
                                        end_stand.append(ENDPV)

                                if count_parallel < 3 and frequency_count_list[temp2] > 3:
                                    x_count += 0.05
                                    point = (x_count, -0.06)
                                    ENDPV=msp.add_blockref('PH', point, dxfattribs={
                                        'xscale': 4.9,
                                        'yscale': 0.1,
                                        'rotation': 0
                                    })
                                    end_stand.append(ENDPV)


                            else:
                                end_stand.append(ENDPV)

                            if frequency_count_list[temp7] == 1 and len(frequency.keys()) == 1:
                                x_count += 0.05
                                point = (x_count, -0.06)
                                ENDPV = msp.add_blockref('LINE_DOWN', point, dxfattribs={
                                    'xscale': 3,
                                    'yscale': -0.2,
                                    'rotation': 0
                                })

                        else:
                            x_count -= 0.65
                            y_count -= 1
                        end_two[data] = ENDPV
                        if MPPTin > 1 and len(frequency.keys()) == 1:
                            if pv_number == (sum_count_list[temp2]):
                                end_point.append(ENDPV)
                                end_points[data] = ENDPV
                            if temp7 + 1 < len(frequency_count_list):
                                if frequency_count_list[temp7] >= 5 and (
                                        frequency_count_list[temp7 + 1] == 4 or frequency_count_list[temp7 + 1] == 5 or
                                        frequency_count_list[temp7 + 1] > 5) and len(
                                        frequency_count_list) >= 2:
                                    if pv_number == sum_count_list[temp2] + 1:
                                        temp2 += 1
                                        temp7 += 1
                                else:
                                    if pv_number == sum_count_list[temp2]:
                                        temp2 += 1
                                        temp7 += 1
                        else:
                            if num != data and (count_parallel != len(data_raw) or len(data_raw)) == 1:
                                end_point.append(ENDPV)
                                # end_points[data] = ENDPV
                                num = data
                        count_parallel += 1
                    ir = 0
                    siNb = iNb + 1

                elif len(frequency.keys()) != len(count_list):
                    pv_number = 0
                    count_parallel = 1
                    temp = 0
                    temp2 = 0
                    temp6 = 0
                    temp7 = 0
                    x_count = 0
                    y_count = 0
                    sum_count_list = count_list.copy()
                    frequency_count_list = count_list.copy()
                    for key in range(1, len(sum_count_list) - 1 + 1):
                        sum_count_list[key] = sum_count_list[key] + sum_count_list[key - 1]
                    for data in data_raw:
                        # import pdb;
                        # pdb.set_trace()
                        temp += 1
                        temp6 += 1
                        if raw == 1:
                            x_count += 0.78
                            y_count += 1
                        else:
                            # point =(0.78,1)+ dots_list[-1]
                            # print("pointsss",point)
                            x_count = 0.78 + point[0]
                            y_count = 1 + point[1]
                            x_count += 0.02
                            y_count += 1
                        if temp == count_list[temp2]:
                            temp = 0
                        if temp6 == frequency_count_list[temp7]:
                            temp6 = 0
                        pv_number += 1
                        if temp7 + 1 < len(frequency_count_list):
                            if len(frequency_count_list) >= 2:
                                if pv_number == (sum_count_list[temp2] + 1) and frequency_count_list[
                                    temp7] >= 5 and frequency_count_list[temp7 + 1] == 4:
                                    count_parallel = 1
                                if pv_number == (sum_count_list[temp2] + 1) and frequency_count_list[
                                    temp7] >= 5 and frequency_count_list[temp7 + 1] == 5:
                                    count_parallel = 1
                                if pv_number == (sum_count_list[temp2] + 1) and frequency_count_list[
                                    temp7] >= 5 and frequency_count_list[temp7 + 1] > 5:
                                    count_parallel = 1
                            if frequency_count_list[temp7] <= 3 and frequency_count_list[temp7 + 1] > 3:
                                count_parallel = 0
                            if (pv_number) == sum_count_list[temp2] + 1 and frequency_count_list[temp7] >= 3 and \
                                    frequency_count_list[temp7 + 1] > 3 and (frequency_count_list[temp7] >= 5 and (
                                    frequency_count_list[temp7 + 1] != 4 and frequency_count_list[temp7 + 1] != 5 and
                                    frequency_count_list[temp7 + 1] < 5)):
                                count_parallel = 0
                        if count_parallel >= 3 and sum_count_list[temp7] >= 3 and frequency_count_list[temp7] <= 3:
                            pv_numbers = pv_number - 1
                            count_parallel = 1
                        if frequency_count_list[temp7] >= 3 and (frequency_count_list[temp7] + 1) == count_parallel:
                            pv_numbers = pv_number - 1
                            count_parallel = 1
                        if count_parallel <= 3:
                            if data > 0:
                                x_pos = x_count
                                values = {
                                    'NAME': pv+ str(pv_number) + '-' + '1',
                                    'XPOS': x_pos,
                                    'YPOS': 0
                                }
                                point = (x_count, 0)
                                # Every flag has a different scaling and a rotation of -15 deg.
                                random_scale = 0.5 + random.random() * 2.0
                                # Add a block reference to the block named 'FLAG' at the coordinates 'point'.
                                blockref = msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })
                                dots_list.append(blockref.dxf.insert)
                                blockref.add_auto_attribs(values)
                                point = (x_count, 0.6)
                                block_s = msp.add_blockref('FLAG2', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })
                                x_pos = x_count + 0.5
                                values = {
                                    'NAME2': pv + str(pv_number) + '-' + str(data),
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                block_s.add_auto_attribs(values)


                            if count_parallel == 3 and frequency_count_list[temp7] > 3:
                                point = (x_count, 0)
                                # Every flag has a different scaling and a rotation of -15 deg.
                                random_scale = 0.5 + random.random() * 2.0
                                # Add a block reference to the block named 'FLAG' at the coordinates 'point'.
                                msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })
                                point = (x_count, 0.6)
                                msp.add_blockref('FLAG2', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })

                                point = (x_count, 0.35)
                                msp.add_blockref('DASHED', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })
                                x_count += 0.05
                                point = (x_count, -0.06)
                                msp.add_blockref('PH', point, dxfattribs={
                                    'xscale': 4.9,
                                    'yscale': 0.1,
                                    'rotation': 0
                                })
                                x_count += 0.8
                                point = (x_count, 0)
                                x_pos = x_count
                                values = {
                                    'NAME': pv + str(pv_number + (frequency[data] - 3)) + '-' + '1',
                                    'XPOS': x_pos,
                                    'YPOS': 0
                                }
                                blockref = msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })
                                blockref.add_auto_attribs(values)
                                point = (x_count, 0.6)
                                x_pos = x_count + 0.5
                                values = {
                                    'NAME2': pv + str(pv_number + (frequency[data] - 3)) + '-' + str(data),
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                block_s = msp.add_blockref('FLAG2', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })
                                block_s.add_auto_attribs(values)

                            if pv_number != len(data_raw):
                                if frequency_count_list[temp7] <= 3 and frequency_count_list[temp7] != 1:
                                    if pv_number != sum_count_list[temp2]:
                                        print("gg1")
                                        x_count += 0.05
                                        point = (x_count, -0.06)
                                        ENDPV=msp.add_blockref('PH', point, dxfattribs={
                                            'xscale': 4.9,
                                            'yscale': 0.1,
                                            'rotation': 0
                                        })
                                        end_stand.append(ENDPV)

                                if count_parallel < 3 and frequency_count_list[temp2] > 3:
                                    x_count += 0.05
                                    point = (x_count, -0.06)
                                    ENDPV=msp.add_blockref('PH', point, dxfattribs={
                                        'xscale': 4.9,
                                        'yscale': 0.1,
                                        'rotation': 0
                                    })
                                    end_stand.append(ENDPV)


                            else:
                                end_stand.append(ENDPV)
                            if frequency_count_list[temp7] == 1 and len(frequency.keys()) >= 1:
                                x_count += 0.05
                                point = (x_count, -0.06)
                                ENDPV = msp.add_blockref('LINE_DOWN', point, dxfattribs={
                                    'xscale': 3,
                                    'yscale': -0.2,
                                    'rotation': 0
                                })
                        else:
                            x_count -= 0.65
                            y_count -= 1
                        end_two[data] = ENDPV
                        # print("count list tem[", count_list[temp2])
                        if len(frequency.keys()) >= 1:
                            if pv_number == (sum_count_list[temp2]):
                                end_point.append(ENDPV)
                                end_points[data] = ENDPV
                            if temp7 + 1 < len(frequency_count_list):
                                if frequency_count_list[temp7] >= 5 and (
                                        frequency_count_list[temp7 + 1] == 4 or frequency_count_list[temp7 + 1] == 5 or
                                        frequency_count_list[temp7 + 1] > 5) and len(
                                    frequency_count_list) >= 2:
                                    if pv_number == sum_count_list[temp2] + 1:
                                        temp2 += 1
                                        temp7 += 1
                                else:
                                    if pv_number == sum_count_list[temp2]:
                                        temp2 += 1
                                        temp7 += 1
                        else:
                            if num != data and (count_parallel != len(data_raw) or len(data_raw)) == 1:
                                num = data
                                end_point.append(ENDPV)
                                # end_points[data] = ENDPV
                        count_parallel += 1
                    ir = 0
                    siNb = iNb + 1

                else:
                    for data in data_raw:
                        temp += 1
                        temp6 += 1

                        if raw == 1:
                            x_count += 0.78
                            y_count += 1
                        else:
                            x_count = 0.78 + point[0]
                            y_count =1 + point[1]
                            x_count += 0.02
                            y_count += 1

                        if temp == count_list[temp2]:
                            temp = 0
                        if temp6 == frequency_count_list[temp7]:
                            temp6 = 0
                        pv_number += 1
                        if temp7 + 1 < len(frequency_count_list):
                            if frequency_count_list[temp7] <= 3 and frequency_count_list[
                                temp7 + 1] > 3:
                                count_parallel = 0
                        if count_parallel >= 3 and sum_count_list[temp7] >= 3 and frequency_count_list[temp7] <= 3:
                            pv_numbers = pv_number - 1
                            count_parallel = 1
                        if num != data and count_parallel >= 3:
                            pv_numbers = pv_number - 1
                            count_parallel = 1
                        if count_parallel <= 3:
                            if data > 0:
                                x_pos = x_count
                                values = {
                                    'NAME': pv + str(pv_number) + '-' + '1',
                                    'XPOS': x_pos,
                                    'YPOS': 0
                                }
                                point = (x_count, 0)
                                # Every flag has a different scaling and a rotation of -15 deg.
                                random_scale = 0.5 + random.random() * 2.0
                                # Add a block reference to the block named 'FLAG' at the coordinates 'point'.
                                blockref = msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })
                                dots_list.append(blockref.dxf.insert)
                                blockref.add_auto_attribs(values)
                                point = (x_count, 0.6)
                                block_s = msp.add_blockref('FLAG2', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })
                                x_pos = x_count + 0.5
                                values = {
                                    'NAME2': pv + str(pv_number) + '-' + str(data),
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                block_s.add_auto_attribs(values)
                            if count_parallel == 3 and frequency[data] > 3:
                                point = (x_count, 0)
                                # Every flag has a different scaling and a rotation of -15 deg.
                                random_scale = 0.5 + random.random() * 2.0
                                # Add a block reference to the block named 'FLAG' at the coordinates 'point'.
                                msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })
                                point = (x_count, 0.6)
                                msp.add_blockref('FLAG2', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })

                                point = (x_count, 0.35)
                                x_pos = x_count + 0.5
                                values = {
                                    'NAME3':  str(frequency[data]) + 'string',
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                blockref=msp.add_blockref('DASHED', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })
                                blockref.add_auto_attribs(values)
                                x_count += 0.05
                                point = (x_count, -0.06)
                                msp.add_blockref('PH', point, dxfattribs={
                                    'xscale': 5,
                                    'yscale': 0.1,
                                    'rotation': 0
                                })
                                x_count += 0.8
                                point = (x_count, 0)
                                x_pos = x_count
                                values = {
                                    'NAME': pv + str(pv_number + (frequency[data] - 3)) + '-' + '1',
                                    'XPOS': x_pos,
                                    'YPOS': 0
                                }
                                blockref = msp.add_blockref('FLAG', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })
                                blockref.add_auto_attribs(values)
                                point = (x_count, 0.6)
                                x_pos = x_count + 0.5
                                values = {
                                    'NAME2': pv + str(pv_number + (frequency[data] - 3)) + '-' + str(data),
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                block_s = msp.add_blockref('FLAG2', point, dxfattribs={
                                    'xscale': 0.2,
                                    'yscale': 0.3,
                                    'rotation': 0
                                })
                                block_s.add_auto_attribs(values)
                            if pv_number != len(data_raw):
                                if frequency[data] <= 3 and frequency[data] != 1:
                                    if pv_number != sum_count_list[temp2]:
                                        x_count += 0.05
                                        point = (x_count, -0.06)
                                        ENDPV=msp.add_blockref('PH', point, dxfattribs={
                                            'xscale': 4.9,
                                            'yscale': 0.1,
                                            'rotation': 0
                                        })
                                        end_stand.append(ENDPV)

                                if count_parallel < 3 and frequency[data] > 3:
                                    x_count += 0.05
                                    point = (x_count, -0.06)
                                    ENDPV =msp.add_blockref('PH', point, dxfattribs={
                                        'xscale': 4.9,
                                        'yscale': 0.1,
                                        'rotation': 0
                                    })
                                    end_stand.append(ENDPV)


                            if frequency[data] == 1:
                                x_count += 0.05
                                point = (x_count, -0.06)
                                ENDPV = msp.add_blockref('LINE_DOWN', point, dxfattribs={
                                         'xscale': 3,
                                         'yscale': -0.2,
                                         'rotation': 0
                                          })
                        else:
                            x_count -= 0.65
                            y_count -= 1
                        end_two[data] = ENDPV
                        if num != data and (count_parallel != len(data_raw) or len(data_raw)) == 1:
                            end_point.append(ENDPV)
                            end_points[data] = ENDPV
                            num = data
                            # temp2 += 1
                        if pv_number == sum_count_list[temp2]:
                            temp2 += 1
                            temp7 += 1
                            end_point.append(ENDPV)
                            end_points[data] = ENDPV
                        count_parallel += 1
                    ir = 0
                    siNb = iNb + 1

            # adding the Combiner Box, the Inverters and the Batteries:
            if MPPTin < Mp:  # if Inv inputs < than number of parallels --> add CB
                    # import pdb
                    # pdb.set_trace();
                    CBout = 1
                    # d.push()
                    count = 0
                    print("higigfirst" ,Mp, MPPTin)
                    diff = maximum - 150
                    if diff % 60 == 0:
                        diff = int(diff / 60)
                    else:
                        diff = int(diff / 60) + 1
                    if maximum <= 20:
                        n = 6.5
                        n_length = 19.3
                        m_length = 19.65
                        length = 20.9
                        # l=30
                        ns = 13.95
                        w_l = 4
                        w_ls = 4
                        grid_pos=4
                        inv_y=6
                        # name_y = 0.69
                        # in_y = 0
                        # out_y = 0
                        # max_y = 0
                        # used_y = 0
                        # BB_x = 48
                        # inverter_x = 9
                    elif maximum > 20 and maximum <= 60:
                        n = 12
                        n_length = 30.2
                        m_length = 30.65
                        length = 28
                        # l=46
                        ns = 18.5
                        w_l = 4
                        w_ls = 4
                        grid_pos=4
                        inv_y=14
                        # name_y = 26
                        # in_y = 26
                        # out_y = 25.5
                        # max_y = 27
                        # used_y = 27
                        # BB_x = 70
                        # inverter_x = 13
                    elif maximum > 60 and maximum <= 150:
                        n = 18
                        n_length = 42.3
                        m_length = 42.6
                        length = 28
                        # l=69
                        ns = 63.5
                        w_l = 14.5
                        w_ls = 4
                        grid_pos=4
                        inv_y = 30.9
                    else:
                        n = (diff * 12.5) + 18
                        n_length = (25 * diff) + 42.3
                        m_length = (25 * diff) + 42.6
                        ns = (5 * diff) + 63.5
                        inv_y = (diff * 26) + 30.9
                        w_l = (diff * 3) +15
                        w_ls = (diff * 3) + 4
                        grid_pos=(diff*3)+14.5
                    if MPPTin <=20:
                        l = 8
                        BB_x=42
                        # inverter_x=9
                        inv_length=1.5
                        # inv_y=6
                    # else:
                    #     l = 15
                    #     BB_x=45
                    #     ns=10
                    #     inverter_x = -30

                    elif MPPTin > 20 and MPPTin <= 60:
                        l = 15
                        BB_x=76
                        # inverter_x = 18
                        inv_length = 3
                        # inv_y=14

                    elif MPPTin > 60 and MPPTin <= 150:
                        l = 29
                        BB_x=146
                        inverter_x = -73
                        # w_l=16.5
                        # w_ls=17.5
                        inv_length = 12
                    else:
                        l = (diff * 10) + 29
                        BB_x=(diff*13.5)+146
                        inverter_x = (diff*13.5)+-60
                        w_l = (diff*13.5)+19.5
                        inv_length = (diff*4)+12
                    print("BB_x",BB_x)
                    inverters=doc.blocks.get(str(MPPTin))
                    if not inverters:
                        inverter_label=doc.blocks.new(str(MPPTin))
                    else:
                        inverter_label=doc.blocks.get(str(MPPTin))

                    inverter_label.add_attdef('inverter_name', (-3, -0.3), dxfattribs={'height': 0.2, 'color': 3})
                    inverter_label.add_attdef('inverter_max_inputs', (-3, 0.3), dxfattribs={'height': 0.2, 'color': 3})
                    inverter_label.add_attdef('inverter_used_inputs', (-3, 0.6), dxfattribs={'height': 0.2, 'color': 3})
                    inverter_label.add_attdef('inverter_in_name', (BB_x, 0), dxfattribs={'height': 0.2, 'color': 3})
                    inverter_label.add_attdef('inverter_out_name', (BB_x, 0.5), dxfattribs={'height': 0.2, 'color': 3})
                    x_pos = 5
                    values = {
                        'inverter_max_inputs': MaxInputs+': ' + str(Nb_Mppt),
                        'inverter_used_inputs': UsedInputs+': ' + str(MPPTin),
                        'inverter_in_name': In + str(VL2) + '/' + str(AL2),
                        'inverter_out_name': Out + str(VL3) + '/' + str(AL3),
                        'inverter_name': InverterNr,
                        'XPOS': x_pos,
                        'YPOS': 0
                    }
                    global blockref_inverter;
                    point = (list(end_points.values())[len(end_points) - 1].dxf.insert[0] + inv_length,
                             list(end_points.values())[len(end_points) - 1].dxf.insert[1] - inv_y)
                    blockref_inverter = msp.add_blockref('INVERTER', point, dxfattribs={
                        'xscale': l,
                        'yscale': 1.5,
                        'rotation': 0
                    })
                    # blockref = msp.add_blockref('inverter_label', blockref_inverter.dxf.insert, dxfattribs={
                    #     'xscale': 0.2,
                    #     'yscale': 0.2,
                    #     'rotation': 0
                    # })
                    # blockref.add_auto_attribs(values)
                    inv_list.append(blockref_inverter)
                    print("inverterist",inv_list,inv_list[-1])
                    blockref = msp.add_blockref(str(MPPTin), inv_list[-1].dxf.insert, dxfattribs={
                        'xscale': 0.2,
                        'yscale': 0.2,
                        'rotation': 0
                    })
                    # a=doc.blocks.get('inverter_label')
                    # blockref.delete_attrib('inverter_in_name')
                    # print(inverter_label.has_attrib('inverter_in_name'))
                    # a.delete_attrib('inverter_in_name')
                    # for flag_ref in msp.query('INSERT[name=="inverter_label"]'):
                    #     if flag_ref.has_attrib('inverter_in_name'):
                    #         print(str(flag_ref))
                    #         flag.delete_attrib('inverter_in_name')
                    #         print("getsss",flag.del_dxf_attrib('inverter_in_name'))
                    blockref.add_auto_attribs(values)
                    draw_diagram(end_points, data_raw, frequency, counts, end_point, blockref_inverter,'LINE_DOWN' ,count_list,
                                 MPPTin, maximum, diff, total_mpptin, total_dict_length, Mp,doc, msp)

            else:
                    # import pdb
                    # pdb.set_trace();
                    print("higig",Mp,MPPTin)
                    CBout = 1
                    # d.push()
                    count = 0
                    diff = maximum - 150
                    if diff % 60 == 0:
                        diff = int(diff / 60)
                    else:
                        diff = int(diff / 60) + 1
                    if maximum <= 20:
                        n = 6.5
                        n_length = 19.3
                        m_length = 19.65
                        length = 20.9
                        # l=30
                        ns = 13.95
                        w_l = 4
                        w_ls = 4
                        grid_pos=4
                        inv_y=6
                    elif maximum > 20 and maximum <= 60:
                        n = 12
                        n_length = 30.2
                        m_length = 30.65
                        length = 28
                        # l=46
                        ns = 18.5
                        w_l = 4
                        w_ls = 4
                        grid_pos=7.5
                        inv_y=14
                    elif maximum > 60 and maximum <= 150:
                        n = 18
                        n_length = 42.3
                        m_length = 42.6
                        length = 28
                        # l=69
                        ns = 63.5
                        w_l = 14.5
                        w_ls = 4
                        grid_pos=4
                        inv_y=30.9

                    else:
                        n = (diff * 12.5) + 18
                        n_length = (25 * diff) + 42.3
                        m_length = (25 * diff) + 42.6
                        ns = (5 * diff) + 63.5
                        w_l = 4
                        w_ls = (diff * 3)+10.5
                        grid_pos=(diff*3)+14.5
                        inv_y = (diff * 26)+30.9
                    if MPPTin <= 20:
                        l = 8
                        BB_x = 42
                        inverter_x = 9
                        inv_length = 1.5
                        # inv_y=14
                        # w_l = 4
                        # w_ls=7.5
                        # name_y=0.69
                        # in_y = 0
                        # out_y = 0
                        # max_y = 0
                        # used_y = 0
                    # else:
                    #     l = 15
                    #     BB_x=45
                    #     ns=10
                    #     inverter_x = -30

                    elif MPPTin > 20 and MPPTin <= 60:
                        l = 15
                        BB_x = 76
                        inverter_x = 12
                        # w_l = 4
                        # w_ls=7.5
                        inv_length = 3
                        # inv_y=14
                        # name_y=26
                        # in_y=26
                        # out_y=25.5
                        # max_y=27
                        # used_y=27

                    elif MPPTin > 60 and MPPTin <= 150:
                        l = 29
                        BB_x = 146
                        inverter_x = -60
                        # w_l = 16.5
                        # w_ls=17.5
                        inv_length = 12
                        # name_y = 84
                        # in_y = 84
                        # out_y = 85
                        # max_y = 88
                        # used_y = 88
                    else:
                        l = (diff * 10) + 29
                        BB_x = (diff * 13.5) + 146
                        inverter_x = (diff * 13.5) + 12
                        inv_length = (diff * 4) + 12
                    inverters = doc.blocks.get(str(MPPTin))
                    if not inverters:
                        inverter_label = doc.blocks.new(str(MPPTin))
                    else:
                        inverter_label = doc.blocks.get(str(MPPTin))

                    inverter_label.add_attdef('inverter_name', (-3, -0.3), dxfattribs={'height': 0.2, 'color': 3})
                    inverter_label.add_attdef('inverter_max_inputs', (-3, 0.3), dxfattribs={'height': 0.2, 'color': 3})
                    inverter_label.add_attdef('inverter_used_inputs', (-3, 0.6),
                                               dxfattribs={'height': 0.2, 'color': 3})
                    inverter_label.add_attdef('inverter_in_name', (BB_x, 0), dxfattribs={'height': 0.2, 'color': 3})
                    inverter_label.add_attdef('inverter_out_name', (BB_x, 0.5), dxfattribs={'height': 0.2, 'color': 3})
                    x_pos = 5
                    values = {
                        'inverter_max_inputs': MaxInputs+': ' + str(Nb_Mppt),
                        'inverter_used_inputs': UsedInputs+': ' + str(MPPTin),
                        'inverter_in_name': In + str(VL2) + '/' + str(AL2),
                        'inverter_out_name': Out + str(VL3) + '/' + str(AL3),
                        'inverter_name': InverterNr,
                        'XPOS': x_pos,
                        'YPOS': 0
                    }

                    point = (list(end_points.values())[len(end_points) - 1].dxf.insert[0] + inv_length,
                             list(end_points.values())[len(end_points) - 1].dxf.insert[1] - inv_y)
                    blockref_inverter = msp.add_blockref('INVERTER', point, dxfattribs={
                        'xscale': l,
                        'yscale': 1.5,
                        'rotation': 0
                    })
                    blockref = msp.add_blockref(str(MPPTin), blockref_inverter.dxf.insert, dxfattribs={
                        'xscale': 0.2,
                        'yscale': 0.2,
                        'rotation': 0
                    })
                    blockref.add_auto_attribs(values)
                    inv_list.append(blockref_inverter)
                    draw_diagram(end_points, data_raw, frequency, counts, end_point, blockref_inverter, 'LINE_DOWN',
                                 count_list,
                                 MPPTin, maximum, diff, total_mpptin, total_dict_length, Mp, doc,msp)


            iNb = iNb + 1

        # ----------------------------------------------------------------------------

        # Placement of the BB label on y-axis
        BBLabelY = 0
        BBLabelY = -3.2

        # Placement of the BB label on x-axis
        BBLabelX = 0

        # Calling the main function bb(), to create the Building Blocks:
        raw = 0
        no=0
        # import pdb
        # pdb.set_trace();
        project_name = {
            'projects_name': str(ProjName),
            'XPOS': 1,
            'YPOS': 0.5
        }
        point = (0.62, 0.98)
        blockref_project = msp.add_blockref('project_name_label', point, dxfattribs={
            'xscale': 0.2,
            'yscale': 0.3,
            'rotation': 0
        })
        blockref_project.add_auto_attribs(project_name)
        ground_name = {
            'grounds_name':GND_name,
            'XPOS': 0.2,
            'YPOS': 0.5
        }
        if NumBB == 1:
            for key in res_data:
                no += 1
                # import pdb
                # pdb.set_trace();
                raw += 1
                max_parallel = max(max_parallels)
                total_mpptin=sum(max_parallels)
                if raw == 1:
                    values = {
                        'bb_label': BB + str(1),
                        'XPOS': 1,
                        'YPOS': 0.5
                    }
                    point = (0.35, 0.9)
                    bb_labels = msp.add_blockref('BB_Labels', point, dxfattribs={
                        'xscale': 0.2,
                        'yscale': 0.3,
                        'rotation': 0
                    })
                    bb_labels.add_auto_attribs(values)
                    point = (0.62, 0.98)
                    blockref_line = msp.add_blockref('LINE', point, dxfattribs={
                        'xscale': 0.2,
                        'yscale': 0.3,
                        'rotation': 0
                    })
                    point = (0.62, -0.07)
                    blockref = msp.add_blockref('GND', point, dxfattribs={
                        'xscale': 0.5,
                        'yscale': 0.5,
                        'rotation': 0
                    })
                    blockref.add_auto_attribs(ground_name)

                    bb(res_data[key][:-3], res_data[key][-3], res_data[key][-2],res_data[key][-1],max_parallel,total_mpptin,len(res_data),raw,point)
                else:
                    bb(res_data[key][:-3], res_data[key][-3], res_data[key][-2],res_data[key][-2],max_parallel,total_mpptin,len(res_data),raw,point)

        else:
            # import pdb;
            # pdb.set_trace()
            for key in res_data:
                raw += 1
                no+= 1
                max_parallel = max(max_parallels)
                total_mpptin=sum(max_parallels)
                if len(res_data) == len(values[0].keys()):
                    if raw == 1:
                        point = (0.62, 0.98)
                        bb_values = {
                            'bb_label': BB + str(1),
                            'XPOS': 1,
                            'YPOS': 0.5
                        }
                        lb_point = (0.5, 0.98)
                        bb_labels = msp.add_blockref('BB_Labels', lb_point, dxfattribs={
                            'xscale': 0.2,
                            'yscale': 0.3,
                            'rotation': 0
                        })
                        bb_labels.add_auto_attribs(bb_values)
                        blockref_line = msp.add_blockref('LINE', point, dxfattribs={
                            'xscale': 0.2,
                            'yscale': 0.3,
                            'rotation': 0
                        })
                        point = (0.62, -0.07)
                        blockref = msp.add_blockref('GND', point, dxfattribs={
                            'xscale': 0.5,
                            'yscale': 0.5,
                            'rotation': 0
                        })
                        blockref.add_auto_attribs(ground_name)
                        bb(res_data[key][:-3], res_data[key][-3], res_data[key][-2], res_data[key][-1], max_parallel,
                           total_mpptin, len(res_data),raw,point)
                        # BB1DOT = d.add(e.DOT)
                    else:
                        point = (ns, 0.98) + dots_list[-1]
                        bb_values = {
                            'bb_label': BB + str(no),
                            'XPOS': 1,
                            'YPOS': 0.5
                        }
                        lb_point = (ns, 0.98) + dots_list[-1]
                        bb_labels = msp.add_blockref('BB_Labels', lb_point, dxfattribs={
                            'xscale': 0.2,
                            'yscale': 0.3,
                            'rotation': 0
                        })
                        bb_labels.add_auto_attribs(bb_values)
                        gnd_points = (point[0] + 0.65, 0.98)
                        blockref_line = msp.add_blockref('LINE', gnd_points, dxfattribs={
                            'xscale': 0.2,
                            'yscale': 0.3,
                            'rotation': 0
                        })
                        point = (ns, -0.07) + dots_list[-1]
                        gnd_points = (point[0] + 0.65, -0.065)
                        blockref = msp.add_blockref('GND', gnd_points, dxfattribs={
                            'xscale': 0.5,
                            'yscale': 0.5,
                            'rotation': 0
                        })
                        blockref.add_auto_attribs(ground_name)
                        bb(res_data[key][:-3], res_data[key][-3], res_data[key][-2], res_data[key][-1], max_parallel,
                           total_mpptin, len(res_data),raw,point)
                        point_x1 = blockref_inverter.dxf.insert[0] + w_l
                        point_y1 = blockref_inverter.dxf.insert[1] - 0.15
                        point_x2 = inv_list[0].dxf.insert[0] + w_ls
                        point_y2 = inv_list[0].dxf.insert[1] - 0.15
                        msp.add_line((point_x1, point_y1), (point_x2, point_y2))
        # # -----------------------------------------------------------------------------
        #
    pv_name = {
            'pvname': PVmodule+" "+ str(PvName)+" " + str(Wp),
            'XPOS': 1.3,
            'YPOS': 0.2
        }
    point = (ns, 0.6)+ dots_list[-1]
    block_s = msp.add_blockref('PV', point, dxfattribs={
            'xscale': 0.2,
            'yscale': 0.3,
            'rotation': 0
        })
    block_s.add_auto_attribs(pv_name)
    cable_name = {
            'cable_name': cabletype,
            'XPOS': 5,
            'YPOS': 0.2
        }
    combinerbox_name = {
        'combinerboxname': CombinerBox,
        'XPOS': 1.3,
        'YPOS': 0.2
    }
    point = (ns, 0.6) + dots_list[-1]
    point = (ns-0.05, 0.35) + dots_list[-1]
    block_ss = msp.add_blockref('COMBINERBOX', point, dxfattribs={
            'xscale': 0.2,
            'yscale': 0.3,
            'rotation': 0
        })
    block_ss.add_auto_attribs(combinerbox_name)
    inverter_name = {
            'inverter_names': InverterDCACtype+" "+paco+" "+ str(InvName),
            'XPOS': 1.3,
            'YPOS': 0.2
        }
    point = (ns-0.05, 0.22) + dots_list[-1]
    inverter_blocks = msp.add_blockref('INVERTER', point, dxfattribs={
            'xscale': 0.3,
            'yscale': 0.3,
            'rotation': 0
        })
    inverter_blocks.add_auto_attribs(inverter_name)
    point = (ns, 0.12) + dots_list[-1]
    block_ss = msp.add_blockref('PH', point, dxfattribs={
        'xscale': 5,
        'yscale': 0.3,
        'rotation': 0
    })
    block_ss = msp.add_blockref('PH_labels', block_ss.dxf.insert, dxfattribs={
        'xscale': 0.3,
        'yscale': 0.3,
        'rotation': 0
    })
    block_ss.add_auto_attribs(cable_name)
    point = (inv_list[0].dxf.insert[0]+grid_pos-0.29,inv_list[0].dxf.insert[1]-0.5)
        # x_pos = x_count
    grid_values = {
            'grid_name': gridconnection,
            'XPOS': 2,
            'YPOS': 0
        }
    blockref_grids=msp.add_blockref('GRIDCONNECTION', point, dxfattribs={
            'xscale': 0.1,
            'yscale': 0,
            'rotation': 0
        })
    blockref_grids = msp.add_blockref('GRID_labels', blockref_grids.dxf.insert, dxfattribs={
        'xscale': 0.3,
        'yscale': 0.3,
        'rotation': 0
    })
    blockref_grids.add_auto_attribs(grid_values)

    doc.saveas(dxf_file2_name)