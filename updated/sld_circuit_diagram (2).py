# importing the JSON, matplotlib,
# sys to exit the script and the SchemDraw_PV and elements for the drawing

import json  # for writing, saving and opening JSON files
import matplotlib
import os
from os import listdir
import sys
import subprocess  # open pdf after plot
from sldcircuit.PVScheme import SchemDraw_PV as schem  # import SchemDraw_PV as schem
from sldcircuit.PVScheme.SchemDraw_PV import elements_PV as e# import the elements
from sldcircuit.PVScheme.utilities import draw,draw_two,draw_diagram_brazilian,draw_diagram,brazilian_version,outline_diagram
# matplotlib.backend_bases.register_backend('dxf', FigureCanvasDxf)
from matplotlib.backends.backend_ps import FigureCanvasPS
# matplotlib.backend_bases.register_backend('dxf', FigureCanvasDxf)


matplotlib.use('Agg')  # Set the backend here, to save as PDF
res = {}
res={"String Config":{1: [[9, 9], [10, 10]],

 2: [[9], [10]],

 }}


def Ec_brazilian(res, pvdata, invdata, svg_file2_name, pdf_file2_name, ProjName, PvName, InvName, lang, Wp, paco, Nb_Mppt):
    values = list(res.values())
    count = 0
    res_data = {}
    max_parallels = []
    invs = []
    global max_value
    max_value = 0
    for key in values[0].keys():
        count += 1
        count_inverter_input = 0
        count_parallel = 0
        data_list = []
        pvamp = 0
        sum_pvolt = 0
        pvvolt =0
        count_list_len = []
        for data in values[0][key]:
            count_inverter_input += 1
            no_of_input = len(values[0][key])
            count_list_len.append(len(data))
            for num in data:
                count_parallel += 1
                data_list.append(num)
            pvamp += (count_parallel *pvdata['I_mp_ref'])
            sum_pvolt +=(count_parallel*pvdata['V_mp_ref'])
        data_list.append(count_inverter_input)
        data_list.append(count_list_len)
        max_parallels.append(count_inverter_input)
        data_list.append(count_parallel)
        res_data[count] = data_list
        pvvolt=sum_pvolt/count_parallel
        pvdata['Mp'] =count_parallel
        pvdata['NumBB']=len(values[0].keys())
        pvdata['Bat']=1
        pvdata['BatName']='battery'
        pvdata['PVName']='PVName'
        pvdata['Cable']='Cable'
        invdata= {
            'MPPTin':count_inverter_input,
            'InvName':'InvName',
            'InvEff':0.67,
        }

        # with open('inputs.json', 'w') as json_input_file:
        #     json.dump(Out, json_input_file)
        with open('pvdata.json', 'w') as json_input_file:
            json.dump(pvdata, json_input_file)
        with open('invdata.json', 'w') as json_input_file:
            json.dump(invdata, json_input_file)

        data = open("language_translation.json").read()
        lang_json_data = json.loads(data)
        Electricalscheme = lang_json_data[lang]['electricalscheme']
        PVmodule = lang_json_data[lang]['pvmodule']
        InverterDCACtype = lang_json_data[lang]['Inverter DCAC type']
        CombinerBox = lang_json_data[lang]['combinerbox']
        CombinerBoxAllBB = lang_json_data[lang]['combinerboxforbb']
        Inputs = lang_json_data[lang]['inputs']
        Outputs = lang_json_data[lang]['outputs']
        V = lang_json_data[lang]['V']
        A = lang_json_data[lang]['A']
        In = lang_json_data[lang]['in']
        Out = lang_json_data[lang]['out']
        BB = lang_json_data[lang]['BB']
        pv = lang_json_data[lang]['pv']
        cabletype = lang_json_data[lang]['cabletype']
        gridconnection = lang_json_data[lang]['gridconnection']
        GND = lang_json_data[lang]['GND']
        InverterNr = lang_json_data[lang]['InverterNr']
        MaxInputs = lang_json_data[lang]['MaxInputs']
        UsedInputs = lang_json_data[lang]['UsedInputs']
        # -----------------------------------------------------------------------------

        # opening the JSON file to be able to read its content

        str_data = open("pvdata.json").read()
        pv_json_data = json.loads(str_data)
        str_data = open("invdata.json").read()
        inv_json_data = json.loads(str_data)

        # -----------------------------------------------------------------------------

        # reading the values in the JSON file
        # and connecting them to the correct variable

        Mp = pv_json_data['Mp']
        Mp = int(Mp)

        NumBB = pv_json_data['NumBB']
        NumBB = int(NumBB)

        Bat = pv_json_data['Bat']
        Bat = int(Bat)

        MPPTin = count_inverter_input
        MPPTin = int(MPPTin)

        PVVolt = pv_json_data['V_mp_ref']
        PVVolt = float(PVVolt)

        PVAmp = pv_json_data['I_mp_ref']
        PVAmp = float(PVAmp)

        BatName = pv_json_data['BatName']

        InvEff = inv_json_data['InvEff']
        InvEff = float(InvEff)

        Cable = pv_json_data['Cable']


        # -----------------------------------------------------------------------------

        # defining parameter needed in loops

        ir = 0
        ip = 0
        global iNb,inv,inv_count,max
        iNb = 0
        inv_count = 0
        inv_list= []
        # invs = []

        # Calculating the maximum Voltage and current at different positions

        VL1 = round(pvvolt)  # Labeling Voltage After BB
        VL2 = round(VL1)
        VL3 = 230  # Labeling Voltage After INV

        AL1 = round(pvamp)  # Labeling the current of one string
        AL2 = round(count_parallel * pvamp)  # Labeling the added up current of one BB
        AL3 = round(AL2 * VL2 * InvEff / VL3)  # Labeling the current of all BB

        # -----------------------------------------------------------------------------

        # One way to change the scaling:
        l1 = 2  # size of elements and lines;

        # -----------------------------------------------------------------------------
        # To avoid inputs, which do not make sense and do not work within he program

        if Mp <= 0:
            sys.exit("The variable Mp has an unexpected value (0 or below)!")
        elif NumBB <= 0:
            sys.exit("The variable NumBB has an unexpected value (0 or below)!")
        elif MPPTin <= 0:
            sys.exit("The variable MPPTin has an unexpected value (0 or below)!")
        elif Bat > 2:
            sys.exit("The variable Bat has an unexpected value (NOT 0, 1, 2)!")
        elif Bat < 0:
            sys.exit("The variable Bat has an unexpected value (NOT 0, 1, 2)!")
        else:
            pass

        # -----------------------------------------------------------------------------

        # starting the drawing with d = schem.Drawing()

        d = schem.Drawing()
        d.push()  # fixes the drawing point [0 / 0]. Return with d.pop() below.

        def bb(data_raw,key,count_list,count_parallelss,max,total_mpptin,total_dict_length):  # function
            global ir, iNb,ms
            MPPTin = key
            counts = count_parallelss
            maximum=max
            # adding the PV modules:
            if Mp:
                d.add(e.LINE, d='right', l=l1)
                d.push()
                sir = 1
                end_point = []
                num = 0
                count_parallel = 1
                pv_number = 0
                pv_numbers = 0
                end_stand = []
                frequency = {}

                # iterating over the list
                for item in data_raw:
                    # checking the element in dictionary
                    if item in frequency:
                        # incrementing the counr
                        frequency[item] += 1
                    else:
                        # initializing the count
                        frequency[item] = 1
                end_points ={}
                end_two={}
                global dots_list
                dots_list=[]
                # print("len of keys",len(frequency.keys()))
                # print("data raw",data_raw)
                if len(frequency.keys())==1 and MPPTin>1 :
                    # import pdb
                    # pdb.set_trace();
                    color_list = ['red', 'blue', 'green', 'yellow', 'pink', 'brown', 'orange', 'violet','purple','indigo']
                    color = 0
                    temp = 0
                    temp2 = 0
                    temp6 = 0
                    temp7 = 0
                    c=0
                    sum_count_list=count_list.copy()
                    frequency_count_list = count_list.copy()
                    for key in range(1, len(sum_count_list) - 1 + 1):
                        sum_count_list[key] = sum_count_list[key] + sum_count_list[key - 1]

                    for data in data_raw:
                        # import pdb;
                        # pdb.set_trace()
                        temp += 1
                        temp6 += 1
                        # if pv_number ==2:
                        #     import pdb
                        #     pdb.set_trace();
                        if color == len(color_list) - 1:
                            color = 0
                        if temp == sum_count_list[temp2]:
                            temp = 0
                        if temp6 == frequency_count_list[temp7]:
                            temp6 = 0
                        pv_number += 1
                        c += 1
                        if temp7 + 1 < len(frequency_count_list):
                            if len(frequency_count_list) >= 2:
                                if pv_number == (sum_count_list[temp2]+1) and frequency_count_list[
                                    temp7] >= 5 and frequency_count_list[temp7 + 1] == 4:
                                    count_parallel = 1
                                if pv_number == (sum_count_list[temp2] + 1) and frequency_count_list[
                                    temp7] >= 5 and frequency_count_list[temp7 + 1] == 5:
                                    count_parallel = 1
                                if pv_number == (sum_count_list[temp2]+1) and frequency_count_list[
                                    temp7] >= 5 and frequency_count_list[temp7 + 1] > 5:
                                    count_parallel = 1

                            if frequency_count_list[temp7] <= 3 and frequency_count_list[temp7 + 1] > 3:
                                count_parallel = 0
                            if (pv_number) == sum_count_list[temp2] + 1 and frequency_count_list[temp7] >= 3 and \
                                    frequency_count_list[temp7 + 1] > 3 and (frequency_count_list[temp7]>=5 and (frequency_count_list[temp7+1]!=4 and frequency_count_list[temp7+1]!=5 and frequency_count_list[temp7]<5)):
                                count_parallel = 0
                        if count_parallel >= 3 and sum_count_list[temp7] >= 3 and frequency_count_list[temp7] <= 3:
                            pv_numbers = pv_number - 1
                            count_parallel = 1
                        if frequency_count_list[temp7] >= 3 and (frequency_count_list[temp7] + 1) == count_parallel:
                            pv_numbers = pv_number - 1
                            count_parallel = 1
                        if count_parallel <= 3:
                            if data > 0:
                                sf = d.add(e.PV,color=color_list[color],d='down', label=pv + str(pv_number) + '-' + '1')
                                dots_list.append(sf)
                                dot = d.add(e.PH)
                                sd = d.add(e.PV,color=color_list[color], label=pv + str(pv_number) + '-' + str(data))
                                d.add(e.LINE, d='left', xy=sd.end - [0.12, -6.9], l=1.9)
                                joint = d.add(e.LINE, d='down', l=1)
                                circle = d.add(e.Round_Circle, xy=joint.end + [-0.9, 1.2])
                                line = d.add(e.LINE, d='left', xy=circle.end + [0.5, -0.8], l=0.9)
                                d.add(e.VSS, d='left', xy=circle.end + [0, -0.8], l=0.5)
                                d.add(e.GND, xy=circle.end + [0.9, -0.8])
                                d.add(e.LABEL, xy=circle.end + [0.5, 0], toplabel='#6mm² PVC 70º' + '\n' + ' 750V')
                                d.add(e.LABEL, xy=circle.end + [4.65, -4.5], fontsize=8,
                                      toplabel="Tensão Nominal: 273,7V" + "\n" + "Corrente Nominal: 10,49A")
                                d.add(e.LABEL, xy=circle.end + [4.75, -6.3],
                                      toplabel="CANADIAN SOLAR" + "\n" + "CS3W - 410P" + "\n" + str(data) + "x410Wp")
                            if count_parallel == 3 and frequency_count_list[temp7] > 3:
                                label = d.add(e.PH, d='right', xy=sf.end - [0, 1], l=5)
                                d.add(e.LABEL, xy=label.end + [1, 0],
                                      label=str(frequency_count_list[temp7]) + 'string')
                                ENDPV = d.add(e.LINE, d='right', xy=sd.end, l=2.69)
                                end_stand.append(ENDPV)
                                x = d.add(e.LINE)
                                d.add(e.DOT, xy=sf.start + [5, 0])
                                sf = d.add(e.PV,color=color_list[color], d='down', label=pv + str(pv_number+(frequency_count_list[temp7]-3)) + '-' + '1')
                                dot = d.add(e.PH)
                                sd = d.add(e.PV,color=color_list[color], label=pv + str(pv_number+(frequency_count_list[temp7]-3)) + '-' + str(data))
                                d.add(e.LINE, d='left', xy=sd.end - [0.12, -6.9], l=1.8)
                                joint = d.add(e.LINE, d='down', l=1)
                                circle = d.add(e.Round_Circle, xy=joint.end + [-0.9, 1.2])
                                line = d.add(e.LINE, d='left', xy=circle.end + [0.5, -0.8], l=0.9)
                                d.add(e.VSS, d='left', xy=circle.end + [0, -0.8], l=0.5)
                                d.add(e.GND, xy=circle.end + [0.9, -0.8])
                                d.add(e.LABEL, xy=circle.end + [0.5, 0], toplabel='#6mm² PVC 70º' + '\n' + ' 750V')
                                d.add(e.LABEL, xy=circle.end + [4.65, -4.5], fontsize=8,
                                      toplabel="Tensão Nominal: 273,7V" + "\n" + "Corrente Nominal: 10,49A")
                                d.add(e.LABEL, xy=circle.end + [4.75, -6.3],
                                      toplabel="CANADIAN SOLAR" + "\n" + "CS3W - 410P" + "\n" + str(data) + "x410Wp")
                                if len(data_raw) != (pv_number + frequency_count_list[temp7] - 3):
                                    d.add(e.DOT, xy=sf.start + [5, 0])
                            if pv_number != len(data_raw):
                                # ENDPV = d.add(e.LINE, d='right')
                                # # end_stand.append(ENDPV)
                                # if count_parallel != frequency[data] and frequency[data]<=3 and frequency[data]!=1:
                                #     ENDPV = d.add(e.LINE, d='right',l=2.2)
                                #     end_stand.append(ENDPV)
                                #     # x = d.add(e.LINE)
                                #     d.add(e.DOT,xy=sf.start+[2.2,0])
                                if frequency_count_list[ temp7] <= 3 and frequency_count_list[temp7] != 1:
                                    if pv_number!=sum_count_list[temp2]:
                                        ENDPV = d.add(e.LINE, d='right', l=4.95,xy=sd.end+[0,-0])
                                        end_stand.append(ENDPV)
                                        # x = d.add(e.LINE)
                                        d.add(e.DOT, xy=sf.start + [5, 0])
                                # if frequency[data] == 1:
                                #     ENDPV = d.add(e.DOT)
                                #     d.add(e.DOT, xy=sf.start + [2.2, 0])
                                if frequency_count_list[temp7] == 1 and len(frequency.keys()) == 1:
                                    ENDPV = d.add(e.DOT,xy=sd.end+[0,-0])
                                    d.add(e.DOT, xy=sf.start + [5, 0])

                                # else:
                                #     d.add(e.DOT, xy=sf.start + [2.5, 0])
                                # if count_parallel <3 and frequency[data] >3:
                                #     ENDPV = d.add(e.LINE, d='right',xy=sd.end,l=2.2)
                                #     end_stand.append(ENDPV)
                                #     # x = d.add(e.LINE)
                                #     global dots
                                #     d.add(e.DOT, xy=sf.start + [2.2, 0])
                                # else:
                                #         if count_parallel !=3 or frequency[data]<=3 or (count_parallel!=3 and frequency[data]>3) :
                                #             d.add(e.DOT, xy=sf.start + [2.2, 0])
                                if count_parallel < 3 and frequency_count_list[temp2] > 3:
                                    ENDPV = d.add(e.LINE, d='right', xy=sd.end+[0,-0], l=5)
                                    end_stand.append(ENDPV)
                                    # x = d.add(e.LINE)
                                    global dots
                                    d.add(e.DOT, xy=sf.start + [5, 0])
                                else:
                                    if count_parallel != 3 or frequency_count_list[temp7] <= 3 or (
                                            count_parallel != 3 and frequency_count_list[temp7] > 3):
                                        d.add(e.DOT, xy=sf.start + [5, 0])

                                # d.add(e.LINE, xy=sf.start)
                                # d.add(e.LINE)
                            else:
                                ENDPV = d.add(e.DOT,xy=sd.end+[0,-0])
                                end_stand.append(ENDPV)
                        end_two[data] = ENDPV
                        if MPPTin > 1 and len(frequency.keys()) == 1:
                            if pv_number == (sum_count_list[temp2]):
                                end_point.append(ENDPV)
                                end_points[data] = ENDPV
                                color += 1
                            if temp7+1 < len(frequency_count_list):
                                if frequency_count_list[temp7] >= 5 and (frequency_count_list[temp7 + 1] == 4 or frequency_count_list[temp7+1]==5 or frequency_count_list[temp7+1] > 5)and len(
                                        frequency_count_list) >= 2:
                                    if pv_number == sum_count_list[temp2] + 1:
                                        temp2 += 1
                                        temp7 += 1
                                else:
                                    if pv_number == sum_count_list[temp2]:
                                        temp2 += 1
                                        temp7 += 1
                        else:
                            if num != data and (count_parallel != len(data_raw) or len(data_raw)) == 1:
                                end_point.append(ENDPV)
                                end_points[data] = ENDPV
                                num = data
                        count_parallel += 1
                    ir = 0
                    siNb = iNb + 1
                elif len(frequency.keys())!= len(count_list) :
                    pv_number=0
                    count_parallel=1
                    color_list = ['red', 'blue', 'green', 'yellow', 'pink', 'brown', 'orange', 'violet','purple','indigo']
                    color = 0
                    temp = 0
                    temp2 = 0
                    temp6 = 0
                    temp7 = 0
                    sum_count_list=count_list.copy()
                    frequency_count_list = count_list.copy()
                    for key in range(1, len(sum_count_list) - 1 + 1):
                        sum_count_list[key] = sum_count_list[key] + sum_count_list[key - 1]
                    for data in data_raw:
                        # import pdb;
                        # pdb.set_trace()
                        temp += 1
                        temp6 += 1
                        if color == len(color_list)-1:
                            color=0
                        if temp == count_list[temp2]:
                            temp = 0
                        if temp6 == frequency_count_list[temp7]:
                            temp6 = 0
                        pv_number += 1
                        if temp7 + 1 < len(frequency_count_list):
                            if len(frequency_count_list) >= 2:
                                if pv_number == (sum_count_list[temp2]+1) and frequency_count_list[
                                    temp7] >= 5 and frequency_count_list[temp7 + 1] == 4:
                                    count_parallel = 1
                                if pv_number == (sum_count_list[temp2]+1 ) and frequency_count_list[
                                    temp7] >= 5 and frequency_count_list[temp7 + 1] == 5:
                                    count_parallel = 1
                                if pv_number == (sum_count_list[temp2]+1) and frequency_count_list[
                                    temp7] >= 5 and frequency_count_list[temp7 + 1] > 5:
                                    count_parallel = 1
                            if frequency_count_list[temp7] <= 3 and frequency_count_list[temp7 + 1] > 3:
                                count_parallel = 0
                            if (pv_number) == sum_count_list[temp2] + 1 and frequency_count_list[temp7] >= 3 and \
                                    frequency_count_list[temp7 + 1] > 3 and (frequency_count_list[temp7] >= 5 and (
                                    frequency_count_list[temp7 + 1] != 4 and frequency_count_list[temp7 + 1] != 5 and frequency_count_list[temp7 + 1] < 5)):
                                count_parallel = 0
                        # print("pvumner", pv_number)
                        # print("frkddkddldldllist", frequency_count_list[temp2])
                        # if num != data and count_parallel > 3:
                        #     pv_numbers = pv_number -1
                        # if num != data and count_parallel >= 3 :
                        #     pv_numbers = pv_number - 1
                        #     count_parallel = 1
                        if count_parallel >= 3 and sum_count_list[temp7] >= 3 and frequency_count_list[temp7] <= 3:
                            pv_numbers = pv_number - 1
                            count_parallel = 1
                        if frequency_count_list[temp7] >= 3 and (frequency_count_list[temp7] + 1) == count_parallel:
                            pv_numbers = pv_number - 1
                            count_parallel = 1
                        if count_parallel <= 3:
                            if data > 0:
                                sf = d.add(e.PV,color=color_list[color],d='down', label=pv + str(pv_number) + '-' + '1')
                                dots_list.append(sf)
                                dot = d.add(e.PH)
                                sd = d.add(e.PV,color=color_list[color], label=pv + str(pv_number) + '-' + str(data))
                                d.add(e.LINE, d='left', xy=sd.end - [0.12, -6.9], l=1.9)
                                joint = d.add(e.LINE, d='down', l=1)
                                circle = d.add(e.Round_Circle, xy=joint.end + [-0.9, 1.2])
                                line = d.add(e.LINE, d='left', xy=circle.end + [0.5, -0.8], l=0.9)
                                d.add(e.VSS, d='left', xy=circle.end + [0, -0.8], l=0.5)
                                d.add(e.GND, xy=circle.end + [0.9, -0.8])
                                d.add(e.LABEL, xy=circle.end + [0.5, 0], toplabel='#6mm² PVC 70º' + '\n' + ' 750V')
                                d.add(e.LABEL, xy=circle.end + [4.65, -4.5], fontsize=8,
                                      toplabel="Tensão Nominal: 273,7V" + "\n" + "Corrente Nominal: 10,49A")
                                d.add(e.LABEL, xy=circle.end + [4.75, -6.3],
                                      toplabel="CANADIAN SOLAR" + "\n" + "CS3W - 410P" + "\n" + str(data) + "x410Wp")
                            if count_parallel == 3 and frequency_count_list[temp7] > 3:
                                label = d.add(e.PH, d='right', xy=sf.end - [0, 1], l=5)
                                d.add(e.LABEL, xy=label.end + [1, 0],
                                      label=str(frequency_count_list[temp7]) + 'string')
                                ENDPV = d.add(e.LINE, d='right', xy=sd.end, l=2.69)
                                end_stand.append(ENDPV)
                                x = d.add(e.LINE)
                                d.add(e.DOT, xy=sf.start + [5, 0])
                                sf = d.add(e.PV,color=color_list[color], d='down', label=pv + str(pv_number+(frequency_count_list[temp7]-3)) + '-' + '1')
                                dot = d.add(e.PH)
                                sd = d.add(e.PV,color=color_list[color], label=pv + str(pv_number+(frequency_count_list[temp7]-3)) + '-' + str(data))
                                d.add(e.LINE, d='left', xy=sd.end - [0.12, -6.9], l=1.9)
                                joint = d.add(e.LINE, d='down', l=1)
                                circle = d.add(e.Round_Circle, xy=joint.end + [-0.9, 1.2])
                                line = d.add(e.LINE, d='left', xy=circle.end + [0.5, -0.8], l=0.9)
                                d.add(e.VSS, d='left', xy=circle.end + [0, -0.8], l=0.5)
                                d.add(e.GND, xy=circle.end + [0.9, -0.8])
                                d.add(e.LABEL, xy=circle.end + [0.5, 0], toplabel='#6mm² PVC 70º' + '\n' + ' 750V')
                                d.add(e.LABEL, xy=circle.end + [4.65, -4.5], fontsize=8,
                                      toplabel="Tensão Nominal: 273,7V" + "\n" + "Corrente Nominal: 10,49A")
                                d.add(e.LABEL, xy=circle.end + [4.75, -6.3],
                                      toplabel="CANADIAN SOLAR" + "\n" + "CS3W - 410P" + "\n" + str(data) + "x410Wp")
                                if len(data_raw) != (pv_number + frequency_count_list[temp7] - 3):
                                    d.add(e.DOT, xy=sf.start + [5, 0])
                            if pv_number != len(data_raw):
                                # ENDPV = d.add(e.LINE, d='right')
                                # # end_stand.append(ENDPV)
                                # if count_parallel != frequency[data] and frequency[data]<=3 and frequency[data]!=1:
                                #     ENDPV = d.add(e.LINE, d='right',l=2.2)
                                #     end_stand.append(ENDPV)
                                #     # x = d.add(e.LINE)
                                #     d.add(e.DOT,xy=sf.start+[2.2,0])
                                if  frequency_count_list[ temp7] <= 3 and frequency_count_list[temp7] != 1:
                                    if pv_number!=sum_count_list[temp2]:
                                        ENDPV = d.add(e.LINE, d='right', l=4.95,xy=sd.end+[0,-0])
                                        end_stand.append(ENDPV)
                                        # x = d.add(e.LINE)
                                        d.add(e.DOT, xy=sf.start + [5, 0])
                                # if frequency[data] == 1:
                                #     ENDPV = d.add(e.DOT)
                                #     d.add(e.DOT, xy=sf.start + [2.2, 0])
                                if frequency_count_list[temp7] == 1 and len(frequency.keys()) >= 1:
                                    ENDPV = d.add(e.DOT,xy=sd.end+[0,-0])
                                    d.add(e.DOT, xy=sf.start + [5, 0])

                                # else:
                                #     d.add(e.DOT, xy=sf.start + [2.5, 0])
                                # if count_parallel <3 and frequency[data] >3:
                                #     ENDPV = d.add(e.LINE, d='right',xy=sd.end,l=2.2)
                                #     end_stand.append(ENDPV)
                                #     # x = d.add(e.LINE)
                                #     global dots
                                #     d.add(e.DOT, xy=sf.start + [2.2, 0])
                                # else:
                                #         if count_parallel !=3 or frequency[data]<=3 or (count_parallel!=3 and frequency[data]>3) :
                                #             d.add(e.DOT, xy=sf.start + [2.2, 0])
                                if count_parallel < 3 and frequency_count_list[temp2] > 3:
                                    ENDPV = d.add(e.LINE, d='right', xy=sd.end+[0,-0], l=5)
                                    end_stand.append(ENDPV)
                                    # x = d.add(e.LINE)
                                    global dots
                                    d.add(e.DOT, xy=sf.start + [5, 0])
                                else:
                                    if count_parallel != 3 or frequency_count_list[temp7] <= 3 or (
                                            count_parallel != 3 and frequency_count_list[temp7] > 3):
                                        d.add(e.DOT, xy=sf.start + [5, 0])

                                # d.add(e.LINE, xy=sf.start)
                                # d.add(e.LINE)
                            else:
                                ENDPV = d.add(e.DOT,xy=sd.end+[0,-0])
                                end_stand.append(ENDPV)
                        end_two[data] = ENDPV
                        # print("count list tem[", count_list[temp2])
                        if len(frequency.keys()) >= 1:
                            if pv_number == (sum_count_list[temp2]):
                                end_point.append(ENDPV)
                                end_points[data] = ENDPV
                                color += 1
                            if temp7 + 1 < len(frequency_count_list):
                                if frequency_count_list[temp7] >= 5 and (
                                        frequency_count_list[temp7 + 1] == 4 or frequency_count_list[temp7 + 1] == 5 or frequency_count_list[temp7 + 1] > 5 )and len(
                                        frequency_count_list) >= 2:
                                    if pv_number == sum_count_list[temp2] + 1:
                                        temp2 += 1
                                        temp7 += 1
                                else:
                                    if pv_number == sum_count_list[temp2]:
                                        temp2 += 1
                                        temp7 += 1
                        else:
                            if num != data and (count_parallel != len(data_raw) or len(data_raw)) == 1:
                                end_point.append(ENDPV)
                                num = data
                        count_parallel += 1
                    ir = 0
                    siNb = iNb + 1
                else:
                    # import pdb
                    # pdb.set_trace();
                    color_list=['red','blue','green','yellow','pink','brown','orange','violet','purple','indigo']
                    color = 0
                    temp = 0
                    temp2 =0
                    temp7 = 0
                    temp6 = 0
                    sum_count_list = count_list.copy()
                    frequency_count_list = count_list.copy()

                    for key in range(1, len(sum_count_list) - 1 + 1):
                        sum_count_list[key] = sum_count_list[key] + sum_count_list[key - 1]
                    for data in data_raw:
                        # import pdb;
                        # pdb.set_trace()
                        temp +=1
                        temp6 +=1
                        if color == len(color_list)-1:
                            color =0
                        if num !=data:
                            color += 1
                        if temp == count_list[temp2]:
                            temp = 0
                        if temp6 == frequency_count_list[temp7]:
                            temp6 = 0
                        pv_number += 1
                        if temp7 + 1 < len(frequency_count_list):
                            if frequency_count_list[temp7] <= 3 and frequency_count_list[
                                temp7 + 1] > 3:
                                count_parallel = 0
                        if count_parallel >= 3 and sum_count_list[temp7] >= 3 and frequency_count_list[temp7] <= 3:
                            pv_numbers = pv_number - 1
                            count_parallel = 1
                        if num != data and count_parallel >= 3:
                            pv_numbers = pv_number - 1
                            count_parallel = 1
                        if count_parallel <= 3:
                            if data > 0:
                                sf = d.add(e.PV, d='down',color=color_list[color], label=pv + str(pv_number) + '-' + '1')
                                dots_list.append(sf)
                                dot = d.add(e.PH)
                                sd = d.add(e.PV,color=color_list[color], label=pv + str(pv_number) + '-' + str(data))
                                d.add(e.LINE,d='left',xy=sd.end-[0.12,-6.9],l=1.9)
                                joint=d.add(e.LINE, d='down',l=1)
                                circle=d.add(e.Round_Circle,xy=joint.end+[-0.9,1.2])
                                line=d.add(e.LINE, d='left', xy=circle.end+[0.5,-0.8],l=0.9)
                                d.add(e.VSS, d='left', xy=circle.end + [0, -0.8], l=0.5)
                                d.add(e.GND,xy=circle.end + [0.9,-0.8])
                                d.add(e.LABEL, xy=circle.end + [0.5, 0], toplabel='#6mm² PVC 70º'+'\n'+' 750V')
                                d.add(e.LABEL, xy=circle.end + [4.65, -4.5],fontsize=8,
                                      toplabel="Tensão Nominal: 273,7V"+"\n"+ "Corrente Nominal: 10,49A")
                                d.add(e.LABEL, xy=circle.end + [4.75, -6.3], toplabel="CANADIAN SOLAR"+"\n"+"CS3W - 410P"+"\n"+str(data)+"x410Wp")
                            if count_parallel == 3 and frequency[data] > 3:
                                label = d.add(e.PH, d='right', xy=sf.end - [0, 1], l=5)
                                d.add(e.LABEL, xy=label.end + [1, 0],
                                      label=str(frequency[data]) + 'string')
                                # d.add(e.PH,d='right',xy=sd.start,l=4.2)
                                ENDPV = d.add(e.LINE, d='right', xy=sd.end, l=2.69)
                                end_stand.append(ENDPV)
                                x = d.add(e.LINE)
                                # d.add(e.LINE, xy=sf.start)
                                # d.add(e.LINE)
                                d.add(e.DOT, xy=sf.start + [5, 0])
                                sf = d.add(e.PV,color=color_list[color], d='down', label=pv + str(pv_number+(frequency[data]-3)) + '-' + '1')
                                dot = d.add(e.PH)
                                sd = d.add(e.PV,color=color_list[color], label=pv + str(pv_number+(frequency[data]-3)) + '-' + str(data))
                                d.add(e.LINE, d='left', xy=sd.end - [0.12, -6.9], l=1.9)
                                joint = d.add(e.LINE, d='down', l=1)
                                circle = d.add(e.Round_Circle, xy=joint.end + [-0.9, 1.2])
                                line = d.add(e.LINE, d='left', xy=circle.end + [0.5, -0.8], l=0.9)
                                d.add(e.VSS, d='left', xy=circle.end + [0, -0.8], l=0.5)
                                d.add(e.GND, xy=circle.end + [0.9, -0.8])
                                d.add(e.LABEL, xy=circle.end + [0.5, 0], toplabel='#6mm² PVC 70º' + '\n' + ' 750V')
                                d.add(e.LABEL, xy=circle.end + [4.65, -4.5], fontsize=8,
                                      toplabel="Tensão Nominal: 273,7V" + "\n" + "Corrente Nominal: 10,49A")
                                d.add(e.LABEL, xy=circle.end + [4.75, -6.3],
                                      toplabel="CANADIAN SOLAR" + "\n" + "CS3W - 410P" + "\n" + str(data) + "x410Wp")
                                # d.add(e.LINE, d='left', xy=sd.end - [0.3, -1.5], l=0.5)
                                if len(data_raw) != (pv_number + frequency_count_list[temp7] - 3):
                                    d.add(e.DOT, xy=sf.start + [5, 0])
                                    # dots_diagram(dots_end, d)
                            if pv_number != len(data_raw):
                                # ENDPV = d.add(e.LINE, d='right')
                                # end_stand.append(ENDPV)
                                if frequency[data] <= 3 and frequency[data] != 1:
                                    if pv_number != sum_count_list[temp2]:
                                        ENDPV = d.add(e.LINE, d='right',xy=sd.end+[0,-0], l=5)
                                        end_stand.append(ENDPV)
                                        # x = d.add(e.LINE)
                                        dots_end=d.add(e.DOT, xy=sf.start + [5, 0])
                                        # dots_diagram(dots_end, d)
                                        # d.push()
                                    # ENDPV = d.add(e.LINE, d='right', l=2.2)
                                    # end_stand.append(ENDPV)
                                    # # x = d.add(e.LINE)
                                    # d.add(e.DOT, xy=sf.start + [2.2, 0])
                                if frequency[data] == 1:
                                    ENDPV = d.add(e.DOT,xy=sd.end+[0,-0])
                                    dot_end=d.add(e.DOT, xy=sf.start + [5, 0])
                                    # dots_diagram(dot_end,d)
                                    # d.add(e.LINE, d='left', xy=dot_s.end , l=1.8)
                                    # joint = d.add(e.LINE, d='down', l=1)
                                    # circle = d.add(e.SOURCE_Testing100, xy=joint.end + [3, 1.2])
                                    # line = d.add(e.LINE, d='left', xy=circle.end + [-3.2, -0.8], l=0.8)
                                    # d.add(e.VSS, d='left', xy=circle.end + [-3.5, -0.8], l=0.5)
                                    # d.add(e.GND, xy=circle.end + [-3, -0.8])
                                    # d.add(e.LABEL, xy=circle.end + [-2.5, 0.3],
                                    #       toplabel='#6mm² PVC 70º' + '\n' + ' 750V')

                                # else:
                                #     d.add(e.DOT, xy=sf.start + [2.5, 0])
                                if count_parallel < 3 and frequency[data] > 3:
                                    ENDPV = d.add(e.LINE, d='right', xy=sd.end+[0,-0], l=5)
                                    end_stand.append(ENDPV)
                                    # x = d.add(e.LINE)
                                    global dots
                                    dots_end=d.add(e.DOT, xy=sf.start + [5, 0])
                                    # dots_diagram(dots_end, d)
                                    # d.push()
                                else:
                                    if count_parallel != 3 or frequency[data] <= 3 or (
                                            count_parallel != 3 and frequency[data] > 3):
                                        dots_end=d.add(e.DOT, xy=sf.start + [5, 0])
                                        # dots_diagram(dots_end, d)

                                # d.add(e.LINE, xy=sf.start)
                                # d.add(e.LINE)
                            else:
                                # sf = d.add(e.PV, d='down', xy=sf.start + [2,0],label='PV' + str(frequency[data]) + '-' + '1')
                                # dot = d.add(e.PH)
                                # sd = d.add(e.PV, xy=sd.start+[2,0],label='PV' + str(frequency[data]) + '-' + str(data))
                                # ENDPV = d.add(e.LINE, d='right')
                                # d.add(e.SEP)
                                # d.add(e.LINE,xy=sf.start)
                                # d.add(e.SEP)
                                ENDPV = d.add(e.DOT,xy=sd.end+[0,-0])
                                end_stand.append(ENDPV)
                                # dots_diagram(ENDPV, d)
                        end_two[data] = ENDPV
                        if num != data and (count_parallel != len(data_raw) or len(data_raw)) == 1:
                            end_point.append(ENDPV)
                            end_points[data] = ENDPV
                            num = data
                            # temp2 += 1
                        if pv_number==sum_count_list[temp2]:
                            temp2 += 1
                            temp7 += 1
                        count_parallel += 1
                    ir = 0
                    siNb = iNb + 1

            # adding the Combiner Box, the Inverters and the Batteries:
            if MPPTin < Mp:  # if Inv inputs < than number of parallels --> add CB
                # import pdb
                # pdb.set_trace();
                if Bat == 2:  # if battery per Building Block do this
                    CBout = 1
                    d.push()
                    count = 0
                    diff = maximum - 150
                    if diff % 60 == 0:
                        diff = int(diff / 60)
                    else:
                        diff = int(diff / 60) + 1
                    if maximum <= 20:
                        n = 8.5
                        n_length = 23.2
                        m_length = 22.25
                        length = 25
                        # l=30
                        ms = 9
                    elif maximum > 20 and maximum <= 60:
                        n = 12
                        n_length = 30.2
                        m_length = 29.2
                        length = 32
                        # l=46
                        ms = 10
                    elif maximum > 60 and maximum <= 150:
                        n = 28
                        n_length = 62.2
                        m_length = 61.3
                        length = 63.5
                        # l=69
                        ms = 20
                    else:
                        n = (diff * 12.5) + 28
                        n_length = (25 * diff) + 62.2
                        m_length = (25 * diff) + 61.3
                        length = (diff * 22.9) + 63.5
                        ms = (5 * diff) + 20
                    if MPPTin <= 20:
                        l = 30
                        BB_x = 5
                        xx_y = 7.5
                    elif MPPTin > 20 and MPPTin <= 60:
                        l = 48
                        BB_x = 7
                        xx_y = 12
                    elif MPPTin > 60 and MPPTin <= 150:
                        l = 120
                        BB_x = 20.5
                        xx_y = 30
                    else:
                        l = (diff * 50) + 120
                        BB_x = (diff * 13.5) + 20.5
                        xx_y = (diff * 13.5) + 29
                    for line in end_point:
                        count += 1
                        first_key = end_point[0]
                        last_key = end_point[len(end_point) - 1]
                        if count == len(end_point) and len(data_raw) == 4 and len(end_point) == 2:
                            d.add(e.LABEL, color='red', xy=line.start + [5.8, -1], titlabel='NOTA:')
                            d.add(e.LABEL, color='red', xy=line.start + [12.6, -2.5],
                                  titlabel=" 1 - O terra do sistema fotovoltaico sera conectado ao do terra ")
                            d.add(e.LABEL, color='red', xy=line.start + [7.3, -3.5],
                                  titlabel="padrão de entrada.")
                        if len(data_raw) > 2 and len(data_raw)!=4and len(end_point) == 2:
                            d_joints = draw(first_key, line, last_key,length,len(data_raw), d)
                            ends = d.add(e.LINE, d='down', xy=line.start, l=8)
                            # if count % 2 == 0:
                            #     d.add(e.LINE, d='left', xy=ends.end, l=l1)
                            #     x = d.add(e.LINE, d='down', l=3 * l1)
                            # else:
                            #     if len(end_point) > 1:
                            #         d.add(e.LINE, d='right', xy=ends.end, l=l1)
                            #         d.add(e.LINE, d='down',theta=-90, l=12.6)
                            #     else:
                            #         d.add(e.LINE, d='right', xy=ends.end, l=l1)
                            #         d.add(e.LINE, d='down', l=3 * l1)
                        if len(data_raw) == 4 and len(end_point) == 2:
                            first_key = end_point[0]
                            last_key = end_point[len(end_point) - 1]
                            d_joints = draw_two(first_key, line, last_key,length,count, d)
                            d_end_label = d.add(e.LABEL, color='green',
                                                xy=last_key.start - [-4.2, 12.8],
                                                smlltlabel="??????")
                            d_end = d.add(e.LABEL, xy=d_end_label.end - [0, -0.3],
                                          smlltlabel="STRING BOX")
                            outline_diagram(line, count, end_point, d)
                            ends = d.add(e.LINE, d='down', xy=line.start, l=1)
                            if count % 2 == 0:
                                d.add(e.LINE, d='right', xy=ends.end, l=1)
                                x = d.add(e.LINE, d='down', l=3* l1)
                                # x = d.add(e.LINE, d='right',xy=x.end, l=0.9 * l1)

                            else:
                                if len(end_point) > 1:
                                    xy = d.add(e.LINE, d='right', xy=ends.end, l=1)
                                    xx = d.add(e.LINE, d='down', xy=xy.end, toy=d_joints['d_joints'].end, l=12)
                                    ds = d.add(e.dashed_line, d='down', xy=xx.end, l=0.75)
                                    x = d.add(e.LINE, d='down', xy=ds.end + [0, -2.89], l=(n_length-15.15))
                                    x_right = d.add(e.LINE, d='right', xy=x.end, tox=end_point[1].start - [1, 0],
                                                    l=1.8 * l1)
                                    d.add(e.LINE, d='down', xy=x_right.end, l=0.23* l1)
                                else:
                                    d.add(e.LINE, d='right', xy=ends.end, l=l1)
                                    d.add(e.LINE, d='down', l=3 * l1)

                        if len(data_raw) > 2 and len(end_point) > 2 and (count == len(end_point)):
                            first_key = end_point[0]
                            last_key = end_point[len(end_point) - 1]
                            d_joints = draw_two(first_key, line, last_key,length,count,len(data_raw), d)
                            ends = d.add(e.LINE, d='down', xy=line.start, l=8)
                        if len(data_raw) >= 3 and len(end_point) ==1:
                            first_key = end_point[0]
                            last_key = end_point[len(end_point) - 1]
                            if count == len(end_point):
                                d.add(e.LABEL, color='red', xy=line.start + [5.8, -1], titlabel='NOTA:')
                                d.add(e.LABEL, color='red', xy=line.start + [5.6, -2.5],
                                      titlabel=" 1 - O terra do sistema fotovoltaico sera conectado ao do terra ")
                                d.add(e.LABEL, color='red', xy=line.start + [5.9, -3.5],
                                      titlabel="padrão de entrada.")
                            d_joints = draw(first_key, line, last_key,length,count,len(data_raw), d)
                            d_end_label = d.add(e.LABEL, color='green',
                                                xy=last_key.start - [-5, 13],
                                                smlltlabel="??????")
                            d_end = d.add(e.LABEL, xy=d_end_label.end - [0, -0.3],
                                          smlltlabel="STRING BOX")
                            ends = d.add(e.LINE, d='down', xy=line.start, l=8)
                    counts=0
                    for line in end_two:
                        count += 1
                        counts += 1
                        first_key = list(end_two.values())[0]
                        last_key = list(end_two.values())[len(end_two) - 1]
                        if len(data_raw) == 2 and len(end_two) == 1:
                            if count == 2:
                                d.add(e.LABEL, color='red', xy=end_two[line].start + [5.2, -1], titlabel='NOTA:')
                                d.add(e.LABEL, color='red', xy=end_two[line].start + [5, -2.5],
                                      titlabel=" 1 - O terra do sistema fotovoltaico sera conectado ao do terra ")
                                d.add(e.LABEL, color='red', xy=end_two[line].start + [5.2, -3.5],
                                      titlabel="padrão de entrada.")
                            d_end_label = d.add(e.LABEL, color='green',
                                                xy=last_key.start - [-6.9, 12.8],
                                                smlltlabel="??????")
                            d_end = d.add(e.LABEL, xy=d_end_label.end - [0, -0.3],
                                          smlltlabel="STRING BOX")
                            d_joints = draw(first_key, end_two[line], last_key,length,counts,len(data_raw), d)
                        if len(data_raw) == 2 and len(end_two) == 2:
                            d.add(e.LABEL, color='red', xy=end_two[line].start + [10.2, -1], titlabel='NOTA:')
                            d.add(e.LABEL, color='red', xy=end_two[line].start + [10, -2.5],
                                  titlabel=" 1 - O terra do sistema fotovoltaico sera conectado ao do terra ")
                            d.add(e.LABEL, color='red', xy=end_two[line].start + [10.2, -3.5],
                                  titlabel="padrão de entrada.")
                            d_joints = draw_two(first_key, end_two[line], last_key,length,counts, d)
                            d_end_label = d.add(e.LABEL, color='green',
                                                xy=last_key.start - [-4.3, 12.9],
                                                smlltlabel="??????")
                            d_end = d.add(e.LABEL, xy=d_end_label.end - [0, -0.3],
                                          smlltlabel="STRING BOX")
                            outline_diagram(end_two[line], counts, end_two, d)
                        if len(data_raw) == 1:
                            if count == 2:
                                d.add(e.LABEL, color='red', xy=end_two[line].start + [1.2, -3], titlabel='NOTA:')
                                d.add(e.LABEL, color='red', xy=end_two[line].start + [1, -4.5],
                                      titlabel=" 1 - O terra do sistema fotovoltaico sera conectado ao do terra ")
                                d.add(e.LABEL, color='red', xy=end_two[line].start + [1.2, -5.5],
                                      titlabel="padrão de entrada.")
                            d_joints = draw(first_key, end_two[line], last_key,length,counts,len(data_raw), d)
                            d_end_label = d.add(e.LABEL, color='green',
                                                xy=last_key.start - [-5, 12.9],
                                                smlltlabel="??????")
                            d_end = d.add(e.LABEL, xy=d_end_label.end - [0, -0.3],
                                          smlltlabel="STRING BOX")
                        if len(data_raw) == 2 and len(end_two) != 1:
                            ends = d.add(e.LINE, d='down', xy=end_two[line].start, l=l1)
                            if count % 2 == 0:
                                xy = d.add(e.LINE, d='right', xy=ends.end, l=1)
                                xx = d.add(e.LINE, d='down', xy=xy.end, toy=d_joints['d_joints'].end, l=12)
                                ds = d.add(e.dashed_line, d='down', xy=xx.end, l=0.65)
                                x = d.add(e.LINE, d='down', xy=ds.end + [0, -2.75], l=(m_length - 13.55))
                            else:
                                d.add(e.LINE, d='right', xy=ends.end, l=1)
                                d.add(e.LINE, d='down', l=2.9 * l1)
                        if len(data_raw) == 2 and len(end_two) == 1:
                            ends = d.add(e.LINE, d='down', xy=end_two[line].start, l=8)
                        if len(data_raw) == 1:
                            ends = d.add(e.LINE, d='down', xy=end_two[line].start, l=4 * l1)

                    d.push()
                    # d_right =d.add(e.LINE, d='right', l=2 * l1)
                    # d.add(e.CONVDCDC, d='down',
                    #       smllblabel='In ' + str(VL1) + ' V / ' + str(AL2) + ' A',
                    #       smlrblabel='Out ' + str(VL3) + ' V / ' + str(AL3) + ' A',
                    #       toplabel='DCDC Converter')
                    # d.add(e.BATTERY, d='down', toplabel='Battery ' + str(siNb))
                    d.pop()
                    # inv_start =d.add(e.LINE, d='down', l=3* l1)
                    starts = d.add(e.LINE, d='down', l=4.3 * l1)
                    ds = d.add(e.dashed_line, d='down', xy=starts.end, l=0.565)
                    inv_start = d.add(e.LINE, d='down', xy=ds.end, l=(n - 4.58) * l1)
                    # CBout = 1
                    # d.add(e.CB, d='down', xy=ENDPV.start,
                    #       smlltlabel='In ' + str(VL1) + ' V / ' + str(AL1) + ' A',
                    #       smlrtlabel='Out ' + str(VL2) + ' V / ' + str(AL2) + ' A',
                    #       toplabel='Combiner Box ' + str(siNb)
                    #                + '\nInputs: ' + str(Mp)
                    #                + '\nOutputs: ' + str(CBout))
                    # d.push()
                    # d.add(e.LINE, d='left', l=l1)
                    # d.add(e.CONVDCDC, d='down',
                    #       smllblabel='In ' + str(VL1) + ' V / ' + str(AL2) + ' A',
                    #       smlrblabel='Out ' + str(VL3) + ' V / ' + str(AL3) + ' A',
                    #       toplabel='DCDC Converter')
                    # d.add(e.BATTERY, d='down', toplabel='Battery ' + str(siNb))
                    # d.pop()
                    # d.add(e.LINE, d='down', l=2 * l1)

                else:
                    # import pdb
                    # pdb.set_trace();
                    CBout = 1
                    d.push()
                    count = 0
                    diff = maximum - 150
                    if diff % 60 == 0:
                        diff = int(diff / 60)
                    else:
                        diff = int(diff / 60) + 1
                    if maximum <= 20:
                        n = 8.5
                        n_length = 23.2
                        m_length = 22.25
                        length = 25
                        # l=30
                        ms = 13
                    elif maximum > 20 and maximum <= 60:
                        n = 12
                        n_length = 30.2
                        m_length = 29.2
                        length = 32
                        # l=46
                        ms = 10.9
                    elif maximum > 60 and maximum <= 150:
                        n = 28
                        n_length =62.2
                        m_length = 61.3
                        length = 63.5
                        # l=69
                        ms = 20
                    else:
                        n = (diff * 12.5) + 28
                        n_length = (25 * diff) + 62.2
                        m_length = (25 * diff) + 61.3
                        length=(diff*22.9)+63.5
                        ms = (5 * diff) + 20
                    if MPPTin <= 20:
                        l = 30
                        BB_x = 5
                        xx_y = 7.5
                    elif MPPTin > 20 and MPPTin <= 60:
                        l = 48
                        BB_x = 7
                        xx_y=12
                    elif MPPTin > 60 and MPPTin <= 150:
                        l = 120
                        BB_x = 20.5
                        xx_y=30
                    else:
                        l = (diff * 50) + 120
                        BB_x = (diff * 13.5) + 20.5
                        xx_y = (diff * 13.5) + 29

                    for line in end_point:
                        count += 1
                        first_key = end_point[0]
                        last_key = end_point[len(end_point) - 1]
                        if count == len(end_point) and len(data_raw) == 4 and len(end_point)==2:
                            d.add(e.LABEL, color='red', xy=line.start + [5.8, -1], titlabel='NOTA:')
                            d.add(e.LABEL, color='red', xy=line.start + [12.6, -2.5],
                                  titlabel=" 1 - O terra do sistema fotovoltaico sera conectado ao do terra ")
                            d.add(e.LABEL, color='red', xy=line.start + [7.3, -3.5],
                                  titlabel="padrão de entrada.")
                        if len(data_raw) > 2  and len(data_raw)!=4and len(end_point) == 2:
                            d_joints = draw(first_key, line, last_key,MPPTin,length,count,len(data_raw), d)
                            ends = d.add(e.LINE, d='down', xy=line.start, l=8)
                            # if count % 2 == 0:
                            #         d.add(e.LINE, d='left', xy=ends.end, l=l1)
                            #         x = d.add(e.LINE, d='down', l=3 * l1)
                            # else:
                            #     if len(end_point) > 1:
                            #             d.add(e.LINE, d='right', xy=ends.end, l=1.5)
                            #             d.add(e.LINE, d='down', theta=-90, l=18.6)
                            #     else:
                            #         d.add(e.LINE, d='right', xy=ends.end, l=l1)
                            #         d.add(e.LINE, d='down', l=3 * l1)
                        if len(data_raw) == 4 and len(end_point) == 2:
                            first_key = end_point[0]
                            last_key = end_point[len(end_point) - 1]
                            d_joints = draw_two(first_key, line, last_key,length,count, d)
                            d_end_label = d.add(e.LABEL, color='green',
                                                xy=last_key.start - [-4.2, 12.8],
                                                smlltlabel="??????")
                            d_end = d.add(e.LABEL, xy=d_end_label.end - [0, -0.3],
                                          smlltlabel="STRING BOX")
                            outline_diagram(line,count,end_point,d)
                            ends = d.add(e.LINE, d='down', xy=line.start, l=l1)
                            if count % 2 == 0:
                                d.add(e.LINE, d='right', xy=ends.end, l=1)
                                x = d.add(e.LINE, d='down', l=3* l1)
                                # x = d.add(e.LINE, d='right',xy=x.end, l=0.9 * l1)

                            else:
                                if len(end_point) > 1:
                                    xy=d.add(e.LINE, d='right', xy=ends.end,l=1)
                                    xx=d.add(e.LINE, d='down', xy=xy.end, toy=d_joints['d_joints'].end, l=12)
                                    ds = d.add(e.dashed_line, d='down', xy=xx.end, l=0.75)
                                    x=d.add(e.LINE, d='down', xy=ds.end+[0,-2.89], l=(n_length-15.15))
                                    x_right=d.add(e.LINE, d='right', xy=x.end,tox=end_point[1].start-[1,0] , l=1.8*l1)
                                    d.add(e.LINE, d='down', xy=x_right.end, l=0.23 * l1)
                                else:
                                    d.add(e.LINE, d='right', xy=ends.end, l=l1)
                                    d.add(e.LINE, d='down', l=3 * l1)

                        if len(data_raw) > 2 and len(end_point) > 2 and (count == len(end_point)):
                            first_key = end_point[0]
                            last_key = end_point[len(end_point) - 1]
                            d_joints = draw(first_key, line, last_key,MPPTin,length,count,len(data_raw),d)
                            ends = d.add(e.LINE, d='down', xy=line.start, l=8)
                        if len(data_raw) >= 3 and len(end_point) ==1:
                            if count == len(end_point):
                                d.add(e.LABEL, color='red', xy=line.start + [5.8, -1], titlabel='NOTA:')
                                d.add(e.LABEL, color='red', xy=line.start + [5.6, -2.5],
                                      titlabel=" 1 - O terra do sistema fotovoltaico sera conectado ao do terra ")
                                d.add(e.LABEL, color='red', xy=line.start + [5.9, -3.5],
                                      titlabel="padrão de entrada.")
                            first_key = end_point[0]
                            last_key = end_point[len(end_point) - 1]
                            d_joints = draw(first_key, line, last_key,MPPTin,length,count,len(data_raw), d)
                            d_end_label = d.add(e.LABEL, color='green',
                                                xy=last_key.start - [-5, 13],
                                                smlltlabel="??????")
                            d_end = d.add(e.LABEL, xy=d_end_label.end - [0, -0.3],
                                          smlltlabel="STRING BOX")
                            ends = d.add(e.LINE, d='down', xy=line.start, l=8)
                    counts = 0
                    for line in end_two:
                        count += 1
                        counts += 1
                        first_key = list(end_two.values())[0]
                        last_key = list(end_two.values())[len(end_two) - 1]
                        if len(data_raw) == 2 and len(end_two)==1:
                            if count == 2:
                                d.add(e.LABEL, color='red', xy=end_two[line].start + [5.2, -1], titlabel='NOTA:')
                                d.add(e.LABEL, color='red', xy=end_two[line].start + [5, -2.5],
                                      titlabel=" 1 - O terra do sistema fotovoltaico sera conectado ao do terra ")
                                d.add(e.LABEL, color='red', xy=end_two[line].start + [5.2, -3.5],
                                      titlabel="padrão de entrada.")
                            d_end_label = d.add(e.LABEL, color='green',
                                                xy=last_key.start - [-6.9, 12.8],
                                                smlltlabel="??????")
                            d_end = d.add(e.LABEL, xy=d_end_label.end - [0, -0.3],
                                          smlltlabel="STRING BOX")
                            d_joints = draw(first_key, end_two[line], last_key,MPPTin,length,counts,len(data_raw), d)
                        if len(data_raw)==2 and len(end_two)==2:
                            if count == 2:
                                d.add(e.LABEL, color='red', xy=end_two[line].start + [10.2, -1], titlabel='NOTA:')
                                d.add(e.LABEL, color='red', xy=end_two[line].start + [10, -2.5],
                                      titlabel=" 1 - O terra do sistema fotovoltaico sera conectado ao do terra ")
                                d.add(e.LABEL, color='red', xy=end_two[line].start + [10.2, -3.5],
                                      titlabel="padrão de entrada.")
                            d_joints = draw_two(first_key, end_two[line], last_key,length,counts, d)
                            d_end_label = d.add(e.LABEL, color='green',
                                                xy=last_key.start - [-4.3, 12.9],
                                                smlltlabel="??????")
                            d_end = d.add(e.LABEL, xy=d_end_label.end - [0, -0.3],
                                          smlltlabel="STRING BOX")
                            outline_diagram(end_two[line], counts, end_two, d)
                        if len(data_raw) == 1:
                            if count == 2:
                                d.add(e.LABEL, color='red', xy=end_two[line].start + [1.2, -3], titlabel='NOTA:')
                                d.add(e.LABEL, color='red', xy=end_two[line].start + [1, -4.5],
                                      titlabel=" 1 - O terra do sistema fotovoltaico sera conectado ao do terra ")
                                d.add(e.LABEL, color='red', xy=end_two[line].start + [1.2, -5.5],
                                      titlabel="padrão de entrada.")
                            d_joints = draw(first_key, end_two[line], last_key,MPPTin,length,counts,len(data_raw), d)
                            d_end_label = d.add(e.LABEL, color='green',
                                                xy=last_key.start - [-5, 12.9],
                                                smlltlabel="??????")
                            d_end = d.add(e.LABEL, xy=d_end_label.end - [0, -0.3],
                                          smlltlabel="STRING BOX")
                        if len(data_raw) == 2 and len(end_two)!=1:
                            ends = d.add(e.LINE, d='down', xy=end_two[line].start, l=l1)
                            if count % 2 == 0:
                                xy = d.add(e.LINE, d='right', xy=ends.end, l=1)
                                xx = d.add(e.LINE, d='down', xy=xy.end, toy=d_joints['d_joints'].end, l=12)
                                ds = d.add(e.dashed_line, d='down', xy=xx.end, l=0.65)
                                x = d.add(e.LINE, d='down', xy=ds.end + [0, -2.99], l=(m_length - 13.75))
                            else:
                                d.add(e.LINE, d='right', xy=ends.end, l=1)
                                d.add(e.LINE, d='down', l=3* l1)
                        if len(data_raw) ==2 and len(end_two)==1:
                            ends = d.add(e.LINE, d='down', xy=end_two[line].start, l=8)
                        if len(data_raw) == 1:
                            ends = d.add(e.LINE, d='down', xy=end_two[line].start, l=4 * l1)

                    d.push()
                    # d_right =d.add(e.LINE, d='right', l=2 * l1)
                    # d.add(e.CONVDCDC, d='down',
                    #       smllblabel='In ' + str(VL1) + ' V / ' + str(AL2) + ' A',
                    #       smlrblabel='Out ' + str(VL3) + ' V / ' + str(AL3) + ' A',
                    #       toplabel='DCDC Converter')
                    # d.add(e.BATTERY, d='down', toplabel='Battery ' + str(siNb))
                    d.pop()

                    # inv_start = d.add(e.LINE, d='down', l=n* l1)
                    starts = d.add(e.LINE, d='down', l=4.3*l1)
                    ds = d.add(e.dashed_line, d='down', xy=starts.end, l=0.565)
                    inv_start = d.add(e.LINE, d='down', xy=ds.end, l=(n-4.58)*l1)
                    # d.add(e.CB, d='down', xy=ENDPV.start,
                    #       smlltlabel='In ' + str(VL1) + ' V / ' + str(AL1) + ' A',
                    #       smlrtlabel='Out ' + str(VL2) + ' V / ' + str(AL2) + ' A',
                    #       toplabel='Combiner Box ' + str(siNb)
                    #                + '\nInputs: ' + str(Mp)
                    #                + '\nOutputs: ' + str(CBout))
                global inv;
                INVDCAC = e.update_inverter(l)
                inv= d.add(INVDCAC, d='down',
                      toplabel="Inverter FRONIUS PRIMO 6.0-1" + str(siNb) +
                               '\n'+MaxInputs+': ' + str(Nb_Mppt)
                               + '\n'+UsedInputs+': ' + str(MPPTin))
                d.add(e.LINE, d='right', xy=inv.end + [xx_y, 1.3], l=0.5 * l1)
                d_end = d.add(e.LINE, d='down', l=0.2)
                d.add(e.SOURCE_Testing99, d='down', xy=d_end.start, smlltlabel='#6mm² PVC 70º 750V')
                d.add(e.LABEL, xy=inv.end + [BB_x, 1.8], smlltlabel=In + str(VL2) + V + '/ ' + str(AL2) + A)
                d.add(e.LABEL, xy=inv.end + [BB_x, 0.5], smlrtlabel=Out + str(VL3) + V + '/ ' + str(AL3) + A)
                inv_list.append(inv)
                draw_diagram_brazilian(end_points,data_raw,frequency,counts,end_point,inv,inv_start,count_list,MPPTin,maximum,diff,total_mpptin,total_dict_length,Mp,d)

            else:
                # import pdb
                # pdb.set_trace();
                if Bat == 2:  # if battery per BB do this
                    CBout = 1
                    count = 0
                    diff = maximum - 150
                    if diff % 60 == 0:
                        diff = int(diff / 60)
                    else:
                        diff = int(diff / 60) + 1
                    if maximum <= 20:
                        n = 8.5
                        n_length = 23.2
                        m_length = 22.25
                        length = 25
                        # l=30
                        ms = 9
                    elif maximum > 20 and maximum <= 60:
                        n = 12
                        n_length = 30.2
                        m_length = 29.2
                        length = 32
                        # l=46
                        ms = 10
                    elif maximum > 60 and maximum <= 150:
                        n = 28
                        n_length = 62.2
                        m_length = 61.3
                        length = 63.5
                        # l=69
                        ms = 20
                    else:
                        n = (diff * 12.5) + 28
                        n_length = (25 * diff) + 62.2
                        m_length = (25 * diff) + 61.3
                        length = (diff * 22.9) + 63.5
                        ms = (5 * diff) + 20
                    if MPPTin <= 20:
                        l = 30
                        BB_x = 5
                        xx_y = 7.5
                    elif MPPTin > 20 and MPPTin <= 60:
                        l = 48
                        BB_x = 7
                        xx_y = 12
                    elif MPPTin > 60 and MPPTin <= 150:
                        l = 120
                        BB_x = 20.5
                        xx_y = 30
                    else:
                        l = (diff * 50) + 120
                        BB_x = (diff * 13.5) + 20.5
                        xx_y = (diff * 13.5) + 29
                    for line in end_point:
                        count += 1
                        first_key = list(end_points.values())[0]
                        last_key = list(end_points.values())[len(end_points) - 1]
                        if count == len(end_point) and len(data_raw) == 4 and len(end_point) == 2:
                            d.add(e.LABEL, color='red', xy=line.start + [5.8, -1], titlabel='NOTA:')
                            d.add(e.LABEL, color='red', xy=line.start + [12.6, -2.5],
                                  titlabel=" 1 - O terra do sistema fotovoltaico sera conectado ao do terra ")
                            d.add(e.LABEL, color='red', xy=line.start + [7.3, -3.5],
                                  titlabel="padrão de entrada.")
                        if len(data_raw) > 2 and len(data_raw)!=4 and len(end_point) == 2:
                            d_joints = draw(first_key, line, last_key,MPPTin,length,count,len(data_raw), d)
                            ends = d.add(e.LINE, d='down', xy=line.start, l=8)
                        if len(data_raw) == 4 and len(end_point) == 2:
                            first_key = end_point[0]
                            last_key = end_point[len(end_point) - 1]
                            d_joints = draw_two(first_key, line, last_key,length,count, d)
                            d_end_label = d.add(e.LABEL, color='green',
                                                xy=last_key.start - [-4.2, 12.8],
                                                smlltlabel="??????")
                            d_end = d.add(e.LABEL, xy=d_end_label.end - [0, -0.3],
                                          smlltlabel="STRING BOX")
                            outline_diagram(line, count, end_point, d)
                            ends = d.add(e.LINE, d='down', xy=line.start, l=l1)
                            if count % 2 == 0:
                                d.add(e.LINE, d='right', xy=ends.end, l=1)
                                x = d.add(e.LINE, d='down', l=3* l1)
                                # x = d.add(e.LINE, d='right',xy=x.end, l=0.9 * l1)

                            else:
                                if len(end_point) > 1:
                                    xy = d.add(e.LINE, d='right', xy=ends.end, l=1)
                                    xx = d.add(e.LINE, d='down', xy=xy.end, toy=d_joints['d_joints'].end, l=12)
                                    ds = d.add(e.dashed_line, d='down', xy=xx.end, l=0.75)
                                    x = d.add(e.LINE, d='down', xy=ds.end + [0, -2.89], l=(n_length - 15.15))
                                    x_right = d.add(e.LINE, d='right', xy=x.end, tox=end_point[1].start - [1, 0],
                                                    l=1.8 * l1)
                                    d.add(e.LINE, d='down', xy=x_right.end, l=0.23* l1)
                                else:
                                    d.add(e.LINE, d='right', xy=ends.end, l=l1)
                                    d.add(e.LINE, d='down', l=3 * l1)

                        if len(data_raw) > 2 and len(end_point) > 2 and (count == len(end_point)):
                            first_key = end_point[0]
                            last_key = end_point[len(end_point) - 1]
                            d_joints = draw(first_key, line, last_key,MPPTin,length,count,len(data_raw), d)
                            ends = d.add(e.LINE, d='down', xy=line.start, l=8)
                        if len(data_raw) >= 3 and len(end_point) ==1 :
                            first_key = end_point[0]
                            last_key = end_point[len(end_point) - 1]
                            if count == len(end_point):
                                d.add(e.LABEL, color='red', xy=line.start + [5.8, -1], titlabel='NOTA:')
                                d.add(e.LABEL, color='red', xy=line.start + [5.6, -2.5],
                                      titlabel=" 1 - O terra do sistema fotovoltaico sera conectado ao do terra ")
                                d.add(e.LABEL, color='red', xy=line.start + [5.9, -3.5],
                                      titlabel="padrão de entrada.")
                            d_joints = draw(first_key, line,MPPTin,length,count,len(data_raw), d)
                            d_end_label = d.add(e.LABEL, color='green',
                                                xy=last_key.start - [-5, 13],
                                                smlltlabel="??????")
                            d_end = d.add(e.LABEL, xy=d_end_label.end - [0, -0.3],
                                          smlltlabel="STRING BOX")
                            ends = d.add(e.LINE, d='down', xy=line.start, l=8)
                    counts=0
                    for line in end_two:
                        count += 1
                        counts += 1
                        first_key = list(end_two.values())[0]
                        last_key = list(end_two.values())[len(end_two) - 1]
                        if len(data_raw) == 2 and len(end_two) == 1:
                            if count == 2:
                                d.add(e.LABEL, color='red', xy=end_two[line].start + [5.2, -1], titlabel='NOTA:')
                                d.add(e.LABEL, color='red', xy=end_two[line].start + [5, -2.5],
                                      titlabel=" 1 - O terra do sistema fotovoltaico sera conectado ao do terra ")
                                d.add(e.LABEL, color='red', xy=end_two[line].start + [5.2, -3.5],
                                      titlabel="padrão de entrada.")
                            d_end_label = d.add(e.LABEL, color='green',
                                                xy=last_key.start - [-6.9, 12.8],
                                                smlltlabel="??????")
                            d_end = d.add(e.LABEL, xy=d_end_label.end - [0, -0.3],
                                          smlltlabel="STRING BOX")
                            d_joints = draw(first_key, end_two[line], last_key,MPPTin,length,counts,d)
                        if len(data_raw) == 2 and len(end_two) == 2:
                            if count == 2:
                                d.add(e.LABEL, color='red', xy=end_two[line].start + [10.2, -1], titlabel='NOTA:')
                                d.add(e.LABEL, color='red', xy=end_two[line].start + [10, -2.5],
                                      titlabel=" 1 - O terra do sistema fotovoltaico sera conectado ao do terra ")
                                d.add(e.LABEL, color='red', xy=end_two[line].start + [10.2, -3.5],
                                      titlabel="padrão de entrada.")
                            d_joints = draw_two(first_key, end_two[line], last_key,length,counts, d)
                            d_end_label = d.add(e.LABEL, color='green',
                                                xy=last_key.start - [-4.3, 12.9],
                                                smlltlabel="??????")
                            d_end = d.add(e.LABEL, xy=d_end_label.end - [0, -0.3],
                                          smlltlabel="STRING BOX")
                            outline_diagram(end_two[line], counts, end_two, d)
                        if len(data_raw) == 1:
                            if count == 2:
                                d.add(e.LABEL, color='red', xy=end_two[line].start + [1.2, -3], titlabel='NOTA:')
                                d.add(e.LABEL, color='red', xy=end_two[line].start + [1, -4.5],
                                      titlabel=" 1 - O terra do sistema fotovoltaico sera conectado ao do terra ")
                                d.add(e.LABEL, color='red', xy=end_two[line].start + [1.2, -5.5],
                                      titlabel="padrão de entrada.")
                            d_joints = draw(first_key, end_two[line], last_key,MPPTin,length,counts,len(data_raw), d)
                            d_end_label = d.add(e.LABEL, color='green',
                                                xy=last_key.start - [-5, 12.9],
                                                smlltlabel="??????")
                            d_end = d.add(e.LABEL, xy=d_end_label.end - [0, -0.3],
                                          smlltlabel="STRING BOX")
                        if len(data_raw) == 2 and len(end_two) != 1:
                            ends = d.add(e.LINE, d='down', xy=end_two[line].start, l=l1)
                            if count % 2 == 0:
                                xy = d.add(e.LINE, d='right', xy=ends.end, l=1)
                                xx = d.add(e.LINE, d='down', xy=xy.end, toy=d_joints['d_joints'].end, l=12)
                                ds = d.add(e.dashed_line, d='down', xy=xx.end, l=0.65)
                                x = d.add(e.LINE, d='down', xy=ds.end + [0, -2.99], l=(m_length - 13.75))
                            else:
                                d.add(e.LINE, d='right', xy=ends.end, l=1)
                                d.add(e.LINE, d='down', l=3 * l1)
                        if len(data_raw) == 2 and len(end_two) == 1:
                            ends = d.add(e.LINE, d='down', xy=end_two[line].start, l=8)
                        if len(data_raw) == 1:
                            ends = d.add(e.LINE, d='down', xy=end_two[line].start, l=4 * l1)

                    d.push()
                    # d.add(e.LINE, d='right', l=2 * l1)
                    # d.add(e.CONVDCDC, d='down',
                    #       smllblabel='In ' + str(VL1) + ' V / ' + str(AL2) + ' A',
                    #       smlrblabel='Out ' + str(VL3) + ' V / ' + str(AL3) + ' A',
                    #       toplabel='DCDC Converter')
                    # d.add(e.BATTERY, d='down', toplabel='Battery ' + str(siNb))
                    d.pop()
                    # inv_start = d.add(e.LINE, d='down', l=6 * l1)
                    starts = d.add(e.LINE, d='down', l=4.3 * l1)
                    ds = d.add(e.dashed_line, d='down', xy=starts.end, l=0.565)
                    inv_start = d.add(e.LINE, d='down', xy=ds.end, l=(n - 4.58) * l1)
                    # global inv;
                    INVDCAC = e.update_inverter(l)
                    inv = d.add(INVDCAC,
                          toplabel="Inverter FRONIUS PRIMO 6.0-1"+ str(siNb) +
                                   '\n'+MaxInputs+': ' + str(Nb_Mppt)
                                   + '\n'+UsedInputs+': ' + str(MPPTin))
                    inv_list.append(inv)
                    draw_diagram_brazilian(end_points, data_raw, frequency, counts, end_point, inv, inv_start, count_list, MPPTin,total_mpptin,total_dict_length,Mp,
                                 d)

                else:  # if NO battery  do this
                    # import pdb
                    # pdb.set_trace();
                    CBout = 1
                    count = 0
                    diff = maximum - 150
                    if diff % 60 == 0:
                        diff = int(diff / 60)
                    else:
                        diff = int(diff / 60) + 1
                    if maximum <= 20:
                        n = 8.5
                        n_length = 23.2
                        m_length = 22.25
                        length = 25
                        # l=30
                        ms = 13
                    elif maximum > 20 and maximum <= 60:
                        n = 12
                        n_length = 30.2
                        m_length =29.2
                        length = 32
                        # l=46
                        ms = 10.9
                    elif maximum > 60 and maximum <= 150:
                        n = 28
                        n_length = 62.2
                        m_length = 61.3
                        length = 63.5
                        # l=69
                        ms = 20
                    else:
                        n = (diff * 12.5) + 28
                        n_length = (25 * diff) + 62.2
                        m_length = (25 * diff) + 61.3
                        length = (diff * 22.9) + 63.5
                        ms = (5 * diff) + 20
                    if MPPTin <= 20:
                        l = 30
                        BB_x = 5
                        xx_y = 7.5
                    elif MPPTin > 20 and MPPTin <= 60:
                        l = 48
                        BB_x = 7
                        xx_y = 12
                    elif MPPTin > 60 and MPPTin <= 150:
                        l = 120
                        BB_x = 20.5
                        xx_y=30
                    else:
                        l = (diff * 50) +120
                        BB_x = (diff * 13.5) + 20.5
                        xx_y = (diff * 13.5) + 29

                    for line in end_point:
                        count += 1
                        first_key = end_point[0]
                        last_key = end_point[len(end_point) - 1]
                        if count == len(end_point) and len(data_raw) == 4 and len(end_point) == 2:
                            d.add(e.LABEL, color='red', xy=line.start + [5.8, -1], titlabel='NOTA:')
                            d.add(e.LABEL, color='red', xy=line.start + [12.6, -2.5],
                                  titlabel=" 1 - O terra do sistema fotovoltaico sera conectado ao do terra ")
                            d.add(e.LABEL, color='red', xy=line.start + [7.3, -3.5],
                                  titlabel="padrão de entrada.")
                        if len(data_raw) > 2 and len(data_raw) !=4 and len(end_point) == 2:
                            d_joints = draw(first_key, line, last_key,MPPTin,length,count,len(data_raw), d)
                            ends = d.add(e.LINE, d='down', xy=line.start, l=8)
                        if len(data_raw) == 4 and len(end_point) == 2:
                            first_key = end_point[0]
                            last_key = end_point[len(end_point) - 1]
                            d_joints = draw_two(first_key, line, last_key,length,count, d)
                            d_end_label = d.add(e.LABEL, color='green',
                                                xy=last_key.start - [-4.2, 12.8],
                                                smlltlabel="??????")
                            d_end = d.add(e.LABEL, xy=d_end_label.end - [0, -0.3],
                                          smlltlabel="STRING BOX")
                            outline_diagram(line,count,end_point, d)
                            ends = d.add(e.LINE, d='down', xy=line.start, l=l1)
                            if count % 2 == 0:
                                d.add(e.LINE, d='right', xy=ends.end, l=1)
                                x = d.add(e.LINE, d='down', l=3* l1)
                                # x = d.add(e.LINE, d='right',xy=x.end, l=0.9 * l1)

                            else:
                                if len(end_point) > 1:
                                    xy=d.add(e.LINE, d='right', xy=ends.end, l=1)
                                    xx = d.add(e.LINE, d='down', xy=xy.end, toy=d_joints['d_joints'].end, l=12)
                                    ds = d.add(e.dashed_line, d='down', xy=xx.end, l=0.75)
                                    x = d.add(e.LINE, d='down', xy=ds.end + [0, -2.89], l=(n_length-15.15))
                                    x_right = d.add(e.LINE, d='right', xy=x.end, tox=end_point[1].start - [1, 0],
                                                    l=1.8 * l1)
                                    d.add(e.LINE, d='down', xy=x_right.end, l=0.23 * l1)
                                else:
                                    d.add(e.LINE, d='right', xy=ends.end, l=l1)
                                    d.add(e.LINE, d='down', l=3* l1)

                        if len(data_raw) > 2 and len(end_point) > 2 and (count == len(end_point)):
                            first_key = end_point[0]
                            last_key = end_point[len(end_point) - 1]
                            d_joints = draw(first_key, line, last_key,MPPTin,length,count,len(data_raw), d)
                            ends = d.add(e.LINE, d='down', xy=line.start, l=8)
                        if len(data_raw) >= 3 and len(end_point) == 1:
                            first_key = end_point[0]
                            last_key = end_point[len(end_point) - 1]
                            if count == len(end_point):
                                d.add(e.LABEL, color='red', xy=line.start + [5.8, -1], titlabel='NOTA:')
                                d.add(e.LABEL, color='red', xy=line.start + [5.6, -2.5],
                                      titlabel=" 1 - O terra do sistema fotovoltaico sera conectado ao do terra ")
                                d.add(e.LABEL, color='red', xy=line.start + [5.9, -3.5],
                                      titlabel="padrão de entrada.")
                            d_joints = draw(first_key, line, last_key,MPPTin,length,count,len(data_raw), d)
                            d_end_label = d.add(e.LABEL, color='green',
                                                xy=last_key.start - [-5, 13],
                                                smlltlabel="??????")
                            d_end = d.add(e.LABEL, xy=d_end_label.end - [0, -0.3],
                                          smlltlabel="STRING BOX")
                            ends = d.add(e.LINE, d='down', xy=line.start, l=8)
                    counts = 0
                    for line in end_two:
                        counts += 1
                        count += 1
                        first_key = list(end_two.values())[0]
                        last_key = list(end_two.values())[len(end_two) - 1]
                        # if count == 2:
                        #     d.add(e.LABEL, color='red', xy= end_two[line].start+ [10.2, -1], titlabel='NOTA:')
                        #     d.add(e.LABEL, color='red', xy= end_two[line].start + [10, -2.5],
                        #           titlabel=" 1 - O terra do sistema fotovoltaico sera conectado ao do terra ")
                        #     d.add(e.LABEL, color='red', xy= end_two[line].start + [10.2, -3.5],
                        #           titlabel="padrão de entrada.")
                        if len(data_raw) == 2 and len(end_two) == 1:
                            if count ==2:
                                d.add(e.LABEL, color='red', xy=end_two[line].start + [5.2, -1], titlabel='NOTA:')
                                d.add(e.LABEL, color='red', xy=end_two[line].start + [5, -2.5],
                                      titlabel=" 1 - O terra do sistema fotovoltaico sera conectado ao do terra ")
                                d.add(e.LABEL, color='red', xy=end_two[line].start + [5.2, -3.5],
                                      titlabel="padrão de entrada.")
                            d_end_label = d.add(e.LABEL, color='green',
                                                xy=last_key.start - [-6.9,12.8],
                                                smlltlabel="??????")
                            d_end = d.add(e.LABEL, xy=d_end_label.end - [0, -0.3],
                                          smlltlabel="STRING BOX")
                            d_joints = draw(first_key, end_two[line], last_key,MPPTin,length,counts,len(data_raw), d)
                        if len(data_raw) == 2 and len(end_two) == 2:
                            if count == 2:
                                d.add(e.LABEL, color='red', xy=end_two[line].start + [10.2, -1], titlabel='NOTA:')
                                d.add(e.LABEL, color='red', xy=end_two[line].start + [10, -2.5],
                                      titlabel=" 1 - O terra do sistema fotovoltaico sera conectado ao do terra ")
                                d.add(e.LABEL, color='red', xy=end_two[line].start + [10.2, -3.5],
                                      titlabel="padrão de entrada.")
                            d_joints = draw_two(first_key, end_two[line], last_key,length,counts, d)
                            d_end_label = d.add(e.LABEL, color='green',
                                                xy=last_key.start - [-4.3, 12.9],
                                                smlltlabel="??????")
                            d_end = d.add(e.LABEL, xy=d_end_label.end - [0, -0.3],
                                          smlltlabel="STRING BOX")
                            outline_diagram(end_two[line], counts, end_two, d)
                        if len(data_raw)==1:
                            if count ==2 :
                                d.add(e.LABEL, color='red', xy=end_two[line].start + [1.2, -3], titlabel='NOTA:')
                                d.add(e.LABEL, color='red', xy=end_two[line].start + [1, -4.5],
                                      titlabel=" 1 - O terra do sistema fotovoltaico sera conectado ao do terra ")
                                d.add(e.LABEL, color='red', xy=end_two[line].start + [1.2, -5.5],
                                      titlabel="padrão de entrada.")
                            d_joints = draw(first_key, end_two[line], last_key,MPPTin,length,counts,len(data_raw), d)
                            d_end_label = d.add(e.LABEL, color='green',
                                                xy=last_key.start - [-5, 12.9],
                                                smlltlabel="??????")
                            d_end = d.add(e.LABEL, xy=d_end_label.end - [0, -0.3],
                                          smlltlabel="STRING BOX")
                        if len(data_raw) == 2 and len(end_two)!=1:
                            ends = d.add(e.LINE, d='down', xy=end_two[line].start, l=l1)
                            if count % 2 == 0:
                                xy = d.add(e.LINE, d='right', xy=ends.end, l=1)
                                xx = d.add(e.LINE, d='down', xy=xy.end, toy=d_joints['d_joints'].end, l=12)
                                ds = d.add(e.dashed_line, d='down', xy=xx.end, l=0.65)
                                x = d.add(e.LINE, d='down', xy=ds.end + [0, -2.99], l=(m_length - 13.75))
                            else:
                                d.add(e.LINE, d='right', xy=ends.end, l=1)
                                d.add(e.LINE, d='down', l=3 * l1)
                        if len(data_raw)==2 and len(end_two)==1:
                            ends = d.add(e.LINE, d='down', xy=end_two[line].start, l=8)
                        if len(data_raw) == 1:
                            ends = d.add(e.LINE, d='down', xy=end_two[line].start, l=4 * l1)


                    d.push()
                    # d.add(e.LINE, d='right', l=2 * l1)
                    # d.add(e.CONVDCDC, d='down',
                    #       smllblabel='In ' + str(VL1) + ' V / ' + str(AL2) + ' A',
                    #       smlrblabel='Out ' + str(VL3) + ' V / ' + str(AL3) + ' A',
                    #       toplabel='DCDC Converter')
                    # d.add(e.BATTERY, d='down', toplabel='Battery ' + str(siNb))
                    d.pop()
                    # inv_start = d.add(e.LINE, d='down', l=n * l1)
                    starts = d.add(e.LINE, d='down', l=4.3 * l1)
                    ds = d.add(e.dashed_line, d='down', xy=starts.end, l=0.565)
                    inv_start = d.add(e.LINE, d='down', xy=ds.end, l=(n - 4.58) * l1)
                    # global inv;
                    INVDCAC=e.update_inverter(l)
                    inv = d.add(INVDCAC,
                                toplabel="Inverter FRONIUS PRIMO 6.0-1"+ str(siNb) +
                                         '\n'+MaxInputs+': ' + str(Nb_Mppt)
                                         + '\n'+UsedInputs+': ' + str(MPPTin))
                    d.add(e.LINE, d='right', xy=inv.end + [xx_y, 1.3], l=0.5 * l1)
                    d_end = d.add(e.LINE, d='down', l=0.2)
                    d.add(e.SOURCE_Testing99, d='down', xy=d_end.start, smlltlabel='#6mm² PVC 70º 750V')
                    d.add(e.LABEL, xy=inv.end + [BB_x, 1.8], smlltlabel=In + str(VL2) + V + '/ ' + str(AL2) + A)
                    d.add(e.LABEL, xy=inv.end + [BB_x, 0.5], smlrtlabel=Out + str(VL3) + V + '/ ' + str(AL3) + A)
                    inv_list.append(inv)
                    draw_diagram_brazilian(end_points, data_raw, frequency, counts, end_point, inv, inv_start, count_list, MPPTin,maximum,
                                 diff,total_mpptin,total_dict_length,Mp,d)
            brazilian_version(inv_list,inv_count,d)

            iNb = iNb + 1

        # ----------------------------------------------------------------------------

        # Placement of the BB label on y-axis
        BBLabelY = 0
        BBLabelY = -8.5

        # Placement of the BB label on x-axis
        BBLabelX = -4.2

        # Calling the main function bb(), to create the Building Blocks:
        raw = 0
        no=0
        # import pdb
        # pdb.set_trace();
        if NumBB == 1:
            for key in res_data:
                no += 1
                # import pdb
                # pdb.set_trace();
                inv_count += 1
                raw += 1
                max_parallel = max(max_parallels)
                total_mpptin = sum(max_parallels)
                if raw == 1:
                    BBLabelX = -9
                    ENDBB = d.add(e.DOT,l=l1,xy=[-5,0])
                    bb(res_data[key][:-3], res_data[key][-3], res_data[key][-2],res_data[key][-1],max_parallel,total_mpptin,len(res_data))
                    BB1DOT = d.add(e.DOT)
                else:
                    ENDBB = d.add(e.DOT, xy=dots_list[-1].start + [l1*ms, 0], l=l1)
                    d.add(e.LABEL, xy=ENDBB.start + [BBLabelX, BBLabelY])
                    bb(res_data[key][:-3], res_data[key][-3], res_data[key][-2],res_data[key][-1],max_parallel,total_mpptin,len(res_data))
                    d.add(e.LINE, xy=inv.end, to=BB1DOT.start - [0, 0], l=l1)

        else:
            # import pdb;
            # pdb.set_trace()
            # dl=0
            # for key in res_data:
            #     dl += 1
            #     raw += 1
            #     no+= 1
            #     inv_count += 1
            #     max_parallel=max(max_parallels)
            #     total_mpptin=sum(max_parallels)
            if len(res_data) == len(values[0].keys()):
                for key in res_data:
                    raw += 1
                    no += 1
                    inv_count += 1
                    max_parallel = max(max_parallels)
                    total_mpptin = sum(max_parallels)
                    if raw == 1:
                        ENDBB = d.add(e.DOT, l=l1)
                        # d.add(e.DOT)
                        bb(res_data[key][:-3], res_data[key][-3], res_data[key][-2], res_data[key][-1], max_parallel,
                           total_mpptin, len(res_data))
                        BB1DOT = d.add(e.DOT)
                    else:
                        BBLabelX= -9
                        ENDBB = d.add(e.DOT, xy=dots_list[-1].start + [l1 * ms, 0],
                                      l=l1)
                        # d.add(e.DOT, xy=ENDBB.start)
                        bb(res_data[key][:-3], res_data[key][-3], res_data[key][-2], res_data[key][-1], max_parallel,
                           total_mpptin, len(res_data))
                        bs = d.add(e.LINE, xy=inv.end, to=inv_list[0].end, l=l1)
                        invs.append(bs)
        # # -----------------------------------------------------------------------------
        #
        # # The Combiner Box after the Building Blocks
        #
        # if NumBB > 1:
        #     d.add(e.CB, d='down',xy=inv_list[0].end,
        #           smlltlabel=In + str(VL3) + V+' / ' + str(AL3) + A,
        #           smlrtlabel=Out+ str(VL3) + ' V / ' + str(AL3 * NumBB) + A,
        #           toplabel=CombinerBoxAllBB
        #                    + '\n'+Inputs+': ' + str(NumBB)
        #                    + '\n'+Outputs+': 1')

        # -----------------------------------------------------------------------------

        # If the system uses only one Battery, it gets added here:

        # if Bat == 1:
        #     d.push()
        #     d.add(e.LINE, d='left', l=2*l1)
        #     d.add(e.INVACDC, d='down',
        #           smllblabel='In ' + str(VL3) + ' V / ' + str(AL3) + ' A',
        #           smlrblabel='Out ' + str(VL3) + ' V / ' + str(AL3) + ' A',
        #           toplabel='Inverter transformer')
        #     d.add(e.BATTERY, d='down', toplabel='Battery All BB')
        #     d.pop()
        #     d.add(e.LINE, d='down', l=1*l1)

        # -----------------------------------------------------------------------------

        # The connection to the electrical grid:
        # if NumBB == 1:
        #     Grid = d.add(e.GRID, d='down',xy=inv.end, toplabel=gridconnection)
        # else:
        #     Grid = d.add(e.GRID, d='down', toplabel=gridconnection)

        # -----------------------------------------------------------------------------

        # Finishing the drawing, saving it to a pdf and automatically open it.
    x_points = d.add(e.rectangle_bases100, d='down', xy=dots_list[-1].start+[12.5* l1 + 1, 0])
    # d.add(e.LINE, d='right', xy=dots_list[-1].start+[8 * l1 - 0.5, -7.5], l=l1,rgtlabel=cabletype+':\n' + str(Cable))
    d.add(e.KWxh_component, d='down', xy=dots_list[-1].start + [8* l1+2.5 , -0.9])
    d.add(e.LABEL, d='down', xy=dots_list[-1].start + [8 * l1+5.2, -1.5],smlltlabel="MEDIDOR DE ENERGIA"+"\n"+"PADRÃO CONCESSIONÁRIA LOCAL")
    d.add(e.Polar, d='right', xy=dots_list[-1].start + [8 * l1+2.3 , -4.37])
    d.add(e.LABEL, d='down', xy=dots_list[-1].start + [8 * l1+5.2, -4.2],smlltlabel="DISJUNTOR MONOPOLAR TERMOMAGNÉTICO"+"\n"+"PADRÃO IEC"+"\n"+"-CORRENTE"+"\n"+"-TENSÃO")
    d.add(e.Bi_Polar, d='right', xy=dots_list[-1].start + [8* l1+2.3  , -6.45])
    d.add(e.LABEL, d='down', xy=dots_list[-1].start + [8 * l1+5.2, -6.7],smlltlabel="DISJUNTOR BiPOLAR TERMOMAGNÉTICO"+"\n"+"PADRÃO IEC"+"\n"+"-CORRENTE"+"\n"+"-TENSÃO")
    d.add(e.Tri_Polar, d='right', xy=dots_list[-1].start + [8 * l1+2.3 , -8.55])
    d.add(e.LABEL, d='down', xy=dots_list[-1].start + [8 * l1+5.2, -8.7],smlltlabel="DISJUNTOR TriPOLAR TERMOMAGNÉTICO"+"\n"+"PADRÃO IEC"+"\n"+"-CORRENTE"+"\n"+"-TENSÃO")
    d.add(e.Line40, d='down',theta=60, xy=dots_list[-1].start + [8 * l1+2.3 , -10.8],l=0.5)
    d.add(e.LABEL, d='down', xy=dots_list[-1].start + [8 * l1+5.2, -11],smlltlabel="CHAVE SECCIONADORA SOB CARGA"+"\n"+"-TENSÃO"+"\n"+"-CORRENTE")
    d.add(e.RBOX_Testing56, d='down', xy=dots_list[-1].start + [8 * l1+2.3 , -12])
    d.add(e.LABEL, d='down', xy=dots_list[-1].start + [8 * l1+5.2, -13.3],smlltlabel="DISPOSITIVO DE PROTEÇÃO CONTRA-SURTO"+"\n"+"-MÁXIMA TENSÃO DE OPERAÇÃO"+"\n"+"-CORRENTE MÁXIMA")
    d.add(e.INVERSOR999, d='down', xy=dots_list[-1].start + [8 * l1+2.45 , -14.1])
    d.add(e.LABEL, d='down', xy=dots_list[-1].start + [8 * l1+5.2, -15.8],smlltlabel="INVERSOR FOTOVOLTAICO"+"\n"+"-FABRICANTE"+"\n"+"\n"+"-MODELO"+"\n"+"-POTÊNCIA")
    d.add(e.Line399, d='right', xy=dots_list[-1].start + [8 * l1+1.5, -17.95])
    d.add(e.LABEL, d='down', xy=dots_list[-1].start + [8 * l1+5.2, -18.4],smlltlabel="CIRCUITO DE CORRENTE ALTERNADA"+"\n"+"-INDICAÇÃO DE CONDUTORES"+"\n"+"-BITOLA DO CABO"+"\n"+"-TENSÃO")
    d.add(e.Line39, d='right', xy=dots_list[-1].start + [8* l1+1.5 , -20.389])
    d.add(e.LABEL, d='down', xy=dots_list[-1].start + [8 * l1+5.2, -21.5],smlltlabel="CIRCUITO DE CORRENTE CONTÍNUA -"+"\n"+"-POLARIDADE E STRING"+"\n"+"\n"+"-INDICAÇÃO DE FASE POSITIVA E NEGATIVA"+"\n"+"-BITOLA DO CABO"+"\n"+"-TENSÃO")
    d.add(e.Power_generation1, d='right', xy=dots_list[-1].start + [8 * l1+2.3, -22.989])
    d.add(e.LABEL, d='down', xy=dots_list[-1].start + [8 * l1+5.2, -23.2],smlltlabel="MÓDULO FOTOVOLTAICO"+"\n"+"-FABRICANTE-MODELO"+"\n"+"-QUANTIDADE E POTÊNCIA")
    d.add(e.LABEL,color='blue',fontize=25,xy=x_points.end+[-11.45,-25],smlltlabel="NOTA :")
    d.add(e.LABEL,color='blue',fontize=25,xy=x_points.end+[-4.39,-26],smlltlabel="1 - TODOS OS TERRAS SERAO INTERIGAODS, GARANATINDO")
    d.add(e.LABEL,color='blue',fontize=25,xy=x_points.end+[-4.8,-26.3],smlltlabel="ASSIM A EQUIPOTENTIAL ZAÇÃO DE TODO O SISTEMA")
    d.add(e.LABEL,xy=x_points.end+[-11.39,-27],smlltlabel="NOTAS :")
    d.add(e.LABEL,color='red', xy=x_points.end + [-12.39, -27.45], fontize=60,toplabel="1")
    d.add(e.LABEL,xy=x_points.end+[-9.1,-27.8],fontize=130,toplabel="- No decorrer da, execução instalação"+"\n")
    d.add(e.LABEL,xy=x_points.end+[-7.9,-27.85],fontize=130,toplabel="devdem ser seguidas as orientaçoes contidas nas")
    d.add(e.LABEL,xy=x_points.end+[-8.55,-28.2],fontize=130,toplabel="normas brasileiras relativas a  orientaçoes")
    d.add(e.LABEL,xy=x_points.end+[-8.8,-28.5],fontize=130,toplabel="eletricas em baixa tensão (NBR -5410)")
    d.add(e.LABEL,color='red', xy=x_points.end + [-12.45, -29.3], fontize=60,toplabel="2")
    d.add(e.LABEL,xy=x_points.end+[-8.6,-29.3],fontize=130,toplabel=" - E obeigatoria a utilização de EPI's E  EPC's")
    d.add(e.LABEL,xy=x_points.end+[-8.4,-29.6],fontize=130,toplabel="ao longo de toda a exceção da orientaçãoes")
    d.add(e.LABEL,color='red',xy=x_points.end+[-12.5,-30],toplabel="3")
    d.add(e.LABEL,xy=x_points.end+[-9.2,-30],toplabel="- Todos as profissionais envolvidos na")
    d.add(e.LABEL,xy=x_points.end+[-8,-30.3],toplabel="devidamente trinados capacitados habilitados e")
    d.add(e.LABEL,xy=x_points.end+[-8,-30.7],toplabel="autorizados a exercer a  atividade estando com")
    d.add(e.LABEL,xy=x_points.end+[-8.1,-31],toplabel="os treinamentos de NR10 e NR35 atualizados ")
    d.add(e.LABEL,xy=x_points.end+[-7.7,-31.3],toplabel="sendo de responsabilidade do responsavel tecnicao")
    d.add(e.LABEL,xy=x_points.end+[-7.7,-31.6],toplabel="pela instalação certificar - se dessa conformedade.")
    d.add(e.LABEL, color='red', xy=x_points.end + [-12.45, -32], fontize=60, toplabel="4")
    d.add(e.LABEL,xy=x_points.end+[-7.7,-32],toplabel="- Qualquer necessidade de alteração do  instalação no")
    d.add(e.LABEL,xy=x_points.end+[-8.9,-32.3],toplabel="momento da  instalação devera ser")
    d.add(e.LABEL,xy=x_points.end+[-8.7,-32.6],toplabel="comunicado peviamente ao projetista")
    d.add(e.LABEL, color='red', xy=x_points.end + [-12.5, -33], toplabel="5")
    d.add(e.LABEL,xy=x_points.end+[-8.5,-33],toplabel=" - Os cabos CC devem ter cobertura vermelha")
    d.add(e.LABEL, xy=x_points.end + [-7.8, -33.3], toplabel="(polo positivo) e preta (polo negativa. Os cabos")
    d.add(e.LABEL,xy=x_points.end+[-8.02,-33.6],toplabel="CA podem ter cobertura em qualquer col que")
    d.add(e.LABEL,xy=x_points.end+[-8.02,-33.9],toplabel="não seja azul ,verde ou verde e amerelo. Os  ")
    d.add(e.LABEL,xy=x_points.end+[-8.45,-34.2],toplabel="cabos de equipotencialização devem ter ")
    d.add(e.LABEL,xy=x_points.end+[-10.3,-34.5],toplabel="cobertura verde")


    # d.add(e.CB, d='down', xy=dots_list[-1].start+[9* l1 + 1, -2.5],
    #       botlabel=CombinerBox)
    # d.add(e.INVDCAC, xy=dots_list[-1].start+[9* l1 + 1, -5])
    # d.add(e.LABEL, xy=dots_list[-1].start+[9, -5],
    #       botlabel=InverterDCACtype+" "+paco+':\n' + str(InvName))
    # d.add(e.LINE, d='right', xy=dots_list[-1].start+[8 * l1 - 0.5, -7.5], l=l1,rgtlabel=cabletype+':\n' + str(Cable))

    d.pop()  # returns the drawing cursor to the last d.push() --> [0 / 0]

    # -----------------------------------------------------------------------------

    # Writing the title above the drawing

    d.push()
    # d.add(e.LABEL, xy=[0, 2], titlabel=Electricalscheme + str(ProjName))

    d.pop()
    d.draw()

    d.fig.savefig(svg_file2_name, bbox_extra_artists=d.ax.get_default_bbox_extra_artists(),
                  bbox_inches='tight', pad_inches=0.15, dpi=360)
    d.fig.savefig(pdf_file2_name, bbox_extra_artists=d.ax.get_default_bbox_extra_artists(),
                  bbox_inches='tight', pad_inches=0.15, dpi=360)

    subprocess.Popen([svg_file2_name], shell=True)
    subprocess.Popen([pdf_file2_name], shell=True)


def Ec(res, pvdata, invdata, svg_file2_name, pdf_file2_name, ProjName, PvName, InvName, lang, Wp, paco, Nb_Mppt):
    values = list(res.values())
    count = 0
    res_data = {}
    max_parallels = []
    for key in values[0].keys():
        count += 1
        count_inverter_input = 0
        count_parallel = 0
        data_list = []
        pvamp = 0
        sum_pvolt = 0
        pvvolt =0
        count_list_len = []
        for data in values[0][key]:
            count_inverter_input += 1
            no_of_input = len(values[0][key])
            count_list_len.append(len(data))
            for num in data:
                count_parallel += 1
                data_list.append(num)
            pvamp += (count_parallel *pvdata['I_mp_ref'])
            sum_pvolt +=(count_parallel*pvdata['V_mp_ref'])
        data_list.append(count_inverter_input)
        data_list.append(count_list_len)
        max_parallels.append(count_inverter_input)
        data_list.append(count_parallel)
        res_data[count] = data_list
        pvvolt=sum_pvolt/count_parallel
        pvdata['Mp'] =count_parallel
        pvdata['NumBB']=len(values[0].keys())
        pvdata['Bat']=1
        pvdata['BatName']='battery'
        pvdata['PVName']='PVName'
        pvdata['Cable']='Cable'
        invdata= {
            'MPPTin':count_inverter_input,
            'InvName':'InvName',
            'InvEff':0.67,
        }

        # with open('inputs.json', 'w') as json_input_file:
        #     json.dump(Out, json_input_file)
        with open('pvdata.json', 'w') as json_input_file:
            json.dump(pvdata, json_input_file)
        with open('invdata.json', 'w') as json_input_file:
            json.dump(invdata, json_input_file)

        data = open("language_translation.json").read()
        lang_json_data = json.loads(data)
        Electricalscheme = lang_json_data[lang]['electricalscheme']
        PVmodule = lang_json_data[lang]['pvmodule']
        InverterDCACtype = lang_json_data[lang]['Inverter DCAC type']
        CombinerBox = lang_json_data[lang]['combinerbox']
        CombinerBoxAllBB = lang_json_data[lang]['combinerboxforbb']
        Inputs = lang_json_data[lang]['inputs']
        Outputs = lang_json_data[lang]['outputs']
        V = lang_json_data[lang]['V']
        A = lang_json_data[lang]['A']
        In = lang_json_data[lang]['in']
        Out = lang_json_data[lang]['out']
        BB = lang_json_data[lang]['BB']
        pv = lang_json_data[lang]['pv']
        cabletype = lang_json_data[lang]['cabletype']
        gridconnection = lang_json_data[lang]['gridconnection']
        GND = lang_json_data[lang]['GND']
        InverterNr = lang_json_data[lang]['InverterNr']
        MaxInputs = lang_json_data[lang]['MaxInputs']
        UsedInputs = lang_json_data[lang]['UsedInputs']
        # -----------------------------------------------------------------------------

        # opening the JSON file to be able to read its content

        str_data = open("pvdata.json").read()
        pv_json_data = json.loads(str_data)
        str_data = open("invdata.json").read()
        inv_json_data = json.loads(str_data)

        # -----------------------------------------------------------------------------

        # reading the values in the JSON file
        # and connecting them to the correct variable

        Mp = pv_json_data['Mp']
        Mp = int(Mp)

        NumBB = pv_json_data['NumBB']
        NumBB = int(NumBB)

        Bat = pv_json_data['Bat']
        Bat = int(Bat)

        MPPTin = count_inverter_input
        MPPTin = int(MPPTin)

        PVVolt = pv_json_data['V_mp_ref']
        PVVolt = float(PVVolt)

        PVAmp = pv_json_data['I_mp_ref']
        PVAmp = float(PVAmp)

        BatName = pv_json_data['BatName']

        InvEff = inv_json_data['InvEff']
        InvEff = float(InvEff)

        Cable = pv_json_data['Cable']


        # -----------------------------------------------------------------------------

        # defining parameter needed in loops

        ir = 0
        ip = 0
        global iNb
        iNb = 0
        inv_list= []

        # Calculating the maximum Voltage and current at different positions

        VL1 = round(pvvolt)  # Labeling Voltage After BB
        VL2 = round(VL1)
        VL3 = 230  # Labeling Voltage After INV

        AL1 = round(pvamp)  # Labeling the current of one string
        AL2 = round(count_parallel * pvamp)  # Labeling the added up current of one BB
        AL3 = round(AL2 * VL2 * InvEff / VL3)  # Labeling the current of all BB

        # -----------------------------------------------------------------------------

        # One way to change the scaling:
        l1 = 2  # size of elements and lines;

        # -----------------------------------------------------------------------------
        # To avoid inputs, which do not make sense and do not work within he program

        if Mp <= 0:
            sys.exit("The variable Mp has an unexpected value (0 or below)!")
        elif NumBB <= 0:
            sys.exit("The variable NumBB has an unexpected value (0 or below)!")
        elif MPPTin <= 0:
            sys.exit("The variable MPPTin has an unexpected value (0 or below)!")
        elif Bat > 2:
            sys.exit("The variable Bat has an unexpected value (NOT 0, 1, 2)!")
        elif Bat < 0:
            sys.exit("The variable Bat has an unexpected value (NOT 0, 1, 2)!")
        else:
            pass

        # -----------------------------------------------------------------------------

        # starting the drawing with d = schem.Drawing()

        d = schem.Drawing()
        d.push()  # fixes the drawing point [0 / 0]. Return with d.pop() below.

        def bb(data_raw,key,count_list,count_parallelss,max,total_mpptin,total_dict_length):  # function
            global ir, iNb,ns
            MPPTin = key
            counts = count_parallelss
            maximum=max
            # adding the PV modules:
            if Mp:
                d.add(e.LINE, d='right', l=l1)
                d.push()
                sir = 1
                end_point = []
                num = 0
                count_parallel = 1
                pv_number = 0
                pv_numbers = 0
                end_stand = []
                frequency = {}

                # iterating over the list
                for item in data_raw:
                    # checking the element in dictionary
                    if item in frequency:
                        # incrementing the counr
                        frequency[item] += 1
                    else:
                        # initializing the count
                        frequency[item] = 1
                end_points ={}
                end_two={}
                global dots_list
                dots_list=[]
                # print("len of keys",len(frequency.keys()))
                # print("data raw",data_raw)
                if len(frequency.keys())==1 and MPPTin>1 :
                    # import pdb
                    # pdb.set_trace();
                    temp = 0
                    temp2 = 0
                    temp6 = 0
                    temp7 = 0
                    c=0
                    sum_count_list=count_list.copy()
                    frequency_count_list = count_list.copy()
                    for key in range(1, len(sum_count_list) - 1 + 1):
                        sum_count_list[key] = sum_count_list[key] + sum_count_list[key - 1]

                    for data in data_raw:
                        # import pdb;
                        # pdb.set_trace()
                        temp += 1
                        temp6 += 1
                        # if pv_number ==2:
                        #     import pdb
                        #     pdb.set_trace();
                        if temp == sum_count_list[temp2]:
                            temp = 0
                        if temp6 == frequency_count_list[temp7]:
                            temp6 = 0
                        pv_number += 1
                        c += 1
                        if temp7 + 1 < len(frequency_count_list):
                            if len(frequency_count_list) >= 2:
                                if pv_number == (sum_count_list[temp2]+1) and frequency_count_list[
                                    temp7] >= 5 and frequency_count_list[temp7 + 1] == 4:
                                    count_parallel = 1
                                if pv_number == (sum_count_list[temp2] + 1) and frequency_count_list[
                                    temp7] >= 5 and frequency_count_list[temp7 + 1] == 5:
                                    count_parallel = 1
                                if pv_number == (sum_count_list[temp2]+1) and frequency_count_list[
                                    temp7] >= 5 and frequency_count_list[temp7 + 1] > 5:
                                    count_parallel = 1

                            if frequency_count_list[temp7] <= 3 and frequency_count_list[temp7 + 1] > 3:
                                count_parallel = 0
                            if (pv_number) == sum_count_list[temp2] + 1 and frequency_count_list[temp7] >= 3 and \
                                    frequency_count_list[temp7 + 1] > 3 and (frequency_count_list[temp7]>=5 and (frequency_count_list[temp7+1]!=4 and frequency_count_list[temp7+1]!=5 and frequency_count_list[temp7]<5)):
                                count_parallel = 0
                        if count_parallel >= 3 and sum_count_list[temp7] >= 3 and frequency_count_list[temp7] <= 3:
                            pv_numbers = pv_number - 1
                            count_parallel = 1
                        if frequency_count_list[temp7] >= 3 and (frequency_count_list[temp7] + 1) == count_parallel:
                            pv_numbers = pv_number - 1
                            count_parallel = 1
                        if count_parallel <= 3:
                            if data > 0:
                                sf = d.add(e.PV,d='down', label=pv + str(pv_number) + '-' + '1')
                                dots_list.append(sf)
                                dot = d.add(e.PH)
                                sd = d.add(e.PV,label=pv + str(pv_number) + '-' + str(data))
                            if count_parallel == 3 and frequency_count_list[temp7] > 3:
                                label = d.add(e.PH, d='right', xy=sf.end - [0, 1], l=3.9)
                                d.add(e.LABEL, xy=label.end + [1, 0],
                                      label=str(frequency_count_list[temp7]) + 'string')
                                ENDPV = d.add(e.LINE, d='right', xy=sd.end, l=1.62)
                                end_stand.append(ENDPV)
                                x = d.add(e.LINE)
                                d.add(e.DOT, xy=sf.start + [3.9, 0])
                                sf = d.add(e.PV, d='down', label=pv + str(pv_number+(frequency_count_list[temp7]-3)) + '-' + '1')
                                dot = d.add(e.PH)
                                sd = d.add(e.PV, label=pv + str(pv_number+(frequency_count_list[temp7]-3)) + '-' + str(data))
                                if len(data_raw) != (pv_number + frequency_count_list[temp7] - 3):
                                    d.add(e.DOT, xy=sf.start + [3.9, 0])
                            if pv_number != len(data_raw):
                                # ENDPV = d.add(e.LINE, d='right')
                                # # end_stand.append(ENDPV)
                                # if count_parallel != frequency[data] and frequency[data]<=3 and frequency[data]!=1:
                                #     ENDPV = d.add(e.LINE, d='right',l=2.2)
                                #     end_stand.append(ENDPV)
                                #     # x = d.add(e.LINE)
                                #     d.add(e.DOT,xy=sf.start+[2.2,0])
                                if frequency_count_list[ temp7] <= 3 and frequency_count_list[temp7] != 1:
                                    if pv_number!=sum_count_list[temp2]:
                                        ENDPV = d.add(e.LINE, d='right', l=3.9,xy=sd.end+[0,-0])
                                        end_stand.append(ENDPV)
                                        # x = d.add(e.LINE)
                                        d.add(e.DOT, xy=sf.start + [3.9, 0])
                                # if frequency[data] == 1:
                                #     ENDPV = d.add(e.DOT)
                                #     d.add(e.DOT, xy=sf.start + [2.2, 0])
                                if frequency_count_list[temp7] == 1 and len(frequency.keys()) == 1:
                                    ENDPV = d.add(e.DOT,xy=sd.end+[0,-0])
                                    d.add(e.DOT, xy=sf.start + [3.9, 0])

                                # else:
                                #     d.add(e.DOT, xy=sf.start + [2.5, 0])
                                # if count_parallel <3 and frequency[data] >3:
                                #     ENDPV = d.add(e.LINE, d='right',xy=sd.end,l=2.2)
                                #     end_stand.append(ENDPV)
                                #     # x = d.add(e.LINE)
                                #     global dots
                                #     d.add(e.DOT, xy=sf.start + [2.2, 0])
                                # else:
                                #         if count_parallel !=3 or frequency[data]<=3 or (count_parallel!=3 and frequency[data]>3) :
                                #             d.add(e.DOT, xy=sf.start + [2.2, 0])
                                if count_parallel < 3 and frequency_count_list[temp2] > 3:
                                    ENDPV = d.add(e.LINE, d='right', xy=sd.end+[0,-0], l=3.9)
                                    end_stand.append(ENDPV)
                                    # x = d.add(e.LINE)
                                    global dots
                                    d.add(e.DOT, xy=sf.start + [3.9, 0])
                                else:
                                    if count_parallel != 3 or frequency_count_list[temp7] <= 3 or (
                                            count_parallel != 3 and frequency_count_list[temp7] > 3):
                                        d.add(e.DOT, xy=sf.start + [3.9, 0])

                                # d.add(e.LINE, xy=sf.start)
                                # d.add(e.LINE)
                            else:
                                ENDPV = d.add(e.DOT,xy=sd.end+[0,-0])
                                end_stand.append(ENDPV)
                        end_two[data] = ENDPV
                        if MPPTin > 1 and len(frequency.keys()) == 1:
                            if pv_number == (sum_count_list[temp2]):
                                end_point.append(ENDPV)
                                end_points[data] = ENDPV
                            if temp7+1 < len(frequency_count_list):
                                if frequency_count_list[temp7] >= 5 and (frequency_count_list[temp7 + 1] == 4 or frequency_count_list[temp7+1]==5 or frequency_count_list[temp7+1] > 5)and len(
                                        frequency_count_list) >= 2:
                                    if pv_number == sum_count_list[temp2] + 1:
                                        temp2 += 1
                                        temp7 += 1
                                else:
                                    if pv_number == sum_count_list[temp2]:
                                        temp2 += 1
                                        temp7 += 1
                        else:
                            if num != data and (count_parallel != len(data_raw) or len(data_raw)) == 1:
                                end_point.append(ENDPV)
                                end_points[data] = ENDPV
                                num = data
                        count_parallel += 1
                    ir = 0
                    siNb = iNb + 1
                elif len(frequency.keys())!= len(count_list) :
                    pv_number=0
                    count_parallel=1
                    temp = 0
                    temp2 = 0
                    temp6 = 0
                    temp7 = 0
                    sum_count_list=count_list.copy()
                    frequency_count_list = count_list.copy()
                    for key in range(1, len(sum_count_list) - 1 + 1):
                        sum_count_list[key] = sum_count_list[key] + sum_count_list[key - 1]
                    for data in data_raw:
                        # import pdb;
                        # pdb.set_trace()
                        temp += 1
                        temp6 += 1
                        if temp == count_list[temp2]:
                            temp = 0
                        if temp6 == frequency_count_list[temp7]:
                            temp6 = 0
                        pv_number += 1
                        if temp7 + 1 < len(frequency_count_list):
                            if len(frequency_count_list) >= 2:
                                if pv_number == (sum_count_list[temp2]+1) and frequency_count_list[
                                    temp7] >= 5 and frequency_count_list[temp7 + 1] == 4:
                                    count_parallel = 1
                                if pv_number == (sum_count_list[temp2]+1 ) and frequency_count_list[
                                    temp7] >= 5 and frequency_count_list[temp7 + 1] == 5:
                                    count_parallel = 1
                                if pv_number == (sum_count_list[temp2]+1) and frequency_count_list[
                                    temp7] >= 5 and frequency_count_list[temp7 + 1] > 5:
                                    count_parallel = 1
                            if frequency_count_list[temp7] <= 3 and frequency_count_list[temp7 + 1] > 3:
                                count_parallel = 0
                            if (pv_number) == sum_count_list[temp2] + 1 and frequency_count_list[temp7] >= 3 and \
                                    frequency_count_list[temp7 + 1] > 3 and (frequency_count_list[temp7] >= 5 and (
                                    frequency_count_list[temp7 + 1] != 4 and frequency_count_list[temp7 + 1] != 5 and frequency_count_list[temp7 + 1] < 5)):
                                count_parallel = 0
                        # print("pvumner", pv_number)
                        # print("frkddkddldldllist", frequency_count_list[temp2])
                        # if num != data and count_parallel > 3:
                        #     pv_numbers = pv_number -1
                        # if num != data and count_parallel >= 3 :
                        #     pv_numbers = pv_number - 1
                        #     count_parallel = 1
                        if count_parallel >= 3 and sum_count_list[temp7] >= 3 and frequency_count_list[temp7] <= 3:
                            pv_numbers = pv_number - 1
                            count_parallel = 1
                        if frequency_count_list[temp7] >= 3 and (frequency_count_list[temp7] + 1) == count_parallel:
                            pv_numbers = pv_number - 1
                            count_parallel = 1
                        if count_parallel <= 3:
                            if data > 0:
                                sf = d.add(e.PV,d='down', label=pv + str(pv_number) + '-' + '1')
                                dots_list.append(sf)
                                dot = d.add(e.PH)
                                sd = d.add(e.PV, label=pv + str(pv_number) + '-' + str(data))
                            if count_parallel == 3 and frequency_count_list[temp7] > 3:
                                label = d.add(e.PH, d='right', xy=sf.end - [0, 1], l=3.9)
                                d.add(e.LABEL, xy=label.end + [1, 0],
                                      label=str(frequency_count_list[temp7]) + 'string')
                                ENDPV = d.add(e.LINE, d='right', xy=sd.end, l=1.62)
                                end_stand.append(ENDPV)
                                x = d.add(e.LINE)
                                d.add(e.DOT, xy=sf.start + [3.9, 0])
                                sf = d.add(e.PV, d='down', label=pv + str(pv_number+(frequency_count_list[temp7]-3)) + '-' + '1')
                                dot = d.add(e.PH)
                                sd = d.add(e.PV, label=pv + str(pv_number+(frequency_count_list[temp7]-3)) + '-' + str(data))
                                if len(data_raw) != (pv_number + frequency_count_list[temp7] - 3):
                                    d.add(e.DOT, xy=sf.start + [3.9, 0])
                            if pv_number != len(data_raw):
                                # ENDPV = d.add(e.LINE, d='right')
                                # # end_stand.append(ENDPV)
                                # if count_parallel != frequency[data] and frequency[data]<=3 and frequency[data]!=1:
                                #     ENDPV = d.add(e.LINE, d='right',l=2.2)
                                #     end_stand.append(ENDPV)
                                #     # x = d.add(e.LINE)
                                #     d.add(e.DOT,xy=sf.start+[2.2,0])
                                if  frequency_count_list[ temp7] <= 3 and frequency_count_list[temp7] != 1:
                                    if pv_number!=sum_count_list[temp2]:
                                        ENDPV = d.add(e.LINE, d='right', l=3.9,xy=sd.end+[0,-0])
                                        end_stand.append(ENDPV)
                                        # x = d.add(e.LINE)
                                        d.add(e.DOT, xy=sf.start + [3.9, 0])
                                # if frequency[data] == 1:
                                #     ENDPV = d.add(e.DOT)
                                #     d.add(e.DOT, xy=sf.start + [2.2, 0])
                                if frequency_count_list[temp7] == 1 and len(frequency.keys()) >= 1:
                                    ENDPV = d.add(e.DOT,xy=sd.end+[0,-0])
                                    d.add(e.DOT, xy=sf.start + [3.9, 0])

                                # else:
                                #     d.add(e.DOT, xy=sf.start + [2.5, 0])
                                # if count_parallel <3 and frequency[data] >3:
                                #     ENDPV = d.add(e.LINE, d='right',xy=sd.end,l=2.2)
                                #     end_stand.append(ENDPV)
                                #     # x = d.add(e.LINE)
                                #     global dots
                                #     d.add(e.DOT, xy=sf.start + [2.2, 0])
                                # else:
                                #         if count_parallel !=3 or frequency[data]<=3 or (count_parallel!=3 and frequency[data]>3) :
                                #             d.add(e.DOT, xy=sf.start + [2.2, 0])
                                if count_parallel < 3 and frequency_count_list[temp2] > 3:
                                    ENDPV = d.add(e.LINE, d='right', xy=sd.end+[0,-0], l=3.9)
                                    end_stand.append(ENDPV)
                                    # x = d.add(e.LINE)
                                    global dots
                                    d.add(e.DOT, xy=sf.start + [3.9, 0])
                                else:
                                    if count_parallel != 3 or frequency_count_list[temp7] <= 3 or (
                                            count_parallel != 3 and frequency_count_list[temp7] > 3):
                                        d.add(e.DOT, xy=sf.start + [3.9, 0])

                                # d.add(e.LINE, xy=sf.start)
                                # d.add(e.LINE)
                            else:
                                ENDPV = d.add(e.DOT,xy=sd.end+[0,-0])
                                end_stand.append(ENDPV)
                        end_two[data] = ENDPV
                        # print("count list tem[", count_list[temp2])
                        if len(frequency.keys()) >= 1:
                            if pv_number == (sum_count_list[temp2]):
                                end_point.append(ENDPV)
                                end_points[data] = ENDPV
                            if temp7 + 1 < len(frequency_count_list):
                                if frequency_count_list[temp7] >= 5 and (
                                        frequency_count_list[temp7 + 1] == 4 or frequency_count_list[temp7 + 1] == 5 or frequency_count_list[temp7 + 1] > 5 )and len(
                                        frequency_count_list) >= 2:
                                    if pv_number == sum_count_list[temp2] + 1:
                                        temp2 += 1
                                        temp7 += 1
                                else:
                                    if pv_number == sum_count_list[temp2]:
                                        temp2 += 1
                                        temp7 += 1
                        else:
                            if num != data and (count_parallel != len(data_raw) or len(data_raw)) == 1:
                                end_point.append(ENDPV)
                                num = data
                        count_parallel += 1
                    ir = 0
                    siNb = iNb + 1
                else:
                    # import pdb
                    # pdb.set_trace();
                    temp = 0
                    temp2 =0
                    temp7 = 0
                    temp6 = 0
                    sum_count_list = count_list.copy()
                    frequency_count_list = count_list.copy()

                    for key in range(1, len(sum_count_list) - 1 + 1):
                        sum_count_list[key] = sum_count_list[key] + sum_count_list[key - 1]
                    for data in data_raw:
                        # import pdb;
                        # pdb.set_trace()
                        temp +=1
                        temp6 +=1
                        if temp == count_list[temp2]:
                            temp = 0
                        if temp6 == frequency_count_list[temp7]:
                            temp6 = 0
                        pv_number += 1
                        if temp7 + 1 < len(frequency_count_list):
                            if frequency_count_list[temp7] <= 3 and frequency_count_list[
                                temp7 + 1] > 3:
                                count_parallel = 0
                        if count_parallel >= 3 and sum_count_list[temp7] >= 3 and frequency_count_list[temp7] <= 3:
                            pv_numbers = pv_number - 1
                            count_parallel = 1
                        if num != data and count_parallel >= 3:
                            pv_numbers = pv_number - 1
                            count_parallel = 1
                        if count_parallel <= 3:
                            if data > 0:
                                sf = d.add(e.PV, d='down', label=pv + str(pv_number) + '-' + '1')
                                dots_list.append(sf)
                                dot = d.add(e.PH)
                                sd = d.add(e.PV,label=pv + str(pv_number) + '-' + str(data))
                            if count_parallel == 3 and frequency[data] > 3:
                                label = d.add(e.PH, d='right', xy=sf.end - [0, 1], l=3.9)
                                # d.add(e.PH,d='right',xy=sd.start,l=4.2)
                                ENDPV = d.add(e.LINE, d='right', xy=sd.end, l=1.62)
                                end_stand.append(ENDPV)
                                x = d.add(e.LINE)
                                # d.add(e.LINE, xy=sf.start)
                                # d.add(e.LINE)
                                d.add(e.DOT, xy=sf.start + [3.9, 0])
                                sf = d.add(e.PV, d='down', label=pv + str(pv_number+(frequency[data]-3)) + '-' + '1')
                                dot = d.add(e.PH)
                                sd = d.add(e.PV, label=pv + str(pv_number+(frequency[data]-3)) + '-' + str(data))
                                # d.add(e.LINE, d='left', xy=sd.end - [0.3, -1.5], l=0.5)
                                if len(data_raw) != (pv_number + frequency_count_list[temp7] - 3):
                                    d.add(e.DOT, xy=sf.start + [2.2, 0])
                                    # dots_diagram(dots_end, d)
                            if pv_number != len(data_raw):
                                # ENDPV = d.add(e.LINE, d='right')
                                # end_stand.append(ENDPV)
                                if frequency[data] <= 3 and frequency[data] != 1:
                                    if pv_number != sum_count_list[temp2]:
                                        ENDPV = d.add(e.LINE, d='right',xy=sd.end+[0,-0], l=3.9)
                                        end_stand.append(ENDPV)
                                        # x = d.add(e.LINE)
                                        dots_end=d.add(e.DOT, xy=sf.start + [3.9, 0])
                                        # dots_diagram(dots_end, d)
                                        # d.push()
                                    # ENDPV = d.add(e.LINE, d='right', l=2.2)
                                    # end_stand.append(ENDPV)
                                    # # x = d.add(e.LINE)
                                    # d.add(e.DOT, xy=sf.start + [2.2, 0])
                                if frequency[data] == 1:
                                    ENDPV = d.add(e.DOT,xy=sd.end+[0,-0])
                                    dot_end=d.add(e.DOT, xy=sf.start + [3.9, 0])
                                    # dots_diagram(dot_end,d)
                                    # d.add(e.LINE, d='left', xy=dot_s.end , l=1.8)
                                    # joint = d.add(e.LINE, d='down', l=1)
                                    # circle = d.add(e.SOURCE_Testing100, xy=joint.end + [3, 1.2])
                                    # line = d.add(e.LINE, d='left', xy=circle.end + [-3.2, -0.8], l=0.8)
                                    # d.add(e.VSS, d='left', xy=circle.end + [-3.5, -0.8], l=0.5)
                                    # d.add(e.GND, xy=circle.end + [-3, -0.8])
                                    # d.add(e.LABEL, xy=circle.end + [-2.5, 0.3],
                                    #       toplabel='#6mm² PVC 70º' + '\n' + ' 750V')

                                # else:
                                #     d.add(e.DOT, xy=sf.start + [2.5, 0])
                                if count_parallel < 3 and frequency[data] > 3:
                                    ENDPV = d.add(e.LINE, d='right', xy=sd.end+[0,-0], l=3.9)
                                    end_stand.append(ENDPV)
                                    # x = d.add(e.LINE)
                                    global dots
                                    dots_end=d.add(e.DOT, xy=sf.start + [3.9, 0])
                                    # dots_diagram(dots_end, d)
                                    # d.push()
                                else:
                                    if count_parallel != 3 or frequency[data] <= 3 or (
                                            count_parallel != 3 and frequency[data] > 3):
                                        dots_end=d.add(e.DOT, xy=sf.start + [3.9, 0])
                                        # dots_diagram(dots_end, d)

                                # d.add(e.LINE, xy=sf.start)
                                # d.add(e.LINE)
                            else:
                                # sf = d.add(e.PV, d='down', xy=sf.start + [2,0],label='PV' + str(frequency[data]) + '-' + '1')
                                # dot = d.add(e.PH)
                                # sd = d.add(e.PV, xy=sd.start+[2,0],label='PV' + str(frequency[data]) + '-' + str(data))
                                # ENDPV = d.add(e.LINE, d='right')
                                # d.add(e.SEP)
                                # d.add(e.LINE,xy=sf.start)
                                # d.add(e.SEP)
                                ENDPV = d.add(e.DOT,xy=sd.end+[0,-0])
                                end_stand.append(ENDPV)
                                # dots_diagram(ENDPV, d)
                        end_two[data] = ENDPV
                        if num != data and (count_parallel != len(data_raw) or len(data_raw)) == 1:
                            end_point.append(ENDPV)
                            end_points[data] = ENDPV
                            num = data
                            # temp2 += 1
                        if pv_number==sum_count_list[temp2]:
                            temp2 += 1
                            temp7 += 1
                        count_parallel += 1
                    ir = 0
                    siNb = iNb + 1

            # adding the Combiner Box, the Inverters and the Batteries:
            if MPPTin < Mp:  # if Inv inputs < than number of parallels --> add CB
                # import pdb
                # pdb.set_trace();
                if Bat == 2:  # if battery per Building Block do this
                    CBout = 1
                    d.push()
                    count = 0
                    diff = maximum - 150
                    if diff % 60 == 0:
                        diff = int(diff / 60)
                    else:
                        diff = int(diff / 60) + 1
                    if maximum <= 20:
                        n = 6.5
                        n_length = 19.3
                        m_length = 19.65
                        length = 20.9
                        # l=30
                        ns = 9
                    elif maximum > 20 and maximum <= 60:
                        n = 12
                        n_length = 30.2
                        m_length = 30.65
                        length = 28
                        # l=46
                        ns = 10
                    elif maximum > 60 and maximum <= 150:
                        n = 18
                        n_length = 42.3
                        m_length = 42.6
                        length = 28
                        # l=69
                        ns = 18
                    else:
                        n = (diff * 12.5) + 18
                        n_length = (25 * diff) + 42.3
                        m_length = (25 * diff) + 42.6
                        ns = (5 * diff) + 18
                    if MPPTin <= 20:
                        l = 30
                        BB_x = 5
                    elif MPPTin > 20 and MPPTin <= 60:
                        l = 48
                        BB_x = 7
                    elif MPPTin > 60 and MPPTin <= 150:
                        l = 120
                        BB_x = 20.5
                    else:
                        l = (diff * 50) + 120
                        BB_x = (diff * 13.5) + 20.5

                    for line in end_point:
                        count += 1
                        if len(data_raw) > 2 and len(data_raw)!=4and len(end_point) == 2:
                            ends = d.add(e.LINE, d='down', xy=line.start, l=8)
                            # if count % 2 == 0:
                            #     d.add(e.LINE, d='left', xy=ends.end, l=l1)
                            #     x = d.add(e.LINE, d='down', l=3 * l1)
                            # else:
                            #     if len(end_point) > 1:
                            #         d.add(e.LINE, d='right', xy=ends.end, l=l1)
                            #         d.add(e.LINE, d='down',theta=-90, l=12.6)
                            #     else:
                            #         d.add(e.LINE, d='right', xy=ends.end, l=l1)
                            #         d.add(e.LINE, d='down', l=3 * l1)
                        if len(data_raw) == 4 and len(end_point) == 2:
                            ends = d.add(e.LINE, d='down', xy=line.start, l=1)
                            if count % 2 == 0:
                                d.add(e.LINE, d='left', xy=ends.end, l=1)
                                x = d.add(e.LINE, d='down', l=3 * l1)
                            else:
                                if len(end_point) > 1:
                                    d.add(e.LINE, d='right', xy=ends.end, l=1)
                                    x = d.add(e.LINE, d='down', theta=-90, l=n_length)
                                    x_right = d.add(e.LINE, d='right', xy=x.end, tox=end_point[1].start - [1.3, 0],
                                                    l=1.8 * l1)
                                    d.add(e.LINE, d='down', xy=x_right.end, l=0.18 * l1)
                                else:
                                    d.add(e.LINE, d='right', xy=ends.end, l=l1)
                                    d.add(e.LINE, d='down', l=3 * l1)

                        if len(data_raw) > 2 and len(end_point) > 2 and (count == len(end_point)):
                            ends = d.add(e.LINE, d='down', xy=line.start, l=8)
                        if len(data_raw) >= 3 and len(end_point) ==1:
                            ends = d.add(e.LINE, d='down', xy=line.start, l=8)

                    for line in end_two:
                        count += 1
                        if len(data_raw) == 2 and len(end_two)!=1:
                            ends = d.add(e.LINE, d='down', xy=end_two[line].start, l=l1)
                            if count % 2 == 0:
                                d.add(e.LINE, d='right', xy=ends.end, l=1)
                                d.add(e.LINE, d='down', l=9.3 * l1)
                            else:
                                d.add(e.LINE, d='left', xy=ends.end, l=1)
                                d.add(e.LINE, d='down', l=3 * l1)
                        if len(data_raw) == 2 and len(end_two) == 1:
                            ends = d.add(e.LINE, d='down', xy=end_two[line].start, l=8)
                        if len(data_raw) == 1:
                            ends = d.add(e.LINE, d='down', xy=end_two[line].start, l=4 * l1)

                    d.push()
                    # d_right =d.add(e.LINE, d='right', l=2 * l1)
                    # d.add(e.CONVDCDC, d='down',
                    #       smllblabel='In ' + str(VL1) + ' V / ' + str(AL2) + ' A',
                    #       smlrblabel='Out ' + str(VL3) + ' V / ' + str(AL3) + ' A',
                    #       toplabel='DCDC Converter')
                    # d.add(e.BATTERY, d='down', toplabel='Battery ' + str(siNb))
                    d.pop()
                    inv_start =d.add(e.LINE, d='down', l=n* l1)
                    # CBout = 1
                    # d.add(e.CB, d='down', xy=ENDPV.start,
                    #       smlltlabel='In ' + str(VL1) + ' V / ' + str(AL1) + ' A',
                    #       smlrtlabel='Out ' + str(VL2) + ' V / ' + str(AL2) + ' A',
                    #       toplabel='Combiner Box ' + str(siNb)
                    #                + '\nInputs: ' + str(Mp)
                    #                + '\nOutputs: ' + str(CBout))
                    # d.push()
                    # d.add(e.LINE, d='left', l=l1)
                    # d.add(e.CONVDCDC, d='down',
                    #       smllblabel='In ' + str(VL1) + ' V / ' + str(AL2) + ' A',
                    #       smlrblabel='Out ' + str(VL3) + ' V / ' + str(AL3) + ' A',
                    #       toplabel='DCDC Converter')
                    # d.add(e.BATTERY, d='down', toplabel='Battery ' + str(siNb))
                    # d.pop()
                    # d.add(e.LINE, d='down', l=2 * l1)

                else:
                    # import pdb
                    # pdb.set_trace();
                    CBout = 1
                    d.push()
                    count = 0
                    diff = maximum - 150
                    if diff % 60 == 0:
                        diff = int(diff / 60)
                    else:
                        diff = int(diff / 60) + 1
                    if maximum <= 20:
                        n = 6.5
                        n_length = 19.3
                        m_length = 19.65
                        length = 20.9
                        # l=30
                        ns = 9
                    elif maximum > 20 and maximum <= 60:
                        n = 12
                        n_length = 30.2
                        m_length = 30.65
                        length = 28
                        # l=46
                        ns = 10
                    elif maximum > 60 and maximum <= 150:
                        n = 18
                        n_length = 42.3
                        m_length = 42.6
                        length = 28
                        # l=69
                        ns = 18
                    else:
                        n = (diff * 12.5) + 18
                        n_length = (25 * diff) + 42.3
                        m_length = (25 * diff) + 42.6
                        ns = (5 * diff) + 18
                    if MPPTin <= 20:
                        l = 30
                        BB_x=5
                    elif MPPTin > 20 and MPPTin <= 60:
                        l = 48
                        BB_x=7
                    elif MPPTin > 60 and MPPTin <= 150:
                        l = 120
                        BB_x=20.5
                    else:
                        l = (diff * 50) + 120
                        BB_x=(diff*13.5)+20.5
                    for line in end_point:
                        count += 1
                        if len(data_raw) > 2  and len(data_raw)!=4and len(end_point) == 2:
                            ends = d.add(e.LINE, d='down', xy=line.start, l=8)
                            # if count % 2 == 0:
                            #         d.add(e.LINE, d='left', xy=ends.end, l=l1)
                            #         x = d.add(e.LINE, d='down', l=3 * l1)
                            # else:
                            #     if len(end_point) > 1:
                            #             d.add(e.LINE, d='right', xy=ends.end, l=1.5)
                            #             d.add(e.LINE, d='down', theta=-90, l=18.6)
                            #     else:
                            #         d.add(e.LINE, d='right', xy=ends.end, l=l1)
                            #         d.add(e.LINE, d='down', l=3 * l1)
                        if len(data_raw) == 4 and len(end_point) == 2:
                            ends = d.add(e.LINE, d='down', xy=line.start, l=l1)
                            if count % 2 == 0:
                                d.add(e.LINE, d='left', xy=ends.end, l=1)
                                x = d.add(e.LINE, d='down', l=3 * l1)
                            else:
                                if len(end_point) > 1:
                                    d.add(e.LINE, d='right', xy=ends.end, l=1)
                                    x = d.add(e.LINE, d='down', theta=-90, l=n_length)
                                    x_right = d.add(e.LINE, d='right', xy=x.end, tox=end_point[1].start - [1.3, 0],
                                                    l=1.8*l1)
                                    d.add(e.LINE, d='down', xy=x_right.end, l=0.18 * l1)
                                else:
                                    d.add(e.LINE, d='right', xy=ends.end, l=l1)
                                    d.add(e.LINE, d='down', l=3 * l1)

                        if len(data_raw) > 2 and len(end_point) > 2 and (count == len(end_point)):
                            ends = d.add(e.LINE, d='down', xy=line.start, l=8)
                        if len(data_raw) >= 3 and len(end_point) ==1:
                            ends = d.add(e.LINE, d='down', xy=line.start, l=8)

                    for line in end_two:
                        count += 1
                        if len(data_raw) == 2 and len(end_two)!=1:
                            ends = d.add(e.LINE, d='down', xy=end_two[line].start, l=l1)
                            if count % 2 == 0:
                                d.add(e.LINE, d='right', xy=ends.end, l=1)
                                d.add(e.LINE, d='down', l=m_length)
                            else:
                                d.add(e.LINE, d='left', xy=ends.end, l=1)
                                d.add(e.LINE, d='down', l=3 * l1)
                        if len(data_raw) ==2 and len(end_two)==1:
                            ends = d.add(e.LINE, d='down', xy=end_two[line].start, l=8)
                        if len(data_raw) == 1:
                            ends = d.add(e.LINE, d='down', xy=end_two[line].start, l=4 * l1)

                    d.push()
                    # d_right =d.add(e.LINE, d='right', l=2 * l1)
                    # d.add(e.CONVDCDC, d='down',
                    #       smllblabel='In ' + str(VL1) + ' V / ' + str(AL2) + ' A',
                    #       smlrblabel='Out ' + str(VL3) + ' V / ' + str(AL3) + ' A',
                    #       toplabel='DCDC Converter')
                    # d.add(e.BATTERY, d='down', toplabel='Battery ' + str(siNb))
                    d.pop()
                    inv_start = d.add(e.LINE, d='down', l=n* l1)
                    # d.add(e.CB, d='down', xy=ENDPV.start,
                    #       smlltlabel='In ' + str(VL1) + ' V / ' + str(AL1) + ' A',
                    #       smlrtlabel='Out ' + str(VL2) + ' V / ' + str(AL2) + ' A',
                    #       toplabel='Combiner Box ' + str(siNb)
                    #                + '\nInputs: ' + str(Mp)
                    #                + '\nOutputs: ' + str(CBout))
                global inv;
                INVDCAC = e.update_inverter(l)
                inv= d.add(INVDCAC, d='down',
                      toplabel=InverterNr  + str(siNb) +
                               '\n'+MaxInputs+': ' + str(Nb_Mppt)
                               + '\n'+UsedInputs+': ' + str(MPPTin))
                d.add(e.LABEL, xy=inv.end + [BB_x, 2], smlltlabel=In + str(VL2) + V + '/ ' + str(AL2) + A)
                d.add(e.LABEL, xy=inv.end + [BB_x, 0.5], smlrtlabel=Out + str(VL3) + V + '/ ' + str(AL3) + A)
                inv_list.append(inv)
                draw_diagram(end_points, data_raw, frequency, counts, end_point, inv, inv_start, count_list,
                                       MPPTin, maximum, diff,total_mpptin,total_dict_length,Mp,d)

            else:
                # import pdb
                # pdb.set_trace();
                if Bat == 2:  # if battery per BB do this
                    CBout = 1
                    direction = 'right'
                    count = 0
                    diff = maximum - 150
                    if diff % 60 == 0:
                        diff = int(diff / 60)
                    else:
                        diff = int(diff / 60) + 1
                    if maximum <= 20:
                        n = 6.5
                        n_length = 19.3
                        m_length = 19.65
                        length = 20.9
                        # l=30
                        ns = 9
                    elif maximum > 20 and maximum <= 60:
                        n = 12
                        n_length = 30.2
                        m_length = 30.65
                        length = 28
                        # l=46
                        ns = 10
                    elif maximum > 60 and maximum <= 150:
                        n = 18
                        n_length = 42.3
                        m_length = 42.6
                        length = 28
                        # l=69
                        ns = 18
                    else:
                        n = (diff * 12.5) + 18
                        n_length = (25 * diff) + 42.3
                        m_length = (25 * diff) + 42.6
                        ns = (5 * diff) + 18
                    if MPPTin <= 20:
                        l = 30
                        BB_x = 5
                    elif MPPTin > 20 and MPPTin <= 60:
                        l = 48
                        BB_x = 7
                    elif MPPTin > 60 and MPPTin <= 150:
                        l = 120
                        BB_x = 20.5
                    else:
                        l = (diff * 50) + 120
                        BB_x = (diff * 13.5) + 20.5

                    for line in end_point:
                        count += 1
                        if len(data_raw) > 2 and len(data_raw)!=4 and len(end_point) == 2:
                            ends = d.add(e.LINE, d='down', xy=line.start, l=8)
                            # if count % 2 == 0:
                            #     d.add(e.LINE, d='left', xy=ends.end, l=l1)
                            #     x = d.add(e.LINE, d='down', l=3 * l1)
                            # else:
                            #     if len(end_point) > 1:
                            #         d.add(e.LINE, d='right', xy=ends.end, l=l1)
                            #         d.add(e.LINE, d='down', theta=-90, l=12.6)
                            #     else:
                            #         d.add(e.LINE, d='right', xy=ends.end, l=l1)
                            #         d.add(e.LINE, d='down', l=3 * l1)
                        if len(data_raw) == 4 and len(end_point) == 2:
                            ends = d.add(e.LINE, d='down', xy=line.start, l=l1)
                            if count % 2 == 0:
                                d.add(e.LINE, d='left', xy=ends.end, l=1)
                                x = d.add(e.LINE, d='down', l=3 * l1)
                            else:
                                if len(end_point) > 1:
                                    d.add(e.LINE, d='right', xy=ends.end, l=1)
                                    x = d.add(e.LINE, d='down', theta=-90, l=n_length)
                                    x_right = d.add(e.LINE, d='right', xy=x.end, tox=end_point[1].start - [1.3, 0],
                                                    l=1.8 * l1)
                                    d.add(e.LINE, d='down', xy=x_right.end, l=0.18 * l1)
                                else:
                                    d.add(e.LINE, d='right', xy=ends.end, l=l1)
                                    d.add(e.LINE, d='down', l=3 * l1)

                        if len(data_raw) > 2 and len(end_point) > 2 and (count == len(end_point)):
                            ends = d.add(e.LINE, d='down', xy=line.start, l=8)
                        if len(data_raw) >= 3 and len(end_point) ==1 :
                            ends = d.add(e.LINE, d='down', xy=line.start, l=8)
                    for line in end_two:
                        count += 1
                        if len(data_raw) == 2 and len(end_two)!=1:
                            ends = d.add(e.LINE, d='down', xy=end_two[line].start, l=l1)
                            if count % 2 == 0:
                                d.add(e.LINE, d='right', xy=ends.end, l=1)
                                d.add(e.LINE, d='down', l=m_length)
                            else:
                                d.add(e.LINE, d='left', xy=ends.end, l=1)
                                d.add(e.LINE, d='down', l=3 * l1)
                        if len(data_raw) == 2 and len(end_two) == 1:
                            ends = d.add(e.LINE, d='down', xy=end_two[line].start, l=8)
                        if len(data_raw) == 1:
                            ends = d.add(e.LINE, d='down', xy=end_two[line].start, l=4 * l1)

                    d.push()
                    # d.add(e.LINE, d='right', l=2 * l1)
                    # d.add(e.CONVDCDC, d='down',
                    #       smllblabel='In ' + str(VL1) + ' V / ' + str(AL2) + ' A',
                    #       smlrblabel='Out ' + str(VL3) + ' V / ' + str(AL3) + ' A',
                    #       toplabel='DCDC Converter')
                    # d.add(e.BATTERY, d='down', toplabel='Battery ' + str(siNb))
                    d.pop()
                    inv_start = d.add(e.LINE, d='down', l=n * l1)
                    # global inv;
                    INVDCAC = e.update_inverter(l)
                    inv = d.add(INVDCAC,
                          toplabel=InverterNr  + str(siNb) +
                                   '\n'+MaxInputs+': ' + str(Nb_Mppt)
                                   + '\n'+UsedInputs+': ' + str(MPPTin))
                    d.add(e.LABEL, xy=inv.end + [BB_x, 2], smlltlabel=In + str(VL2) + V + '/ ' + str(AL2) + A)
                    d.add(e.LABEL, xy=inv.end + [BB_x, 0.5], smlrtlabel=Out + str(VL3) + V + '/ ' + str(AL3) + A)
                    inv_list.append(inv)

                    draw_diagram(end_points, data_raw, frequency, counts, end_point, inv, inv_start,
                                           count_list, MPPTin, maximum,total_mpptin,total_dict_length, d)

                else:  # if NO battery  do this
                    # import pdb
                    # pdb.set_trace();
                    CBout = 1
                    direction = 'right'
                    count = 0
                    diff = maximum - 150
                    if diff % 60 == 0:
                        diff = int(diff / 60)
                    else:
                        diff = int(diff / 60) + 1
                    if maximum <= 20:
                        n = 6.5
                        n_length = 19.3
                        m_length = 19.65
                        length = 20.9
                        # l=30
                        ns = 9
                    elif maximum > 20 and maximum <= 60:
                        n = 12
                        n_length = 30.2
                        m_length = 30.65
                        length = 28
                        # l=46
                        ns = 10
                    elif maximum > 60 and maximum <= 150:
                        n = 18
                        n_length = 42.3
                        m_length = 42.6
                        length = 28
                        # l=69
                        ns = 18
                    else:
                        n = (diff * 12.5) + 18
                        n_length = (25 * diff) + 42.3
                        m_length = (25 * diff) + 42.6
                        ns = (5 * diff) + 18
                    if MPPTin <= 20:
                        l = 30
                        BB_x=5
                    elif MPPTin > 20 and MPPTin <= 60:
                        l = 48
                        BB_x=7
                    elif MPPTin > 60 and MPPTin <= 150:
                        l = 120
                        BB_x=20.5
                    else:
                        BB_x=(diff*13.5)+20.5
                        l = (diff * 50) + 120
                    for line in end_point:
                        count += 1
                        if len(data_raw) > 2 and len(data_raw) !=4 and len(end_point) == 2:
                            ends = d.add(e.LINE, d='down', xy=line.start, l=8)
                            # if count % 2 == 0:
                            #     d.add(e.LINE, d='left', xy=ends.end, l=l1)
                            #     x = d.add(e.LINE, d='down', l=3 * l1)
                            # else:
                            #     if len(end_point) > 1:
                            #         d.add(e.LINE, d='right', xy=ends.end, l=l1)
                            #         d.add(e.LINE, d='down', theta=-90, l=12.6)
                            #     else:
                            #         d.add(e.LINE, d='right', xy=ends.end, l=l1)
                            #         d.add(e.LINE, d='down', l=3 * l1)
                        if len(data_raw) == 4 and len(end_point) == 2:
                            ends = d.add(e.LINE, d='down', xy=line.start, l=l1)
                            if count % 2 == 0:
                                d.add(e.LINE, d='left', xy=ends.end, l=1)
                                x = d.add(e.LINE, d='down', l=3 * l1)
                            else:
                                if len(end_point) > 1:
                                    d.add(e.LINE, d='right', xy=ends.end, l=1)
                                    x = d.add(e.LINE, d='down', theta=-90, l=n_length)
                                    x_right = d.add(e.LINE, d='right', xy=x.end, tox=end_point[1].start - [1.3, 0],
                                                    l=1.8*l1)
                                    d.add(e.LINE, d='down', xy=x_right.end, l=0.18 * l1)
                                else:
                                    d.add(e.LINE, d='right', xy=ends.end, l=l1)
                                    d.add(e.LINE, d='down', l=3 * l1)

                        if len(data_raw) > 2 and len(end_point) > 2 and (count == len(end_point)):
                            ends = d.add(e.LINE, d='down', xy=line.start, l=8)
                        if len(data_raw) >= 3 and len(end_point) == 1:
                            ends = d.add(e.LINE, d='down', xy=line.start, l=8)

                    for line in end_two:
                        count += 1
                        if len(data_raw) == 2 and len(end_two)!=1:
                            ends = d.add(e.LINE, d='down', xy=end_two[line].start, l=l1)
                            if count % 2 == 0:
                                d.add(e.LINE, d='right', xy=ends.end, l=1)
                                d.add(e.LINE, d='down', l=m_length)
                            else:
                                d.add(e.LINE, d='left', xy=ends.end, l=1)
                                d.add(e.LINE, d='down', l=3 * l1)
                        if len(data_raw)==2 and len(end_two)==1:
                            ends = d.add(e.LINE, d='down', xy=end_two[line].start, l=8)
                        if len(data_raw) == 1:
                            ends = d.add(e.LINE, d='down', xy=end_two[line].start, l=4 * l1)

                    d.push()
                    # d.add(e.LINE, d='right', l=2 * l1)
                    # d.add(e.CONVDCDC, d='down',
                    #       smllblabel='In ' + str(VL1) + ' V / ' + str(AL2) + ' A',
                    #       smlrblabel='Out ' + str(VL3) + ' V / ' + str(AL3) + ' A',
                    #       toplabel='DCDC Converter')
                    # d.add(e.BATTERY, d='down', toplabel='Battery ' + str(siNb))
                    d.pop()
                    inv_start = d.add(e.LINE, d='down', l=n * l1)
                    # global inv;
                    INVDCAC = e.update_inverter(l)
                    inv = d.add(INVDCAC,
                                toplabel=InverterNr + str(siNb) +
                                         '\n'+MaxInputs+': ' + str(Nb_Mppt)
                                         + '\n'+UsedInputs+': ' + str(MPPTin))
                    d.add(e.LABEL, xy=inv.end + [BB_x, 2], smlltlabel=In + str(VL2) + V + '/ ' + str(AL2) + A)
                    d.add(e.LABEL, xy=inv.end + [BB_x, 0.5], smlrtlabel=Out + str(VL3) + V + '/ ' + str(AL3) + A)
                    inv_list.append(inv)

                    draw_diagram(end_points, data_raw, frequency, counts, end_point, inv, inv_start,
                                           count_list, MPPTin, maximum,diff,total_mpptin,total_dict_length,Mp, d)

            iNb = iNb + 1

        # ----------------------------------------------------------------------------

        # Placement of the BB label on y-axis
        BBLabelY = 0
        BBLabelY = -3.2

        # Placement of the BB label on x-axis
        BBLabelX = 0

        # Calling the main function bb(), to create the Building Blocks:
        raw = 0
        no=0
        # import pdb
        # pdb.set_trace();
        if NumBB == 1:
            for key in res_data:
                no += 1
                # import pdb
                # pdb.set_trace();
                raw += 1
                max_parallel = max(max_parallels)
                total_mpptin=sum(max_parallels)
                if raw == 1:
                    ENDBB = d.add(e.GND, botlabel=GND, l=l1)
                    d.add(e.DOT)
                    d.add(e.LABEL, xy=ENDBB.start + [BBLabelX, BBLabelY], titlabel=BB + str(1))
                    bb(res_data[key][:-3], res_data[key][-3], res_data[key][-2],res_data[key][-1],max_parallel,total_mpptin,len(res_data))
                    BB1DOT = d.add(e.DOT)
                else:
                    ENDBB = d.add(e.GND, xy=dots_list[-1].start + [l1 * ns, 0],
                                  botlabel=GND, l=l1)
                    d.add(e.DOT, xy=ENDBB.start)
                    d.add(e.LABEL, xy=ENDBB.start + [BBLabelX, BBLabelY],
                          titlabel=BB + str(no))
                    bb(res_data[key][:-3], res_data[key][-3], res_data[key][-2],res_data[key][-2],max_parallel,total_mpptin,len(res_data))
                    d.add(e.LINE, xy=inv.end, to=BB1DOT.start - [0, 0], l=l1)

        else:
            # import pdb;
            # pdb.set_trace()
            for key in res_data:
                raw += 1
                no+= 1
                max_parallel = max(max_parallels)
                total_mpptin=sum(max_parallels)
                if len(res_data) == len(values[0].keys()):
                    if raw == 1:
                        ENDBB = d.add(e.GND, botlabel=GND, l=l1)
                        d.add(e.DOT)
                        d.add(e.LABEL, xy=ENDBB.start + [BBLabelX, BBLabelY], titlabel=BB + str(1))
                        bb(res_data[key][:-3], res_data[key][-3], res_data[key][-2], res_data[key][-1], max_parallel,
                           total_mpptin, len(res_data))
                        BB1DOT = d.add(e.DOT)
                    else:
                        ENDBB = d.add(e.GND, xy=dots_list[-1].start + [l1 * ns, 0],
                                      botlabel=GND, l=l1)
                        d.add(e.DOT, xy=ENDBB.start)
                        d.add(e.LABEL, xy=ENDBB.start + [BBLabelX, BBLabelY],
                              titlabel=BB + str(no))
                        bb(res_data[key][:-3], res_data[key][-3], res_data[key][-2], res_data[key][-1], max_parallel,
                           total_mpptin, len(res_data))
                        d.add(e.LINE, xy=inv.end, to=inv_list[0].end, l=l1)
        # # -----------------------------------------------------------------------------
        #
        # # The Combiner Box after the Building Blocks
        #
        if len(res_data) == len(values[0].keys()):
            if NumBB > 1:
                d.add(e.CB, d='down', xy=inv_list[0].end,
                      smlltlabel=In + str(VL3) + V + ' / ' + str(AL3) + A,
                      smlrtlabel=Out + str(VL3) + ' V / ' + str(AL3 * NumBB) + A,
                      toplabel=CombinerBoxAllBB
                               + '\n' + Inputs + ': ' + str(NumBB)
                               + '\n' + Outputs + ': 1')

        # -----------------------------------------------------------------------------

        # If the system uses only one Battery, it gets added here:

        # if Bat == 1:
        #     d.push()
        #     d.add(e.LINE, d='left', l=2*l1)
        #     d.add(e.INVACDC, d='down',
        #           smllblabel='In ' + str(VL3) + ' V / ' + str(AL3) + ' A',
        #           smlrblabel='Out ' + str(VL3) + ' V / ' + str(AL3) + ' A',
        #           toplabel='Inverter transformer')
        #     d.add(e.BATTERY, d='down', toplabel='Battery All BB')
        #     d.pop()
        #     d.add(e.LINE, d='down', l=1*l1)

        # -----------------------------------------------------------------------------

        # The connection to the electrical grid:
        if NumBB == 1:
            Grid = d.add(e.GRID, d='down',xy=inv.end, toplabel=gridconnection)
        else:
            Grid = d.add(e.GRID, d='down', toplabel=gridconnection)

        # -----------------------------------------------------------------------------

        # Finishing the drawing, saving it to a pdf and automatically open it.

    d.add(e.PV, d='down', xy=dots_list[-1].start+[8 * l1 + 1, 0], botlabel=PVmodule+'\n' + str(PvName) +
                   '\n' + str(Wp))
    d.add(e.CB, d='down', xy=dots_list[-1].start+[8* l1 + 1, -2.5],
          botlabel=CombinerBox)
    d.add(e.INVDCACS, xy=dots_list[-1].start+[8* l1 + 1, -5])
    d.add(e.LABEL, xy=dots_list[-1].start+[18, -5],
          botlabel=InverterDCACtype+" "+paco+':\n' + str(InvName))
    d.add(e.LINE, d='right', xy=dots_list[-1].start+[6.5 * l1 - 0.5, -7.5], l=l1,rgtlabel=cabletype+':\n' + str(Cable))

    d.pop()  # returns the drawing cursor to the last d.push() --> [0 / 0]

    # -----------------------------------------------------------------------------

    # Writing the title above the drawing

    d.push()
    d.add(e.LABEL, xy=[0, 1], titlabel=Electricalscheme + str(ProjName))

    d.pop()
    d.draw()

    d.fig.savefig(svg_file2_name, bbox_extra_artists=d.ax.get_default_bbox_extra_artists(),
                  bbox_inches='tight', pad_inches=0.15, dpi=360)
    d.fig.savefig(pdf_file2_name, bbox_extra_artists=d.ax.get_default_bbox_extra_artists(),
                  bbox_inches='tight', pad_inches=0.15, dpi=360)

    subprocess.Popen([svg_file2_name], shell=True)
    subprocess.Popen([pdf_file2_name], shell=True)