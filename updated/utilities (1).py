import json  # for writing, saving and opening JSON files
import matplotlib
from sldcircuit.PVScheme import SchemDraw_PV as schem  # import SchemDraw_PV as schem
from sldcircuit.PVScheme.SchemDraw_PV import elements_PV as e# import the elements

# d = schem.Drawing()
# d.push()
l1=2
global max_y_limit
# max_y_limit=0


def draw(first_key,end_points,last_key,MPPTin,length,dict_lenght,data_raw_length,d):
    if last_key != end_points and first_key != end_points:
        d_end = d.add(e.solid_line,color='blue', d='right', xy=end_points.start + [-1.5, -12], l=1)
        d.add(e.dashed_line,color='blue', d='right', l=3)
        d.add(e.solid_line,color='blue', d='right', l=0.5)
        d.add(e.dashed_line, color='blue',d='right', l=3)
        d.add(e.solid_line,color='blue', d='right', l=0.5)
        d_end = d.add(e.solid_line,color='blue', d='right', xy=end_points.start + [-1.5, -length], l=1)
    elif first_key == end_points and first_key!=last_key:
        d_end = d.add(e.solid_line,color='blue', d='right', xy=end_points.start + [-3, -12], l=2)
        d.add(e.dashed_line,color='blue', d='right', l=3)
        d.add(e.solid_line,color='blue', d='right', l=0.5)
        d.add(e.dashed_line,color='blue', d='right', l=3)
        d.add(e.solid_line, color='blue',d='right', l=0.5)
        d.add(e.dashed_line,color='blue', d='right', l=3)
        d.add(e.solid_line, color='blue',d='right', l=0.3)
        if MPPTin !=2 and data_raw_length!=2:
            d.add(e.dashed_line,color='blue', d='right', l=3)
            d.add(e.solid_line, color='blue',d='right', l=0.5)
            d.add(e.dashed_line,color='blue', d='right', l=3)
            d.add(e.solid_line, color='blue',d='right', l=0.5)
        d.add(e.LINE,color='blue', d='down', xy=d_end.start, l=2)
        d_end = d.add(e.solid_line,color='blue', d='right', xy=end_points.start + [-3, -length], l=2)
        d.add(e.dashed_line,color='blue', d='right', l=3)
        d.add(e.solid_line, color='blue',d='right', l=0.5)
        d.add(e.dashed_line,color='blue', d='right', l=3)
        d.add(e.solid_line,color='blue', d='right', l=0.5)
        d.add(e.dashed_line,color='blue', d='right', l=3)
        d.add(e.solid_line, color='blue',d='right', l=0.5)
        if MPPTin !=2 and data_raw_length!=2:
            d.add(e.dashed_line,color='blue', d='right', l=3)
            d.add(e.solid_line, color='blue',d='right', l=0.5)
            d.add(e.dashed_line,color='blue', d='right', l=3)
            d.add(e.solid_line, color='blue',d='right', l=0.5)
        d.add(e.LINE,color='blue', d='up', xy=d_end.start, l=2)
    elif first_key == end_points and first_key == last_key and MPPTin==1:
        d_end = d.add(e.solid_line,color='blue', d='right', xy=end_points.start + [-3, -12], l=1)
        d.add(e.dashed_line, color='blue',d='right', l=2)
        d.add(e.solid_line,color='blue', d='right', l=1)
        d.add(e.dashed_line,color='blue', d='right', l=2)
        d.add(e.solid_line,color='blue', d='right', l=1)
        d.add(e.dashed_line,color='blue', d='right', l=2)
        d.add(e.solid_line, color='blue',d='right', l=1)
        d.add(e.solid_line,color='blue', d='down', l=1)
        d.add(e.dashed_line,color='blue', d='down', l=2)
        d.add(e.solid_line, color='blue',d='down', l=1)
        d.add(e.solid_line, color='blue', d='down', l=1)
        d.add(e.dashed_line, color='blue', d='down', l=2)
        d.add(e.solid_line, color='blue', d='down', l=1)
        d.add(e.dashed_line, color='blue', d='down', l=2)
        d.add(e.solid_line, color='blue', d='down', l=1)
        if length==28:
            d.add(e.solid_line, color='blue', d='down', l=1)
            d.add(e.dashed_line, color='blue', d='down', l=2)
            d.add(e.solid_line, color='blue', d='down', l=1)
            d.add(e.dashed_line, color='blue', d='down', l=2)
            d.add(e.solid_line, color='blue', d='down', l=1)

        if length==42 or length==32:
            d.add(e.solid_line, color='blue', d='down', l=1)
            d.add(e.dashed_line, color='blue', d='down', l=2)
            d.add(e.solid_line, color='blue', d='down', l=1)
            d.add(e.dashed_line, color='blue', d='down', l=2)
            if length!=32 and length==42:
                d.add(e.solid_line, color='blue', d='down', l=1)
                d.add(e.solid_line, color='blue', d='down', l=1)
                d.add(e.dashed_line, color='blue', d='down', l=2)
                d.add(e.solid_line, color='blue', d='down', l=1)
                d.add(e.dashed_line, color='blue', d='down', l=2)
                d.add(e.solid_line, color='blue', d='down', l=1)
        d.add(e.solid_line,color='blue', d='left', l=1)
        d.add(e.dashed_line,color='blue', d='left', l=2)
        d.add(e.solid_line, color='blue',d='left', l=1)
        d.add(e.dashed_line,color='blue', d='left', l=2)
        d.add(e.solid_line, color='blue',d='left', l=1)
        d.add(e.dashed_line,color='blue', d='left', l=2)
        d.add(e.solid_line, color='blue',d='left', l=1)
        d.add(e.dashed_line,color='blue', d='left', l=2)
        d.add(e.solid_line, color='blue',d='left', l=1)
        d.add(e.solid_line, color='blue',d='up', l=1)
        d.add(e.dashed_line,color='blue', d='up', l=2)
        d.add(e.solid_line, color='blue',d='up', l=1)
        d.add(e.dashed_line,color='blue', d='up', l=2)
        d.add(e.solid_line,color='blue', d='up', l=1)
        d.add(e.solid_line, color='blue', d='up', l=1)
        d.add(e.dashed_line, color='blue', d='up', l=2)
        d.add(e.solid_line, color='blue', d='up', l=1)
        if length== 42 or length==32:
            d.add(e.solid_line, color='blue', d='up', l=1)
            d.add(e.dashed_line, color='blue', d='up', l=2)
            d.add(e.solid_line, color='blue', d='up', l=1)
            d.add(e.dashed_line, color='blue', d='up', l=2)
            if length!=32 and length==42:
                d.add(e.solid_line, color='blue', d='up', l=1)
                d.add(e.solid_line, color='blue', d='up', l=1)
                d.add(e.dashed_line, color='blue', d='up', l=2)
                d.add(e.solid_line, color='blue', d='up', l=1)
                d.add(e.dashed_line, color='blue', d='up', l=2)
                d.add(e.solid_line, color='blue', d='up', l=1)

        d.add(e.solid_line,color='blue', d='right', l=1)
    else:
        d_end = d.add(e.solid_line,color='blue', d='right', xy=end_points.start + [1.5, -12], l=1)
        d.add(e.dashed_line,color='blue', d='right', l=2)
        d.add(e.solid_line,color='blue', d='right', l=1)
        d.add(e.solid_line, color='blue', d='right', l=1)
        d_end=d.add(e.solid_line, color='blue', d='right', l=1)
        d.add(e.LINE,color='blue',xy=d_end.end, d='down', l=2)
        d_end = d.add(e.solid_line,color='blue', d='right', xy=end_points.start + [1.5,-length], l=1)
        d.add(e.dashed_line,color='blue', d='right', l=2)
        d.add(e.solid_line,color='blue', d='right', l=1)
        d.add(e.solid_line, color='blue', d='right', l=1)
        d_end=d.add(e.solid_line, color='blue', d='right', l=1)
        d.add(e.LINE,color='blue', d='up',xy=d_end.end, l=2)

    d_source = d.add(e.Round_Circle, d='down', xy=end_points.start + [-0.8, -0.2])
    d_label = d.add(e.LABEL, xy=end_points.start + [0.59,-2.3],
                    smlltlabel="HEPR 90º 1,0kV" + "\n" + "Eletroduto" + "\n" + "galvanizado Ø ")
    d_end = d.add(e.LABEL, color='green', xy=d_label.end - [-2, 0.0689],
                  smlltlabel="????")
    d.add(e.LABEL, color='red', xy=d_label.start - [0.5, -1.8],
          smlltlabel="String" + str(dict_lenght) + "- MPPt " + str(dict_lenght))

    d_source = d.add(e.CAP_2P, d='down', xy=d_source.start + [0.59, -0.8], theta=180)
    d_label = d.add(e.LABEL, xy=d_source.start + [-1.5,-0.35],
                    smlltlabel="4mm2")
    d_label = d.add(e.LABEL, xy=d_source.start + [-1.5,0.3],
                    smlltlabel=str(dict_lenght))
    d_label = d.add(e.LABEL, xy=d_source.start + [-1, 0.3],
                    smlltlabel="-"+str(dict_lenght))
    dot = d.add(e.DOT, xy=end_points.start + [0, -13.9])
    d_joints = d.add(e.LINE, d='left', l=0.5)
    resistor = d.add(e.RBOX_Testing, d='down', xy=d_joints.end)
    resistor_labels = d.add(e.LABEL,xy=d_joints.end+[0.6,0.59],
                     botlabel='PS 175V 20kA' + '\n' + 'Inominal = 10kA' + '\n' + 'Imáxima = 20kA' + '\n' + 'Classe 2')
    d_junction = d.add(e.LINE, theta=35, xy=resistor.start - [0.39, 1.5], l=0.8)
    d.add(e.LINE, d='down', xy=d_junction.start, l=0.2)
    # resistor_line=d.add(e.LINE, d='down',xy=resistor.end, l=1 * l1)
    battery_end = d.add(e.Round_Circle, d='down', xy=resistor.end + [-0.9, 0.95])
    battery_end_label = d.add(e.LABEL,xy=resistor.end + [-1.39, 0.85], toplabel='#6mm²PVC'+"\n"+'70º750V')
    # d_resistor_final =d.add(e.LINE, d='down',xy=resistor_line.end, l=0.5)
    d.add(e.GND, d='right', xy=battery_end.start + [0.9, -0.69])
    d.add(e.LINE,d='left',xy=battery_end.end+[0.52,-0.8],l=1.5)
    d.add(e.VSS,d='left',xy=battery_end.end+[0,-0.83])
    resistor_line = d.add(e.LINE, d='down', xy=dot.end, l=1 * l1)
    dot = d.add(e.DOT, d='down')
    d_joint = d.add(e.LINE, d='down', l=0.8)
    d.add(e.LABEL, xy=d_joint.end + [-1.2, 1.8], rgtlabel='1000V' + '\n' + '40kA')
    resistor_line = d.add(e.LABEL, xy=d_joint.end + [-1.3, -1.2],
                          rgtlabel='1000V' + '\n' + '25A')
    resistor_line = d.add(e.LINE, d='down',theta=-139, xy=d_joint.end + [0, 0],l=0.8)
    d.add(e.LINE,theta=-200,xy=resistor_line.end+[0.35,0.35],l=0.3)
    d.add(e.LINE,theta=-200,xy=resistor_line.end+[0.138,0.2],l=0.3)
    dashed = d.add(e.dashed_line, d='down', xy=d_joint.end, l=0.5)
    d.add(e.ARC_Circle)
    return {'d_joints':d_joints,'dashed':dashed}


def draw_two(first_key,end_points,last_key,length,dict_lenght,d):
    if last_key != end_points and first_key != end_points:
        d_end = d.add(e.solid_line,color='blue', d='right', xy=end_points.start + [-1.5, -12], l=1)
        d.add(e.dashed_line,color='blue', d='right', l=3)
        d.add(e.solid_line,color='blue', d='right', l=0.5)
        d.add(e.dashed_line,color='blue', d='right', l=3)
        d.add(e.solid_line, color='blue',d='right', l=0.5)
        d_end = d.add(e.solid_line,color='blue', d='right', xy=end_points.start + [-1.5, -length], l=1)
    elif first_key == end_points and first_key != last_key:
        d_end = d.add(e.solid_line,color='blue', d='right', xy=end_points.start + [-3, -12], l=2)
        d.add(e.dashed_line,color='blue', d='right', l=3)
        d.add(e.solid_line,color='blue', d='right', l=0.5)
        d.add(e.dashed_line,color='blue', d='right', l=3)
        d.add(e.solid_line,color='blue', d='right', l=0.5)
        d.add(e.dashed_line,color='blue', d='right', l=3)
        d.add(e.solid_line,color='blue', d='right', l=0.5)
        d.add(e.LINE,color='blue', d='down', xy=d_end.start, l=2)
        d_end = d.add(e.solid_line,color='blue', d='right', xy=end_points.start + [-3, -length], l=2)
        d.add(e.dashed_line,color='blue', d='right', l=3)
        d.add(e.solid_line,color='blue', d='right', l=0.5)
        d.add(e.dashed_line,color='blue', d='right', l=3)
        d.add(e.solid_line, color='blue',d='right', l=0.5)
        d.add(e.dashed_line,color='blue', d='right', l=3)
        d.add(e.solid_line,color='blue', d='right', l=0.5)
        d.add(e.LINE,color='blue', d='up', xy=d_end.start, l=2)
    elif first_key == end_points and first_key == last_key and MPPTin == 1:
        d_end = d.add(e.solid_line,color='blue', d='right', xy=end_points.start + [-3, -12], l=1)
        d.add(e.dashed_line,color='blue', d='right', l=2)
        d.add(e.solid_line, color='blue',d='right', l=1)
        d.add(e.dashed_line,color='blue', d='right', l=2)
        d.add(e.solid_line, color='blue',d='right', l=1)
        d.add(e.dashed_line,color='blue', d='right', l=2)
        d.add(e.solid_line,color='blue', d='right', l=1)
        d.add(e.solid_line,color='blue', d='down', l=1)
        d.add(e.dashed_line,color='blue', d='down', l=2)
        d.add(e.solid_line,color='blue', d='down', l=1)
        d.add(e.dashed_line,color='blue', d='down', l=2)
        d.add(e.solid_line, color='blue',d='down', l=1)
        d.add(e.solid_line,color='blue', d='left', l=1)
        d.add(e.dashed_line,color='blue', d='left', l=2)
        d.add(e.solid_line, color='blue',d='left', l=1)
        d.add(e.dashed_line,color='blue', d='left', l=2)
        d.add(e.solid_line, color='blue',d='left', l=1)
        d.add(e.dashed_line,color='blue', d='left', l=2)
        d.add(e.solid_line, color='blue',d='left', l=1)
        d.add(e.dashed_line,color='blue', d='left', l=2)
        d.add(e.solid_line,color='blue', d='left', l=1)
        d.add(e.solid_line,color='blue', d='up', l=1)
        d.add(e.dashed_line,color='blue', d='up', l=2)
        d.add(e.solid_line, color='blue',d='up', l=1)
        d.add(e.dashed_line,color='blue', d='up', l=2)
        d.add(e.solid_line, color='blue',d='up', l=1)
        d.add(e.solid_line,color='blue', d='right', l=1)
    else:
        d_end = d.add(e.solid_line,color='blue', d='right', xy=end_points.start + [1.5, -12], l=1)
        d.add(e.dashed_line,color='blue', d='right', l=2)
        d.add(e.solid_line,color='blue', d='right', l=1)
        d.add(e.solid_line, color='blue', d='right', l=1)
        d.add(e.LINE,color='blue', d='down', l=2)
        d_end = d.add(e.solid_line,color='blue', d='right', xy=end_points.start + [1.5, -length], l=1)
        d.add(e.dashed_line,color='blue', d='right', l=2)
        d.add(e.solid_line,color='blue', d='right', l=1)
        d_end=d.add(e.solid_line, color='blue', d='right', l=1)
        d.add(e.LINE,color='blue', d='up', xy=d_end.end, l=2)
    d_source = d.add(e.Round_Circle, d='down', xy=end_points.start + [-0.8, -0.2])
    d_label = d.add(e.LABEL,  xy=end_points.start +[1.2, -2.3],
                        smlltlabel="HEPR 90º 1,0kV" + "\n" + "Eletroduto" + "\n" + "galvanizado Ø ")
    d_end = d.add(e.LABEL, color='green', xy=d_label.end - [-2, 0],
                  smlltlabel="????")
    d.add(e.LABEL, color='red', xy=d_label.start - [0.5, -1.8],
          smlltlabel="String" + str(dict_lenght) + "- MPPt " + str(dict_lenght))

    d_source = d.add(e.CAP_2P, d='down', xy=d_source.start + [0.59, -0.8], theta=180)
    d_label = d.add(e.LABEL, xy=d_source.start + [-1.3,-0.35],
                    smlltlabel="4mm2")
    d_label = d.add(e.LABEL, xy=d_source.start + [-1.5,0.3],
                    smlltlabel=str(dict_lenght))
    d_label = d.add(e.LABEL, xy=d_source.start + [-1, 0.3],
                    smlltlabel="-"+str(dict_lenght))
    dot = d.add(e.DOT, xy=end_points.start + [1, -13.5])
    d_joints = d.add(e.LINE, d='left', l=0.5)
    resistor = d.add(e.RBOX_Testing, d='down', xy=d_joints.end)
    resistor_labels = d.add(e.LABEL, xy=d_joints.end + [0.6, 0.59],
                            botlabel='PS 175V 20kA' + '\n' + 'Inominal = 10kA' + '\n' + 'Imáxima = 20kA' + '\n' + 'Classe 2')
    d_junction = d.add(e.LINE, theta=35, xy=resistor.start - [0.39, 1.5], l=0.8)
    d.add(e.LINE, d='down', xy=d_junction.start, l=0.2)
    # resistor_line=d.add(e.LINE, d='down',xy=resistor.end, l=1 * l1)
    battery_end = d.add(e.Round_Circle, d='down', xy=resistor.end + [-0.9, 0.95])
    battery_end_label = d.add(e.LABEL, xy=resistor.end + [-1.39, 0.85], toplabel='#6mm²PVC' + "\n" + '70º750V')
    # d_resistor_final =d.add(e.LINE, d='down',xy=resistor_line.end, l=0.5)
    d.add(e.GND, d='right', xy=battery_end.start + [0.9, -0.69])
    d.add(e.LINE, d='left', xy=battery_end.end + [0.52, -0.8], l=1.5)
    d.add(e.VSS, d='left', xy=battery_end.end + [0, -0.83])
    resistor_line = d.add(e.LINE, d='down', xy=dot.end, l=1 * l1)
    dot = d.add(e.DOT, d='down')
    d_joint = d.add(e.LINE, d='down', l=1)
    d.add(e.LABEL, xy=d_joint.end + [-1.2, 1.8], rgtlabel='1000V' + '\n' + '40kA')
    resistor_line = d.add(e.LABEL, xy=d_joint.end + [-1.3, -1.2],
                          rgtlabel='1000V' + '\n' + '25A')
    resistor_line = d.add(e.LINE, d='down', theta=-130, xy=dot.end + [0, -1], l=0.8)
    d.add(e.LINE, theta=-200, xy=resistor_line.end + [0.32, 0.33], l=0.3)
    ds=d.add(e.LINE, theta=-200, xy=resistor_line.end + [0.138, 0.2], l=0.3)
    dashed = d.add(e.dashed_line, d='down', xy=d_joint.end, l=0.35)
    d.add(e.ARC_Circle,xy=ds.end+[0.69,-0.339])
    return {'d_joints':d_joints,'dashed':dashed}


def draw_diagram_brazilian(end_points,data_raw,frequency,counts,end_point,inv,inv_start,count_list,MPPTin,maximum,diff,total_mpptin,total_dict_length,Mp,d):
    if len(data_raw) >= 1 and len(end_point) >= 1 and len(frequency.keys()) == 1 and MPPTin > 1 or (
            len(frequency.keys()) > 1 and len(frequency.keys()) != len(count_list)):
        global max_y_limit,maximum_y,difference,mpp,total_length
        max_y_limit=total_mpptin
        maximum_y = maximum
        difference = diff
        mpp=Mp
        total_length=total_dict_length

        if MPPTin <= 20:
            x = -7.2
            lengths = 19
            l5 = 18
            x_increase = 0.001
            length = 25
        elif MPPTin > 20 and MPPTin <= 60:
            x = -12
            lengths = 26
            l5 = 25
            x_increase = 0.001
            length = 32
        elif MPPTin > 60 and MPPTin <= 150:
            x = -30
            lengths = 48
            l5 = 47
            x_increase = 0.000000001
            length = 63.5
        else:
            x = ((-12.5) * diff) - 30
            lengths = (26 * diff) + 48
            l5 = lengths - 1
            length = (diff * 22.9) + 63.5
            x_increase = 0.00000000001

        y = 0
        end_points = end_points
        dict_lenght = 0
        p = 0.6
        count = 0
        dict_lenght = 0
        for end in end_point:
            first_key = end_point[0]
            last_key = end_point[len(end_point) - 1]
            dict_lenght += 1
            if len(data_raw) >= 1 and (len(data_raw) != 4 or len(end_point) != 2) and len(end_point) >= 1 and (
                    count != len(data_raw)):
                d_joints = draw(first_key, end, last_key,MPPTin,length,dict_lenght,len(data_raw), d)
                if dict_lenght == len(end_point):
                    d.add(e.LABEL, color='red', xy=end.start + [6, -1], titlabel='NOTA:')
                    d.add(e.LABEL, color='red', xy=end.start + [12.8, -2.5],
                          titlabel=" 1 - O terra do sistema fotovoltaico sera conectado ao do terra ")
                    d.add(e.LABEL, color='red', xy=end.start + [7.5, -3.5],
                          titlabel="padrão de entrada.")
                if dict_lenght != len(end_point):
                    arrow_end = d.add(e.ARROWLINE, d='down', xy=end.start + [-0.5, -2.3])
                    d.add(e.LINE,d='right',xy=arrow_end.end+[0.8,-0.6],l=4.75)
                if dict_lenght<len(end_point)-1:
                    d.add(e.LINE, d='right', xy=end_point[dict_lenght-1].end + [0, -2.9],
                          tox=end_point[dict_lenght + 1].start + [-0.3, -1])
                if dict_lenght != 1:
                    arr = d.add(e.ARROWLINE, d='right', theta=90, xy=end.start + [0.5, -3.5])
                    label_end = d.add(e.LABEL, xy=arr.start + [-2.5, 0.9],
                                      smlltlabel="o Mesmo Eletrodut")
                if len(end_point)!=2 and dict_lenght==len(end_point)-1:
                    d_end_label = d.add(e.LABEL, color='green', xy=end_point[len(end_point) - 1].start - [-6.9, 13],
                                        smlltlabel="??????")
                    d_end = d.add(e.LABEL, xy=d_end_label.end - [0, -0.3],
                                  smlltlabel="STRING BOX")
                d.add(e.LINE, d='down', xy=end.start, toy=d_joints['d_joints'].end, l=13.9)
                d_point = d.add(e.LINE, xy=d_joints['dashed'].end, l=lengths - 13)
                if dict_lenght != len(end_point):
                    point = d.add(e.LINE, theta=0, xy=d_point.end, tox=inv_start.end + [x, y])
                    d.add(e.LINE, d='down', xy=point.end, toy=inv.start - p)
            lengths -= 0.2
            l5 -= 0.2;
            x += 0.2
            y -=1
            p += x_increase
    else:
        count = 0
        max_y_limit=total_mpptin
        maximum_y = maximum
        difference = diff
        mpp=Mp
        total_length=total_dict_length

        if MPPTin <= 20:
            x = -7.2
            lengths = 19
            l5 = 18
            x_increase = 0.001
            length = 25
        elif MPPTin > 20 and MPPTin <= 60:
            x = -12
            lengths = 26
            l5 = 25
            x_increase = 0.001
            length = 32
        elif MPPTin > 60 and MPPTin <= 150:
            x = -30
            lengths = 48
            l5 = 47
            x_increase = 0.000000001
            length = 63
        else:
            x = ((-12.5) * diff) - 30
            lengths = (26 * diff) + 48
            l5 = lengths - 1
            length = (diff * 22.9) + 63.5
            x_increase = 0.000000001
        y = 0
        end_points = end_points
        dict_lenght = 0
        p = 0.6
        for end in end_points:
            first_key = list(end_points.values())[0]
            last_key = list(end_points.values())[len(end_points) - 1]
            dict_lenght += 1
            if len(data_raw) > 2 and (len(data_raw) != 4 or len(end_point) != 2) and len(end_point) >= 2 and (
                    count != len(data_raw)):
                d_joints = draw(first_key, end_points[end], last_key, MPPTin,length,dict_lenght,len(data_raw),d)
                if dict_lenght == len(end_point) :
                    d.add(e.LABEL, color='red', xy=end.start + [6, -1], titlabel='NOTA:')
                    d.add(e.LABEL, color='red', xy=end.start + [12.8, -2.5],
                          titlabel=" 1 - O terra do sistema fotovoltaico sera conectado ao do terra ")
                    d.add(e.LABEL, color='red', xy=end.start + [7.5, -3.5],
                          titlabel="padrão de entrada.")
                if dict_lenght != len(end_points):
                    arrow_end = d.add(e.ARROWLINE, d='down', xy=end_points[end].start + [-0.5, -2.3])
                    d.add(e.LINE, d='right', xy=arrow_end.end + [0.8, -0.6], l=4.75)
                if dict_lenght < len(end_points) - 1:
                    d.add(e.LINE, d='right', xy=list(end_points.values())[dict_lenght-1].end + [0, -2.9],
                          tox=list(end_points.values())[dict_lenght+1].start + [-0.2, -1])
                if dict_lenght != 1:
                    arr = d.add(e.ARROWLINE, d='right', theta=90, xy=end_points[end].start + [0.5, -3.5])
                    label_end = d.add(e.LABEL, xy=arr.start + [-2.2, 0.9],
                                      smlltlabel="o Mesmo Eletrodut")
                d_end_label = d.add(e.LABEL,color='green', xy=end_point[len(end_point) - 1].start - [-5, 13],
                              smlltlabel="??????")
                d_end = d.add(e.LABEL, xy=d_end_label.end - [0, -0.3],
                              smlltlabel="STRING BOX")
                if frequency[end] > 1:
                    d.add(e.LINE, d='down', xy=end_points[end].start, toy=d_joints['d_joints'].end, l=13.9)
                    d_point = d.add(e.LINE, xy=d_joints['dashed'].end, l=lengths - 13)
                else:
                    d.add(e.LINE, d='down', xy=end_points[end].start, toy=d_joints['d_joints'].end, l=13.9)
                    d_point = d.add(e.LINE, xy=d_joints['dashed'].end, l=l5 - 13)
                if dict_lenght != len(end_points):
                    point = d.add(e.LINE, theta=0, xy=d_point.end, tox=inv_start.end + [x, y])
                    d.add(e.LINE, d='down', xy=point.end, toy=inv.start - p)

            lengths -= 0.2
            l5 -= 0.2;
            x += 0.2
            y -= 1
            p += x_increase


def brazilian_version(inv_list,inv_count,d):
    for inv in inv_list:
        if inv_count !=1:
             break
        if inv_count == 1:
            # d.add(e.LINE, d='right', xy=inv_list[0].end + [9.99, -1.2], l=0.5 * l1)
            # d_end = d.add(e.LINE, d='down', l=0.2)
            # d.add(e.SOURCE_Testing99, d='down', xy=d_end.start, smlltlabel='#6mm² PVC 70º 750V')
            # d_final = d.add(e.LINE, d='down', xy=d_end.end, l=0.5)
            # d.add(e.BAT_CELL, d='down', xy=d_final.end)
            d_s = d.add(e.LINE, d='left', xy=inv.start + [0, -2.3], l=5 * l1)
            d_circle = d.add(e.Round_Circle, d='down', xy=d_s.start + [-9.5, 0.75])
            d_circle_label = d.add(e.LABEL, xy=d_s.start + [-8, 0.5])
            d.add(e.LINE, d='down', xy=d_circle.end + [0.9, -0.92], l=1.2)
            d_finals = d.add(e.LINE, d='right', l=0.5)
            d.add(e.SOURCE_Testing120, d='down', xy=d_finals.end + [0, 0.5])
            d.add(e.LABEL, xy=d_finals.end + [0.5, -0.59],
                  smlrtlabel='2#6mm² (F1+F2) + 6mm² PE' + '\n' + 'VC 70º 750V' + '\n' + 'Eletroduto galvanizado Ø ??')
            dot_point = d.add(e.DOT, xy=d_s.end)
            dot_point_label = d.add(e.LABEL, xy=d_s.end + [-1,0],
                                    smlltlabel='Corrente de saida maxima do inversor 1 - 26,1A')
            dot_lined = d.add(e.LINE, d='right', xy=dot_point.end + [0, 0.25], l=0.8)
            dot_line_arrow = d.add(e.ARROWLINE, xy=dot_point.end + [-0.65, 0.85], theta=-90)
            d.add(e.LINE, d='down', xy=d_s.end, l=3.9 * l1)
            d_joint = d.add(e.DOT)
            d_resistor = d.add(e.LINE, d='left', xy=d_joint.end, l=2.2 * l1)
            resistor_label = d.add(e.LABEL, xy=d_resistor.end + [3.99, -0.25],
                                   smlrtlabel='DPS 175V 20kA' + '\n' + 'Inominal = 10kA' + '\n' + 'Imáxima = 20kA Classe 2')
            resistor = d.add(e.RBOX_Testing, d='down', xy=d_resistor.end)
            d_junction=d.add(e.LINE,theta=35,xy=d_resistor.start-[4.65,1.35],l=0.8)
            d.add(e.LINE,d='down',xy=d_junction.start,l=0.2)
            # resistor_line=d.add(e.LINE, d='down',xy=resistor.end, l=1 * l1)
            battery_end=d.add(e.Round_Circle, d='down',xy=resistor.end+[-0.9,0.95])
            battery_end_label = d.add(e.LABEL,xy=resistor.end + [1.3, -0.69], toplabel='#6mm²PVC 70º 750V')
            # d_resistor_final =d.add(e.LINE, d='down',xy=resistor_line.end, l=0.5)
            d.add(e.GND, d='right', xy=battery_end.start+[0.9,-0.69])
            d.add(e.LINE, d='left', xy=battery_end.end + [0.55, -0.8], l=1.5)
            d.add(e.VSS, d='left', xy=battery_end.end + [0, -0.83])
            T_end = d_resistor = d.add(e.LABEL, xy=d_joint.end + [6.69, 0.2], theta=135,
                                       botlabel='FORNECIMENTO' + '\n' + ' EM TENSÃO SECUNDÁRIA')
            label_heading = d.add(e.LABEL, xy=T_end.end + [9, 3.5],
                          botlabel="DIAGRAMA DE BLOCOS DO GERADOR FOTOVOLTAICO"+"\n\n" + "A INSTALAR" )
            d.add(e.LINE,d='right',xy=label_heading.end+[-4.5,-0.38],l=9)
            T_end = d_resistor = d.add(e.VSS, d='down', xy=d_joint.end + [7.2, 0])
            d_resistors = d.add(e.LINE, d='right', xy=T_end.end + [0.3, 0.19], l=0.3* l1)
            d_measu = d.add(e.Measurement, d='right',xy=d_resistors.end+[0,0], botlabel='MEDICAO')
            d_resistor = d.add(e.LINE, d='right', xy=d_measu.end + [0, 0], l=0.48 * l1)
            label = d.add(e.LABEL, xy=d_measu.end + [6, -3], botlabel='PROTEÇÕES INTEGRADAS AO INVERSOR')
            d.add(e.LINE, d='right', xy=label.end + [-3.5, -0.38], l=6.99)
            label = d.add(e.LABEL, xy=label.end + [0, -1],
                          botlabel='Anti-Ilhamento' + '\n' + 'Sobretensão de fase (59)' + '\n' + 'Subtensão de fase (27)' + '\n' + 'Sincronismo(25)' + '\n' + 'Sobrefrequência e Subfrequência (81O/U)' + '\n' + 'have Seccionadora DC')
            d_stringbox = d.add(e.StringBox, d='down', xy=d_measu.end + [2.45, 1.2], rgtlabel='StringBoxCa')
            d_resistor = d.add(e.LINE, d='right', xy=d_stringbox.end + [1.49, 1.2], l=0.3 * l1)
            d_inversor = d.add(e.INVERSOR, xy=d_stringbox.end + [5.09, 2.39], d='down', rgtlabel='Inversor')
            d_inv_line = d.add(e.LINE, d='right', xy=d_inversor.end + [3, 1.2], l=0.5 * l1)
            d_inv = d.add(e.Power_generation, xy=d_inv_line.end + [1.86, 0.8], d='down',
                          rgtlabel='GERAÇÃO DE ENERGI')
            d_bipolarstart = d.add(e.LINE, d='down', xy=d_joint.end, l=0.5 * l1)
            d.add(e.DOT)
            d_bipolarend = d.add(e.LABEL, xy=d_bipolarstart.end + [0.2, -0.3],
                                 smlrtlabel='32A' + '\n' + 'Biploar')
            d_bipolarends = d.add(e.Bi_Polar2, d='down', theta=180, xy=d_bipolarstart.end + [0, -0.2])
            d_lined = d.add(e.LINE,d='down', xy=d_bipolarend.end + [-0.5, 4.2], l=0.9)
            d_line_arrow = d.add(e.ARROWLINE, d='right', xy=d_bipolarend.end + [-1.09, 2.5])
            d.add(e.DOT, xy=d_bipolarends.end + [0, -1.2])
            d.add(e.LINE, d='right', xy=d_bipolarends.end + [-0.9, -0.4], l=1)
            d.add(e.LINE, d='right', xy=d_bipolarends.end + [-0.9, -0.6], l=1)
            d.add(e.LINE, d='right', xy=d_bipolarends.end + [-0.9, -0.8], l=1)
            d_bipolar = d.add(e.LINE, d='down', xy=d_bipolarend.end + [-0.2, -1.2], l=0.5 * l1)
            # d_joint2 = d.add(e.DOT)
            d_end = d.add(e.LINE, d='down', l=2)
            d_source = d.add(e.Round_Circle, d='down', xy=d_end.start + [-0.9, -3.5])
            d_source_label = d.add(e.LABEL,xy=d_end.start + [-0.9, -3.5],
                             toplabel="2#6mm² (F1) + 6mm² PE"+"\n"+"PVC 70º 750V"+"\n"+"Eletroduto galvanizado Ø ??")
            # d_final = d.add(e.LINE, d='right', xy=d_source.end, l=0.5)
            d.add(e.SOURCE_Testing150, d='down', xy=d_source.end + [1.29, -0.29])
            d_final = d.add(e.LINE, d='down', xy=d_end.end, l=1.5)
            d_final = d.add(e.LINE, d='down', xy=d_final.end + [-0, 0], l=2.5)
            d_joint2 = d.add(e.DOT)
            d.add(e.LABEL,xy=d_joint2.end+[-1,0.6],
                             smlltlabel='Conexão através do conector' + '\n' + 'perfurante de derivação (CDP)')
            dot_lined = d.add(e.LINE, d='right',theta=120, xy=d_joint2.end + [-0.6, 0.1], l=0.8)
            dot_line_arrow = d.add(e.ARROWLINE, d='right',xy=d_joint2.end + [-1, 0.8], theta=-112)
            d_final = d.add(e.LINE, d='down', xy=d_joint2.end, l=1.5)
            d_source = d.add(e.SOURCE_Testing, d='down')
            d_source_label = d.add(e.LABEL, color='green', xy=d_source.end + [-6.9, 0.3],
                                         smlrtlabel='3??(??)')
            d.add(e.LABEL,xy=d_source_label.end+[0.99,-0.55],smlltlabel="mm² F1+F2+F3+(N) + ??mm² PE"+"\n"+"PVC 70º750")
            d_final = d.add(e.LINE, d='down', xy=d_source.end + [-1.32, 0], l=1.5)
            d.add(e.DOT)
            d_tripolarsend_label=d.add(e.LABEL, color='green', xy=d_final.end + [0.2, 0],
                  smlrtlabel='??')
            d.add(e.LABEL, xy=d_tripolarsend_label.end + [0.2, 0],
                                     smlrtlabel='Tripolar' + '\n' + 'A')
            d_tripolarend = d.add(e.Bi_Polar2, d='down', theta=180, xy=d_final.end + [0, -0.2])
            d.add(e.DOT, xy=d_tripolarend.end + [0, -1.2])
            d.add(e.LINE, d='right', xy=d_tripolarend.end + [-0.9, -0.39], l=1)
            d.add(e.LINE, d='right', xy=d_tripolarend.end + [-0.9, -0.6], l=1)
            d.add(e.LINE, d='right', xy=d_tripolarend.end + [-0.9, -0.8], l=1)
            d_bipolar = d.add(e.LINE, d='down', xy=d_tripolarend.end + [0, -1.2], l=1.5 * l1)
            d_tripolarlabel = d.add(e.LABEL, xy=d_bipolar.end + [0.65, -1],
                                    smlltlabel='MEDIÇÃO' + '\n' + 'BIDIRECIONAL' + '\n' + ' A INSTALAR')
            d_tripolarend = d.add(e.KWxh, d='down', xy=d_bipolar.end + [0, 0])
            d_tripolarsend_label = d.add(e.LABEL, xy=d_tripolarend.end + [-12.5, -0.5],
                                         smlltlabel='No padrão de entrada do consumidor' + '\n' + 'será instalada uma' + '\n' + ' placa de sinalização')
            d_tripolarsend_label = d.add(e.LABEL, xy=d_tripolarend.end + [-5, 3],
                                         smlrtlabel='PADRÃO DE ENTRADA')
            d_tripolarsend = d.add(e.Entry_Pattern,color='yellow', d='down', xy=d_tripolarend.end + [-3.5, 2])
            d.add(e.rectangle_base,color='black',d='down',xy=d_tripolarsend.end+[-4.3,2])
            d.add(e.LABEL,color='yellow', xy=d_tripolarend.end + [-4.5, 1.5],l=5,
                                         smlrtlabel='CUIDADO')
            d.add(e.LABEL, xy=d_tripolarend.end + [-0.9, 2.5],
                                         smlrtlabel='**')
            d.add(e.ARROWLINE_Testing,xy=d_tripolarend.end+[-6,2.5])
            d.add(e.LABEL,xy=d_tripolarend.end+[-3.5,2.2],smlltlabel='250')
            d.add(e.ARROWLINE_Testing,d='up', xy=d_tripolarend.end + [-1, 1.89])
            d.add(e.LINE, d='down', xy=d_tripolarend.end + [-1.3, 2.2], l=1.9)
            d.add(e.LABEL, xy=d_tripolarend.end + [-1.5, 0], smlltlabel='200')
            d.add(e.ARROWLINE_Testing,d='left', xy=d_tripolarend.end + [-1, 2.69])
            label=d.add(e.ARROWLINE_Testing, d='right', xy=d_tripolarend.end + [-1.59, -2.5])
            d.add(e.LINE, d='up', xy=label.end+[0.3,0.5], l=1.9)
            d.add(e.LINE, d='right', xy=d_tripolarend.end + [-5.2, 2.2], l=3.5)
            d_final = d.add(e.LINE, d='right', xy=d_tripolarend.end + [0.66, 0.8], l=1.5)
            battery_end=d.add(e.SOURCE_Testing99, d='down')
            battery_end_label = d.add(e.LABEL, color='green', xy=battery_end.end + [0.3, -0.5],
                                   smlltlabel="#??")
            d.add(e.LABEL,xy=battery_end_label.end+[0.6,0.3],smlrtlabel="mm2"+"\n"+"Cobre Nu")
            d.add(e.GND, d='right', xy=battery_end.start + [0, -0.5])
            d_final = d.add(e.LINE, d='down', xy=d_tripolarend.end, l=4)
            d_source = d.add(e.SOURCE_Testing19, d='down')
            # d_source_label = d.add(e.LABEL,xy=d_source.end+[-5,0],
            #                  smlltlabel="#mm² F1+F2+F3+(N)"+"\n"+"PVC 70º 750")
            d_source_label=d.add(e.LABEL,color='green', xy=d_source.end + [-5,0],
                                   smlltlabel="3#??(??))")
            d.add(e.LABEL,xy=d_source_label.end+[1.3,-0.3],
                             smlltlabel="mm² F1+F2+F3+(N)"+"\n"+"PVC 70º 750")
            # d.add(e.LINE, d='down', xy=d_final.end + [0.3, 0],l=0.5)
            d_final = d.add(e.LINE, d='down', xy=d_source.end + [-0.8, 0], l=1.5)
            d_arrow = d.add(e.ARROWLINE, d='right', xy=d_final.end + [-0.55, -0.75],
                            botlabel='Rede Secundária de Distribuição (BT)')
            d_arrow_right = d.add(e.LINE, d='right', xy=d_arrow.end + [0.3, 1.2], l=5 * l1)
            d.add(e.LINE, d='left', xy=d_arrow.end + [0.3, 1.2], l=5 * l1,
                  toplabel='RESIDENCIAL - TRIFÁSICO 127/220V',
                  botlabel='N° Poste: Sem ID' + '\n' + 'N° Trafo mais Próximo: Sem ID')
            d_arrow = d.add(e.ARROWLINE, d='right', xy=d_arrow_right.end + [-1.2, -0.5])
            d_arrow = d.add(e.LABEL,xy=d_arrow_right.end + [-1.2, 0.42],
                            toplabel='CONCESSIONÁRIA')
            d_arrow = d.add(e.ARROWLINE, d='right', theta=180, xy=d_arrow_right.end + [0, 0.5])
            d_arrow = d.add(e.LABEL,xy=d_arrow_right.end + [-0.63, -0.49],
                            smlltlabel='UC')
            d.add(e.LABEL, color='green', xy=d_arrow_right.end + [-0.2, -0.49],
                            smlltlabel='??????')
            d_line = d.add(e.LINE, d='right', xy=d_joint2.end, l=5.3 * l1)
            d_line2 = d.add(e.LINE, d='down', xy=d_line.end, l=0.5 * l1)
            d_arrow = d.add(e.ARROWLINE, d='right', xy=d_line2.end + [-0.6, -0.8], botlabel='Outras cargas')
            d_end = d.add(e.solid_line,color='blue', d='right', xy=d_line2.end + [-3, 0.5], l=1)
            d.add(e.dashed_line, d='right', l=1)
            d.add(e.solid_line,color='blue', d='right', l=0.5)
            d.add(e.dashed_line, d='right', l=1.5)
            d.add(e.solid_line,color='blue', d='right', l=1)
            d.add(e.dashed_line, d='right', l=1)
            d.add(e.solid_line,color='blue', d='right', l=1)
            d_junction = d.add(e.solid_line,color='blue', d='right', l=1)
            d_junction = d.add(e.solid_line,color='blue', d='down', xy=d_junction.end + [0, 0], l=1)
            d.add(e.dashed_line, d='down', l=1)
            d_junction = d.add(e.solid_line,color='blue', d='down', l=1)
            d_junction = d.add(e.solid_line,color='blue', d='left', xy=d_junction.end + [0, 0], l=1)
            d.add(e.dashed_line, d='left', l=1)
            d.add(e.solid_line,color='blue', d='left', l=1)
            d.add(e.dashed_line, d='left', l=1)
            d.add(e.solid_line,color='blue', d='left', l=1)
            d.add(e.dashed_line, d='left', l=1)
            d.add(e.solid_line,color='blue', d='left', l=1)
            d.add(e.dashed_line, d='left', l=1)
            d.add(e.solid_line,color='blue', d='left', l=1)
            d_junction = d.add(e.solid_line,color='blue', d='left', l=1)
            d_junction = d.add(e.solid_line,color='blue', d='up', xy=d_junction.end + [0, 0], l=1)
            d.add(e.dashed_line,color='blue', d='up', l=1)
            d_junction = d.add(e.solid_line,color='blue', d='up', l=1)
            d_junction = d.add(e.solid_line,color='blue', d='right', xy=d_junction.end + [0, 0], l=1)
            d_end = d.add(e.solid_line,color='blue', d='right', xy=d_tripolarend.end + [-5, 3.3], l=1)
            d.add(e.dashed_line, d='right', l=1)
            d.add(e.solid_line,color='blue', d='right', l=1)
            d.add(e.dashed_line, d='right', l=1)
            d.add(e.solid_line,color='blue', d='right', l=1)
            d.add(e.dashed_line,d='right', l=1)
            d.add(e.solid_line,color='blue', d='right', l=1)
            d.add(e.dashed_line, d='right', l=1)
            d.add(e.solid_line,color='blue', d='right', l=1)
            d_junction = d.add(e.solid_line,color='blue', d='right', l=1)
            d_junction = d.add(e.solid_line,color='blue', d='down', xy=d_junction.end + [0, 0], l=1)
            d.add(e.dashed_line, d='down', l=1)
            d_junction = d.add(e.solid_line,color='blue', d='down', l=1)
            d.add(e.solid_line,color='blue', d='down', l=1)
            d.add(e.dashed_line, d='down', l=1)
            d_junction = d.add(e.solid_line,color='blue', d='down', l=1)
            d_junction = d.add(e.solid_line,color='blue', d='left', xy=d_junction.end + [0, 0], l=1)
            d.add(e.dashed_line, d='left', l=1)
            d.add(e.solid_line,color='blue', d='left', l=1)
            d.add(e.dashed_line, d='left', l=1)
            d.add(e.solid_line,color='blue', d='left', l=1)
            d.add(e.dashed_line, d='left', l=1)
            d.add(e.solid_line,color='blue', d='left', l=1)
            d.add(e.dashed_line, d='left', l=1)
            d.add(e.solid_line,color='blue', d='left', l=1)
            d.add(e.dashed_line, d='left', l=1)
            d.add(e.solid_line,color='blue', d='left', l=1)
            d_junction = d.add(e.solid_line,color='blue', d='left', l=1)
            d_junction = d.add(e.solid_line,color='blue', d='up', xy=d_junction.end + [0, 0], l=1)
            d.add(e.dashed_line,d='up', l=1)
            d.add(e.solid_line,color='blue', d='up', l=1)
            d.add(e.dashed_line, d='up', l=1)
            d.add(e.solid_line,color='blue', d='up', l=1)
            d_junction = d.add(e.solid_line,color='blue', d='up', l=1)
            d_junction = d.add(e.solid_line,color='blue', d='right', xy=d_junction.end + [0, 0], l=1)
            d_end = d.add(e.solid_line,color='blue', d='right', xy=d_bipolarend.end + [-5, 2.5], l=1)
            stringbox_label = d.add(e.LABEL, xy=d_bipolarend.end + [-5, 2.3], botlabel='STRINGBOXCA')
            string_label = d.add(e.LABEL, xy=d_bipolarend.end + [-4.35, 3],
                                 botlabel='Corrente de saida maxima do inversor 26,1A')
            d.add(e.dashed_line, d='right', l=1)
            d.add(e.solid_line,color='blue',d='right', l=1)
            d.add(e.dashed_line ,d='right', l=1)
            d.add(e.solid_line,color='blue', d='right', l=1)
            d.add(e.dashed_line, d='right', l=1)
            d.add(e.solid_line,color='blue', d='right', l=1)
            d_junction = d.add(e.solid_line,color='blue', d='right', l=1)
            d_junction = d.add(e.solid_line,color='blue', d='down', xy=d_junction.end + [0, 0], l=1)
            d.add(e.dashed_line, d='down', l=1)
            d_junction = d.add(e.solid_line,color='blue', d='down', l=1)
            d.add(e.dashed_line, d='down', l=1)
            d.add(e.solid_line,color='blue', d='down', l=1)
            d.add(e.dashed_line, d='down', l=1)
            d_junction = d.add(e.solid_line,color='blue', d='down', l=1)
            d_junction = d.add(e.solid_line,color='blue', d='left', xy=d_junction.end + [0, 0], l=1)
            d.add(e.dashed_line, d='left', l=1)
            d.add(e.solid_line,color='blue', d='left', l=1)
            d.add(e.dashed_line, d='left', l=1)
            d.add(e.solid_line,color='blue', d='left', l=1)
            d.add(e.dashed_line, d='left', l=1)
            d.add(e.solid_line, d='left', l=1)
            d.add(e.dashed_line, d='left', l=1)
            d.add(e.solid_line,color='blue', d='left', l=1)
            d_junction = d.add(e.solid_line,color='blue', d='left', l=1)
            d_junction = d.add(e.solid_line,color='blue', d='up', xy=d_junction.end + [0, 0], l=1)
            d.add(e.dashed_line, d='up', l=1)
            d.add(e.solid_line,color='blue', d='up', l=1)
            d.add(e.dashed_line, d='up', l=1)
            d.add(e.solid_line,color='blue', d='up', l=1)
            d.add(e.dashed_line, d='up', l=1)
            d_junction = d.add(e.solid_line,color='blue', d='up', l=1)
            d_junction = d.add(e.solid_line,color='blue', d='right', xy=d_junction.end + [0, 0], l=1)


def outline_diagram(line,count,end,d):
    if count != len(end):
        arrow_end = d.add(e.ARROWLINE, d='down', xy=line.start + [0.5, -2.3])
        d.add(e.LINE, d='right', xy=arrow_end.end + [0.8, -0.6], l=4.66)
    # if count <len(end)-1:

    if type(end)==list:
        d.add(e.LINE, d='right', xy=end[0].end + [1, -2.9],
                          tox=end[1].start + [-0.6, -1])
    if count != 1:
        if type(end)==list:
            arr = d.add(e.ARROWLINE, d='right', theta=90, xy=line.start + [1.46, -3.5])
            d.add(e.LINE, d='left', xy=arr.end + [-0.5, 0.6], l=2)
            label_end = d.add(e.LABEL, xy=arr.start + [-5, 0.9],
                                      smlltlabel="o Mesmo Eletrodut")
        else:
            arr = d.add(e.ARROWLINE, d='right', theta=90, xy=line.start + [1.46, -3.5])
            label_end = d.add(e.LABEL, xy=arr.start + [-2.2, 0.9],
                              smlltlabel="o Mesmo Eletrodut")


def draw_diagram(end_points,data_raw,frequency,counts,end_point,inv,inv_start,count_list,MPPTin,maximum,diff,total_mpptin,total_dict_length,Mp,d):
    global max_y_limit,max_x_limit,maximum_y,difference,mpp,total_length
    max_y_limit=0
    max_x_limit=total_mpptin
    maximum_y=maximum
    difference=diff
    mpp=Mp
    total_length=total_dict_length


    if MPPTin <= 20:
        x = -7.2
        lengths = 19
        l5 = 18
        x_increase=0.001
    elif MPPTin > 20 and MPPTin <= 60:
        x = -12
        lengths = 26
        l5 = 25
        x_increase=0.001
    elif MPPTin>60 and MPPTin<=150:
        x = -30
        lengths = 33
        l5 = 32
        x_increase=0.0001
    else:
        x=((-12.5)*diff)-30
        lengths=(26*diff)+33
        l5=lengths-1
        x_increase=0.0001
    if len(data_raw) >= 1 and len(end_point) >= 1 and len(frequency.keys()) == 1 and MPPTin > 1 or (
            len(frequency.keys()) > 1 and len(frequency.keys()) != len(count_list)):
        y = 0
        end_points = end_points
        dict_lenght = 0
        p = 0.6
        count = 0
        dict_lenght = 0
        for end in end_point:
            dict_lenght += 1
            if len(data_raw) >= 1 and (len(data_raw) != 4 or len(end_point) != 2) and len(end_point) >= 1 and (
                    count != len(data_raw)):
                d_point = d.add(e.LINE, d='down', xy=end.start, l=lengths)
                if dict_lenght != len(end_point):
                    point = d.add(e.LINE, theta=0, xy=d_point.end, tox=inv_start.end + [x, y])
                    d.add(e.LINE, d='down', xy=point.end, toy=inv.start - p)
            lengths -= 0.2
            l5 -= 0.2;
            x += 0.2
            y -= 1
            p += x_increase
    else:
        count = 0
        y = 0
        end_points = end_points
        dict_lenght = 0
        p = 0.6
        max_y_limit = 0
        max_x_limit=total_mpptin
        maximum_y=maximum
        difference=diff
        mpp = Mp
        total_length = total_dict_length
        if MPPTin <= 20:
            x = -7.2
            lengths = 19
            l5 = 18
            x_increase=0.001
        elif MPPTin > 20 and MPPTin <= 60:
            x = -12
            lengths = 26
            l5 = 25
            x_increase=0.001
        elif MPPTin > 60 and MPPTin <= 150:
            x = -30
            lengths = 33
            l5 = 32
            x_increase=0.0001
        else:
            x = ((-12) * diff) - 30
            lengths = (26 * diff) + 33
            l5 = lengths - 1
            x_increase=0.001
        for end in end_points:
            dict_lenght += 1
            if len(data_raw) > 2 and (len(data_raw) != 4 or len(end_point) != 2) and len(end_point) >= 2 and (
                    count != len(data_raw)):
                if frequency[end] > 1:
                    d_point = d.add(e.LINE, d='down', xy=end_points[end].start, l=lengths)
                    # d.add(e.LINE, theta=0,xy=d_point.end,to=inv_start.end- [0.5,0.1])
                else:
                    d_point = d.add(e.LINE, d='down', xy=end_points[end].start, l=l5)
                if dict_lenght != len(end_points):
                    point = d.add(e.LINE, theta=0, xy=d_point.end, tox=inv_start.end + [x, y])
                    d.add(e.LINE, d='down', xy=point.end, toy=inv.start - p)
            lengths -= 0.2
            l5 -= 0.2
            x += 0.2
            y -= 1
            p += x_increase