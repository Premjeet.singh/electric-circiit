#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul  7 10:23:04 2020

@author: vipluv.aga
"""

import os, json
import pandas as pd
import numpy as np

import datetime as dt

# from flask_sqlalchemy import SQLAlchemy
import sqlalchemy

from sqlalchemy import MetaData, Table, update
from sqlalchemy.sql.expression import bindparam
from sqlalchemy.dialects.mysql import insert
from sqlalchemy.sql import select

if __name__ == '__main__':

    Envvar = {"Environ":
                  {"BUCKET_NAME": "solbucket",
                   "PV_DB_FILENAME": "CECModules07-2020.pkl",
                   "PVS_DB_FILENAME": "PvSystModules2020-06.pkl",
                   "INV_DB_FILENAME": "BigInvCEC07-2020.pkl",
                   "NASA_22YR_FILENAME": "22yr_nasa_monthly.pkl",
                   "USER_UPLOADS_FOLDER": "UserUploads/",
                   "FETCHED_WEATHER_FOLDER": "FetchedWeather/",
                   "AUXFILES_FOLDER": "AuxFiles/",
                   "RESULTS_FOLDER": "Results/",
                   "ELECPARAMS": "elec_design_params.json",
                   "FINPARAMS": "finparameter_inputs.json",
                   "SQLALCHEMY_DATABASE_URI": "mysql+pymysql://roots:default123@/refdata?unix_socket=/cloudsql/solex-mvp-2:europe-west6:component-cost"

                   }}

    for var in Envvar['Environ']:
        os.environ[var] = Envvar['Environ'][var]


# import pdb
# pdb.set_trace();
def init_connection_engine():
    # import pdb
    # pdb.set_trace();
    db_config = {
        # Pool size is the maximum number of permanent connections to keep.
        "pool_size": 5,
        # Temporarily exceeds the set pool_size if no connections are available.
        "max_overflow": 2,
        # The total number of concurrent connections for your application will be
        # a total of pool_size and max_overflow.
        # [END cloud_sql_mysql_sqlalchemy_limit]
        # [START cloud_sql_mysql_sqlalchemy_backoff]
        # SQLAlchemy automatically uses delays between failed connection attempts,
        # but provides no arguments for configuration.
        # [END cloud_sql_mysql_sqlalchemy_backoff]
        # [START cloud_sql_mysql_sqlalchemy_timeout]
        # 'pool_timeout' is the maximum number of seconds to wait when retrieving a
        # new connection from the pool. After the specified amount of time, an
        # exception will be thrown.
        "pool_timeout": 30,  # 30 seconds
        # [END cloud_sql_mysql_sqlalchemy_timeout]
        # [START cloud_sql_mysql_sqlalchemy_lifetime]
        # 'pool_recycle' is the maximum number of seconds a connection can persist.
        # Connections that live longer than the specified amount of time will be
        # reestablished
        "pool_recycle": 1800,  # 30 minutes
        # [END cloud_sql_mysql_sqlalchemy_lifetime]
    }
    if os.environ.get("DB_HOST"):
        return init_tcp_connection_engine(db_config)

    else:
        return init_unix_connection_engine(db_config)


def init_tcp_connection_engine(db_config):
    # import pdb
    # pdb.set_trace();
    SQLALCHEMY_DB_URI = "mysql+pymysql://roots:default123@/refdata?unix_socket=/cloudsql/solex-mvp-2:europe-west6:component-cost"
    if os.environ.get("DB_NAME"):
        db_name = os.environ['DB_NAME']
    else:
        db_name = 'refdata'
    # SQLALCHEMY_DB_URI=SQLALCHEMY_DB_URI.replace('refdata',db_name)
    pool = sqlalchemy.create_engine(
        SQLALCHEMY_DB_URI, **db_config
    )

    return pool


def init_unix_connection_engine(db_config):
    # import pdb
    # pdb.set_trace();
    SQLALCHEMY_DATABASE_URI = "mysql+pymysql://roots:default123@/refdata?unix_socket=/cloudsql/solex-mvp-2:europe-west6:component-cost"
    if os.environ.get("DB_NAME"):
        db_name = os.environ['DB_NAME']
    else:
        db_name = 'refdata'

    # SQLALCHEMY_DATABASE_URI =SQLALCHEMY_DATABASE_URI.replace('refdata',db_name)
    pool = sqlalchemy.create_engine(
        SQLALCHEMY_DATABASE_URI, **db_config
    )

    return pool


db = init_connection_engine()


def readnames():
    # import pdb
    # pdb.set_trace();

    systemdf = pd.read_sql('SYSTEM', db)  # Table or query as first arg

    with db.connect() as conn:
        names = conn.execute(
            "SELECT * from SYSTEM").fetchall()

    return names, systemdf


def AddNames(csvfilename):
    with open(csvfilename, 'r') as f:
        data_df = pd.read_csv(f)
    data_df.to_sql(con=db, name='SYSTEM', if_exists='append', index=False)

    return


def InsertOrUpdate(Dict2Upsert, tablename='PRODUCT_GROUP'):
    # import pdb
    # pdb.set_trace();
    """

    Parameters
    ----------
    entrydf : pandas.DataFrame
        Columns are [ProductName	GroupName	BusinessIdentifierCode	Name	TargetMargin	 PriorityFlag]


    Returns
    -------
    None

    """
    metadata = MetaData()

    with db.connect() as conn:
        PROD = Table(tablename, metadata, autoload=True, autoload_with=conn)
        r = update(PROD).values(ActiveFlag=0)
        conn.execute(r)

        for ent in Dict2Upsert:
            ent['ModifiedDate'] = dt.datetime.now()

            s = select([PROD.c.Id]).where((PROD.c.BusinessIdentifierCode == ent['BusinessIdentifierCode']) & (
                        PROD.c.ProductName == ent['ProductName']))
            # Set all ActiveFlags to 0 for all Ids

            d = conn.execute(s).fetchone()
            if d:
                ent['Id'] = d[0]
            else:
                ent['CreatedDate'] = dt.datetime.now()
            ins = insert(PROD).values(
                ent)
            ins = ins.on_duplicate_key_update(ent)
            conn.execute(ins)

    return


def AddEntry(entrydf):
    """
    Parameters
    ----------
     entrydf: dataframe of the all the entries that are being entered into the table

    Returns:
    -------
     ids : list of Ids for every operation

    """

    connection = db.raw_connection()
    paramnames = entrydf.columns
    paramorder = ["PerfModelKey", "Supplier", "SupplierID",
                  "Name", "DisplayName", "Description", "BusinessIdentifierCode", "SystemType",
                  "Manufacturer", "ManufacturerID",
                  "Category", "SubCategory",
                  "CostFunctionDependentVariable", "CostFunctionDependentVariableDefinition",
                  "CostFunction", "Currency", "Cost", "Unit", "BasePrice", "ImageKey",
                  "ActiveStartDate", "ActiveEndDate", "UploadName",
                  "UploadType", "UnitDependantVariable", "UnitDependantVariableFunction",
                  "ActiveFlag",
                  "Custom1", "Custom2", "Custom3", "Custom4"]
    # CleanUp
    # entrydf['BusinessIdentifierCode'] = entrydf['BusinessIdentifierCode'].astype(str)
    # entrydf['Value'] = entrydf['Value'].astype(float)
    ids = {}
    try:
        cursor = connection.cursor()
        for row in entrydf.index:
            entry = entrydf.loc[row]

            parameterlist_1 = [entry[param] if param in paramnames else None for param in paramorder]
            parameterlist_2 = [p.item() if ((type(p) == np.float64) or (type(p) == np.int64)) else p for p in
                               parameterlist_1]

            cursor.callproc('usp_Add_System', parameterlist_2)
            idreturned = cursor.fetchall()[0][0]
            ids.update({row: idreturned})
        cursor.close()
        connection.commit()
    finally:
        connection.close()

    return ids


def GetEntry(tablename, attribute, searchstring):
    # Fetch all active records that match searchstring for the attribute from the table
    connection = db.raw_connection()
    paramlist = [tablename, attribute, searchstring]

    try:
        cursor = connection.cursor()
        cursor.callproc('usp_DisplayListOfValues', paramlist)
        results = pd.DataFrame(cursor.fetchall(), columns=[k[0] for k in cursor.description])

        cursor.close()
        connection.commit()
    finally:
        connection.close()

    return results


def MakeBoMQuote(quotedf):
    """
    Parameters
    ----------
    quotedf : TYPE  Pandas Dataframe
        This contains all the entries to be added row by row into a quote. Values are taken by selecting from system or accessory tables.
        This call is always preceded by a GetEntry call where the entry is selected and then the typeID is added to a dataframe row by row and
        composed together

    Returns
    -------
    results : list
        List of the items added into the DB as a list of dicts
    """

    connection = db.raw_connection()

    paramnames = quotedf.columns
    paramorder = ["ProjectID", "QuoteID", "RevID", "Group", "Type", "TypeID", "CreatedBy", "PriceAdjustment"]

    try:
        cursor = connection.cursor()
        for row in quotedf.index:
            entry = quotedf.loc[row]
            parameterlist_1 = [entry[param] if param in paramnames else None for param in paramorder]
            parameterlist_2 = [p.item() if ((type(p) == np.float64) or (type(p) == np.int64)) else p for p in
                               parameterlist_1]
            # Project_Id, Quote_ID, Rev_ID, Group, Type (System/Accessory) Type_ID, UserId
            cursor.callproc('usp_Add_BOMQuote', parameterlist_2)  # Add all the values to a quote
            # results = list(cursor.fetchall())
        cursor.close()
        connection.commit()

    finally:
        connection.close()

    return "success"


def GetBoMQuote(ProjectID, QuoteID, RevID, **kwargs):
    import pdb
    pdb.set_trace();

    """

    Parameters
    ----------
    ProjectId : str
        Project id from backed
    QuoteID : str
        quote id for current quote.
    RevID : str
        revision id for different revisions.
    **args : TYPE
        CreateRevision - 'Y' or 'N' 
        CreatedBy - name of user making revisions
        NewRevID - string of new revid to store existing display as by making it active

    Returns
    -------
    pandas dataframe with column headers as columns from SQL
        Dataframe showing values of content in sql db

    """

    connection = db.raw_connection()
    # TODO: Fix error in CreateRevision - selected NewRev is not becoming active
    if "CreateRevision" in kwargs:
        dbproc = 'usp_Display_BOMQuoteRevision_Active'
        paramorder = [ProjectID, QuoteID, RevID, kwargs['CreatedBy'], kwargs['CreateRevision'], kwargs['NewRevId']]

    else:
        dbproc = 'usp_Display_BOMQuoteRevision_Active'
        paramorder = [ProjectID, QuoteID, RevID, '_', 'N', '_']
    try:
        cursor = connection.cursor()
        cursor.callproc(dbproc, paramorder)
        results = pd.DataFrame(cursor.fetchall(), columns=[k[0] for k in cursor.description])
        results = results.drop_duplicates(subset=['Id'])
        cursor.close()
        connection.commit()
    finally:
        connection.close()

    return results


def GetQuotefromtemplate(productname, tablename='PRODUCT_GROUP'):
    connection = db.raw_connection()
    dbproc = 'usp_Display_Product'
    try:
        cursor = connection.cursor()
        cursor.callproc(dbproc, [productname])
        results = pd.DataFrame(cursor.fetchall(), columns=[k[0] for k in cursor.description])
        cursor.close()
        connection.commit()
    finally:
        connection.close()
    return results


def RunRawSQLQuery(querytext):
    with db.connect() as conn:
        try:
            results = conn.execute(querytext).fetchall()
        except:
            results = {}
    return results


def DfFromRawSQLQuery(querytext):
    Df = pd.read_sql(querytext, db)
    # with db.connect() as conn:
    #     try:
    #         results = conn.execute(querytext).fetchall()
    #     except:
    #         results =  {}
    return Df


def UpdateBoM(BoM2Update, tablename='BOM_QUOTE'):
    # BoM2UpdateDict = [{'Id:2,'Quantity',234,'TotalCost':23, "Type":'System'....}, {'Id':.....}, {} ]
    metadata = MetaData()
    # tablename = 'BOM_QUOTE'
    with db.connect() as conn:
        BOM = Table(tablename, metadata, autoload=True, autoload_with=conn)

        # ins = BOM.update(). \
        #     where(BOM.c.Id== bindparam('_Id')).values({'Quantity':bindparam('Quantity'),'TotalCost':bindparam('TotalCost')})

        # Update Anything...
        ins = BOM.update(). \
            where(BOM.c.Id == bindparam('_Id')).values(
            {BomKey: bindparam(BomKey) for BomKey in BoM2Update[0] if BomKey != '_Id'})

        conn.execute(ins, BoM2Update)

    return


def DeleteFromBoM(DelBoM, tablename='BOM_QUOTE'):
    metadata = MetaData()
    # tablename = 'BOM_QUOTE'
    with db.connect() as conn:
        BOM = Table(tablename, metadata, autoload=True, autoload_with=conn)

        # Delete Rows...
        delquer = BOM.delete(). \
            where(BOM.c.Id == bindparam('_Id'))
        conn.execute(delquer, DelBoM)

    return


def InserttoBoM(BoM2Insert, tablename='BOM_QUOTE'):
    metadata = MetaData()

    with db.connect() as conn:
        BOM = Table(tablename, metadata, autoload=True, autoload_with=conn)

        ins = BOM.insert(). \
            values({BomKey: bindparam(BomKey) for BomKey in BoM2Insert[0]})

        conn.execute(ins, BoM2Insert)

    return


def StoreRevtoBoM(ProjectID, QuoteID, RevID, CreatedBy, NewRevID):
    connection = db.raw_connection()

    # paramnames = quotedf.columns
    paramorder = [ProjectID, QuoteID, RevID, CreatedBy, 'Y', NewRevID]

    try:
        cursor = connection.cursor()

        cursor.callproc('usp_CreateBomQuoteRevisionFromExisting', paramorder)  # Add all the values to a quote
        # results = list(cursor.fetchall())
        results = pd.DataFrame(cursor.fetchall(), columns=[k[0] for k in cursor.description])
        cursor.close()
        connection.commit()

    finally:
        connection.close()

    return results


def BoMExcludeFlags(ViewQuoteDf, OrgBomInput={}):
    excludefromBill = OrgBomInput.get('groupsCAPEXexclude', [])  # ['Service','Externe Förderung', 'Option']
    excludefromBC = OrgBomInput.get('groupsBCinclude', [])  # ['Externe Förderung']

    ViewQuoteDf['ExcludeFromCAPEX'] = ViewQuoteDf['Group'].apply(lambda x: 1 if x in excludefromBill else 0)
    ViewQuoteDf['IncludeforBC'] = ViewQuoteDf['Group'].apply(lambda x: 1 if x in excludefromBC else 0)
    return ViewQuoteDf


def ReadProperties(nameofcomponent, properties):
    return


# %%
if __name__ == '__main__':

    Envvar = {"Environ":
                  {"BUCKET_NAME": "solbucket",
                   "PV_DB_FILENAME": "CECModules07-2020.pkl",
                   "PVS_DB_FILENAME": "PvSystModules2020-06.pkl",
                   "INV_DB_FILENAME": "BigInvCEC07-2020.pkl",
                   "NASA_22YR_FILENAME": "22yr_nasa_monthly.pkl",
                   "USER_UPLOADS_FOLDER": "UserUploads/",
                   "FETCHED_WEATHER_FOLDER": "FetchedWeather/",
                   "AUXFILES_FOLDER": "AuxFiles/",
                   "RESULTS_FOLDER": "Results/",
                   "ELECPARAMS": "elec_design_params.json",
                   "FINPARAMS": "finparameter_inputs.json",
                   "SQLALCHEMY_DATABASE_URI": "mysql+pymysql://root:default123@/refdata?unix_socket=/cloudsql/solex-mvp-2:europe-west6:component-cost",
                   "DB_NAME": 'refdata'
                   }}

    for var in Envvar['Environ']:
        os.environ[var] = Envvar['Environ'][var]

    credential_path = r"C:\Users\dell\Downloads\application_default_credentials.json"
    # project=r"solex-mvp-2"
    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = credential_path
    # os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = os.path.join("/Users","vipluv.aga","Code","auth",
    #                                                             "gcloudauthkeys","solex-mvp-2-e18a10831f31.json")
    os.environ['SQLALCHEMY_LOCALDB_URI'] = "mysql+pymysql://roots:default123@127.0.0.1:3306/refdata"
    os.environ['DB_HOST'] = '127.0.0.1'
    os.environ['DB_NAME'] = 'ref_agrola'
    ####
    # parameters for test

    SQLDataDir = "/Users/vipluv.aga/Documents/Solextron/Products/Design/SQLWork/"
    xlsfilename = SQLDataDir + "entryuploadexample_2.xlsx"
    Df = pd.read_excel(xlsfilename)
    with open(SQLDataDir + "entryuploadexample_1.json", 'w') as f:
        Df.to_json(f, orient='split')

    csvfilename = SQLDataDir + "ComponentTableUpload.csv"
    exlf = SQLDataDir + 'ProductComponentGroup.xlsx'
    entrydf = pd.read_excel(exlf)
    #####
    """
    db = init_connection_engine()
    Names,systemdf = readnames()

    print(Names)


    csvfile = SQLDataDir+'ExampleCsvEntry.xlsx'
    entrydf = pd.read_excel(xlsfilename)
    entrydf.fillna(np.nan,inplace=True)
    entrydf.replace([np.nan],[None],inplace=True)
    """
    # read="/Users/vipluv.aga/Documents/Solextron/Customer/Agrola/MasterList/MasterList Rev7a up.xlsx"
    # entrydf=pd.read_excel(read)

    # results = AddEntry(entrydf)

    # %% Read
    tablename = 'SYSTEM'
    attribute = 'Name'
    searchstring = 'ads-tec'

    entresults = GetEntry(tablename, attribute, searchstring)

    # %% Make a Quote
    # First Read inthe values
    quotedf = Df.iloc[[1, 5, 10, 16, 50, 100]]
    parameterdf = pd.DataFrame(columns=["ProjectID", "QuoteID", "RevID", "Group", "Type", "TypeID", "CreatedBy"])

    ProjectID = '47b6df36-23a9-11eb-8b74-0242ac110007'
    QuoteID = "Q1"
    RevID = "R4-Edit"
    Type = "SYSTEM"
    CreatedBy = 'A'

    for row in quotedf.index:
        en = quotedf.loc[row]
        res = GetEntry('SYSTEM', 'Name', en['Name'])
        parameterdf.loc[row, ["Group", "TypeID"]] = res.iloc[0][['SystemType', 'Id']].values
        parameterdf.loc[row, ["ProjectID", "QuoteID", "RevID", "Type", "CreatedBy"]] = [ProjectID, QuoteID, RevID, Type,
                                                                                        CreatedBy]
    # Parameterdf contains the dataframe to add to a quote
    # BoMRes = MakeBoMQuote(parameterdf)
    # Retrieve BoMQuote
    Andf = GetBoMQuote(ProjectID, QuoteID, RevID)

    # RUN RAW QUERY
    querytext = "SELECT DISTINCT SystemType FROM SYSTEM;"
    querytext = "SELECT * from SYSTEM"
    querytext = "SELECT `QuoteID`,`RevID` FROM BOM_QUOTE WHERE `ProjectID`='{}' \
                    ORDER BY `CreatedDate` DESC     \
                     LIMIT 1;".format(ProjectID)
    querytext = "SELECT DISTINCT RevID FROM BOM_QUOTE WHERE `ProjectID`='{}' and `QuoteID`='{}' \
                     ;".format(ProjectID, QuoteID)

    # res=RunRawSQLQuery(querytext)

    # %%

    tdict = entrydf.iloc[0:2][
        ['ProductName', 'GroupName', 'BusinessIdentifierCode', 'TargetMargin', 'PriorityFlag']].to_dict(
        orient='records')

    # %% Store results as excel
    dirtostore = "/Users/vipluv.aga/Documents/Solextron/Products/Design/SQLWork/"
    storefname = "MasterList Extract.xlsx"
    os.environ['DB_NAME'] = 'ref_agrola'
    db = init_connection_engine()
    querytext = "SELECT * FROM SYSTEM WHERE ActiveFlag=1"
    SystemDf = pd.read_sql(querytext, db)
    SystemDf.sort_values(by='BusinessIdentifierCode', inplace=True)
    MStlist = "/Users/vipluv.aga/Documents/Solextron/Customer/Agrola/MasterList/Masterlist Rev8.xlsx"
    mlist = pd.read_excel(MStlist)
    columnorder = mlist.columns
    SystemDf1 = SystemDf.reindex(columns=columnorder)
    SystemDf1['ActiveFlag'] = 1
    with open(dirtostore + storefname, 'wb') as f:
        SystemDf1.to_excel(f, index=False)



