# importing the JSON, matplotlib,
# sys to exit the script and the SchemDraw_PV and elements for the drawing

import json  # for writing, saving and opening JSON files
import matplotlib
import sys
import subprocess  # open pdf after plot
import SchemDraw_PV as schem  # import SchemDraw_PV as schem
import SchemDraw_PV.elements_PV as e# import the elements
# matplotlib.backend_bases.register_backend('dxf', FigureCanvasDxf)
from matplotlib.backends.backend_ps import FigureCanvasPS
# matplotlib.backend_bases.register_backend('dxf', FigureCanvasDxf)


matplotlib.use('Agg')  # Set the backend here, to save as PDF
res = {}
res={"String Config":{1: [[9, 9], [10, 10]],

 2: [[9], [10]],

 }}


def Ec(res,pvdata,invdata):
    print(res.keys())
    print(type(res))
    print(type(res.values()))
    values = list(res.values())
    count = 0
    res_data = {}
    for key in values[0].keys():
        count += 1
        count_inverter_input = 0
        count_parallel = 0
        data_list = []
        pvamp = 0
        sum_pvolt = 0
        pvvolt =0
        print("no of buildingblocks", len(values[0].keys()))
        for data in values[0][key]:
            print(data)
            count_inverter_input += 1
            no_of_input = len(values[0][key])
            for num in data:
                count_parallel += 1
                data_list.append(num)
            pvamp += (count_parallel *pvdata['I_mp_ref'])
            sum_pvolt +=(count_parallel*num)
        res_data[key] = data_list
        print("count parallel is",count_parallel)
        pvvolt=sum_pvolt/count_parallel
        pvdata['Mp'] =count_parallel
        pvdata['NumBB']=len(values[0].keys())
        pvdata['Bat']=2
        pvdata['BatName']='battery'
        pvdata['PVName']='PVName'
        pvdata['ProjName']='ProjName'
        pvdata['Cable']='Cable'
        print("count",count_inverter_input)
        invdata= {
            'MPPTin':count_inverter_input,
            'InvName':'InvName',
            'InvEff':0.67,
        }

        # with open('inputs.json', 'w') as json_input_file:
        #     json.dump(Out, json_input_file)
        with open('pvdata.json', 'w') as json_input_file:
            json.dump(pvdata, json_input_file)
        # with open('invdata.json', 'w') as json_input_file:
        #     json.dump(invdata, json_input_file)

        # -----------------------------------------------------------------------------

        # opening the JSON file to be able to read its content

        str_data = open("pvdata.json").read()
        pv_json_data = json.loads(str_data)
        # str_data = open("invdata.json").read()
        # inv_json_data = json.loads(str_data)

        # -----------------------------------------------------------------------------

        # reading the values in the JSON file
        # and connecting them to the correct variable

        Mp = pv_json_data['Mp']
        Mp = int(Mp)

        NumBB = pv_json_data['NumBB']
        NumBB = int(NumBB)

        Bat = pv_json_data['Bat']
        Bat = int(Bat)

        MPPTin = invdata['MPPTin']
        MPPTin = int(MPPTin)

        PVName = pv_json_data['PVName']

        PVVolt = pv_json_data['V_mp_ref']
        PVVolt = float(PVVolt)

        PVAmp = pv_json_data['I_mp_ref']
        PVAmp = float(PVAmp)

        BatName = pv_json_data['BatName']

        InvName = invdata['InvName']

        InvEff = invdata['InvEff']
        InvEff = float(InvEff)

        Cable = pv_json_data['Cable']

        ProjName = pv_json_data['ProjName']

        # -----------------------------------------------------------------------------

        # defining parameter needed in loops

        ir = 0
        ip = 0
        global iNb
        iNb = 0

        # Calculating the maximum Voltage and current at different positions

        VL1 = round(pvvolt)  # Labeling Voltage After BB
        VL2 = round(VL1)
        VL3 = 230  # Labeling Voltage After INV

        AL1 = round(pvamp)  # Labeling the current of one string
        AL2 = round(count_parallel * pvamp)  # Labeling the added up current of one BB
        AL3 = round(AL2 * VL2 * InvEff / VL3)  # Labeling the current of all BB

        # -----------------------------------------------------------------------------

        # One way to change the scaling:
        l1 = 2  # size of elements and lines;

        # -----------------------------------------------------------------------------
        # To avoid inputs, which do not make sense and do not work within he program

        if Mp <= 0:
            sys.exit("The variable Mp has an unexpected value (0 or below)!")
        elif NumBB <= 0:
            sys.exit("The variable NumBB has an unexpected value (0 or below)!")
        elif MPPTin <= 0:
            sys.exit("The variable MPPTin has an unexpected value (0 or below)!")
        elif Bat > 2:
            sys.exit("The variable Bat has an unexpected value (NOT 0, 1, 2)!")
        elif Bat < 0:
            sys.exit("The variable Bat has an unexpected value (NOT 0, 1, 2)!")
        else:
            pass

        # -----------------------------------------------------------------------------

        # starting the drawing with d = schem.Drawing()

        d = schem.Drawing()
        d.push()  # fixes the drawing point [0 / 0]. Return with d.pop() below.

        k = 0

        # if Mp == 1:
        #     k = k + 3
        # elif Mp == 2:
        #     k = k + 3
        # else:
        #     k = k + 10
        k =k + 10
        if NumBB == 1:
            k = k * 9
        elif NumBB == 2:
            k = (k + 1) * 6
        else:
            k = (k + 1) * 9

        d.add(e.SOURCE, d='down', xy=[k * l1 + 1, 0],
              botlabel='PV module\n' + str(PVName) +
                       '\n' + str(PVVolt) + 'V')
        d.add(e.CB, d='down', xy=[k * l1 + 1, -2.5],
              botlabel='Combiner Box')
        d.add(e.INVDCAC, xy=[k * l1 + 1, -5],
              botlabel='Inverter DCAC type:\n' + str(InvName))
        d.add(e.INVACDC, xy=[k * l1 + 1, -7.5],
              botlabel='Inverter ACDC type:\n' + str(InvName))
        d.add(e.CONVDCDC, xy=[k * l1 + 1, -10],
              botlabel='DCDC Converter type:\n' + str(InvName))
        d.add(e.BATTERY, xy=[k * l1 + 1, -12.5],
              botlabel='Battery type:\n' + str(BatName))
        d.add(e.LINE, d='right', xy=[k * l1 - 0.5, -16.5], l=l1,
              rgtlabel='Cable type:\n' + str(Cable))

        d.pop()  # returns the drawing cursor to the last d.push() --> [0 / 0]

        # -----------------------------------------------------------------------------

        # Writing the title above the drawing

        d.push()
        d.add(e.LABEL, xy=[0, 1], titlabel='Electrical scheme  ' + str(ProjName))
        d.pop()

        def bb(data_raw):  # function
            global ir, iNb

            # adding the PV modules:
            if Mp:
                d.add(e.LINE, d='right', l=l1)
                d.push()
                sir = 1
                end_point = []
                num = 0
                count_parallel = 1
                pv_number = 0
                pv_numbers = 0
                end_stand = []
                frequency = {}

                # iterating over the list
                for item in data_raw:
                    # checking the element in dictionary
                    if item in frequency:
                        # incrementing the counr
                        frequency[item] += 1
                    else:
                        # initializing the count
                        frequency[item] = 1
                end_points ={}
                for data in data_raw:
                    # import pdb;
                    # pdb.set_trace()
                    pv_number += 1
                    if num != data and count_parallel >= 3:
                        pv_numbers = pv_number -1
                        count_parallel = 1
                    if count_parallel <= 3:
                        if data > 0:
                                sf = d.add(e.PV, d='down', label='PV' + str(pv_number) + '-' + '1')
                                dot = d.add(e.PH)
                                sd=d.add(e.PV, label='PV' + str(pv_number) + '-' + str(data))
                        if count_parallel == 3 and frequency[data] > 3:
                            d.add(e.PH,d='right',xy=sf.end,l=4.2)
                            d.add(e.PH,d='right',xy=sd.start,l=4.2)
                            ENDPV = d.add(e.LINE, d='right',xy=sd.end)
                            end_stand.append(ENDPV)
                            x = d.add(e.LINE)
                            # d.add(e.LINE, xy=sf.start)
                            # d.add(e.LINE)
                            d.add(e.DOT,xy=sf.start+[3.9,0])
                            sf = d.add(e.PV, d='down', label='PV' + str(frequency[data]) + '-' + '1')
                            dot = d.add(e.PH)
                            sd = d.add(e.PV, label='PV' + str(frequency[data]) + '-' + str(data))
                        if pv_number !=len(data_raw) :
                            # ENDPV = d.add(e.LINE, d='right')
                            # end_stand.append(ENDPV)
                            if count_parallel != frequency[data] and frequency[data]<=3 and frequency[data]!=1:
                                ENDPV = d.add(e.LINE, d='right')
                                end_stand.append(ENDPV)
                                x = d.add(e.LINE)
                                d.add(e.DOT,xy=sf.start+[3.9,0])
                            if frequency[data] == 1:
                                ENDPV=d.add(e.DOT)
                                d.add(e.DOT, xy=sf.start + [3.9, 0])

                            else:
                                d.add(e.DOT, xy=sf.start + [3.9, 0])
                            if count_parallel <3 and frequency[data] >3:
                                ENDPV = d.add(e.LINE, d='right',xy=sd.end)
                                end_stand.append(ENDPV)
                                x = d.add(e.LINE)
                                d.add(e.DOT, xy=sf.start + [3.9, 0])
                            else:
                                d.add(e.DOT, xy=sf.start + [3.9, 0])

                            # d.add(e.LINE, xy=sf.start)
                            # d.add(e.LINE)
                        else:
                            # sf = d.add(e.PV, d='down', xy=sf.start + [2,0],label='PV' + str(frequency[data]) + '-' + '1')
                            # dot = d.add(e.PH)
                            # sd = d.add(e.PV, xy=sd.start+[2,0],label='PV' + str(frequency[data]) + '-' + str(data))
                            # ENDPV = d.add(e.LINE, d='right')
                            # d.add(e.SEP)
                            # d.add(e.LINE,xy=sf.start)
                            # d.add(e.SEP)
                            ENDPV = d.add(e.DOT)
                            end_stand.append(ENDPV)
                    if num != data and (count_parallel != len(data_raw) or len(data_raw)) == 1:
                        end_point.append(ENDPV)
                        end_points[data]=ENDPV
                        num=data
                    count_parallel += 1
                ir = 0
                siNb = iNb + 1

            # adding the Combiner Box, the Inverters and the Batteries:
            if MPPTin < Mp:  # if Inv inputs < than number of parallels --> add CB
                print("MPTTin",MPPTin)
                print("paraalel",Mp)
                if Bat == 2:  # if battery per Building Block do this
                    print("end point is", end_point)
                    CBout = 1
                    d.push()
                    count = 0
                    l=len(data_raw)-1
                    if len(end_point) >=3 and len(data_raw)>3:
                        count_raw = 5
                        count_len = 10+(1*l)
                        count_theta = -30
                        count_lens = 3+l
                        count_thetas = 3.6+l
                    else:
                        count_raw = 16
                        count_len = 13
                        count_theta = 0
                    # count_raw = 14
                    for line in end_point:
                        count += 1
                        if len(data_raw) > 4 and len(end_point) == 2:
                            ends = d.add(e.LINE, d='down', xy=line.start, l=l1)
                            if count % 2 == 0:
                                d.add(e.LINE, d='left', xy=ends.end, l=l1)
                                x = d.add(e.LINE, d='down', l=3 * l1)
                            else:
                                if len(end_point) > 1:
                                    d.add(e.LINE, d='right', xy=ends.end, l=9)
                                    d.add(e.LINE, d='down', l=13.5)
                                else:
                                    d.add(e.LINE, d='right', xy=ends.end, l=l1)
                                    d.add(e.LINE, d='down', l=3 * l1)
                        if len(data_raw) == 4 and len(end_point) == 2:
                            ends = d.add(e.LINE, d='down', xy=line.start, l=l1)
                            if count % 2 == 0:
                                d.add(e.LINE, d='left', xy=ends.end, l=l1)
                                x = d.add(e.LINE, d='down', l=3 * l1)
                            else:
                                if len(end_point) > 1:
                                    d.add(e.LINE, d='right', xy=ends.end, l=5)
                                    d.add(e.LINE, d='down',  l=12.9)
                                else:
                                    d.add(e.LINE, d='right', xy=ends.end, l=l1)
                                    d.add(e.LINE, d='down', l=3 * l1)
                        if len(data_raw) > 2 and len(end_point) > 2 and (count != len(data_raw)):
                            print("hi")
                            # ends = d.add(e.LINE, d='down', xy=line.start, l=count_raw)
                            # d.add(e.LINE, d='right', theta=-count_theta,l=count_len)
                            # if count <= 1 and len(end_point) <= 3:
                            #     count_raw -= 2
                            #     count_len -= 3.9
                            #     count_theta += 5
                            #     ends = d.add(e.LINE, d='down', xy=line.start, l=count_raw)
                            #     d.add(e.LINE, d='right', theta=-count_theta, l=count_len)
                            # if count == 2 and len(end_point) <= 3:
                            #     count_raw -= 2
                            #     count_len -= 3.9
                            #     count_theta += 20.5
                            #     ends = d.add(e.LINE, d='down', xy=line.start, l=count_raw)
                            #     d.add(e.LINE, d='right', theta=-count_theta, l=count_len)
                            # if count <= 2 and len(end_point) > 3:
                            #     count_len += 1.5*l
                            #     ends = d.add(e.LINE, d='down', xy=line.start, l=count_raw)
                            #     d.add(e.LINE, d='right',xy=ends.start,theta=count_theta,l=count_len)
                            #     # count_raw -= 1
                            #     count_len -= (3*l)
                            #     count_theta += 0
                            # if count > 2 and len(end_point) > 3 and (count !=len(end_point)):
                            #     ends = d.add(e.LINE, d='down', xy=line.start, l=count_raw)
                            #     d.add(e.LINE, d='right',to=ends.start, l=count_lens)

                        if len(data_raw) > 2 and len(end_point) > 2 and (count == len(end_point)):
                            ends = d.add(e.LINE, d='down', xy=line.start, l=8)

                    for line in end_stand:
                        count += 1
                        if len(data_raw) == 2:
                            ends = d.add(e.LINE, d='down', xy=line.start, l=l1)
                            if count % 2 == 0:
                                d.add(e.LINE, d='right', xy=ends.end, l=l1)
                                x = d.add(e.LINE, d='down', l=6.5 * l1)
                            else:
                                d.add(e.LINE, d='left', xy=ends.end, l=l1)
                                d.add(e.LINE, d='down', l=3 * l1)
                        if len(data_raw) == 1:
                            ends = d.add(e.LINE, d='down', xy=line.start, l=4 * l1)

                    d.push()
                    d_right =d.add(e.LINE, d='right', l=2 * l1)
                    d.add(e.CONVDCDC, d='down',
                          smllblabel='In ' + str(VL1) + ' V / ' + str(AL2) + ' A',
                          smlrblabel='Out ' + str(VL3) + ' V / ' + str(AL3) + ' A',
                          toplabel='DCDC Converter')
                    d.add(e.BATTERY, d='down', toplabel='Battery ' + str(siNb))
                    d.pop()
                    inv_start =d.add(e.LINE, d='down', l=3* l1)
                    # CBout = 1
                    # d.add(e.CB, d='down', xy=ENDPV.start,
                    #       smlltlabel='In ' + str(VL1) + ' V / ' + str(AL1) + ' A',
                    #       smlrtlabel='Out ' + str(VL2) + ' V / ' + str(AL2) + ' A',
                    #       toplabel='Combiner Box ' + str(siNb)
                    #                + '\nInputs: ' + str(Mp)
                    #                + '\nOutputs: ' + str(CBout))
                    # d.push()
                    # d.add(e.LINE, d='left', l=l1)
                    # d.add(e.CONVDCDC, d='down',
                    #       smllblabel='In ' + str(VL1) + ' V / ' + str(AL2) + ' A',
                    #       smlrblabel='Out ' + str(VL3) + ' V / ' + str(AL3) + ' A',
                    #       toplabel='DCDC Converter')
                    # d.add(e.BATTERY, d='down', toplabel='Battery ' + str(siNb))
                    # d.pop()
                    # d.add(e.LINE, d='down', l=2 * l1)

                else:
                    CBout = MPPTin
                    d.add(e.CB, d='down', xy=ENDPV.start,
                          smlltlabel='In ' + str(VL1) + ' V / ' + str(AL1) + ' A',
                          smlrtlabel='Out ' + str(VL2) + ' V / ' + str(AL2) + ' A',
                          toplabel='Combiner Box ' + str(siNb)
                                   + '\nInputs: ' + str(Mp)
                                   + '\nOutputs: ' + str(CBout))
                global inv;
                inv= d.add(e.INVDCAC, d='down',
                      smlltlabel='In ' + str(VL2) + ' V / ' + str(AL2) + ' A',
                      smlrtlabel='Out ' + str(VL3) + ' V / ' + str(AL3) + ' A',
                      toplabel='Inverter Nr ' + str(siNb) +
                               '\nMax Inputs: ' + str(MPPTin)
                               + '\nUsed Inputs: ' + str(CBout))
                count = 0
                # for line in end_point:
                #     count += 1
                #     if len(data_raw) > 2 and len(end_point) > 2 and (count != len(data_raw)):
                #         d_point =d.add(e.LINE,d='right',xy=line.start,l=3)
                #         d.add(e.LINE, xy=d_point.end,to=inv_start.end+[0,-1])
                print("ends",end_points)
                lengths = 12
                l5=10
                x=0.2
                y=0
                end_points=end_points
                dict_lenght = 0
                for end in end_points:
                    dict_lenght +=1
                    if len(data_raw) > 2 and len(end_point) >2and (count != len(data_raw)):
                        if frequency[end]>1:
                            d_point = d.add(e.LINE, d='down', xy=end_points[end].start, l=lengths)
                            # d.add(e.LINE, theta=0,xy=d_point.end,to=inv_start.end- [0.5,0.1])
                        else:
                            d_point = d.add(e.LINE, d='down', xy=end_points[end].start, l=l5)
                        point =d.add(e.LINE,theta=0,xy=d_point.end, tox=inv_start.end + [x, y])
                        d.add(e.LINE,d='down',xy=point.end,to=inv.end+2)
                    lengths -= 1
                    l5 -=1
                    x-=0.59
                    y-=1

            else:
                print("inside combiner box")
                if Bat == 2:  # if battery per BB do this
                    CBout = 1
                    direction = 'right'
                    count = 0
                    l = len(data_raw) - 1
                    if len(end_point) >= 3 and len(data_raw) > 3:
                        count_raw = 5
                        count_len = 10 + (1 * l)
                        count_theta = -30
                        count_lens = 3 + l
                        count_thetas = 3.6 + l
                    else:
                        count_raw = 16
                        count_len = 13
                        count_theta = 0
                    # count_raw = 14
                    for line in end_point:
                        count += 1
                        if len(data_raw) > 4 and len(end_point) == 2:
                            ends = d.add(e.LINE, d='down', xy=line.start, l=l1)
                            if count % 2 == 0:
                                d.add(e.LINE, d='left', xy=ends.end, l=l1)
                                x = d.add(e.LINE, d='down', l=3 * l1)
                            else:
                                if len(end_point) > 1:
                                    d.add(e.LINE, d='right', xy=ends.end, l=l1)
                                    d.add(e.LINE, d='down', theta=-52, l=15.9)
                                else:
                                    d.add(e.LINE, d='right', xy=ends.end, l=l1)
                                    d.add(e.LINE, d='down', l=3 * l1)
                        if len(data_raw) == 4 and len(end_point) == 2:
                            ends = d.add(e.LINE, d='down', xy=line.start, l=l1)
                            if count % 2 == 0:
                                d.add(e.LINE, d='left', xy=ends.end, l=l1)
                                x = d.add(e.LINE, d='down', l=3 * l1)
                            else:
                                if len(end_point) > 1:
                                    d.add(e.LINE, d='right', xy=ends.end, l=l1)
                                    d.add(e.LINE, d='down', theta=-70, l=13.5)
                                else:
                                    d.add(e.LINE, d='right', xy=ends.end, l=l1)
                                    d.add(e.LINE, d='down', l=3 * l1)

                        if len(data_raw) > 2 and len(end_point) > 2 and (count == len(end_point)):
                            ends = d.add(e.LINE, d='down', xy=line.start, l=8)

                    for line in end_stand:
                        count += 1
                        if len(data_raw) == 2:
                            ends = d.add(e.LINE, d='down', xy=line.start, l=l1)
                            if count % 2 == 0:
                                d.add(e.LINE, d='right', xy=ends.end, l=l1)
                                x = d.add(e.LINE, d='down', l=6.5 * l1)
                            else:
                                d.add(e.LINE, d='left', xy=ends.end, l=l1)
                                d.add(e.LINE, d='down', l=3 * l1)
                        if len(data_raw) == 1:
                            ends = d.add(e.LINE, d='down', xy=line.start, l=4 * l1)

                    d.push()
                    d.add(e.LINE, d='right', l=2 * l1)
                    d.add(e.CONVDCDC, d='down',
                          smllblabel='In ' + str(VL1) + ' V / ' + str(AL2) + ' A',
                          smlrblabel='Out ' + str(VL3) + ' V / ' + str(AL3) + ' A',
                          toplabel='DCDC Converter')
                    d.add(e.BATTERY, d='down', toplabel='Battery ' + str(siNb))
                    d.pop()
                    inv_start = d.add(e.LINE, d='down', l=3 * l1)
                    inv_combine = d.add(e.INVDCAC,
                          smlltlabel='In ' + str(VL1) + ' V / ' + str(AL2) + ' A',
                          smlrtlabel='Out ' + str(VL3) + ' V / ' + str(AL3) + ' A',
                          toplabel='Inverter Nr ' + str(siNb) +
                                   '\nMax Inputs: ' + str(MPPTin)
                                   + '\nUsed Inputs: ' + str(1))
                    # count = 0
                    # for line in end_point:
                    #     count += 1
                    #     if len(data_raw) > 2 and len(end_point) > 2 and (count != len(data_raw)):
                    #         d_point = d.add(e.LINE, d='down', xy=line.start, l=12)
                    #         d.add(e.LINE, xy=d_point.end, to=inv_start.end + [0, -1])
                    print("end_points is",end_points)
                    for end in end_points:
                        if len(data_raw) > 2 and len(end_point) > 2 and (count != len(data_raw)):
                            if frequency[end] > 1:
                                d_point = d.add(e.LINE, d='right', xy=end_points[end].start, l=3)
                            else:
                                d_point = d.add(e.LINE, d='down', xy=end_points[end].start, l=10)
                            d.add(e.LINE, xy=d_point.end, to=inv_start.end + [0, -1])


                else:  # if NO battery  do this
                    d.add(e.INVDCAC, d='down', xy=ENDPV.start,
                          smlltlabel='In ' + str(VL1) + ' V / ' + str(AL1) + ' A',
                          smlrtlabel='Out ' + str(VL3) + ' V / ' + str(AL3) + ' A',
                          toplabel='Inverter Nr ' + str(siNb)
                                   + '\nMax Inputs: ' + str(MPPTin)
                                   + '\nUsed Inputs: ' + str(Mp))
            iNb = iNb + 1

        # ----------------------------------------------------------------------------

        # Placement of the BB label on y-axis
        BBLabelY = 0
        BBLabelY = -3.2

        # Placement of the BB label on x-axis
        BBLabelX = -0.5

        # Calling the main function bb(), to create the Building Blocks:
        raw = 0
        if NumBB == 1:
            print("resdatafinal",res_data)
            for key in res_data:
                print(res_data[key])
                raw += 1
                if raw == 1:
                    ENDBB = d.add(e.GND, botlabel='GND', l=l1)
                    d.add(e.DOT)
                    d.add(e.LABEL, xy=ENDBB.start + [BBLabelX, BBLabelY], titlabel='BB  ' + str(1))
                    bb(res_data[key])
                    BB1DOT = d.add(e.DOT)
                else:
                    ENDBB = d.add(e.GND, xy=ENDBB.start + [l1 * 20, 0],
                                  botlabel='GND', l=l1)
                    d.add(e.DOT, xy=ENDBB.start)
                    d.add(e.LABEL, xy=ENDBB.start + [BBLabelX, BBLabelY],
                          titlabel='BB  ' + str(key))
                    bb(res_data[key])
                    d.add(e.SEP,xy=inv.end,to=BB1DOT.start-[0,0], l=l1)

        else:
            print("res datafinsl",res_data)
            for key in res_data:
                print(res_data[key])
                raw += 1
                if raw == 1:
                    ENDBB = d.add(e.GND, botlabel='GND', l=l1)
                    d.add(e.DOT)
                    d.add(e.LABEL, xy=ENDBB.start + [BBLabelX, BBLabelY], titlabel='BB  ' + str(1))
                    bb(res_data[key])
                    BB1DOT = d.add(e.DOT)
                else:
                    ENDBB = d.add(e.GND, xy=ENDBB.start + [l1 * 30, 0],
                                  botlabel='GND', l=l1)
                    d.add(e.DOT, xy=ENDBB.start)
                    d.add(e.LABEL, xy=ENDBB.start + [BBLabelX, BBLabelY],
                          titlabel='BB  ' + str(key))
                    bb(res_data[key])
                    d.add(e.SEP, xy=inv.end,to=BB1DOT.start-[1.3,1.9], l=l1)
        # # -----------------------------------------------------------------------------
        #
        # # The Combiner Box after the Building Blocks
        #
        if NumBB > 1:
            d.add(e.CB, d='down',
                  smlltlabel='In ' + str(VL3) + ' V / ' + str(AL3) + ' A',
                  smlrtlabel='Out ' + str(VL3) + ' V / ' + str(AL3 * NumBB) + ' A',
                  toplabel='Combiner Box All BB'
                           + '\nInputs: ' + str(NumBB)
                           + '\nOutputs: 1')

        # -----------------------------------------------------------------------------

        # If the system uses only one Battery, it gets added here:

        if Bat == 1:
            d.push()
            d.add(e.LINE, d='right', l=3)
            d.add(e.INVACDC, d='right',
                  smllblabel='In ' + str(VL3) + ' V / ' + str(AL3) + ' A',
                  smlrblabel='Out ' + str(VL3) + ' V / ' + str(AL3) + ' A',
                  toplabel='Inverter transformer')
            d.add(e.BATTERY, d='right', toplabel='Battery All BB')
            d.pop()
            d.add(e.LINE, d='down', l=2 * l1)

        # -----------------------------------------------------------------------------

        # The connection to the electrical grid:

        Grid = d.add(e.GRID, d='down', toplabel='Grid connection')

        # -----------------------------------------------------------------------------

        # Finishing the drawing, saving it to a pdf and automatically open it.

    d.draw()

    d.fig.savefig('circuits_output5.svg', bbox_extra_artists=d.ax.get_default_bbox_extra_artists(),
                  bbox_inches='tight', pad_inches=0.15, dpi=1000)

    subprocess.Popen(["circuits_output5.svg"], shell=True)


res = {"String Config": {1: [[9,9,9,9,9],[10,10],[11],[13],[15],[16],[17],[18]],
                         2:[[8],[19],[20],[29]],
                         3:[[9,9],[5,5,5],[8],[12],[10]],
                         4:[[9,9],[5,5]],
                         }}
PVData = {
    'I_mp_ref':39,
    'V_mp_ref':7,
}
INVData = {

}
print(Ec(res, PVData,INVData))