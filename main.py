from middlewareauth import jwt_authenticated
import os
import json
import re
import uuid
import datetime
import calendar
import pyrebase
from flask import Flask, request, jsonify
from google.cloud import storage, datastore

app = Flask(__name__)
BUCKET_NAME = 'solbucket'
data = open("firebase_sdk2.json").read()
firebase_data = json.loads(data)
firebaseConfig = {
    "apiKey": firebase_data['apiKey'],
    "authDomain": firebase_data['authDomain'],
    "projectId": firebase_data['projectId'],
    "storageBucket": firebase_data['storageBucket'],
    "databaseURL": firebase_data['databaseURL'],
    "messagingSenderId": firebase_data['messagingSenderId'],
    "appId": firebase_data['appId'],
    "measurementId": firebase_data['measurementId']
  }
firebases = pyrebase.initialize_app(firebaseConfig)
auth=firebases.auth()

@app.after_request
def add_corsheader(response):
    whitelistpatterns = [re.compile(".*solextron.*\.herokuapp\.com\/?.*"), 
                         re.compile(".*solex-mvp-2.*\.appspot\.com\/?.*"),
                         re.compile(".*localhost.*"),
                         re.compile(".*solextron\.com\/?.*"),
                         re.compile(".*\.run\.*app\/?.*")]
   
    # whitelist =  ["http://localhost","https://3dscene-dot-solex-mvp-2.appspot.com","http://localhost:4200", "http://localhost:8080",
    #           "https://solex-ui-ng-dot-solex-mvp-2.appspot.com","https://solcalcmap-dot-solex-mvp-2.appspot.com","http://localhost:4200/",
    #           "http://solextron-new.herokuapp.com","https://solextron-new.herokuapp.com","https://solextron-solar-calculator.herokuapp.com",
    #           "https://solex-ui-ng-dot-solex-mvp-2.oa.r.appspot.com"]
                            

    weborigin = request.environ.get('HTTP_ORIGIN')
    referer = request.environ.get('HTTP_REFERER')
    
    if len([weborigin for Wlist in whitelistpatterns if Wlist.match(str(weborigin))])>0:
        response.headers["Access-Control-Allow-Origin"] = weborigin 
    elif len([referer for Wlist in whitelistpatterns if Wlist.match(str(referer))])>0:
        response.headers["Access-Control-Allow-Origin"] = referer
    else:
        response.headers["Access-Control-Allow-Origin"] = "http://localhost:4200"
    response.headers["Access-Control-Allow-Credentials"] = "true"
    response.headers["Access-Control-Allow-Headers"] = "authorization, content-type"
    response.headers["Access-Control-Allow-Methods"]=  "GET,POST,OPTIONS,DELETE,PUT"
    
    return response
   

@app.route('/analysis',methods=['GET'])
@jwt_authenticated
def um_analysis():
    args = request.args.to_dict()
    year = 2021
    current_month = 7
    Q = {"accounts": {"month": 0, "total": 0, "year": []}, "users": {"month": 0, "total": 0, "year": []}}
    datastore_client = datastore.Client()
    for month in range(1, 13):
        start_date = datetime.datetime(year, month, 1)
        days_in_month = calendar.monthrange(start_date.year, start_date.month)[1]
        end_date = start_date + datetime.timedelta(days=days_in_month)
        org_query = datastore_client.query(kind='um-organization')
        user_query = datastore_client.query(kind='um-user')
        org_query.add_filter("CreatedDate", ">", start_date)
        org_query.add_filter("CreatedDate", "<", end_date)
        user_query.add_filter("CreatedDate", ">", start_date)
        user_query.add_filter("CreatedDate", "<", end_date)
        org_query = list(org_query.fetch())
        temp1 = len(org_query)
        user_query = list(user_query.fetch())
        temp2 = len(user_query)
        Q["accounts"]["year"].append(temp1)
        Q["accounts"]["total"] = Q["accounts"]["total"] + temp1
        Q["users"]["year"].append(temp2)
        Q["users"]["total"] = Q["users"]["total"] + temp2
        if month == current_month:
            Q["accounts"]["month"] = temp1
            Q["users"]["month"] = temp2

    return jsonify({"message":"organization listed successfully", "data": Q}), 200


@app.route('/org',methods=['POST','GET'])
@jwt_authenticated
def organization_details():
    args = request.args.to_dict()
    page = int(args.get("page", 1))
    status = args.get("status", "").lower()
    offset = (page - 1) * 10
    limit = 10
    datastore_client = datastore.Client()
    if request.method == 'POST':
        req_json = request.get_json()
        org_id = str(uuid.uuid1())
        org_name=req_json.get("OrganizationAccountDetails").get("company")
        org_query = datastore_client.query(kind='um-organization')
        org_query.add_filter("OrganizationAccountDetails.company", "=",org_name)
        Q = list(org_query.fetch())
        if len(Q) !=0:
            return jsonify({"message": "Organization with this  name exists"}), 409

        entity = datastore.Entity(key=datastore_client.key('um-organization', org_id))
        entity.update({
            "OrganizationId": org_id,
            "OrganizationBasicDetails": req_json.get("OrganizationBasicDetails", {}),
            "Status": req_json.get("Status", "Active").lower(),
            "CreatedDate": datetime.datetime.utcnow(),
            "UpdatedDate": datetime.datetime.utcnow(),
            "OrganizationAccountDetails": req_json.get("OrganizationAccountDetails", {}),
            "OrganizationToolAccess": req_json.get("OrganizationToolAccess", {})
            })
        datastore_client.put(entity)
        query = datastore_client.query(kind='um-organization')
        query.add_filter("OrganizationId", "=", org_id)
        Q = list(query.fetch())
        return jsonify({"message":"Organization created successfully", "data": Q[0]}), 200
    else:
        query = datastore_client.query(kind='um-organization')
        # query.order = ["-CreatedDate"]
        if status:
            query.add_filter("Status", "=", status)

        Q = list(query.fetch(offset=offset, limit=limit))
        # Q = sorted(Q, key = lambda i: i['CreatedDate'])
        return jsonify({"message":"organization listed successfully", "data": Q}), 200


@app.route('/org/<org_id>',methods=['GET','POST','DELETE'])
@jwt_authenticated
def update_organization_details(org_id):
    datastore_client = datastore.Client()
    if request.method == 'POST':
        req_json = request.get_json()
        org_entity = datastore_client.get(key=datastore_client.key('um-organization', org_id))
        if not org_entity:
            return jsonify({"message": "organization not found with given ID"}), 404

        org_name = req_json.get("OrganizationAccountDetails")["company"]
        org_query = datastore_client.query(kind='um-organization')
        org_query.add_filter("OrganizationAccountDetails.company", "=", org_name)
        org_data_name = org_entity['OrganizationAccountDetails']['company']
        Q = list(org_query.fetch())
        if org_name != org_data_name and len(Q) != 0:
            return jsonify({"message": " unable to update Organization, organization with same name exist"}), 409

        org_entity.update({
            "OrganizationBasicDetails": req_json.get("OrganizationBasicDetails", {}),
            "UpdatedDate": datetime.datetime.utcnow(),
            "Status": req_json.get("Status", "Active").lower(),
            "OrganizationAccountDetails": req_json.get("OrganizationAccountDetails", {}),
            "OrganizationToolAccess": req_json.get("OrganizationToolAccess", {})
        })
        datastore_client.put(org_entity)
        return jsonify({"message": "organization details updated successfully"}), 200
    elif request.method == 'GET':
        query = datastore_client.query(kind='um-organization')
        query.add_filter("OrganizationId", "=", org_id)
        Q = list(query.fetch())
        if not Q:
            return jsonify({"message": "organization not found with given ID"}), 404

        return jsonify({"message":"organization details fetched successfully","data":Q[0]}), 200
    elif request.method == 'DELETE':
        org_entity = datastore_client.get(key=datastore_client.key('um-organization', org_id))
        if not org_entity:
            return jsonify({"message": "organization not found with given ID"}), 404

        datastore_client.delete(key=datastore_client.key('um-organization', org_id))
        return jsonify({"success":"organization record deleted successfully"}), 200


@app.route('/org/<org_id>/user',methods=['POST','GET'])
@jwt_authenticated
def user_details(org_id):
    args = request.args.to_dict()
    page = int(args.get("page", 1))
    status = args.get("status", "").lower()
    offset = (page - 1) * 10
    limit = 10
    datastore_client = datastore.Client()
    if request.method == 'POST':
        req_json = request.get_json()
        user_id = str(uuid.uuid1())
        user_email=req_json.get("BasicDetails")['email']
        user_query = datastore_client.query(kind='um-user')
        user_query.add_filter("BasicDetails.email", "=",user_email)
        Q = list(user_query.fetch())
        if len(Q) !=0:
            return jsonify({"message": "user with this  email exists"}), 409
        user_entity = datastore.Entity(key=datastore_client.key('um-organization', org_id, 'um-user', user_id))
        user_entity.update({
            "UserId": user_id,
            "OrganizationId": org_id,
            "CreatedDate": datetime.datetime.utcnow(),
            "UpdatedDate": datetime.datetime.utcnow(),
            "BasicDetails": req_json.get("BasicDetails", {}),
            "ToolAccess": req_json.get("ToolAccess", {}),
            "Status": req_json.get("Status", "").lower(),
            "AccessRight":req_json.get("AccessRight"),
        })
        datastore_client.put(user_entity)
        user=auth.create_user_with_email_and_password(user_email,req_json.get("BasicDetails")['password'])
        return jsonify({"message": "User created successfully", "data": req_json}), 200
    elif request.method == 'GET':
        query = datastore_client.query(kind='um-user')
        query.add_filter("OrganizationId", "=", org_id)
        if status:
            query.add_filter("Status", "=", status)

        # query.order = ["-CreatedDate"]
        # Q = sorted(Q, key = lambda i: i['CreatedDate'])
        Q = list(query.fetch(offset=offset, limit=limit))
        return jsonify({"message": "user details listed successfully", "data": Q}), 200


@app.route('/org/<org_id>/user/<user_id>',methods=['GET','POST','DELETE'])
@jwt_authenticated
def update_user_details(org_id, user_id):
    datastore_client = datastore.Client()
    if request.method == 'POST':
        req_json = request.get_json()
        key = datastore_client.key('um-organization', org_id, 'um-user', user_id)
        user_entity = datastore_client.get(key=key)
        if not user_entity:
            return jsonify({"message": "user ID {} not found ".format(user_id)}), 404
        user_email = req_json.get("BasicDetails")['email']
        user_query = datastore_client.query(kind='um-user')
        user_query.add_filter("BasicDetails.email", "=", user_email)
        user_data_email = user_entity['BasicDetails']['email']
        Q = list(user_query.fetch())
        if user_email != user_data_email and len(Q) != 0:
            return jsonify({"message": " unable to update user with this  email exists"}), 409

        user_entity.update({
            "UpdatedDate": datetime.datetime.utcnow(),
            "OrganizationId": org_id,
            "BasicDetails": req_json.get("BasicDetails", {}),
            "ToolAccess": req_json.get("ToolAccess", {}),
            "Status": req_json.get("Status", "").lower(),
            "AccessRight":req_json.get("AccessRight"),
        })
        datastore_client.put(user_entity)
        return jsonify({"message":"user details updated successfully"}),200
    elif request.method == 'GET':
        query = datastore_client.query(kind='um-user')
        query.add_filter("UserId", "=", user_id)
        Q = list(query.fetch())
        if not Q:
            return jsonify({"message": "user not found with given ID"}), 404

        return jsonify({"message":"user details fetched successfully","data":Q[0]}), 200

    elif request.method == 'DELETE':
        key = datastore_client.key('um-organization', org_id, 'um-user', user_id)
        user_entity = datastore_client.get(key=key)
        if not user_entity:
            return jsonify({"message": "User not found with UserID: {}".format(user_id)})

        datastore_client.delete(key=key)
        return jsonify({"success":"user record deleted successfully"}), 200


if __name__ == '__main__':
    app.run(host='127.0.0.1', port=8080, debug=True, use_reloader=True)    
