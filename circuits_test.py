# importing the JSON, matplotlib,
# sys to exit the script and the SchemDraw_PV and elements for the drawing

import json  # for writing, saving and opening JSON files
import matplotlib
import os
from os import listdir
import sys
import subprocess  # open pdf after plot
import SchemDraw_PV as schem  # import SchemDraw_PV as schem
from SchemDraw_PV import elements_PV as e# import the elements
# matplotlib.backend_bases.register_backend('dxf', FigureCanvasDxf)
from matplotlib.backends.backend_ps import FigureCanvasPS
# matplotlib.backend_bases.register_backend('dxf', FigureCanvasDxf)


matplotlib.use('Agg')  # Set the backend here, to save as PDF
res = {}
res={"String Config":{1: [[9, 9], [10, 10]],

 2: [[9], [10]],

 }}


def Ec(res, pvdata, invdata, svg_file2_name, pdf_file2_name, ProjName, PvName, InvName):
    values = list(res.values())
    count = 0
    res_data = {}
    for key in values[0].keys():
        count += 1
        count_inverter_input = 0
        count_parallel = 0
        data_list = []
        data_dict={}
        pvamp = 0
        sum_pvolt = 0
        pvvolt =0
        for data in values[0][key]:
            print(data)
            count_inverter_input += 1
            no_of_input = len(values[0][key])
            for num in data:
                count_parallel += 1
                data_list.append(num)
            pvamp += (count_parallel *pvdata['I_mp_ref'])
            sum_pvolt +=(count_parallel*num)
            data_dict[count] = data_list
        data_list.append(count_inverter_input)
        print("daa",data_dict)
        res_data[count] = data_list
        pvvolt=sum_pvolt/count_parallel
        print("count inverter input",count_inverter_input)
        print("count paralel",count_parallel)
        pvdata['Mp'] =count_parallel
        pvdata['NumBB']=len(values[0].keys())
        pvdata['Bat']=1
        pvdata['BatName']='battery'
        pvdata['PVName']='PVName'
        pvdata['Cable']='Cable'
        invdata= {
            'MPPTin':count_inverter_input,
            'InvName':'InvName',
            'InvEff':0.67,
        }

        # with open('inputs.json', 'w') as json_input_file:
        #     json.dump(Out, json_input_file)
        with open('pvdata.json', 'w') as json_input_file:
            json.dump(pvdata, json_input_file)
        with open('invdata.json', 'w') as json_input_file:
            json.dump(invdata, json_input_file)

        # -----------------------------------------------------------------------------

        # opening the JSON file to be able to read its content

        str_data = open("pvdata.json").read()
        pv_json_data = json.loads(str_data)
        str_data = open("invdata.json").read()
        inv_json_data = json.loads(str_data)

        # -----------------------------------------------------------------------------

        # reading the values in the JSON file
        # and connecting them to the correct variable

        Mp = pv_json_data['Mp']
        Mp = int(Mp)

        NumBB = pv_json_data['NumBB']
        NumBB = int(NumBB)

        Bat = pv_json_data['Bat']
        Bat = int(Bat)

        MPPTin = count_inverter_input
        MPPTin = int(MPPTin)

        PVVolt = pv_json_data['V_mp_ref']
        PVVolt = float(PVVolt)

        PVAmp = pv_json_data['I_mp_ref']
        PVAmp = float(PVAmp)

        BatName = pv_json_data['BatName']

        InvEff = inv_json_data['InvEff']
        InvEff = float(InvEff)

        Cable = pv_json_data['Cable']


        # -----------------------------------------------------------------------------

        # defining parameter needed in loops

        ir = 0
        ip = 0
        global iNb
        iNb = 0
        inv_list= []

        # Calculating the maximum Voltage and current at different positions

        VL1 = round(pvvolt)  # Labeling Voltage After BB
        VL2 = round(VL1)
        VL3 = 230  # Labeling Voltage After INV

        AL1 = round(pvamp)  # Labeling the current of one string
        AL2 = round(count_parallel * pvamp)  # Labeling the added up current of one BB
        AL3 = round(AL2 * VL2 * InvEff / VL3)  # Labeling the current of all BB

        # -----------------------------------------------------------------------------

        # One way to change the scaling:
        l1 = 2  # size of elements and lines;

        # -----------------------------------------------------------------------------
        # To avoid inputs, which do not make sense and do not work within he program

        if Mp <= 0:
            sys.exit("The variable Mp has an unexpected value (0 or below)!")
        elif NumBB <= 0:
            sys.exit("The variable NumBB has an unexpected value (0 or below)!")
        elif MPPTin <= 0:
            sys.exit("The variable MPPTin has an unexpected value (0 or below)!")
        elif Bat > 2:
            sys.exit("The variable Bat has an unexpected value (NOT 0, 1, 2)!")
        elif Bat < 0:
            sys.exit("The variable Bat has an unexpected value (NOT 0, 1, 2)!")
        else:
            pass

        # -----------------------------------------------------------------------------

        # starting the drawing with d = schem.Drawing()

        d = schem.Drawing()
        d.push()  # fixes the drawing point [0 / 0]. Return with d.pop() below.
        #
        # k = 0
        #
        # # if Mp == 1:
        # #     k = k + 3
        # # elif Mp == 2:
        # #     k = k + 3
        # # else:
        # #     k = k + 10
        # k =k + 10
        # if NumBB == 1:
        #     k = k * 9
        # elif NumBB == 2:
        #     k = (k + 1) * 6
        # else:
        #     k = (k + 1) * 12
        #
        # d.add(e.PV, d='down', xy=[k * l1 + 1, 0],
        #       botlabel='PV module\n' + str(PVName) +
        #                '\n' + str(PVVolt) + 'V')
        # d.add(e.CB, d='down', xy=[k * l1 + 1, -2.5],
        #       botlabel='Combiner Box')
        # d.add(e.INVDCAC, xy=[k * l1 + 1, -5],
        #       botlabel='Inverter DCAC type:\n' + str(InvName))
        # # d.add(e.INVACDC, xy=[k * l1 + 1, -7.5],
        # #       botlabel='Inverter ACDC type:\n' + str(InvName))
        # # d.add(e.CONVDCDC, xy=[k * l1 + 1, -10],
        # #       botlabel='DCDC Converter type:\n' + str(InvName))
        # # d.add(e.BATTERY, xy=[k * l1 + 1, -12.5],
        # #       botlabel='Battery type:\n' + str(BatName))
        # d.add(e.LINE, d='right', xy=[k * l1 - 0.5, -16.5], l=l1,
        #       rgtlabel='Cable type:\n' + str(Cable))
        #
        # d.pop()  # returns the drawing cursor to the last d.push() --> [0 / 0]
        #
        # # -----------------------------------------------------------------------------
        #
        # # Writing the title above the drawing
        #
        # d.push()
        # d.add(e.LABEL, xy=[0, 1], titlabel='Electrical scheme  ' + str(ProjName))
        # d.pop()

        def bb(data_raw,key):  # function
            global ir, iNb
            MPPTin = key

            # adding the PV modules:
            if Mp:
                d.add(e.LINE, d='right', l=l1)
                d.push()
                sir = 1
                end_point = []
                num = 0
                count_parallel = 1
                pv_number = 0
                pv_numbers = 0
                end_stand = []
                frequency = {}

                # iterating over the list
                for item in data_raw:
                    # checking the element in dictionary
                    if item in frequency:
                        # incrementing the counr
                        frequency[item] += 1
                    else:
                        # initializing the count
                        frequency[item] = 1
                end_points ={}
                end_two={}
                global dots_list
                dots_list=[]
                data_raw=[[18,18],[18,18]]
                for datas in data_raw:
                    for data in datas:
                        # import pdb;
                        # pdb.set_trace()
                        pv_number += 1
                        if num != data and count_parallel >= 3:
                            pv_numbers = pv_number - 1
                            count_parallel = 1
                        if count_parallel <= 3:
                            if data > 0:
                                sf = d.add(e.PV, d='down', label='PV' + str(pv_number) + '-' + '1')
                                dots_list.append(sf)
                                dot = d.add(e.PH)
                                sd = d.add(e.PV, label='PV' + str(pv_number) + '-' + str(data))
                            if count_parallel == 3 and frequency[data] > 3:
                                label = d.add(e.PH, d='right', xy=sf.end - [0, 1], l=3.9)
                                d.add(e.LABEL, xy=label.end + [1, 0],
                                      label=str(frequency[data]) + 'string')
                                # d.add(e.PH,d='right',xy=sd.start,l=4.2)
                                ENDPV = d.add(e.LINE, d='right', xy=sd.end, l=1.6)
                                end_stand.append(ENDPV)
                                x = d.add(e.LINE)
                                # d.add(e.LINE, xy=sf.start)
                                # d.add(e.LINE)
                                d.add(e.DOT, xy=sf.start + [3.9, 0])
                                sf = d.add(e.PV, d='down', label='PV' + str(frequency[data]) + '-' + '1')
                                dot = d.add(e.PH)
                                sd = d.add(e.PV, label='PV' + str(frequency[data]) + '-' + str(data))
                                if len(frequency.keys()) > 1:
                                    d.add(e.DOT, xy=sf.start + [2.2, 0])
                            if pv_number != len(data_raw):
                                # ENDPV = d.add(e.LINE, d='right')
                                # end_stand.append(ENDPV)
                                if count_parallel != frequency[data] and frequency[data] <= 3 and frequency[data] != 1:
                                    ENDPV = d.add(e.LINE, d='right', l=2.2)
                                    end_stand.append(ENDPV)
                                    # x = d.add(e.LINE)
                                    d.add(e.DOT, xy=sf.start + [2.2, 0])
                                if frequency[data] == 1:
                                    ENDPV = d.add(e.DOT)
                                    d.add(e.DOT, xy=sf.start + [2.2, 0])

                                # else:
                                #     d.add(e.DOT, xy=sf.start + [2.5, 0])
                                if count_parallel < 3 and frequency[data] > 3:
                                    ENDPV = d.add(e.LINE, d='right', xy=sd.end, l=2.2)
                                    end_stand.append(ENDPV)
                                    # x = d.add(e.LINE)
                                    global dots
                                    d.add(e.DOT, xy=sf.start + [2.2, 0])
                                else:
                                    if count_parallel != 3 or frequency[data] <= 3 or (
                                            count_parallel != 3 and frequency[data] > 3):
                                        d.add(e.DOT, xy=sf.start + [2.2, 0])

                                # d.add(e.LINE, xy=sf.start)
                                # d.add(e.LINE)
                            else:
                                # sf = d.add(e.PV, d='down', xy=sf.start + [2,0],label='PV' + str(frequency[data]) + '-' + '1')
                                # dot = d.add(e.PH)
                                # sd = d.add(e.PV, xy=sd.start+[2,0],label='PV' + str(frequency[data]) + '-' + str(data))
                                # ENDPV = d.add(e.LINE, d='right')
                                # d.add(e.SEP)
                                # d.add(e.LINE,xy=sf.start)
                                # d.add(e.SEP)
                                ENDPV = d.add(e.DOT)
                                end_stand.append(ENDPV)
                        end_two[data] = ENDPV
                        if num != data and (count_parallel != len(data_raw) or len(data_raw)) == 1:
                            end_point.append(ENDPV)
                            end_points[data] = ENDPV
                            num = data
                        count_parallel += 1
                    # # import pdb;
                    # # pdb.set_trace()
                    # pv_number += 1
                    # if num != data and count_parallel >= 3:
                    #     pv_numbers = pv_number -1
                    #     count_parallel = 1
                    # if count_parallel <= 3:
                    #     if data > 0:
                    #             sf = d.add(e.PV, d='down', label='PV' + str(pv_number) + '-' + '1')
                    #             dots_list.append(sf)
                    #             dot = d.add(e.PH)
                    #             sd=d.add(e.PV, label='PV' + str(pv_number) + '-' + str(data))
                    #     if count_parallel == 3 and frequency[data] > 3:
                    #         label =d.add(e.PH,d='right',xy=sf.end-[0,1],l=3.9)
                    #         d.add(e.LABEL, xy=label.end + [1,0],
                    #               label=str(frequency[data])+'string')
                    #         # d.add(e.PH,d='right',xy=sd.start,l=4.2)
                    #         ENDPV = d.add(e.LINE, d='right',xy=sd.end,l=1.6)
                    #         end_stand.append(ENDPV)
                    #         x = d.add(e.LINE)
                    #         # d.add(e.LINE, xy=sf.start)
                    #         # d.add(e.LINE)
                    #         d.add(e.DOT,xy=sf.start+[3.9,0])
                    #         sf = d.add(e.PV, d='down', label='PV' + str(frequency[data]) + '-' + '1')
                    #         dot = d.add(e.PH)
                    #         sd = d.add(e.PV, label='PV' + str(frequency[data]) + '-' + str(data))
                    #         if len(frequency.keys())>1:
                    #             d.add(e.DOT, xy=sf.start + [2.2, 0])
                    #     if pv_number !=len(data_raw) :
                    #         # ENDPV = d.add(e.LINE, d='right')
                    #         # end_stand.append(ENDPV)
                    #         if count_parallel != frequency[data] and frequency[data]<=3 and frequency[data]!=1:
                    #             ENDPV = d.add(e.LINE, d='right',l=2.2)
                    #             end_stand.append(ENDPV)
                    #             # x = d.add(e.LINE)
                    #             d.add(e.DOT,xy=sf.start+[2.2,0])
                    #         if frequency[data] == 1:
                    #             ENDPV=d.add(e.DOT)
                    #             d.add(e.DOT, xy=sf.start + [2.2, 0])
                    #
                    #         # else:
                    #         #     d.add(e.DOT, xy=sf.start + [2.5, 0])
                    #         if count_parallel <3 and frequency[data] >3 :
                    #             ENDPV = d.add(e.LINE, d='right',xy=sd.end,l=2.2)
                    #             end_stand.append(ENDPV)
                    #             # x = d.add(e.LINE)
                    #             global dots
                    #             d.add(e.DOT, xy=sf.start + [2.2, 0])
                    #         else:
                    #             if count_parallel !=3 or frequency[data]<=3 or (count_parallel!=3 and frequency[data]>3) :
                    #                 d.add(e.DOT, xy=sf.start + [2.2, 0])
                    #
                    #         # d.add(e.LINE, xy=sf.start)
                    #         # d.add(e.LINE)
                    #     else:
                    #         # sf = d.add(e.PV, d='down', xy=sf.start + [2,0],label='PV' + str(frequency[data]) + '-' + '1')
                    #         # dot = d.add(e.PH)
                    #         # sd = d.add(e.PV, xy=sd.start+[2,0],label='PV' + str(frequency[data]) + '-' + str(data))
                    #         # ENDPV = d.add(e.LINE, d='right')
                    #         # d.add(e.SEP)
                    #         # d.add(e.LINE,xy=sf.start)
                    #         # d.add(e.SEP)
                    #         ENDPV = d.add(e.DOT)
                    #         end_stand.append(ENDPV)
                    # end_two[data]=ENDPV
                    # if num != data and (count_parallel != len(data_raw) or len(data_raw)) == 1:
                    #     end_point.append(ENDPV)
                    #     end_points[data]=ENDPV
                    #     num=data
                    # count_parallel += 1
                ir = 0
                siNb = iNb + 1

            # adding the Combiner Box, the Inverters and the Batteries:
            if MPPTin < Mp:  # if Inv inputs < than number of parallels --> add CB
                # import pdb
                # pdb.set_trace();
                if Bat == 2:  # if battery per Building Block do this
                    print("end point is", end_point)
                    CBout = 1
                    d.push()
                    count = 0
                    l=len(data_raw)-1
                    if len(end_point) >=3 and len(data_raw)>3:
                        count_raw = 5
                        count_len = 10+(1*l)
                        count_theta = -30
                        count_lens = 3+l
                        count_thetas = 3.6+l
                    else:
                        count_raw = 16
                        count_len = 13
                        count_theta = 0
                    # count_raw = 14
                    for line in end_point:
                        count += 1
                        if len(data_raw) > 2 and len(data_raw)!=4and len(end_point) == 2:
                            ends = d.add(e.LINE, d='down', xy=line.start, l=8)
                            # if count % 2 == 0:
                            #     d.add(e.LINE, d='left', xy=ends.end, l=l1)
                            #     x = d.add(e.LINE, d='down', l=3 * l1)
                            # else:
                            #     if len(end_point) > 1:
                            #         d.add(e.LINE, d='right', xy=ends.end, l=l1)
                            #         d.add(e.LINE, d='down',theta=-90, l=12.6)
                            #     else:
                            #         d.add(e.LINE, d='right', xy=ends.end, l=l1)
                            #         d.add(e.LINE, d='down', l=3 * l1)
                        if len(data_raw) == 4 and len(end_point) == 2:
                            ends = d.add(e.LINE, d='down', xy=line.start, l=l1)
                            if count % 2 == 0:
                                d.add(e.LINE, d='left', xy=ends.end, l=l1)
                                x = d.add(e.LINE, d='down', l=3 * l1)
                            else:
                                if len(end_point) > 1:
                                    d.add(e.LINE, d='right', xy=ends.end, l=1.5)
                                    d.add(e.LINE, d='down',theta=-90,l=12.6)
                                else:
                                    d.add(e.LINE, d='right', xy=ends.end, l=l1)
                                    d.add(e.LINE, d='down', l=3 * l1)

                        if len(data_raw) > 2 and len(end_point) > 2 and (count == len(end_point)):
                            ends = d.add(e.LINE, d='down', xy=line.start, l=8)
                        if len(data_raw) >= 4 and len(end_point) ==1:
                            ends = d.add(e.LINE, d='down', xy=line.start, l=8)

                    for line in end_two:
                        count += 1
                        if len(data_raw) == 2 and len(end_two)!=1:
                            ends = d.add(e.LINE, d='down', xy=end_two[line].start, l=l1)
                            if count % 2 == 0:
                                d.add(e.LINE, d='right', xy=ends.end, l=1)
                                d.add(e.LINE, d='down', l=9.3 * l1)
                            else:
                                d.add(e.LINE, d='left', xy=ends.end, l=1)
                                d.add(e.LINE, d='down', l=3 * l1)
                        if len(data_raw) == 2 and len(end_two) == 1:
                            ends = d.add(e.LINE, d='down', xy=end_two[line].start, l=8)
                        if len(data_raw) == 1:
                            ends = d.add(e.LINE, d='down', xy=end_two[line].start, l=4 * l1)

                    d.push()
                    # d_right =d.add(e.LINE, d='right', l=2 * l1)
                    # d.add(e.CONVDCDC, d='down',
                    #       smllblabel='In ' + str(VL1) + ' V / ' + str(AL2) + ' A',
                    #       smlrblabel='Out ' + str(VL3) + ' V / ' + str(AL3) + ' A',
                    #       toplabel='DCDC Converter')
                    # d.add(e.BATTERY, d='down', toplabel='Battery ' + str(siNb))
                    d.pop()
                    inv_start =d.add(e.LINE, d='down', l=3* l1)
                    # CBout = 1
                    # d.add(e.CB, d='down', xy=ENDPV.start,
                    #       smlltlabel='In ' + str(VL1) + ' V / ' + str(AL1) + ' A',
                    #       smlrtlabel='Out ' + str(VL2) + ' V / ' + str(AL2) + ' A',
                    #       toplabel='Combiner Box ' + str(siNb)
                    #                + '\nInputs: ' + str(Mp)
                    #                + '\nOutputs: ' + str(CBout))
                    # d.push()
                    # d.add(e.LINE, d='left', l=l1)
                    # d.add(e.CONVDCDC, d='down',
                    #       smllblabel='In ' + str(VL1) + ' V / ' + str(AL2) + ' A',
                    #       smlrblabel='Out ' + str(VL3) + ' V / ' + str(AL3) + ' A',
                    #       toplabel='DCDC Converter')
                    # d.add(e.BATTERY, d='down', toplabel='Battery ' + str(siNb))
                    # d.pop()
                    # d.add(e.LINE, d='down', l=2 * l1)

                else:
                    # import pdb
                    # pdb.set_trace();
                    print("hi")
                    CBout = 1
                    d.push()
                    count = 0
                    l = len(data_raw) - 1
                    if len(end_point) >= 3 and len(data_raw) > 3:
                        count_raw = 5
                        count_len = 10 + (1 * l)
                        count_theta = -30
                        count_lens = 3 + l
                        count_thetas = 3.6 + l
                    else:
                        count_raw = 16
                        count_len = 13
                        count_theta = 0
                    # count_raw = 14
                    for line in end_point:
                        count += 1
                        if len(data_raw) > 2  and len(data_raw)!=4and len(end_point) == 2:
                            ends = d.add(e.LINE, d='down', xy=line.start, l=8)
                            # if count % 2 == 0:
                            #         d.add(e.LINE, d='left', xy=ends.end, l=2.5)
                            #         x = d.add(e.LINE, d='down', l=3 * l1)
                            # else:
                            #     if len(end_point) > 1:
                            #         if len(data_raw)<=3:
                            #             d.add(e.LINE, d='right', xy=ends.end, l=l1)
                            #             d.add(e.LINE, d='down', theta=-90, l=12.6)
                            #         else:
                            #             d.add(e.LINE, d='right', xy=ends.end, l=2)
                            #             d.add(e.LINE, d='down', theta=-90, l=12.6)
                            #     else:
                            #         d.add(e.LINE, d='right', xy=ends.end, l=l1)
                            #         d.add(e.LINE, d='down', l=3 * l1)
                        if len(data_raw) == 4 and len(end_point) == 2:
                            ends = d.add(e.LINE, d='down', xy=line.start, l=l1)
                            if count % 2 == 0:
                                d.add(e.LINE, d='left', xy=ends.end, l=l1)
                                x = d.add(e.LINE, d='down', l=3 * l1)
                            else:
                                if len(end_point) > 1:
                                    d.add(e.LINE, d='right', xy=ends.end, l=1.5)
                                    d.add(e.LINE, d='down',theta=-90, l=18.6)
                                else:
                                    d.add(e.LINE, d='right', xy=ends.end, l=l1)
                                    d.add(e.LINE, d='down', l=3 * l1)

                        if len(data_raw) > 2 and len(end_point) > 2 and (count == len(end_point)):
                            ends = d.add(e.LINE, d='down', xy=line.start, l=8)
                        if len(data_raw) >= 4 and len(end_point) ==1:
                            ends = d.add(e.LINE, d='down', xy=line.start, l=8)

                    for line in end_two:
                        count += 1
                        if len(data_raw) == 2 and len(end_two)!=1:
                            ends = d.add(e.LINE, d='down', xy=end_two[line].start, l=l1)
                            if count % 2 == 0:
                                d.add(e.LINE, d='right', xy=ends.end, l=1)
                                d.add(e.LINE, d='down', l=9.3* l1)
                            else:
                                d.add(e.LINE, d='left', xy=ends.end, l=1)
                                d.add(e.LINE, d='down', l=3 * l1)
                        if len(data_raw) ==2 and len(end_two)==1:
                            ends = d.add(e.LINE, d='down', xy=end_two[line].start, l=8)
                        if len(data_raw) == 1:
                            ends = d.add(e.LINE, d='down', xy=end_two[line].start, l=4 * l1)

                    d.push()
                    # d_right =d.add(e.LINE, d='right', l=2 * l1)
                    # d.add(e.CONVDCDC, d='down',
                    #       smllblabel='In ' + str(VL1) + ' V / ' + str(AL2) + ' A',
                    #       smlrblabel='Out ' + str(VL3) + ' V / ' + str(AL3) + ' A',
                    #       toplabel='DCDC Converter')
                    # d.add(e.BATTERY, d='down', toplabel='Battery ' + str(siNb))
                    d.pop()
                    inv_start = d.add(e.LINE, d='down', l=6* l1)
                    # d.add(e.CB, d='down', xy=ENDPV.start,
                    #       smlltlabel='In ' + str(VL1) + ' V / ' + str(AL1) + ' A',
                    #       smlrtlabel='Out ' + str(VL2) + ' V / ' + str(AL2) + ' A',
                    #       toplabel='Combiner Box ' + str(siNb)
                    #                + '\nInputs: ' + str(Mp)
                    #                + '\nOutputs: ' + str(CBout))
                global inv;
                inv= d.add(e.INVDCAC, d='down',
                      smlltlabel='In ' + str(VL2) + ' V / ' + str(AL2) + ' A',
                      smlrtlabel='Out ' + str(VL3) + ' V / ' + str(AL3) + ' A',
                      toplabel='Inverter Nr ' + str(siNb) +
                               '\nMax Inputs: ' + str(MPPTin)
                               + '\nUsed Inputs: ' + str(CBout))
                inv_list.append(inv)
                count = 0
                lengths = 19
                l5=18
                x=-3.2
                y=0
                end_points=end_points
                dict_lenght = 0
                p=0.6
                for end in end_points:
                    dict_lenght +=1
                    if len(data_raw) > 2 and len(end_point) >2 and (count != len(data_raw)):
                        if frequency[end]>1:
                            d_point = d.add(e.LINE, d='down', xy=end_points[end].start, l=lengths)
                            # d.add(e.LINE, theta=0,xy=d_point.end,to=inv_start.end- [0.5,0.1])
                        else:
                            d_point = d.add(e.LINE, d='down', xy=end_points[end].start, l=l5)
                        if dict_lenght !=len(end_points):
                            point =d.add(e.LINE,theta=0,xy=d_point.end, tox=inv_start.end + [x, y])
                            d.add(e.LINE,d='down',xy=point.end,toy=inv.start-p)
                    lengths -= 0.5
                    l5 -=0.2;
                    x+=0.2
                    y-=1
                    p+=0.001

            else:
                # import pdb
                # pdb.set_trace();
                if Bat == 2:  # if battery per BB do this
                    CBout = 1
                    direction = 'right'
                    count = 0
                    l = len(data_raw) - 1
                    if len(end_point) >= 3 and len(data_raw) > 3:
                        count_raw = 5
                        count_len = 10 + (1 * l)
                        count_theta = -30
                        count_lens = 3 + l
                        count_thetas = 3.6 + l
                    else:
                        count_raw = 16
                        count_len = 13
                        count_theta = 0
                    # count_raw = 14
                    for line in end_point:
                        count += 1
                        if len(data_raw) > 2 and len(data_raw)!=4 and len(end_point) == 2:
                            ends = d.add(e.LINE, d='down', xy=line.start, l=8)
                            # if count % 2 == 0:
                            #     d.add(e.LINE, d='left', xy=ends.end, l=l1)
                            #     x = d.add(e.LINE, d='down', l=3 * l1)
                            # else:
                            #     if len(end_point) > 1:
                            #         d.add(e.LINE, d='right', xy=ends.end, l=l1)
                            #         d.add(e.LINE, d='down', theta=-90, l=12.6)
                            #     else:
                            #         d.add(e.LINE, d='right', xy=ends.end, l=l1)
                            #         d.add(e.LINE, d='down', l=3 * l1)
                        if len(data_raw) == 4 and len(end_point) == 2:
                            ends = d.add(e.LINE, d='down', xy=line.start, l=l1)
                            if count % 2 == 0:
                                d.add(e.LINE, d='left', xy=ends.end, l=l1)
                                x = d.add(e.LINE, d='down', l=3 * l1)
                            else:
                                if len(end_point) > 1:
                                    d.add(e.LINE, d='right', xy=ends.end, l=l1)
                                    d.add(e.LINE, d='down', theta=-90, l=18.6)
                                else:
                                    d.add(e.LINE, d='right', xy=ends.end, l=l1)
                                    d.add(e.LINE, d='down', l=3 * l1)

                        if len(data_raw) > 2 and len(end_point) > 2 and (count == len(end_point)):
                            ends = d.add(e.LINE, d='down', xy=line.start, l=8)
                        if len(data_raw) >= 4 and len(end_point) ==1 :
                            ends = d.add(e.LINE, d='down', xy=line.start, l=8)
                    for line in end_two:
                        count += 1
                        if len(data_raw) == 2 and len(end_two)!=1:
                            ends = d.add(e.LINE, d='down', xy=end_two[line].start, l=l1)
                            if count % 2 == 0:
                                d.add(e.LINE, d='right', xy=ends.end, l=1)
                                d.add(e.LINE, d='down', l=9.3* l1)
                            else:
                                d.add(e.LINE, d='left', xy=ends.end, l=1)
                                d.add(e.LINE, d='down', l=3 * l1)
                        if len(data_raw) == 2 and len(end_two) == 1:
                            ends = d.add(e.LINE, d='down', xy=end_two[line].start, l=8)
                        if len(data_raw) == 1:
                            ends = d.add(e.LINE, d='down', xy=end_two[line].start, l=4 * l1)

                    d.push()
                    # d.add(e.LINE, d='right', l=2 * l1)
                    # d.add(e.CONVDCDC, d='down',
                    #       smllblabel='In ' + str(VL1) + ' V / ' + str(AL2) + ' A',
                    #       smlrblabel='Out ' + str(VL3) + ' V / ' + str(AL3) + ' A',
                    #       toplabel='DCDC Converter')
                    # d.add(e.BATTERY, d='down', toplabel='Battery ' + str(siNb))
                    d.pop()
                    inv_start = d.add(e.LINE, d='down', l=6 * l1)
                    # global inv;
                    inv = d.add(e.INVDCAC,
                          smlltlabel='In ' + str(VL1) + ' V / ' + str(AL2) + ' A',
                          smlrtlabel='Out ' + str(VL3) + ' V / ' + str(AL3) + ' A',
                          toplabel='Inverter Nr ' + str(siNb) +
                                   '\nMax Inputs: ' + str(MPPTin)
                                   + '\nUsed Inputs: ' + str(1))
                    inv_list.append(inv)
                    count = 0
                    lengths = 19
                    l5 = 18
                    x = -3.2
                    y = 0
                    end_points = end_points
                    dict_lenght = 0
                    p = 0.6
                    for end in end_points:
                        dict_lenght += 1
                        if len(data_raw) > 2 and len(end_point) > 2 and (count != len(data_raw)):
                            if frequency[end] > 1:
                                d_point = d.add(e.LINE, d='down', xy=end_points[end].start, l=lengths)
                                # d.add(e.LINE, theta=0,xy=d_point.end,to=inv_start.end- [0.5,0.1])
                            else:
                                d_point = d.add(e.LINE, d='down', xy=end_points[end].start, l=l5)
                            if dict_lenght != len(end_points):
                                point = d.add(e.LINE, theta=0, xy=d_point.end, tox=inv_start.end + [x, y])
                                d.add(e.LINE, d='down', xy=point.end, toy=inv.start -p)
                        lengths -= 0.5
                        l5 -= 0.2
                        x += 0.2
                        y -= 1
                        p += 0.001



                else:  # if NO battery  do this
                    # import pdb
                    # pdb.set_trace();
                    CBout = 1
                    direction = 'right'
                    count = 0
                    l = len(data_raw) - 1
                    if len(end_point) >= 3 and len(data_raw) > 3:
                        count_raw = 5
                        count_len = 10 + (1 * l)
                        count_theta = -30
                        count_lens = 3 + l
                        count_thetas = 3.6 + l
                    else:
                        count_raw = 16
                        count_len = 13
                        count_theta = 0
                    # count_raw = 14
                    for line in end_point:
                        count += 1
                        if len(data_raw) > 2 and len(data_raw) !=4 and len(end_point) == 2:
                            ends = d.add(e.LINE, d='down', xy=line.start, l=8)
                            # if count % 2 == 0:
                            #     d.add(e.LINE, d='left', xy=ends.end, l=l1)
                            #     x = d.add(e.LINE, d='down', l=3 * l1)
                            # else:
                            #     if len(end_point) > 1:
                            #         d.add(e.LINE, d='right', xy=ends.end, l=l1)
                            #         d.add(e.LINE, d='down', theta=-90, l=12.6)
                            #     else:
                            #         d.add(e.LINE, d='right', xy=ends.end, l=l1)
                            #         d.add(e.LINE, d='down', l=3 * l1)
                        if len(data_raw) == 4 and len(end_point) == 2:
                            ends = d.add(e.LINE, d='down', xy=line.start, l=l1)
                            if count % 2 == 0:
                                d.add(e.LINE, d='left', xy=ends.end, l=l1)
                                x = d.add(e.LINE, d='down', l=3 * l1)
                            else:
                                if len(end_point) > 1:
                                    d.add(e.LINE, d='right', xy=ends.end, l=l1)
                                    d.add(e.LINE, d='down', theta=-90, l=18.6)
                                else:
                                    d.add(e.LINE, d='right', xy=ends.end, l=l1)
                                    d.add(e.LINE, d='down', l=3 * l1)

                        if len(data_raw) > 2 and len(end_point) > 2 and (count == len(end_point)):
                            ends = d.add(e.LINE, d='down', xy=line.start, l=8)
                        if len(data_raw) >= 4 and len(end_point) == 1:
                            ends = d.add(e.LINE, d='down', xy=line.start, l=8)

                    for line in end_two:
                        count += 1
                        if len(data_raw) == 2 and len(end_two)!=1:
                            ends = d.add(e.LINE, d='down', xy=end_two[line].start, l=l1)
                            if count % 2 == 0:
                                d.add(e.LINE, d='right', xy=ends.end, l=1)
                                d.add(e.LINE, d='down', l=9.3 * l1)
                            else:
                                d.add(e.LINE, d='left', xy=ends.end, l=1)
                                d.add(e.LINE, d='down', l=3 * l1)
                        if len(data_raw)==2 and len(end_two)==1:
                            ends = d.add(e.LINE, d='down', xy=end_two[line].start, l=8)
                        if len(data_raw) == 1:
                            ends = d.add(e.LINE, d='down', xy=end_two[line].start, l=4 * l1)

                    d.push()
                    # d.add(e.LINE, d='right', l=2 * l1)
                    # d.add(e.CONVDCDC, d='down',
                    #       smllblabel='In ' + str(VL1) + ' V / ' + str(AL2) + ' A',
                    #       smlrblabel='Out ' + str(VL3) + ' V / ' + str(AL3) + ' A',
                    #       toplabel='DCDC Converter')
                    # d.add(e.BATTERY, d='down', toplabel='Battery ' + str(siNb))
                    d.pop()
                    inv_start = d.add(e.LINE, d='down', l=6 * l1)
                    # global inv;
                    inv = d.add(e.INVDCAC,
                                smlltlabel='In ' + str(VL1) + ' V / ' + str(AL2) + ' A',
                                smlrtlabel='Out ' + str(VL3) + ' V / ' + str(AL3) + ' A',
                                toplabel='Inverter Nr ' + str(siNb) +
                                         '\nMax Inputs: ' + str(MPPTin)
                                         + '\nUsed Inputs: ' + str(1))
                    inv_list.append(inv)
                    count = 0
                    lengths = 19
                    l5 = 18
                    x = -3.2
                    y = 0
                    end_points = end_points
                    dict_lenght = 0
                    p = 0.6
                    for end in end_points:
                        dict_lenght += 1
                        if len(data_raw) > 2 and len(end_point) > 2 and (count != len(data_raw)):
                            if frequency[end] > 1:
                                d_point = d.add(e.LINE, d='down', xy=end_points[end].start, l=lengths)
                                # d.add(e.LINE, theta=0,xy=d_point.end,to=inv_start.end- [0.5,0.1])
                            else:
                                d_point = d.add(e.LINE, d='down', xy=end_points[end].start, l=l5)
                            if dict_lenght != len(end_points):
                                point = d.add(e.LINE, theta=0, xy=d_point.end, tox=inv_start.end + [x, y])
                                d.add(e.LINE, d='down', xy=point.end, toy=inv.start - p)
                        lengths -= 0.5
                        l5 -= 0.2
                        x += 0.2
                        y -= 1
                        p += 0.001

            iNb = iNb + 1

        # ----------------------------------------------------------------------------

        # Placement of the BB label on y-axis
        BBLabelY = 0
        BBLabelY = -3.2

        # Placement of the BB label on x-axis
        BBLabelX = -0.5

        # Calling the main function bb(), to create the Building Blocks:
        raw = 0
        no=0
        # import pdb
        # pdb.set_trace();
        if NumBB == 1:
            for key in res_data:
                no += 1
                # import pdb
                # pdb.set_trace();
                print("res_data",res_data[key])
                raw += 1
                if raw == 1:
                    ENDBB = d.add(e.GND, botlabel='GND', l=l1)
                    d.add(e.DOT)
                    d.add(e.LABEL, xy=ENDBB.start + [BBLabelX, BBLabelY], titlabel='BB  ' + str(1))
                    bb(res_data[key][:-1],res_data[key][-1])
                    BB1DOT = d.add(e.DOT)
                else:
                    ENDBB = d.add(e.GND, xy=dots_list[-1].start + [l1 * 5, 0],
                                  botlabel='GND', l=l1)
                    d.add(e.DOT, xy=ENDBB.start)
                    d.add(e.LABEL, xy=ENDBB.start + [BBLabelX, BBLabelY],
                          titlabel='BB  ' + str(no))
                    bb(res_data[key][:-1],res_data[key][-1])
                    d.add(e.SEP,xy=inv.end,to=BB1DOT.start-[0,0], l=l1)

        else:
            # import pdb;
            # pdb.set_trace()
            for key in res_data:
                raw += 1
                no+= 1
                if raw == 1:
                    ENDBB = d.add(e.GND, botlabel='GND', l=l1)
                    d.add(e.DOT)
                    d.add(e.LABEL, xy=ENDBB.start + [BBLabelX, BBLabelY], titlabel='BB  ' + str(1))
                    bb(res_data[key][:-1],res_data[key][-1])
                    BB1DOT = d.add(e.DOT)
                else:
                    ENDBB = d.add(e.GND, xy=dots_list[-1].start + [l1 * 5, 0],
                                      botlabel='GND', l=l1)
                    d.add(e.DOT, xy=ENDBB.start)
                    d.add(e.LABEL, xy=ENDBB.start + [BBLabelX, BBLabelY],
                          titlabel='BB  ' + str(no))
                    bb(res_data[key][:-1],res_data[key][-1])
                    d.add(e.SEP, xy=inv.end,to=inv_list[0].end, l=l1)
        # # -----------------------------------------------------------------------------
        #
        # # The Combiner Box after the Building Blocks
        #
        if NumBB > 1:
            d.add(e.CB, d='down',xy=inv_list[0].end,
                  smlltlabel='In ' + str(VL3) + ' V / ' + str(AL3) + ' A',
                  smlrtlabel='Out ' + str(VL3) + ' V / ' + str(AL3 * NumBB) + ' A',
                  toplabel='Combiner Box All BB'
                           + '\nInputs: ' + str(NumBB)
                           + '\nOutputs: 1')

        # -----------------------------------------------------------------------------

        # If the system uses only one Battery, it gets added here:

        # if Bat == 1:
        #     d.push()
        #     d.add(e.LINE, d='left', l=2*l1)
        #     d.add(e.INVACDC, d='down',
        #           smllblabel='In ' + str(VL3) + ' V / ' + str(AL3) + ' A',
        #           smlrblabel='Out ' + str(VL3) + ' V / ' + str(AL3) + ' A',
        #           toplabel='Inverter transformer')
        #     d.add(e.BATTERY, d='down', toplabel='Battery All BB')
        #     d.pop()
        #     d.add(e.LINE, d='down', l=1*l1)

        # -----------------------------------------------------------------------------

        # The connection to the electrical grid:
        if NumBB == 1:
            Grid = d.add(e.GRID, d='down',xy=inv.end, toplabel='Grid connection')
        else:
            Grid = d.add(e.GRID, d='down', toplabel='Grid connection')

        # -----------------------------------------------------------------------------

        # Finishing the drawing, saving it to a pdf and automatically open it.

    d.add(e.PV, d='down', xy=dots_list[-1].start+[5 * l1 + 1, 0],
          botlabel='PV module\n' + str(PvName) +
                   '\n' + str(PVVolt) + 'V')
    d.add(e.CB, d='down', xy=dots_list[-1].start+[5* l1 + 1, -2.5],
          botlabel='Combiner Box')
    d.add(e.INVDCAC, xy=dots_list[-1].start+[5* l1 + 1, -5],
          botlabel='Inverter DCAC type:\n' + str(InvName))
    # d.add(e.INVACDC, xy=[k * l1 + 1, -7.5],
    #       botlabel='Inverter ACDC type:\n' + str(InvName))
    # d.add(e.CONVDCDC, xy=[k * l1 + 1, -10],
    #       botlabel='DCDC Converter type:\n' + str(InvName))
    # d.add(e.BATTERY, xy=[k * l1 + 1, -12.5],
    #       botlabel='Battery type:\n' + str(BatName))
    d.add(e.LINE, d='right', xy=dots_list[-1].start+[5 * l1 - 0.5, -7.5], l=l1,
          rgtlabel='Cable type:\n' + str(Cable))

    d.pop()  # returns the drawing cursor to the last d.push() --> [0 / 0]

    # -----------------------------------------------------------------------------

    # Writing the title above the drawing

    d.push()
    d.add(e.LABEL, xy=[0, 1], titlabel='Electrical scheme  ' + str(ProjName))
    d.pop()
    d.draw()

    d.fig.savefig(svg_file2_name, bbox_extra_artists=d.ax.get_default_bbox_extra_artists(),
                  bbox_inches='tight', pad_inches=0.15, dpi=360)
    d.fig.savefig(pdf_file2_name, bbox_extra_artists=d.ax.get_default_bbox_extra_artists(),
                  bbox_inches='tight', pad_inches=0.15, dpi=360)

    subprocess.Popen([svg_file2_name], shell=True)
    # subprocess.Popen([pdf_file2_name], shell=True)

res = {"String Config": {1: [[18,18],[18,18]],
                         2:[[18,18],[20,20]],
                         # 3:[[9,9],[5,5,5],[8],[12],[10]],
                         # 4:[[9,9],[5,5]],
                         }}
PVData = {
    'I_mp_ref':39,
    'V_mp_ref':7,
}
INVData = {

}
print(Ec(res, PVData,INVData,"sld.svg","sld.pdf","projn","pv","in"))