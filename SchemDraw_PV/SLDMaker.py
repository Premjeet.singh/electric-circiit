#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 26 10:35:22 2021

@author: vipluv.aga
"""

import matplotlib.pyplot as plt
from sldcircuitsdxf.sld_circuit_diagram import Ec_dxf
from sldcircuitsdxf.sld_circuit_trafouni import Ec_brazilian_dxf
from sldcircuitsdxf.sld_circuit_trafobi import Ec_brazilian_trafobi_dxf
from sldcircuitsdxf.sld_circuit_trafotri import Ec_brazilian_trafotri_dxf
from sldcircuitsdxf.sld_circuit_notrafouni import Ec_brazilian_notrafouni_dxf
from sldcircuitsdxf.sld_circuit_notrafobi import Ec_brazilian_notrafobi_dxf
from sldcircuitsdxf.sld_circuit_notrafotri import Ec_brazilian_notrafotri_dxf
from sldcircuit.PVScheme.sld_circuit_diagram import Ec, Ec_brazilian
from sldcircuitchversion.sld_circuit_basicstructure import Ec_ch_basicstructure_dxf
from sldcircuitchversion.sld_circuit_evchargers import Ec_ch_evchargers_dxf
from sldcircuitchversion.sld_circuit_evchargers_circuitbreakers import Ec_ch_evchargers_circuitbreakers_dxf
from sldcircuitchversion.sld_circuit_batteryac import Ec_ch_batteryac_dxf
from sldcircuitchversion.sld_circuit_batterydc import Ec_ch_batterydc_dxf
from sldcircuitchversion.sld_circuit_circuitbreaker import Ec_ch_circuitbreaker_dxf
from sldcircuitchversion.sld_meter_ev_decoupling import Ec_ch_meterevdecoupling_dxf
from sldcircuitchversion.sld_metering_decoupling import Ec_ch_metering_decoupling_dxf
from sldcircuitchversion.sld_metering import Ec_ch_metering_basicstructures_dxf
from sldcircuitchversion.sld_decoupling import Ec_ch_decoupling_basicstructures_dxf
from sldcircuitchversion.sld_circuit_batteryac_allcomponents import Ec_ch_batteryac_allcomponents_dxf
from sldcircuitchversion.sld_ev_ac import Ec_ch_ev_ac_dxf
from sldcircuitchversion.sld_ev_dc import Ec_ch_ev_dc_dxf
from sldcircuitchversion.sld_charger_ac import Ec_ch_charger_ac_dxf
from sldcircuitchversion.sld_charger_dc import Ec_ch_charger_dc_dxf
from sldcircuitchversion.sld_ac_metering_decoupling import Ec_ch_ac_metering_decoupling_dxf
from sldcircuitchversion.sld_dc_metering_decoupling import Ec_ch_dc_metering_decoupling_dxf
from sldcircuitchversion.sld_ac import Ec_ch_ac_dxf
from sldcircuitchversion.sld_dc import Ec_ch_dc_dxf

class SLDMaker():
    def __init__(self, res,PVData,INVData):
        # self.array4sld = array4sld.get('StringArrayConfig')
        self.res = res
        self.PVData = PVData
        self.INVData = INVData

    def makebraziliansld(self, bucket, svg_file2save, pdf_file2save, ProjName, PvName, InvName, lang, Wp, paco, Nb_Mppt):
        Ec_brazilian(self.res, self.PVData, self.INVData, svg_file2save, pdf_file2save, ProjName, PvName, InvName, lang, Wp, paco, Nb_Mppt)
        success = "SLD brazilian generated"
        return success

    def makesld(self, bucket, svg_file2save, pdf_file2save, ProjName, PvName, InvName, lang, Wp, paco, Nb_Mppt):
        Ec(self.res, self.PVData, self.INVData, svg_file2save, pdf_file2save, ProjName, PvName, InvName, lang, Wp, paco, Nb_Mppt)
        success = "SLD generated"
        return success

    def makedxfsld(self, bucket, dxf_file2save, ProjName, PvName, InvName, lang, Wp, paco, Nb_Mppt):
        Ec_dxf(self.res, self.PVData,  dxf_file2save, ProjName, PvName, InvName, lang, Wp, paco, Nb_Mppt)
        success = "SLD generated"
        return success

    def maketrafouni_braziliansld(self, dxf_file2save, ProjName, InvName, lang, Inverter_current, Nb_Mppt,dict_inv):
        Ec_brazilian_dxf(self.res, self.PVData, dxf_file2save, ProjName,  InvName, lang, Inverter_current, Nb_Mppt,dict_inv)
        success = "SLD trafouni brazilian generated"
        return success

    def maketrafobi_braziliansld(self, dxf_file2save,  ProjName, InvName, lang,Inverter_current, Nb_Mppt):
        Ec_brazilian_trafobi_dxf(self.res, self.PVData, dxf_file2save, ProjName, InvName, lang, Inverter_current, Nb_Mppt)
        success = "SLD trafobi brazilian generated"
        return success

    def maketrafotri_braziliansld(self, dxf_file2save,  ProjName, InvName, lang, Inverter_current, Nb_Mppt):
        Ec_brazilian_trafotri_dxf(self.res, self.PVData, dxf_file2save, ProjName, InvName, lang, Inverter_current, Nb_Mppt)
        success = "SLD trafotri brazilian generated"
        return success

    def makenotrafouni_braziliansld(self, dxf_file2save, ProjName, InvName, lang, Inverter_current, Nb_Mppt):
        Ec_brazilian_notrafouni_dxf(self.res, self.PVData, dxf_file2save, ProjName, InvName, lang, Inverter_current, Nb_Mppt)
        success = "SLD notrafouni brazilian generated"
        return success

    def makenotrafobi_braziliansld(self, dxf_file2save,  ProjName, InvName, lang, Inverter_current, Nb_Mppt):
        Ec_brazilian_notrafobi_dxf(self.res, self.PVData, dxf_file2save, ProjName, InvName, lang, Inverter_current, Nb_Mppt)
        success = "SLD notrafobi  brazilian generated"
        return success

    def makenotrafotri_braziliansld(self, dxf_file2save, ProjName, InvName, lang, Inverter_current, Nb_Mppt):
        Ec_brazilian_notrafotri_dxf(self.res, self.PVData, dxf_file2save, ProjName, InvName, lang, Inverter_current, Nb_Mppt)
        success = "SLD notrafotri brazilian generated"
        return success

    def makebasicstructure_chsld(self, dxf_file2save, ProjName, InvName, lang, Inverter_current, Nb_Mppt):
        Ec_ch_basicstructure_dxf(self.res, self.PVData, dxf_file2save, ProjName, InvName, lang, Inverter_current,
                                    Nb_Mppt)
        success = "SLD basicstructure ch generated"
        return success

    def makeevchargers_chsld(self, dxf_file2save, ProjName, InvName, lang, Inverter_current, Nb_Mppt):
        Ec_ch_evchargers_dxf(self.res, self.PVData, dxf_file2save, ProjName, InvName, lang, Inverter_current,
                                    Nb_Mppt)
        success = "SLD evchargerscircuitbreakers ch generated"
        return success

    def makeevchargers_circuitbreakers_chsld(self, dxf_file2save, ProjName, InvName, lang, Inverter_current, Nb_Mppt):
        Ec_ch_evchargers_circuitbreakers_dxf(self.res, self.PVData, dxf_file2save, ProjName, InvName, lang, Inverter_current,
                                    Nb_Mppt)
        success = "SLD evchargerscircuitbreakers ch generated"
        return success

    def makebatteryac_chsld(self, dxf_file2save, ProjName, InvName, lang, Inverter_current, Nb_Mppt):
        Ec_ch_batteryac_dxf(self.res, self.PVData, dxf_file2save, ProjName, InvName, lang, Inverter_current,
                             Nb_Mppt)
        success = "SLD batteryac component ch generated"
        return success

    def makebatterydc_chsld(self, dxf_file2save, ProjName, InvName, lang, Inverter_current, Nb_Mppt):
        Ec_ch_batterydc_dxf(self.res, self.PVData, dxf_file2save, ProjName, InvName, lang, Inverter_current,
                            Nb_Mppt)
        success = "SLD batterydc component ch generated"
        return success

    def makecircuitbreaker_chsld(self, dxf_file2save, ProjName, InvName, lang, Inverter_current, Nb_Mppt):
        Ec_ch_circuitbreaker_dxf(self.res, self.PVData, dxf_file2save, ProjName, InvName, lang, Inverter_current,
                            Nb_Mppt)
        success = "SLD circuitbreaker component ch generated"
        return success

    def makemeterevdecoupling_chsld(self, dxf_file2save, ProjName, InvName, lang, Inverter_current, Nb_Mppt):
        Ec_ch_meterevdecoupling_dxf(self.res, self.PVData, dxf_file2save, ProjName, InvName, lang, Inverter_current,
                            Nb_Mppt)
        success = "SLD batterydc component ch generated"
        return success

    def makemetering_decoupling_chsld(self, dxf_file2save, ProjName, InvName, lang, Inverter_current, Nb_Mppt):
        Ec_ch_metering_decoupling_dxf(self.res, self.PVData, dxf_file2save, ProjName, InvName, lang, Inverter_current,
                                    Nb_Mppt)
        success = "SLD meteringdecoupling component ch generated"
        return success

    def makemeteringbasicstructures_chsld(self, dxf_file2save, ProjName, InvName, lang, Inverter_current, Nb_Mppt):
        Ec_ch_metering_basicstructures_dxf(self.res, self.PVData, dxf_file2save, ProjName, InvName, lang, Inverter_current,
                                    Nb_Mppt)
        success = "SLD metering component ch generated"
        return success

    def makedecouplingbasicstructures_chsld(self, dxf_file2save, ProjName, InvName, lang, Inverter_current, Nb_Mppt):
        Ec_ch_decoupling_basicstructures_dxf(self.res, self.PVData, dxf_file2save, ProjName, InvName, lang, Inverter_current,
                                      Nb_Mppt)
        success = "SLD decoupling component ch generated"
        return success

    def makebatteryacallcomponents_chsld(self, dxf_file2save, ProjName, InvName, lang, Inverter_current, Nb_Mppt):
        Ec_ch_batteryac_allcomponents_dxf(self.res, self.PVData, dxf_file2save, ProjName, InvName, lang, Inverter_current,
                            Nb_Mppt)
        success = "SLD batteryac all  component ch generated"
        return success

    def make_ev_ac_chsld(self, dxf_file2save, ProjName, InvName, lang, Inverter_current, Nb_Mppt):
        Ec_ch_ev_ac_dxf(self.res, self.PVData, dxf_file2save, ProjName, InvName, lang, Inverter_current,
                            Nb_Mppt)
        success = "SLD evac component ch generated"
        return success

    def make_ev_dc_chsld(self, dxf_file2save, ProjName, InvName, lang, Inverter_current, Nb_Mppt):
        Ec_ch_ev_dc_dxf(self.res, self.PVData, dxf_file2save, ProjName, InvName, lang, Inverter_current,
                            Nb_Mppt)
        success = "SLD evdc component ch generated"
        return success

    def make_charger_ac_chsld(self, dxf_file2save, ProjName, InvName, lang, Inverter_current, Nb_Mppt):
        Ec_ch_charger_ac_dxf(self.res, self.PVData, dxf_file2save, ProjName, InvName, lang, Inverter_current,
                            Nb_Mppt)
        success = "SLD batteryac charger component ch generated"
        return success


    def make_charger_dc_chsld(self, dxf_file2save, ProjName, InvName, lang, Inverter_current, Nb_Mppt):
        Ec_ch_charger_dc_dxf(self.res, self.PVData, dxf_file2save, ProjName, InvName, lang, Inverter_current,
                            Nb_Mppt)
        success = "SLD batterydc component ch generated"
        return success

    def makemeteringdecouplingac_chsld(self, dxf_file2save, ProjName, InvName, lang, Inverter_current, Nb_Mppt):
        Ec_ch_ac_metering_decoupling_dxf(self.res, self.PVData, dxf_file2save, ProjName, InvName, lang, Inverter_current,
                            Nb_Mppt)
        success = "SLD batteryac component ch generated"
        return success

    def makemeteringdecouplingdc_chsld(self, dxf_file2save, ProjName, InvName, lang, Inverter_current, Nb_Mppt):
        Ec_ch_dc_metering_decoupling_dxf(self.res, self.PVData, dxf_file2save, ProjName, InvName, lang, Inverter_current,
                            Nb_Mppt)
        success = "SLD batterydc component ch generated"
        return success

    def makeac_chsld(self, dxf_file2save, ProjName, InvName, lang, Inverter_current, Nb_Mppt):
        Ec_ch_ac_dxf(self.res, self.PVData, dxf_file2save, ProjName, InvName, lang, Inverter_current,
                             Nb_Mppt)
        success = "SLD batteryac component ch generated"
        return success

    def makedc_chsld(self, dxf_file2save, ProjName, InvName, lang, Inverter_current, Nb_Mppt):
        Ec_ch_dc_dxf(self.res, self.PVData, dxf_file2save, ProjName, InvName, lang, Inverter_current,
                     Nb_Mppt)
        success = "SLD dc component ch generated"
        return success


