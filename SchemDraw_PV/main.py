#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed May 26 09:55:19 2021

@author: vipluv.aga
"""

import json, re
import logging
import os
import datetime as dt
import numpy as np
import pandas as pd
import uuid
import pickle
import math
import importlib
import base64
import ast
from middlewareauth import jwt_authenticated

from flask import Flask, request, jsonify
from google.cloud import storage, datastore
from SLDMaker import SLDMaker

# import sentry_sdk
# from sentry_sdk.integrations.flask import FlaskIntegration
# from sentry_sdk.integrations.sqlalchemy import SqlalchemyIntegration
#
# sentry_sdk.init(
#     dsn="https://508d4d25eaa447c38445f1f0bd8ac634@o434693.ingest.sentry.io/5392060",
#     integrations=[FlaskIntegration(),SqlalchemyIntegration()],
#     traces_sample_rate=1.0
# )
#
#
# BUCKET_NAME = os.environ['BUCKET_NAME']
# PV_DB = os.environ['PV_DB_FILENAME']
# PVS_DB = os.environ['PVS_DB_FILENAME']
# INV_DB = os.environ['INV_DB_FILENAME']
# BATT_DB = os.environ['BATT_DB_FILENAME']
# ELECPARAMS = os.environ['ELECPARAMS']
# FINPARAMS = os.environ['FINPARAMS']
# ALLOWED_EXTENSIONS = {'txt','pdf','png','jpg','jpeg','gif','csv'}
# BOM = os.environ['BOM_FOLDER']
# USER_UPLOADS = os.environ['USER_UPLOADS_FOLDER']
# FETCHED_WEATHER = os.environ['FETCHED_WEATHER_FOLDER']
# AUXFILES = os.environ['AUXFILES_FOLDER']
# RESULTS = os.environ['RESULTS_FOLDER']
# MANUALPV_DB = os.environ['MANUAL_PV_DB_FILENAME']
# DBNAME = os.environ['DB_NAME']
#
#
app = Flask(__name__)
#
#
# @app.before_first_request
# def _load_db():
#     global PVDF, INVDF, ELPARM, FINPARM,PVSYSDF,BATTDF
# #    Electrical parameters
#     client = storage.Client()
#     bucket = client.get_bucket(BUCKET_NAME)
#     blob = bucket.get_blob(AUXFILES+ELECPARAMS)
#     e = blob.download_as_string()
#     ELPARM = json.loads(e)
#
# #   Financial parameters
#     blob = bucket.get_blob(AUXFILES+FINPARAMS)
#     f = blob.download_as_string()
#     FINPARM = json.loads(f)
#
#
# #    Full CEC Inverter Dataframe
#     blob = bucket.get_blob(INV_DB)
#     inv = blob.download_as_string()
#     INVDF = pickle.loads(inv)
#     INVDF=INVDF.loc[:,~INVDF.columns.duplicated()]
#
# #   Full CEC PV Dataframe
#     blob = bucket.get_blob(PV_DB)
#     p = blob.download_as_string()
#     PVDF = pickle.loads(p)
#
#     PVDF = PVDF[~PVDF.index.duplicated()]
#     PVDF=PVDF.loc[:,~PVDF.columns.duplicated()]
#
#
#
#     blob = bucket.get_blob(PVS_DB)
#     pvs = blob.download_as_string()
#     PVSYSDF = pickle.loads(pvs)
#     #PVSYSDF.loc['Wp'] = PVSYSDF.loc['I_mp_ref']*PVSYSDF.loc['V_mp_ref']
#     PVSYSDF=PVSYSDF.loc[:,~PVSYSDF.columns.duplicated()]
#     ## Battery Load
#
#     BATTDF = pickle.loads(bucket.get_blob(BATT_DB).download_as_string())
#
# @app.after_request
# def add_corsheader(response):
#
#
#     whitelistpatterns = [re.compile(".*solextron.*\.herokuapp\.com\/?.*"),
#                          re.compile(".*solex-mvp-2.*\.appspot\.com\/?.*"),
#                          re.compile(".*localhost.*"),
#                          re.compile(".*solextron\.com\/?.*"),
#                          re.compile(".*\.run\.*app\/?.*")]
#
#     # whitelist =  ["http://localhost","https://3dscene-dot-solex-mvp-2.appspot.com","http://localhost:4200", "http://localhost:8080",
#     #           "https://solex-ui-ng-dot-solex-mvp-2.appspot.com","https://solcalcmap-dot-solex-mvp-2.appspot.com","http://localhost:4200/",
#     #           "http://solextron-new.herokuapp.com","https://solextron-new.herokuapp.com","https://solextron-solar-calculator.herokuapp.com",
#     #           "https://solex-ui-ng-dot-solex-mvp-2.oa.r.appspot.com"]
#
#
#     weborigin = request.environ.get('HTTP_ORIGIN')
#     referer = request.environ.get('HTTP_REFERER')
#
#     if len([weborigin for Wlist in whitelistpatterns if Wlist.match(str(weborigin))])>0:
#         response.headers["Access-Control-Allow-Origin"] = weborigin
#     elif len([referer for Wlist in whitelistpatterns if Wlist.match(str(referer))])>0:
#         response.headers["Access-Control-Allow-Origin"] = referer
#     else:
#         response.headers["Access-Control-Allow-Origin"] = "http://localhost:4200"
#     response.headers["Access-Control-Allow-Credentials"] = "true"
#     response.headers["Access-Control-Allow-Headers"] = "authorization, content-type"
#     response.headers["Access-Control-Allow-Methods"]=  "GET,POST,OPTIONS"
#
#     return response



@app.route('/sldmaker', methods=['POST'])
# @jwt_authenticated
def sldmaker():
    # selected_lang = "en"
    # lang = request.args.get("lang")
    version = request.args.get("version")
    # if lang:
    #     selected_lang = lang
    # ds = datastore.Client()
    # gcs=storage.Client()
    # bucket = gcs.get_bucket(BUCKET_NAME)
    # message = {}
    # OutputDic = {}
    # req_json=request.get_json()
    # userinput=req_json
    # userinput['email'] = request.email
    #
    # parent_key=ds.key('users',userinput['email'])
    # entity = ds.get(key=ds.key('Project',userinput['project_id'],parent=parent_key))
    # project_name = entity["projectdetails"]["projectname"]
    # PV_NAME = entity['FinalSummary']['OverallSummary']['PVName']
    # INV_NAME = entity['FinalSummary']['OverallSummary']['InvName']
    # if not entity:
    #     message.update({"error": "No project with username {} and project {} exists".format(userinput['email'],userinput['project_id'])})
    #     return jsonify(message),400
    #
    #
    # uniqueid='Uid_'+userinput['email']+'_Pid_'+userinput['project_id']
    # version = entity['Location']['countrycode']
    # if entity.get('StringArrayConfig',{})=={}:
    #     return jsonify({"error":"String Array config not valid"}),400
    #
    # def str2array(s):
    #     # Remove space after [
    #     s=re.sub('\[ +', '[', s.strip())
    #     # Replace commas and spaces
    #     s=re.sub('[,\s]+', ', ', s)
    #     return np.array(ast.literal_eval(s),dtype=object)
    #
    # StringArrayConfigEnt = json.loads(json.dumps(entity.get('StringArrayConfig',{})))
    #
    # Array4SLD = {"String Config": {int(k):str2array(v).tolist() for k,v in StringArrayConfigEnt.items()}}
    Array4SLD = {"String Config": {
                             # 2:[[8],[19],[20],[29],[30],[31],[32]],
                             # 3:[[14,14,14,14],[14,14,14,14],[14,14,14,14]],
                             # 4:[[9,9],[8,8],[10,10]],
        # 5:[[9,9],[9,9]],
        # 6: [[9, 9], [9, 9]],
        # 7: [[9, 9], [9, 9]],
        # 8: [[9, 9], [9, 9]],
        # 9: [[9, 9], [9, 9]],
        # 10: [[9, 9], [9, 9]],
        # 11:[[9,9],[9,9]],
        # 12:[[9,9],[9,9]],
        # 13:[[9,9],[9,9]],
        # 14: [[9, 9], [9, 9]],
        # 15: [[9, 9], [9, 9]],
        # 16: [[9, 9], [9, 9]],
        # 17: [[9, 9], [9, 9]],
        # 18: [[9, 9], [9, 9]],
        # 5: [[9]],
        # 6: [[9, 9]],
        # 7: [[14],[14]],
        # 18: [[9, 9,9]],
        # 7: [[9, 9]],
        19: [[14],[15]],
        # 17: [[9, 9],[9,9]],
        # 18: [[9, 9],[9,9],[10,10]],


                         }}
    # if 'FinalSummary' in entity:
    #     PVName = entity['FinalSummary']['OverallSummary']['PVName'].replace(' ','_')
    #     InvName = entity['FinalSummary']['OverallSummary']['InvName'].replace(' ','_')
    # elif 'SelectedConfig' in entity:
    #     PVName = entity['SelectedConfig']['Config']['PVName']
    #     InvName = entity['SelectedConfig']['Config']['InvName']
    # else:
    #     return jsonify({"error": "Inverter and PV names not avaialble"}), 400
    # PV_MODULE = "350W"
    # if PVName in PVDF:
    #     PVData = PVDF.loc[:,PVName]
    # elif PVName in PVSYSDF:
    #     PVData = PVSYSDF.loc[:,PVName]
    # else:
    #
    #     return jsonify({"error":"PV Module name not found"})
    # PV_MODULE =  PVData.Wp
    # # elif blobpv:
    # #     USERPVDF = pickle.loads(blobpv.download_as_string())
    # #     if PVName in USERPVDF:
    # #         PVModuleData = USERPVDF.loc[:,PVName]
    # #         PV_MODULE = PVModuleData.Wp
    #
    # INVData = INVDF.loc[:,InvName]
    # max_inputs  = INVData.Nb_MPPT
    # INVData_KW = str(INVData.Paco / 1000) + "kW"

    PVData = {
        'I_mp_ref': 39,
        'V_mp_ref': 7,
    }
    INVData = {

    }
    # local_svg_file_name = uniqueid + "_SLD.svg"
    # local_pdf_file_name = uniqueid + "_SLD.pdf"
    # svg_filename2save =  RESULTS + uniqueid + "_SLD.svg"
    # pdf_filename2save =  RESULTS + uniqueid + "_SLD.pdf"

    ## Initialise and run the function
    sld_obj = SLDMaker(Array4SLD, PVData, INVData)
    if version == "BRA":
        dict_inv = {1:"invs1",2:"invs2"}
        max_inputs={"invs1":10,"invs2":20}
        # success = sld_obj.make_chversion_chsld("sld.dxf","sldcircuit",dict_inv,
        #                           "en","50", max_inputs)
        component_names = ["EV","Metering"]
        success = sld_obj.make_chversion_chsld("sld.dxf","sldcircuit","Fronius International GmbH Fronius Symo 10.0-3 480 [480V]",
                                  "en","pv","brazil","mtechlimited","premjeet","2-jan-2020",component_names,"50")
        # dict_inv = {3:"invs1",2:"invs2"}
        # max_inputs={"invs1":10,"invs2":20}
        # inverter_current = {"invs1":10,"invs2":20}
        # success = sld_obj.maketrafouni_braziliansld("sldsss.dxf","sldcircuit",dict_inv,
        #                           "en",inverter_current, max_inputs)
        # dict_inv = {"3":"invs1","2":"invs2"}
        # success = sld_obj.maketrafouni_braziliansld("sldsss.dxf","sldcircuit", "illuminousinverter",
        #                           "en","50", "10")
        # success = sld_obj.makebraziliansld(bucket, local_svg_file_name, local_pdf_file_name, project_name, PV_NAME, INV_NAME,
        #                           selected_lang, PV_MODULE, INVData_KW, max_inputs)
    else:
        success = sld_obj.makedxfsld("bucket","sld.dxf","sldcircuit","pv","illuminousinverter",
                                  "en","350W","250KW","10")

    # if len(success)>0:
    #     blob = bucket.blob(svg_filename2save)
    #     blob.upload_from_filename(local_svg_file_name)
    #     os.remove(local_svg_file_name)
    #     blob = bucket.blob(pdf_filename2save)
    #     blob.upload_from_filename(local_pdf_file_name)
    #     os.remove(local_pdf_file_name)
    #     urltodownload =  "https://www.googleapis.com/storage/v1/b/"+BUCKET_NAME+"/o/"+  \
    #                         svg_filename2save.replace('/','%2F')+"?alt=media"
    #     OutputDic.update({"urltodownload":urltodownload})
    # else:
    #     OutputDic.update({"urltodownload":{}})

    return "hi"

if __name__ == '__main__':

    Envvar = {"Environ":
        {"BUCKET_NAME" : "solbucket",
    "PV_DB_FILENAME": "CECModules09-2020.pkl",
    "PVS_DB_FILENAME": "PvSystModules2021-03.pkl",
    "BATT_DB_FILENAME": "BatteryModel-01-2021.pkl",
    "INV_DB_FILENAME": "BigInvCEC05-2021.pkl",
    "NASA_22YR_FILENAME": "22yr_nasa_monthly.pkl",
    "MANUAL_PV_DB_FILENAME":"ManualPVModules2020-11.pkl",
    "USER_UPLOADS_FOLDER": "UserUploads/",
    "FETCHED_WEATHER_FOLDER": "FetchedWeather/",
    "AUXFILES_FOLDER": "AuxFiles/",
    "RESULTS_FOLDER": "Results/",
    "BOM_FOLDER":"BoM/",
    "ELECPARAMS": "elec_design_params.json",
    "FINPARAMS": "finparameter_inputs.json",
    "SOLARCALCLEADS":"CalcLeads/",
    "GOOGLE_CLOUD_PROJECT": "solex-mvp-2",
    "SQLALCHEMY_DATABASE_URI" : "mysql+pymysql://root:default123@/refdata?unix_socket=/cloudsql/solex-mvp-2:europe-west6:component-cost",
    "DB_NAME":"ref_agrola",


    } }
    for var in Envvar['Environ']:
        os.environ[var]=Envvar['Environ'][var]
    # os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = os.path.join("/Users","vipluv.aga","Code","auth",
    #                                                             "gcloudauthkeys","solex-mvp-2-e18a10831f31.json")
    credential_path = r"C:\Users\dell\Downloads\application_default_credentials.json"
    # project=r"solex-mvp-2"
    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = credential_path
    os.environ['SQLALCHEMY_LOCALDB_URI'] = "mysql+pymysql://root:default123@127.0.0.1:3306/refdata"
    os.environ['DB_HOST']='127.0.0.1'

    os.environ['FLASK_ENV']='development'

    app.run(host='127.0.0.1', port=8080, debug=True, use_reloader=True)
