import json  # for writing, saving and opening JSON files
# import matplotlib
import ezdxf
from ezdxf.addons import Importer
import random
import os
# d = schem.Drawing()
# d.push()
l1=2
global max_y_limit
# max_y_limit=0
def draw(first_key,end_points,last_key,MPPTin,dict_lenght,data_raw_length,ROUND_CIRCLE,lengths1,lengths2,total_length,len_data,frequency_count_list,doc,msp):
    # line_down = msp.add_blockref(ROUND_CIRCLE, end_points.dxf.insert+(-1500, -200), dxfattribs={
    #     'xscale':1500,
    #     'yscale': 1000,
    #     'rotation': 0
    # })
    dir2save = os.getcwd()
    string_file = os.path.join(dir2save, 'sldcircuitsdxf', 'dxfinput', 'StringPROTSS.dxf')
    files = [string_file]
    x = -1
    # doc.layers.new(name='MyCircles', dxfattribs={'color': 4})
    for file in files:
        x += 1
        source_dxf = ezdxf.readfile(file)
        importer = Importer(source_dxf, doc)
        importer.import_block('StringsProt')
        importer.finalize()
        block = doc.blocks.get('StringsProt')
        block = block.block
        base_point = block.dxf.base_point
        block.dxf.base_point = (1600, -48000)
        base_point = block.dxf.base_point
    PH2=doc.blocks.get('PH2')
    if not PH2:
        PH = doc.blocks.new(name='PH2')
        PH.add_line((0, 0), (0.59, 0))
        PH.add_line((0.59, 0), (0.59, -0.09))
    PH3 = doc.blocks.get('PH3')
    if not PH3:
        PH3 = doc.blocks.new(name='PH3')
        PH3.add_line((0, 0), (0, 1)),
        PH3.add_line((0, 0), (-0.5, -0.5)),
        PH3.add_line((-0.25, -0.25), (-0.25, 0)),
        PH3.add_line((-0.32, -0.25), (-0.32, 0)),
    arcs = doc.blocks.get('arc')
    if not arcs:
        arcs2 = doc.blocks.new(name='arc')
        arcs2.add_arc(center=(1.05, 0.5), radius=0.02, start_angle=0, end_angle=180)
    circle3 = doc.blocks.get('circle')
    if not circle3:
        circle_3 = doc.blocks.new(name='circle')
        circle_3.add_circle(center=(1,0),radius=0.02,color=7)

    PH_labels=doc.blocks.get('PH2_labels')
    if not PH_labels:
        labels = doc.blocks.new(name='PH2_labels')
        labels.add_attdef('labels_name', end_points.dxf.insert + (-600,300), dxfattribs={'height': 200, 'color': 7})
        labels.add_attdef('labels_name2', end_points.dxf.insert + (-600, -100), dxfattribs={'height':200, 'color': 7})
        labels.add_attdef('labels_name3', end_points.dxf.insert + (-600, -300), dxfattribs={'height': 200, 'color': 7})
        labels.add_attdef('labels_name4', end_points.dxf.insert + (-600, -600), dxfattribs={'height': 200, 'color': 7})
        labels.add_attdef('labels_name5', end_points.dxf.insert + (-600, 800), dxfattribs={'height': 150, 'color': 7})
        labels.add_attdef('labels_names', end_points.dxf.insert + (-1300, 1500),
                                dxfattribs={'height': 150, 'color': 7})
        labels.add_attdef('labels_names2', end_points.dxf.insert + (-2500, 1500),
                          dxfattribs={'height': 150, 'color': 7})
        labels.add_attdef('labels_names3', end_points.dxf.insert + (-1550, 800),
                                dxfattribs={'height': 200, 'color': 7})
        labels.add_attdef('labels_names5', end_points.dxf.insert + (-400, -1200),
                          dxfattribs={'height': 150, 'color': 7})
        labels.add_attdef('labels_names6', end_points.dxf.insert + (-600, -16200),
                          dxfattribs={'height': 150, 'color': 7})
        labels.add_attdef('labels_names7', end_points.dxf.insert + (-600, -16500),
                          dxfattribs={'height': 150, 'color': 7})
        labels.add_attdef('labels_names8', end_points.dxf.insert + (-600, -16800),
                          dxfattribs={'height': 150, 'color': 7})
        labels.add_attdef('labels_names9', end_points.dxf.insert + (-600, -17100),
                          dxfattribs={'height': 150, 'color': 7})
        labels.add_attdef('labels_names10', end_points.dxf.insert + (-2000, -16200),
                          dxfattribs={'height': 150, 'color': 7})
        labels.add_attdef('labels_names11', end_points.dxf.insert + (-3900, -17600),
                          dxfattribs={'height': 150, 'color': 7})
        labels.add_attdef('labels_names12', end_points.dxf.insert + (-3900, -17900),
                          dxfattribs={'height': 150, 'color': 7})
        labels.add_attdef('labels_names13', end_points.dxf.insert + (-3000, -18200),
                          dxfattribs={'height': 150, 'color': 7})
    x_pos=5
    values = {
        'labels_name':"HEPR 90º 1,0kV",
        'labels_name2': "Eletroduto",
        'labels_name3': "galvanizado Ø ",
        'labels_name4': "???",
        'labels_name5': "String" + str(dict_lenght) + "- MPPt " + str(dict_lenght),
        'labels_names': str(dict_lenght),
        'labels_names2': str(dict_lenght),
        'labels_names3': "4mm2",
        'labels_names6': "PS 175V 20kA",
        'labels_names7': "Inominal = 10kA",
        'labels_names8':"Imáxima = 20kA",
        'labels_names9': "Classe 2",
        'labels_names10': "100V 40kA",
        'labels_names11': "#6mm2 PVC",
        'labels_names12': "70 750V",
        'labels_names13': "100 V 25kA",

        'XPOS': x_pos,
        'YPOS': 0
    }
    values2={
        'labels_names5': "o Mesmo Eletroduto",
    }
    values3 = {
        'labels_names10': "100V 40kA",
        'labels_names11': "#6mm2 PVC",
        'labels_names12': "70 750V",
        'labels_names13': "100V 25kA",
    }

    point = (end_points.dxf.insert)+(-500,-15000)
    # msp.add_text(str(dict_lenght), dxfattribs={
    #     'height': 100}).set_pos((end_points.dxf.insert)+(-390,-90), align='CENTER')
    # msp.add_text(str(dict_lenght), dxfattribs={
    #     'height': 100}).set_pos((end_points.dxf.insert)+(-1200, -90), align='CENTER')
    # msp.add_text("4mm2", dxfattribs={
    #     'height': 100}).set_pos((end_points.dxf.insert)+(-500, -500), align='CENTER')
    msp.add_text("String" + str(dict_lenght) +"-MPPT"+ str(dict_lenght), dxfattribs={
        'height': 90,'color':1}).set_pos((end_points.dxf.insert)+(670, -450), align='CENTER')
    # msp.add_text("HEPR 90º 1,0kV", dxfattribs={
    #     'height': 35}).set_pos((end_points.dxf.insert)+(350, -600), align='CENTER')
    # msp.add_text("Eletroduto", dxfattribs={
    #     'height': 35}).set_pos((end_points.dxf.insert)+(330, -750), align='CENTER')
    # msp.add_text("galvanizado Ø ", dxfattribs={
    #     'height': 35}).set_pos((end_points.dxf.insert)+(350, -900), align='CENTER')
    # msp.add_text("#6mm2 PVC", dxfattribs={
    #     'height': 42}).set_pos((end_points.dxf.insert)+(-1200, -2900), align='CENTER')
    # msp.add_text("70 750V", dxfattribs={
    #     'height': 42}).set_pos((end_points.dxf.insert)+(-1200, -3000), align='CENTER')
    # if dict_lenght < total_length:
    #     msp.add_text("o Mesmo Eletroduto", dxfattribs={
    #         'height': 80}).set_pos((end_points.dxf.insert) + (1480, -1100), align='CENTER')

    line_left = msp.add_blockref('circle', end_points.dxf.insert + (-50, -5500), dxfattribs={
        'xscale':0.003,
        'yscale':0.003,
        'rotation': 0
    })
    new_RBOX_block = doc.blocks.get(name='new_RBOX')
    if not new_RBOX_block:
        new_RBOX = doc.blocks.new(name='new_RBOX')
        new_RBOX.add_lwpolyline([(0, 0.5), (0.2, 0.5), (0.2, 0), (0, 0), (0, 0.5)]),
        new_RBOX.add_line((-0.2, 0.3), (0.5, 0.5))
        new_RBOX.add_line((-0.2, 0.3), (-0.2, 0.25))
        new_RBOX.add_line((0.1, 0), (0.1, -0.5))
        new_RBOX.add_line((0.02, -0.4), (0.2, -0.4))
        new_RBOX.add_line((0.05, -0.45), (0.15, -0.45))
        new_RBOX.add_line((0.08, -0.5), (0.1, -0.5))
        new_RBOX.add_ellipse((0.1, -0.2), major_axis=(0.39, 0), ratio=0.2)
        new_RBOX.add_line((-0.29, -0.2), (-0.69, -0.2))
        new_RBOX.add_line((-0.43, -0.2), (-0.43, 0.1))
        new_RBOX.add_line((-0.59, 0.1), (-0.29, 0.1))
    new_dashed_block = doc.blocks.get(name='new_dashed')
    if not new_dashed_block:
        DASHED = doc.blocks.new(name='new_dashed')
        # DASHED.add_line((0.5, 0.5), (0.6, 0.5) ,dxfattribs={'color': 5})
        # DASHED.add_line((0.9, 0.5), (1, 0.5), dxfattribs={'color': 5})
        DASHED.add_line((1.3, 0.5), (1.4, 0.5), dxfattribs={'color': 5})
        DASHED.add_line((1.7, 0.5), (1.8, 0.5), dxfattribs={'color': 5})
        DASHED.add_line((2.1, 0.5), (2.2, 0.5), dxfattribs={'color': 5})
        DASHED.add_line((2.5, 0.5), (2.6, 0.5), dxfattribs={'color': 5})
        DASHED.add_line((2.9, 0.5), (3, 0.5), dxfattribs={'color': 5})
    new_dasheds_block = doc.blocks.get(name='new_dasheds')
    new_labels = doc.blocks.get(name='old_labels')
    if not new_dasheds_block :
        DASHEDS = doc.blocks.new(name='new_dasheds')
        # DASHEDS.add_line((0.5, 0.5), (0.5, 0.6), dxfattribs={'color': 5})
        DASHEDS.add_line((0.5, 0.9), (0.5, 1), dxfattribs={'color': 5})
        DASHEDS.add_line((0.5, 1.3), (0.5, 1.4), dxfattribs={'color': 5})
        DASHEDS.add_line((0.5, 1.7), (0.5, 1.8), dxfattribs={'color': 5})
        DASHEDS.add_line((0.5, 2), (0.5, 2.1), dxfattribs={'color': 5})
        if lengths1 == 7600 or lengths1==12100:
            DASHEDS.add_line((0.5, 2.3), (0.5, 2.4), dxfattribs={'color': 5})
            DASHEDS.add_line((0.5, 2.6), (0.5, 2.7), dxfattribs={'color': 5})
            DASHEDS.add_line((0.5, 2.9), (0.5, 3), dxfattribs={'color': 5})



        # DASHEDS.add_line((0.5, 2.3), (0.5, 2.35), dxfattribs={'color': 5})

    panels_block = doc.blocks.get(name='panels_dashedss')
    if not panels_block:
        panels = doc.blocks.new(name='panels_dashedss')
        panels.add_line((0.5, 0.5), (0.5, 1.5), dxfattribs={'color': 5})
        panels.add_line((0.5, 0.5), (1.3, 0.5), dxfattribs={'color': 5})
    panels_block = doc.blocks.get(name='panels_dashedsss')
    if not panels_block:
        panels = doc.blocks.new(name='panels_dashedsss')
        panels.add_line((0.5, 0.5), (0.5, 1.5), dxfattribs={'color': 5})
        panels.add_line((0.5, 0.5), (-1, 0.5), dxfattribs={'color': 5})


    new_dashedss_block = doc.blocks.get(name='new_dashedss')
    new_labels = doc.blocks.get(name='old_labels')
    if not new_dashedss_block:
        DASHEDS = doc.blocks.new(name='new_dashedss')
        DASHEDS.add_line((0.5, 0.5), (0.5, 0.6), dxfattribs={'color': 5})
        DASHEDS.add_line((0.5, 0.9), (0.5, 1), dxfattribs={'color': 5})
        DASHEDS.add_line((0.5, 1.3), (0.5, 1.4), dxfattribs={'color': 5})
        DASHEDS.add_line((0.5, 1.7), (0.5, 1.8), dxfattribs={'color': 5})
        DASHEDS.add_line((0.5, 2), (0.5, 2.1), dxfattribs={'color': 5})
        DASHEDS.add_line((0.5, 2.3), (0.5, 2.4), dxfattribs={'color': 5})
        DASHEDS.add_line((0.5, 2.6), (0.5, 2.7), dxfattribs={'color': 5})
        DASHEDS.add_line((0.5, 2.9), (0.5, 3), dxfattribs={'color': 5})
        DASHEDS.add_line((0.5, 3.2), (0.5, 3.3), dxfattribs={'color': 5})
        DASHEDS.add_line((0.5, 3.5), (0.5, 3.6), dxfattribs={'color': 5})
        DASHEDS.add_line((0.5, 3.8), (0.5, 3.9), dxfattribs={'color': 5})
        DASHEDS.add_line((0.5, 4.1), (0.5, 4.2), dxfattribs={'color': 5})
        DASHEDS.add_line((0.5, 4.4), (0.5, 4.5), dxfattribs={'color': 5})
        DASHEDS.add_line((0.5, 4.7), (0.5, 4.8), dxfattribs={'color': 5})
        DASHEDS.add_line((0.5, 5), (0.5, 5.1), dxfattribs={'color': 5})
        DASHEDS.add_line((0.5, 5.3), (0.5, 5.4), dxfattribs={'color': 5})
        DASHEDS.add_line((0.5, 5.6), (0.5, 5.7), dxfattribs={'color': 5})
        DASHEDS.add_line((0.5, 5.9), (0.5, 6), dxfattribs={'color': 5})
        DASHEDS.add_line((0.5, 6.2), (0.5, 6.3), dxfattribs={'color': 5})
        DASHEDS.add_line((0.5, 6.5), (0.5, 6.6), dxfattribs={'color': 5})
        DASHEDS.add_line((0.5, 6.8), (0.5, 6.9), dxfattribs={'color': 5})
        DASHEDS.add_line((0.5, 7.1), (0.5, 7.2), dxfattribs={'color': 5})
        DASHEDS.add_line((0.5, 7.4), (0.5, 7.5), dxfattribs={'color': 5})
        DASHEDS.add_line((0.5, 7.7), (0.5, 7.8), dxfattribs={'color': 5})
        DASHEDS.add_line((0.5, 8), (0.5, 8.1), dxfattribs={'color': 5})
        DASHEDS.add_line((0.5, 8.3), (0.5, 8.4), dxfattribs={'color': 5})
        DASHEDS.add_line((0.5, 8.6), (0.5, 8.7), dxfattribs={'color': 5})
        DASHEDS.add_line((0.5, 8.9), (0.5, 9), dxfattribs={'color': 5})
    # if not new_labels:
    #     D_labels = doc.blocks.new(name='old_labels')

    line_down_y = msp.add_blockref('circle',end_points.dxf.insert +(-560.29, -52780), dxfattribs={
        'xscale':0.003,
        'yscale': 0.003,
        'rotation': 0
    })
    # msp.add_text("#6mm2 PVC", dxfattribs={
    #     'height': 100}).set_pos((line_down_y.dxf.insert)+(20, 0), align='CENTER')
    line_down_y.add_auto_attribs(values3)
    line_circle_x = msp.add_blockref('circle', line_left.dxf.insert + (-72, 3350), dxfattribs={
        'xscale': 0.0003,
        'yscale': 0.0003,
        'rotation': 0
    })
    line_down_ys = msp.add_blockref('circle', line_left.dxf.insert + (-2060, 910), dxfattribs={
        'xscale': 0.0003,
        'yscale': 0.0003,
        'rotation': 0
    })
    # line_dasheds = msp.add_blockref('new_dashed', end_points.dxf.insert + (-6200, -2500), dxfattribs={
    #     'xscale': 3200,
    #     'yscale': 900,
    #     'rotation': 0,
    #     'color':5,
    # })
    panels_dasheds = msp.add_blockref('new_dashed', end_points.dxf.insert + (-6200, -963), dxfattribs={
        'xscale': 3200,
        'yscale': 900,
        'rotation': 0,
        'color': 5,
    })
    panels_dasheds = msp.add_blockref('new_dashed', end_points.dxf.insert + (-6200, 1700), dxfattribs={
        'xscale': 3200,
        'yscale': 900,
        'rotation': 0,
        'color': 5,
    })
    # line_dashed = msp.add_blockref('new_dashed', end_points.dxf.insert + (-6200, -lengths1), dxfattribs={
    #     'xscale': 3200,
    #     'yscale': 900,
    #     'rotation': 0,
    #     'color':5,
    # })
    if dict_lenght == 1 :
        if lengths1 > 30000:
            labels ='new_dashedss'
        else:
            labels='new_dasheds'
    #     line_dashed = msp.add_blockref(labels, end_points.dxf.insert + (-3000, -60), dxfattribs={
    #     'xscale': 900,
    #     'yscale':-lengths2,
    #     'rotation': 0
    # })
        if len_data == 1 or len_data==2:
            xx=-3700
        else:
            xx=-3000
        panel_dashed = msp.add_blockref('panels_dashedss', end_points.dxf.insert + (xx, 2800), dxfattribs={
            'xscale': 900,
            'yscale': -1300,
            'rotation': 0
        })
        if frequency_count_list == 1:
            gnd_length = -3690
        else:
            gnd_length =-3000
        print("kkdfd",len_data,gnd_length)
        gnd_points = (end_points.dxf.insert + (gnd_length, 2000))
        blockref_line = msp.add_blockref('LINE', gnd_points, dxfattribs={
            'xscale': 500,
            'yscale': 500,
            'rotation': 0
        })
        gnd_points = (end_points.dxf.insert + (gnd_length, -1150))
        blockref = msp.add_blockref('GND', gnd_points, dxfattribs={
            'xscale': 1500,
            'yscale': 1500,
            'rotation': 0
        })
        panels_dashed = msp.add_blockref('panels_dashedss', end_points.dxf.insert + (xx, -1013), dxfattribs={
            'xscale': 900,
            'yscale': 1000,
            'rotation': 0
        })
    if dict_lenght == total_length:
        if lengths1 > 30000:
            labels = 'new_dashedss'
        else:
            labels = 'new_dasheds'
        # line_dashed = msp.add_blockref(labels, end_points.dxf.insert + (3000, -60), dxfattribs={
        #     'xscale': 900,
        #     'yscale': -lengths2,
        #     'rotation': 0
        # })
        panel_dashed = msp.add_blockref('panels_dashedsss', end_points.dxf.insert + (3000,2800), dxfattribs={
            'xscale': 900,
            'yscale': -1300,
            'rotation': 0
        })
        panel_dashed = msp.add_blockref('panels_dashedsss', end_points.dxf.insert + (3000, -1013), dxfattribs={
            'xscale': 900,
            'yscale': 1000,
            'rotation': 0
        })
    return {'d_joints':line_circle_x,'dashed':line_down_ys}


def draws(first_key,end_points,last_key,MPPTin,dict_lenght,data_raw_length,ROUND_CIRCLE,lengths1,lengths2,total_length,total_frequency,len_data,frequency_count_list,doc,msp):
    dir2save = os.getcwd()
    string_file = os.path.join(dir2save, 'sldcircuitsdxf', 'dxfinput', 'StringProtSS.dxf')
    files = [string_file]
    x = -1
    print("heoeodo")
    block_s = ['panels']
    for file in files:
        x += 1
        source_dxf = ezdxf.readfile(file)
        importer = Importer(source_dxf, doc)
        importer.import_block('StringsProt')
        importer.finalize()
        block = doc.blocks.get('StringsProt')
        block = block.block
        base_point = block.dxf.base_point
        block.dxf.base_point = (1600, -48000)
        base_point = block.dxf.base_point
    PH2=doc.blocks.get('PH2')
    if not PH2:
        PH = doc.blocks.new(name='PH2')
        PH.add_line((0, 0), (0.59, 0))
        PH.add_line((0.59, 0), (0.59, -0.09))
    PH3 = doc.blocks.get('PH3')
    if not PH3:
        PH3 = doc.blocks.new(name='PH3')
        PH3.add_line((0, 0), (0, 1)),
        PH3.add_line((0, 0), (-0.5, -0.5)),
        PH3.add_line((-0.25, -0.25), (-0.25, 0)),
        PH3.add_line((-0.32, -0.25), (-0.32, 0)),
    arcs = doc.blocks.get('arc')
    if not arcs:
        arcs2 = doc.blocks.new(name='arc')
        arcs2.add_arc(center=(1.05, 0.5), radius=0.02, start_angle=0, end_angle=180)
    circle3 = doc.blocks.get('circle')
    if not circle3:
        circle_3 = doc.blocks.new(name='circle')
        circle_3.add_circle(center=(1,0),radius=0.02,color=7)

    PH_labels=doc.blocks.get('PH2_labels')
    if not PH_labels:
        labels = doc.blocks.new(name='PH2_labels')
        labels.add_attdef('labels_name', end_points.dxf.insert + (-2000,300), dxfattribs={'height': 200, 'color': 7})
        labels.add_attdef('labels_name2', end_points.dxf.insert + (-2000, -100), dxfattribs={'height':200, 'color': 7})
        labels.add_attdef('labels_name3', end_points.dxf.insert + (-2000, -300), dxfattribs={'height': 200, 'color': 7})
        labels.add_attdef('labels_name4', end_points.dxf.insert + (-2000, -600), dxfattribs={'height': 200, 'color': 7})
        labels.add_attdef('labels_name5', end_points.dxf.insert + (-2000, 800), dxfattribs={'height': 100, 'color': 7})
        labels.add_attdef('labels_names', end_points.dxf.insert + (-4000, 1500),
                                dxfattribs={'height': 150, 'color': 7})
        labels.add_attdef('labels_names2', end_points.dxf.insert + (-3000, 1500),
                          dxfattribs={'height': 150, 'color': 7})
        labels.add_attdef('labels_names3', end_points.dxf.insert + (-3500, 800),
                                dxfattribs={'height': 200, 'color': 7})
        labels.add_attdef('labels_names5', end_points.dxf.insert + (-400, -1200),
                          dxfattribs={'height': 100, 'color': 7})
        labels.add_attdef('labels_names6', end_points.dxf.insert + (-2000, -16200),
                          dxfattribs={'height': 200, 'color': 7})
        labels.add_attdef('labels_names7', end_points.dxf.insert + (-2000, -16500),
                          dxfattribs={'height': 200, 'color': 7})
        labels.add_attdef('labels_names8', end_points.dxf.insert + (-2000, -16800),
                          dxfattribs={'height': 200, 'color': 7})
        labels.add_attdef('labels_names9', end_points.dxf.insert + (-2000, -17100),
                          dxfattribs={'height': 200, 'color': 7})
        labels.add_attdef('labels_names10', end_points.dxf.insert + (-5000, -17000),
                          dxfattribs={'height': 200, 'color': 7})
        labels.add_attdef('labels_names11', end_points.dxf.insert + (-5700, -17600),
                          dxfattribs={'height': 200, 'color': 7})
        labels.add_attdef('labels_names12', end_points.dxf.insert + (-5700, -17900),
                          dxfattribs={'height': 200, 'color': 7})
        labels.add_attdef('labels_names13', end_points.dxf.insert + (-4800, -18200),
                          dxfattribs={'height': 200, 'color': 7})
    x_pos=5
    values = {
        'labels_name':"HEPR 90º 1,0kV",
        'labels_name2': "Eletroduto",
        'labels_name3': "galvanizado Ø ",
        'labels_name4': "???",
        'labels_name5': "String" + str(dict_lenght) +"-MPPt"+ str(dict_lenght),
        'labels_names': str(dict_lenght),
        'labels_names2': str(dict_lenght),
        'labels_names3': "4mm2",
        'labels_names6': "PS 175V 20kA",
        'labels_names7': "Inominal = 10kA",
        'labels_names8':"Imáxima = 20kA",
        'labels_names9': "Classe 2",
        'labels_names10': "100V 40kA",
        'labels_names11': "#6mm2 PVC",
        'labels_names12': "70 750V",
        'labels_names13': "100V 25kA",

        'XPOS': x_pos,
        'YPOS': 0
    }
    values2={
        'labels_names5': "o Mesmo Eletroduto",
    }
    values3 = {
        'labels_names10': "100V 40kA",
        'labels_names11': "#6mm2 PVC",
        'labels_names12': "70 750V",
        'labels_names13': "100V 25kA",
    }

    point = (end_points.dxf.insert)+(-500,-15000)
    # msp.add_text(str(dict_lenght), dxfattribs={
    #     'height': 100}).set_pos((end_points.dxf.insert)+(-390, -120), align='CENTER')
    # msp.add_text(str(dict_lenght), dxfattribs={
    #     'height': 100}).set_pos((end_points.dxf.insert)+(-1200, -120), align='CENTER')
    # msp.add_text("4mm2", dxfattribs={
    #     'height': 100}).set_pos((end_points.dxf.insert)+(-500, -500), align='CENTER')
    msp.add_text("String" + str(dict_lenght) +"-MPPT"+ str(dict_lenght), dxfattribs={
        'height': 90,'color':1}).set_pos((end_points.dxf.insert)+(670, -450), align='CENTER')
    # msp.add_text("HEPR 90º 1,0kV", dxfattribs={
    #     'height': 35}).set_pos((end_points.dxf.insert)+(350, -600), align='CENTER')
    # msp.add_text("Eletroduto", dxfattribs={
    #     'height': 35}).set_pos((end_points.dxf.insert)+(330, -750), align='CENTER')
    # msp.add_text("galvanizado Ø ", dxfattribs={
    #     'height': 35}).set_pos((end_points.dxf.insert)+(350, -900), align='CENTER')
    # msp.add_text("#6mm2 PVC", dxfattribs={
    #     'height': 42}).set_pos((end_points.dxf.insert)+(-1200, -2900), align='CENTER')
    # msp.add_text("70 750V", dxfattribs={
    #     'height': 42}).set_pos((end_points.dxf.insert)+(-1200, -3000), align='CENTER')
    # if dict_lenght < total_length:
    #     msp.add_text("o Mesmo Eletroduto", dxfattribs={
    #         'height': 80}).set_pos((end_points.dxf.insert) + (1480, -1100), align='CENTER')


    line_left = msp.add_blockref('circle', end_points.dxf.insert + (-50, -5500), dxfattribs={
        'xscale':0.003,
        'yscale': 0.003,
        'rotation': 0
    })
    new_RBOX_block = doc.blocks.get(name='new_RBOX')
    if not new_RBOX_block:
        new_RBOX = doc.blocks.new(name='new_RBOX')
        new_RBOX.add_lwpolyline([(0, 0.5), (0.2, 0.5), (0.2, 0), (0, 0), (0, 0.5)]),
        new_RBOX.add_line((-0.2, 0.3), (0.5, 0.5))
        new_RBOX.add_line((-0.2, 0.3), (-0.2, 0.25))
        new_RBOX.add_line((0.1, 0), (0.1, -0.5))
        new_RBOX.add_line((0.02, -0.4), (0.2, -0.4))
        new_RBOX.add_line((0.05, -0.45), (0.15, -0.45))
        new_RBOX.add_line((0.08, -0.5), (0.1, -0.5))
        new_RBOX.add_ellipse((0.1, -0.2), major_axis=(0.39, 0), ratio=0.2)
        new_RBOX.add_line((-0.29, -0.2), (-0.69, -0.2))
        new_RBOX.add_line((-0.43, -0.2), (-0.43, 0.1))
        new_RBOX.add_line((-0.59, 0.1), (-0.29, 0.1))
    new_dashed_block = doc.blocks.get(name='new_dashed')
    if not new_dashed_block:
        DASHED = doc.blocks.new(name='new_dashed')
        # DASHED.add_line((0.5, 0.5), (0.6, 0.5) ,dxfattribs={'color': 5})
        # DASHED.add_line((0.9, 0.5), (1, 0.5), dxfattribs={'color': 5})
        DASHED.add_line((1.3, 0.5), (1.4, 0.5), dxfattribs={'color': 5})
        DASHED.add_line((1.7, 0.5), (1.8, 0.5), dxfattribs={'color': 5})
        if total_frequency ==3:
            DASHED.add_line((2.1, 0.5), (2.2, 0.5), dxfattribs={'color': 5})
            DASHED.add_line((2.5, 0.5), (2.6, 0.5), dxfattribs={'color': 5})
            DASHED.add_line((2.9, 0.5), (3, 0.5), dxfattribs={'color': 5})
        if total_frequency>=4:
            DASHED.add_line((2.1, 0.5), (2.4, 0.5), dxfattribs={'color': 5})
            DASHED.add_line((2.5, 0.5), (2.6, 0.5), dxfattribs={'color': 5})
            DASHED.add_line((2.9, 0.5), (3, 0.5), dxfattribs={'color': 5})
        # if dict_lenght !=total_length and total_length!=1:
        #     print("final",dict_lenght,total_length)
        #     DASHED.add_line((3.3, 0.5), (3.4, 0.5), dxfattribs={'color': 5})
        #     DASHED.add_line((3.5, 0.5), (3.6, 0.5), dxfattribs={'color': 5})
        #     DASHED.add_line((3.7, 0.5), (3.8, 0.5), dxfattribs={'color': 5})

    panels_block = doc.blocks.get(name='panels_dashedss')
    if not panels_block:
        panels = doc.blocks.new(name='panels_dashedss')
        panels.add_line((0.5, 0.5), (0.5, 1.5), dxfattribs={'color': 5})
        panels.add_line((0.5, 0.5), (1.3, 0.5), dxfattribs={'color': 5})
    panels_block = doc.blocks.get(name='panels_dashedsss')
    if not panels_block:
        panels = doc.blocks.new(name='panels_dashedsss')
        panels.add_line((0.5, 0.5), (0.5, 1.5), dxfattribs={'color': 5})
        panels.add_line((0.5, 0.5), (-1, 0.5), dxfattribs={'color': 5})


    new_dasheds_block = doc.blocks.get(name='new_dasheds')
    new_labels = doc.blocks.get(name='old_labels')
    if not new_dasheds_block :
        DASHEDS = doc.blocks.new(name='new_dasheds')
        # DASHEDS.add_line((0.5, 0.5), (0.5, 0.6), dxfattribs={'color': 5})
        DASHEDS.add_line((0.5, 0.9), (0.5, 1), dxfattribs={'color': 5})
        DASHEDS.add_line((0.5, 1.3), (0.5, 1.4), dxfattribs={'color': 5})
        DASHEDS.add_line((0.5, 1.7), (0.5, 1.8), dxfattribs={'color': 5})
        DASHEDS.add_line((0.5, 2), (0.5, 2.1), dxfattribs={'color': 5})
        # DASHEDS.add_line((0.5, 2.3), (0.5, 2.35), dxfattribs={'color': 5})
        if lengths1 == 7600 or lengths1==12100:
            DASHEDS.add_line((0.5, 2.3), (0.5, 2.4), dxfattribs={'color': 5})
            DASHEDS.add_line((0.5, 2.6), (0.5, 2.7), dxfattribs={'color': 5})
            DASHEDS.add_line((0.5, 2.9), (0.5, 3), dxfattribs={'color': 5})

    new_dashedss_block = doc.blocks.get(name='new_dashedss')
    new_labels = doc.blocks.get(name='old_labels')
    if not new_dashedss_block:
        DASHEDS = doc.blocks.new(name='new_dashedss')
        DASHEDS.add_line((0.5, 0.5), (0.5, 0.6), dxfattribs={'color': 5})
        DASHEDS.add_line((0.5, 0.9), (0.5, 1), dxfattribs={'color': 5})
        DASHEDS.add_line((0.5, 1.3), (0.5, 1.4), dxfattribs={'color': 5})
        DASHEDS.add_line((0.5, 1.7), (0.5, 1.8), dxfattribs={'color': 5})
        DASHEDS.add_line((0.5, 2), (0.5, 2.1), dxfattribs={'color': 5})
        DASHEDS.add_line((0.5, 2.3), (0.5, 2.4), dxfattribs={'color': 5})
        DASHEDS.add_line((0.5, 2.6), (0.5, 2.7), dxfattribs={'color': 5})
        DASHEDS.add_line((0.5, 2.9), (0.5, 3), dxfattribs={'color': 5})
        DASHEDS.add_line((0.5, 3.2), (0.5, 3.3), dxfattribs={'color': 5})
        DASHEDS.add_line((0.5, 3.5), (0.5, 3.6), dxfattribs={'color': 5})
        DASHEDS.add_line((0.5, 3.8), (0.5, 3.9), dxfattribs={'color': 5})
        DASHEDS.add_line((0.5, 4.1), (0.5, 4.2), dxfattribs={'color': 5})
        DASHEDS.add_line((0.5, 4.4), (0.5, 4.5), dxfattribs={'color': 5})
        DASHEDS.add_line((0.5, 4.7), (0.5, 4.8), dxfattribs={'color': 5})
        DASHEDS.add_line((0.5, 5), (0.5, 5.1), dxfattribs={'color': 5})
        DASHEDS.add_line((0.5, 5.3), (0.5, 5.4), dxfattribs={'color': 5})
        DASHEDS.add_line((0.5, 5.6), (0.5, 5.7), dxfattribs={'color': 5})
        DASHEDS.add_line((0.5, 5.9), (0.5, 6), dxfattribs={'color': 5})
        DASHEDS.add_line((0.5, 6.2), (0.5, 6.3), dxfattribs={'color': 5})
        DASHEDS.add_line((0.5, 6.5), (0.5, 6.6), dxfattribs={'color': 5})
        DASHEDS.add_line((0.5, 6.8), (0.5, 6.9), dxfattribs={'color': 5})
        DASHEDS.add_line((0.5, 7.1), (0.5, 7.2), dxfattribs={'color': 5})
        DASHEDS.add_line((0.5, 7.4), (0.5, 7.5), dxfattribs={'color': 5})
        DASHEDS.add_line((0.5, 7.7), (0.5, 7.8), dxfattribs={'color': 5})
        DASHEDS.add_line((0.5, 8), (0.5, 8.1), dxfattribs={'color': 5})
        DASHEDS.add_line((0.5, 8.3), (0.5, 8.4), dxfattribs={'color': 5})
        DASHEDS.add_line((0.5, 8.6), (0.5, 8.7), dxfattribs={'color': 5})
        DASHEDS.add_line((0.5, 8.9), (0.5, 9), dxfattribs={'color': 5})
    line_down_y = msp.add_blockref('circle', end_points.dxf.insert +(-560.29, -52780), dxfattribs={
        'xscale':0.003,
        'yscale': 0.003,
        'rotation': 0
    })
    line_down_y.add_auto_attribs(values3)
    line_circle_x = msp.add_blockref('circle', line_left.dxf.insert + (-72, 3350), dxfattribs={
        'xscale': 0.0003,
        'yscale': 0.0003,
        'rotation': 0
    })
    line_down_ys = msp.add_blockref('circle', line_left.dxf.insert + (-2060, 910), dxfattribs={
        'xscale': 0.003,
        'yscale': 0.003,
        'rotation': 0
    })
    # line_down_ys = msp.add_blockref('arc', line_left.dxf.insert + (-2110, -2500), dxfattribs={
    #     'xscale': 2000,
    #     'yscale': 2000,
    #     'rotation': 0
    # })
    point_x=end_points.dxf.insert[0]+(-6200)
    point_y=end_points.dxf.insert[1]+(-2500)
    # line_dasheds = msp.add_blockref('new_dashed', (point_x,point_y), dxfattribs={
    #     'xscale': 3200,
    #     'yscale': 900,
    #     'rotation': 0,
    #     'color':5,
    # })
    panels_dasheds = msp.add_blockref('new_dashed', end_points.dxf.insert + (-5400, -963), dxfattribs={
        'xscale': 3200,
        'yscale': 900,
        'rotation': 0,
        'color': 5,
    })
    panels_dasheds = msp.add_blockref('new_dashed', end_points.dxf.insert + (-5400, 1700), dxfattribs={
        'xscale': 3200,
        'yscale': 900,
        'rotation': 0,
        'color': 5,
    })
    if dict_lenght != total_length and total_length != 1 and total_frequency>=4:
        point_x = end_points.dxf.insert[0] + (-500)
        point_y = end_points.dxf.insert[1] + (-2500)
        # line_dasheds = msp.add_blockref('new_dashed',(point_x,point_y), dxfattribs={
        #     'xscale': 3200,
        #     'yscale': 900,
        #     'rotation': 0,
        #     'color': 5,
        # })
        # line_dasheds = msp.add_blockref('new_dashed',end_points.dxf.insert+ (-500, -lengths1), dxfattribs={
        #     'xscale': 3200,
        #     'yscale': 900,
        #     'rotation': 0,
        #     'color': 5,
        # })
    # line_dashed = msp.add_blockref('new_dashed', end_points.dxf.insert + (-6200, -lengths1), dxfattribs={
    #     'xscale': 3200,
    #     'yscale': 900,
    #     'rotation': 0,
    #     'color':5,
    # })
    if dict_lenght == 1 :
        if lengths1 > 30000:
            labels ='new_dashedss'
        else:
            labels='new_dasheds'
        if len_data == 3 :
            xx = -4500
        else:
            xx = -3000
        print("xxxxx",xx)
        panel_dashed = msp.add_blockref('panels_dashedss', end_points.dxf.insert + (-4500, 2800), dxfattribs={
            'xscale': 900,
            'yscale': -1300,
            'rotation': 0
        })
        gnd_points = (end_points.dxf.insert+(-4500, 2000))
        blockref_line = msp.add_blockref('LINE', gnd_points, dxfattribs={
            'xscale': 500,
            'yscale': 500,
            'rotation': 0
        })
        gnd_points = (end_points.dxf.insert+(-4500, -1150))
        blockref = msp.add_blockref('GND', gnd_points, dxfattribs={
            'xscale': 1500,
            'yscale': 1500,
            'rotation': 0
        })
        panels_dashed = msp.add_blockref('panels_dashedss', end_points.dxf.insert + (-4500, -1013), dxfattribs={
            'xscale': 900,
            'yscale': 1000,
            'rotation': 0
        })
    #     line_dashed = msp.add_blockref(labels, end_points.dxf.insert + (-3000, -60), dxfattribs={
    #     'xscale': 900,
    #     'yscale':-lengths2,
    #     'rotation': 0
    # })
    if dict_lenght == total_length:
        if lengths1 > 30000:
            labels = 'new_dashedss'
        else:
            labels = 'new_dasheds'
        # line_dashed = msp.add_blockref(labels, end_points.dxf.insert + (3000, -60), dxfattribs={
        #     'xscale': 900,
        #     'yscale': -lengths2,
        #     'rotation': 0
        # })
        if len_data == 3:
            xx = 3750
        else:
            xx = 5300
        panel_dashed = msp.add_blockref('panels_dashedsss', end_points.dxf.insert + (xx, 2800), dxfattribs={
            'xscale': 900,
            'yscale': -1300,
            'rotation': 0
        })
        panel_dashed = msp.add_blockref('panels_dashedsss', end_points.dxf.insert + (xx, -1013), dxfattribs={
            'xscale': 900,
            'yscale': 1000,
            'rotation': 0
        })
    return {'d_joints':line_circle_x,'dashed':line_down_ys}


def draw_diagram_chversion(end_points,data_raw,frequency,frequency_count_list,counts,end_point,blockref_inverter,LINEDOWN,count_list,MPPTin,maximum,diff,total_mpptin,total_dict_length,Mp,new_labels,flag,len_data,NumBB,doc,msp):
    global max_y_limit,max_x_limit,maximum_y,difference,mpp,total_length
    max_y_limit=0
    max_x_limit=total_mpptin
    maximum_y=maximum
    difference=diff
    mpp=Mp
    total_length=total_dict_length
    round_circle_block = doc.blocks.get('ROUND_CIRCLE')
    arrow_block = doc.blocks.get('ARROW')
    arrow_right_block = doc.blocks.get('ARROW_RIGHT')
    if not round_circle_block:
        round_circle = doc.blocks.new(name='ROUND_CIRCLE')
        round_circle.add_line((0, 0), (0.5, 0))
        round_circle.add_line((0.6, 0), (0.85, 0))
        round_circle.add_line((0.5, -0.1), (0.5, 0.1))
        round_circle.add_line((0.6, -0.1), (0.6, 0.1))
        round_circle.add_ellipse((1, 0), major_axis=(0.15, 0), ratio=0.8)
        round_circle.add_text("+", dxfattribs={
            'height': 0.1}).set_pos((0.4, -0.1), align='CENTER')
        round_circle.add_text("+", dxfattribs={
            'height': 0.1}).set_pos((0.7, -0.1), align='CENTER')
    if not arrow_block:
        arrow = doc.blocks.new(name='ARROW')
        arrow.add_line((-2, 0), (-2, 1)),
        arrow.add_line((-2, 1), (-2.5, 0.5)),
        arrow.add_line((-2.5, 0.5), (-2, 0)),
    if not arrow_right_block:
        arrow = doc.blocks.new(name='ARROW_RIGHT')
        arrow.add_line((2, 0), (2, 1)),
        arrow.add_line((2, 0), (2.5, 0.5)),
        arrow.add_line((2, 1), (2.5, 0.5)),
    new_labels.add_attdef('labels_names',  (-5, -3.9),
                          dxfattribs={'height': 400, 'color': 7})
    if MPPTin <=20:
        x_decreases = 35
    elif MPPTin >20 and MPPTin <=60:
        x_decreases = 13.3
    elif MPPTin >60 and MPPTin <=150:
        x_decreases = 5.33
    elif MPPTin >150 and MPPTin <=500:
        x_decreases =1.6
    elif MPPTin >500 and MPPTin <=1000:
        x_decreases = 0.6
    else:
        x_decreases = MPPTin // 800

    if maximum <= 20:
        x = 0
        xs=2000
        lengths = 500
        lengthss=990
        l5 = 14300
        x_increase=35
        # BB_x=10
        # inverter_x=9
        junction_point = 47890
        lengths1 = 5500
        lengths2 = 2200
        x_decrease = x_decreases
    elif maximum > 20 and maximum <= 60:
        x = 0
        xs=7000
        lengths = 2200
        lengthss = 2690
        l5 = 28400
        x_increase=35
        junction_point =47890
        # BB_x=20
        # inverter_x=16
        lengths1 = 7600
        lengths2 = 2300
        x_decrease = x_decreases
    elif maximum>60 and maximum<=150:
        x = 0
        xs=13000
        lengths = 5000
        lengthss=5520
        l5 = 69400
        x_increase=35
        junction_point = 47890
        lengths1 = 12100
        lengths2 =3500
        x_decrease =x_decreases
    elif maximum > 150 and maximum <= 500:
        x = 0
        xs=25000
        lengths = 17000
        lengthss = 17600
        l5 = 277600
        x_increase = 35
        junction_point = 47890
        lengths1 = 31000
        lengths2 = 3390
        x_decrease= x_decreases
    elif maximum > 500 and maximum <= 1000:
        x = 0
        xs=25000
        lengths = 34600
        lengthss = 35100
        l5 = 277600
        x_increase = 35
        lengths1 = 39000
        lengths2 = 4200
        x_decrease= x_decreases
    else:
        x=0
        lengths=(MPPTin*40)
        lengthss=lengths+600
        lengths1 = (MPPTin * 43.8)
        lengths2 = (MPPTin* 4.8)
        l5=lengths-1
        x_increase=35
        x_decrease = x_decreases
    if len(data_raw) >= 1 and len(end_point) >= 1 and len(frequency.keys()) == 1 and MPPTin > 1 or (
            len(frequency.keys()) > 1 and len(frequency.keys()) != len(count_list)):
        y = 0
        end_points = end_points
        dict_lenght = 0
        p = 1440.57
        count = 0
        dict_lenght = 0
        x_s=-1
        print("lemof fe",len(frequency_count_list),len(end_point))
        for end in end_point:
            dict_lenght += 1
            first_key = end_point[0]
            last_key = end_point[len(end_point) - 1]
            total_frequency = frequency[data_raw[0]]
            total_frequencys = frequency_count_list[x_s]
            print("*****88", total_frequencys)
            if len(data_raw) >= 1 and len(end_point) >= 1 and (
                    count != len(data_raw)):

                if flag:
                    d_joints = draws(first_key, end, last_key, MPPTin, dict_lenght, len(data_raw), 'ROUND_CIRCLE',
                                    lengths1, lengths2,
                                    len(end_point),total_frequency,len_data,total_frequencys,
                                    doc, msp)
                else:
                    d_joints = draw(first_key, end, last_key, MPPTin,  dict_lenght, len(data_raw), 'ROUND_CIRCLE',lengths1,lengths2,
                                len(end_point),len_data,total_frequencys,
                                doc, msp)
                line_down = msp.add_blockref(LINEDOWN, end.dxf.insert, dxfattribs={
                        'layer': 'MyLines',
                        'xscale': 2,
                        'yscale': 100,
                        'rotation': 0
                    })
                point22_x=d_joints['d_joints'].dxf.insert[0]+110
                point22_y=d_joints['d_joints'].dxf.insert[1]-(-1150)
                point_22=(point22_x,point22_y)
                msp.add_line(end.dxf.insert,point_22)
                point22_x = d_joints['dashed'].dxf.insert[0] +2100
                point22_y = d_joints['dashed'].dxf.insert[1]+3570
                point_22 = (point22_x, point22_y)
                if len(end_point)<=1 :
                    line_down = msp.add_blockref(LINEDOWN, point_22, dxfattribs={
                        'layer': 'MyLines',
                        'xscale': 2,
                        'yscale': -lengthss,
                        'rotation': 0
                    })
                else:
                    line_down = msp.add_blockref(LINEDOWN, point_22, dxfattribs={
                        'layer': 'MyLines',
                        'xscale': 2,
                        'yscale': -lengths,
                        'rotation': 0
                    })
                # if dict_lenght != 1:
                #     arrow = msp.add_blockref('ARROW_RIGHT', end.dxf.insert+ [-780, -1390], dxfattribs={
                #         'layer': 'MyLines',
                #         'xscale': 300,
                #         'yscale': 320,
                #         'rotation': 0
                #     })
                if dict_lenght == len(end_point):
                    new_labelss = msp.add_blockref('old_labels', end.dxf.insert + (300, -5200), dxfattribs={
                            'xscale': 0.2,
                            'yscale': 0.2,
                            'rotation': 0
                    })
                    x_pos = 5
                    values = {
                            'labels_names': "String ???",
                            'XPOS': x_pos,
                            'YPOS': 0,
                            'rotation': 180
                    }
                    # msp.add_text("String ???", dxfattribs={
                    #         'height': 100}).set_pos((end.dxf.insert) + (2790, -2500), align='CENTER')
                        # new_labelss.add_auto_attribs(values)
                # if dict_lenght != len(end_point):
                #     arrow = msp.add_blockref('ARROW', end.dxf.insert + [800,-1390], dxfattribs={
                #         'layer': 'MyLines',
                #         'xscale': 300,
                #         'yscale': 320,
                #         'rotation': 0
                #     })

                point_y1 = -lengths +junction_point
                point_x1 = line_down.dxf.insert[0]
                point_y1 = point_y1
                point_x2 = blockref_inverter.dxf.insert[0] - x
                point_y2 = point_y1
                final_point = (point_x2, point_y2)
                if dict_lenght == len(end_point)-1:
                    final_pointss=final_point
                if len(end_point)>1:
                    line_start = msp.add_line((point_x1, point_y1), (final_point[0], final_point[1]))
                    if dict_lenght == len(end_point):
                        final_point = (final_point[0],final_point[1]+20)
                        x_scale=930
                    else:
                        x_scale=950
                    line_downss = msp.add_blockref('LINE_DOWN', final_point, dxfattribs={
                        'xscale': 2,
                        'yscale': x_scale - p,
                        'rotation': 0
                    })

            lengths -= 35
            l5 -= 480
            x -= x_decrease
            y -= 1
            p += x_increase
    else:
        count = 0
        y = 0
        end_points = end_points
        dict_lenght = 0
        p = 1440.57
        max_y_limit = 0
        max_x_limit=total_mpptin
        maximum_y=maximum
        difference=diff
        mpp = Mp
        x_s=-1
        total_length = total_dict_length
        if MPPTin <= 20:
            x_decreases = 35
        elif MPPTin > 20 and MPPTin <= 60:
            x_decreases = 13.3
        elif MPPTin > 60 and MPPTin <= 150:
            x_decreases = 5.33
        elif MPPTin > 150 and MPPTin <= 500:
            x_decreases = 1.6
        elif MPPTin > 500 and MPPTin <= 1000:
            x_decreases = 0.6
        else:
            x_decreases = MPPTin // 800
        if maximum <= 20:
            x = 0
            xs = 2000
            lengths = 1000
            lengthss = 1490
            l5 = 14300
            x_increase = 35
            # BB_x=10
            # inverter_x=9
            lengths1 = 5500
            lengths2 = 2200
            x_decrease = x_decreases
        elif maximum > 20 and maximum <= 60:
            x = 0
            xs = 7000
            lengths = 3200
            lengthss = 3690
            l5 = 28400
            x_increase = 35
            # BB_x=20
            # inverter_x=16
            lengths1 = 7600
            lengths2 = 2300
            x_decrease = x_decreases
        elif maximum > 60 and maximum <= 150:
            x = 0
            xs = 13000
            lengths = 7600
            lengthss = 8120
            l5 = 69400
            x_increase = 35
            lengths1 = 12100
            lengths2 = 3500
            x_decrease = x_decreases
        elif maximum > 150 and maximum <= 500:
            x = 0
            xs = 25000
            lengths = 27000
            lengthss = 27600
            l5 = 29000
            x_increase = 35
            lengths1 = 31000
            lengths2 = 3390
            x_decrease = x_decreases
        elif maximum > 500 and maximum <= 1000:
            x = 0
            xs = 25000
            lengths = 34600
            lengthss = 35100
            l5 = 277600
            x_increase = 35
            lengths1 = 39000
            lengths2 = 4200
            x_decrease = x_decreases
        else:
            x = 0
            lengths = (MPPTin * 40)
            lengthss = lengths + 600
            lengths1 = (MPPTin *43.8)
            lengths2 = (MPPTin * 4.8)
            l5 = lengths - 1
            x_increase = 35
            x_decrease = x_decreases
        x_s= -1
        for end in end_points:
            x_s += 1
            dict_lenght += 1
            first_key = list(end_points.values())[0]
            last_key = list(end_points.values())[len(end_points) - 1]
            total_frequency=frequency[end]
            total_frequencys=frequency_count_list[x_s]
            print("*****88",total_frequency)
            if len(data_raw) >=1  and len(end_point) >= 1 and (
                    count != len(data_raw)):
                # if dict_lenght < len(end_points):
                #     print(dict_lenght)
                #     x1 = list(end_points.values())[x_s].dxf.insert[0] -200
                #     x2 = list(end_points.values())[x_s + 1].dxf.insert[0] +200
                #     y1 = list(end_points.values())[x_s].dxf.insert[1] - 1250
                #     y2 = list(end_points.values())[x_s + 1].dxf.insert[1] - 1250
                #     result = ((((x2 - x1) ** 2) + ((y2 - y1) ** 2)) ** 0.5)
                #     lines = msp.add_line((x1, y1), (x2, y2))
                if flag:
                    d_joints = draws(first_key, end_points[end], last_key, MPPTin, dict_lenght, len(data_raw), 'ROUND_CIRCLE',
                                     lengths1, lengths2,
                                     len(end_points),total_frequency,len_data,total_frequencys,
                                     doc, msp)
                else:
                    d_joints = draw(first_key, end_points[end], last_key, MPPTin, dict_lenght, len(data_raw), 'ROUND_CIRCLE',
                                    lengths1, lengths2,
                                    len(end_points),len_data,total_frequencys,
                                    doc, msp)
                line_down = msp.add_blockref(LINEDOWN, end_points[end].dxf.insert, dxfattribs={
                                'layer': 'MyLines',
                                'xscale': 2,
                                'yscale': 100,
                                'rotation': 0
                            })
                point22_x = d_joints['d_joints'].dxf.insert[0] + 110
                point22_y = d_joints['d_joints'].dxf.insert[1]-1150
                point_22 = (point22_x, point22_y)
                msp.add_line(end_points[end].dxf.insert, point_22)
                point22_x = d_joints['dashed'].dxf.insert[0] + 2100
                point22_y = d_joints['dashed'].dxf.insert[1] +3570
                point_22 = (point22_x, point22_y)
                print("lengthdd",lengths)
                if len(end_points) <= 1 :
                    line_down = msp.add_blockref(LINEDOWN, point_22, dxfattribs={
                        'layer': 'MyLines',
                        'xscale': 2,
                        'yscale': -lengthss,
                        'rotation': 0
                    })
                else:
                    line_down = msp.add_blockref(LINEDOWN, point_22, dxfattribs={
                        'layer': 'MyLines',
                        'xscale': 2,
                        'yscale': -lengths,
                        'rotation': 0
                    })
                # if dict_lenght != 1:
                #         points=(end_points[end].dxf.insert[0]-780,end_points[end].dxf.insert[1]-1390)
                #         arrow = msp.add_blockref('ARROW_RIGHT', points, dxfattribs={
                #             'layer': 'MyLines',
                #             'xscale': 300,
                #             'yscale': 320,
                #             'rotation': 0
                #         })
                if dict_lenght == len(end_points):
                    new_labelss = msp.add_blockref('old_labels', end_points[end].dxf.insert + (300, -5200), dxfattribs={
                            'xscale': 0.2,
                            'yscale': 0.2,
                            'rotation': 0
                    })
                    x_pos = 5
                    values = {
                            'labels_names': "String ???",
                            'XPOS': x_pos,
                            'YPOS': 0,
                            'rotation': 180
                    }
                    # msp.add_text("String ???", dxfattribs={
                    #             'height': 100}).set_pos((end_points[end].dxf.insert) + (2790, -2500), align='CENTER')
                            # new_labelss.add_auto_attribs(values)
                # if dict_lenght != len(end_points):
                #     arrow = msp.add_blockref('ARROW', end_points[end].dxf.insert+ (800, -1390), dxfattribs={
                #         'layer': 'MyLines',
                #         'xscale': 300,
                #         'yscale': 320,
                #         'rotation': 0
                #     })
                point_y1 = -lengths +47739
                point_x1 = line_down.dxf.insert[0]
                point_y1 = point_y1
                point_x2 = blockref_inverter.dxf.insert[0] - x
                point_y2 = point_y1
                final_point = (point_x2, point_y2)
                if len(end_points) > 1:
                        msp.add_line((point_x1, point_y1), (final_point[0], final_point[1]))
                        if dict_lenght == len(end_point):
                            final_point = (final_point[0], final_point[1] + 20)
                            x_scale = 930
                        else:
                            x_scale = 950
                        line_downss = msp.add_blockref('LINE_DOWN', final_point, dxfattribs={
                            'xscale': 2,
                            'yscale': x_scale - p,
                            'rotation': 0
                        })
            lengths -= 300
            l5 -= 480
            x -= x_decrease
            y -= 1
            p += x_increase
