import ezdxf
from ezdxf import zoom

doc = ezdxf.new()
msp = doc.modelspace()
msp.add_circle((0, 0), radius=20)  # add a CIRCLE entity, not required
# zoom.extents(msp)
doc.saveas("zooms.dxf")