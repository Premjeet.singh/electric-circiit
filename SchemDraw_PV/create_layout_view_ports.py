# importing the JSON, matplotlib,
# sys to exit the script and the SchemDraw_PV and elements for the drawing

import json  # for writing, saving and opening JSON files
import sys
from sldcircuitchversion.utilities import draw_diagram_chversion
from sldcircuitchversion.create_layout_view_ports import create_viewports
import ezdxf
from ezdxf import zoom
from ezdxf.addons import Importer
from ezdxf.math import Vec3
import random
import os


def Ec_chversion_dxf(res, pvdata, dxf_file2_name, ProjName, InvNames, lang,pv_names,installation_address,company_name,salesperson,project_created_date,component_names,Nb_Mppt):
    values = list(res.values())
    count = 0
    res_data = {}
    max_parallels = []
    total_string = 0
    for key in values[0].keys():
        count += 1
        count_inverter_input = 0
        count_parallel = 0
        data_list = []
        pvamp = 0
        sum_pvolt = 0
        pvvolt = 0
        count_list_len = []
        for data in values[0][key]:
            count_inverter_input += 1
            no_of_input = len(values[0][key])
            count_list_len.append(len(data))
            for num in data:
                count_parallel += 1
                total_string += 1
                data_list.append(num)
            pvamp += (count_parallel * pvdata['I_mp_ref'])
            sum_pvolt += (count_parallel * pvdata['V_mp_ref'])
        data_list.append(count_inverter_input)
        data_list.append(count_list_len)
        max_parallels.append(count_inverter_input)
        data_list.append(count_parallel)
        res_data[count] = data_list
        pvvolt = sum_pvolt / count_parallel
        pvdata['Mp'] = count_parallel
        pvdata['NumBB'] = len(values[0].keys())
        pvdata['Bat'] = 1
        pvdata['BatName'] = 'battery'
        pvdata['PVName'] = 'PVName'
        pvdata['Cable'] = 'Cable'
        invdata = {
            'MPPTin': count_inverter_input,
            'InvName': 'InvName',
            'InvEff': 0.67,
        }

        # with open('inputs.json', 'w') as json_input_file:
        #     json.dump(Out, json_input_file)
        with open('pvdata.json', 'w') as json_input_file:
            json.dump(pvdata, json_input_file)
        with open('invdata.json', 'w') as json_input_file:
            json.dump(invdata, json_input_file)

        data = open("language_translation.json").read()
        lang_json_data = json.loads(data)
        Electricalscheme = lang_json_data[lang]['electricalscheme']
        PVmodule = lang_json_data[lang]['pvmodule']
        InverterDCACtype = lang_json_data[lang]['Inverter DCAC type']
        CombinerBox = lang_json_data[lang]['combinerbox']
        CombinerBoxAllBB = lang_json_data[lang]['combinerboxforbb']
        Inputs = lang_json_data[lang]['inputs']
        Outputs = lang_json_data[lang]['outputs']
        V = lang_json_data[lang]['V']
        A = lang_json_data[lang]['A']
        In = lang_json_data[lang]['in']
        Out = lang_json_data[lang]['out']
        BB = lang_json_data[lang]['BB']
        pv = lang_json_data[lang]['pv']
        cabletype = lang_json_data[lang]['cabletype']
        gridconnection = lang_json_data[lang]['gridconnection']
        GND_name = lang_json_data[lang]['GND']
        InverterNr = lang_json_data[lang]['InverterNr']
        MaxInputs = lang_json_data[lang]['MaxInputs']
        UsedInputs = lang_json_data[lang]['UsedInputs']
        # -----------------------------------------------------------------------------

        # opening the JSON file to be able to read its content

        str_data = open("pvdata.json").read()
        pv_json_data = json.loads(str_data)
        str_data = open("invdata.json").read()
        inv_json_data = json.loads(str_data)

        # -----------------------------------------------------------------------------

        # reading the values in the JSON file
        # and connecting them to the correct variable

        Mp = pv_json_data['Mp']
        Mp = int(Mp)

        NumBB = pv_json_data['NumBB']
        NumBB = int(NumBB)

        Bat = pv_json_data['Bat']
        Bat = int(Bat)

        MPPTin = count_inverter_input
        MPPTin = int(MPPTin)

        PVVolt = pv_json_data['V_mp_ref']
        PVVolt = float(PVVolt)

        PVAmp = pv_json_data['I_mp_ref']
        PVAmp = float(PVAmp)

        BatName = pv_json_data['BatName']

        InvEff = inv_json_data['InvEff']
        InvEff = float(InvEff)

        Cable = pv_json_data['Cable']

        # -----------------------------------------------------------------------------

        # defining parameter needed in loops

        ir = 0
        ip = 0
        global iNb, dots_list, ns, w_l, xxs, legend_positions,inverter_names_list
        iNb = 0
        legend_positions = 0
        ns = 0
        xxs = 0
        w_l = 0
        inverter_names_list = []
        inv_list = []
        dots_list = []
        doc = ezdxf.new('R2010', setup=True)
        if "VIEWPORTS" not in doc.layers:
            vp_layer = doc.layers.new(name='VIEWPORTS')
        else:
            vp_layer = doc.layers.get("VIEWPORTS")
            # switch viewport layer off to hide the viewport border lines
        vp_layer.off()
        layout = doc.layout("Layout1")
        layout.page_setup(size=(14000, 5500), margins=(0, 0, 0, 0), units="inch")
        # create_viewports(layout, 'R2010')
        # Create a block with the name 'FLAG'
        flag = doc.blocks.new(name='FLAG')
        flag2 = doc.blocks.new(name='FLAG2')
        tblock = doc.blocks.new(name='br')
        tblock2 = doc.blocks.new(name='trafo')
        flag_pv = doc.blocks.new(name='PV')
        PH = doc.blocks.new(name='PH')
        PH.add_line((0, 0), (0.17, 0))
        DASHED = doc.blocks.new(name='DASHED')
        Line = doc.blocks.new(name='LINE')
        Line_down = doc.blocks.new(name='LINE_DOWN')
        grid_connection = doc.blocks.new(name='GRIDCONNECTION')
        grid_connection.add_lwpolyline([(0, 0.3), (6, 0.3), (6, 0), (0, 0), (0, 0.3)])
        grid_connection.add_line((3, 0), (3, 0.4))
        grid_connection.add_attdef('grid_name', (-13, 0.1), dxfattribs={'height': 0.1, 'color': 7})
        bb_label = doc.blocks.new(name='BB_labels')
        project_name_label = doc.blocks.new(name='project_name_label')
        ground_name_label = doc.blocks.new(name='ground_name_label')
        bb_label.add_attdef('bb_label', (-2, -1.6), dxfattribs={'height': 0.3, 'color': 7})
        DASHED.add_line((0.5, 0.5), (0.6, 0.5))
        DASHED.add_line((0.9, 0.5), (1, 0.5))
        DASHED.add_line((1.3, 0.5), (1.4, 0.5))
        DASHED.add_line((1.7, 0.5), (1.8, 0.5))
        DASHED.add_line((2.1, 0.5), (2.2, 0.5))
        DASHED.add_line((2.5, 0.5), (2.6, 0.5))
        DASHED.add_line((2.9, 0.5), (3, 0.5))
        DASHED.add_line((3.3, 0.5), (3.4, 0.5))
        DASHED.add_line((3.7, 0.5), (3.8, 0.5))
        DASHED.add_line((4, 0.5), (4.1, 0.5))
        DASHED.add_line((4.4, 0.5), (4.5, 0.5))
        DASHED.add_line((4.8, 0.5), (4.9, 0.5))
        DASHED.add_line((5.3, 0.5), (5.4, 0.5))
        DASHED.add_line((5.8, 0.5), (5.9, 0.5))
        DASHED.add_line((6.3, 0.5), (6.4, 0.5))
        DASHED.add_line((6.8, 0.5), (6.9, 0.5))
        DASHED.add_line((7.3, 0.5), (7.4, 0.5))
        DASHED.add_line((7.8, 0.5), (7.9, 0.5))
        DASHED.add_line((8.4, 0.5), (8.5, 0.5))
        inverter = doc.blocks.new(name='INVERTER')
        inverter_label = doc.blocks.new(name='inverter_label')
        inverter_labels = doc.blocks.new(name='inverter_labels')
        inverter_labelss = doc.blocks.new(name='inverter_labelss')
        inverter_labelsss = doc.blocks.new(name='inverter_labelsss')
        points = [(0, 0), (1, 0), (1, 0.2), (0, 0.2), (0, 0)]
        inverter.add_lwpolyline(points)
        inverter.add_line((1, 0.2), (0, 0))
        inverter.add_line((0.3, 0.15), (0.45, 0.15))
        inverter.add_line((0.3, 0.13), (0.45, 0.13))
        # inverter.add_line((0.5, 0), (0.5, -0.1))
        fit_points = [(0, 0, 0), (750, 500, 0), (1750, 500, 0), (2250, 1250, 0)]
        # inverter.add_arc((0.6, 0.08), radius=0.01, start_angle=0, end_angle=90)
        # inverter.add_arc((0.65, 0.0801), radius=0.01, start_angle=90, end_angle=0)
        # inverter.add_arc((0.6, 0.1), radius=0.01, start_angle=0, end_angle=90)
        # inverter.add_arc((0.65, 0.101), radius=0.01, start_angle=90, end_angle=0)
        # fit_points = [(0.6, 0.008, 0), (0.7, 0.03, 0), (0.8, 0.01, 0), (0.9, 0.02, 0)]
        # inverter.add_spline_control_frame(fit_points, method='uniform', dxfattribs={'color': 1})
        fit_points = [(0.6, 0.09, 0), (0.7, 0.099, 0), (0.8, 0.083, 0), (0.88, 0.089, 0)]
        inverter.add_spline_control_frame(fit_points, method='uniform', dxfattribs={'color': 7})
        fit_points = [(0.6, 0.08, 0), (0.7, 0.081, 0), (0.8, 0.065, 0), (0.88, 0.073, 0)]
        inverter.add_spline_control_frame(fit_points, method='uniform', dxfattribs={'color': 7})
        inverters = doc.blocks.new(name='INVERTERS')
        points = [(0, 0), (1, 0), (1, 0.2), (0, 0.2), (0, 0)]
        inverters.add_lwpolyline(points)
        inverters.add_line((1, 0.2), (0, 0))
        inverters.add_line((0.3, 0.15), (0.45, 0.15))
        inverters.add_line((0.3, 0.13), (0.45, 0.13))
        inverters.add_line((0.5, 0), (0.5, -0.08))
        inverters.add_line((0.5, -0.08), (0, -0.08))
        fit_points = [(0, 0, 0), (750, 500, 0), (1750, 500, 0), (2250, 1250, 0)]
        # inverters.add_arc((0.6, 0.08), radius=0.01, start_angle=0, end_angle=180)
        # inverters.add_arc((0.6, 0.0801), radius=0.01, start_angle=180, end_angle=0)
        # inverters.add_arc((0.65, 0.1), radius=0.01, start_angle=0, end_angle=180)
        # inverters.add_arc((0.65, 0.101), radius=0.01, start_angle=180, end_angle=0)
        fit_points = [(0.6, 0.09, 0), (0.7, 0.099, 0), (0.8, 0.083, 0), (0.88, 0.089, 0)]
        inverters.add_spline_control_frame(fit_points, method='uniform', dxfattribs={'color': 7})
        fit_points = [(0.6, 0.08, 0), (0.7, 0.081, 0), (0.8, 0.065, 0), (0.88, 0.073, 0)]
        inverters.add_spline_control_frame(fit_points, method='uniform', dxfattribs={'color': 7})
        combinerbox = doc.blocks.new(name='COMBINERBOX')
        combinerbox.add_circle((0.5, 0.25), radius=0.2)  # add a CIRCLE entity, not required
        # msp.add_lwpolyline([(-0.2, 1), (1, 1)])
        # msp.add_line((1, 1), (1, 0))
        combiner_points = [(0, 0), (1, 0), (1, 0.5), (0, 0.5), (0, 0)]
        combinerbox.add_lwpolyline(combiner_points)
        combinerbox.add_line((0.5, 0.5), (0.5, 0.7))
        combinerbox.add_line((0.5, 0), (0.5, -0.2))
        dasheds = doc.blocks.new(name='Dasheds')
        dasheds.add_line((0.5, 0.5), (0.5, 0.6), dxfattribs={'color': 7})
        dasheds.add_line((0.5, 0.9), (0.5, 1), dxfattribs={'color': 7})
        dasheds.add_line((0.5, 1.3), (0.5, 1.4), dxfattribs={'color': 7})
        dasheds.add_line((0.5, 1.7), (0.5, 1.8), dxfattribs={'color': 7})
        dasheds.add_line((0.5, 2), (0.5, 2.1), dxfattribs={'color': 7})
        dasheds.add_line((0.5, 2.3), (0.5, 2.4), dxfattribs={'color': 7})
        flag2.add_attdef('NAME2', (-1.5, -2), dxfattribs={'height': 0.3, 'color': 7})
        inverter.add_attdef('inverter_names', (1.2, 0.2), dxfattribs={'height': 0.2, 'color': 7})
        circle = doc.blocks.new(name='CIRCLE')
        circle.add_circle((0.25, 1.25), .05, dxfattribs={'color': 7})
        GND = doc.blocks.new(name='GND')
        GND.add_line((0, 2), (0, 2.1))
        GND.add_line((-0.08, 2), (0.08, 2))
        GND.add_line((-0.05, 1.93), (0.05, 1.93))
        GND.add_line((-0.03, 1.86), (0.03, 1.86))
        points = [(0, 0), (0.5, 0), (0.5, 1), (0, 1), (0, 0)]
        flag.add_lwpolyline(points)  # the flag symbol as 2D polyline
        flag.add_line((0, 1), (0.3, 0.6))
        flag.add_line((0.5, 1), (0.3, 0.6))
        flag.add_line((0.25, -0.2), (0.25, 0))
        flag2.add_lwpolyline(points)  # the flag symbol as 2D polyline
        flag2.add_line((0, 1), (0.3, 0.6))
        flag2.add_line((0.5, 1), (0.3, 0.6))
        flag2.add_line((0.25, -0.2), (0.25, 0))
        flag2.add_line((0.25, -0.6), (0.25, -0.5))
        flag2.add_line((0.25, -1), (0.25, -0.9))
        flag2.add_line((0.25, 1), (0.25, 1.2))
        flag2.add_circle((0.25, 1.25), .05, dxfattribs={'color': 7})
        flag_pv.add_lwpolyline(points)  # the flag symbol as 2D polyline
        flag_pv.add_line((0, 1), (0.3, 0.6))
        flag_pv.add_line((0.5, 1), (0.3, 0.6))
        flag_pv.add_line((0.25, -0.2), (0.25, 0))
        # flag.add_line((0.25, -0.6), (0.25, -0.5))
        # flag_pv.add_line((0.25, -1), (0.25, -0.9))
        flag_pv.add_line((0.25, 1), (0.25, 1.2))
        flag_pv.add_circle((0.25, 1.25), .05, dxfattribs={'color': 7})
        points = [(0, 0), (0.5, 0), (0.5, 1), (0, 1), (0, 0)]
        flag3 = doc.blocks.new(name='FLAG3')
        flag3.add_lwpolyline(points)  # the flag symbol as 2D polyline
        flag3.add_lwpolyline(points)  # the flag symbol as 2D polyline
        flag3.add_line((0, 1), (0.3, 0.6))
        flag3.add_line((0.5, 1), (0.3, 0.6))
        flag3.add_line((0.25, -0.2), (0.25, 0))
        flag3.add_line((0.25, -0.6), (0.25, -0.5))
        flag3.add_line((0.25, -1), (0.25, -0.9))
        flag3.add_line((0.25, -1.4), (0.25, -1.6))
        flag3.add_line((0.25, -2), (0.25, -2.1))
        flag3.add_line((0.25, -2.4), (0.25, -2.5))
        flag3.add_line((0.25, -2.9), (0.25, -3))
        flag3.add_line((0.25, -3.4), (0.25, -3.5))
        flag3.add_line((0.25, -3.9), (0.25, -4))
        flag3.add_line((0.25, -4.4), (0.25, -4.5))
        flag3.add_line((0.25, -4.9), (0.25, -5))
        flag3.add_line((0.25, 1), (0.25, 1.2))
        flag3.add_circle((0.25, 1.25), .05, dxfattribs={'color': 7})
        flag3.add_line((-0.5, 1.25), (0.2, 1.25))
        flag3.add_line((-0.5, 1.25), (-0.5, 1))
        flag3.add_line((-0.69, 1), (-0.29, 1))
        flag3.add_line((-0.59, 0.9), (-0.39, 0.9))
        flag3.add_line((-0.56, 0.8), (-0.42, 0.8))
        flag3.add_ellipse((-0.5, 1.12), major_axis=(0.5, 0), ratio=0.09)
        flag3.add_line((-1, 1.12), (-1.6, 1.12))
        flag3.add_line((-1.3, 1.12), (-1.3, 1.39))
        flag3.add_line((-1.3, 1.39), (-1.1, 1.39))
        flag3.add_line((-1.3, 1.39), (-1.5, 1.39))
        PHs = doc.blocks.new(name='LINE_X')
        PHs.add_line((0, 0), (0.5, 0))
        PHs.add_line((0.5, 0), (0.5, -0.05))
        Rounds_block = doc.blocks.new(name='ROUNDS')
        Rounds_block.add_line((0.2, 0), (0.2, -0.5))
        Rounds_block.add_line((0.02, -0.4), (0.35, -0.4))
        Rounds_block.add_line((0.05, -0.45), (0.3, -0.45))
        Rounds_block.add_line((0.08, -0.5), (0.25, -0.5))
        Rounds_block.add_ellipse((0.1, -0.2), major_axis=(0.39, 0), ratio=0.1)
        Rounds_block.add_line((0.49, -0.2), (1.5, -0.2))
        Rounds_block.add_line((1.13, -0.2), (1.13, -0.1))
        Rounds_block.add_line((0.9, -0.1), (1.3, -0.1))
        flag.add_attdef('NAME', (-2.5, 6.9), dxfattribs={'height': 0.3, 'color': 7})
        flag3.add_attdef('NAME2', (-2.5, -6), dxfattribs={'height': 0.3, 'color': 7})
        flag3.add_attdef('NAME3', (-1.2, 2), dxfattribs={'height': 0.3, 'color': 7})
        flag3.add_attdef('NAME4', (0.35, -0.8), dxfattribs={'height': 0.3, 'color': 7})
        flag3.add_attdef('NAME5', (0.35, -1.2), dxfattribs={'height': 0.3, 'color': 7})
        flag_pv.add_attdef('pvname', (1, 0.39), dxfattribs={'height': 0.2, 'color': 7})
        DASHED.add_attdef('NAME3', (9, 0.39), dxfattribs={'height': 0.5, 'color': 7})
        project_name_label.add_attdef('projects_name', (0.5, 5), dxfattribs={'height': 0.5, 'color': 7})
        GND.add_attdef('grounds_name', (-0.9, 2), dxfattribs={'height': 0.2, 'color': 7})
        combinerbox.add_attdef('combinerboxname', (1.2, 0.2), dxfattribs={'height': 0.2, 'color': 7})
        combinerbox.add_attdef('combinerboxname', (1.2, 0.2), dxfattribs={'height': 0.2, 'color': 7})
        PH.add_attdef('cable_name', (0.2, -0.2), dxfattribs={'height': 0.2, 'color': 7})
        new_labels = doc.blocks.new(name='old_labels')

        Line.add_line((0, 0), (1, 0))
        Line_down.add_line((0, 0), (0, 1))
        Line.add_attdef('projectname', (-2, 0.5), dxfattribs={'height': 0.3, 'color': 7})
        # flag.add_circle((0, 0), .4, dxfattribs={'color': 2})
        msp = doc.modelspace()

        # Calculating the maximum Voltage and current at different positions

        VL1 = round(pvvolt)  # Labeling Voltage After BB
        VL2 = round(VL1)
        VL3 = 230  # Labeling Voltage After INV

        AL1 = round(pvamp)  # Labeling the current of one string
        AL2 = round(count_parallel * pvamp)  # Labeling the added up current of one BB
        AL3 = round(AL2 * VL2 * InvEff / VL3)  # Labeling the current of all BB

        # -----------------------------------------------------------------------------

        # One way to change the scaling:
        l1 = 2  # size of elements and lines;

        # -----------------------------------------------------------------------------
        # To avoid inputs, which do not make sense and do not work within he program

        if Mp <= 0:
            sys.exit("The variable Mp has an unexpected value (0 or below)!")
        elif NumBB <= 0:
            sys.exit("The variable NumBB has an unexpected value (0 or below)!")
        elif MPPTin <= 0:
            sys.exit("The variable MPPTin has an unexpected value (0 or below)!")
        elif Bat > 2:
            sys.exit("The variable Bat has an unexpected value (NOT 0, 1, 2)!")
        elif Bat < 0:
            sys.exit("The variable Bat has an unexpected value (NOT 0, 1, 2)!")
        else:
            pass

        # -----------------------------------------------------------------------------

        # starting the drawing with d = schem.Drawing()

        # d = schem.Drawing()
        # d.push()  # fixes the drawing point [0 / 0]. Return with d.pop() below.
        def bb(data_raw, key, count_list, count_parallelss, max, total_mpptin, total_dict_length, raw,
               point):  # function
            global ir, iNb, ns, w_l, w_ls, maximum, flags, len_data, xxs, legend_positions, legend_positionss
            MPPTin = key
            counts = count_parallelss
            maximum = max
            flags = False
            # adding the PV modules:
            if Mp:
                sir = 1
                end_point = []
                num = 0
                count_parallel = 1
                pv_number = 0
                pv_numbers = 0
                end_stand = []
                frequency = {}

                # iterating over the list
                for item in data_raw:
                    # checking the element in dictionary
                    if item in frequency:
                        # incrementing the counr
                        frequency[item] += 1
                    else:
                        # initializing the count
                        frequency[item] = 1
                end_points = {}
                end_two = {}
                # global dots_list
                # dots_list=[]
                temp = 0
                temp2 = 0
                temp7 = 0
                temp6 = 0
                sum_count_list = count_list.copy()
                frequency_count_list = count_list.copy()

                for key in range(1, len(sum_count_list) - 1 + 1):
                    sum_count_list[key] = sum_count_list[key] + sum_count_list[key - 1]
                x_count = 0
                y_count = 0
                # print("len of keys",len(frequency.keys()))
                # print("data raw",data_raw)
                len_data = len(data_raw)
                dir2save = os.getcwd()
                pv_file = os.path.join(dir2save, 'sldcircuitenversion', 'dxfinput', 'Modules.dxf')
                # pv_file = os.path.join(dir2save, 'sldcircuitsdxf', 'dxfinput', 'newnext.dxf')
                files = [pv_file]
                x = -1
                block_s = ['panels', 'newnext']
                blockss_name = ['newModules', 'newnext', 'newModules2', 'newModules3', 'newModules4', 'newModules5',
                                'newModules6', 'newModules7']
                for file in files:
                    x += 1
                    source_dxf = ezdxf.readfile(file)
                    msp_3 = source_dxf.modelspace()
                    importer = Importer(source_dxf, doc)
                    importer.import_block('newModules')
                    importer.import_block('newnext')
                    importer.import_block('newModules2')
                    importer.import_block('newModules3')
                    importer.import_block('newModules4')
                    importer.import_block('newModules5')
                    importer.import_block('newModules6')
                    importer.import_block('newModules9')
                    importer.finalize()
                    block = doc.blocks.get('newModules')
                    if block:
                        block = block.block
                        base_point = block.dxf.base_point
                        block.dxf.base_point = (1600, -48000)
                        base_point = block.dxf.base_point
                    newblock = doc.blocks.get('newModules')
                    if newblock:
                        block = newblock.block
                        base_point = block.dxf.base_point
                        block.dxf.base_point = (1600, -48000)
                        base_point = block.dxf.base_point
                    block2 = doc.blocks.get('newnext')
                    if block2:
                        block = block2.block
                        base_point = block.dxf.base_point
                        block.dxf.base_point = (1600, -48000)
                        base_point = block.dxf.base_point
                    block3 = doc.blocks.get('newModules2')
                    if block3:
                        block = block3.block
                        base_point = block.dxf.base_point
                        block.dxf.base_point = (1600, -48000)
                        base_point = block.dxf.base_point
                    block4 = doc.blocks.get('newModules3')
                    if block4:
                        block = block4.block
                        base_point = block.dxf.base_point
                        block.dxf.base_point = (1600, -48000)
                        base_point = block.dxf.base_point
                    block5 = doc.blocks.get('newModules4')
                    if block5:
                        block = block5.block
                        base_point = block.dxf.base_point
                        block.dxf.base_point = (1600, -48000)
                        base_point = block.dxf.base_point
                    block6 = doc.blocks.get('newModules5')
                    if block6:
                        block = block6.block
                        base_point = block.dxf.base_point
                        block.dxf.base_point = (1600, -48000)
                        base_point = block.dxf.base_point
                    block7 = doc.blocks.get('newModules6')
                    if block7:
                        block = block7.block
                        base_point = block.dxf.base_point
                        block.dxf.base_point = (1600, -48000)
                        base_point = block.dxf.base_point
                    block8 = doc.blocks.get('newModules9')
                    if block8:
                        block = block8.block
                        base_point = block.dxf.base_point
                        block.dxf.base_point = (1600, -48000)
                        base_point = block.dxf.base_point
                line_values = [(1111.853817, 2282.616195, 0.0), (1081.073811, 2282.616195, 0.0),
                               (1050.293805, 2282.616195, 0.0),
                               (1050.293805, 2267.316192, 0.0), (1037.333802, 2264.256191, 0.0),
                               (1022.753799, 2264.256191, 0.0),
                               (1021.133799, 2276.496194, 0.0), (1021.133799, 2264.256191, 0.0),
                               (1021.133799, 2264.256191, 0.0),
                               (1050.293805, 2264.256191, 0.0), (1050.293805, 2252.016189, 0.0),
                               (1050.293805, 2252.016189, 0.0),
                               (1042.193803, 2252.016189, 0.0), (1044.623804, 2248.344188, 0.0),
                               (1048.673804, 2244.672188, 0.0),
                               (1050.941805, 2280.321195, 0.0)]
                labeel = ['newModules', 'newnext', 'newModules2', 'newModules3', 'newModules4', 'newModules5',
                          'newModules6', 'newModules8']
                if len(frequency.keys()) == 1 and MPPTin > 1:
                    # import pdb
                    # pdb.set_trace();
                    x_count += 0.78
                    y_count += 1
                    temp = 0
                    temp2 = 0
                    temp6 = 0
                    temp7 = 0
                    x_count = 0
                    y_count = 0
                    c = 0
                    sum_count_list = count_list.copy()
                    frequency_count_list = count_list.copy()
                    for key in range(1, len(sum_count_list) - 1 + 1):
                        sum_count_list[key] = sum_count_list[key] + sum_count_list[key - 1]
                    color_list = [0, 1, 2, 3, 4, 5, 6, 7]
                    colors = 0
                    panels = 0
                    # labeel =['newModules','newnext','newModules2','newModules3','newModules4','newModules5','newModules6','newModules8']

                    for data in data_raw:
                        # panels += 1
                        # colors += 1
                        # import pdb;
                        # pdb.set_trace()
                        if colors == len(color_list):
                            colors = 0
                        if panels == len(labeel) - 1:
                            panels = 0
                        points = [(0, 0), (0.5, 0), (0.5, 1), (0, 1), (0, 0)]
                        flag3.add_lwpolyline(points,
                                             dxfattribs={
                                                 'color': color_list[colors]})  # the flag symbol as 2D polyline
                        flag3.add_lwpolyline(points,
                                             dxfattribs={
                                                 'color': color_list[colors]})  # the flag symbol as 2D polyline
                        flag3.add_line((0, 1), (0.3, 0.6), dxfattribs={'color': color_list[colors]})
                        flag3.add_line((0.5, 1), (0.3, 0.6), dxfattribs={'color': color_list[colors]})
                        flag3.add_line((0.25, -0.2), (0.25, 0), dxfattribs={'color': color_list[colors]})
                        points = [(0, 0), (0.5, 0), (0.5, 1), (0, 1), (0, 0)]
                        flag.add_lwpolyline(points,
                                            dxfattribs={
                                                'color': color_list[colors]})  # the flag symbol as 2D polyline
                        flag.add_line((0, 1), (0.3, 0.6), dxfattribs={'color': color_list[colors]})
                        flag.add_line((0.5, 1), (0.3, 0.6), dxfattribs={'color': color_list[colors]})
                        flag.add_line((0.25, -0.2), (0.25, 0), dxfattribs={'color': color_list[colors]})
                        temp += 1
                        temp6 += 1
                        if raw == 1:
                            x_count += 1500
                            y_count += 400
                        else:
                            # point =(0.78,1)+ dots_list[-1]
                            # print("pointsss",point)
                            x_count = 780 + point[0]
                            y_count = 1 + point[1]
                            x_count += 700
                            y_count += 1000
                        # if pv_number ==2:
                        #     import pdb
                        #     pdb.set_trace();
                        if temp == sum_count_list[temp2]:
                            temp = 0
                        if temp6 == frequency_count_list[temp7]:
                            temp6 = 0
                        pv_number += 1
                        c += 1
                        if temp7 + 1 < len(frequency_count_list):
                            if len(frequency_count_list) >= 2:
                                if pv_number == (sum_count_list[temp2] + 1) and frequency_count_list[
                                    temp7] >= 5 and frequency_count_list[temp7 + 1] == 4:
                                    count_parallel = 1
                                if pv_number == (sum_count_list[temp2] + 1) and frequency_count_list[
                                    temp7] >= 5 and frequency_count_list[temp7 + 1] == 5:
                                    count_parallel = 1
                                if pv_number == (sum_count_list[temp2] + 1) and frequency_count_list[
                                    temp7] >= 5 and frequency_count_list[temp7 + 1] > 5:
                                    count_parallel = 1

                            if frequency_count_list[temp7] <= 3 and frequency_count_list[temp7 + 1] > 3:
                                count_parallel = 0
                            if (pv_number) == sum_count_list[temp2] + 1 and frequency_count_list[temp7] >= 3 and \
                                    frequency_count_list[temp7 + 1] > 3 and (frequency_count_list[temp7] >= 5 and (
                                    frequency_count_list[temp7 + 1] != 4 and frequency_count_list[
                                temp7 + 1] != 5 and
                                    frequency_count_list[temp7] < 5)):
                                count_parallel = 0
                        if count_parallel >= 3 and sum_count_list[temp7] >= 3 and frequency_count_list[temp7] <= 3:
                            pv_numbers = pv_number - 1
                            count_parallel = 1
                        if frequency_count_list[temp7] >= 3 and (frequency_count_list[temp7] + 1) == count_parallel:
                            pv_numbers = pv_number - 1
                            count_parallel = 1
                        if count_parallel <= 3:
                            if data > 0:
                                x_pos = x_count
                                values = {
                                    'NAME': pv + str(pv_number) + '-' + '1',
                                    'XPOS': x_pos,
                                    'YPOS': 52000
                                }
                                blockss = doc.blocks.get(labeel[panels])
                                for insert in blockss.query('LINE'):
                                    if insert.dxf.start not in line_values:
                                        insert.dxf.color = colors
                                point = (x_count + 5013, -451800)
                                # Every flag has a different scaling and a rotation of -15 deg.
                                random_scale = 0.5 + random.random() * 2.0
                                # Add a block reference to the block named 'FLAG' at the coordinates 'point'.
                                msp.add_text(pv + str(pv_number) + '-' + '1', dxfattribs={
                                    'height': 100}).set_pos((x_count - 260, 50500), align='CENTER')
                                msp.add_text("Spannung: 273,7V", dxfattribs={
                                    'height': 52}).set_pos((x_count + 600, 50300), align='CENTER')
                                msp.add_text(" Strom: 10,49A", dxfattribs={
                                    'height': 52}).set_pos((x_count + 630, 50200), align='CENTER')
                                blockref = msp.add_blockref(labeel[panels], point, dxfattribs={
                                    'xscale': 10,
                                    'yscale': 10,
                                    'rotation': 0,
                                    'color': 3,
                                })
                                blockrefs = msp.add_blockref('Dasheds', (point[0] - 4867, 49500), dxfattribs={
                                    'xscale': 50,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                msp.add_text(pv + str(pv_number) + '-' + str(data), dxfattribs={
                                    'height': 100}).set_pos((x_count - 260, 49300), align='CENTER')
                                # dots_list.append(blockref.dxf.insert)
                                # blockref.add_auto_attribs(values)
                                point = (x_count, 50000)
                                dots_list.append(blockref.dxf.insert)
                                x_pos = x_count + 5000
                                values = {
                                    'NAME2': pv + str(pv_number) + '-' + str(data),
                                    'NAME3': "#6mm² PVC 70º 750V",
                                    'NAME4': "Tensão Nominal: 273,7V",
                                    "NAME5": "Corrente Nominal: 10,49A",
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                # block_s.add_auto_attribs(values)
                            if count_parallel == 3 and frequency_count_list[temp7] > 3:
                                flags = True
                                point = (x_count + 5013, -451800)
                                # Every flag has a different scaling and a rotation of -15 deg.
                                random_scale = 0.5 + random.random() * 2.0
                                # Add a block reference to the block named 'FLAG' at the coordinates 'point'.
                                # blockss = doc.blocks.get('newModules')
                                # for insert in blockss.query('LINE'):
                                #     # print("kline", insert.dxf.color)
                                #     insert.dxf.color = 2
                                #     print("kline*******", insert.dxf.color, colors)
                                msp.add_blockref(labeel[panels], point, dxfattribs={
                                    'xscale': 10,
                                    'yscale': 10,
                                    'rotation': 0
                                })
                                blockrefs = msp.add_blockref('Dasheds', (point[0] - 4867, 49500), dxfattribs={
                                    'xscale': 50,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                point = (x_count, 49600)
                                x_pos = x_count + 5000
                                values = {
                                    'NAME3': str(frequency_count_list[temp7]) + 'string',
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                msp.add_text(str(frequency_count_list[temp7]) + 'string', dxfattribs={
                                    'height': 100}).set_pos((x_count + 2100, 49700), align='CENTER')
                                blockref = msp.add_blockref('DASHED', point, dxfattribs={
                                    'xscale': 235,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                # blockref.add_auto_attribs(values)
                                x_count += 50
                                point = (x_count, 48900)
                                msp.add_blockref('PH', point, dxfattribs={
                                    'xscale': 9929,
                                    'yscale': 100,
                                    'rotation': 0
                                })
                                x_count += 1500
                                point = (x_count + 5013, -451800)
                                x_pos = x_count
                                values = {
                                    'NAME': pv + str(pv_number + (frequency_count_list[temp7] - 3)) + '-' + '1',
                                    'XPOS': x_pos,
                                    'YPOS': 0
                                }
                                blockss = doc.blocks.get(labeel[panels])
                                for insert in blockss.query('LINE'):
                                    if insert.dxf.start not in line_values:
                                        insert.dxf.color = colors
                                msp.add_text(pv + str(pv_number + (frequency_count_list[temp7] - 3)) + '-' + '1',
                                             dxfattribs={
                                                 'height': 100}).set_pos((x_count - 260, 50500), align='CENTER')
                                msp.add_text("Spannung:: 273,7V", dxfattribs={
                                    'height': 52}).set_pos((x_count + 600, 50300), align='CENTER')
                                msp.add_text("Storm: 10,49A", dxfattribs={
                                    'height': 52}).set_pos((x_count + 630, 50200), align='CENTER')
                                blockref = msp.add_blockref(labeel[panels], point, dxfattribs={
                                    'xscale': 10,
                                    'yscale': 10,
                                    'rotation': 0
                                })
                                blockrefs = msp.add_blockref('Dasheds', (point[0] - 4867, 49500), dxfattribs={
                                    'xscale': 50,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                msp.add_text(
                                    pv + str(pv_number + (frequency_count_list[temp7] - 3)) + '-' + str(data),
                                    dxfattribs={
                                        'height': 100}).set_pos((x_count - 260, 49300), align='CENTER')
                                # blockref.add_auto_attribs(values)
                                point = (x_count + 5013, -451800)
                                x_pos = x_count + 5000
                                values = {
                                    'NAME2': pv + str(pv_number + (frequency_count_list[temp7] - 3)) + '-' + str(
                                        data),
                                    'NAME3': "#6mm² PVC 70º 750V",
                                    'NAME4': "Tensão Nominal: 273,7V",
                                    "NAME5": "Corrente Nominal: 10,49A",
                                    'XPOS': x_pos,
                                    'YPOS': 49500
                                }
                                block_s = msp.add_blockref(labeel[panels], point, dxfattribs={
                                    'xscale': 10,
                                    'yscale': 10,
                                    'rotation': 0
                                })
                                blockrefs = msp.add_blockref('Dasheds', (point[0] - 4867, 49500), dxfattribs={
                                    'xscale': 50,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                # block_s.add_auto_attribs(values)

                            if pv_number != len(data_raw):
                                if frequency_count_list[temp7] <= 3 and frequency_count_list[temp7] != 1:
                                    if pv_number != sum_count_list[temp2]:
                                        x_count += 170
                                        point = (x_count, 48900)
                                        ENDPV = msp.add_blockref('PH', point, dxfattribs={
                                            'xscale': 9929,
                                            'yscale': 100,
                                            'rotation': 0
                                        })
                                        end_stand.append(ENDPV)

                                if count_parallel < 3 and frequency_count_list[temp2] > 3:
                                    x_count += 170
                                    point = (x_count, 48900)
                                    ENDPV = msp.add_blockref('PH', point, dxfattribs={
                                        'xscale': 9929,
                                        'yscale': 100,
                                        'rotation': 0
                                    })
                                    end_stand.append(ENDPV)


                            else:
                                end_stand.append(ENDPV)

                            if frequency_count_list[temp7] == 1 and len(frequency.keys()) == 1:
                                x_count += 170
                                point = (x_count, 48900)
                                ENDPV = msp.add_blockref('LINE_DOWN', point, dxfattribs={
                                    'xscale': 600,
                                    'yscale': -200,
                                    'rotation': 0
                                })

                        else:
                            x_count -= 0.65
                            y_count -= 1
                        end_two[data] = ENDPV
                        if MPPTin > 1 and len(frequency.keys()) == 1:
                            if pv_number == (sum_count_list[temp2]):
                                end_point.append(ENDPV)
                                end_points[data] = ENDPV
                                colors += 1
                                panels += 1
                                # labeel='

                            if temp7 + 1 < len(frequency_count_list):
                                if frequency_count_list[temp7] >= 5 and (
                                        frequency_count_list[temp7 + 1] == 4 or frequency_count_list[
                                    temp7 + 1] == 5 or
                                        frequency_count_list[temp7 + 1] > 5) and len(
                                    frequency_count_list) >= 2:
                                    if pv_number == sum_count_list[temp2] + 1:
                                        temp2 += 1
                                        temp7 += 1
                                else:
                                    if pv_number == sum_count_list[temp2]:
                                        temp2 += 1
                                        temp7 += 1
                        else:
                            if num != data and (count_parallel != len(data_raw) or len(data_raw)) == 1:
                                end_point.append(ENDPV)
                                # end_points[data] = ENDPV
                                num = data
                        count_parallel += 1
                    ir = 0
                    siNb = iNb + 1

                elif len(frequency.keys()) != len(count_list):
                    pv_number = 0
                    count_parallel = 1
                    temp = 0
                    temp2 = 0
                    temp6 = 0
                    temp7 = 0
                    x_count = 0
                    y_count = 0
                    sum_count_list = count_list.copy()
                    frequency_count_list = count_list.copy()
                    for key in range(1, len(sum_count_list) - 1 + 1):
                        sum_count_list[key] = sum_count_list[key] + sum_count_list[key - 1]
                    color_list = [0, 1, 2, 3, 4, 5, 6, 7]
                    colors = 0
                    panels = 0
                    for data in data_raw:
                        # import pdb;
                        # pdb.set_trace()
                        if colors == len(color_list):
                            colors = 0
                        if panels == len(labeel) - 1:
                            panels = 0
                        points = [(0, 0), (0.5, 0), (0.5, 1), (0, 1), (0, 0)]
                        flag3.add_lwpolyline(points,
                                             dxfattribs={
                                                 'color': color_list[colors]})  # the flag symbol as 2D polyline
                        flag3.add_lwpolyline(points,
                                             dxfattribs={
                                                 'color': color_list[colors]})  # the flag symbol as 2D polyline
                        flag3.add_line((0, 1), (0.3, 0.6), dxfattribs={'color': color_list[colors]})
                        flag3.add_line((0.5, 1), (0.3, 0.6), dxfattribs={'color': color_list[colors]})
                        flag3.add_line((0.25, -0.2), (0.25, 0), dxfattribs={'color': color_list[colors]})
                        points = [(0, 0), (0.5, 0), (0.5, 1), (0, 1), (0, 0)]
                        flag.add_lwpolyline(points,
                                            dxfattribs={
                                                'color': color_list[colors]})  # the flag symbol as 2D polyline
                        flag.add_line((0, 1), (0.3, 0.6), dxfattribs={'color': color_list[colors]})
                        flag.add_line((0.5, 1), (0.3, 0.6), dxfattribs={'color': color_list[colors]})
                        flag.add_line((0.25, -0.2), (0.25, 0), dxfattribs={'color': color_list[colors]})
                        temp += 1
                        temp6 += 1
                        if frequency_count_list[temp7] == 3:
                            flags = True
                        if raw == 1:
                            x_count += 2200
                            y_count += 400
                        else:
                            # point =(0.78,1)+ dots_list[-1]
                            # print("pointsss",point)
                            x_count = 780 + point[0]
                            y_count = 1 + point[1]
                            x_count += 1400
                            y_count += 1000
                        if temp == count_list[temp2]:
                            temp = 0
                        if temp6 == frequency_count_list[temp7]:
                            temp6 = 0
                        pv_number += 1
                        if temp7 + 1 < len(frequency_count_list):
                            if len(frequency_count_list) >= 2:
                                if pv_number == (sum_count_list[temp2] + 1) and frequency_count_list[
                                    temp7] >= 5 and frequency_count_list[temp7 + 1] == 4:
                                    count_parallel = 1
                                if pv_number == (sum_count_list[temp2] + 1) and frequency_count_list[
                                    temp7] >= 5 and frequency_count_list[temp7 + 1] == 5:
                                    count_parallel = 1
                                if pv_number == (sum_count_list[temp2] + 1) and frequency_count_list[
                                    temp7] >= 5 and frequency_count_list[temp7 + 1] > 5:
                                    count_parallel = 1
                            if frequency_count_list[temp7] <= 3 and frequency_count_list[temp7 + 1] > 3:
                                count_parallel = 0
                            if (pv_number) == sum_count_list[temp2] + 1 and frequency_count_list[temp7] >= 3 and \
                                    frequency_count_list[temp7 + 1] > 3 and (frequency_count_list[temp7] >= 5 and (
                                    frequency_count_list[temp7 + 1] != 4 and frequency_count_list[
                                temp7 + 1] != 5 and
                                    frequency_count_list[temp7 + 1] < 5)):
                                count_parallel = 0
                        if count_parallel >= 3 and sum_count_list[temp7] >= 3 and frequency_count_list[temp7] <= 3:
                            pv_numbers = pv_number - 1
                            count_parallel = 1
                        if frequency_count_list[temp7] >= 3 and (frequency_count_list[temp7] + 1) == count_parallel:
                            pv_numbers = pv_number - 1
                            count_parallel = 1
                        if count_parallel <= 3:
                            if data > 0:
                                x_pos = x_count
                                values = {
                                    'NAME': pv + str(pv_number) + '-' + '1',
                                    'XPOS': x_pos,
                                    'YPOS': 0
                                }
                                blockss = doc.blocks.get(labeel[panels])
                                for insert in blockss.query('LINE'):
                                    if insert.dxf.start not in line_values:
                                        insert.dxf.color = colors
                                point = (x_count + 5013, -451800)
                                # Every flag has a different scaling and a rotation of -15 deg.
                                random_scale = 0.5 + random.random() * 2.0
                                # Add a block reference to the block named 'FLAG' at the coordinates 'point'.
                                msp.add_text(pv + str(pv_number) + '-' + '1', dxfattribs={
                                    'height': 100}).set_pos((x_count - 260, 50500), align='CENTER')
                                msp.add_text("Spannung:: 273,7V", dxfattribs={
                                    'height': 52}).set_pos((x_count + 600, 50300), align='CENTER')
                                msp.add_text("Storm: 10,49A", dxfattribs={
                                    'height': 52}).set_pos((x_count + 630, 50200), align='CENTER')
                                blockref = msp.add_blockref(labeel[panels], point, dxfattribs={
                                    'xscale': 10,
                                    'yscale': 10,
                                    'rotation': 0
                                })
                                blockrefs = msp.add_blockref('Dasheds', (point[0] - 4867, 49500), dxfattribs={
                                    'xscale': 50,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                msp.add_text(pv + str(pv_number) + '-' + str(data), dxfattribs={
                                    'height': 100}).set_pos((x_count - 260, 49300), align='CENTER')
                                dots_list.append(blockref.dxf.insert)
                                # blockref.add_auto_attribs(values)
                                point = (x_count, 50000)
                                x_pos = x_count + 5000
                                values = {
                                    'NAME2': pv + str(pv_number) + '-' + str(data),
                                    'NAME3': "#6mm² PVC 70º 750V",
                                    'NAME4': "Tensão Nominal: 273,7V",
                                    "NAME5": "Corrente Nominal: 10,49A",
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                # block_s.add_auto_attribs(values)

                            if count_parallel == 3 and frequency_count_list[temp7] > 3:
                                flags = True
                                point = (x_count + 5013, -451800)
                                # Every flag has a different scaling and a rotation of -15 deg.
                                random_scale = 0.5 + random.random() * 2.0
                                # Add a block reference to the block named 'FLAG' at the coordinates 'point'.
                                msp.add_blockref(labeel[panels], point, dxfattribs={
                                    'xscale': 10,
                                    'yscale': 10,
                                    'rotation': 0
                                })
                                blockrefs = msp.add_blockref('Dasheds', (point[0] - 4867, 49500), dxfattribs={
                                    'xscale': 50,
                                    'yscale': 300,
                                    'rotation': 0
                                })

                                point = (x_count, 49600)
                                x_pos = x_count + 5000
                                values = {
                                    'NAME3': str(frequency_count_list[temp7]) + 'string',
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                msp.add_text(str(frequency_count_list[temp7]) + 'string', dxfattribs={
                                    'height': 100}).set_pos((x_count + 4000, 49700), align='CENTER')
                                blockref = msp.add_blockref('DASHED', point, dxfattribs={
                                    'xscale': 285,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                # blockref.add_auto_attribs(values)
                                x_count += 50
                                point = (x_count, 48900)
                                msp.add_blockref('PH', point, dxfattribs={
                                    'xscale': 13929,
                                    'yscale': 100,
                                    'rotation': 0
                                })
                                x_count += 2200
                                point = (x_count + 5013, -451800)
                                x_pos = x_count
                                values = {
                                    'NAME': pv + str(pv_number + (frequency[data] - 3)) + '-' + '1',
                                    'XPOS': x_pos,
                                    'YPOS': 0
                                }
                                blockss = doc.blocks.get(labeel[panels])
                                for insert in blockss.query('LINE'):
                                    if insert.dxf.start not in line_values:
                                        insert.dxf.color = colors
                                msp.add_text(pv + str(pv_number + (frequency_count_list[temp7] - 3)) + '-' + '1',
                                             dxfattribs={
                                                 'height': 100}).set_pos((x_count - 260, 50500), align='CENTER')
                                msp.add_text("Spannung:: 273,7V", dxfattribs={
                                    'height': 52}).set_pos((x_count + 600, 50300), align='CENTER')
                                msp.add_text("Storm: 10,49A", dxfattribs={
                                    'height': 52}).set_pos((x_count + 630, 50200), align='CENTER')
                                blockref = msp.add_blockref(labeel[panels], point, dxfattribs={
                                    'xscale': 10,
                                    'yscale': 10,
                                    'rotation': 0
                                })
                                blockrefs = msp.add_blockref('Dasheds', (point[0] - 4867, 49500), dxfattribs={
                                    'xscale': 50,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                msp.add_text(
                                    pv + str(pv_number + (frequency_count_list[temp7] - 3)) + '-' + str(data),
                                    dxfattribs={
                                        'height': 100}).set_pos((x_count - 260, 49300), align='CENTER')
                                # blockref.add_auto_attribs(values)
                                point = (x_count + 5013, -451800)
                                x_pos = x_count + 5000
                                values = {
                                    'NAME2': pv + str(pv_number + (frequency[data] - 3)) + '-' + str(data),
                                    'NAME3': "#6mm² PVC 70º 750V",
                                    'NAME4': "Tensão Nominal: 273,7V",
                                    "NAME5": "Corrente Nominal: 10,49A",
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                block_s = msp.add_blockref(labeel[panels], point, dxfattribs={
                                    'xscale': 10,
                                    'yscale': 10,
                                    'rotation': 0
                                })
                                blockrefs = msp.add_blockref('Dasheds', (point[0] - 4867, 49500), dxfattribs={
                                    'xscale': 50,
                                    'yscale': 300,
                                    'rotation': 0
                                })

                            if pv_number != len(data_raw):
                                if frequency_count_list[temp7] <= 3 and frequency_count_list[temp7] != 1:
                                    if pv_number != sum_count_list[temp2]:
                                        x_count += 170
                                        point = (x_count, 48900)
                                        ENDPV = msp.add_blockref('PH', point, dxfattribs={
                                            'xscale': 13929,
                                            'yscale': 100,
                                            'rotation': 0
                                        })
                                        end_stand.append(ENDPV)

                                if count_parallel < 3 and frequency_count_list[temp2] > 3:
                                    x_count += 170
                                    point = (x_count, 48900)
                                    ENDPV = msp.add_blockref('PH', point, dxfattribs={
                                        'xscale': 13929,
                                        'yscale': 100,
                                        'rotation': 0
                                    })
                                    end_stand.append(ENDPV)


                            else:
                                end_stand.append(ENDPV)
                            if frequency_count_list[temp7] == 1 and len(frequency.keys()) >= 1:
                                x_count += 170
                                point = (x_count, 48900)
                                ENDPV = msp.add_blockref('LINE_DOWN', point, dxfattribs={
                                    'xscale': 600,
                                    'yscale': -200,
                                    'rotation': 0
                                })
                        else:
                            x_count -= 0.65
                            y_count -= 1
                        end_two[data] = ENDPV
                        # print("count list tem[", count_list[temp2])
                        if len(frequency.keys()) >= 1:
                            if pv_number == (sum_count_list[temp2]):
                                end_point.append(ENDPV)
                                end_points[data] = ENDPV
                                colors += 1
                                panels += 1
                            if temp7 + 1 < len(frequency_count_list):
                                if frequency_count_list[temp7] >= 5 and (
                                        frequency_count_list[temp7 + 1] == 4 or frequency_count_list[
                                    temp7 + 1] == 5 or
                                        frequency_count_list[temp7 + 1] > 5) and len(
                                    frequency_count_list) >= 2:
                                    if pv_number == sum_count_list[temp2] + 1:
                                        temp2 += 1
                                        temp7 += 1
                                else:
                                    if pv_number == sum_count_list[temp2]:
                                        temp2 += 1
                                        temp7 += 1
                        else:
                            if num != data and (count_parallel != len(data_raw) or len(data_raw)) == 1:
                                num = data
                                end_point.append(ENDPV)
                                # end_points[data] = ENDPV
                        count_parallel += 1
                    ir = 0
                    siNb = iNb + 1

                else:
                    color_list = [0, 1, 2, 3, 4, 5, 6, 7]
                    colors = 0
                    panels = 0
                    for data in data_raw:
                        temp += 1
                        temp6 += 1
                        if colors == len(color_list):
                            colors = 0
                        if panels == len(labeel) - 1:
                            panels = 0
                        points = [(0, 0), (0.5, 0), (0.5, 1), (0, 1), (0, 0)]
                        flag3.add_lwpolyline(points,
                                             dxfattribs={
                                                 'color': color_list[colors]})  # the flag symbol as 2D polyline
                        flag3.add_lwpolyline(points,
                                             dxfattribs={
                                                 'color': color_list[colors]})  # the flag symbol as 2D polyline
                        flag3.add_line((0, 1), (0.3, 0.6), dxfattribs={'color': color_list[colors]})
                        flag3.add_line((0.5, 1), (0.3, 0.6), dxfattribs={'color': color_list[colors]})
                        flag3.add_line((0.25, -0.2), (0.25, 0), dxfattribs={'color': color_list[colors]})
                        points = [(0, 0), (0.5, 0), (0.5, 1), (0, 1), (0, 0)]
                        flag.add_lwpolyline(points,
                                            dxfattribs={
                                                'color': color_list[colors]})  # the flag symbol as 2D polyline
                        flag.add_line((0, 1), (0.3, 0.6), dxfattribs={'color': color_list[colors]})
                        flag.add_line((0.5, 1), (0.3, 0.6), dxfattribs={'color': color_list[colors]})
                        flag.add_line((0.25, -0.2), (0.25, 0), dxfattribs={'color': color_list[colors]})
                        if frequency_count_list[temp7] == 3:
                            flags = True

                        if raw == 1:
                            x_count += 2200
                            y_count += 400
                        else:
                            x_count = 780 + point[0]
                            y_count = 1 + point[1]
                            x_count += 1400
                            y_count += 1000

                        if temp == count_list[temp2]:
                            temp = 0
                        if temp6 == frequency_count_list[temp7]:
                            temp6 = 0
                        pv_number += 1
                        if temp7 + 1 < len(frequency_count_list):
                            if frequency_count_list[temp7] <= 3 and frequency_count_list[
                                temp7 + 1] > 3:
                                count_parallel = 0
                        if count_parallel >= 3 and sum_count_list[temp7] >= 3 and frequency_count_list[temp7] <= 3:
                            pv_numbers = pv_number - 1
                            count_parallel = 1
                        if num != data and count_parallel >= 3:
                            pv_numbers = pv_number - 1
                            count_parallel = 1
                        if count_parallel <= 3:
                            if data > 0:
                                x_pos = x_count
                                values = {
                                    'NAME': pv + str(pv_number) + '-' + '1',
                                    'XPOS': x_pos,
                                    'YPOS': 0
                                }
                                blockss = doc.blocks.get(labeel[panels])
                                for insert in blockss.query('LINE'):
                                    if insert.dxf.start not in line_values:
                                        insert.dxf.color = colors
                                point = (x_count + 5013, -451800)
                                # Every flag has a different scaling and a rotation of -15 deg.
                                random_scale = 0.5 + random.random() * 2.0
                                # Add a block reference to the block named 'FLAG' at the coordinates 'point'.
                                msp.add_text(pv + str(pv_number) + '-' + '1', dxfattribs={
                                    'height': 100}).set_pos((x_count - 260, 50500), align='CENTER')
                                msp.add_text("Spannung:: 273,7V", dxfattribs={
                                    'height': 52}).set_pos((x_count + 600, 50300), align='CENTER')
                                msp.add_text("Storm: 10,49A", dxfattribs={
                                    'height': 52}).set_pos((x_count + 630, 50200), align='CENTER')
                                blockref = msp.add_blockref(labeel[panels], point, dxfattribs={
                                    'xscale': 10,
                                    'yscale': 10,
                                    'rotation': 0
                                })
                                blockrefs = msp.add_blockref('Dasheds', (point[0] - 4867, 49500), dxfattribs={
                                    'xscale': 50,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                msp.add_text(pv + str(pv_number) + '-' + str(data), dxfattribs={
                                    'height': 100}).set_pos((x_count - 260, 49300), align='CENTER')
                                dots_list.append(blockref.dxf.insert)
                                # blockref.add_auto_attribs(values)
                                point = (x_count, 50000)
                                x_pos = x_count + 5000
                                values = {
                                    'NAME2': pv + str(pv_number) + '-' + str(data),
                                    'NAME3': "#6mm² PVC 70º 750V",
                                    'NAME4': "Tensão Nominal: 273,7V",
                                    "NAME5": "Corrente Nominal: 10,49A",
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                # block_s.add_auto_attribs(values)
                            if count_parallel == 3 and frequency[data] > 3:
                                flags = True
                                point = (x_count + 5013, -451800)
                                # Every flag has a different scaling and a rotation of -15 deg.
                                random_scale = 0.5 + random.random() * 2.0
                                # Add a block reference to the block named 'FLAG' at the coordinates 'point'.
                                msp.add_blockref(labeel[panels], point, dxfattribs={
                                    'xscale': 10,
                                    'yscale': 10,
                                    'rotation': 0
                                })
                                blockrefs = msp.add_blockref('Dasheds', (point[0] - 4867, 49500), dxfattribs={
                                    'xscale': 50,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                point = (x_count, 49600)
                                x_pos = x_count + 5000
                                values = {
                                    'NAME3': str(frequency_count_list[temp7]) + 'string',
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                blockss = doc.blocks.get(labeel[panels])
                                for insert in blockss.query('LINE'):
                                    if insert.dxf.start not in line_values:
                                        insert.dxf.color = colors
                                msp.add_text(str(frequency_count_list[temp7]) + 'string', dxfattribs={
                                    'height': 100}).set_pos((x_count + 3000, 49700), align='CENTER')
                                blockref = msp.add_blockref('DASHED', point, dxfattribs={
                                    'xscale': 285,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                # blockref.add_auto_attribs(values)
                                x_count += 50
                                point = (x_count, 48900)
                                msp.add_blockref('PH', point, dxfattribs={
                                    'xscale': 14155,
                                    'yscale': 100,
                                    'rotation': 0
                                })
                                x_count += 2200
                                point = (x_count + 5013, -451800)
                                x_pos = x_count
                                values = {
                                    'NAME': pv + str(pv_number + (frequency[data] - 3)) + '-' + '1',
                                    'XPOS': x_pos,
                                    'YPOS': 0
                                }
                                msp.add_text(pv + str(pv_number + (frequency_count_list[temp7] - 3)) + '-' + '1',
                                             dxfattribs={
                                                 'height': 100}).set_pos((x_count - 260, 50500), align='CENTER')
                                msp.add_text("Spannung:: 273,7V", dxfattribs={
                                    'height': 52}).set_pos((x_count + 600, 50300), align='CENTER')
                                msp.add_text("Storm: 10,49A", dxfattribs={
                                    'height': 52}).set_pos((x_count + 630, 50200), align='CENTER')
                                blockref = msp.add_blockref(labeel[panels], point, dxfattribs={
                                    'xscale': 10,
                                    'yscale': 10,
                                    'rotation': 0
                                })
                                blockrefs = msp.add_blockref('Dasheds', (point[0] - 4867, 49500), dxfattribs={
                                    'xscale': 50,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                msp.add_text(
                                    pv + str(pv_number + (frequency_count_list[temp7] - 3)) + '-' + str(data),
                                    dxfattribs={
                                        'height': 100}).set_pos((x_count - 260, 49300), align='CENTER')
                                # blockref.add_auto_attribs(values)
                                point = (x_count + 5013, -451800)
                                x_pos = x_count + 5000
                                values = {
                                    'NAME2': pv + str(pv_number + (frequency[data] - 3)) + '-' + str(data),
                                    'NAME3': "#6mm² PVC 70º  750V",
                                    'NAME4': "Tensão Nominal: 273,7V",
                                    "NAME5": "Corrente Nominal: 10,49A",
                                    'XPOS': x_pos,
                                    'YPOS': 0.6
                                }
                                block_s = msp.add_blockref(labeel[panels], point, dxfattribs={
                                    'xscale': 10,
                                    'yscale': 10,
                                    'rotation': 0
                                })
                                blockrefs = msp.add_blockref('Dasheds', (point[0] - 4867, 49500), dxfattribs={
                                    'xscale': 50,
                                    'yscale': 300,
                                    'rotation': 0
                                })
                                # block_s.add_auto_attribs(values)
                            if pv_number != len(data_raw):
                                if frequency[data] <= 3 and frequency[data] != 1:
                                    if pv_number != sum_count_list[temp2]:
                                        x_count += 170
                                        point = (x_count, 48900)
                                        ENDPV = msp.add_blockref('PH', point, dxfattribs={
                                            'xscale': 14155,
                                            'yscale': 100,
                                            'rotation': 0
                                        })
                                        end_stand.append(ENDPV)

                                if count_parallel < 3 and frequency[data] > 3:
                                    x_count += 170
                                    point = (x_count, 48900)
                                    ENDPV = msp.add_blockref('PH', point, dxfattribs={
                                        'xscale': 14155,
                                        'yscale': 100,
                                        'rotation': 0
                                    })
                                    end_stand.append(ENDPV)

                            if frequency[data] == 1:
                                x_count += 170
                                point = (x_count, 48900)
                                ENDPV = msp.add_blockref('LINE_DOWN', point, dxfattribs={
                                    'xscale': 3,
                                    'yscale': -0.2,
                                    'rotation': 0
                                })
                        else:
                            x_count -= 2200
                            y_count -= 700
                        end_two[data] = ENDPV
                        if num != data and (count_parallel != len(data_raw) or len(data_raw)) == 1:
                            end_point.append(ENDPV)
                            end_points[data] = ENDPV
                            num = data
                            # colors += 1
                            # temp2 += 1
                        if pv_number == sum_count_list[temp2]:
                            temp2 += 1
                            temp7 += 1
                            colors += 1
                            panels += 1
                            end_point.append(ENDPV)
                            end_points[data] = ENDPV
                        count_parallel += 1
                    ir = 0
                    siNb = iNb + 1

            # adding the Combiner Box, the Inverters and the Batteries:
            if MPPTin < Mp:  # if Inv inputs < than number of parallels --> add CB
                # import pdb
                # pdb.set_trace();
                CBout = 1
                # d.push()
                count = 0
                diff = maximum - 150
                if diff % 60 == 0:
                    diff = int(diff / 60)
                else:
                    diff = int(diff / 60) + 1
                if maximum <= 20:
                    n = 6.5
                    n_length = 19.3
                    m_length = 19.65
                    length = 20.9
                    # l=30
                    ns = 500
                    w_l = 13000
                    w_ls = 2000
                    inv_y = 2800
                    BB_y = 36300
                    inverter_xx = 5
                    legend_positionss = 1500
                elif maximum > 20 and maximum <= 60:
                    n = 12
                    n_length = 30.2
                    m_length = 30.65
                    length = 28
                    # l=46
                    ns = 500
                    w_l = 6000
                    w_ls = 2000
                    inv_y = 4500
                    BB_y = 31300
                    x_length = 21000
                    inverter_xx = 5
                    legend_positionss = 1500
                elif maximum > 60 and maximum <= 150:
                    n = 18
                    n_length = 42.3
                    m_length = 42.6
                    length = 28
                    # l=69
                    ns = 500
                    w_l = 13000
                    w_ls = 2000
                    inv_y = 7300
                    BB_y = 274
                    # x_length = 21000
                    inverter_xx = 5
                    legend_positionss = -500
                elif maximum > 150 and maximum <= 500:
                    n = 18
                    n_length = 42.3
                    m_length = 42.6
                    length = 28
                    # l=69
                    ns = 500
                    w_l = 13000
                    w_ls = 2000
                    inv_y = 19300
                    BB_y = 1104
                    x_length = 68000
                    inverter_xx = 5
                    legend_positionss = -10000
                elif maximum > 500 and maximum <= 1000:
                    n = 18
                    n_length = 42.3
                    m_length = 42.6
                    length = 28
                    # l=69
                    ns = 500
                    w_l = 13000
                    w_ls = 2000
                    inv_y = 33400
                    BB_y = 1104
                    x_length = 68000
                    inverter_xx = 5
                    legend_positionss = -30000

                else:
                    n = (diff * 12.5) + 18
                    n_length = (25 * diff) + 42.3
                    m_length = (25 * diff) + 42.6
                    ns = 500
                    inv_y = (MPPTin * 35)+2000
                    legend_positionss = -(MPPTin*25)
                    BB_y = (MPPTin * 29.5) + 269
                    w_l = (diff * 3) + 15
                    w_ls = 2000
                    inverter_xx = 5

                if MPPTin <= 20:
                    l = 800
                    l_y = 4000
                    l_s = 22000
                    ls = 38000
                    BB_x = 300
                    BBs_x = 15
                    BB_x2 = 134500
                    inverter_x = 9
                    inv_length = -(45 + ((MPPTin - 2) * 35))
                    invs_length = 500
                    BB_x3 = -80
                    bb_x3 = 30
                    x_length = 0

                elif MPPTin > 20 and MPPTin <= 60:
                    l = 800
                    l_y = 4000
                    l_s = 45000
                    ls = 42000
                    BB_x = 300
                    BBs_x = 67
                    BB_x2 = 134500
                    inverter_x = 12
                    # w_l = 4
                    # w_ls=7.5
                    inv_length = -(MPPTin * 13.3)
                    invs_length = 4500
                    BB_x3 = -100
                    bb_x3 = 75
                    x_length = 21000

                elif MPPTin > 60 and MPPTin <= 150:
                    l = 800
                    l_y = 4000
                    l_s = 89500
                    ls = 82000
                    BB_x = 40
                    inverter_x = -60
                    # w_l = 16.5
                    # w_ls=17.5
                    inv_length = -(MPPTin * 5.33)
                    invs_length = 13500
                    BB_x = 300
                    BBs_x = 160
                    BB_x2 = 134500
                    BB_x3 = -140
                    bb_x3 = 189
                elif MPPTin > 150 and MPPTin <= 500:
                    l = 800
                    l_y = 4000
                    l_s = 330000
                    BB_x = 300
                    BBs_x = 540
                    BB_x2 = 135
                    inverter_x = -73
                    # w_l=16.5
                    # w_ls=17.5
                    inv_length = -(MPPTin * 1.6)
                    invs_length = 54000
                    ls = 82000
                    BB_x3 = -440
                    bb_x3 = 730
                elif MPPTin > 500 and MPPTin <= 1000:
                    l = 800
                    l_y = 4000
                    l_s = 330000
                    BB_x = 300
                    BBs_x = 540
                    BB_x2 = 135
                    inverter_x = -73
                    # w_l=16.5
                    # w_ls=17.5
                    inv_length = -(MPPTin * 0.6)
                    invs_length = 54000
                    ls = 82000
                    BB_x3 = -440
                    bb_x3 = 730
                else:
                    l = 800
                    l_y = 4000
                    ls = (diff * 10) + 82000
                    BB_x = 300
                    BB_x2 = 15
                    inverter_x = (diff * 13.5) + 12
                    inv_length = -(MPPTin * (800 // MPPTin))
                    invs_length = (MPPTin * 25)
                inverters = doc.blocks.get(str(raw))
                if len_data == 3 and MPPTin == 1:
                    ns = -5313
                if len_data == 3 and MPPTin == 2:
                    ns = -593
                if len_data == 3 and MPPTin == 3:
                    ns = -593
                if not inverters:
                    inverter_label = doc.blocks.new(str(raw))
                else:
                    inverter_label = doc.blocks.get(str(raw))

                x_pos = 5
                values = {
                    'inverter_max_inputs': MaxInputs + ': ' + str(Nb_Mppt),
                    'inverter_used_inputs': UsedInputs + ': ' + str(MPPTin),
                    'inverter_in_name': In + str(VL2) + '/' + str(AL2),
                    'inverter_out_name': Out + str(VL3) + '/' + str(AL3),
                    'XPOS': x_pos,
                    'YPOS': 0
                }
                if NumBB > 1:
                    inverter.add_line((0.5, -0.08), (inverter_xx, -0.08))
                if raw == 1:
                    if MPPTin <= 20:
                        xx = 66450.4
                    elif MPPTin > 20 and MPPTin <= 60:
                        xx = 66450.4
                    elif MPPTin > 60 and MPPTin <= 150:
                        xx = 66450.4
                    elif MPPTin > 150 and MPPTin <= 500:
                        xx = 66450.4
                    else:
                        xx = 66450.4
                    label = 'INVERTER'
                else:
                    label = 'INVERTERS'
                global blockref_inverter;
                if len(data_raw) >= 1 and len(end_point) >= 1 and len(frequency.keys()) == 1 and MPPTin > 1 or (
                        len(frequency.keys()) > 1 and len(frequency.keys()) != len(count_list)):
                    if len(end_point) <= 1:
                        if raw == 1:
                            xxs = xx
                            legend_positions = 6500
                            inverter_label.add_attdef('inverter_name', (-50, -(BB_y + 10)),
                                                      dxfattribs={'height': 0.9, 'color': 7})
                            inverter_label.add_attdef('inverter_max_inputs', (-37, -(BB_y + 2)),
                                                      dxfattribs={'height': 0.9, 'color': 7})
                            inverter_label.add_attdef('inverter_used_inputs', (-37, -(BB_y + 4)),
                                                      dxfattribs={'height': 0.9, 'color': 7})
                            inverter_label.add_attdef('inverter_in_name', (BBs_x, -BB_y),
                                                      dxfattribs={'height': 0.9, 'color': 7})
                            inverter_label.add_attdef('inverter_out_name', (BBs_x, -(BB_y + 3)),
                                                      dxfattribs={'height': 0.9, 'color': 7})
                        point = (list(end_points.values())[len(end_points) - 1].dxf.insert[0] - invs_length,
                                 list(end_points.values())[len(end_points) - 1].dxf.insert[1] - inv_y)
                        blockref_inverter = msp.add_blockref(label, point, dxfattribs={
                            'xscale': l,
                            'yscale': l_y,
                            'rotation': 0
                        })
                    else:
                        if raw == 1:
                            xxs = xx
                            legend_positions = 32000
                        inverter_label.add_attdef('inverter_name', ((BB_x2 - 10), -(BB_y + 10)),
                                                  dxfattribs={'height': 0.9, 'color': 7})
                        inverter_label.add_attdef('inverter_max_inputs', (BB_x2, -(BB_y + 2)),
                                                  dxfattribs={'height': 0.9, 'color': 7})
                        inverter_label.add_attdef('inverter_used_inputs', (BB_x2, -(BB_y + 4)),
                                                  dxfattribs={'height': 0.9, 'color': 7})
                        inverter_label.add_attdef('inverter_in_name', (BB_x, -(BB_y)),
                                                  dxfattribs={'height': 0.9, 'color': 7})
                        inverter_label.add_attdef('inverter_out_name', (BB_x, -(BB_y + 3)),
                                                  dxfattribs={'height': 0.9, 'color': 7})
                        point = (list(end_points.values())[len(end_points) - 1].dxf.insert[0] + inv_length,
                                 list(end_points.values())[len(end_points) - 1].dxf.insert[1] - inv_y)
                        # point=(point)
                        blockref_inverter = msp.add_blockref(label, point, dxfattribs={
                            'xscale': l,
                            'yscale': l_y,
                            'rotation': 0
                        })
                else:
                    if len(end_points) <= 1:
                        if raw == 1:
                            xxs = xx
                            legend_positions = 6500
                            inverter_label.add_attdef('inverter_name', (-50, -(BB_y + 10)),
                                                      dxfattribs={'height': 0.9, 'color': 7})
                            inverter_label.add_attdef('inverter_max_inputs', (-37, -(BB_y + 2)),
                                                      dxfattribs={'height': 0.9, 'color': 7})
                            inverter_label.add_attdef('inverter_used_inputs', (-37, -(BB_y + 4)),
                                                      dxfattribs={'height': 0.9, 'color': 7})
                            inverter_label.add_attdef('inverter_in_name', (BBs_x, -BB_y),
                                                      dxfattribs={'height': 0.9, 'color': 7})
                            inverter_label.add_attdef('inverter_out_name', (BBs_x, -(BB_y + 3)),
                                                      dxfattribs={'height': 0.9, 'color': 7})
                        point = (list(end_points.values())[len(end_points) - 1].dxf.insert[0] - invs_length,
                                 list(end_points.values())[len(end_points) - 1].dxf.insert[1] - inv_y)
                        blockref_inverter = msp.add_blockref(label, point, dxfattribs={
                            'xscale': l,
                            'yscale': l_y,
                            'rotation': 0
                        })
                    else:
                        if raw == 1:
                            xxs = xx
                            legend_positions = 32000
                        inverter_label.add_attdef('inverter_name', ((BB_x2 - 10), -(BB_y + 10)),
                                                  dxfattribs={'height': 0.9, 'color': 7})
                        inverter_label.add_attdef('inverter_max_inputs', (BB_x2, -(BB_y + 2)),
                                                  dxfattribs={'height': 0.9, 'color': 7})
                        inverter_label.add_attdef('inverter_used_inputs', (BB_x2, -(BB_y + 4)),
                                                  dxfattribs={'height': 0.9, 'color': 7})
                        inverter_label.add_attdef('inverter_in_name', (BB_x, -(BB_y)),
                                                  dxfattribs={'height': 0.9, 'color': 7})
                        inverter_label.add_attdef('inverter_out_name', (BB_x, -(BB_y + 3)),
                                                  dxfattribs={'height': 0.9, 'color': 7})
                        point = (list(end_points.values())[len(end_points) - 1].dxf.insert[0] + inv_length,
                                 list(end_points.values())[len(end_points) - 1].dxf.insert[1] - inv_y)
                        blockref_inverter = msp.add_blockref(label, point, dxfattribs={
                            'xscale': l,
                            'yscale': l_y,
                            'rotation': 0
                        })
                if type(InvNames) == dict:
                    ##just to handle mppt need to discussed with premjeet
                    InvName = InvNames.get(MPPTin, " ")
                    if not InvName:
                        InvName = InvNames[next(iter(InvNames))]

                    inverter_names_list.append(InvName)
                    Nb_Mppts = Nb_Mppt.get(InvName)
                    if not Nb_Mppts:
                        Nb_Mppts = Nb_Mppt[next(iter(Nb_Mppt))]

                    # InvName = InvNames.get(MPPTin, " ")
                    # print("in", InvName)
                    # inverter_names_list.append(InvName)
                    # Nb_Mppts = Nb_Mppt[InvName]
                else:
                    InvName = InvNames
                    inverter_names_list.append(InvName)
                    Nb_Mppts = Nb_Mppt
                msp.add_text(MaxInputs + ': ' + str(Nb_Mppts), dxfattribs={
                    'height': 60}).set_pos((blockref_inverter.dxf.insert + (BB_x + 1340, 700)), align='CENTER')
                msp.add_text(UsedInputs + ': ' + str(MPPTin), dxfattribs={
                    'height': 60}).set_pos((blockref_inverter.dxf.insert + (BB_x + 1340, 500)), align='CENTER')
                msp.add_text(str(InvName), dxfattribs={
                    'height': 60}).set_pos((blockref_inverter.dxf.insert + (BB_x + 1850, -100)), align='CENTER')
                msp.add_text(In + str(VL2) + '/' + str(AL2), dxfattribs={
                    'height': 60}).set_pos(blockref_inverter.dxf.insert + (BB_x + 1340, 100), align='CENTER')
                msp.add_text(Out + str(VL3) + '/' + str(AL3), dxfattribs={
                    'height': 60}).set_pos(blockref_inverter.dxf.insert + (BB_x + 1340, 300), align='CENTER')
                blockref = msp.add_blockref(str(raw), blockref.dxf.insert, dxfattribs={
                    'xscale': 300,
                    'yscale': 300,
                    'rotation': 0
                })
                inv_list.append(blockref_inverter)
                draw_diagram_chversion(end_points, data_raw, frequency,frequency_count_list, counts, end_point, blockref_inverter,
                                       'LINE_DOWN', count_list,
                                       MPPTin, maximum, diff, total_mpptin, total_dict_length, Mp, new_labels, flags,len_data,
                                       raw, doc, msp)

            else:
                # import pdb
                # pdb.set_trace();
                CBout = 1
                count = 0
                diff = maximum - 150
                if diff % 60 == 0:
                    diff = int(diff / 60)
                else:
                    diff = int(diff / 60) + 1

                if maximum <= 20:
                    n = 6.5
                    n_length = 19.3
                    m_length = 19.65
                    length = 20.9
                    # l=30
                    ns = 500
                    w_l = 13000
                    w_ls = 2000
                    inv_y = 2800
                    BB_y = 36300
                    x_length = 0
                    inverter_xx = 5
                    legend_positionss = 1500
                elif maximum > 20 and maximum <= 60:
                    n = 12
                    n_length = 30.2
                    m_length = 30.65
                    length = 28
                    # l=46
                    ns = 500
                    w_l = 10000
                    w_ls = 2000
                    inv_y = 4500
                    BB_y = 31300
                    x_length = 21000
                    inverter_xx = 5
                    legend_positionss = 1500
                elif maximum > 60 and maximum <= 150:
                    n = 18
                    n_length = 42.3
                    m_length = 42.6
                    length = 28
                    # l=69
                    ns = 500
                    w_l = 13000
                    w_ls = 2000
                    inv_y = 7300
                    BB_y = 274
                    x_length = 21000
                    inverter_xx = 5
                    legend_positionss = -500
                elif maximum > 150 and maximum <= 500:
                    n = 18
                    n_length = 42.3
                    m_length = 42.6
                    length = 28
                    # l=69
                    ns = 500
                    w_l = 13000
                    w_ls = 2000
                    inv_y = 19300
                    BB_y = 1104
                    x_length = 68000
                    inverter_xx = 5
                    legend_positionss = -10000
                elif maximum > 500 and maximum <= 1000:
                    n = 18
                    n_length = 42.3
                    m_length = 42.6
                    length = 28
                    # l=69
                    ns = 500
                    w_l = 13000
                    w_ls = 2000
                    inv_y = 33400
                    BB_y = 1104
                    x_length = 68000
                    inverter_xx = 5
                    legend_positionss = -30000

                else:
                    n = (diff * 12.5) + 18
                    n_length = (25 * diff) + 42.3
                    m_length = (25 * diff) + 42.6
                    ns = 500
                    inv_y = (MPPTin * 35)+2000
                    BB_y = (MPPTin * 29.5) + 269
                    legend_positionss = -(MPPTin*25)
                    w_l = (diff * 3) + 15
                    w_ls = 2000
                    inverter_xx = 5
                    legend_positionss = -40000

                if MPPTin <= 20:
                    l = 800
                    l_y = 4000
                    l_s = 22000
                    ls = 38000
                    BB_x = 300
                    BBs_x = 15
                    BB_x2 = 134500
                    inverter_x = 9
                    inv_length = -(45 + ((MPPTin - 2) * 35))
                    invs_length = 500
                    BB_x3 = -80
                    bb_x3 = 30

                elif MPPTin > 20 and MPPTin <= 60:
                    l = 800
                    l_y = 4000
                    l_s = 45000
                    ls = 42000
                    BB_x = 300
                    BBs_x = 67
                    BB_x2 = 134500
                    inverter_x = 12
                    # w_l = 4
                    # w_ls=7.5
                    inv_length = -(MPPTin * 13.3)
                    invs_length = 4500
                    BB_x3 = -100
                    bb_x3 = 75

                elif MPPTin > 60 and MPPTin <= 150:
                    l = 800
                    l_y = 4000
                    l_s = 89500
                    ls = 82000
                    BB_x = 40
                    inverter_x = -60
                    # w_l = 16.5
                    # w_ls=17.5
                    inv_length = -(MPPTin * 5.33)
                    invs_length = 13500
                    BB_x = 300
                    BBs_x = 160
                    BB_x2 = 20
                    BB_x3 = -140
                    bb_x3 = 189
                elif MPPTin > 150 and MPPTin <= 500:
                    l = 800
                    l_y = 4000
                    l_s = 330000
                    BB_x = 300
                    BBs_x = 540
                    BB_x2 = 135
                    inverter_x = -73
                    # w_l=16.5
                    # w_ls=17.5
                    inv_length = -(MPPTin * 1.6)
                    invs_length = 54000
                    ls = 82000
                    BB_x3 = -440
                    bb_x3 = 730
                elif MPPTin > 500 and MPPTin <= 1000:
                    l = 800
                    l_y = 4000
                    l_s = 330000
                    BB_x = 300
                    BBs_x = 540
                    BB_x2 = 135
                    inverter_x = -73
                    # w_l=16.5
                    # w_ls=17.5
                    inv_length = -(MPPTin * 0.6)
                    invs_length = 54000
                    ls = 82000
                    BB_x3 = -440
                    bb_x3 = 730
                else:
                    l = 800
                    l_y = 4000
                    ls = (diff * 10) + 82000
                    BB_x = 300
                    BB_x2 = 15
                    inverter_x = (diff * 13.5) + 12
                    inv_length = -(MPPTin * (800 // MPPTin))
                    invs_length = (MPPTin * 25)

                inverters = doc.blocks.get(str(raw))
                if len_data == 3 and MPPTin == 1:
                    ns = -5313
                if len_data == 3 and MPPTin == 2:
                    ns = -593
                if len_data == 3 and MPPTin == 3:
                    ns = -593
                if not inverters:
                    inverter_label = doc.blocks.new(str(raw))
                else:
                    inverter_label = doc.blocks.get(str(raw))

                x_pos = 5
                values = {
                    'inverter_max_inputs': MaxInputs + ': ' + str(Nb_Mppt),
                    'inverter_used_inputs': UsedInputs + ': ' + str(MPPTin),
                    'inverter_in_name': In + str(VL2) + '/' + str(AL2),
                    'inverter_out_name': Out + str(VL3) + '/' + str(AL3),
                    'XPOS': x_pos,
                    'YPOS': 0
                }
                if NumBB > 1:
                    inverter.add_line((0.5, -0.08), (inverter_xx, -0.08))
                if raw == 1:
                    if MPPTin <= 20:
                        xx = 66450.4
                    elif MPPTin > 20 and MPPTin <= 60:
                        xx = 66450.4
                    elif MPPTin > 60 and MPPTin <= 150:
                        xx = 66450.4
                    elif MPPTin > 150 and MPPTin <= 500:
                        xx = 66450.4
                    else:
                        xx = 66450.4

                    label = 'INVERTER'
                else:
                    label = 'INVERTERS'
                if len(data_raw) >= 1 and len(end_point) >= 1 and len(frequency.keys()) == 1 and MPPTin > 1 or (
                        len(frequency.keys()) > 1 and len(frequency.keys()) != len(count_list)):
                    if len(end_point) <= 1:
                        if raw == 1:
                            xxs = xx
                            legend_positions = 6500
                        inverter_label.add_attdef('inverter_name', (-50, -(BB_y + 10)),
                                                  dxfattribs={'height': 0.9, 'color': 7})
                        inverter_label.add_attdef('inverter_max_inputs', (-37, -(BB_y + 2)),
                                                  dxfattribs={'height': 0.9, 'color': 7})
                        inverter_label.add_attdef('inverter_used_inputs', (-37, -(BB_y + 4)),
                                                  dxfattribs={'height': 0.9, 'color': 7})
                        inverter_label.add_attdef('inverter_in_name', (BBs_x, -BB_y),
                                                  dxfattribs={'height': 0.9, 'color': 7})
                        inverter_label.add_attdef('inverter_out_name', (BBs_x, -(BB_y + 3)),
                                                  dxfattribs={'height': 0.9, 'color': 7})
                        point = (list(end_points.values())[len(end_points) - 1].dxf.insert[0] - invs_length,
                                 list(end_points.values())[len(end_points) - 1].dxf.insert[1] - inv_y)
                        blockref_inverter = msp.add_blockref(label, point, dxfattribs={
                            'xscale': l,
                            'yscale': l_y,
                            'rotation': 0
                        })
                    else:
                        if raw == 1:
                            xxs = xx
                            legend_positions = 32000
                        inverter_label.add_attdef('inverter_name', ((BB_x2 - 10), -(BB_y + 10)),
                                                  dxfattribs={'height': 0.9, 'color': 7})
                        inverter_label.add_attdef('inverter_max_inputs', (BB_x2, -(BB_y + 2)),
                                                  dxfattribs={'height': 0.9, 'color': 7})
                        inverter_label.add_attdef('inverter_used_inputs', (BB_x2, -(BB_y + 4)),
                                                  dxfattribs={'height': 0.9, 'color': 7})
                        inverter_label.add_attdef('inverter_in_name', (BB_x, -(BB_y)),
                                                  dxfattribs={'height': 0.9, 'color': 7})
                        inverter_label.add_attdef('inverter_out_name', (BB_x, -(BB_y + 3)),
                                                  dxfattribs={'height': 0.9, 'color': 7})
                        point = (list(end_points.values())[len(end_points) - 1].dxf.insert[0] + inv_length,
                                 list(end_points.values())[len(end_points) - 1].dxf.insert[1] - inv_y)
                        blockref_inverter = msp.add_blockref(label, point, dxfattribs={
                            'xscale': l,
                            'yscale': l_y,
                            'rotation': 0
                        })
                else:
                    if len(end_points) <= 1:
                        if raw == 1:
                            xxs = xx
                            legend_positions = 6500
                        inverter_label.add_attdef('inverter_name', (-50, -(BB_y + 10)),
                                                  dxfattribs={'height': 0.9, 'color': 7})
                        inverter_label.add_attdef('inverter_max_inputs', (-37, -(BB_y + 2)),
                                                  dxfattribs={'height': 0.9, 'color': 7})
                        inverter_label.add_attdef('inverter_used_inputs', (-37, -(BB_y + 4)),
                                                  dxfattribs={'height': 0.9, 'color': 7})
                        inverter_label.add_attdef('inverter_in_name', (BBs_x, -BB_y),
                                                  dxfattribs={'height': 0.9, 'color': 7})
                        inverter_label.add_attdef('inverter_out_name', (BBs_x, -(BB_y + 3)),
                                                  dxfattribs={'height': 0.9, 'color': 7})
                        point = (list(end_points.values())[len(end_points) - 1].dxf.insert[0] - invs_length,
                                 list(end_points.values())[len(end_points) - 1].dxf.insert[1] - inv_y)
                        blockref_inverter = msp.add_blockref(label, point, dxfattribs={
                            'xscale': l,
                            'yscale': l_y,
                            'rotation': 0
                        })
                    else:
                        if raw == 1:
                            xxs = xx
                            legend_positions = 32000
                        inverter_label.add_attdef('inverter_name', ((BB_x2 - 10), -(BB_y + 10)),
                                                  dxfattribs={'height': 0.9, 'color': 7})
                        inverter_label.add_attdef('inverter_max_inputs', (BB_x2, -(BB_y + 2)),
                                                  dxfattribs={'height': 0.9, 'color': 7})
                        inverter_label.add_attdef('inverter_used_inputs', (BB_x2, -(BB_y + 4)),
                                                  dxfattribs={'height': 0.9, 'color': 7})
                        inverter_label.add_attdef('inverter_in_name', (BB_x, -(BB_y)),
                                                  dxfattribs={'height': 0.9, 'color': 7})
                        inverter_label.add_attdef('inverter_out_name', (BB_x, -(BB_y + 3)),
                                                  dxfattribs={'height': 0.9, 'color': 7})
                        point = (list(end_points.values())[len(end_points) - 1].dxf.insert[0] + inv_length,
                                 list(end_points.values())[len(end_points) - 1].dxf.insert[1] - inv_y)
                        blockref_inverter = msp.add_blockref(label, point, dxfattribs={
                            'xscale': l,
                            'yscale': l_y,
                            'rotation': 0
                        })
                if type(InvNames) == dict:
                    ##just to handle mppt need to discussed with premjeet
                    InvName = InvNames.get(MPPTin, " ")
                    if not InvName:
                        InvName = InvNames[next(iter(InvNames))]

                    inverter_names_list.append(InvName)
                    Nb_Mppts = Nb_Mppt.get(InvName)
                    if not Nb_Mppts:
                        Nb_Mppts = Nb_Mppt[next(iter(Nb_Mppt))]
                else:
                    InvName = InvNames
                    inverter_names_list.append(InvName)
                    Nb_Mppts = Nb_Mppt
                msp.add_text(MaxInputs + ': ' + str(Nb_Mppts), dxfattribs={
                    'height': 60}).set_pos((blockref_inverter.dxf.insert + (BB_x + 1340, 700)), align='CENTER')
                msp.add_text(UsedInputs + ': ' + str(MPPTin), dxfattribs={
                    'height': 60}).set_pos((blockref_inverter.dxf.insert + (BB_x + 1340, 500)), align='CENTER')
                msp.add_text(str(InvName), dxfattribs={
                    'height': 60}).set_pos((blockref_inverter.dxf.insert + (BB_x + 1850, -100)), align='CENTER')
                msp.add_text(In + str(VL2) + '/' + str(AL2), dxfattribs={
                    'height': 60}).set_pos(blockref_inverter.dxf.insert + (BB_x + 1340, 300), align='CENTER')
                msp.add_text(Out + str(VL3) + '/' + str(AL3), dxfattribs={
                    'height': 60}).set_pos(blockref_inverter.dxf.insert + (BB_x + 1340, 100), align='CENTER')
                blockref = msp.add_blockref(str(raw), blockref.dxf.insert, dxfattribs={
                    'xscale': 300,
                    'yscale': 300,
                    'rotation': 0
                })
                inv_list.append(blockref_inverter)
                draw_diagram_chversion(end_points, data_raw, frequency,frequency_count_list, counts, end_point, blockref_inverter,
                                       'LINE_DOWN',
                                       count_list,
                                       MPPTin, maximum, diff, total_mpptin, total_dict_length, Mp, new_labels, flags,len_data,
                                       raw, doc, msp)

            iNb = iNb + 1

        # ----------------------------------------------------------------------------

        # Placement of the BB label on y-axis
        BBLabelY = 0
        BBLabelY = -3.2

        # Placement of the BB label on x-axis
        BBLabelX = 0

        # Calling the main function bb(), to create the Building Blocks:
        raw = 0
        no = 0
        # import pdb
        # pdb.set_trace();
        project_name = {
            'projects_name': str(ProjName),
            'XPOS': 1,
            'YPOS': 0.5
        }
        point = (1000, 50900)
        blockref_project = msp.add_blockref('project_name_label', point, dxfattribs={
            'xscale': 200,
            'yscale': 300,
            'rotation': 0
        })
        # blockref_project.add_auto_attribs(project_name)
        ground_name = {
            'grounds_name': GND_name,
            'XPOS': 0.2,
            'YPOS': 0.5
        }
        if NumBB == 1:
            for key in res_data:
                no += 1
                # import pdb
                # pdb.set_trace();
                raw += 1
                max_parallel = max(max_parallels)
                total_mpptin = sum(max_parallels)
                if raw == 1:
                    values = {
                        'bb_label': BB + str(1),
                        'XPOS': 1,
                        'YPOS': 0.5
                    }
                    point = (1000, 50500)
                    bb_labels = msp.add_blockref('BB_Labels', point, dxfattribs={
                        'xscale': 500,
                        'yscale': 500,
                        'rotation': 0
                    })

                    bb(res_data[key][:-3], res_data[key][-3], res_data[key][-2], res_data[key][-1], max_parallel,
                       total_mpptin, len(res_data), raw, point)
                else:
                    bb(res_data[key][:-3], res_data[key][-3], res_data[key][-2], res_data[key][-2], max_parallel,
                       total_mpptin, len(res_data), raw, point)

        else:
            # import pdb;
            # pdb.set_trace()
            for key in res_data:
                raw += 1
                no += 1
                max_parallel = max(max_parallels)
                total_mpptin = sum(max_parallels)
                if len(res_data) == len(values[0].keys()):
                    if raw == 1:
                        point = (6200, 2000)
                        bb_values = {
                            'bb_label': BB + str(1),
                            'XPOS': 1,
                            'YPOS': 0.5
                        }
                        lb_point = (1000, 50500)
                        bb_labels = msp.add_blockref('BB_Labels', lb_point, dxfattribs={
                            'xscale': 500,
                            'yscale': 500,
                            'rotation': 0
                        })
                        bb(res_data[key][:-3], res_data[key][-3], res_data[key][-2], res_data[key][-1], max_parallel,
                           total_mpptin, len(res_data), raw, point)
                    else:
                        point = (ns, 2000) + dots_list[-1]
                        bb_values = {
                            'bb_label': BB + str(no),
                            'XPOS': 1,
                            'YPOS': 0.5
                        }
                        lb_point = (ns, 502000) + dots_list[-1]
                        bb_labels = msp.add_blockref('BB_Labels', lb_point, dxfattribs={
                            'xscale': 500,
                            'yscale': 500,
                            'rotation': 0
                        })
                        bb(res_data[key][:-3], res_data[key][-3], res_data[key][-2], res_data[key][-1], max_parallel,
                           total_mpptin, len(res_data), raw, point)
                        point_x1 = blockref_inverter.dxf.insert[0]
                        point_y1 = blockref_inverter.dxf.insert[1] - 320
                        point_x2 = inv_list[0].dxf.insert[0] + w_ls
                        point_y2 = inv_list[0].dxf.insert[1] - 320
                        msp.add_line((point_x1, point_y1), (point_x2, point_y2))
        # # -----------------------------------------------------------------------------
        #

    dir2save = os.getcwd()
    files = {}
    file_basic_nocircuitbreaker = os.path.join(dir2save, 'sldcircuitenversion', 'dxfinput', 'Basic.dxf')
    file_basic_circuitbreaker = os.path.join(dir2save, 'sldcircuitenversion', 'dxfinput', 'Basics.dxf')
    br_legend_file = os.path.join(dir2save, 'sldcircuitenversion', 'dxfinput', 'Legends.dxf')
    files['br_legends']=br_legend_file
    information_file = os.path.join(dir2save, 'sldcircuitenversion', 'dxfinput', 'Informations.dxf')
    files['information'] = information_file
    # component_names = ['CircuitBreaker','metering','ev','decoupling','ac_battery','dc_battery']
    component_names.append('br_legends')
    component_names.append('basic_structures')
    # component_names = ['CircuitBreaker', 'Metering', 'EV', 'Decoupling', 'AC_battery', 'DC_battery']
    if 'CircuitBreaker' not in component_names:
        files['basic_structures'] = file_basic_nocircuitbreaker
    else:
        component_names.remove('CircuitBreaker')
        files['basic_structures']=file_basic_circuitbreaker
    if 'EV' in component_names:
        file_ev = os.path.join(dir2save, 'sldcircuitenversion', 'dxfinput', 'EV.dxf')
        files['ev'] = file_ev
    if 'Metering' in component_names:
        file_metering = os.path.join(dir2save, 'sldcircuitenversion', 'dxfinput', 'Metering.dxf')
        files['metering']= file_metering
    if 'Decoupling' in component_names:
        file_decoupling = os.path.join(dir2save, 'sldcircuitenversion', 'dxfinput', 'Decoupling.dxf')
        files['decoupling']=file_decoupling
    if 'DC_battery' in component_names:
        file_dcbattery = os.path.join(dir2save, 'sldcircuitenversion', 'dxfinput', 'Battery_DC.dxf')
        files['dc_battery']=file_dcbattery
    if 'AC_battery' in component_names:
        file_acbattery= os.path.join(dir2save, 'sldcircuitenversion', 'dxfinput', 'Battery_AC.dxf')
        files['ac_battery']=file_acbattery
    x_index = -1
    # block_s=['br','trfo','ev','meters']
    for components_name in files.keys():
        x_index+=1
        source_dxf = ezdxf.readfile(files[components_name])
        for block in source_dxf.blocks:
            print("blocksname", block.name)
        importer = Importer(source_dxf, doc)
        if components_name =='br_legends' :
            importer.import_block('legends')
        elif components_name== 'basic_structures':
            importer.import_block('basic_structuress')
        elif components_name == 'information':
            importer.import_block('Informationss')
        elif components_name== 'metering':
            importer.import_block('Metering')
        elif components_name== 'decoupling':
            importer.import_block('Decoupling')
        elif components_name == 'ac_battery':
            importer.import_block('Battery_AC')
        elif components_name == 'dc_battery':
            importer.import_block('Battery_DC')
        else:
            msp2=source_dxf.modelspace()
            def get_point():
                for text in msp2.query('TEXT'):
                    if text.dxf.text == 'maxima do inversor':
                        point = text.dxf.insert
                        return point
            point = get_point()
            importer.import_block('EV')
            def get_inverter_location():
                for text in msp2.query('TEXT'):
                    if text.dxf.text == 'maxima do inversor':
                        point = text.dxf.insert
                        return points
            points=get_inverter_location()

        importer.finalize()
        if components_name == 'br_legends':
            block = doc.blocks.get('legends')
            block = block.block
            # print(" initial base point", block.dxf.base_point)
            base_point = block.dxf.base_point
            if MPPTin >60 and MPPTin <=150:
                x_positions = 39500
            elif MPPTin >150:
                x_positions = 190000
            else:
                x_positions = -90500
            if NumBB == 1 and MPPTin == 1:
                xx_positionss = 8000
            else:
                xx_positionss = 35500

            if NumBB == 1 and MPPTin == 1:
                xx_positionss = 8500
            else:
                xx_positionss = 35500
            if MPPTin <= 5:
                x_distances = 33600
            else:
                x_distances = 35500
            if MPPTin == 2 and len_data == 4:
                x_distances = 35900
            if MPPTin == 1 and len_data == 2:
                x_distances = 9900
            if MPPTin == 1 and len_data == 1 and NumBB > 1:
                x_distances = 7900
            if MPPTin == 1 and len_data == 1 and NumBB == 1:
                x_distances = 8000
            if MPPTin == 4 and NumBB == 1:
                x_distances = 4000
            if len_data >= 4 and NumBB == 1:
                x_distances = 6000
            if len_data >= 4 and NumBB == 1:
                xx_positionss = 33900
            if dots_list[-1][0] <=50000:
                dots_list[-1]=((dots_list[-1][0]-xx_positionss),(dots_list[-1][1]))
            elif (dots_list[-1][0] > 50000 and dots_list[-1][0] <=100000):
                dots_list[-1] = ((dots_list[-1][0]-34500),(dots_list[-1][1]))
            else:
                dots_list[-1] = ((dots_list[-1][0]-x_distances),(dots_list[-1][1]))

            block.dxf.base_point = (dots_list[-1])*(-1) + (0,-1310000)
            # block.dxf.base_point = (-50000,-40000, 50000)
            base_point = block.dxf.base_point
            # print(" initial base point", block.dxf.base_point)
            if NumBB == 2 and MPPTin <=6:
                legend_positions += 13000
            x_x = dots_list[-1][0] + legend_positions
            if NumBB == 1 and (len_data == 1 or len_data == 2 or len_data == 3):
                y_y = dots_list[-1][1] - (5500)
            else:
                y_y = dots_list[-1][1] - (legend_positionss)
            if (len_data >=1 and NumBB>1) or (len_data>=4):
                y_y -= 3500
            if len_data == 3 and NumBB == 1:
                xs_positions = 4300
            else:
                xs_positions=3500
            if flags == True and NumBB == 1:
                xs_positions = 6000
            # inverter_names=['illuminousinverter']
            inverter_names_list=list(set(inverter_names_list))
            for block in doc.blocks:
                if block.name == 'legends':
                    for insert in block.query('TEXT'):
                        if insert.dxf.text == 'pv':
                            insert.dxf.height =80
                            insert.dxf.text = str(pv_names)
                        if insert.dxf.text == '5':
                            insert.dxf.height = 100
                            insert.dxf.text = str(total_string)
                        if len(inverter_names_list)==1:
                            if insert.dxf.text == 'inv1':
                                insert.dxf.height = 80
                                insert.dxf.text = inverter_names_list[0]
                        if len(inverter_names_list) >= 2:
                            if insert.dxf.text == 'inv1':
                                insert.dxf.height = 100
                                insert.dxf.text = inverter_names_list[0]
                            if insert.dxf.text == 'inv22':
                                insert.dxf.height = 80
                                insert.dxf.text = inverter_names_list[1]
            block_refernce = msp.add_blockref('legends', (inv_list[-1].dxf.insert + (xs_positions, y_y)), dxfattribs={
                'xscale': 0.35,
                'yscale': 0.35,
            })
        elif components_name=='basic_structures':
            block = doc.blocks.get('basic_structuress')
            block = block.block
            # print(" initial base point", block.dxf.base_point)
            base_point = block.dxf.base_point
            if MPPTin > 60 and MPPTin <= 150:
                x_positions = 39500
            elif MPPTin > 150:
                x_positions = 190000
            else:
                x_positions = -90500
            if NumBB == 1 and MPPTin == 1:
                xx_positionss = 8000
            else:
                xx_positionss = 35500

            if NumBB == 1 and MPPTin == 1:
                xx_positionss = 8500
            else:
                xx_positionss = 35500
            if MPPTin <= 5:
                x_distances = 33600
            else:
                x_distances = 35500
            if MPPTin == 2 and len_data == 4:
                x_distances = 35900
            if MPPTin == 1 and len_data == 2:
                x_distances = 9900
            if MPPTin == 1 and len_data == 1 and NumBB > 1:
                x_distances = 7900
            if MPPTin == 1 and len_data == 1 and NumBB == 1:
                x_distances = 8000
            if MPPTin == 4 and NumBB == 1:
                x_distances = 4000
            if len_data >= 4 and NumBB == 1:
                x_distances = 6000
            if len_data >= 4 and NumBB == 1:
                xx_positionss = 33900
            if dots_list[-1][0] <= 50000:
                dots_list[-1] = ((dots_list[-1][0] - xx_positionss), (dots_list[-1][1]))
            elif (dots_list[-1][0] > 50000 and dots_list[-1][0] <= 100000):
                dots_list[-1] = ((dots_list[-1][0] - 34500), (dots_list[-1][1]))
            else:
                dots_list[-1] = ((dots_list[-1][0] - x_distances), (dots_list[-1][1]))

            block.dxf.base_point = (dots_list[-1]) * (-1) + (0, -1310000)
            # block.dxf.base_point = (-50000,-40000, 50000)
            base_point = block.dxf.base_point
            # print(" initial base point", block.dxf.base_point)
            if NumBB == 2 and MPPTin <= 6:
                legend_positions += 13000
            x_x = dots_list[-1][0] + legend_positions
            y_y = dots_list[-1][1] - (legend_positionss)
            if len_data >= 4:
                y_y -= 2500
            block.dxf.base_point = (10300, -3300)
            block_refernce = msp.add_blockref('basic_structuress', (inv_list[0].dxf.insert + (3800, -9590)), dxfattribs={
                'xscale': 1,
                'yscale': 1,
            })
        elif components_name =='information':
            block = doc.blocks.get('Informationss')
            block = block.block
            # print(" initial base point", block.dxf.base_point)
            base_point = block.dxf.base_point
            if MPPTin > 60 and MPPTin <= 150:
                x_positions = 39500
            elif MPPTin > 150:
                x_positions = 190000
            else:
                x_positions = -90500
            if NumBB == 1 and MPPTin == 1:
                xx_positionss = 8000
            else:
                xx_positionss = 35500

            if NumBB == 1 and MPPTin == 1:
                xx_positionss = 8500
            else:
                xx_positionss = 35500
            if MPPTin <= 5:
                x_distances = 33600
            else:
                x_distances = 35500
            if MPPTin == 2 and len_data == 4:
                x_distances = 35900
            if MPPTin == 1 and len_data == 2:
                x_distances = 9900
            if MPPTin == 1 and len_data == 1 and NumBB > 1:
                x_distances = 7900
            if MPPTin == 1 and len_data == 1 and NumBB == 1:
                x_distances = 8000
            if MPPTin == 4 and NumBB == 1:
                x_distances = 4000
            if len_data >= 4 and NumBB == 1:
                x_distances = 6000
            if len_data >= 4 and NumBB == 1:
                xx_positionss = 33900
            if dots_list[-1][0] <= 50000:
                dots_list[-1] = ((dots_list[-1][0] - xx_positionss), (dots_list[-1][1]))
            elif (dots_list[-1][0] > 50000 and dots_list[-1][0] <= 100000):
                dots_list[-1] = ((dots_list[-1][0] - 34500), (dots_list[-1][1]))
            else:
                dots_list[-1] = ((dots_list[-1][0] - x_distances), (dots_list[-1][1]))

            block.dxf.base_point = (dots_list[-1]) * (-1) + (0, -1310000)
            # block.dxf.base_point = (-50000,-40000, 50000)
            base_point = block.dxf.base_point
            # print(" initial base point", block.dxf.base_point)
            if NumBB == 2 and MPPTin <= 6:
                legend_positions += 13000
            x_x = dots_list[-1][0] + legend_positions
            y_y = dots_list[-1][1] - (legend_positionss)
            if len_data >= 4:
                y_y -= 2500
            block.dxf.base_point = (10300, -3300)
            for block in doc.blocks:
                if block.name == 'Informationss':
                    for insert in block.query('TEXT'):
                        print("textss",insert.dxf.text)
                        if insert.dxf.text == 'companyname':
                            # insert.dxf.insert += (0, 200)
                            insert.dxf.text ='Company:'+str(company_name)
                        if insert.dxf.text == 'Project':
                            # insert.dxf.insert += (0, 200)
                            insert.dxf.text = 'Project:'+str(ProjName)
                        if insert.dxf.text == 'Sales':
                            # insert.dxf.insert += (0, 200)
                            insert.dxf.text ='Salesperson:'+str(salesperson)
                        if insert.dxf.text == 'Date':
                            # insert.dxf.insert += (0, 200)
                            insert.dxf.text = 'Projects'
                        if insert.dxf.text == 'installation':
                            # insert.dxf.insert += (0, 200)
                            insert.dxf.height =0.3
                            insert.dxf.text = 'installation:' + str(installation_address)
                        if insert.dxf.text == 'Plantpower(40KW)':
                            # print("heididid",insert.dxf.height)
                            insert.dxf.height =0.4
                            insert.dxf.text = '40KW'
                        if insert.dxf.text == 'compan':
                            insert.dxf.height =0.0005
                        if insert.dxf.text == 'date':
                            print("heidid")
                            insert.dxf.height = 0.3
                            insert.dxf.text = str(project_created_date)
            block_refernce = msp.add_blockref('Informationss', (inv_list[-1].dxf.insert + (628000, -1212000)),
                                              dxfattribs={
                                                  'xscale': 200,
                                                  'yscale': 200,
                                              })
        elif components_name == 'metering':
            block = doc.blocks.get('Metering')
            block = block.block
            # print(" initial base point", block.dxf.base_point)
            base_point = block.dxf.base_point
            if MPPTin > 60 and MPPTin <= 150:
                x_positions = 39500
            elif MPPTin > 150:
                x_positions = 190000
            else:
                x_positions = -90500
            if NumBB == 1 and MPPTin == 1:
                xx_positionss = 8000
            else:
                xx_positionss = 35500

            if NumBB == 1 and MPPTin == 1:
                xx_positionss = 8500
            else:
                xx_positionss = 35500
            if MPPTin <= 5:
                x_distances = 33600
            else:
                x_distances = 35500
            if MPPTin == 2 and len_data == 4:
                x_distances = 35900
            if MPPTin == 1 and len_data == 2:
                x_distances = 9900
            if MPPTin == 1 and len_data == 1 and NumBB > 1:
                x_distances = 7900
            if MPPTin == 1 and len_data == 1 and NumBB == 1:
                x_distances = 8000
            if MPPTin == 4 and NumBB == 1:
                x_distances = 4000
            if len_data >= 4 and NumBB == 1:
                x_distances = 6000
            if len_data >= 4 and NumBB == 1:
                xx_positionss = 33900
            if dots_list[-1][0] <= 50000:
                dots_list[-1] = ((dots_list[-1][0] - xx_positionss), (dots_list[-1][1]))
            elif (dots_list[-1][0] > 50000 and dots_list[-1][0] <= 100000):
                dots_list[-1] = ((dots_list[-1][0] - 34500), (dots_list[-1][1]))
            else:
                dots_list[-1] = ((dots_list[-1][0] - x_distances), (dots_list[-1][1]))

            block.dxf.base_point = (dots_list[-1]) * (-1) + (0, -1310000)
            # block.dxf.base_point = (-50000,-40000, 50000)
            base_point = block.dxf.base_point
            # print(" initial base point", block.dxf.base_point)
            if NumBB == 2 and MPPTin <= 6:
                legend_positions += 13000
            x_x = dots_list[-1][0] + legend_positions
            y_y = dots_list[-1][1] - (legend_positionss)
            if len_data >= 4:
                y_y -= 2500
            block.dxf.base_point =(10300,-3000)
            block_refernce = msp.add_blockref('Metering', (inv_list[0].dxf.insert + (3800, -9590)), dxfattribs={
                'xscale': 1,
                'yscale': 1,
            })
            doc.saveas(dxf_file2_name)

        elif components_name =='decoupling':
            block = doc.blocks.get('Decoupling')
            block = block.block
            # print(" initial base point", block.dxf.base_point)
            base_point = block.dxf.base_point
            if MPPTin > 60 and MPPTin <= 150:
                x_positions = 39500
            elif MPPTin > 150:
                x_positions = 190000
            else:
                x_positions = -90500
            if NumBB == 1 and MPPTin == 1:
                xx_positionss = 8000
            else:
                xx_positionss = 35500

            if NumBB == 1 and MPPTin == 1:
                xx_positionss = 8500
            else:
                xx_positionss = 35500
            if MPPTin <= 5:
                x_distances = 33600
            else:
                x_distances = 35500
            if MPPTin == 2 and len_data == 4:
                x_distances = 35900
            if MPPTin == 1 and len_data == 2:
                x_distances = 9900
            if MPPTin == 1 and len_data == 1 and NumBB > 1:
                x_distances = 7900
            if MPPTin == 1 and len_data == 1 and NumBB == 1:
                x_distances = 8000
            if MPPTin == 4 and NumBB == 1:
                x_distances = 4000
            if len_data >= 4 and NumBB == 1:
                x_distances = 6000
            if len_data >= 4 and NumBB == 1:
                xx_positionss = 33900
            if dots_list[-1][0] <= 50000:
                dots_list[-1] = ((dots_list[-1][0] - xx_positionss), (dots_list[-1][1]))
            elif (dots_list[-1][0] > 50000 and dots_list[-1][0] <= 100000):
                dots_list[-1] = ((dots_list[-1][0] - 34500), (dots_list[-1][1]))
            else:
                dots_list[-1] = ((dots_list[-1][0] - x_distances), (dots_list[-1][1]))

            block.dxf.base_point = (dots_list[-1]) * (-1) + (0, -1310000)
            # block.dxf.base_point = (-50000,-40000, 50000)
            base_point = block.dxf.base_point
            # print(" initial base point", block.dxf.base_point)
            if NumBB == 2 and MPPTin <= 6:
                legend_positions += 13000
            x_x = dots_list[-1][0] + legend_positions
            y_y = dots_list[-1][1] - (legend_positionss)
            if len_data >= 4:
                y_y -= 2500
            block.dxf.base_point = (10300, -3200)
            block_refernce = msp.add_blockref('Decoupling', (inv_list[0].dxf.insert + (3800, -9590)), dxfattribs={
                'xscale': 1,
                'yscale': 1,
            })
        elif components_name == 'ac_battery':
            block = doc.blocks.get('Battery_AC')
            block = block.block
            # print(" initial base point", block.dxf.base_point)
            base_point = block.dxf.base_point
            if MPPTin > 60 and MPPTin <= 150:
                x_positions = 39500
            elif MPPTin > 150:
                x_positions = 190000
            else:
                x_positions = -90500
            if NumBB == 1 and MPPTin == 1:
                xx_positionss = 8000
            else:
                xx_positionss = 35500

            if NumBB == 1 and MPPTin == 1:
                xx_positionss = 8500
            else:
                xx_positionss = 35500
            if MPPTin <= 5:
                x_distances = 33600
            else:
                x_distances = 35500
            if MPPTin == 2 and len_data == 4:
                x_distances = 35900
            if MPPTin == 1 and len_data == 2:
                x_distances = 9900
            if MPPTin == 1 and len_data == 1 and NumBB > 1:
                x_distances = 7900
            if MPPTin == 1 and len_data == 1 and NumBB == 1:
                x_distances = 8000
            if MPPTin == 4 and NumBB == 1:
                x_distances = 4000
            if len_data >= 4 and NumBB == 1:
                x_distances = 6000
            if len_data >= 4 and NumBB == 1:
                xx_positionss = 33900
            if dots_list[-1][0] <= 50000:
                dots_list[-1] = ((dots_list[-1][0] - xx_positionss), (dots_list[-1][1]))
            elif (dots_list[-1][0] > 50000 and dots_list[-1][0] <= 100000):
                dots_list[-1] = ((dots_list[-1][0] - 34500), (dots_list[-1][1]))
            else:
                dots_list[-1] = ((dots_list[-1][0] - x_distances), (dots_list[-1][1]))

            block.dxf.base_point = (dots_list[-1]) * (-1) + (0, -1310000)
            # block.dxf.base_point = (-50000,-40000, 50000)
            base_point = block.dxf.base_point
            # print(" initial base point", block.dxf.base_point)
            if NumBB == 2 and MPPTin <= 6:
                legend_positions += 13000
            x_x = dots_list[-1][0] + legend_positions
            y_y = dots_list[-1][1] - (legend_positionss)
            if len_data >= 4:
                y_y -= 2500
            block.dxf.base_point = (10315, -3320)
            block_refernce = msp.add_blockref('Battery_AC', (inv_list[0].dxf.insert + (3800, -9590)), dxfattribs={
                'xscale': 1,
                'yscale': 1,
            })
        elif components_name =='dc_battery':
            block = doc.blocks.get('Battery_DC')
            block = block.block
            # print(" initial base point", block.dxf.base_point)
            base_point = block.dxf.base_point
            if MPPTin > 60 and MPPTin <= 150:
                x_positions = 39500
            elif MPPTin > 150:
                x_positions = 190000
            else:
                x_positions = -90500
            if NumBB == 1 and MPPTin == 1:
                xx_positionss = 8000
            else:
                xx_positionss = 35500

            if NumBB == 1 and MPPTin == 1:
                xx_positionss = 8500
            else:
                xx_positionss = 35500
            if MPPTin <= 5:
                x_distances = 33600
            else:
                x_distances = 35500
            if MPPTin == 2 and len_data == 4:
                x_distances = 35900
            if MPPTin == 1 and len_data == 2:
                x_distances = 9900
            if MPPTin == 1 and len_data == 1 and NumBB > 1:
                x_distances = 7900
            if MPPTin == 1 and len_data == 1 and NumBB == 1:
                x_distances = 8000
            if MPPTin == 4 and NumBB == 1:
                x_distances = 4000
            if len_data >= 4 and NumBB == 1:
                x_distances = 6000
            if len_data >= 4 and NumBB == 1:
                xx_positionss = 33900
            if dots_list[-1][0] <= 50000:
                dots_list[-1] = ((dots_list[-1][0] - xx_positionss), (dots_list[-1][1]))
            elif (dots_list[-1][0] > 50000 and dots_list[-1][0] <= 100000):
                dots_list[-1] = ((dots_list[-1][0] - 34500), (dots_list[-1][1]))
            else:
                dots_list[-1] = ((dots_list[-1][0] - x_distances), (dots_list[-1][1]))

            block.dxf.base_point = (dots_list[-1]) * (-1) + (0, -1310000)
            # block.dxf.base_point = (-50000,-40000, 50000)
            base_point = block.dxf.base_point
            # print(" initial base point", block.dxf.base_point)
            if NumBB == 2 and MPPTin <= 6:
                legend_positions += 13000
            x_x = dots_list[-1][0] + legend_positions
            y_y = dots_list[-1][1] - (legend_positionss)
            if len_data >= 4:
                y_y -= 2500
            block.dxf.base_point = (9300, -650)
            block_refernce = msp.add_blockref('Battery_DC', (inv_list[0].dxf.insert + (3800, -9590)), dxfattribs={
                'xscale': 1,
                'yscale': 1,
            })

        else:
            block = doc.blocks.get('EV')
            block = block.block
            # print(" initial base point", block.dxf.base_point)
            base_point = block.dxf.base_point
            if MPPTin > 60 and MPPTin <= 150:
                x_positions = 39500
            elif MPPTin > 150:
                x_positions = 190000
            else:
                x_positions = -90500
            if NumBB == 1 and MPPTin == 1:
                xx_positionss = 8000
            else:
                xx_positionss = 35500

            if NumBB == 1 and MPPTin == 1:
                xx_positionss = 8500
            else:
                xx_positionss = 35500
            if MPPTin <= 5:
                x_distances = 33600
            else:
                x_distances = 35500
            if MPPTin == 2 and len_data == 4:
                x_distances = 35900
            if MPPTin == 1 and len_data == 2:
                x_distances = 9900
            if MPPTin == 1 and len_data == 1 and NumBB > 1:
                x_distances = 7900
            if MPPTin == 1 and len_data == 1 and NumBB == 1:
                x_distances = 8000
            if MPPTin == 4 and NumBB == 1:
                x_distances = 4000
            if len_data >= 4 and NumBB == 1:
                x_distances = 6000
            if len_data >= 4 and NumBB == 1:
                xx_positionss = 33900
            if dots_list[-1][0] <= 50000:
                dots_list[-1] = ((dots_list[-1][0] - xx_positionss), (dots_list[-1][1]))
            elif (dots_list[-1][0] > 50000 and dots_list[-1][0] <= 100000):
                dots_list[-1] = ((dots_list[-1][0] - 34500), (dots_list[-1][1]))
            else:
                dots_list[-1] = ((dots_list[-1][0] - x_distances), (dots_list[-1][1]))

            block.dxf.base_point = (dots_list[-1]) * (-1) + (0, -1310000)
            # block.dxf.base_point = (-50000,-40000, 50000)
            base_point = block.dxf.base_point
            # print(" initial base point", block.dxf.base_point)
            if NumBB == 2 and MPPTin <= 6:
                legend_positions += 13000
            x_x = dots_list[-1][0] + legend_positions
            y_y = dots_list[-1][1] - (legend_positionss)
            if len_data >= 4:
                y_y -= 2500
            block.dxf.base_point = (10315, -3030)
            block_refernce = msp.add_blockref('EV', (inv_list[0].dxf.insert + (3800, -9590)), dxfattribs={
                'xscale': 1,
                'yscale': 1,
            })
        zoom.extents(msp, factor=1)
        # Layout2=doc.new_layout('Layout2')
        create_viewports(layout, msp, 'R2010')
        doc.saveas(dxf_file2_name)
