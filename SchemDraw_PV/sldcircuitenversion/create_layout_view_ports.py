import math
from pathlib import Path
import ezdxf
from ezdxf import zoom


def create_viewports(paperspace,msp, dxfversion):
    # paperspace.ZOOM_TO_PAPER_ON_UPDATE
    # paperspace.set_plot_type(4)
    vp = paperspace.add_viewport(
        center=(7000, 2750), size=(14000, 5500), view_center_point=(10000, 5000), view_height=20000
    )
    vp.dxf.view_target_point = (1000,40000, 2000)
    vp.dxf.view_direction_vector = (0, 0, 1)
    # paperspace.ZOOM_TO_PAPER_ON_UPDATE
    # zoom.window(model,(7000,2750),(10000,5000))

    zoom.objects(layout=paperspace,entities=msp)
    # zoom.extents(paperspace)
    # vp.set_flag_state(ezdxf.const.VSF_FAST_ZOOM,128)
    # vp.reset_limits((0,0),(14000,5500))
    paperspace.add_text(
        "Viewport to 3D Mesh", dxfattribs={"height": 0.18, "color": 1}
    ).set_pos((16, 12.5), align="CENTER")
