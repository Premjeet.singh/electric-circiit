#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan  8 09:53:56 2020

@author: vaga
"""

import os, json, re
import pandas as pd
import numpy as np
import datetime as dt
import pickle
import importlib
# from xhtml2pdf import pisa
import pdfkit
import base64
from jinja2 import Environment, FileSystemLoader
from webcalc.webcalcMods import TrendMaker as TM
# from webcalc.webcalcMods import Supplier as Sup
# from webcalc.webcalcMods import Storage as ST
from addonModules import ComponentDB as CDB
from addonModules import PvSystemData_Calculation as PVcalc
from addonModules import ReportPlots as RPlt
from addonModules import ReportTemplateSpecificGen as RTSG
from google.cloud import storage, datastore
# import pdfkit

import matplotlib.pyplot as plt
import inspect

# import matplotlib.dates as mdates
# import matplotlib.patches as mpatches
# import seaborn as sns
# from matplotlib import sankey
# from io import BytesIO

if __name__ == '__main__':
    Envvar = {"Environ":
                  {"BUCKET_NAME": "solbucket",
                   "PV_DB_FILENAME": "CECModules07-2020.pkl",
                   "PVS_DB_FILENAME": "PvSystModules2020-06.pkl",
                   "INV_DB_FILENAME": "BigInvCEC07-2020.pkl",
                   "NASA_22YR_FILENAME": "22yr_nasa_monthly.pkl",
                   "USER_UPLOADS_FOLDER": "UserUploads/",
                   "FETCHED_WEATHER_FOLDER": "FetchedWeather/",
                   "AUXFILES_FOLDER": "AuxFiles/",
                   "RESULTS_FOLDER": "Results/",
                   "ELECPARAMS": "elec_design_params.json",
                   "FINPARAMS": "finparameter_inputs.json",
                   "SOLARCALCLEADS": "CalcLeads/",
                   "GOOGLE_CLOUD_PROJECT": "solex-mvp-2"}}
    for var in Envvar['Environ']:
        os.environ[var] = Envvar['Environ'][var]

USER_UPLOADS = os.environ['USER_UPLOADS_FOLDER']
AUXFILES = os.environ['AUXFILES_FOLDER']
BATT_DB = os.environ['BATT_DB_FILENAME']


class Report():
    def __init__(self, bucket, abstemppath=None, templatename='template-gen-v1', lang='EN', path2template=''):
        # self.contentlist = contentlist
        # import pdb
        # pdb.set_trace()
        self.errmsg = ""
        self.W2D = {}
        self.bucket = bucket
        self.localstoredfiles = []
        self.options = {}
        if abstemppath:
            self.abstemppath = abstemppath
        self.templatename = templatename
        self.lang = lang
        self.path2template = path2template if path2template != '' else AUXFILES + self.templatename

    def gettemplatefrombucket(self, bucket, path2template):
        # import pdb
        # pdb.set_trace();
        self.bucket = bucket

        # self.templatename = path2template.split('/')[-1]
        ##### Functions to use in jinja
        # Convert multiline text to iterator
        def multiline(multitext):
            # return [line.strip() for line in re.findall(r'^.*$',multitext,re.MULTILINE)]
            return [line.strip() for line in multitext.split('<br>')]

        func_dict = {"multiline": multiline}

        def numformatter_de(num2format, dp=0):
            try:
                if dp == 0:
                    return str('{:,.0f}'.format(num2format)).replace(",", "&rsquo;").replace(".", ",")
                else:
                    return str(str("{:,." + str(dp) + "f}").format(num2format)).replace(",", "&rsquo;").replace(".",
                                                                                                                ",")
            except:
                return str(num2format)

        func_dict.update({"numformatter_de": numformatter_de})

        def numformatter_en(num2format, dp=0):
            try:
                if dp == 0:
                    return str('{:,.0f}'.format(num2format)).replace(",", "&rsquo;")
                else:
                    return str(str("{:,." + str(dp) + "f}").format(num2format)).replace(",", "&rsquo;")
            except:
                return str(num2format)

        func_dict.update({"numformatter_en": numformatter_en})
        #####
        # TODO: TEMP FIX
        # if self.lang == 'FR':
        #     self.templatename = path2template.split('fr-')[-1]
        self.path2template = path2template
        cwd = os.curdir
        blobs = self.bucket.list_blobs(prefix=path2template)  # Get list of files

        for blob in blobs:
            filename = blob.name.split('/')[-1]
            if (filename != ''):
                blob.download_to_filename(os.path.join(cwd, filename))  # Download file here

                self.localstoredfiles.append(os.path.abspath(os.path.join(cwd, filename)))
                #    s = blob.download_as_string()

        self.abstemppath = os.path.abspath(cwd)

        try:
            file_loader = FileSystemLoader(cwd)
            env = Environment(loader=file_loader)
            self.template = env.get_template('base.html')
            # Define Python Functions to use in jinja here
            self.template.globals.update(func_dict)
        except:
            self.errmsg += "cwd :" + cwd + "file :" + str(self.localstoredfiles)
            return "ERROR"

    def GenReportVars(self, entity, PVModuleData, InvData, orgentity, bucket=None):
        # import pdb
        # pdb.set_trace();
        self.ReportContents = {}
        cwd = os.curdir
        ds = datastore.Client()
        if bucket:
            self.bucket = bucket
        projectdetails = entity['projectdetails']
        projectdetails.update({"id": str(entity.key.name)})
        projectdetails.update({"email": str(entity.key.parent.name)})
        self.ReportContents.update({"projectdetails": projectdetails})
        self.ReportContents.update({"abspath": self.abstemppath})
        self.ReportContents.update({"ImageFolder": orgentity['ImagesDb']})

        # OrgInfo = json.loads(self.bucket.get_blob(AUXFILES+"organisation_info.json").download_as_string())
        # OrgDict = OrgInfo.get(orgentity.key.name,OrgInfo['Agrola'])

        OrgBomInput = orgentity.get('bominput', {})
        OrgDetails = orgentity.get('info', {"name": "Solar Company", "address": ""})

        userentity = ds.get(entity.key.parent)
        newones = ['jobtitle', 'firstname', 'lastname', 'phone', 'address']
        userdet = {}
        for n in newones:
            if n in userentity['userdetails']:
                userdet[n] = userentity['userdetails'][n]
            else:
                userdet[n] = " "
        s = userdet['phone'].strip().replace(" ", "")
        userdet['phone'] = re.sub(r'(\d{2})(\d{2})(\d{3})(\d{2})(\d{2})', r'\1 \2 \3 \4 \5', s)
        # Phone Number +41 87 999 12 12 format

        self.ReportContents.update({"userdetails": userdet})
        # Dates
        self.ReportContents.update({"todaysdate": dt.datetime.now()})
        #  =========== OrgDetails =======
        self.ReportContents.update({"orgname": OrgDetails['name']})
        self.ReportContents.update({"orgaddress": OrgDetails['address']})
        # Get Logo
        logoblob = orgentity['ImagesDb'] + 'orglogo.png'
        if self.bucket.get_blob(logoblob):
            self.bucket.get_blob(logoblob).download_to_filename(os.path.join(cwd, 'tmp.png'))
            with open('tmp.png', 'rb') as f:
                image = base64.b64encode(f.read())
            self.localstoredfiles.append(os.path.abspath(os.path.join(cwd, 'tmp.png')))

            image = image.decode('utf-8')
            if image:
                self.ReportContents.update({"OrgLogo": image})
            else:
                self.ReportContents.update({"OrgLogo": ""})
        else:
            self.ReportContents.update({"OrgLogo": ""})
        # ===============================

        ## ==========WEATHER========================
        SolData = pickle.loads(self.bucket.get_blob(entity['Weather']['finalweatherfile']).download_as_string())
        TZ = entity['Weather']['TZ']
        ## ========= LOCATION ======================
        self.ReportContents.update({'Location': entity['Location']})
        tilt = entity['Location']['tilt']
        azimuth = entity['Location']['azimuth']
        longitude = entity['Location']['longitude']
        latitude = entity['Location']['latitude']
        ## Weather2Display - Monthly Summary ###
        locationD = type('locationclass', (object,),
                         {"latitude": latitude, "longitude": longitude})
        Weather2Display = pd.DataFrame()
        for AreaLabel in tilt:
            tdf = pd.concat(
                {AreaLabel: PVcalc.WeatherDatatoDisplay(SolData, locationD, tilt[AreaLabel], azimuth[AreaLabel])},
                names=['AreaLabels'])
            Weather2Display = pd.concat([Weather2Display, tdf])

        self.W2D = (Weather2Display[['GHI', 'IrrOnSurface', 'IrrOnSurface_Ideal']].loc[pd.IndexSlice[
                                                                                       Weather2Display.groupby(
                                                                                           level=[0]).sum()[
                                                                                           'IrrOnSurface'].idxmax(), :]
                    ].groupby([pd.Grouper(freq='1M')]).sum() / 1000).to_dict('list')  # Convert to kWh/m2
        self.W2D['Temperature'] = list((Weather2Display[['Temperature']].loc[pd.IndexSlice[
                                                                             Weather2Display.groupby(level=[0]).sum()[
                                                                                 'IrrOnSurface'].idxmax(), :]
                                        ].groupby([pd.Grouper(freq='1M')]).mean()).to_dict('list').values())[0]
        Months = pd.date_range(start='1/1/2020', periods=12, freq='M')
        monthlabels = [dt.datetime.strftime(s, '%b') for s in Months]

        monthlysummary = pd.DataFrame(self.W2D)
        monthlysummary['Month'] = monthlabels
        monthlysummary['dailyrad'] = monthlysummary['IrrOnSurface'].div(np.array([x.day for x in Months]))
        self.ReportContents.update({"Weather": {"weathersource": entity['Weather']['weathersource'],
                                                "totalyearly": np.sum(self.W2D['IrrOnSurface']),
                                                "monthlysummary": monthlysummary}})
        ## ==================== PROJECT DETAILS ===========================
        self.ReportContents.update({"projectdetails": entity['projectdetails']})

        ## ==================== SCREENSHOT OF LOCATION =====================
        uniqueid = "UID_" + entity.key.parent.name + "_PID_" + entity.key.name + "_screenshot_"
        scrblobs = self.bucket.list_blobs(prefix=USER_UPLOADS + uniqueid)  # Get list of files
        PicList = [(b.updated, b.name) for b in scrblobs]
        import pdb
        pdb.set_trace();
        if PicList:
            # import pdb
            # pdb.set_trace();
            PicList.sort(key=lambda t: t[0], reverse=True)
            LatestPic = PicList[0][1]  # Name of latest pic
            blob = self.bucket.get_blob(LatestPic)
            Screenshot = base64.b64encode(blob.download_as_string())
            Screenshot = Screenshot.decode('utf8')
            self.ReportContents.update({"locscreenshot": Screenshot})
        else:
            self.ReportContents.update({"locscreenshot": ""})

        ## ================== SOLAR CONFIG COMPONENTS =====================================
        if 'SelectedConfig_mu' in entity:
            self.ReportContents.update({"SelectedConfig": entity['SelectedConfig_mu']['Config']})
            self.ReportContents.update({"ConfigResultsSummary": entity['SelectedConfig_mu']['ConfigResultsSummary']})
            self.ReportContents.update({"PVModuleData": PVModuleData.to_dict()})
            self.ReportContents.update({"InvData": dict(InvData)})
        else:
            self.ReportContents.update({"SelectedConfig": entity['SelectedConfig']['Config']})
            self.ReportContents.update({"ConfigResultsSummary": entity['SelectedConfig']['ConfigResultsSummary']})
            self.ReportContents.update({"PVModuleData": PVModuleData.to_dict()})
            self.ReportContents.update({"InvData": InvData.to_dict()})

        ## =================== TARIFF EPRICE DEMAND =======================
        ## DEMAND ###
        self.ReportContents.update({"Demand": entity['Demand']})

        subs = {"DE": {"WP": "Wärmepumpe", "OL": "Oel/Holz", "EL": "El. Boiler"},
                "EN": {"WP": "Heat Pump", "OL": "Oil/Wood", "EL": "El. Boiler"}}

        if "Customer Inputs" in entity:
            try:
                self.ReportContents.update(
                    {"CustomerInputs": {"Heating": subs[self.lang][entity['Customer Inputs']['Heating']],
                                        "Water": subs[self.lang][entity['Customer Inputs']['Water']],
                                        "TotalElec": entity['Demand']['YearlyDemand'],
                                        "TotalHeat": entity['Customer Inputs']['YearlyElecRequired']['Elec4HeatkWh']},
                     "NumPeople": entity['Customer Inputs']['NumPeople'],
                     "Type": entity['Customer Inputs']['Type']})
            except:
                self.ReportContents.update(
                    {"CustomerInputs": {"Heating": "", "Water": "", "TotalElec": entity['Demand']['YearlyDemand']
                        , "TotalHeat": 0}})
        else:
            self.ReportContents.update(
                {"CustomerInputs": {"Heating": "", "Water": "", "TotalElec": entity['Demand']['YearlyDemand']
                    , "TotalHeat": 0}})
        DemandTS = pickle.loads(self.bucket.get_blob(entity['Demand']['DemandTS']).download_as_string())
        ###### TARIFF EPRICE #####
        if 'EpriceTariffFile' in entity:
            TariffDic = pickle.loads(self.bucket.get_blob(entity['EpriceTariffFile']).download_as_string())
            self.ReportContents.update({"PVTariffSummary": TariffDic['PVTariffSummary']})
            Eprice = TariffDic['PVTariffSummary'].get('EpriceAvg', TariffDic['YearlyTariff']['Eprice'].mean())
            Eprice = TariffDic['YearlyTariff']['Eprice']


        else:
            self.ReportContents.update({"PVTariffSummary": {"EpriceAvg": 0, "pvtariffAvg": 0}})
        ## =================ENERGY ==========================================
        energydisplay = entity['FinalSummary']['Energydisplay']
        self.ReportContents.update(energydisplay)
        ## =========================== LOSS CHAIN ==========================
        if 'TotalLossChain' in entity['FinalSummary']:
            TL = entity['FinalSummary']['TotalLossChain']
            TotalLossChainDf = pd.DataFrame(TL)
            TotalLossChainDf.index = TotalLossChainDf.index.astype(int)
            TotalLossChainDf.sort_index(inplace=True)
            self.ReportContents.update({"TotalLossChain": TotalLossChainDf})
        else:
            self.ReportContents.update({"TotalLossChain": pd.DataFrame()})

        ## ============================ BATTERY ============================

        blobnameBattTS = entity['Battery']['TS']
        FinalTS = pickle.loads(self.bucket.get_blob(blobnameBattTS).download_as_string())

        self.ReportContents.update({"BatterySpec": entity['Battery']['BatterySpec']})
        DemandTS = FinalTS['Demand']

        BatterySpec = entity['Battery']['BatterySpec']
        if type(BatterySpec) != str:
            BattName = entity['Battery']['BatterySpec']['Type']
            BatterySpec2store = BatterySpec
        else:
            BattName = 'None'
            BatterySpec2store = {"Type": "None", "Technology": "", "MaxE2P": 0, "MinE2P": 0, "PmaxD": 0,
                                 "NumCabinets": 0, "Estep": 0,
                                 "ChargeEff": 0, "CycleLife": 0, "Emax": 0, "PmaxC": 0, "Enom": 0, "DischargeEff": 0,
                                 "E2P": 0}
        self.ReportContents.update({"BatterySpec": BatterySpec2store})
        BATTDF = pickle.loads(self.bucket.get_blob(BATT_DB).download_as_string())
        if BattName in BATTDF:
            BatteryMod = BATTDF[BattName]
        else:
            BatteryMod = {"E2P": 2,
                          "ChargeEff": 0.96, "DischargeEff": 0.96}
        # Battery KPIs
        Ch_ef = BatteryMod['ChargeEff']
        Di_ef = BatteryMod['DischargeEff']
        EnergyAggKPI = TM.ReportMakerTrends(FinalTS, BatteryMod, TZ)  # {"EnergyAggKPI}

        EnergyAggKPI['selfconsumptionWbatt']['year'] = entity['Battery']['KPI']['SelfConsumptionWBatt']
        EnergyAggKPI['independanceWbatt']['year'] = entity['Battery']['KPI']['FulfilmentWBatt']

        self.ReportContents.update({"EnergyAgg": EnergyAggKPI})
        ## ====================== BILL OF MATERIALS ===========================
        # GET FROM SQL DB IF EXISTING OR FROM FINALSUMMARY ENTITY
        if 'ActiveQuote' in entity:

            ActiveQuote = entity['ActiveQuote']

            self.ReportContents.update({"ActiveQuote": ActiveQuote})
            ViewQuoteDf = CDB.GetBoMQuote(entity.key.name, ActiveQuote['QuoteID'], ActiveQuote['RevID'])

            ViewQuoteDf.fillna(np.nan, inplace=True)
            ViewQuoteDf.replace([np.nan], [None], inplace=True)
            # ViewQuoteDf.loc[~ViewQuoteDf['PerfModelKey'].isnull(),'PerfModelKey']=ViewQuoteDf.loc[~ViewQuoteDf['PerfModelKey'].isnull(),'PerfModelKey'].apply(lambda x:x.replace('\ufeff','')).values
            ViewQuoteDf['Quantity'] = ViewQuoteDf['Quantity'].apply(lambda x: int(x) if x else x)
            ViewQuoteDf.fillna(np.nan, inplace=True)
            ViewQuoteDf.replace([np.nan], [None], inplace=True)

            ViewQuoteDf = CDB.BoMExcludeFlags(ViewQuoteDf, OrgBomInput=OrgBomInput)

            # BoMDisplay=ViewQuoteDf.loc[(ViewQuoteDf['Quantity']!=0)|(ViewQuoteDf['TotalCost']!=0)]
            BoMDisplay = ViewQuoteDf.loc[~(ViewQuoteDf['Quantity'].isnull())].copy()
            BoMDisplay['Num'] = BoMDisplay['Quantity']
            BoMDisplay['CostperUnit'] = BoMDisplay['Cost']
            BoMDisplay['Curr'] = BoMDisplay['Currency']
            BoMDisplay['Name'] = BoMDisplay['Name'].apply(lambda x: x.replace('_', ' '))

            self.ReportContents.update({"BoMDisplay": BoMDisplay[
                ~(BoMDisplay['Quantity'].isnull()) & ((BoMDisplay['Quantity'] != 0))].drop_duplicates(subset=['Name'])})

            BoMDisplay = BoMDisplay[
                ~(BoMDisplay['Quantity'].isnull()) & ((BoMDisplay['Quantity'] != 0))].drop_duplicates(subset=['Name'])

            # BreakDown for key components
            try:
                # TODO: Temporary stucture until all the templates are updated to be multinv ready
                ComponentBreakDown = {}
                if len(BoMDisplay.loc[BoMDisplay['Group'] == 'Solarmodule'].index) > 1:
                    SolarBreakdown = {"Solarmodule": [{"Num": BoMDisplay.loc[ind, 'Quantity'],
                                                       "NetPrice": BoMDisplay.loc[ind, 'NetPrice'],
                                                       "UnitPrice": (BoMDisplay.loc[ind, 'NetPrice'] /
                                                                     BoMDisplay.loc[ind, 'Quantity']),
                                                       "PricePerWp": (BoMDisplay.loc[ind, 'NetPrice'] /
                                                                      (entity['FinalSummary']['OverallSummary'][
                                                                           'DCPower'] * 1000)),
                                                       "Name": BoMDisplay.loc[ind, 'Name'],
                                                       "Manufacturer": BoMDisplay.loc[ind, 'Manufacturer']
                                                       }
                                                      for ind in
                                                      BoMDisplay.loc[BoMDisplay['Group'] == 'Solarmodule'].index]}
                else:
                    ind = BoMDisplay.loc[BoMDisplay['Group'] == 'Solarmodule'].index
                    SolarBreakdown = {"Solarmodule": {"Num": BoMDisplay.loc[ind, 'Quantity'].values[0],
                                                      "NetPrice": BoMDisplay.loc[ind, 'NetPrice'].values[0],
                                                      "UnitPrice": (BoMDisplay.loc[ind, 'NetPrice'].values[0] /
                                                                    BoMDisplay.loc[ind, 'Quantity'].values[0]),
                                                      "PricePerWp": (BoMDisplay.loc[ind, 'NetPrice'].values[0] /
                                                                     (entity['FinalSummary']['OverallSummary'][
                                                                          'DCPower'] * 1000)),
                                                      "Name": BoMDisplay.loc[ind, 'Name'].values[0],
                                                      "Manufacturer": BoMDisplay.loc[ind, 'Manufacturer'].values[0]
                                                      }
                                      }

                ComponentBreakDown.update(SolarBreakdown)

                if len(BoMDisplay.loc[BoMDisplay['Group'] == 'Inverter'].index) > 1:
                    InvBreakdown = {"Inverter": [{"Num": BoMDisplay.loc[ind, 'Quantity'],
                                                  "NetPrice": BoMDisplay.loc[ind, 'NetPrice'],
                                                  "UnitPrice": (BoMDisplay.loc[ind, 'NetPrice'] /
                                                                BoMDisplay.loc[ind, 'Quantity']),
                                                  "PricePerWp": (BoMDisplay.loc[ind, 'NetPrice'] /
                                                                 (entity['FinalSummary']['OverallSummary'][
                                                                      'DCPower'] * 1000)),
                                                  "Name": BoMDisplay.loc[ind, 'Name'],
                                                  "Manufacturer": BoMDisplay.loc[ind, 'Manufacturer']
                                                  } for ind in BoMDisplay.loc[BoMDisplay['Group'] == 'Inverter'].index]}
                else:
                    ind = BoMDisplay.loc[BoMDisplay['Group'] == 'Inverter'].index
                    InvBreakdown = {"Inverter": {"Num": BoMDisplay.loc[ind, 'Quantity'].values[0],
                                                 "NetPrice": BoMDisplay.loc[ind, 'NetPrice'].values[0],
                                                 "UnitPrice": (BoMDisplay.loc[ind, 'NetPrice'].values[0] /
                                                               BoMDisplay.loc[ind, 'Quantity'].values[0]),
                                                 "PricePerWp": (BoMDisplay.loc[ind, 'NetPrice'].values[0] /
                                                                (entity['FinalSummary']['OverallSummary'][
                                                                     'DCPower'] * 1000)),
                                                 "Name": BoMDisplay.loc[ind, 'Name'].values[0],
                                                 "Manufacturer": BoMDisplay.loc[ind, 'Manufacturer'].values[0]
                                                 }}
                ComponentBreakDown.update(InvBreakdown)
                ind = BoMDisplay.loc[BoMDisplay['Group'] == 'Battery'].index
                if len(BoMDisplay.loc[BoMDisplay['Group'] == 'Battery'].index) > 0:
                    BattBreakdown = {"Battery": {"Num": BoMDisplay.loc[ind, 'Quantity'].values[0],
                                                 "NetPrice": BoMDisplay.loc[ind, 'NetPrice'].values[0],
                                                 "UnitPrice": (BoMDisplay.loc[ind, 'NetPrice'].values[0] /
                                                               BoMDisplay.loc[ind, 'Quantity'].values[0]),
                                                 "PricePerWp": (BoMDisplay.loc[ind, 'NetPrice'].values[0] /
                                                                (entity['FinalSummary']['OverallSummary'][
                                                                     'DCPower'] * 1000)),
                                                 "Name": BoMDisplay.loc[ind, 'Name'].values[0],
                                                 "Manufacturer": BoMDisplay.loc[ind, 'Manufacturer'].values[0]
                                                 }
                                     }
                    ComponentBreakDown.update(BattBreakdown)
            except IndexError:
                ComponentBreakDown = {"Solarmodule": {}, "Inverter": {}, "Battery": {}}

            self.ReportContents.update({"ComponentBreakDown": ComponentBreakDown})
            self.ReportContents.update({"Currency": BoMDisplay['Currency'].values[0]})

        else:
            BoMblobname = entity['FinalSummary']['BoM']
            BoMDf = pickle.loads(self.bucket.get_blob(BoMblobname).download_as_string())
            """
            BoMDf.loc[(AreaLabel,costitems),['TotalCost','Num','CostperUnit','Unit','Name']]

            """
            BoMDisplay = BoMDf.groupby(level=[1]).sum()  # Total across all areas
            BoMDisplay[['CostperUnit', 'Unit', 'Curr', 'Name']] = BoMDf.xs(BoMDf.index.get_level_values(0)[0])[
                ['CostperUnit', 'Unit', 'Curr', 'Name']]
            BoMDisplay.dropna(inplace=True)
            BoMDisplay['Description'] = ' '
            BoMDisplay['Name'] = BoMDisplay['Name'].apply(lambda x: x.replace('_', ' '))
            BoMDisplay['Type'] = ''
            BoMDisplay['DisplayName'] = BoMDisplay['Name']

            ################################
            self.ReportContents.update({"BoMDisplay": BoMDisplay})

            ## ==================== FINANCIAL KPIS ==========================

        blobcash = entity['FinalSummary']['CashFlow']
        OvSum = entity['FinalSummary']['OverallSummary']
        if type(blobcash) == str:
            CashFlowDf = pickle.loads(self.bucket.get_blob(blobcash).download_as_string())
        else:
            CashFlowDf = pd.DataFrame(blobcash)  # CashFlow is in raw form
        FinKPIs = {"IRR": OvSum['IRR'], "PaybackTime": OvSum['PaybackTime']}
        self.ReportContents.update(
            {"Financial": {"CashFlowDf": CashFlowDf, "finparams": dict(entity['finparams']).copy(),
                           "FinKPIs": dict(FinKPIs).copy()}})
        self.ReportContents.update({"OverallSummary": dict(OvSum).copy()})
        # Get before after electricity bills
        Eprice.index = DemandTS.index
        EBillDemand = (DemandTS.mul(Eprice)).sum() / 100000  # kWh*CHF
        # Calculate Net reduction with time varying effect
        FinalTS.loc[FinalTS['Solar'].ge(FinalTS['Demand']), 'DirectUse'] = FinalTS.loc[
            FinalTS['Solar'].ge(FinalTS['Demand']), 'Demand']
        FinalTS.loc[FinalTS['Solar'].lt(FinalTS['Demand']), 'DirectUse'] = FinalTS.loc[
            FinalTS['Solar'].lt(FinalTS['Demand']), 'Solar']
        FinalTS.loc[FinalTS['DirectUse'] < 0, 'DirectUse'] = 0
        FinalTS.loc[FinalTS['Battery'] < 0, 'BatterytoUse'] = -FinalTS.loc[
            FinalTS['Battery'] < 0, 'Battery'] * Di_ef  # Battery is discharged for Use
        FinalTS['NettoUseWBatt'] = FinalTS['Demand'] - FinalTS['DirectUse'] - FinalTS['BatterytoUse']

        EBillSolar = (FinalTS['NettoUseWBatt'].mul(Eprice)).sum() / 100000  # kWh*CHF

        ## ================ SUSTAINABILITY ============================
        EnergyReduced = entity['FinalSummary']['OverallSummary']['ACEnergy']
        self.ReportContents.update({"EBillYearly": {"EBillSolar": EBillSolar, "EBillDemand": EBillDemand}})

        sustainability = {"CO2redkg": EnergyReduced * 0.2059,
                          "Autokm": EnergyReduced * 0.2059 / 0.235,
                          "CO2redpercent": (EnergyReduced * 0.2059 * 100) / 47300,
                          "trees": EnergyReduced * 0.2059 / 12}
        self.ReportContents.update({"sustainability": sustainability})

        ###### TEMPLATE SPECIFIC ITEMS ########### ###########
        ### Template Specific Report Contents and Plots ######
        # template_name_func = self.templatename.replace('-','_')
        template_name_func = self.templatename
        ListOfClasses = [obj for name, obj in inspect.getmembers(RTSG, inspect.isclass) if 'Template' in name]
        TemplateClass = [Obj for Obj in ListOfClasses if hasattr(Obj, template_name_func)][0] if len(
            [Obj for Obj in ListOfClasses if hasattr(Obj, template_name_func)]) > 0 else None
        if TemplateClass:
            TemContents = TemplateClass(self.ReportContents, entity, PVModuleData, InvData, bucket=self.bucket,
                                        lang=self.lang)
            getattr(TemplateClass, template_name_func)(TemContents)
        else:
            raise Exception(f'Template {template_name_func} not found')
        self.ReportContents.update(TemContents.ReportContents)
        self.options = self.ReportContents['options']

        ############## -------------------------#################

        # ------------ STATIC IMAGES ---------------------------
        # blobname = AUXFILES+self.templatename+"/staticimages.json"
        blobname = self.path2template + "/staticimages.json"
        staticimages = json.loads(self.bucket.get_blob(blobname).download_as_string())
        for dat in staticimages:
            self.ReportContents.update({dat: staticimages[dat]})
        ### Picture of location #####
        uniqueid = "UID_" + entity.key.parent.name + "_PID_" + entity.key.name + "_screenshot_"

        scrblobs = self.bucket.list_blobs(prefix=USER_UPLOADS + uniqueid)  # Get list of files
        PicList = [(b.updated, b.name) for b in scrblobs]

        if PicList:
            PicList.sort(key=lambda t: t[0], reverse=True)
            LatestPic = PicList[0][1]  # Name of latest pic
            blob = self.bucket.get_blob(LatestPic)
            Screenshot = base64.b64encode(blob.download_as_string())
            Screenshot = Screenshot.decode('utf8')
            self.ReportContents.update({"locscreenshot": Screenshot})
        else:
            self.ReportContents.update({"locscreenshot": ""})

    def rendertemplate(self, pdfname, pages, ReportCont=None, cwd=None):
        if not cwd:
            cwd = os.path.curdir

        try:
            Report = self.ReportContents
            self.localstoredfiles.append(os.path.abspath(os.path.join(cwd, pdfname)))
        except:
            Report = ReportCont
        output = self.template.render(Report, pages=pages)

        # with open(os.path.join(cwd,pdfname),'wb') as b:
        #     pisa.CreatePDF(output,b)
        if self.options == {}:
            options = {}
        else:
            options = self.options
        footer_location = os.path.abspath(os.path.join(self.abstemppath, 'footer.html'))
        # Add Footer
        options.update({"enable-local-file-access": None,
                        "--footer-html": footer_location,
                        "--replace": [(
                            'abspath', self.abstemppath
                        )]
                        })

        pdfkit.from_string(output, pdfname, options=options)

        return os.path.join(cwd, pdfname)

    def uploadtobucket(self, path2file, file2store):
        from google.cloud import storage
        solblobname = file2store
        solblob = storage.Blob(solblobname, self.bucket)
        solblob.upload_from_filename(path2file)

    def removetempfiles(self):
        for file in self.localstoredfiles:
            try:
                os.remove(file)
            except:
                pass


# %%
if __name__ == "__main__":
    # sys.path.append('..')
    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = os.path.join("/Users", "vipluv.aga", "Code", "auth",
                                                                "gcloudauthkeys", "solex-mvp-2-e18a10831f31.json")
    cwd = os.path.curdir
    thisdir = os.path.dirname(__file__)

    # templatefldr = os.path.join(thisdir,os.pardir,os.pardir,os.pardir,'pdf-report','template-pt',)
    # templatefldr = os.path.join(cwd,'bucketsim','devtemp')
    databases = os.path.join(thisdir, os.pardir, os.pardir, os.pardir, 'offline', 'databases')
    databases = '/Users/vipluv.aga/Code/solex-design/offline/databases'

    PVpick = "CECModules11-2021.pkl"
    PVSpick = "PvSystModules2022-01.pkl"
    Invpick = "BigInvCEC01-2022.pkl"
    with open(os.path.join(databases, PVpick), 'rb') as f:
        PVDF = pickle.load(f)
    with open(os.path.join(databases, PVSpick), 'rb') as f:
        PVSYSDF = pickle.load(f)

    with open(os.path.join(databases, Invpick), 'rb') as f:
        INVDF = pickle.load(f)

    ds = datastore.Client()
    gcs = storage.Client()
    bucket = gcs.bucket("solbucket")

    # email="test@bluesol.com.br"
    # pid="c77e8ea8-72b8-11ec-86ab-97d64c70bb41"

    orgname = 'bluesol'
    email = "test@bluesol.com.br"
    pid = "0bcab306-77a6-11ec-b9cc-c56ce57b2b60"
    parent_key = ds.key('users', email)
    entity = ds.get(key=ds.key('Project', pid, parent=parent_key))
    orgentity = ds.get(key=ds.key('organisation', orgname))
    dbname = orgentity['compdb']
    if dbname:
        os.environ['DB_NAME'] = dbname
    else:
        os.environ['DB_NAME'] = 'ref_' + orgname
    importlib.reload(CDB)

    templatekey = 'template-ag-mfh-fix-kurz'
    lang = 'DE'

    tempfuncfolder = {"template-ag-agsolar-fix-kurz": {"func": "template_ag_agsolar_fix_kurz",
                                                       "folder": "template-ag-agsolar-fix-kurz"},
                      "template-ag-efh-fix-kurz": {"func": "template_ag_efh_fix_kurz",
                                                   "folder": "template-ag-efh-fix-kurz"},
                      "template-ag-efh-richt-kurz": {"func": "template_ag_efh_richt_kurz",
                                                     "folder": "template-ag-efh-richt-kurz"},
                      "template-ag-mfh-fix-kurz": {"func": "template_ag_mfh_fix_kurz",
                                                   "folder": "template-ag-mfh-fix-kurz"},
                      "template-ag-mfh-richt-kurz": {"func": "template_ag_mfh_richt_kurz",
                                                     "folder": "template-ag-mfh-richt-kurz"},
                      "fr-template-ag-mfh-fix-kurz": {"func": "template_ag_mfh_fix_kurz",
                                                      "folder": "fr-template-ag-mfh-fix-kurz"},
                      "fr-template-ag-efh-fix-kurz": {"func": "template_ag_efh_fix_kurz",
                                                      "folder": "fr-template-ag-efh-fix-kurz"},
                      "fr-template-ag-agsolar-fix-kurz": {"func": "template_ag_agsolar_fix_kurz",
                                                          "folder": "fr-template-ag-agsolar-fix-kurz"},
                      "template-ag-mfh-fix-lease": {"func": "template_ag_mfh_fix_kurz",
                                                    "folder": "template-ag-mfh-fix-kurz"},

                      "template-agnew-en": {"func": "template_agnew_en", "folder": "template-agnew-en"},
                      "template-agnew-blue-pt": {"func": "template_agnew_blue_pt", "folder": "template-agnew-blue-pt"},
                      "template-pt-short": {"func": "template_agnew_blue_pt", "folder": "template-pt-short"},
                      "template-tech-report": {"func": "template_tech_report", "folder": "template-tech-report"},
                      "template-br-homolog": {"func": "template_br_homolog", "folder": "template-br-homolog"},

                      "template-gen-v1": {"func": "template_gen_v1", "folder": "template-gen-v1-<lang>"},
                      "template-gen-v2": {"func": "template_gen_v2", "folder": "template-gen-v2-<lang>"},
                      "template-gen-v3": {"func": "template_gen_v3", "folder": "template-gen-v3-<lang>"}
                      }
    templatefolder = tempfuncfolder.get(templatekey, tempfuncfolder['template-gen-v1']).get('folder', '').replace(
        '<lang>', lang.lower())
    templatefunc = tempfuncfolder.get(templatekey, 'template-gen-v1').get('func')

    pagetype = 'long'
    path2template = AUXFILES + templatefolder

    if lang == 'FR':
        templatefldr = '/Users/vipluv.aga/Code/solex-design/pdf-report/' + templatefolder
    else:
        templatefldr = '/Users/vipluv.aga/Code/solex-design/pdf-report/' + templatefolder
    abspathtotemplate = os.path.abspath(templatefldr)
    pdfname = os.path.join(templatefldr, os.pardir, "ReportMakerOrigin.pdf")
    templatesettingfile = '/Users/vipluv.aga/Code/solex-design/pdf-report/templatesettings.json'

    freetext = {"description": "Full Plant", "delivery": "Everything"}


    def multiline(multitext):
        # return [line.strip() for line in re.findall(r'^.*$',multitext,re.MULTILINE)]
        return [line.strip() for line in multitext.split('<br>')]


    func_dict = {"multiline": multiline}


    def numformatter_de(num2format, dp=0):
        try:
            if dp == 0:
                return str('{:,.0f}'.format(num2format)).replace(",", "&rsquo;").replace(".", ",")
            else:
                return str(str("{:,." + str(dp) + "f}").format(num2format)).replace(",", "&rsquo;").replace(".", ",")
        except:
            return str(num2format)


    func_dict.update({"numformatter_de": numformatter_de})

    file_loader = FileSystemLoader(templatefldr)
    env = Environment(loader=file_loader)
    template = env.get_template('base.html')
    template.globals.update(func_dict)
    figures = {}

    #    ReportDic.update(figures)
    if 'SelectedConfig_mu' in entity:
        PVName = entity['SelectedConfig_mu']['Config']['PVName']
        InvName = [n for n in entity['SelectedConfig_mu']['Config']['InvCombo'].keys()]
        InvData = {name: INVDF.loc[:, name].to_dict() for name in InvName}
    else:
        PVName = entity['SelectedConfig']['Config']['PVName']
        InvName = entity['SelectedConfig']['Config']['InvName']
        InvData = INVDF.loc[:, InvName]

    if PVName in PVDF:
        PVModuleData = PVDF.loc[:, PVName]
    elif PVName in PVSYSDF:
        PVModuleData = PVSYSDF.loc[:, PVName]
    else:
        PVModuleData = PVSYSDF.iloc[:, 300]

    with open(templatesettingfile, 'rb') as f:
        templatesettings = json.load(f)

    # pages = templatesettings[templatename]['pages']
    pages = ["page1", "page2", "page8", "page10"]
    # if (templatename == 'template-ag-corp') or (templatename == 'template-agnew-pt') :
    #   pages = ["page1","page2", "page3", "page4", "page8", "page10", "page11", "page12a"]
    # elif templatename == 'template-ag-pt':
    #     pages = ["energyuse", "sustainability", "bom", "page9", "Bestellformular"]+\
    #             ["term_and_condition1", "term_and_condition2", "term_and_condition3"]
    # elif templatename == 'template-ag':
    #     lang='DE'
    #     if pagetype=='short':
    #        pages = [ "energyuse", "sustainability", "bom", "page9", "Bestellformular"]+\
    #         ["term_and_condition1", "term_and_condition2", "term_and_condition3"]
    #     else:
    #        pages = [ "energyuse", "energyproduction","losschain", "costs", "sustainability", "bom", "page9","Bestellformular"]+\
    #         ["term_and_condition1", "term_and_condition2", "term_and_condition3"]

    if lang == 'FR':
        # templatename = templatename.split('fr-')[-1] #'TEMP FIX
        R = Report(bucket, abspathtotemplate, templatename=templatefunc.split('fr-')[-1], lang=lang,
                   path2template=path2template)
    else:
        R = Report(bucket, abspathtotemplate, templatename=templatefunc, lang=lang, path2template=path2template)
    R.template = template
    R.GenReportVars(entity, PVModuleData, InvData, orgentity)
    # R.ReportContents.update({"freetext":freetext})
    # tandc1 = entity['ReportContentsStored'].get('tandc1','')
    # R.ReportContents.update({"tandc1":tandc1})
    # paymentoptions = {"EFH":[{"percentage":50, "description": "Bei Auftragsbestätigung"},
    #                          {"percentage":50,"description":"Bei Inbetriebnahme"}],
    #            "Kommerziell":[{"percentage":30, "description": "Bei Auftragsbestätigung"},
    #                         {"percentage":30, "description": "Bei Materiallieferung"},
    #                         {"percentage":30, "description": "Bei Inbetriebnahme"},
    #                         {"percentage":10, "description": "nach Übergabe"}],
    #             "AgroSolar/Vorfinanzierung": [{"percentage":40, "description": "Bei Auftragsbestätigung"},
    #                         {"percentage":40, "description": "Bei Materiallieferung"},
    #                         {"percentage":10, "description": "Bei Inbetriebnahme"},
    #                         {"percentage":10, "description": "nach Übergabe"}],
    #                "Maison individuelle": [{"percentage":50, "description": "avec confirmation de commande"},
    #                          {"percentage":50,"description":"après la mise en service"}],
    #                 "commercial":  [{"percentage":30, "description": "avec confirmation de commande"},
    #                         {"percentage":30, "description": "pour la livraison de matériel"},
    #                         {"percentage":30, "description": "après la mise en service"},
    #                         {"percentage":10, "description": "après la livraison"}],
    #                 "AgroSolar/Préfinancement" :  [{"percentage":40, "description":"avec confirmation de commande"},
    #                         {"percentage":40, "description": "pour la livraison de matériel"},
    #                         {"percentage":10, "description": "après la mise en service"},
    #                         {"percentage":10, "description": "après la livraison"}]
    #                         }

    # R.ReportContents.update({"paymentschedule":paymentoptions['Maison individuelle']} ) #paymentoptions['EFH']})
    # tandc1 = {"DE":""" Freie Zufahrt zur Baustelle
    #          Montage des Steuerkabels (falls vorgegeben vom lokalen Netzbetreiber)
    #         Standort für technische Geräte wie Wechselrichter und Kommunikation
    #         Zuleitung Strom- und Netzwerkanschluss für Datenlogger
    #         Mitbenutzung internes Netzwerk für Kommunikation unter den Wechselrichtern oder ggf. SIM Karte / Abo für Router
    #         Anforderungen der VKF-Brandschutzbestimmungen
    #         Baugesuch inkl. Gebühren falls notwendig
    #         Dachdeckerarbeiten bis Solarlattung (bei Indachanlagen)
    #         Spenglerarbeiten um das Modulefeld (Übergang zur Dacheindeckung)
    #         Prüfung der Objektstatik""",
    #         "FR": ""}
    # firstpage_paragraphs =""
    # R.ReportContents.update({"firstpage_paragraphs":firstpage_paragraphs})
    # R.ReportContents.update({"tandc1":tandc1.get("DE","")})
    # R.rendertemplate(pdfname,pages)

    PickleFile = templatefolder.replace('template', 'ReportContents')
    with open(os.path.join(templatefldr, os.pardir, PickleFile + '.pkl'), 'wb') as f:
        pickle.dump(R.ReportContents, f)
    R.removetempfiles()
    print(PickleFile)
    plt.close('all')

# %%

# iofig=makeweatherbarplot(R.ReportContents['Weather']['monthlysummary'])
