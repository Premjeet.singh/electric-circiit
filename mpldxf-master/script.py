# importing the JSON, matplotlib,
# sys to exit the script and the SchemDraw_PV and elements for the drawing

import json  # for writing, saving and opening JSON files
import matplotlib
import sys
import subprocess  # open pdf after plot
import SchemDraw_PV as schem  # import SchemDraw_PV as schem
import SchemDraw_PV.elements_PV as e# import the elements
from mpldxf import FigureCanvasDxf
matplotlib.backend_bases.register_backend('dxf', FigureCanvasDxf)
# from matplotlib.backends.backend_pgf import FigureCanvasDxf
# matplotlib.backend_bases.register_backend('dxf', FigureCanvasDxf)


# matplotlib.use('Agg')  # Set the backend here, to save as PDF


# Mp = int(input("no of parallels"))
# Ms = int(input("module in series"))
# NumBB = int(input("no of building blocks"))
# Bat = int(input("no of  battery"))
# MPPTin = int(input("no of inputs on inverter"))
# PVName = input("PVName")
# PVVolt = float(input("voltage per module"))
# PVAmp = float(input("current per module"))
# BatName = input("Enter battery name")
# InvName = input("Enter inverter name")
# InvEff = input("Inverter efficency")
# Cable = input("Enter cable name")
# ProjName = input("Enter project name")

Out = {'Mp':5,
       'Ms': 25,
       'NumBB': 5,
       'Bat': 1,
       'MPPTin': 1,
       'BatName': 'battery',
       'InvName': 'InvName',
       'InvEff': 0.67,
       'PVName': 'PVName',
       'ProjName':'ProjName',
       'PVVolt': 39,
       'PVAmp': 9,
       'Cable': 'Cable'
       }

with open('input.json', 'w') as json_input_file:
    json.dump(Out, json_input_file)

# -----------------------------------------------------------------------------

# opening the JSON file to be able to read its content

str_data = open("input.json").read()
json_data = json.loads(str_data)

# -----------------------------------------------------------------------------

# reading the values in the JSON file
# and connecting them to the correct variable

Mp = json_data['Mp']
Mp = int(Mp)

Ms = json_data['Ms']
Ms = int(Ms)

NumBB = json_data['NumBB']
NumBB = int(NumBB)

Bat = json_data['Bat']
Bat = int(Bat)

MPPTin = json_data['MPPTin']
MPPTin = int(MPPTin)

PVName = json_data['PVName']

PVVolt = json_data['PVVolt']
PVVolt = float(PVVolt)

PVAmp = json_data['PVAmp']
PVAmp = float(PVAmp)

BatName = json_data['BatName']

InvName = json_data['InvName']

InvEff = json_data['InvEff']
InvEff = float(InvEff)

Cable = json_data['Cable']

ProjName = json_data['ProjName']

# -----------------------------------------------------------------------------

# defining parameter needed in loops

ir = 0
ip = 0
iNb = 0

# Calculating the maximum Voltage and current at different positions

VL1 = round(Ms * PVVolt)  # Labeling Voltage After BB
VL2 = round(VL1)  # Labeling Voltage After CB
VL3 = 230  # Labeling Voltage After INV

AL1 = round(PVAmp)  # Labeling the current of one string
AL2 = round(Mp * PVAmp)  # Labeling the added up current of one BB
AL3 = round(AL2 * VL2 * InvEff / VL3)  # Labeling the current of all BB

# -----------------------------------------------------------------------------

# One way to change the scaling:
l1 = 2  # size of elements and lines;

# -----------------------------------------------------------------------------
# To avoid inputs, which do not make sense and do not work within he program

if Mp <= 0:
    sys.exit("The variable Mp has an unexpected value (0 or below)!")
elif Ms <= 0:
    sys.exit("The variable Ms has an unexpected value (0 or below)!")
elif NumBB <= 0:
    sys.exit("The variable NumBB has an unexpected value (0 or below)!")
elif MPPTin <= 0:
    sys.exit("The variable MPPTin has an unexpected value (0 or below)!")
elif Bat > 2:
    sys.exit("The variable Bat has an unexpected value (NOT 0, 1, 2)!")
elif Bat < 0:
    sys.exit("The variable Bat has an unexpected value (NOT 0, 1, 2)!")
else:
    pass

# -----------------------------------------------------------------------------

# starting the drawing with d = schem.Drawing()

d = schem.Drawing()
d.push()  # fixes the drawing point [0 / 0]. Return with d.pop() below.

k = 0

if Mp == 1:
    k = k + 3
elif Mp == 2:
    k = k + 3
else:
    k = k + 4
if NumBB == 1:
    k = k * 1
elif NumBB == 2:
    k = (k + 1) * 2
else:
    k = (k + 1) * 2

d.add(e.SOURCE, d='down', xy=[k * l1 + 1, 0],
      botlabel='PV module\n' + str(PVName) +
               '\n' + str(PVVolt) + 'V')
d.add(e.CB, d='down', xy=[k * l1 + 1, -2.5],
      botlabel='Combiner Box')
d.add(e.INVDCAC, xy=[k * l1 + 1, -5],
      botlabel='Inverter DCAC type:\n' + str(InvName))
d.add(e.INVACDC, xy=[k * l1 + 1, -7.5],
      botlabel='Inverter ACDC type:\n' + str(InvName))
d.add(e.CONVDCDC, xy=[k * l1 + 1, -10],
      botlabel='DCDC Converter type:\n' + str(InvName))
d.add(e.BATTERY, xy=[k * l1 + 1, -12.5],
      botlabel='Battery type:\n' + str(BatName))
d.add(e.LINE, d='right', xy=[k * l1 - 0.5, -16.5], l=l1,
      rgtlabel='Cable type:\n' + str(Cable))

d.pop()  # returns the drawing cursor to the last d.push() --> [0 / 0]

# -----------------------------------------------------------------------------

# Writing the title above the drawing

d.push()
d.add(e.LABEL, xy=[0, 1], titlabel='Electrical scheme  ' + str(ProjName))
d.pop()


def bb():  # function
    global ir, iNb

    # adding the PV modules:
    if Mp == 1:
        d.add(e.LINE, d='right', l=l1)
        sir = ir + 1
        if Ms > 2:
            d.add(e.PV, d='down', label='PV' + str(sir) + '-' + '1')
            d.add(e.PH)
            d.add(e.PV, label='PV' + str(sir) + '-' + str(Ms))
        elif Ms == 1:
            d.add(e.PV, d='down', label='PV' + str(sir) + '-1')
        else:
            d.add(e.PV, d='down', label='PV' + str(sir) + '-1')
            d.add(e.PV, d='down', label='PV' + str(sir) + '-2')

        ENDPV = d.add(e.DOT)
        pass
    elif Mp == 2:
        while ir < Mp:
            d.add(e.LINE, d='right', l=l1)
            d.push()
            sir = ir + 1
            if Ms > 2:
                d.add(e.PV, d='down', label='PV' + str(sir) + '-' + '1')
                d.add(e.PH)
                d.add(e.PV, label='PV' + str(sir) + '-' + str(Ms))
            elif Ms == 1:
                d.add(e.PV, d='down', label='PV' + str(sir) + '-1')
            else:
                d.add(e.PV, d='down', label='PV' + str(sir) + '-1')
                d.add(e.PV, d='down', label='PV' + str(sir) + '-2')
            if Mp == 1:
                ENDPV = d.add(e.DOT)
                break
            elif ir == 0:
                ENDPV = d.add(e.LINE, d='right', l=l1)
            elif ir + 1 == Mp:
                break
            else:
                d.add(e.LINE, d='right', l=l1)
            d.pop()
            ir = ir + 1
    else:
        d.add(e.LINE, d='right', l=l1)
        d.push()
        sir = 1
        if Ms > 2:
            d.add(e.PV, d='down', label='PV' + str(sir) + '-' + '1')
            d.add(e.PH)
            d.add(e.PV, label='PV' + str(sir) + '-' + str(Ms))
        elif Ms == 1:
            d.add(e.PV, d='down', label='PV' + str(sir) + '-1')
        else:
            # d.add(e.PV, d='down', label='PV' + str(sir) + '-1')
            d.add(e.PV, d='down', label='PV' + str(sir) + '-2')
        ENDPV = d.add(e.LINE, d='right')
        d.add(e.SEP)
        d.pop()
        d.add(e.LINE)
        d.add(e.SEP)
        sir = Mp
        if Ms > 2:
            d.add(e.PV, d='down', label='PV' + str(sir) + '-' + '1')
            d.add(e.PH)
            d.add(e.PV, label='PV' + str(sir) + '-' + str(Ms))
        elif Ms == 1:
            d.add(e.PV, d='down', label='PV' + str(sir) + '-1')
        else:
            d.add(e.PV, d='down', label='PV' + str(sir) + '-1')
            d.add(e.PV, d='down', label='PV' + str(sir) + '-2')
    ir = 0
    siNb = iNb + 1



    # adding the Combiner Box, the Inverters and the Batteries:
    if MPPTin < Mp:  # if Inv inputs < than number of parallels --> add CB
        if Bat == 2:  # if battery per Building Block do this
            CBout = 1
            d.add(e.CB, d='down', xy=ENDPV.start,
                  smlltlabel='In ' + str(VL1) + ' V / ' + str(AL1) + ' A',
                  smlrtlabel='Out ' + str(VL2) + ' V / ' + str(AL2) + ' A',
                  toplabel='Combiner Box ' + str(siNb)
                           + '\nInputs: ' + str(Mp)
                           + '\nOutputs: ' + str(CBout))
            d.push()
            d.add(e.LINE, d='left', l=l1)
            d.add(e.CONVDCDC, d='down',
                  smllblabel='In ' + str(VL1) + ' V / ' + str(AL2) + ' A',
                  smlrblabel='Out ' + str(VL3) + ' V / ' + str(AL3) + ' A',
                  toplabel='DCDC Converter')
            d.add(e.BATTERY, d='down', toplabel='Battery ' + str(siNb))
            d.pop()
            d.add(e.LINE, d='down', l=3 * l1)

        else:
            CBout = MPPTin
            d.add(e.CB, d='down', xy=ENDPV.start,
                  smlltlabel='In ' + str(VL1) + ' V / ' + str(AL1) + ' A',
                  smlrtlabel='Out ' + str(VL2) + ' V / ' + str(AL2) + ' A',
                  toplabel='Combiner Box ' + str(siNb)
                           + '\nInputs: ' + str(Mp)
                           + '\nOutputs: ' + str(CBout))
        d.add(e.INVDCAC, d='down',
              smlltlabel='In ' + str(VL2) + ' V / ' + str(AL2) + ' A',
              smlrtlabel='Out ' + str(VL3) + ' V / ' + str(AL3) + ' A',
              toplabel='Inverter Nr ' + str(siNb) +
                       '\nMax Inputs: ' + str(MPPTin)
                       + '\nUsed Inputs: ' + str(CBout))

    else:
        if Bat == 2:  # if battery per BB do this
            CBout = 1
            d.add(e.CB, d='down', xy=ENDPV.start,
                  smlltlabel='In ' + str(VL1) + ' V / ' + str(AL1) + ' A',
                  smlrtlabel='Out ' + str(VL2) + ' V / ' + str(AL2) + ' A',
                  toplabel='Combiner Box ' + str(siNb)
                           + '\nInputs: ' + str(Mp)
                           + '\nOutputs: ' + str(CBout))
            d.push()
            d.add(e.LINE, d='left', l=l1)
            d.add(e.CONVDCDC, d='down',
                  smllblabel='In ' + str(VL1) + ' V / ' + str(AL2) + ' A',
                  smlrblabel='Out ' + str(VL3) + ' V / ' + str(AL3) + ' A',
                  toplabel='DCDC Converter')
            d.add(e.BATTERY, d='down', toplabel='Battery ' + str(siNb))
            d.pop()
            d.add(e.LINE, d='down', l=3 * l1)
            d.add(e.INVDCAC,
                  smlltlabel='In ' + str(VL1) + ' V / ' + str(AL2) + ' A',
                  smlrtlabel='Out ' + str(VL3) + ' V / ' + str(AL3) + ' A',
                  toplabel='Inverter Nr ' + str(siNb) +
                           '\nMax Inputs: ' + str(MPPTin)
                           + '\nUsed Inputs: ' + str(1))

        else:  # if NO battery  do this
            d.add(e.INVDCAC, d='down', xy=ENDPV.start,
                  smlltlabel='In ' + str(VL1) + ' V / ' + str(AL1) + ' A',
                  smlrtlabel='Out ' + str(VL3) + ' V / ' + str(AL3) + ' A',
                  toplabel='Inverter Nr ' + str(siNb)
                           + '\nMax Inputs: ' + str(MPPTin)
                           + '\nUsed Inputs: ' + str(Mp))
    iNb = iNb + 1


# ----------------------------------------------------------------------------

# Placement of the BB label on y-axis
BBLabelY = 0
if Ms == 1:
    BBLabelY = -2
elif Ms == 2:
    BBLabelY = -2.4
else:
    BBLabelY = -3.2

# Placement of the BB label on x-axis
BBLabelX = -0.5


# Calling the main function bb(), to create the Building Blocks:

if NumBB == 1:
    ENDBB = d.add(e.GND, botlabel='GND', l=l1)
    d.add(e.DOT)
    d.add(e.LABEL, xy=ENDBB.start + [BBLabelX, BBLabelY], titlabel='BB  ' + str(1))
    bb()
    BB1DOT = d.add(e.DOT)

elif NumBB == 2:
    ENDBB = d.add(e.GND, botlabel='GND', l=l1)
    d.add(e.DOT)
    d.add(e.LABEL, xy=ENDBB.start + [BBLabelX, BBLabelY], titlabel='BB  ' + str(1))
    bb()
    BB1DOT = d.add(e.DOT)
    if Mp == 1:
        ENDBB = d.add(e.GND, xy=ENDBB.start + [l1 * 4, 0],
                      botlabel='GND', l=l1)
        d.add(e.DOT, xy=ENDBB.start)
        d.add(e.LABEL, xy=ENDBB.start + [BBLabelX, BBLabelY], titlabel='BB  ' + str(2))
        bb()
        d.add(e.LINE, to=BB1DOT.start, l=l1)
    else:
        ENDBB = d.add(e.GND, xy=ENDBB.start + [l1 * 5, 0],
                      botlabel='GND', l=l1)
        d.add(e.DOT, xy=ENDBB.start)
        d.add(e.LABEL, xy=ENDBB.start + [BBLabelX, BBLabelY], titlabel='BB  ' + str(2))
        bb()
        d.add(e.LINE, to=BB1DOT.start, l=l1)

else:
    ENDBB = d.add(e.GND, botlabel='GND', l=l1)
    d.add(e.DOT)
    d.add(e.LABEL, xy=ENDBB.start + [BBLabelX, BBLabelY], titlabel='BB  ' + str(1))
    bb()
    BB1DOT = d.add(e.DOT)
    if Mp == 1:
        ENDBB = d.add(e.GND, xy=ENDBB.start + [l1 * 4, 0],
                      botlabel='GND', l=l1)
        d.add(e.DOT, xy=ENDBB.start)
        d.add(e.LABEL, xy=ENDBB.start + [BBLabelX, BBLabelY],
              titlabel='BB  ' + str(NumBB))
        bb()
        d.add(e.SEP, to=BB1DOT.start, l=l1)
    else:
        ENDBB = d.add(e.GND, xy=ENDBB.start + [l1 * 5, 0],
                      botlabel='GND', l=l1)
        d.add(e.DOT, xy=ENDBB.start)
        d.add(e.LABEL, xy=ENDBB.start + [BBLabelX, BBLabelY],
              titlabel='BB  ' + str(NumBB))
        bb()
        d.add(e.SEP, to=BB1DOT.start, l=l1)

# # -----------------------------------------------------------------------------
#
# # The Combiner Box after the Building Blocks
#
if NumBB > 1:
    d.add(e.CB, d='down',
          smlltlabel='In ' + str(VL3) + ' V / ' + str(AL3) + ' A',
          smlrtlabel='Out ' + str(VL3) + ' V / ' + str(AL3 * NumBB) + ' A',
          toplabel='Combiner Box All BB'
                   + '\nInputs: ' + str(NumBB)
                   + '\nOutputs: 1')

# -----------------------------------------------------------------------------

# If the system uses only one Battery, it gets added here:

if Bat == 1:
    d.push()
    d.add(e.LINE, d='right', l=3)
    d.add(e.INVACDC, d='right',
          smllblabel='In ' + str(VL3) + ' V / ' + str(AL3) + ' A',
          smlrblabel='Out ' + str(VL3) + ' V / ' + str(AL3) + ' A',
          toplabel='Inverter transformer')
    d.add(e.BATTERY, d='right', toplabel='Battery All BB')
    d.pop()
    d.add(e.LINE, d='down', l=2 * l1)

# -----------------------------------------------------------------------------

# The connection to the electrical grid:

Grid = d.add(e.GRID, d='down', toplabel='Grid connection')

# -----------------------------------------------------------------------------

# Finishing the drawing, saving it to a pdf and automatically open it.

d.draw()
# import pdb
# pdb.set_trace();
d.fig.savefig('schematic.dxf',
              bbox_extra_artists=d.ax.get_default_bbox_extra_artists(),
              bbox_inches='tight', pad_inches=0.15, dpi=360)

subprocess.Popen(["schematic.dxf"], shell=True)
