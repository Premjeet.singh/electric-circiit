# !/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 31 23:38:49 2018

@author: vaga
"""

import json, re
import logging
import os
import datetime as dt
import numpy as np
import pandas as pd
import uuid
import pickle
import math
import importlib
import base64

from middlewareauth import jwt_authenticated
from flask import Flask, request, jsonify
from google.cloud import storage, datastore

import time
import copy

if __name__ == '__main__':
    Envvar = {"Environ":
                  {"BUCKET_NAME": "solbucket",
                   "PV_DB_FILENAME": "CECModules11-2021.pkl",
                   "PVS_DB_FILENAME": "PvSystModules2021-11.pkl",
                   "BATT_DB_FILENAME": "BatteryModel-08-2021.pkl",
                   "INV_DB_FILENAME": "BigInvCEC08-2021.pkl",
                   "NASA_22YR_FILENAME": "22yr_nasa_monthly.pkl",
                   "MANUAL_PV_DB_FILENAME": "ManualPVModules2020-11.pkl",
                   "USER_UPLOADS_FOLDER": "UserUploads/",
                   "FETCHED_WEATHER_FOLDER": "FetchedWeather/",
                   "AUXFILES_FOLDER": "AuxFiles/",
                   "RESULTS_FOLDER": "Results/",
                   "BOM_FOLDER": "BoM/",
                   "ELECPARAMS": "elec_design_params.json",
                   "FINPARAMS": "finparameter_inputs.json",
                   "SOLARCALCLEADS": "CalcLeads/",
                   "GOOGLE_CLOUD_PROJECT": "solex-mvp-2",
                   "SQLALCHEMY_DATABASE_URI": "mysql+pymysql://root:default123@/refdata?unix_socket=/cloudsql/solex-mvp-2:europe-west6:component-cost",
                   "DB_NAME": "ref_agrola"
                   }}
    for var in Envvar['Environ']:
        os.environ[var] = Envvar['Environ'][var]

# import pdb;
# pdb.set_trace()
# from addonModules import weatherGet as WG
from addonModules import ComponentDB as CDB
# from addonModules import SaveJsonOutputs as sjson
from addonModules import PVSYS_el_class as PVsel
from addonModules import PvSystemData_Calculation as PVcalc
from addonModules import Opti_Module as Opt
from addonModules import Finance_model as fm
# import addonModules.GeoCoordtoMetersCy as Geo2M # ensure cython extension
import addonModules.GeoCoordtoMeters as Geo2M
# import addonModules.GridMakerCy as GridM
import addonModules.GridMaker as GridM
from pvlib.location import Location

import Levenshtein
from addonModules import VizLinkMaker as VzLink

# import addonModules
import addonModules.ReportMaker as RepM
import addonModules.MakeSDparam as SD
from webcalc.webcalcMods import Supplier as Sup
from webcalc.webcalcMods import Storage as ST

from shapely.geometry import polygon, Polygon, MultiPolygon, LinearRing
from shapely.affinity import translate

import sentry_sdk
from sentry_sdk.integrations.flask import FlaskIntegration
from sentry_sdk.integrations.sqlalchemy import SqlalchemyIntegration

sentry_sdk.init(
    dsn="https://508d4d25eaa447c38445f1f0bd8ac634@o434693.ingest.sentry.io/5392060",
    integrations=[FlaskIntegration(), SqlalchemyIntegration()],
    traces_sample_rate=1.0
)

# from opencensus.common.transports.async_ import AsyncTransport
# from opencensus.ext.stackdriver import trace_exporter as stackdriver_exporter
# import opencensus.trace.tracer
# def initialize_tracer(project_id):
#     exporter = stackdriver_exporter.StackdriverExporter(
#         project_id=project_id, transport = AsyncTransport
#     )
#     tracer = opencensus.trace.tracer.Tracer(
#         exporter=exporter,
#         sampler=opencensus.trace.tracer.samplers.AlwaysOnSampler()
#     )

#     return tracer

BUCKET_NAME = os.environ['BUCKET_NAME']
PV_DB = os.environ['PV_DB_FILENAME']
PVS_DB = os.environ['PVS_DB_FILENAME']
INV_DB = os.environ['INV_DB_FILENAME']
BATT_DB = os.environ['BATT_DB_FILENAME']
ELECPARAMS = os.environ['ELECPARAMS']
FINPARAMS = os.environ['FINPARAMS']
ALLOWED_EXTENSIONS = {'txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif', 'csv'}
BOM = os.environ['BOM_FOLDER']
USER_UPLOADS = os.environ['USER_UPLOADS_FOLDER']
FETCHED_WEATHER = os.environ['FETCHED_WEATHER_FOLDER']
AUXFILES = os.environ['AUXFILES_FOLDER']
RESULTS = os.environ['RESULTS_FOLDER']
MANUALPV_DB = os.environ['MANUAL_PV_DB_FILENAME']
DBNAME = os.environ['DB_NAME']

# os.environ['DB_NAME'] = 'refdata'

PVDB = None

# from webcalc import webcalc
# from multiopt import multiopt

app = Flask(__name__)


# app.register_blueprint(webcalc.webcalc,url_prefix='/calc')
#
# app.register_blueprint(multiopt.multiopt,url_prefix='/multiop')


# @app.before_first_request
# def _load_db():
#     global PVDF, INVDF, ELPARM, FINPARM,PVSYSDF,BATTDF
# #    Electrical parameters
#     client = storage.Client()
#     bucket = client.get_bucket(BUCKET_NAME)
#     blob = bucket.get_blob(AUXFILES+ELECPARAMS)
#     e = blob.download_as_string()
#     ELPARM = json.loads(e)
#
# #   Financial parameters
#     blob = bucket.get_blob(AUXFILES+FINPARAMS)
#     f = blob.download_as_string()
#     FINPARM = json.loads(f)
#
#
# #    Full CEC Inverter Dataframe
#     blob = bucket.get_blob(INV_DB)
#     inv = blob.download_as_string()
#     INVDF = pickle.loads(inv)
#     INVDF=INVDF.loc[:,~INVDF.columns.duplicated()]
#
# #   Full CEC PV Dataframe
#     blob = bucket.get_blob(PV_DB)
#     p = blob.download_as_string()
#     PVDF = pickle.loads(p)
#     #PVDF.loc['Wp'] = PVDF.loc['I_mp_ref']*PVDF.loc['V_mp_ref']
#     #PVDF=PVDF.loc[:,~PVDF.columns.duplicated()]
#     # UPLOAD THE MANUAL DESOTO MODELS CREATED
#     # MANUALPVDF = pickle.loads(bucket.get_blob(MANUALPV_DB).download_as_string())
#     # PVDF=PVDF.join(MANUALPVDF,how='left')
#     PVDF = PVDF[~PVDF.index.duplicated()]
#     PVDF=PVDF.loc[:,~PVDF.columns.duplicated()]
#
#
#
#     blob = bucket.get_blob(PVS_DB)
#     pvs = blob.download_as_string()
#     PVSYSDF = pickle.loads(pvs)
#     #PVSYSDF.loc['Wp'] = PVSYSDF.loc['I_mp_ref']*PVSYSDF.loc['V_mp_ref']
#     PVSYSDF=PVSYSDF.loc[:,~PVSYSDF.columns.duplicated()]
#     ## Battery Load
#
#     BATTDF = pickle.loads(bucket.get_blob(BATT_DB).download_as_string())
#
#     #SQLALCHEMY_DATABASE_URI="mysql+pymysql://root:default123@/refdata?unix_socket=/tmp/cloudsql/solex-mvp-2:europe-west6:component-cost"
#
#     #tracer = initialize_tracer(os.environ['GOOGLE_CLOUD_PROJECT'])


@app.after_request
def add_corsheader(response):
    whitelistpatterns = [re.compile(".*solextron.*\.herokuapp\.com\/?.*"),
                         re.compile(".*solex-mvp-2.*\.appspot\.com\/?.*"),
                         re.compile(".*localhost.*"),
                         re.compile(".*solextron\.com\/?.*"),
                         re.compile(".*\.run\.*app\/?.*")]

    weborigin = request.environ.get('HTTP_ORIGIN')
    referer = request.environ.get('HTTP_REFERER')

    if len([weborigin for Wlist in whitelistpatterns if Wlist.match(str(weborigin))]) > 0:
        response.headers["Access-Control-Allow-Origin"] = weborigin
    elif len([referer for Wlist in whitelistpatterns if Wlist.match(str(referer))]) > 0:
        response.headers["Access-Control-Allow-Origin"] = referer
    else:
        response.headers["Access-Control-Allow-Origin"] = "http://localhost:4200"
    response.headers["Access-Control-Allow-Credentials"] = "true"
    response.headers["Access-Control-Allow-Headers"] = "authorization, content-type"
    response.headers["Access-Control-Allow-Methods"] = "GET,POST,OPTIONS"

    return response


@app.route('/user-projects', methods=['POST'])
@jwt_authenticated
def userprojects():
    # import pdb
    # pdb.set_trace()
    ds = datastore.Client()
    gcs = storage.Client()
    bucket = gcs.get_bucket(BUCKET_NAME)
    message = {}
    record = {}
    print("ririee", request)

    req_json = request.get_json()['userdata']

    user_details = req_json  # json.loads(req_json)
    # user_ip = '.'.join(user_ip.split('.')[:2])
    #    query = ds.query(kind='users')
    #    query.add_filter('email','=',user_details['email'])
    user_details['email'] = request.email
    orgname = request.org
    # Checklist of userinputs  - check for the required ones
    uschklist = ['crudstate', 'email']
    for us in uschklist:
        if us not in user_details:
            return jsonify({"error": "{} not set in input request".format(us)}), 400

    crudstate = user_details['crudstate']  # create, update, delete 

    # Creating new projects
    entity = ds.get(key=ds.key('users', user_details['email']))
    parent_key = ds.key('users', user_details['email'])
    if not entity:
        # entity = datastore.Entity(key=ds.key('users',user_details['email']))
        # entity.update(record)
        # ds.put(entity)
        message.update({"error": f"No user with username {user_details['email']} exists"})
        return jsonify(message), 400

    if (crudstate == "create"):
        ProjId = str(uuid.uuid1())
        entity = datastore.Entity(key=ds.key('users', user_details['email'], 'Project', ProjId))

        message.update(
            {"Success": "User {} has created a new project with project id: {}".format(user_details['email'], ProjId),
             "project_id": ProjId})
        record.update({'created': dt.datetime.utcnow(), 'status': 'Project Overview'})
        if 'projectdetails' in user_details:
            if 'projecttype' not in user_details['projectdetails']:
                user_details['projectdetails'].update({'projecttype': 'default'})
            if 'projectaddress' not in user_details['projectdetails']:
                user_details['projectdetails'].update({'projectaddress': ' '})
            if 'owner' not in user_details['projectdetails']:
                owner = user_details['email']
                user_details['projectdetails'].update({'owner': 'default'})
            record.update({"projectdetails": user_details['projectdetails']})

        else:
            projectdetails = {"projectname": "New Project", "projecttype": "default", "owner": user_details['email']}
            record.update({"projectdetails": projectdetails})
        if 'projectaddress' in record['projectdetails']:
            if 'Location' not in entity:
                entity['Location'] = {}
                entity['Location']['map address'] = record['projectdetails']['projectaddress']
            else:
                entity['Location']['map address'] = record['projectdetails']['projectaddress']

        entity.update(record)
        ds.put(entity)

        return jsonify(message), 200

    elif (crudstate == 'update project'):
        ProjId = user_details['project_id']
        entity = ds.get(key=ds.key('Project', ProjId, parent=parent_key))
        if not entity:
            message.update({"error": "Project id {} not found".format(ProjId)})
            return jsonify(message), 400
        if 'projectdetails' in user_details:
            record.update({"projectdetails": user_details['projectdetails']})

        if "status" in user_details:
            record.update({"status": user_details['status']})

        if 'projectdetails' in record:
            if 'projectaddress' in record['projectdetails']:
                if 'Location' not in entity:
                    entity['Location'] = {}
                    entity['Location']['map address'] = record['projectdetails']['projectaddress']
                else:
                    entity['Location']['map address'] = record['projectdetails']['projectaddress']

        entity.update(record)
        ds.put(entity)
        message.update({"Success": 'Updated project details of {} for user {}'.format(ProjId, user_details['email'])})
        return jsonify(message), 200

    elif (crudstate == 'update user'):
        entity = ds.get(key=ds.key('users', user_details['email']))
        if not entity:
            message.update({"error": "User email {} not found".format(user_details['email'])})
            return jsonify(message), 400
        record.update({"userdetails": user_details['userdetails']})
        entity.update(record)
        ds.put(entity)
        message.update({"Success": 'Updated details for user {}'.format(user_details['email'])})
        return jsonify(message), 200
    elif (crudstate == 'addtoorg'):

        if orgname:
            orgentity = ds.get(key=ds.key('organisation', orgname))
            if not orgentity:
                orgentity = datastore.Entity(key=ds.key('organisation', orgname))
                orgentity.update({"users": [user_details['email']]})
            else:
                users = orgentity.get('users')
                users.append(user_details['email'])
        else:
            return jsonify({"error": "organame not found in request"}), 400
        ds.put(entity)
        ds.put(orgentity)
        return jsonify({"success": "Added user {} to org {}".format(user_details['email'], orgname)}), 200
    elif (crudstate == 'removefromorg'):

        if orgname:
            orgentity = ds.get(key=ds.key('organisation', orgname))
            if not orgentity:
                return jsonify({"error": "orgname {} not found".format(orgname)}), 400
            else:
                users = orgentity.get('users')
                users.remove(user_details['email'])
        else:
            return jsonify({"error": "orgname not found in request"}), 400
        ds.put(entity)
        ds.put(orgentity)
        return jsonify({"success": "removed user {} from org {}".format(user_details['email'], orgname)}), 200
    elif (crudstate == 'upsertorg'):
        # qur = ds.query(kind='organisation')
        # qur.add_filter('users','=',user_details['email'])
        # Sol=list(qur.fetch())
        # if Sol != []:   
        #     orgname =  Sol[0].key.name 
        # else:           
        #     return jsonify({"error":"user {} doesnt belong to any org"}.format(user_details['email']))
        orgentity = ds.get(key=ds.key('organisation', orgname))
        orgtoupdate = user_details.get('orgsettings', {})
        for key, val in orgtoupdate.items():
            if isinstance(orgentity.get(key), list):
                EntVal = orgentity.get(key)
                E = EntVal
                [E.append(x) for x in val if val not in EntVal]
                orgentity[key] = list(set(E))

            elif isinstance(orgentity.get(key), dict):  # Just catches two levels of nesting not more
                EntDict = orgentity.get(key)
                EntDict.update(
                    {k: ({vk: v for vk, v in v1.items()} if isinstance(v1, dict) else v1) for k, v1 in val.items()})
                orgentity.update({key: EntDict})

            else:
                orgentity.update({key: val})
        ds.put(orgentity)
        return jsonify({"success": "updated org {} by user {}".format(orgname, user_details['email'])}), 200
    elif crudstate == 'displaypermissions':
        orgentity = ds.get(key=ds.key('organisation', orgname))
        output = {}
        if 'displaypermissions' in orgentity:
            if user_details['email'] in orgentity['admins']:
                displaypermissions = orgentity['displaypermissions'].get('admins', {})
            elif user_details['email'] in orgentity['users']:
                displaypermissions = orgentity['displaypermissions'].get('users', {})
            else:
                displaypermissions = {}
            output.update({"displaypermissions": displaypermissions})
        return jsonify(output), 200

    elif (crudstate == 'read user'):
        if 'userdetails' not in entity:
            return jsonify({"error": "No user details stored for this user {}".format(user_details['email'])})
        # qur = ds.query(kind='organisation')
        # qur.add_filter('users','=',user_details['email'])
        # Sol=list(qur.fetch())
        # if Sol != []:

        #     orgname =  Sol[0].key.name 
        # else:        

        #     orgname = 'default'
        orgentity = ds.get(key=ds.key('organisation', orgname))
        outdic = {"userdetails": entity['userdetails']}
        if orgentity:
            outdic.update({"organisation": orgentity, "orgname": orgname})
        output = json.loads(json.dumps(outdic))  # full entity read as json
        return jsonify(output), 200

    elif (crudstate == 'read project'):
        ProjId = user_details['project_id']
        entity = ds.get(key=ds.key('Project', ProjId, parent=parent_key))
        if not entity:
            message.update({"error": "Project id {} not found".format(ProjId)})
            return jsonify(message), 400

        return jsonify(entity), 200

    elif (crudstate == 'delete'):
        entity = ds.get(key=ds.key('Project', user_details['project_id'], parent=parent_key))
        if not entity:
            message.update({"error": "Project ID {} not found for user {} ".format(user_details['project_id'],
                                                                                   user_details['email'])})

        ds.delete(key=ds.key('users', user_details['email'], 'Project', user_details['project_id']))
        # datastore deleted 
        # delete storage values
        uniqueid = "Uid_" + entity.key.parent.name + "_Pid_" + entity.key.name
        usrblobs = bucket.list_blobs(prefix=USER_UPLOADS + uniqueid)  # Get list of files in User_Uploads
        resblobs = bucket.list_blobs(prefix=RESULTS + uniqueid)
        UIDids = "UID_" + entity.key.parent.name + "_PID_" + entity.key.name
        layoutblobs = bucket.list_blobs(prefix=USER_UPLOADS + UIDids)
        try:
            bucket.delete_blobs(resblobs)
            bucket.delete_blobs(usrblobs)
            bucket.delete_blobs(layoutblobs)
        except:
            message.update({"error": "couldnt delete bucket contents"})
        message.update(
            {"Success": "deleted project {} from user {} ".format(user_details['project_id'], user_details['email'])})
        # ds.put(entity)

        return jsonify(message), 200


#

@app.route('/project-details', methods=['GET', 'POST'])
@jwt_authenticated
def readproject():
    ds = datastore.Client()

    req_json = request.get_json()['readProject']
    userinput = req_json
    #### AUTENTICATED EMAIL
    userinput['email'] = request.email
    orgname = request.org
    parent_key = ds.key('users', userinput['email'])
    # entity=ds.get(key=ds.key('Project',userinput['project_id'],parent=parent_key))
    userentity = ds.get(key=ds.key('users', userinput['email']))
    OutputDic = {}
    uschklist = ['email', 'attribute']
    for us in uschklist:
        if us not in userinput:
            return jsonify({"error": "{} not set in input request".format(us)}), 400
    if not userentity:
        return jsonify({"error": "Email -- {} -- not found".format(userinput['email'])}), 400

    def filternonjson(obj):
        if isinstance(obj, dict):
            obj = {k: filternonjson(v) for k, v in obj.items()}
        elif isinstance(obj, list):
            obj = [filternonjson(it) for it in obj]
        elif isinstance(obj, float):
            if (np.isnan(obj) or (obj == np.inf) or (obj == -np.inf)):
                obj = 0
        return obj

    with ds.transaction(read_only=True):
        att = userinput['attribute']
        searchvalue = userinput.get('searchvalue', '')
        if 'numrecords' in userinput:
            numr = userinput['numrecords']
        else:
            numr = 20

        query = ds.query(kind='Project', ancestor=parent_key)
        query.order = ['-created']
        Q = query.fetch(limit=int(numr))

        # TODO: Use pagination and cursors to get to next set of results
        # Improve this list
        # att : 'Location', 'Weather', 'Components', 'Cost' , 'Financial', 'OptMatrix','SelectedConfig',
        #     : 'Demand"  

        if att == 'projectnums':
            # TODO: 

            projList = []
            attribs = [{'Location': ['locationName', 'map address']},
                       {'projectdetails': ['projectname', 'projecttype', 'owner']},
                       {'Demand': ['ACpower']}, 'status', 'created']

            for s in Q:
                entry = {"id": str(s.key.name)}
                for att in attribs:
                    if type(att) != dict:
                        if att in s:
                            entry.update({att: s[att]})
                    elif type(att) == dict:
                        if list(att.keys())[0] in s:
                            mainatt = str(list(att.keys())[0])  # Main attribute and subattributes

                            # _ = [entry.update({subatt : s[mainatt][subatt]}) for subatt in list(att.values())[0] if subatt in list(s[mainatt].keys())]
                            _ = [
                                entry.update({subatt: s[mainatt][subatt]}) if (s[mainatt].get(subatt)) else {subatt: ''}
                                for subatt in list(att.values())[0]]
                entry = filternonjson(entry)
                projList.append(entry)

                # OutputDic.update({"data":projList})    
            output = jsonify({"message": "Project List", "data": projList})
        elif att == 'QuoteID':
            projList = []
            attribs = [{'ActiveQuote': ['QuoteID']}, {'projectdetails': ['projectname', 'owner']},
                       {"FinalSummary": ["OverallSummary"]}, 'status', 'created']
            for s in Q:
                if s.get('ActiveQuote'):
                    quotestored = s['ActiveQuote'].get('QuoteID', '')
                    if quotestored == searchvalue:
                        entry = {"id": str(s.key.name)}
                        for att in attribs:
                            if type(att) != dict:
                                if att in s:
                                    entry.update({att: s[att]})
                            elif type(att) == dict:
                                if list(att.keys())[0] in s:
                                    mainatt = str(list(att.keys())[0])  # Main attribute and subattributes 
                                    _ = [entry.update({subatt: s[mainatt][subatt]}) for subatt in list(att.values())[0]
                                         if subatt in list(s[mainatt].keys())]
                        entry = filternonjson(entry)
                        projList.append(entry)
            # OutputDic.update({"data":projList}) 
            output = jsonify({"message": "Project List", "data": projList})
        else:
            entity = ds.get(key=ds.key('Project', userinput['project_id'], parent=parent_key))
            if not entity:
                return jsonify({"error": "No record for project_id {}".format(userinput['project_id'])}), 400
            if att in entity:
                output = (jsonify(entity[att]))
                # OutputDic.update(dict(entity[att]))
            else:
                return jsonify(
                    {"error": " No record for {} in project_id {}".format(att, userinput['project_id'])}), 400

    return output, 200


@app.route('/demand', methods=['POST'])
@jwt_authenticated
def demandread():
    ds = datastore.Client()
    gcs = storage.Client()
    bucket = gcs.get_bucket(BUCKET_NAME)

    req_json = request.get_json()['Demand']
    userinput = req_json  # json.loads(req_json)      
    userinput['email'] = request.email
    orgname = request.org
    parent_key = ds.key('users', userinput['email'])
    entity = ds.get(key=ds.key('Project', userinput['project_id'], parent=parent_key))
    # uniqueid='Uid_'+userinput['id']+'_Pid_'+userinput['project_id']
    #    if entity is None:
    #        entity=datastore.Entity(key=ds.key('users',userinput['email'],'Project',userinput['project_id']))
    #     
    message = {}
    if not entity:
        # entity = datastore.Entity(key=ds.key('users',user_details['email']))
        # entity.update(record)
        # ds.put(entity)
        message.update({"error": "No user with username {} exists".format(userinput['email'])})
        return jsonify(message), 400

    # Get Customer Input Consumption per year

    uniqueid = 'Uid_' + userinput['email'] + '_Pid_' + userinput['project_id']
    ACPowerFromYearlyDemand = 0
    YearlyDemand = 0
    DailyProfile2Display = {}
    Profile = {}
    ACPowerStored = 0
    ACPowerFromArea = [0, 0]
    if 'Location' in entity:
        TotalPanelArea = entity['Location'].get('MaxPanelArea', entity['Location'].get('TotalPanelArea', 0))
        # if 'TotalPanelArea' in entity['Location']:
        #     TotalPanelArea = entity['Location']['TotalPanelArea']
        Maxm2perkW = 5.5  # ELPARM['systemparams']['Maxm2perkW']*1.3 
        Minm2perkW = 4.5  # ELPARM['systemparams']['Minm2perkW']*1.3
        ACPowerFromArea = [TotalPanelArea / Maxm2perkW, TotalPanelArea / Minm2perkW]
    else:
        ACPowerFromArea = [0, 0]

    if entity['status'] != 'Lock':
        entity.update({"status": "Demand"})
    if 'readstored' in userinput:
        if 'Location' in entity:
            TotalPanelArea = entity['Location'].get('MaxPanelArea', entity['Location'].get('TotalPanelArea', 0))
            # if 'TotalPanelArea' in entity['Location']:
            #     TotalPanelArea = entity['Location']['TotalPanelArea']
            Maxm2perkW = 5.5  # ELPARM['systemparams']['Maxm2perkW']
            Minm2perkW = 4.5  # ELPARM['systemparams']['Minm2perkW']
            ACPowerFromArea = [TotalPanelArea / Maxm2perkW, TotalPanelArea / Minm2perkW]
        else:
            ACPowerFromArea = [0, 0]
        if 'Demand' in entity:
            if 'ACpower' in entity['Demand']:
                ACPowerStored = entity['Demand']['ACpower']
            else:
                ACPowerStored = 0
            if 'YearlyDemand' in entity['Demand']:
                YearlyDemand = entity['Demand']['YearlyDemand']

            elif 'Customer Inputs' in entity:
                if 'YearlyElecRequired' in entity['Customer Inputs']:
                    YearlyDemand = entity['Customer Inputs']['YearlyElecRequired']['TotalElec']
                else:
                    YearlyDemand = 0
            else:
                YearlyDemand = 0
            if 'Profile' in entity['Demand']:
                Profile = entity['Demand']['Profile']
                country = Profile['country']
                profiletype = Profile['profiletype']

                if profiletype == 'UserDefined':
                    DemandTS = pickle.loads(bucket.get_blob(entity['Demand']['DemandTS']).download_as_string())
                    DailyProfile2Display = {}
                    monthnames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
                    YearlyDemand = DemandTS.sum() / 1000
                    for month, i in zip(monthnames, range(1, 13)):
                        DailyProfile2Display[month] = {
                            str(j): DemandTS[(DemandTS.index.month == i) & (DemandTS.index.hour == j)].mean() / 1000 for
                            j in range(0, 24)}
                else:
                    # Fetch the dataset for daily profile

                    blobname = AUXFILES + 'PriceAPICountry.json'
                    CI = json.loads(bucket.blob(blobname).download_as_string())
                    if country in CI['Data']:
                        blobnamePic = AUXFILES + CI['Data'][country]['PriceDb']
                    else:
                        blobnamePic = AUXFILES + CI['Data']['CHE']['PriceDb']
                    Data = pickle.loads(bucket.get_blob(blobnamePic).download_as_string())
                    if profiletype in Data['ConsumptionProfile']:
                        DemDailyDf_E = Data['ConsumptionProfile'][profiletype]
                    else:
                        firstkey = next(iter(Data['ConsumptionProfile']))
                        DemDailyDf_E = Data['ConsumptionProfile'][firstkey]
                    DemDailyDf_E = pd.DataFrame(DemDailyDf_E)

                    DailyProfile2Display = DemDailyDf_E[DemDailyDf_E.columns[0:]].to_dict()
            else:
                DailyProfile2Display = {}
                Profile = {}

        ACPowerFromYearlyDemand = np.ceil(YearlyDemand / 1200)  # Assume a kWh/kWp of 1200

        return jsonify({"ACPowerFromArea": ACPowerFromArea, "ACPowerStore": ACPowerStored,
                        "YearlyDemand": YearlyDemand, "ACPowerFromConsumption": ACPowerFromYearlyDemand,
                        "DailyProfile2Display": DailyProfile2Display, "Profile": Profile}), 200

    if 'ACPower' in userinput:
        demand = {"Demand": {'ACpower': float(userinput['ACPower'])}}
        if 'Demand' in entity:
            entity['Demand']['ACpower'] = float(userinput['ACPower'])
        else:
            entity.update(demand)

    if 'ACpower' in userinput:
        demand = {"Demand": {'ACpower': float(userinput['ACpower'])}}
        if 'Demand' in entity:
            entity['Demand']['ACpower'] = float(userinput['ACpower'])
        else:
            entity.update(demand)
        # ds.put(entity)
        # return jsonify({'success':'ACPower {} updated'.format(userinput['ACpower'])}),200 
    # try:    
    #    demand={"Demand":{'ACpower':float(userinput['ACpower'])}}#,
    #    #     'dailyvalues':userinput['dailyvalues']}
    #    if 'Demand' in entity:
    #        entity['Demand']['ACpower'] = float(userinput['ACpower'])
    #    else:                                           
    #        entity.update(demand)
    #    ds.put(entity)
    # except:
    #     return jsonify({"error": "No value for ACpower given"}),400

    # Upload Detailed DemandTS here
    ####

    ##### Daily Profile
    country = 'Default'
    profiletype = 'H4'
    if 'Profile' in userinput:
        if type(userinput['Profile']) == dict:
            country = userinput['Profile']['country']
            profiletype = userinput['Profile']['profiletype']
        else:
            country = 'CHE'
            profiletype = 'H4'
    else:
        country = 'Default'
        profiletype = 'H4'
    ###########
    Profile = {"country": country, "profiletype": profiletype}
    ### Yearly Demand
    if 'Customer Inputs' in entity:
        YearlyDemand = entity['Customer Inputs']['YearlyElecRequired']['TotalElec']

    ##
    if Profile['profiletype'] != 'UserDefined':  # if user defined then demand is already known   
        blobname = AUXFILES + 'PriceAPICountry.json'
        CI = json.loads(bucket.blob(blobname).download_as_string())
        if country in CI['Data']:
            blobnamePic = AUXFILES + CI['Data'][country]['PriceDb']
        else:
            blobnamePic = AUXFILES + CI['Data']['CHE']['PriceDb']
        Data = pickle.loads(bucket.get_blob(blobnamePic).download_as_string())

        if 'YearlyDemand' in userinput:
            YearlyDemand = userinput['YearlyDemand']
        else:
            YearlyDemand = 11000

            # Every consumption profile will be a different type of df
        if (country == 'CHE') or (country == 'Default'):

            if profiletype in Data['ConsumptionProfile']:
                DemDailyDf_E = Data['ConsumptionProfile'][profiletype]
                DemDailyDf_H = Data['ConsumptionProfile']['HELECTRO']
            else:
                DemDailyDf_E = Data['ConsumptionProfile']['H4']
                DemDailyDf_H = Data['ConsumptionProfile']['HELECTRO']
            DemDailyDf_E = pd.DataFrame(DemDailyDf_E)
            DemDailyDf_H = pd.DataFrame(DemDailyDf_H)

            if 'YearlyDemand' in userinput:
                YearlyDemand = userinput['YearlyDemand']
            elif 'monthlydemand' in userinput:
                monthlydemand = userinput['monthlydemand']
                YearlyDemand = sum(monthlydemand)
                monthlydemand = [(YearlyDemand / 12)] * 12
            elif profiletype in Data['Electrical']['Kategorie'].values:
                YearlyDemand = Data['Electrical'].loc[Data['Electrical']['Kategorie'] == profiletype, 'YearkWh'].values[
                    0].astype(float)
            else:
                YearlyDemand = 11000

            s = time.time()
            Demand = Sup.CHDemandGetter(DemDailyDf_E, DemDailyDf_H, YearlyDemand, 0)  # TotalHeat=0
            print(time.time() - s)
            DemandTS = Demand['Total'].astype('float')
            DemandTS.name = 'Demand'

            DailyProfile2Display = DemDailyDf_E[DemDailyDf_E.columns[0:]].to_dict()

        else:
            if profiletype in Data['ConsumptionProfile']:
                DailyProfile = pd.DataFrame(Data['ConsumptionProfile'][profiletype])

            else:
                firstkey = next(iter(Data['ConsumptionProfile']))
                DailyProfile = pd.DataFrame(Data['ConsumptionProfile'][firstkey])

            if 'monthlydemand' in userinput:
                monthlydemand = userinput['monthlydemand']
                YearlyDemand = sum(monthlydemand)
            elif 'YearlyDemand' in userinput:
                YearlyDemand = userinput['YearlyDemand']
                monthlydemand = [(YearlyDemand / 12)] * 12
            else:
                monthlydemand = [(YearlyDemand / 12)] * 12

            Demand = Sup.DemandfromMonthly(monthlydemand, DailyProfile)
            DemandTS = Demand['Demand'].astype('float')
            # DailyProfile2Display = DailyProfile[DailyProfile.columns[1:]].to_dict()
            DailyProfile2Display = DailyProfile[DailyProfile.columns[0:]].to_dict()
        Demandblobname = USER_UPLOADS + uniqueid + '_demandTS.pkl'
        bucket.blob(Demandblobname).upload_from_string(pickle.dumps(DemandTS))
    else:
        Demandblobname = entity['Demand']['DemandTS']
        DemandTS = pickle.loads(bucket.get_blob(entity['Demand']['DemandTS']).download_as_string())
        DailyProfile2Display = {}
        monthnames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        YearlyDemand = DemandTS.sum() / 1000
        for month, i in zip(monthnames, range(1, 13)):
            DailyProfile2Display[month] = {
                str(j): DemandTS[(DemandTS.index.month == i) & (DemandTS.index.hour == j)].mean() / 1000 for j in
                range(0, 24)}

    if 'fileupload' in userinput:
        fname = userinput['fileupload']

        try:
            bucket.get_blob(USER_UPLOADS + fname).download_to_filename(fname)
            if fname.endswith('.xlsx'):
                demandfromfileraw = pd.read_excel(fname, engine='openpyxl')
            elif fname.endswith('.csv'):
                demandfromfileraw = pd.read_csv(fname)
            else:
                demandfromfileraw = pd.read_csv(fname)

            if not (('kW' in demandfromfileraw.columns) or ('kWh' in demandfromfileraw.columns)):
                return jsonify({"error": "column containing load data should have label kW or kWh"}), 400
            # if 'kW' in demandfromfile.columns:  # convert to kWh 

            #     demandfromfile = (8760/len(demandfromfile))*demandfromfile
            #     demandfromfile.rename(columns={'kW':'kWh'},inplace=True)

            if 'time' in demandfromfileraw.columns:
                yearindex = pd.to_datetime(demandfromfileraw['time'], errors='coerce')
                kWconvert = ((yearindex[1] - yearindex[0]).seconds) / 3600  # fraction of an hour
                demandfromfileraw.set_index('time', inplace=True)

                if 'kW' in demandfromfileraw.columns:
                    demandfromfile = pd.DataFrame(demandfromfileraw.values * kWconvert, index=yearindex,
                                                  columns=['kWh'])
                else:
                    demandfromfile = pd.DataFrame(demandfromfileraw.values, index=yearindex,
                                                  columns=['kWh'])
                # Replicate if not enough data
                deltaTimeStep = ((demandfromfile.index[1] - demandfromfile.index[0]).seconds) / 3600  # Number of Hours
                timeres = int(((demandfromfile.index[1] - demandfromfile.index[0]).seconds) / 60)  # Number of minutes
                yearperiods = int(8760 / deltaTimeStep)
                Arr = np.array(demandfromfileraw.values)
                TOTARR = np.concatenate(
                    (np.tile(Arr, [int(yearperiods / len(Arr)), 1]), Arr[0:np.mod(yearperiods, len(Arr))]), axis=0)
                yearindex = pd.date_range(start='1/1/{}'.format(str(dt.datetime.now().year)), periods=yearperiods,
                                          freq=str(timeres) + 'T')
                demandfromfile = pd.DataFrame(TOTARR, index=yearindex, columns=['kWh'])

            else:  # use only the length of the file as a filter with 15 min steps
                if 'timeres' in userinput:
                    # 15, 60
                    timeres = int(userinput['timeres'])
                else:
                    timeres = 15
                Arr = np.array(pd.to_numeric(demandfromfileraw['kW'],
                                             errors='coerce').values) if 'kW' in demandfromfileraw else np.array(
                    pd.to_numeric(demandfromfileraw['kWh'], errors='coerce').values)
                # Arr = np.array(demandfromfileraw.values)
                if timeres == 60:
                    yearperiods = 8760
                else:
                    yearperiods = 8760 * 4

                # TOTARR = np.concatenate(np.ravel(np.tile(Arr,[int(yearperiods/len(Arr)),1])),np.ravel(Arr[0:np.mod(yearperiods,len(Arr))]),axis=0)
                TOTARR = np.hstack((np.ravel(np.tile(Arr, [int(yearperiods / len(Arr)), 1])),
                                    np.ravel(Arr[0:np.mod(yearperiods, len(Arr))])))
                yearindex = pd.date_range(start='1/1/{}'.format(str(dt.datetime.now().year)), periods=yearperiods,
                                          freq=str(timeres) + 'T')
                if 'kW' in demandfromfileraw.columns:
                    TOTARR = TOTARR * (8760 / yearperiods)
                demandfromfile = pd.DataFrame(TOTARR, index=yearindex, columns=['kWh'])

            os.remove(fname)
        except:
            return jsonify({"error": "file {} has invalid format or does not exist".format(USER_UPLOADS + fname)}), 400

        Profile = {"country": entity['projectdetails']['customerinfo']['country'], "profiletype": "UserDefined"}
        Demand, DailyProfile2Display = Sup.DemandFromFile(demandfromfile['kWh'])

        DemandTS = Demand['Demand'].astype('float')
        YearlyDemand = DemandTS.sum() / 1000
        Demandblobname = USER_UPLOADS + uniqueid + '_demandTS.pkl'
        bucket.blob(Demandblobname).upload_from_string(pickle.dumps(DemandTS))
        # DailyProfile2Display = {}
        # TODO : Return a profile

    # Generate  

    #######

    if 'Demand' in entity:
        entity['Demand']['DemandTS'] = Demandblobname
        entity['Demand']['YearlyDemand'] = YearlyDemand
    else:
        entity.update({"Demand": {"DemandTS": Demandblobname, "YearlyDemand": YearlyDemand}})
    if 'Profile' in entity['Demand']:
        entity['Demand']['Profile'].update(Profile)
    else:
        entity['Demand'].update({"Profile": Profile})
    if 'maxcurrent' in userinput:
        if 'maxcurrent' in entity['Demand']:
            entity['Demand']['maxcurrent'] = userinput['maxcurrent']
        else:
            entity['Demand'].update({"maxcurrent": userinput["maxcurrent"]})
    ds.put(entity)
    # Prepare Output

    MonthlyOutput = list(DemandTS.groupby([pd.Grouper(freq='1M')]).sum().values / 1000)  # Make List in kWh
    MonthlyPeak = list(DemandTS.groupby([pd.Grouper(freq='1M')]).max().values / 1000)  # Max List in kWh
    YearlyDemand = np.ceil(DemandTS.sum() / 1000)  # in kWh

    # Just to have adequate energy

    # RecommendedACPower = np.ceil(YearlyDemand/(8760*15/100)) # Assuming a 15% Capacity Factor
    RecommendedACPower = np.ceil(YearlyDemand / 1200)

    OutputDic = {"MonthlyOutput": MonthlyOutput, "MonthlyPeak": MonthlyPeak, "YearlyDemand": YearlyDemand,
                 "RecommendedACPower": RecommendedACPower, "DailyProfile2Display": DailyProfile2Display,
                 "Profile": Profile,
                 "ACPowerFromConsumption": RecommendedACPower}

    # OutputDic = {"MonthlyPeak":MonthlyPeak,"MonthlyOutput":MonthlyOutput,"YearlyDemand":YearlyDemand
    #               } 

    return jsonify(OutputDic), 200


@app.route('/locationlayout', methods=['POST'])
@jwt_authenticated
def location():
    gcs = storage.Client()
    ds = datastore.Client()

    req_json = request.get_json()['Location']
    userinput = req_json  # json.loads(req_json)
    userinput['email'] = request.email
    orgname = request.org
    parent_key = ds.key('users', userinput['email'])

    errmsg = ""
    entity = ds.get(key=ds.key('Project', userinput['project_id'], parent=parent_key))
    uniqueid = 'Uid_' + userinput['email'] + '_Pid_' + userinput['project_id']
    message = {}
    if not entity:
        # entity = datastore.Entity(key=ds.key('users',user_details['email']))
        # entity.update(record)
        # ds.put(entity)
        message.update({"error": "No user with username {}  and projectname {} exists".format(userinput['email'],
                                                                                              userinput['project_id'])})
        return jsonify(message), 400

    bucket = gcs.get_bucket(BUCKET_NAME)
    # TODO : Ideal Azimuth from Shading horizon

    if 'readstored' in userinput.keys():
        OutputDic = {}
        if 'Location' in entity:
            try:
                blobname = entity['Location'].get('GeoCoords')
                LatPolyfromfile = json.loads(bucket.get_blob(blobname).download_as_string())
                OutputDic.update({"GeoCoords": LatPolyfromfile})
                if entity['Location'].get('PanelCoords'):
                    PanelCoords = json.loads(bucket.get_blob(entity['Location']['PanelCoords']).download_as_string())
                    OutputDic.update({"PanelCoords": PanelCoords})
                for key, val in entity['Location'].items():
                    if key not in ['PanelCoords', 'GeoCoords']:
                        OutputDic.update({key: val})
                return jsonify(json.loads(json.dumps(OutputDic))), 200

            except:
                jsonify({"error": "Geocoord values not entered"}), 400
        else:
            return jsonify({"error": "Location values not entered"}), 400

    if ('landcoordGeofile' in userinput.keys()):
        AreaCoords = userinput['landcoordGeofile']
        GeoCoordFile = USER_UPLOADS + AreaCoords

        blobname = USER_UPLOADS + AreaCoords
        bucket = gcs.get_bucket(BUCKET_NAME)
        blob = bucket.get_blob(blobname)
        s = blob.download_as_string()

        LatPolyfromfile = json.loads(s)


    else:
        # try:
        if userinput.get('GeoCoords', {}) != {}:
            AreaCoords = userinput['GeoCoords']
            LatPolyfromfile = AreaCoords
            blobname = USER_UPLOADS + uniqueid + '_layoutarea.json'
            blob = bucket.blob(blobname)
            blob.upload_from_string(json.dumps(AreaCoords), content_type='application/json')
            GeoCoordFile = blobname

        else:
            # except:
            blobname = USER_UPLOADS + uniqueid + '_layoutarea.json'
            # blob=bucket.blob(blobname)
            # blob.upload_from_string(json.dumps(AreaCoords),content_type='application/json')
            GeoCoordFile = blobname
            if bucket.get_blob(blobname):
                LatPolyfromfile = json.loads(bucket.get_blob(blobname).download_as_string())
            else:
                return jsonify({"error": "coordinates not provided"}), 400
        # store the area coords as json in file
        # blobname=USER_UPLOADS+uniqueid+'_layoutarea.json'  
        # blob=bucket.blob(blobname)
        # blob.upload_from_string(json.dumps(AreaCoords),content_type='application/json')
        # GeoCoordFile = blobname

    # TODO: Ensure - uniqueness of uploaded file name    

    Areas = dict()
    DCfromArea = dict()
    SurfArea = dict()

    ModDefData = dict()

    GeoPanOut = dict()
    RealPanOut = dict()
    AreaFigures = {"Areawise": {}}
    #    if type(AreaCoords)==str:
    #    
    #        blobname=USER_UPLOADS+AreaCoords   
    #        bucket=gcs.get_bucket(BUCKET_NAME)
    #        blob=bucket.get_blob(blobname)
    #        s = blob.download_as_string()
    #         
    #        LatPolyfromfile = json.loads(s)
    #    
    #         
    #    elif type(AreaCoords)==dict:
    #        LatPolyfromfile = AreaCoords
    #        
    #    else:
    #         MaxPanelArea = -1
    #         LatPolyfromfile = {}

    elecDesign = ELPARM

    defaultSpacingParams = elecDesign['systemparams']['SpacingParams']
    # if ('SpacingParams' not in userinput.keys()):    
    #     SpacingParams = elecDesign['systemparams']['SpacingParams']

    # else:
    #     SpacingParams = userinput['SpacingParams']
    #     for vals in elecDesign['systemparams']['SpacingParams']:
    #          if vals not in SpacingParams.keys():
    #             SpacingParams[vals] = elecDesign['systemparams']['SpacingParams'][vals]

    defaultAreaSpacingParams = {AreaLabel: defaultSpacingParams for AreaLabel in LatPolyfromfile}
    if 'AreaSpacingParams' in userinput:
        AreaSpacingParams = userinput['AreaSpacingParams']
        for AreaLabel in AreaSpacingParams:
            SpacingParams = AreaSpacingParams[AreaLabel]
            for vals in elecDesign['systemparams']['SpacingParams']:
                if vals not in SpacingParams.keys():
                    SpacingParams[vals] = elecDesign['systemparams']['SpacingParams'][vals]
            AreaSpacingParams[AreaLabel] = SpacingParams
    elif 'Location' in entity:
        AreaSpacingParams = entity['Location'].get('AreaSpacingParams', defaultAreaSpacingParams)
    else:
        AreaSpacingParams = {AreaLabel: defaultSpacingParams for AreaLabel in LatPolyfromfile}

    ModDefData['LongSide'] = defaultSpacingParams['H_default']
    ModDefData['ShortSide'] = defaultSpacingParams['W_default']
    PVModuleData = PVSYSDF['JA_Solar_JAM54S30_405_MR']  # PVSYSDF['Longi_LR4_60_HIH_380_M_G2']

    Maxm2perkW = elecDesign['systemparams']['Maxm2perkW']
    Minm2perkW = elecDesign['systemparams']['Minm2perkW']
    # Read in Spacing Params from REST API

    for AreaLabel in AreaSpacingParams:
        # Sanitise inputs here only
        AreaSpacingParams[AreaLabel]['Ti_P'] = abs(AreaSpacingParams[AreaLabel]['Ti_P'])
        # if LatPolyfromfile[AreaLabel]['Ext'][0][0]>0:  # Northern Hemisphere 
        #     if AreaSpacingParams[AreaLabel]['layout']=='flat':
        #         AreaSpacingParams[AreaLabel]['Ti_P'] = abs(AreaSpacingParams[AreaLabel]['Ti_P']) # Should be negative to face south
        #     else:
        #         AreaSpacingParams[AreaLabel]['Ti_P']  = abs(AreaSpacingParams[AreaLabel]['Ti_P'])  # Should be positive for east-west  
        # else: # Southern Hemisphere
        #     if AreaSpacingParams[AreaLabel]['layout']=='flat':
        #         AreaSpacingParams[AreaLabel]['Ti_P'] = abs(AreaSpacingParams[AreaLabel]['Ti_P'])
        #     else:
        #         AreaSpacingParams[AreaLabel]['Ti_P'] = abs(AreaSpacingParams[AreaLabel]['Ti_P'])   

    TotalPanelArea = 0

    NumPanelsArea, AreaFigures, Areas = GridM.MakeGeoPanLayout(LatPolyfromfile, AreaSpacingParams, PVModuleData)

    for AreaLabel in NumPanelsArea:
        SurfArea[AreaLabel] = AreaFigures['Areawise'][AreaLabel]['area']

    # #TODO: Save Install Plane Points - or perform second rotation
    Maxm2perkW = ELPARM['systemparams']['Maxm2perkW']
    Minm2perkW = ELPARM['systemparams']['Minm2perkW']

    MaxNumPanels = np.sum([d for d in NumPanelsArea.values()])
    TotalDCfromArea = [(MaxNumPanels * PVModuleData.Wp / 1.11) / 1000, (MaxNumPanels * PVModuleData.Wp) / 1000]  # in kW
    TotalPanelArea = MaxNumPanels * PVModuleData.A_c

    # TotalDCfromArea = [TotalPanelArea/Maxm2perkW,TotalPanelArea/Minm2perkW]
    RecommendedAC = TotalDCfromArea  # [TotalPanelArea/Maxm2perkW,TotalPanelArea/Minm2perkW]
    # except:
    #     return jsonify({"error": "Error in geometry - not closed shapes, non in plane  or unusual distortions"}), 400
    AreaFigures.update({"TotalDCInOut": 5})

    location_entity = {'Location': {'latitude': userinput['latitude'], 'longitude': userinput['longitude'],
                                    'locationName': userinput['locationName'],
                                    'map address': userinput['map address'],
                                    'GeoCoords': GeoCoordFile, 'AreaSpacingParams': AreaSpacingParams,
                                    'tilt': dict([(AreaLabel, Areas[AreaLabel].tilt) for AreaLabel in Areas]),
                                    'azimuth': dict([(AreaLabel, Areas[AreaLabel].azimuth) for AreaLabel in Areas]),
                                    'SurfArea': dict([(AreaLabel, SurfArea[AreaLabel]) for AreaLabel in Areas]),
                                    'TotalPanelArea': TotalPanelArea, 'MaxPanelArea': TotalPanelArea,
                                    "AreaFigures": AreaFigures
                                    }}

    if 'CountryCode' in userinput:
        location_entity['Location'].update({"countrycode": userinput['CountryCode']})
        errmsg += " Country Code {} set ".format(userinput['CountryCode'])
    if 'Postal' in userinput:
        if userinput['Postal']:
            location_entity['Location'].update({"PLZ": userinput['Postal']})
            errmsg += " PLZ {} set".format(userinput['Postal'])
        else:
            try:
                ZIP = entity['projectdetails']['customerinfo']['zip']
                location_entity['Location'].update({"PLZ": ZIP})
                errmsg += " PLZ {} set".format(ZIP)
            except:
                location_entity['Location'].update({"PLZ": userinput['Postal']})
                errmsg += " PLZ {} set".format(userinput['Postal'])
    entity.update(location_entity)
    entity.update({"status": "Location"})
    ds.put(entity)

    outputdic = {"status": errmsg, "TotalDCfromArea": TotalDCfromArea, "RecommendedAC": RecommendedAC,
                 "GeoPanCoords": GeoPanOut, "RealPanCoords": RealPanOut}

    return jsonify(outputdic), 200


@app.route('/get-weather-service', methods=['POST', 'GET'])
@jwt_authenticated
def weatherservice():
    # TODO: Split this into 2 parts
    #   1.  Upload location and layout details worth recommended AC power 
    #   2.  Input weather service selection and get plots 
    #     
    gcs = storage.Client()
    ds = datastore.Client()
    # stor=storage.Client()
    bucket = gcs.get_bucket(BUCKET_NAME)

    if request.method == 'GET':
        project_id = request.args.get('project_id')
        uniqueid = 'Uid_' + request.email + '_Pid_' + project_id
        blobname = FETCHED_WEATHER + uniqueid + '_weather.pkl'
        if bucket.get_blob(blobname):

            SolData = pickle.loads(bucket.get_blob(blobname).download_as_string())
            SolData['Time'] = SolData.index.astype(str)

            parent_key = ds.key('users', request.email)
            entity = ds.get(key=ds.key('Project', project_id, parent=parent_key))
            lat = entity['Location']['latitude']
            lon = entity['Location']['longitude']
            if 'Weather' in entity:
                TZ = entity['Weather'].get('TZ', 0)
            else:
                TZ = 0

            loc = Location(lat, lon, tz=TZ)
            solpos = loc.get_solarposition(SolData['Time'])
            SolDataWSolPos = pd.concat([SolData, solpos], axis=1)
            return jsonify(SolDataWSolPos[['Time', 'DNI', 'GHI', 'DHI', 'Temperature', 'Wspd', 'zenith', 'elevation',
                                           'azimuth']].to_dict(orient='list')), 200
        else:
            return jsonify({"error": "No weather data found"}), 400
    req_json = request.get_json()['Weather']
    userinput = req_json  # json.loads(req_json)
    userinput['email'] = request.email
    orgname = request.org
    parent_key = ds.key('users', userinput['email'])

    entity = ds.get(key=ds.key('Project', userinput['project_id'], parent=parent_key))
    uniqueid = 'Uid_' + userinput['email'] + '_Pid_' + userinput['project_id']
    message = {}
    if not entity:
        message.update({"error": "No user with username {}  and projectname {} exists".format(userinput['email'],
                                                                                              userinput['project_id'])})
        return jsonify(message), 400

    # entityNone=ds.get(key=ds.key('users','None','Project',userinput['project_id']))

    if 'weathersource' in userinput:
        weathersource = userinput['weathersource']  # uniqueid+userinput['weathersource']
    else:
        weathersource = 'best'

    entchecklist = ['Location']
    for ec in entchecklist:
        if ec not in entity:
            return jsonify({"error": "{} not set in datastore".format(ec)}), 400

    lat = entity['Location']['latitude']
    lon = entity['Location']['longitude']
    tilt = entity['Location']['tilt']
    azimuth = entity['Location']['azimuth']
    errmsg = ""

    weatherfile = 'None'
    try:
        locationName = entity['Location']['locationName']
    except:
        locationName = 'default'
    #    if type(userinput['locationName'])==str:
    #        locationName = userinput['locationName']  
    #    else:
    #        locationName = 'None'

    # TODO: Read in downloaded weatherfile

    if (weathersource.endswith('.tm2') or (weathersource.endswith('.csv')) or (weathersource.endswith('.CSV'))):
        weatherfile = userinput['weathersource']
    elif (weathersource == 'none'):
        weatherfile = 'None'

    if (weathersource == 'best' or weathersource == 'Best'):
        if (lat > 8) & (lat < 35) & (lon > 68) & (lon < 96):  # India, South Asia 
            weathersource = 'NSRDB'
        elif (lat > -20) & (lat < 60) & (lon > -170) & (lon < -20):
            weathersource = 'NSRDB'  # Western Hemisphere    
        elif (lat > -60) & (lat < 60) & (lon > -63) & (lon < 63):
            # weathersource = 'CAMS' #Europe, Atlantic, ME, Africa
            weathersource = 'PVGIS'
        else:
            weathersource = 'PVGIS'
    #            weathersource = 'NASA'

    downloadNewWeatherFile = False

    if 'Weather' in entity:
        if entity['Weather'].get('SolmetaData'):
            SolmetaData = entity['Weather'].get('SolmetaData')
            if (lat != entity['Weather']['SolmetaData'].get('latitude', 0)) or (
                    lon != entity['Weather']['SolmetaData'].get('longitude', 0)):
                downloadNewWeatherFile = True
            if weathersource != entity['Weather'].get('weathersource', ''):
                downloadNewWeatherFile = True
        else:
            downloadNewWeatherFile = True
            SolmetaData = {}
    else:
        downloadNewWeatherFile = True
        SolmetaData = {}

    blobname = FETCHED_WEATHER + uniqueid + '_weather.pkl'
    if not bucket.get_blob(blobname):
        downloadNewWeatherFile = True

    if downloadNewWeatherFile:
        SolData, SolmetaData, errmsg = WG.retrieve_weather(weathersource, lat,
                                                           lon, locationName, weatherfile)

        blob = bucket.blob(blobname)
        blob.upload_from_string(pickle.dumps(SolData))
        finalweatherfile = FETCHED_WEATHER + uniqueid + '_weather.pkl'
    else:
        SolData = pickle.loads(bucket.get_blob(blobname).download_as_string())
        finalweatherfile = blobname
        errmsg = "Using stored file"

    try:
        location_data = Location(SolmetaData['latitude'], SolmetaData['longitude'], tz=SolmetaData['TZ'])
    except:
        return jsonify({"data for error": [SolmetaData],
                        "errmsg": errmsg}), 400

    # Get the bucket that the file will be uploaded to.

    # Create a new blob and upload the file's content          

    # Get Optimum Tilt
    opttilt = {}
    # for areas in tilt:
    #     opttilt[areas] = PVcalc.OptimumTilt(SolData,lat,lon,panelazimuth=azimuth[areas])

    #    tilt_temp = tilt
    #    
    #    try:
    #        tiltvalue = float(tilt_temp)
    #    except Valueerror:    
    #        tiltvalue = tilt_temp
    #    
    #    if (type(tiltvalue)==str):
    #            tilt = PVcalc.OptimumTilt(SolData,lat,lon,panelazimuth=azimuth,period=str(tiltvalue))
    #          
    #    elif (~np.isnan(tiltvalue)):
    #           tilt = tiltvalue

    # TODO : Ideal Azimuth from Shading horizon
    """
    if ('GeoCoords' in userinput.keys()):    
        AreaCoords = userinput['GeoCoords']

        #store the area coords as json in file
        blobname=USER_UPLOADS+uniqueid+'_layoutarea.json'  
        blob=bucket.blob(blobname)
        blob.upload_from_string(json.dumps(AreaCoords),content_type='application/json')
        GeoCoordFile = blobname

    else:      
        AreaCoords = userinput['landcoordGeofile']
        GeoCoordFile = USER_UPLOADS+AreaCoords
    #TODO: Ensure - uniqueness of uploaded file name    

    Areas = dict()
    DCfromArea = dict()

    ModDefData = dict()
    """
    Weather2Display = pd.DataFrame()
    """
    if type(AreaCoords)==str:

        blobname=USER_UPLOADS+AreaCoords   
        bucket=gcs.get_bucket(BUCKET_NAME)
        blob=bucket.get_blob(blobname)
        s = blob.download_as_string()

        LatPolyfromfile = json.loads(s)


    elif type(AreaCoords)==dict:
        LatPolyfromfile = AreaCoords

    else:
         MaxPanelArea = -1
         LatPolyfromfile = {}


    elecDesign = ELPARM

    if ('SpacingParams' not in userinput.keys()):    
        SpacingParams = elecDesign['systemparams']['SpacingParams']
    else:
        SpacingParams = userinput['SpacingParams']
        for vals in elecDesign['systemparams']['SpacingParams']:
             if vals not in SpacingParams.keys():
                SpacingParams[vals] = elecDesign['systemparams']['SpacingParams'][vals]



    ModDefData['LongSide'] = SpacingParams['H_default']
    ModDefData['ShortSide'] = SpacingParams['W_default']


    Maxm2perkW = elecDesign['systemparams']['Maxm2perkW'] 
    Minm2perkW = elecDesign['systemparams']['Minm2perkW']
    # Read in Spacing Params from REST API


    TotalPanelArea = 0
    for AreaLabel in LatPolyfromfile:

        Areas[AreaLabel] = Geo2M.Geo2M3D(LatPolyfromfile[AreaLabel])
        InstallPlanePoints = Areas[AreaLabel].Real2InPlane(Areas[AreaLabel].Coords) # Project entire shape to installation surface in 2D
        OuterPoly = InstallPlanePoints  
        OuterPolyWKeepOut = LinearRing(InstallPlanePoints).parallel_offset(SpacingParams['setback'],"right",join_style=2)

        OuterPolyWKeepOut = LinearRing(OuterPoly).parallel_offset(SpacingParams['setback'],"right",join_style=2) # Demarcate keepout areas all along the boundaries

        ProjHoles = [Areas[AreaLabel].Real2InPlane(HoleCoords) for HoleCoords in Areas[AreaLabel].Holes]

        S = Polygon(Polygon(OuterPolyWKeepOut).exterior.coords,[Polygon(inner).exterior.coords for inner in (ProjHoles)]) # Prepare input polygon

        MultiPanelPolygons = GridM.FillAreaWithPanels(S,ModDefData,SpacingParams)

        MaxPanelArea = np.sum([Pol.area for Pol in MultiPanelPolygons])
        TotalPanelArea += MaxPanelArea

        DCfromArea[AreaLabel] = [MaxPanelArea/Maxm2perkW,MaxPanelArea/Minm2perkW]

        tilt = Areas[AreaLabel].tilt
        azimuth = Areas[AreaLabel].azimuth
    """
    # To display, GHI and Total Incident Radiation - Current, Total Incident Radiation - if ideal 
    for AreaLabel in tilt:
        tdf = pd.concat(
            {AreaLabel: PVcalc.WeatherDatatoDisplay(SolData, location_data, tilt[AreaLabel], azimuth[AreaLabel])},
            names=['AreaLabels'])
        Weather2Display = pd.concat([Weather2Display, tdf])

    # TODO: Save Install Plane Points - or perform second rotation
    #    Maxm2perkW = ELPARM['systemparams']['Maxm2perkW'] 
    #    Minm2perkW = ELPARM['systemparams']['Minm2perkW']
    #     
    #    
    #    TotalDCfromArea = [TotalPanelArea/Maxm2perkW,TotalPanelArea/Minm2perkW]
    #    RecommendedAC = [TotalPanelArea/Maxm2perkW/1.3,TotalPanelArea/Minm2perkW/1.3]
    #    
    #        if MaxPanelArea>0:
    #            ACPowerfrmSpace = [MaxPanelArea/Maxm2perkW,MaxPanelArea/Minm2perkW] 
    #        else:
    #            ACPowerfrmSpace = 'None'
    #        

    Weather_entity = {
        'Weather': {'weathersource': weathersource, 'TZ': SolmetaData['TZ'],
                    'finalweatherfile': finalweatherfile,
                    'SolmetaData': SolmetaData
                    }
    }

    entity.update(Weather_entity)
    entity.update({"status": "Weather"})

    ds.put(entity)

    # Show weather for best Irradiated surface in monthly resolution

    outputdic = {"Weather2Display":
                     Weather2Display.loc[
                         pd.IndexSlice[Weather2Display.groupby(level=[0]).sum()['IrrOnSurface'].idxmax(), :]].groupby(
                         [pd.Grouper(freq='1M')]).mean()
                         .to_dict('list'), "IdealTilt": opttilt}

    outputdic.update({"Status": errmsg})

    return jsonify(outputdic), 200


@app.route('/update-user-params', methods=['POST', 'GET'])  # 
@jwt_authenticated
def userparams():
    ds = datastore.Client()
    gcs = storage.Client()
    bucket = gcs.get_bucket(BUCKET_NAME)

    req_json = request.get_json()['Params']
    userinput = req_json
    userinput['email'] = request.email
    orgname = request.org
    parent_key = ds.key('users', userinput['email'])

    message = {}
    outputdict = {}
    default_costparams = FINPARM['cost']
    OutputDic = {}
    uniqueid_Proj = 'Uid_' + userinput['email'] + '_Pid_' + userinput['project_id']

    if "uploadvals" not in userinput:
        message.update({"uploadvals": "No params key to update"})

    else:
        with ds.transaction():
            entity = ds.get(key=ds.key('Project', userinput['project_id'], parent=parent_key))
            if not entity:
                message.update({"error": "No project with username {} and project {} exists".format(userinput['email'],
                                                                                                    userinput[
                                                                                                        'project_id'])})
                return jsonify(message), 400

            if "criteria" in userinput['uploadvals']:
                entity.update({"criteria": userinput['uploadvals']['criteria'], "status": "Optimization Criteria"})

                message.update({"Success": "Criteria updated"})

            elif "finparams" in userinput['uploadvals']:
                entity.update({"status": "Financials"})
                if 'finparams' in entity:
                    Dic = entity['finparams']
                    for keys in userinput['uploadvals']['finparams']:
                        Dic[keys] = userinput['uploadvals']['finparams'][keys]
                    if 'Adjustments' in Dic:
                        Adjustments = userinput['uploadvals']['finparams']['Adjustments']
                        for k, v in Adjustments.items():
                            if 'data' in v:
                                if v['data'] == None:
                                    v['data'] = 0
                        Dic['Adjustments'] = Adjustments

                else:
                    Dic = userinput['uploadvals']['finparams']
                    Adjustments = userinput['uploadvals']['finparams']['Adjustments']
                    for k, v in Adjustments.items():
                        if v['data'] == None:
                            v['data'] = 0
                    Dic['Adjustments'] = Adjustments
                entity.update({"finparams": Dic})
                message.update({"Success": "financial params updated"})

            elif "pvmoduledata" in userinput["uploadvals"]:
                if 'name' not in userinput['uploadvals']['pvmoduledata']:
                    return jsonify({"error": "No component name"}), 400
                else:
                    name = userinput['uploadvals']['pvmoduledata']['name']
                    moddata = userinput['uploadvals']['pvmoduledata']['params']

                uniqueid = 'Uid_' + userinput['email']

                if type(moddata) == str:
                    if moddata.endswith('.csv'):
                        blobcname = USER_UPLOADS + moddata
                        cwd = os.getcwd()
                        try:
                            blobcsv = bucket.get_blob(blobcname)
                        except:
                            return jsonify({"error": "No file uploaded as {}".format(blobcname)}), 400
                        if blobcsv:
                            blobcsv.download_to_filename(os.path.join(cwd, moddata))
                            moddatadict_raw = pd.read_csv(moddata).set_index('params').to_dict()
                            stringkeys = ['BIPV', 'Date', 'celltype', 'Manufacturer', 'Name']
                            moddatadict = {key: float(value) if key not in stringkeys else value
                                           for key, value in moddatadict_raw['value'].items()}
                        else:
                            return jsonify({"error": "No file uploaded as {}".format(blobcname)}), 400

                        moddata = moddatadict
                    else:
                        return jsonify({"error": "wrong format of file "})

                blobname_pvmod = USER_UPLOADS + uniqueid + '_moduledata.pkl'
                blobpv = bucket.get_blob(blobname_pvmod)
                if blobpv:
                    s = blobpv.download_as_string()
                    USERPVDF = pickle.loads(s)
                    # USERPVDF[name] = [moddata[ind] for ind in USERPVDF.index]
                    if name in USERPVDF:
                        USERPVDF.drop(columns=name, inplace=True)
                    NewModDf, errmsg = SD.getModparamsfromDS(moddata, name)  # Only works for desoto model
                    USERPVDF = USERPVDF.join(NewModDf, how='outer').loc[
                        ~USERPVDF.join(NewModDf, how='outer').index.duplicated(keep='first')]

                else:
                    blobpv = bucket.blob(blobname_pvmod)
                    USERPVDF = pd.DataFrame()  # pd.DataFrame(moddata.values(),index=moddata.keys(),columns=[name])
                    NewModDf, errmsg = SD.getModparamsfromDS(moddata, name)  # Only works for desoto model
                    USERPVDF = USERPVDF.join(NewModDf, how='outer').loc[
                        ~USERPVDF.join(NewModDf, how='outer').index.duplicated(keep='first')]

                blobpv.upload_from_string(pickle.dumps(USERPVDF))
                message.update(
                    {"Component upload": "{} uploaded successfully for user {} ".format(name, userinput['email'])})

            elif "invdata" in userinput["uploadvals"]:
                if 'name' not in userinput['uploadvals']['invdata']:
                    return jsonify({"error": "No component name"})
                else:
                    name = userinput['uploadvals']['invdata']['name']
                moddata = userinput['uploadvals']['invdata']['params']
                uniqueid = 'Uid_' + userinput['email']

                if type(moddata) == str:
                    if moddata.endswith('.csv'):
                        blobcname = USER_UPLOADS + moddata
                        cwd = os.getcwd()
                        try:
                            blobcsv = bucket.get_blob(blobcname)
                        except:
                            return jsonify({"error": "No file uploaded as {}".format(blobcname)}), 400
                        if blobcsv:
                            blobcsv.download_to_filename(os.path.join(cwd, moddata))
                        else:
                            return jsonify({"error": "No file uploaded as {}".format(blobcname)}), 400
                        moddatadict = pd.read_csv(moddata).set_index('params').to_dict()
                        moddata = moddatadict['value']
                    else:
                        return jsonify({"error": "wrong format of file "})

                blobname_inv = USER_UPLOADS + uniqueid + '_invdata.pkl'
                blobinv = bucket.get_blob(blobname_inv)
                NEWINVDF = pd.DataFrame(moddata.values(), index=moddata.keys(), columns=[name])
                if blobinv:
                    s = blobinv.download_as_string()
                    USERINVDF = pickle.loads(s)

                    if name in USERINVDF:
                        USERINVDF.drop(columns=name, inplace=True)

                    USERINVDF = USERINVDF.join(NEWINVDF, how='outer').loc[
                        ~USERINVDF.join(NEWINVDF, how='outer').index.duplicated(keep='first')]
                    # USERINVDF[name] = [moddata[ind] for ind in USERINVDF.index]

                else:
                    blobinv = bucket.blob(blobname_inv)
                    USERINVDF = pd.DataFrame(moddata.values(), index=moddata.keys(), columns=[name])

                blobinv.upload_from_string(pickle.dumps(USERINVDF))
                message.update(
                    {"Component upload": "{} uploaded successfully for user {}".format(name, userinput['email'])})

            elif "components" in userinput['uploadvals']:
                UpDic = {}
                if 'Components' in entity:
                    UpDic = entity['Components']
                    UpDic.update(userinput['uploadvals']['components'])
                    entity.update({'Components': UpDic})

                else:
                    UpDic.update(userinput['uploadvals']['components'])
                    entity.update({"Components": UpDic})

                # Change the Cost values also to reflect the new components
                # TODO: Update Cost Params as well
                if "Cost" in entity:
                    if 'ActiveQuote' not in entity:
                        CDic = entity['Cost']
                        CDic['PVCost'] = {PVName: {'data': default_costparams['PVCost']['data'][0],
                                                   'Unit': default_costparams['PVCost']['Unit']} if PVName not in CDic[
                            'PVCost'] else
                        {'data': CDic['PVCost'][PVName]['data'],
                         'Unit': CDic['PVCost'][PVName]['Unit']} for PVName in UpDic['PVName']}

                        CDic['InvCost'] = {InvName: {'data': default_costparams['InvCost']['data'][0],
                                                     'Unit': default_costparams['InvCost']['Unit']} if InvName not in
                                                                                                       CDic[
                                                                                                           'InvCost'] else
                        {'data': CDic['InvCost'][InvName]['data'],
                         'Unit': CDic['InvCost'][InvName]['Unit']} for InvName in UpDic['InvName']}

                        entity.update({'Cost': CDic})
                    else:
                        CDic = {}

                else:
                    CDic = {}
                    CDic['PVCost'] = {PVName: {'data': default_costparams['PVCost']['data'][0],
                                               'Unit': default_costparams['PVCost']['Unit']} for PVName in
                                      UpDic['PVName']}
                    CDic['InvCost'] = {InvName: {'data': default_costparams['InvCost']['data'][0],
                                                 'Unit': default_costparams['InvCost']['Unit']} for InvName in
                                       UpDic['InvName']}

                    entity.update({'Cost': CDic})

                message.update({"Components upload": "{} uploaded successfully for user {}".format(
                    userinput['uploadvals']['components'], userinput['email'])})
                OutputDic.update({"Cost": CDic, "Components": UpDic})
                # message.update({"debug": [CDic,UpDic]})
            elif "cost" in userinput['uploadvals']:
                if 'Cost' in entity:
                    UpDic = userinput['uploadvals']['cost']
                    # UpDic = entity['Cost']
                    entity.update({"Cost": UpDic})
                    # for item in userinput['uploadvals']['cost']:
                    #     UpDic[item]=userinput['uploadvals']['cost'][item]
                else:
                    UpDic = userinput['uploadvals']['cost']
                    entity.update({"Cost": UpDic})
                message.update({"Cost update": "{} uploaded successfully for user {} ".format(
                    userinput['uploadvals']['cost'], userinput['email'])})

            elif "bominputfilter" in userinput['uploadvals']:
                UpDic = userinput['uploadvals']['bominputfilter']
                entity.update({"bominputfilter": UpDic})
                if 'cablelength' in UpDic:
                    TotalDCInOut = UpDic['cablelength']['inner'] + UpDic['cablelength']['outer']
                    if 'AreaFigures' in entity:
                        entity['AreaFigures'].update({"TotalDCInOut": TotalDCInOut})
                    else:
                        entity.update({"AreaFigures": {"TotalDCInOut": TotalDCInOut}})
                message.update({"bomfilter": "{} added for filtering for user {}".format(
                    userinput['uploadvals']['bominputfilter'], userinput['email'])})
            elif "status" in userinput['uploadvals']:
                entity.update({"status": userinput['uploadvals']['status']})
            elif "StringArrayConfig" in userinput['uploadvals']:
                entity.update({"StringArrayConfig": userinput['uploadvals']['StringArrayConfig']})
            elif "ReportContentsStored" in userinput['uploadvals']:
                entity.update({"ReportContentsStored": userinput['uploadvals']['ReportContentsStored']})

            ds.put(entity)

    if "defaultvals" not in userinput:
        message.update({"defaultvals": "No defaultvals requested"})

    else:
        if userinput['defaultvals'] == "cost":
            outputdict.update({"default costs": FINPARM["cost"]})
            message.update({"Success": "Default costs provided"})
        elif userinput['defaultvals'] == "financial":
            outputdict.update({"default financials": FINPARM["financial"]})
            message.update({"Success": "Default financial params provided"})

        elif userinput['defaultvals'] == "ACPower":

            #### Remove this entire thing #####

            entity = ds.get(key=ds.key('Project', userinput['project_id'], parent=parent_key))
            if not entity:
                message.update({"error": "No project with username {} and project {} exists".format(userinput['email'],
                                                                                                    userinput[
                                                                                                        'project_id'])})
                return jsonify(message), 400

            ##################GET DEMAND TS############
            country = "CHE"
            blobname = AUXFILES + 'PriceAPICountry.json'
            CI = json.loads(bucket.blob(blobname).download_as_string())
            blobnamePic = AUXFILES + CI['Data'][country]['PriceDb']
            path = os.getcwd()
            e = bucket.blob(blobnamePic).download_to_filename(os.path.join(path, 'PriceDb.pkl'))
            with open(os.path.join(path, 'PriceDb.pkl'), 'rb') as f:
                Data = pickle.load(f)
            UpDic = {}
            if 'Customer Inputs' in entity:
                CIn = entity['Customer Inputs']
                if 'YearlyElecRequired' in entity['Customer Inputs']:
                    if (('Elec4HeatkWh' in entity['Customer Inputs']['YearlyElecRequired']) and (
                            'EleckWh' in entity['Customer Inputs']['YearlyElecRequired'])):
                        TotalHeat = entity['Customer Inputs']['YearlyElecRequired']['Elec4HeatkWh']
                        TotalElec = entity['Customer Inputs']['YearlyElecRequired']['EleckWh']
                        DemandTS, E, H = Sup.MakeGenericDemand(Data, TotalElec=TotalElec, TotalHeat=TotalHeat)
                    elif 'TotalElec' in entity['Customer Inputs']['YearlyElecRequired']:
                        TotalElec = entity['Customer Inputs']['YearlyElecRequired']['TotalElec']
                        DemandTS, E, H = Sup.MakeGenericDemand(Data, TotalElec=TotalElec)
                    else:
                        DemandTS, E, H = Sup.MakeGenericDemand(Data)
                else:
                    DemandTS, E, H = Sup.MakeGenericDemand(Data)

                UpDic.update({"YearlyElecRequired": {"EleckWh": E, "Elec4HeatkWh": H, "TotalElec": E + H}})
                CIn.update(UpDic)
                entity.update(CIn)
            else:
                DemandTS, E, H = Sup.MakeGenericDemand(Data)
                entity.update(
                    {'Customer Inputs': {"YearlyElecRequired": {"EleckWh": E, "Elec4HeatkWh": H, "TotalElec": E + H}}})

            #  Pickle DemandTS        
            Demandblobname = USER_UPLOADS + uniqueid_Proj + '_demandTS.pkl'
            bucket.blob(Demandblobname).upload_from_string(pickle.dumps(DemandTS))

            TotalElecCons = E + H
            ACPowerFromConsRange = [TotalElecCons / 8760, (TotalElecCons / 8760) * 3]
            #################################################################

            if 'Location' in entity:
                TotalPanelArea = entity['Location'].get('MaxPanelArea', entity['Location'].get('TotalPanelArea', 0))
                # if 'TotalPanelArea' in entity['Location']:
                #     TotalPanelArea = entity['Location']['TotalPanelArea']
                Maxm2perkW = ELPARM['systemparams']['Maxm2perkW'] * 1.3
                Minm2perkW = ELPARM['systemparams']['Minm2perkW'] * 1.3
                ACPowerFromArea = [TotalPanelArea / Maxm2perkW, TotalPanelArea / Minm2perkW]

            else:
                ACPowerFromArea = [0, 0]
            #################################################################        

            if 'Demand' in entity:
                ACPowerStored = entity['Demand']['ACpower']
                if 'DemandTS' not in entity['Demand']:
                    entity['Demand']['DemandTS'] = Demandblobname
            else:
                ACPowerStored = 0
                entity.update({'Demand': {'DemandTS': Demandblobname, "ACpower": ACPowerStored}})
            outputdict.update({"ACPowerFromConsumption": ACPowerFromConsRange, "ACPowerFromArea": ACPowerFromArea,
                               "TotalElecCons": TotalElecCons, "ACPowerStore": ACPowerStored})
            message.update({"Success": "ACPower Estimates provided"})
            ds.put(entity)

    outputdict.update(message)
    outputdict.update(OutputDic)

    return jsonify(outputdict), 200


@app.route('/cost-financials', methods=['POST', 'GET'])  # Without storage /techselect_withstorage
@jwt_authenticated
def costfinancial():
    ds = datastore.Client()
    gcs = storage.Client()

    req_json = request.get_json()['Costs']

    userinput = req_json
    userinput['email'] = request.email
    orgname = request.org
    parent_key = ds.key('users', userinput['email'])
    message = {}
    OutputDic = {}

    entity = ds.get(key=ds.key('Project', userinput['project_id'], parent=parent_key))
    if not entity:
        message.update({"error": "No project with username {} and project {} exists".format(userinput['email'],
                                                                                            userinput['project_id'])})
        return jsonify(message), 400

    uschklist = ['cost', 'financial']
    for us in uschklist:
        if us not in userinput:
            return jsonify({"error": "{} not set in input request".format(us)}), 400

            # output.update(userinput['cost'])

    """
    Transform to such a structure

    {'BatteryCost': {'Unit': 'per kWh', 'data': 223},
     'PVCost': {'Unit': 'per Wp', 'data': [0.3,0.4]},
                },
     'InvCost': {'Unit': 'per Wac', 'data': [0.05,0.03]},
                 },
     'Construction': {'Unit': 'per plant', 'data': 0},
     'Bos': {'Unit': 'per plant', 'data': 0},
     'Curr':"USD"}
    """
    CostData = userinput['cost']

    # CostDic = {k:{num:{"Unit":v['Unit'],"data":d} for num,d in zip(range(len(v['data'])),v['data']) } for k,v in CostData.items() if k!='AddonCosts'}
    CostDic = {k: v for k, v in CostData.items() if k != 'AddonCosts'}

    if 'Curr' in CostData['PVCost']:
        Curr = CostData['PVCost']['Curr']
    else:
        Curr = 'CHF'
    if 'AddonCosts' in CostData:
        # TODO: Change these lines to just one if the structure is rationalised
        try:
            CostDic.update(
                {v['Name']: {"Unit": v['Units'], "data": float(v['data'])} for v in CostData['AddonCosts'].values() if
                 type(v) != str})
        except:
            CostDic.update(
                {v['Name']: {"Unit": v['Unit'], "data": float(v['data'])} for v in CostData['AddonCosts'].values() if
                 type(v) != str})

    CostDic.update({"Curr": Curr})
    # TODO: Data Model for Costing
    OutputDic.update({'cost': CostDic})

    entity.update({"Cost": CostDic})

    # Convert cost and financials in a different function
    FinanceData = userinput['financial']
    Curr = FinanceData['Adjustments']['Curr']
    FinanceData['Adjustments'] = {v['Name']: {'Unit': v['Units'], 'data': v['data']} for v in
                                  FinanceData['Adjustments'].values() if type(v) != str}
    FinanceData['Curr'] = Curr

    entity.update({'finparams': FinanceData})

    ds.put(entity)

    return jsonify({"success": "Cost and financial data uploaded for {}".format(userinput['project_id'])}), 200


@app.route('/tariff', methods=['POST', 'GET'])
@jwt_authenticated
def tariffprice():
    ds = datastore.Client()
    gcs = storage.Client()
    bucket = gcs.get_bucket(BUCKET_NAME)

    req_json = request.get_json()['Tariff']

    userinput = req_json
    userinput['email'] = request.email
    orgname = request.org
    parent_key = ds.key('users', userinput['email'])
    message = {}

    entity = ds.get(key=ds.key('Project', userinput['project_id'], parent=parent_key))
    if not entity:
        message.update({"error": "No project with username {} and project {} exists".format(userinput['email'],
                                                                                            userinput['project_id'])})
        return jsonify(message), 400
    uniqueid = 'Uid_' + userinput['email'] + '_Pid_' + userinput['project_id']
    entchklist = ['Location']
    for ent in entchklist:
        if ent not in entity:
            return jsonify({"error": "{} not set in datastore".format(ent)}), 400

    uschklist = ["action"]
    for us in uschklist:
        if us not in userinput:
            return jsonify({"error": "{} not set in input request".format(us)}), 400

    if 'Profile' in entity['Demand']:
        Profile = entity['Demand']['Profile']
        profiletype = Profile.get('profiletype')

    '''
    {"action:"getdata","update"
      if action==update, 
         {"pricetariff": {"eprice":234, pvtariffavg: 23, pvtariffHiWin:23, pvtariffLoWin .... }}
    '''
    OutputDic = {}
    action = userinput['action']
    if 'countrycode' in entity['Location']:
        country = entity['Location']['countrycode']
        blobname = AUXFILES + 'PriceAPICountry.json'
        blob = bucket.get_blob(blobname)
        e = blob.download_as_string()
        CI = json.loads(e)
    if country in CI['Data']:
        blobnamePic = AUXFILES + CI['Data'][country]['PriceDb']
        Data = pickle.loads(bucket.get_blob(blobnamePic).download_as_string())
        # Data = pickle.loads(bucket.get_blob(AUXFILES+"CH_Gemeinde_EPrice_2022.pkl").download_as_string()) 
    if entity['status'] != 'Lock':
        entity.update({"status": "Tariff Details"})
    if action == 'readstored':
        DataSaved = {}
        try:
            if 'EpriceTariffFile' in entity:
                blobname = entity['EpriceTariffFile']
                if blobname.startswith(USER_UPLOADS):
                    datastored = pickle.loads(bucket.get_blob(blobname).download_as_string())
                else:
                    datastored = pickle.loads(bucket.get_blob(USER_UPLOADS + blobname).download_as_string())

                valsin = ["state", "supplier", "ValsForGraph", "PVTariffSummary", "tariff", "city"]
                for keys in valsin:
                    if keys in datastored:
                        DataSaved.update({keys: datastored[keys]})
        except:
            pass
            # Populate keys for choices
        if 'countrycode' in entity['Location']:
            country = entity['Location']['countrycode']
        else:
            ds.put(entity)
            return jsonify(DataSaved), 200
        if country == 'CHE':
            if 'PLZ' in entity['Location']:
                PLZ = entity['Location']['PLZ']
                if not PLZ:
                    try:
                        if entity['projectdetails']['customerinfo'].get('zip'):
                            PLZ = entity['projectdetails']['customerinfo'].get('zip')
                    except:
                        PLZ = 8001
                else:
                    PLZ = 8001
            else:
                if entity['projectdetails']['customerinfo'].get('zip'):
                    PLZ = entity['projectdetails']['customerinfo'].get('zip')
                else:
                    PLZ = 8001

            # Get connection details to external API
            connection = CI['Data'][country]

            # Get stored files with Price DB information
            blobnamePic = AUXFILES + CI['Data'][country]['PriceDb']
            Data = pickle.loads(bucket.get_blob(blobnamePic).download_as_string())
            # Get Supplier and Tarifflist
            CT = Sup.CHTariffGetter(Data, connection, PLZ)
            allsuplist = CT.supplierlist()
            DataSaved.update({"supplierlist": list(allsuplist.keys()), "Curr": "CHF", "city": "", "state": "",
                              "tarifflist": ['H1', 'H2', 'H3', 'H4', 'H5', 'H6', \
                                             'H7', 'H8', 'C1', 'C2', 'C3', 'C4', 'C5', 'C6', 'C7'], "citylist": "",
                              "countrycode": country, "PLZ": PLZ})
        elif country == 'BRA':
            if 'PLZ' in entity['Location']:
                PLZ = entity['Location']['PLZ']
            else:
                PLZ = 100100

            BRAGet = Sup.BRATariffgetter(Data)
            if 'state' in DataSaved:
                citylist = BRAGet.cityfromstate(DataSaved['state'])
            else:
                citylist = []
            DataSaved.update({"supplierlist": BRAGet.supplierlist(), "tarifflist": BRAGet.tarifflist(),
                              "Curr": "BRL", "citylist": citylist, "statelist": BRAGet.statelist(),
                              "countrycode": country, "PLZ": PLZ})

        ds.put(entity)
        return jsonify(DataSaved), 200

    if action == 'auto getdata':

        if 'countrycode' in entity['Location']:
            country = entity['Location']['countrycode']
        #     blobname = AUXFILES+'PriceAPICountry.json'
        #     blob=bucket.get_blob(blobname)
        #     e = blob.download_as_string()
        #     CI = json.loads(e)

        else:
            return jsonify({"status": "no country information available - update in locationlayout",
                            "countrycode": "", "PLZ": ""}), 400
        if country == 'CHE':
            if 'PLZ' in entity['Location']:
                PLZ = entity['Location']['PLZ']
            else:
                PLZ = 8001
            # Get connection details to external API
            connection = CI['Data'][country]

            # Get stored files with Price DB information
            # blobnamePic = AUXFILES+CI['Data'][country]['PriceDb']
            # Data = pickle.loads(bucket.get_blob(blobnamePic).download_as_string()) 

            # Select Elec Supplier according to PLZ/ZIP
            CT = Sup.CHTariffGetter(Data, connection, PLZ)
            SupplierList = CT.suppliers()
            allsuplist = CT.supplierlist()
            OutputDic.update({"supplierlist": list(allsuplist.keys()), "Curr": "CHF", "city": "", "state": "",
                              "tarifflist": ['H1', 'H2', 'H3', 'H4', 'H5', 'H6', \
                                             'H7', 'H8', 'C1', 'C2', 'C3', 'C4', 'C5', 'C6', 'C7'], "citylist": ""})
            evuID_old = '0'
            try:
                if 'EpriceTariffFile' in entity:
                    blobname = entity['EpriceTariffFile']
                    if blobname.startswith(USER_UPLOADS):
                        epriceTariffFile = pickle.loads(bucket.get_blob(blobname).download_as_string())
                    else:
                        epriceTariffFile = pickle.loads(bucket.get_blob(USER_UPLOADS + blobname).download_as_string())

                    if 'evuID' in epriceTariffFile:
                        evuID_old = epriceTariffFile['evuID']
                        SupplierItemOld = {"Name": epriceTariffFile['Eprice']['Netzbetreiber'][0], 'nrElcom': evuID_old}
                        SupplierList.insert(0, SupplierItemOld)
            except:
                pass

            DailyTariff = 'None'
            i = 0
            while (type(DailyTariff) == str) & (i < len(SupplierList)):
                # Data corruption causes nrElcom string to get messed up
                try:
                    evuID = SupplierList[i]['nrElcom']
                except KeyError:
                    nrstring = [s for s in SupplierList[i].keys() if re.search('.*nrElcom', s)][0]
                    evuID = SupplierList[i][nrstring]
                supplier = SupplierList[i]['Name']
                DailyTariff, tariffsummary = CT.pvtariff(evuID)
                i += 1
            if not (type(DailyTariff) == str):
                DailyTariff.fillna(0, inplace=True)

                # for i in range(len(SupplierList)):
            #     if type(DayTariff) == str:
            #         if (SupplierList[i]!={}):
            #             evuID = SupplierList[i]['nrElcom']
            #             DailyTariff,tariffsummary=CT.pvtariff(evuID)
            #     else:
            #         break

            YearlyTariff = CT.makeyearly(DailyTariff)

            Eprice = CT.getEprice(evuID)
            if 'Profile' in entity['Demand']:
                profiletype = entity['Demand']['Profile']['profiletype']
            else:
                profiletype = ''
            if profiletype not in Eprice.index:
                tariffsummary.update({"EpriceAvg": Eprice.iloc[0]['Total'], "Epricetotal": Eprice.iloc[0]['Total'],
                                      "profiletype": Eprice.iloc[0].name})
            else:
                EpriceExtract = Eprice.loc[profiletype, 'Total'] if isinstance(Eprice.loc[profiletype, 'Total'],
                                                                               float) else \
                Eprice.loc[profiletype, 'Total'][0]
                tariffsummary.update(
                    {"EpriceAvg": EpriceExtract, "Epricetotal": EpriceExtract, "profiletype": profiletype})

            if (YearlyTariff['PVTariff'].mean() == 0):
                YearlyTariff['Eprice'] = tariffsummary['EpriceAvg']
                # ensure only single number
            else:
                YearlyTariff['Eprice'] = (YearlyTariff['PVTariff'] / (YearlyTariff['PVTariff'].mean())) * tariffsummary[
                    'EpriceAvg']
            YearlyTariff.fillna(0, inplace=True)

            # 'pvtariffAvg','pvtariffadjust','EpriceAvg','Epriceadjust'

            tariffsummary.update({"Epriceadjust": 0,
                                  "pvtariffadjust": 0, "pvtarifftotal": 0, "EpriceHi": YearlyTariff['Eprice'].max(),
                                  "EpriceLow": YearlyTariff['Eprice'].min()})  # Must match manual upload objects

            if type(DailyTariff) == str:
                ForGraph = {"hours": [x for x in range(24)], "pvtariffWin": [0 for x in range(24)],
                            "pvtariffSum": [0 for x in range(24)],
                            "Eprice": [p for p in YearlyTariff['Eprice'].iloc[:24].values]}
                #  "EpriceAvg":[Eprice.loc['H5','Total'] for x in range(24)]}
            else:
                ForGraph = {"hours": [x for x in range(24)],
                            "pvtariffWin": list(DailyTariff[('winter', 'pvtariff')].astype('float')),
                            "pvtariffSum": list(DailyTariff[('summer', 'pvtariff')].astype('float')),
                            "Eprice": [p for p in YearlyTariff['Eprice'].iloc[:24].values]}
                # "EpriceAvg":[Eprice.loc['H5','Total'] for x in range(24)]}
            # Extraneous - only to maintain same keys as in manual
            ForGraph.update({"pvtariffAvg": ""})

            # Convert Yearly Series into CHF/kWh
            YearlyTariff['Eprice'] = YearlyTariff['Eprice'] * 0.01
            YearlyTariff['PVTariff'] = YearlyTariff['PVTariff'] * 0.01

            Data2save = {"Eprice": Eprice, "YearlyTariff": YearlyTariff, "PVTariffSummary": tariffsummary,
                         "evuID": evuID, "supplier": supplier, "ValsForGraph": ForGraph}

            blobname = USER_UPLOADS + uniqueid + '_epriceTariff.pkl'
            blob = bucket.blob(blobname)
            blob.upload_from_string(pickle.dumps(Data2save))
            epriceTariffFile = blobname  # uniqueid+'_epriceTariff.pkl'
            entity.update({"EpriceTariffFile": epriceTariffFile})

            if "error" in tariffsummary:
                OutputDic.update({"error": tariffsummary['error']})
            OutputDic.update({"PVTariffSummary": tariffsummary,
                              "countrycode": country, "PLZ": PLZ})

            OutputDic.update({"ValsForGraph": ForGraph})
        elif country == 'BRA':
            # blobnamePic = AUXFILES+CI['Data'][country]['PriceDb']
            # Data = pickle.loads(bucket.get_blob(blobnamePic).download_as_string()) 

            BRAGet = Sup.BRATariffgetter(Data)
            OutputDic = {}
            statelist = BRAGet.statelist()
            OutputDic.update({"supplierlist": BRAGet.supplierlist(), "tarifflist": BRAGet.tarifflist(), "Curr": "BRL",
                              "countrycode": country, "statelist": statelist})
            if 'PLZ' in entity['Location']:
                ICMS = Data['StateTaxes']
                # if entity['Location']['PLZ']!=None:
                try:
                    PLZ = int(str(entity['Location']['PLZ'])[:5])  # take only 5 first values
                    STList = [state for state in ICMS.index if
                              ((ICMS.loc[state, 'PLZMax'] >= PLZ >= ICMS.loc[state, 'PLZMin']) or
                               (ICMS.loc[state, 'PLZMax1'] >= PLZ >= ICMS.loc[state, 'PLZMin1']))]
                except:
                    STList = []
                    PLZ = 100100
                if STList != []:
                    state = STList[0]
                else:
                    state = BRAGet.statelist()[0]

                if 'locationName' in entity['Location']:
                    Loccity = entity['Location']['locationName']
                    if STList != []:
                        city = BRAGet.cityfromstate(state, Loccity)[0]
                    else:
                        city = ""
                if (type(state) == str) & (city != ""):
                    supplier = BRAGet.citytoSup(state, city)[0]
                else:
                    supplier = BRAGet.supplierlist(state)[0]
                tariff = 'Residential - B1 - White'
                OutputDic.update({"tariff": 'Residential - B1 - White'})
                OutputDic.update({"state": state, "city": city, "supplier": supplier, "PLZ": PLZ, "tariff": tariff})
            else:
                PLZ = 100100
                state = BRAGet.statelist()[0]
                tariff = 'Residential - B1 - White'
                supplier = BRAGet.supplierlist(state)[0]
                city = ""
                OutputDic.update({"state": state, "city": city, "PLZ": PLZ, "supplier": supplier, "tariff": tariff})

            EpriceDay, pvfitsummary = BRAGet.DailyTariff(supplier, tariff=tariff)
            EpriceYearly = BRAGet.makeyearly(EpriceDay['EpriceWet'], supplier)
            YearlyTariff = pd.DataFrame(EpriceYearly, columns=['Eprice'])
            YearlyTariff.fillna(0, inplace=True)
            YearlyTariff['PVTariff'] = YearlyTariff['Eprice'] * 0
            statetax = BRAGet.Data['StateTaxes'].loc[state].values[0]
            pvfitsummary.update({"Epriceadjust": statetax,
                                 "pvtariffadjust": 0, "pvtarifftotal": 0})
            OutputDic.update({"PVTariffSummary": pvfitsummary})
            ForGraph = {"hours": [x for x in range(24)], "pvtariffWin": [0] * 24,
                        "pvtariffSum": [0] * 24}
            Etodisplay = {
                "Eprice": {"WetSeasonElecPrice": EpriceDay['EpriceWet'], "DrySeasonElecPrice": EpriceDay['EpriceDry']}}
            ForGraph.update(Etodisplay)
            OutputDic.update({"ValsForGraph": ForGraph})
            OutputDic['PVTariffSummary']['Epricetotal'] = EpriceYearly.mean().values[0]
            OutputDic['PVTariffSummary']['EpriceHi'] = EpriceYearly.max().values[0]
            OutputDic['PVTariffSummary']['EpriceLow'] = EpriceYearly.min().values[0]
            OutputDic['PVTariffSummary']['nm'] = 1  # Net metering is  default in Brazil

            Data2save = {"Eprice": EpriceYearly, "YearlyTariff": YearlyTariff, "PVTariffSummary": pvfitsummary,
                         "supplier": supplier, "tariff": tariff, "state": state, "ValsForGraph": ForGraph,
                         "countrycode": country, "PLZ": PLZ, "city": city}

            if 'EpriceTariffFile' in entity:
                blobname = entity['EpriceTariffFile']
                datastored = pickle.loads(bucket.get_blob(blobname).download_as_string())
                for vals in datastored:
                    if vals in Data2save:
                        datastored[vals] = Data2save[vals]
                for vals in Data2save:
                    if vals not in datastored:
                        datastored[vals] = Data2save[vals]
                bucket.get_blob(blobname).upload_from_string(pickle.dumps(datastored))
            else:
                blobname = USER_UPLOADS + uniqueid + '_epriceTariff.pkl'
                bucket.blob(blobname).upload_from_string(pickle.dumps(Data2save))
                entity.update({"EpriceTariffFile": blobname})



        else:
            if 'PLZ' in entity['Location']:
                PLZ = entity['Location']['PLZ']
            else:
                PLZ = 0
            return jsonify(
                {"status": "information for this country not avaialble - use manual input", "country": country,
                 "PLZ": PLZ}), 400
    elif "get info" in action:
        infoparams = action['get info']
        OutputDic = {}
        if country == 'BRA':
            BRAGet = Sup.BRATariffgetter(Data)
            OutputDic.update({"statelist": BRAGet.statelist(), "tarifflist": BRAGet.tarifflist()})
            if 'state' in infoparams:
                state = infoparams['state']
                OutputDic.update({"supplierlist": BRAGet.supplierlist(state), "state": state})
                if 'city' in infoparams:
                    city = infoparams['city']
                    supplier = BRAGet.citytoSup(state, city)
                    OutputDic.update({"supplier": supplier})
                    citylist = BRAGet.cityfromstate(state)
                    OutputDic.update({"citylist": citylist})
                    citysel = BRAGet.cityfromstate(state, searchcity=city)
                    if len(citysel) > 0:
                        OutputDic.update({"city": citysel[0]})

                else:
                    OutputDic.update({"citylist": BRAGet.cityfromstate(state)})
                # default supplier and tariff
                supplier = OutputDic['supplierlist'][0]
                OutputDic.update({"supplier": supplier})
                tariff = 'Residential - B1 - White'
                OutputDic.update({"tariff": 'Residential - B1 - White'})
                EpriceDay, pvfitsummary = BRAGet.DailyTariff(supplier, tariff=tariff)
                EpriceYearly = BRAGet.makeyearly(EpriceDay['EpriceWet'], supplier)
                YearlyTariff = pd.DataFrame(EpriceYearly, columns=['Eprice'])
                YearlyTariff['PVTariff'] = YearlyTariff['Eprice'] * 0
                statetax = BRAGet.Data['StateTaxes'].loc[state].values[0]
                pvfitsummary.update({"Epriceadjust": statetax,
                                     "pvtariffadjust": 0, "pvtarifftotal": 0})
                OutputDic.update({"PVTariffSummary": pvfitsummary})
                ForGraph = {"hours": [x for x in range(24)], "pvtariffWin": [0] * 24,
                            "pvtariffSum": [0] * 24}
                Etodisplay = {"Eprice": {"WetSeasonElecPrice": EpriceDay['EpriceWet'],
                                         "DrySeasonElecPrice": EpriceDay['EpriceDry']}}
                ForGraph.update(Etodisplay)
                OutputDic.update({"ValsForGraph": ForGraph})
                OutputDic['PVTariffSummary']['Epricetotal'] = EpriceYearly.mean().values[0]
                OutputDic['PVTariffSummary']['EpriceHi'] = EpriceYearly.max().values[0]
                OutputDic['PVTariffSummary']['EpriceLow'] = EpriceYearly.min().values[0]
                OutputDic['PVTariffSummary']['nm'] = 1  # Net metering is  default in Brazil
                OutputDic['PVTariffSummary']['profiletype'] = profiletype
                Data2save = {"Eprice": EpriceYearly, "YearlyTariff": YearlyTariff, "PVTariffSummary": pvfitsummary,
                             "supplier": supplier, "tariff": tariff, "state": state, "ValsForGraph": ForGraph,
                             "countrycode": country}
                if 'city' in OutputDic:
                    Data2save['city'] = OutputDic['city']

                if 'EpriceTariffFile' in entity:
                    blobname = entity['EpriceTariffFile']
                    datastored = pickle.loads(bucket.get_blob(blobname).download_as_string())
                    for vals in datastored:
                        if vals in Data2save:
                            datastored[vals] = Data2save[vals]
                    for vals in Data2save:
                        if vals not in datastored:
                            datastored[vals] = Data2save[vals]
                    bucket.get_blob(blobname).upload_from_string(pickle.dumps(datastored))
                else:
                    blobname = USER_UPLOADS + uniqueid + '_epriceTariff.pkl'
                    bucket.blob(blobname).upload_from_string(pickle.dumps(Data2save))
                    entity.update({"EpriceTariffFile": blobname})

                # blobname = USER_UPLOADS +uniqueid+'_epriceTariff.pkl'                   
                # blob=bucket.blob(blobname)
                # blob.upload_from_string(pickle.dumps(Data2save)) 
                # epriceTariffFile = blobname #uniqueid+'_epriceTariff.pkl'
                # entity.update({"EpriceTariffFile":epriceTariffFile})   


            elif 'supplier' in infoparams:
                supplier = infoparams['supplier']
                OutputDic.update({"supplier": supplier})
                if 'tariff' in infoparams:
                    if infoparams['tariff'] in BRAGet.tarifflist():
                        EpriceDay, pvfitsummary = BRAGet.DailyTariff(supplier, tariff=infoparams['tariff'])
                        OutputDic.update({"tariff": infoparams['tariff']})
                    else:
                        EpriceDay, pvfitsummary = BRAGet.DailyTariff(supplier)
                        OutputDic.update({"tariff": 'Residential - B1 - White'})
                else:
                    EpriceDay, pvfitsummary = BRAGet.DailyTariff(supplier)
                    OutputDic.update({"tariff": 'Residential - B1 - White'})
                state = BRAGet.suppliertostate(supplier)
                OutputDic.update({"state": state})
                statetax = BRAGet.Data['StateTaxes'].loc[state].values[0]

                pvfitsummary.update({"Epriceadjust": statetax,
                                     "pvtariffadjust": 0, "pvtarifftotal": 0})
                OutputDic.update({"PVTariffSummary": pvfitsummary})

                ForGraph = {"hours": [x for x in range(24)], "pvtariffWin": [0] * 24,
                            "pvtariffSum": [0] * 24}
                Etodisplay = {"Eprice": {"WetSeasonElecPrice": EpriceDay['EpriceWet'],
                                         "DrySeasonElecPrice": EpriceDay['EpriceDry']}}
                ForGraph.update(Etodisplay)
                OutputDic.update({"ValsForGraph": ForGraph})

                EpriceYearly = BRAGet.makeyearly(EpriceDay['EpriceWet'], supplier)
                OutputDic['PVTariffSummary']['Epricetotal'] = EpriceYearly.mean().values[0]
                OutputDic['PVTariffSummary']['EpriceHi'] = EpriceYearly.max().values[0]
                OutputDic['PVTariffSummary']['EpriceLow'] = EpriceYearly.min().values[0]
                OutputDic['PVTariffSummary']['nm'] = 1  # Net metering is  default in Brazil
                OutputDic['PVTariffSummary']['profiletype'] = profiletype
                YearlyTariff = pd.DataFrame(EpriceYearly, columns=['Eprice'])
                YearlyTariff['PVTariff'] = YearlyTariff['Eprice'] * 0
                # PVTariff = pd.DataFrame(EpriceYearly*0,columns=['PVTariff'])

                Data2save = {"Eprice": EpriceYearly, "YearlyTariff": YearlyTariff, "PVTariffSummary": pvfitsummary,
                             "supplier": supplier, "tariff": OutputDic['tariff'], "state": state,
                             "ValsForGraph": ForGraph,
                             "countrycode": country}

                if 'EpriceTariffFile' in entity:
                    blobname = entity['EpriceTariffFile']
                    datastored = pickle.loads(bucket.get_blob(blobname).download_as_string())
                    for vals in datastored:
                        if vals in Data2save:
                            datastored[vals] = Data2save[vals]
                    for vals in Data2save:
                        if vals not in datastored:
                            datastored[vals] = Data2save[vals]
                    bucket.get_blob(blobname).upload_from_string(pickle.dumps(datastored))
                else:
                    blobname = USER_UPLOADS + uniqueid + '_epriceTariff.pkl'
                    bucket.blob(blobname).upload_from_string(pickle.dumps(Data2save))
                    entity.update({"EpriceTariffFile": blobname})

                # blobname = USER_UPLOADS +uniqueid+'_epriceTariff.pkl'                   
                # blob=bucket.blob(blobname)
                # blob.upload_from_string(pickle.dumps(Data2save)) 
                # epriceTariffFile = blobname #uniqueid+'_epriceTariff.pkl'
                # entity.update({"EpriceTariffFile":epriceTariffFile})        
        elif country == 'CHE':
            if 'supplier' in infoparams:
                if 'PLZ' in entity['Location']:
                    PLZ = entity['Location']['PLZ']
                else:
                    PLZ = 8001
                # Get connection details to external API
                connection = CI['Data'][country]
                CT = Sup.CHTariffGetter(Data, connection, PLZ)
                OutputDic.update({'tarifflist': ['H1', 'H2', 'H3', 'H4', 'H5', 'H6', \
                                                 'H7', 'H8', 'C1', 'C2', 'C3', 'C4', 'C5', 'C6', 'C7'], "citylist": ""})
                if infoparams['supplier'] == 'all':
                    OutputDic.update({"supplierlist": CT.AllSuppliers})

                else:
                    supname = infoparams['supplier']  # search string
                    supdict = CT.supplierlist(supname)
                    if len(supdict) > 1:
                        OutputDic.update({'supplierlist': list(supdict.keys())})

                    elif len(supdict) != 0:
                        evuID = [*supdict.values()][0]
                        supplier = [*supdict.keys()][0]
                        DailyTariff, tariffsummary = CT.pvtariff(evuID)
                        YearlyTariff = CT.makeyearly(DailyTariff)
                        Eprice = CT.getEprice(evuID)

                        if 'tariff' in infoparams:
                            if infoparams['tariff'] in ['H1', 'H2', 'H3', 'H4', 'H5', 'H6', 'H7', 'H8', 'C1', 'C2',
                                                        'C3', 'C4', 'C5', 'C6', 'C7']:
                                profiletype = infoparams['tariff']
                            else:
                                profiletype = 'H5'

                            EpriceExtract = Eprice.loc[profiletype, 'Total'] if isinstance(
                                Eprice.loc[profiletype, 'Total'], float) else Eprice.loc[profiletype, 'Total'][0]
                            tariffsummary.update(
                                {"Epriceadjust": 0, "Epricetotal": EpriceExtract, "EpriceAvg": EpriceExtract,
                                 "pvtariffadjust": 0, "pvtarifftotal": 0})  # Must match manual upload objects
                            tariffsummary.update({"profiletype": infoparams['tariff']})
                        else:
                            profiletype = 'H5'
                            EpriceExtract = Eprice.loc[profiletype, 'Total'] if isinstance(
                                Eprice.loc[profiletype, 'Total'], float) else Eprice.loc[profiletype, 'Total'][0]
                            tariffsummary.update(
                                {"Epriceadjust": 0, "Epricetotal": EpriceExtract, "EpriceAvg": EpriceExtract,
                                 "pvtariffadjust": 0, "pvtarifftotal": 0})  # Must match manual upload objects
                            tariffsummary.update({"profiletype": infoparams['tariff']})
                        # DailyEPrice = (DailyTariff/DailyTariff.mean())*tariffsummary['EpriceAvg']
                        if (YearlyTariff['PVTariff'].mean() == 0):
                            YearlyTariff['Eprice'] = tariffsummary['EpriceAvg']
                        else:
                            YearlyTariff['Eprice'] = (YearlyTariff['PVTariff'] / (YearlyTariff['PVTariff'].mean())) * \
                                                     tariffsummary['EpriceAvg']
                        YearlyTariff.fillna(0, inplace=True)

                        tariffsummary.update(
                            {"EpriceHi": YearlyTariff['Eprice'].max(), "EpriceLow": YearlyTariff['Eprice'].min()})

                        if "error" in tariffsummary:
                            OutputDic.update({"error": tariffsummary['error']})

                        OutputDic.update({"PVTariffSummary": tariffsummary,
                                          "countrycode": country, "PLZ": PLZ, })
                        if type(DailyTariff) == str:
                            ForGraph = {"hours": [x for x in range(24)], "pvtariffWin": [0 for x in range(24)],
                                        "pvtariffSum": [0 for x in range(24)],
                                        "Eprice": [p for p in YearlyTariff['Eprice'].iloc[:24].values]}
                            # "EpriceAvg":[Eprice.loc['H5','Total'] for x in range(24)]}
                        else:
                            DailyTariff.fillna(0, inplace=True)
                            ForGraph = {"hours": [x for x in range(24)],
                                        "pvtariffWin": list(DailyTariff[('winter', 'pvtariff')].astype('float')),
                                        "pvtariffSum": list(DailyTariff[('summer', 'pvtariff')].astype('float')),
                                        "Eprice": [p for p in YearlyTariff['Eprice'].iloc[:24].values]}
                            #  "EpriceAvg":[Eprice.loc['H5','Total'] for x in range(24)]}
                        # Extraneous - only to maintain same keys as in manual
                        ForGraph.update({"pvtariffAvg": 0})
                        # Convert to CHF/kWh
                        YearlyTariff['Eprice'] = YearlyTariff['Eprice'] * 0.01
                        YearlyTariff['PVTariff'] = YearlyTariff['PVTariff'] * 0.01

                        Data2save = {"Eprice": Eprice, "YearlyTariff": YearlyTariff, "PVTariffSummary": tariffsummary,
                                     "evuID": evuID, "supplier": supplier, "ValsForGraph": ForGraph}
                        blobname = USER_UPLOADS + uniqueid + '_epriceTariff.pkl'
                        blob = bucket.blob(blobname)
                        blob.upload_from_string(pickle.dumps(Data2save))
                        epriceTariffFile = blobname  # uniqueid+'_epriceTariff.pkl'
                        entity.update({"EpriceTariffFile": epriceTariffFile})

                        OutputDic.update({"ValsForGraph": ForGraph})



    elif action == 'manual getdata':
        if 'countrycode' in entity['Location']:
            countrycode = entity['Location']['countrycode']
        else:
            countrycode = ''
        if 'PLZ' in entity['Location']:
            PLZ = entity['Location']['PLZ']
        else:
            PLZ = 0
        if 'EpriceTariffFile' in entity:
            # blobname=USER_UPLOADS +entity['EpriceTariffFile']                  
            blobname = entity['EpriceTariffFile']
            TariffDic = pickle.loads(bucket.get_blob(blobname).download_as_string())
            # blob=bucket.get_blob(blobname)
            # s = blob.download_as_string()
            # TariffDic = pickle.loads(s)

            OutputDic = TariffDic['PVTariffSummary']
            YearlyTariff = TariffDic['YearlyTariff']
            ForGraph = {"hours": [x for x in range(24)],
                        "pvtariff": [p for p in YearlyTariff['PVTariff'].iloc[:24].values],
                        "Eprice": [p for p in YearlyTariff['Eprice'].iloc[:24].values]}

            # if (('pvtariffadjust' in OutputDic) and ('Epriceadjust' in OutputDic)):
            #     ForGraph = {"hours":[x for x in range(24)],"pvtariff":[OutputDic['pvtariffAvg']*(1+OutputDic['pvtariffadjust']/100) for x in range(24)],
            #                      "EpriceAvg":[OutputDic['EpriceAvg']*(1+OutputDic['Epriceadjust']/100) for x in range(24)]}
            # else:
            #     ForGraph = {"hours":[x for x in range(24)],"pvtariff":[OutputDic['pvtariffAvg'] for x in range(24)],
            #                      "EpriceAvg":[OutputDic['EpriceAvg'] for x in range(24)]}
            # OutputDic.update({"pvtariffadjust":"","Epriceadjust":""})
            # Extraneous - only to maintain same keys as in auto
            ForGraph.update({"pvtariffWin": "", "pvtariffSum": ""})
            OutputDic.update({"ValsForGraph": ForGraph, "countrycode": countrycode, "PLZ": PLZ})

        else:
            return jsonify({"error": "no updated tariff and price data avaialble"}), 400

    elif action == 'manual upload':

        ManualPriceTariffKeys = ["pvtariffAvg", "pvtariffadjust", "pvtariffLoSum", "pvtariffHiSum",
                                 "pvtariffHiWin", "pvtariffLoWin", "pvtariffHi", "pvtariffLow",
                                 "EpriceAvg", "Epriceadjust", "EpriceHi", "EpriceLow",
                                 "nm", "capacitycharge"]
        NonNumericKeys = ["elecsupplier", "LinkToTariff", "profiletype"]
        if 'pricetariff' not in userinput:
            return jsonify({"error": "pricetariff not in request"})
        data = {}
        for mptkeys in ManualPriceTariffKeys:
            data[mptkeys] = userinput['pricetariff'].get(mptkeys, 0)
        for mptkeys in NonNumericKeys:
            data[mptkeys] = userinput['pricetariff'].get(mptkeys, '')

        """    
        if ('EpriceHi' in userinput['pricetariff']):
           EpriceHi = userinput['pricetariff']['EpriceHi']
        else:
           EpriceHi = data['EpriceAvg']
        if ('EpriceLow' in userinput['pricetariff']):
            EpriceLow = userinput['pricetariff']['EpriceLow']
        else:
            EpriceLow = data['EpriceAvg']
        highhours=[8,9,10,11,12,13,14,15,16,17,18,19,20,21]
        lowhours=[0,1,2,3,4,5,6,7,22,23]

        data['EpriceAvg'] = np.mean([EpriceHi,EpriceLow])

        if ('pvtariffHi' in userinput['pricetariff']):
           pvtariffHi = userinput['pricetariff']['pvtariffHi']
        else:
           pvtariffHi = data['pvtariffAvg']
        if ('pvtariffLow' in userinput['pricetariff']):
            pvtariffLow = userinput['pricetariff']['pvtariffLow']
        else:
            pvtariffLow = data['pvtariffAvg']

        EpriceHi = EpriceHi*(1+data['Epriceadjust']/100)
        EpriceLow = EpriceLow*(1+data['Epriceadjust']/100)   
        pvtariffHi = pvtariffHi*(1+data['pvtariffadjust']/100)
        pvtariffLow = pvtariffLow*(1+data['pvtariffadjust']/100)

        data['EpriceHi']=EpriceHi
        data['EpriceLow']=EpriceLow
        data['pvtarifftotal'] = data['pvtariffAvg']*(1+data['pvtariffadjust']/100)
        data['Epricetotal'] = data['EpriceAvg']*(1+data['Epriceadjust']/100)
        # Match automatic uploads
        AutoUploadkeys = ["pvtariffHiWin", "pvtariffHiSum","pvtariffLoWin", "pvtariffLoSum"]        

        for upk in AutoUploadkeys:
            if 'Hi' in upk:
                data[upk] =pvtariffHi
            if 'Lo' in upk:
                data[upk] = pvtariffLow   #data['pvtariffAvg']
        if 'LinkToTariff' in userinput['pricetariff']:
            data['LinkToTariff'] =   userinput['pricetariff']['LinkToTariff']
        else:
             data['LinkToTariff']=""
         """

        if 'EpriceTariffFile' in entity:
            # blobname=USER_UPLOADS+entity['EpriceTariffFile']
            blobname = entity['EpriceTariffFile']

            blob = bucket.get_blob(blobname)
            if blob:
                TariffDic = pickle.loads(blob.download_as_string())
            else:
                TariffDic = {"PVTariffSummary": {}}
                blob = bucket.blob(blobname)
        else:
            blobname = USER_UPLOADS + uniqueid + '_epriceTariff.pkl'
            blob = bucket.blob(blobname)
            TariffDic = {"PVTariffSummary": {}}

        for us in data:
            TariffDic['PVTariffSummary'][us] = data[us]

        year = entity['finparams'].get('Ref_year', str(dt.datetime.now().year))
        YearlyTariff = pd.DataFrame(index=pd.date_range(start='1/1/' + str(year), periods=8760, freq='1H'),
                                    columns=['PVTariff', 'Eprice'])
        # Make Yearly Data from High adn low
        country = entity['Location'].get('countrycode', 'CHE')
        if country == 'BRA':
            Multiplier = 1
        elif country == 'CHE':
            Multiplier = 0.01
            for k, v in data.items():
                if ((('Eprice' in k) or ('pvtariff' in k))):
                    data[k] = v * Multiplier
        else:
            Multiplier = 1

        YearlyTariff = Sup.CHTariffGetter.makeTariff(data, year=year)
        # All Yearly data in terms of Currency/kWh
        # Capacity Charge also in Currency/kW/month

        # YearlyTariff['PVTariff'] = data['pvtarifftotal']
        # for hihours in highhours:    
        #     YearlyTariff.loc[(YearlyTariff.index.hour == hihours),'Eprice'] =  EpriceHi*Multiplier
        #     YearlyTariff.loc[(YearlyTariff.index.hour == hihours),'PVTariff'] =  pvtariffHi*Multiplier
        # for lohours in lowhours:       
        #     YearlyTariff.loc[(YearlyTariff.index.hour == lohours),'Eprice']= EpriceLow*Multiplier
        #     YearlyTariff.loc[(YearlyTariff.index.hour == lohours),'PVTariff'] =  pvtariffLow*Multiplier

        TariffDic.update({"YearlyTariff": YearlyTariff})
        # ForGraph = {"hours":[x for x in range(24)],"pvtariff":[TariffDic['PVTariffSummary']['pvtariffAvg']*(1+TariffDic['PVTariffSummary']['pvtariffadjust']/100) for x in range(24)],
        #                      "EpriceAvg":[TariffDic['PVTariffSummary']['EpriceAvg']*(1+TariffDic['PVTariffSummary']['Epriceadjust']/100) for x in range(24)]}
        ForGraph = {"hours": [x for x in range(24)],
                    "pvtariff": [p for p in YearlyTariff['PVTariff'].iloc[:24].values / Multiplier],
                    "Eprice": [p for p in YearlyTariff['Eprice'].iloc[:24].values / Multiplier]}

        TariffDic.update({"ValsForGraph": ForGraph})

        blob = bucket.blob(blobname)
        blob.upload_from_string(pickle.dumps(TariffDic))
        entity.update({"EpriceTariffFile": blobname})
        # else:
        #     blobname=USER_UPLOADS + uniqueid+'_epriceTariff.pkl'
        #     blob = bucket.blob(blobname)
        #     TariffDic = {"PVTariffSummary":{}}

        #     for us in data:     
        #         TariffDic['PVTariffSummary'][us] = data[us]
        #     year = "2021"    
        #     YearlyTariff = pd.DataFrame(index=pd.date_range(start='1/1/'+year,periods=8760,freq='1H'),columns=['PVTariff'])
        #     YearlyTariff['PVTariff']=data['pvtarifftotal']
        #     TariffDic.update({"YearlyTariff":YearlyTariff})
        #     ForGraph = {"hours":[x for x in range(24)],"pvtariff":[TariffDic['PVTariffSummary']['pvtariffAvg']*(1+TariffDic['PVTariffSummary']['pvtariffadjust']/100) for x in range(24)],
        #                          "EpriceAvg":[TariffDic['PVTariffSummary']['EpriceAvg']*(1+TariffDic['PVTariffSummary']['Epriceadjust']/100) for x in range(24)]}

        #     TariffDic.update({"ValsForGraph":ForGraph})     

        #     blob.upload_from_string(pickle.dumps(TariffDic))
        #     #entity.update({"EpriceTariffFile":uniqueid+'_epriceTariff.pkl'})
        #     entity.update({"EpriceTariffFile":blobname})

        ds.put(entity)
        return jsonify({"success": "manual tariff values uploaded", "ValsForGraph": ForGraph}), 200

    entity.update({"status": "Tariff Details"})
    ds.put(entity)

    return jsonify(OutputDic), 200


@app.route('/bom', methods=['POST'])
@jwt_authenticated
def bom():
    OutputDic = {}
    # read entry

    ds = datastore.Client()
    gcs = storage.Client()
    bucket = gcs.get_bucket(BUCKET_NAME)
    req_json = request.get_json()['BoM']

    userinput = req_json
    userinput['email'] = request.email
    orgname = request.org
    parent_key = ds.key('users', userinput['email'])
    message = {}

    entity = ds.get(key=ds.key('Project', userinput['project_id'], parent=parent_key))
    if not entity:
        message.update({"error": "No project with username {} and project {} exists".format(userinput['email'],
                                                                                            userinput['project_id'])})
        return jsonify(message), 400
    ######## CHANGE USERS DB ###########

    # qur = ds.query(kind='organisation')
    # qur.add_filter('users','=',userinput['email'])
    # Sol=list(qur.fetch())
    # if Sol != []:
    #     if os.environ['DB_NAME']!=Sol[0]['compdb']:
    #         os.environ['DB_NAME']= Sol[0]['compdb']
    #     orgname =  Sol[0].key.name 
    # else:        
    #     os.environ['DB_NAME'] = 'refdata' 
    #     orgname = 'default'
    orgentity = ds.get(key=ds.key('organisation', orgname))
    os.environ['DB_NAME'] = orgentity.get("compdb", "refdata")
    # Organisation specific storage areas
    OrgUID = "OrgUID_" + orgname + "_"
    uniqueid = "Uid_" + userinput['email'] + "_" + "Pid" + "_" + userinput['project_id'] + "_"

    importlib.reload(CDB)
    OrgInfo = json.loads(bucket.get_blob(AUXFILES + "organisation_info.json").download_as_string())
    # OrgDict = OrgInfo.get(orgname,OrgInfo['agrola'])
    OrgDict = {"bominput": orgentity.get('bominput', OrgInfo['agrola'])}
    OrgBomInput = OrgDict.get('bominput', {})
    # orgentity = ds.get(key=ds.key('organisation',orgname))

    uschklist = ['action']
    for us in uschklist:
        if us not in userinput:
            return jsonify({"error": "{} not set in input request".format(us)}), 400
    entchklist = ['Demand', 'Location']
    for ent in entchklist:
        if ent not in entity:
            return jsonify({"error": "{} not set in datastore".format(ent)}), 400

    params = userinput['params']
    if userinput[
        'action'] == "getentry":  # Get values from component database based on searching the values of an attribute
        paramchk = ['tablename', 'attribute', 'search']
        for pars in params:
            if pars not in paramchk:
                return jsonify({"error": "{} not set in params".format(pars)}), 400
        Df = CDB.GetEntry(params['tablename'], params['attribute'],
                          params['search'])  # Get values from Component database

        ########## Drop Inverter Sizes Above the kW ############
        Df.loc[~Df['PerfModelKey'].isnull(), 'PerfModelKey'] = Df.loc[
            ~Df['PerfModelKey'].isnull(), 'PerfModelKey'].apply(lambda x: x.replace('\ufeff', '')).values
        kW = entity['Demand'].get('ACpower', 0)
        inverterlist = Df.loc[Df['PerfModelKey'].apply(lambda x: x in INVDF.columns), 'PerfModelKey'].values
        if len(inverterlist) > 0:
            for invname in inverterlist:
                if INVDF[invname].Paco / 1000 > kW:
                    Df.drop(labels=Df[Df['PerfModelKey'] == invname].index, inplace=True)
        ##### ONLY FOR INVERTER FILTERING HERE #########################3
        Df.fillna(np.nan, inplace=True)
        Df.replace([np.nan], [None], inplace=True)
        OutputDic.update(Df[['Name', 'Cost', 'Currency', 'PerfModelKey', 'Description', 'SystemType', 'Id']].to_dict())
    elif userinput['action'] == 'getentrytemplate':
        paramchk = ['ProductName', 'GroupName', 'search']
        for pars in params:
            if pars not in paramchk:
                return jsonify({"error": "{} not set in params".format(pars)}), 400
        productname = params['ProductName']
        groupname = params['GroupName']
        searchstr = params['search']
        TempDf = CDB.GetQuotefromtemplate(productname)

        ########## Drop Inverter Sizes Above the kW ############
        TempDf.loc[~TempDf['PerfModelKey'].isnull(), 'PerfModelKey'] = TempDf.loc[
            ~TempDf['PerfModelKey'].isnull(), 'PerfModelKey'].apply(lambda x: x.replace('\ufeff', '')).values
        kW = entity['Demand'].get('ACpower', 0)
        inverterlist = TempDf.loc[TempDf['PerfModelKey'].apply(lambda x: x in INVDF.columns), 'PerfModelKey'].values
        if len(inverterlist) > 0:
            for invname in inverterlist:
                if INVDF[invname].Paco / 1000 > kW:
                    TempDf.drop(labels=TempDf[TempDf['PerfModelKey'] == invname].index, inplace=True)
        ##### ONLY FOR INVERTER FILTERING HERE #########################3

        TempDf['ProjectID'] = userinput['project_id']
        TempDf.rename(columns={"GroupName": "Group"}, inplace=True)
        # make regex parameter

        searchstr = searchstr.replace("%", ".*")

        return jsonify(
            TempDf.loc[(TempDf['Group'] == groupname) & (TempDf['Name'].str.contains(searchstr, case=False)), ["Name",
                                                                                                               "Group",
                                                                                                               "Currency",
                                                                                                               "Cost",
                                                                                                               "Unit",
                                                                                                               "Id",
                                                                                                               "PriceAdjustment"]].to_dict()), 200

    elif userinput['action'] == "addentry":
        # if 'quoteparams' not in userinput:
        #     return jsonify({"error": "Quote and Rev Ids missing"}),400

        if 'filename' in params:
            if orgentity.get('admins'):
                if userinput['email'] not in orgentity['admins']:
                    return jsonify({"error": "Unauthorized - only admins may upload"}), 400
            fname = params['filename']
            bucket.get_blob(BOM + fname).download_to_filename(fname)
            if fname.endswith('xlsx'):
                entrydf = pd.read_excel(fname, engine='openpyxl')
            elif fname.endswith('csv'):
                entrydf = pd.read_csv(fname)
            elif fname.endswith('json'):
                entrydf = pd.read_json(fname, orient='split')
            else:
                return jsonify({"error": "filename {} format invalid".format(fname)}), 400
            os.remove(fname)
            entrydf.drop(labels=entrydf[entrydf['Name'].isnull()].index, axis=0, inplace=True)
            entrydf.fillna(np.nan, inplace=True)
            entrydf.replace([np.nan], [None], inplace=True)
        else:
            # Allow upload of raw json files
            entrydf = pd.DataFrame(params).transpose()
            entrydf.fillna(np.nan, inplace=True)
            entrydf.replace([np.nan], [None], inplace=True)
            # return jsonify({"error":"upload filename missing"}),400
        Id = CDB.AddEntry(entrydf)
        OutputDic.update({"Id": Id})
        # OutputDic.update(entrydf[['Name','Value','Currency','PerfModelKey','Description','Category']].to_dict())
    elif userinput['action'] == "addtotemplates":

        if orgentity.get('admins'):
            if userinput['email'] not in orgentity['admins']:
                return jsonify({"error": "Unauthorized - only admins may upload"}), 400
        if 'filename' in params:
            fname = params['filename']
            bucket.get_blob(BOM + fname).download_to_filename(fname)
            ProdDb = pd.DataFrame()
            BigDb = pd.DataFrame()

            if fname.endswith('xlsx'):
                Raw = pd.read_excel(fname, skiprows=1, engine='openpyxl')
                ProdNames = pd.read_excel(fname, nrows=0, engine='openpyxl')
                Names = [n for n in ProdNames.columns if 'Unnamed' not in n]
                for Name, colnum in zip(Names, range(6, len(Names) * 3 + 6, 3)):
                    ProdDb = Raw.iloc[:, :4]
                    ProdDb['ProductName'] = Name
                    ProdDb.loc[:, 'ActiveFlag'] = Raw.iloc[:, colnum]
                    ProdDb.loc[:, 'PriorityFlag'] = Raw.iloc[:, colnum + 2]
                    ProdDb.loc[:, 'PriceAdjustment'] = Raw.iloc[:, colnum + 1]
                    id2drop = ProdDb[ProdDb['BusinessIdentifierCode'].isnull()].index
                    ProdDb.drop(index=id2drop, inplace=True)
                    BigDb = pd.concat([BigDb, ProdDb], axis=0)
                BigDb.fillna(0, inplace=True)
            elif fname.endswith('csv'):
                entrydf = pd.read_csv(fname)
            # elif fname.endswith('json'):
            #     entrydf = pd.read_json(fname,orient='split')
            else:
                return jsonify({"error": "filename {} format invalid".format(fname)}), 400
            os.remove(fname)
            entrydf = BigDb

            entrydf.drop(labels=entrydf[entrydf['BusinessIdentifierCode'].isnull()].index, axis=0, inplace=True)
            entrydf.fillna(np.nan, inplace=True)
            entrydf.replace([np.nan], [None], inplace=True)
            # Add ActiveFlag for templates
            EntDict = entrydf[['ProductName', 'GroupName', 'BusinessIdentifierCode',
                               'PriceAdjustment', 'PriorityFlag', 'ActiveFlag']].to_dict(orient='records')
            CDB.InsertOrUpdate(EntDict)

        else:
            return jsonify({"error": "no filename in params"}), 400
    elif userinput["action"] == "getexcel":
        # Check if restricted organisation where downloads are also not allowed
        if orgentity.get('displaypermissions'):  # CHECK - if 'Cost and Price Overview' in user access rights
            if orgentity.get('admins'):
                if userinput['email'] not in orgentity['admins']:
                    return jsonify({"error": "Unauthorized - only admins may download"}), 400
        if 'report' in params:
            report = params['report']
        else:
            report = "SYSTEM"
        if report == "SYSTEM":
            # querytext = "SELECT * FROM SYSTEM WHERE ActiveFlag=1"
            querytext = "SELECT sy0.* FROM SYSTEM sy0 \
                            RIGHT JOIN  \
                        (SELECT MAX(s.Id) as MaxId, s.BusinessIdentifierCode as BC \
                         FROM SYSTEM s GROUP BY BC) as p \
                        ON p.MaxId = sy0.Id  \
                        WHERE p.BC is not null \
                        ORDER by sy0.BusinessIdentifierCode"

            Df2Excel = CDB.DfFromRawSQLQuery(querytext)
            # Df2Excel.sort_values(by='BusinessIdentifierCode',inplace=True)
            Df2Excel['ActiveFlag'] = Df2Excel['ActiveFlag'].apply(lambda x: int.from_bytes(x, byteorder='big'))
            # localfname = "MasterListExtract.xlsx"
            fnameorder = "MasterListExtract.xlsx"
            savefilename = "Uid_" + userinput['email'] + "_MasterListExtract.xlsx"
            blobfname = "BoM/ExcelExports/" + savefilename
            cwd = os.getcwd()

            try:
                with open(os.path.join(cwd, fnameorder), "wb") as f:
                    bucket.get_blob(blobfname.replace(savefilename, fnameorder)).download_to_file(f)
            except:
                return jsonify({"err": blobfname.replace(savefilename, fnameorder)}), 400
            # Organise columns in order from previous stored file
            # Organise columns in this order first
            oldfile = pd.read_excel(os.path.join(cwd, fnameorder), nrows=2, engine='openpyxl')
            columnorder = oldfile.columns
            Df2Excel1 = Df2Excel.reindex(columns=columnorder)
            # with open(os.path.join(cwd,localfname),"wb") as f:
            #     Df2Excel1.to_excel(f,index=False)  
            writer = pd.ExcelWriter(os.path.join(cwd, savefilename), engine='openpyxl')
            Df2Excel1.to_excel(writer, index=False)
            writer.save()
            with open(os.path.join(cwd, savefilename), "rb") as f:
                bucket.blob(blobfname).upload_from_file(f)
            if os.path.exists(os.path.join(cwd, savefilename)):
                os.remove(os.path.join(cwd, savefilename))
            urltodownload = "https://www.googleapis.com/storage/v1/b/" + BUCKET_NAME + "/o/" + \
                            blobfname.replace('/', '%2F') + "?alt=media"

        elif report == 'PRODUCT_GROUP':
            querytext = "SELECT \
                            pd.*, \
                            s.Name \
                            FROM  PRODUCT_GROUP pd \
                            INNER JOIN SYSTEM s	on pd.BusinessIdentifierCode = s.BusinessIdentifierCode \
                            AND s.ActiveFlag=1 AND pd.ActiveFlag=1"

            Df2Excel = CDB.DfFromRawSQLQuery(querytext)
            Df2Excel.sort_values(by=['ProductName', 'GroupName', 'Name'], inplace=True)
            # Df2Excel['ActiveFlag']=Df2Excel['ActiveFlag'].apply(lambda x:int.from_bytes(x,byteorder='big'))

            Df2Excel = Df2Excel[
                ['ProductName', 'GroupName', 'BusinessIdentifierCode', 'Name', 'PriceAdjustment', 'PriorityFlag',
                 'ActiveFlag']]
            productnames = set(Df2Excel['ProductName'].values)
            localfname = "{}_ProductListExtract.xlsx".format(uniqueid)

            blobfname = "BoM/ExcelExports/" + localfname
            cwd = os.getcwd()
            # Write the file
            writer = pd.ExcelWriter(os.path.join(cwd, localfname), engine='openpyxl')
            for prodnames in productnames:
                Df2Excel[Df2Excel['ProductName'] == prodnames].to_excel(writer, sheet_name=prodnames, index=False)
            writer.save()
            # Upload to bucket
            with open(os.path.join(cwd, localfname), "rb") as f:
                bucket.blob(blobfname).upload_from_file(f)
            if os.path.exists(os.path.join(cwd, localfname)):
                os.remove(os.path.join(cwd, localfname))
            urltodownload = "https://www.googleapis.com/storage/v1/b/" + BUCKET_NAME + "/o/" + \
                            blobfname.replace('/', '%2F') + "?alt=media"

        elif report == 'BoMQuote':
            QuoteID = params['QuoteID']
            RevID = params['RevID']
            ProjectID = userinput['project_id']
            ViewQuoteDf = CDB.GetBoMQuote(ProjectID, QuoteID, RevID)

            ViewQuoteDf.fillna(np.nan, inplace=True)
            ViewQuoteDf.replace([np.nan], [None], inplace=True)
            ViewQuoteDf['Quantity'] = ViewQuoteDf['Quantity'].apply(lambda x: float(x) if x else x)
            ViewQuoteDf['ActiveFlag'] = ViewQuoteDf['ActiveFlag'].apply(lambda x: int.from_bytes(x, byteorder='big'))
            ViewQuoteDf.fillna(np.nan, inplace=True)
            ViewQuoteDf.replace([np.nan], [None], inplace=True)
            ToExcel = ViewQuoteDf[(ViewQuoteDf['Quantity'] != 0) & (~ViewQuoteDf['Quantity'].isnull())]

            localfname = "{}_BoMQuoteExtract.xlsx".format(uniqueid)
            blobfname = "BoM/" + "Quotes/" + localfname
            cwd = os.getcwd()
            with open(os.path.join(cwd, localfname), "wb") as f:
                ToExcel.to_excel(f, index=False)
            with open(os.path.join(cwd, localfname), "rb") as f:
                bucket.blob(blobfname).upload_from_file(f)
            if os.path.exists(os.path.join(cwd, localfname)):
                os.remove(os.path.join(cwd, localfname))
            urltodownload = "https://www.googleapis.com/storage/v1/b/" + BUCKET_NAME + "/o/" + \
                            blobfname.replace('/', '%2F') + "?alt=media"
        elif report == 'custom':
            import addonModules.custommods.ExcelExports as CExl
            customreport = params['customreport']
            QuoteID = params['QuoteID']
            RevID = params['RevID']
            ProjectID = userinput['project_id']
            ExcelReport = CExl.ExcelReport(CDB)
            df = getattr(ExcelReport, customreport)(ProjectID, QuoteID, RevID)

            localfname = customreport + "_.xlsx"
            blobfname = "BoM/" + "Quotes/" + uniqueid + localfname
            urltodownload = ExcelReport.excel2download(df, bucket, blobfname)

        return jsonify({"urltodownload": urltodownload})



    elif userinput["action"] == "getinputfilters":
        # OrgDict=json.loads(bucket.get_blob(AUXFILES+"organisation_info.json").download_as_string())
        # s=re.compile("(?<=@)[^.]+(?=\.)")
        # userstring = s.findall(userinput['email'])
        # # TODO: Domain from org directory
        # if len(userstring)>0:
        #     domain = userstring[0]
        # else:
        #     domain=""
        with ds.transaction():
            orgentity = ds.get(key=ds.key('organisation', orgname))
            parent_key = ds.key('users', userinput['email'])
            entity = ds.get(key=ds.key('Project', userinput['project_id'], parent=parent_key))
            # TODO: TEMPORARY, CHANGE to OrgDict[domain]
            tiltareas = entity['Location']['tilt']
            flatcheck = [t for t in tiltareas.values() if t != 0]  # list of non-flat roofs
            if len(flatcheck) == 0:
                rooftilt = 'flat'
            else:
                rooftilt = 'nonflat'
            OrgDict = {"bominput": orgentity.get('bominput', OrgInfo['agrola'])}

            # if orgname in OrgDict:
            OutputDic = OrgDict['bominput']
            if 'roof' in OutputDic:
                OutputDic.update({"rooftilt": OutputDic['roof'][rooftilt]['rooftilt']})
                OutputDic.update({"rooftype": OutputDic['roof'][rooftilt]['rooftype']})

            if "groupfilters" in OrgDict:
                OutputDic.update({"groupfilters": OrgDict['groupfilters']})
            # else:
            #     OutputDic=OrgDict['agrola']['bominput']
            #     OutputDic.update({"rooftilt": OutputDic['roof'][rooftilt]['rooftilt']})
            #     OutputDic.update({"rooftype": OutputDic['roof'][rooftilt]['rooftype']})
            #     if "groupfilters" in OrgDict['agrola']:
            #         OutputDic.update({"groupfilters":OrgDict['Agrola']['groupfilters']})

            if 'ActiveQuote' in entity:
                if entity['ActiveQuote'].get('QuoteID'):
                    OutputDic.update({"QuoteID": entity['ActiveQuote'].get('QuoteID')})

                else:
                    NewQuoteID = int(orgentity['bominput'].get('QuoteAutoIncUID', '100000')) + 1
                    orgentity['bominput'].update({'QuoteAutoIncUID': NewQuoteID})

                    OutputDic.update({"QuoteID": NewQuoteID})
            else:
                NewQuoteID = int(orgentity['bominput'].get('QuoteAutoIncUID', '100000')) + 1
                orgentity['bominput'].update({'QuoteAutoIncUID': NewQuoteID})
                OutputDic.update({"QuoteID": NewQuoteID})
            ds.put(orgentity)

        return jsonify({"success": OutputDic}), 200




    elif userinput['action'] == "makebom":

        ProjectID = userinput['project_id']
        # TODO: Generate Quote and Revision ID
        QuoteID = userinput['quoteparams']['QuoteID']
        RevID = userinput['quoteparams']['RevID']
        if entity.get('ActiveQuote'):
            entity['ActiveQuote'].update({"QuoteID": QuoteID})
            entity['ActiveQuote'].update({"RevID": RevID})
        else:
            entity.update({'ActiveQuote': {"QuoteID": QuoteID, "RevID": RevID}})

        bomquotedic = params
        # Make bomquote into parameterdf['ProjectID','QuoteID','RevID','Type','CreatedBy','Group','TypeID']
        parameterdf = pd.DataFrame(columns=['ProjectID', 'QuoteID', 'RevID', 'Type', 'CreatedBy', 'Group', 'TypeID'])
        # parameterdf.loc[range(np.sum([len(i) for i in bomquotedic.values())]),
        #                 ["ProjectID","QuoteID","RevID","CreatedBy"]] = [ProjectID,QuoteID,RevID,userinput['email']]
        parameterdf = parameterdf.loc[~parameterdf.duplicated(subset=['TypeID'])]

        i = 0
        for groups in bomquotedic:
            for entries in bomquotedic[groups]:
                parameterdf.loc[i, ["ProjectID", "QuoteID", "RevID", "CreatedBy"]] = [ProjectID, QuoteID, RevID,
                                                                                      userinput['email']]
                parameterdf.loc[i, ['Group', 'Type', 'TypeID']] = [groups, entries["tablename"], int(entries['Id'])]
                if 'PriceAdjustment' in entries:  # Remove condition later
                    parameterdf.loc[i, ['PriceAdjustment']] = entries['PriceAdjustment']

                i += 1
        parameterdf.fillna(np.nan, inplace=True)
        parameterdf.replace([np.nan], [None], inplace=True)

        CDB.MakeBoMQuote(parameterdf)
        OutputDic.update((parameterdf.to_dict()))
        entity.update({"status": "Components"})

    elif userinput['action'] == "viewbom":
        ProjectID = userinput['project_id']
        if "QuoteID" in params:
            QuoteID = params['QuoteID']
            RevID = params['RevID']
            if entity.get('ActiveQuote'):
                entity['ActiveQuote'].update({"QuoteID": QuoteID})
                entity['ActiveQuote'].update({"RevID": RevID})

            ViewQuoteDf = CDB.GetBoMQuote(ProjectID, QuoteID, RevID)

            ViewQuoteDf.fillna(np.nan, inplace=True)
            ViewQuoteDf.replace([np.nan], [None], inplace=True)

            ViewQuoteDf['Quantity'] = ViewQuoteDf['Quantity'].apply(lambda x: float(x) if x else x)

            ViewQuoteDf.fillna(np.nan, inplace=True)
            ViewQuoteDf.replace([np.nan], [None], inplace=True)

            ViewQuoteDf = CDB.BoMExcludeFlags(ViewQuoteDf, OrgBomInput=OrgBomInput)

            OutputDic.update(json.loads(json.dumps(ViewQuoteDf.loc[(ViewQuoteDf['Quantity'] != 0),
                                                                   ["ProjectID", "QuoteID", "RevID", "Name",
                                                                    "DisplayName", "Description",
                                                                    "Group", "Currency", "Cost", "Unit", "ID", "Id",
                                                                    "Quantity", "TotalCost", "UnitCost", "TotalPrice",
                                                                    "PriceAdjustment", "NetPrice", "BasePrice",
                                                                    "TotalBasePrice",
                                                                    "ExcludeFromCAPEX", "IncludeforBC"]].to_dict())))


        elif "fromtemplate" in params:

            productname = params['fromtemplate']
            if 'ActiveQuote' in entity:
                entity['ActiveQuote'].update({"ProductName": productname})
            else:
                entity.update({"ActiveQuote": {"ProductName": productname}})
            ViewQuoteDf = CDB.GetQuotefromtemplate(productname)
            ViewQuoteDf['Quantity'] = None
            ViewQuoteDf['TotalCost'] = None
            ViewQuoteDf['ProjectID'] = userinput['project_id']
            ViewQuoteDf.rename(columns={"GroupName": "Group"}, inplace=True)
            ViewQuoteDf['Quantity'] = ViewQuoteDf['Quantity'].apply(lambda x: int(x) if x else x)
            # Eliminate items based on kW or segment selection 
            # Calculate Quantities if information is available
            unitvariable = 'UnitDependantVariable'
            unitfunction = 'UnitDependantVariableFunction'
            costfunction = 'CostFunction'
            costvariable = 'CostFunctionDependentVariable'
            if 'ACpower' in entity['Demand']:
                kW = entity['Demand']['ACpower']
            else:
                kW = 0
            # Roof Flat or Tilt
            tilted = 0
            for ar in entity['Location']['tilt']:
                if entity['Location']['tilt'][ar] != 0:
                    tilted = 1
            if 'AreaFigures' in entity['Location']:
                if 'TotalDCInOut' in entity['Location']['AreaFigures']:
                    TotalDCInOut = entity['Location']['AreaFigures']['TotalDCInOut']
                else:
                    TotalDCInOut = 5
            else:
                TotalDCInOut = 5
            DependantVar = {"kW": kW, "tilt": tilted, "TotalDCInOut": TotalDCInOut}
            ns = vars(math).copy()
            xvars = {}
            for row in ViewQuoteDf.index:
                if ViewQuoteDf.loc[row, unitvariable]:
                    UnitVariables = ViewQuoteDf.loc[row, unitvariable].split(",")
                    # an equation can be made of x, x1, x2.... etc
                    for Vars, num in zip(UnitVariables, range(len(UnitVariables))):
                        if num == 0:
                            if Vars.strip() in DependantVar:
                                xvars["x"] = DependantVar[Vars.strip()]
                            else:
                                xvars["x"] = np.nan
                                ViewQuoteDf.loc[row, unitfunction] = "x"
                        else:
                            if Vars.strip() in DependantVar:
                                xvars["x" + str(num)] = DependantVar[Vars.strip()]
                            else:
                                xvars["x" + str(num)] = np.nan
                                ViewQuoteDf.loc[row, unitfunction] = "x" + str(num)

                    evalstr = str(ViewQuoteDf.loc[row, unitfunction])

                    ViewQuoteDf.loc[row, 'Quantity'] = eval(evalstr, ns, xvars)
                    ViewQuoteDf.loc[row, 'TotalCost'] = ViewQuoteDf.loc[row, 'Quantity']

            ViewQuoteDf.fillna(np.nan, inplace=True)
            ViewQuoteDf.replace([np.nan], [None], inplace=True)

            # Get Filters

            # OrgDict=json.loads(bucket.get_blob(AUXFILES+"organisation_info.json").download_as_string())
            s = re.compile("(?<=@)[^.]+(?=\.)")
            userstring = s.findall(userinput['email'])

            if len(userstring) > 0:
                domain = userstring[0]
            else:
                domain = ""

            # TODO: TEMPORARY, CHANGE to OrgDict[domain]
            tiltareas = entity['Location']['tilt']
            flatcheck = [t for t in tiltareas.values() if t != 0]  # list of non-flat roofs
            if len(flatcheck) == 0:
                rooftilt = 'flat'
            else:
                rooftilt = 'nonflat'

            if 'bominputfilter' in entity:
                bominputfilter = entity['bominputfilter']
            else:
                bominputfilter = {}

            if 'groupfilters' in OrgDict['bominput']:
                groupfilters = OrgDict['bominput']['groupfilters']
                for groupname in groupfilters.keys():
                    searchstring = ".*".join(
                        [bominputfilter[x] for x in OrgDict['bominput']['groupfilters'][groupname] if
                         x in bominputfilter])  # Regex structure to allow string searches
                    if searchstring:
                        ViewQuoteDf.loc[(ViewQuoteDf['Group'] == groupname) & (
                            ViewQuoteDf['Name'].str.contains(".*" + searchstring + ".*")), ['PriorityFlag']] = "1"

            """ if domain in OrgDict:
                if "groupfilters" in OrgDict['Agrola']:
                    groupfilters = OrgDict['Agrola']['groupfilters']
                    for groupname in groupfilters.keys():
                        searchstring = ".*".join([bominputfilter[x] for x in OrgDict['Agrola']['groupfilters'][groupname] if x in bominputfilter]) # Regex structure to allow string searches
                        if searchstring:
                            ViewQuoteDf.loc[(ViewQuoteDf['Group']==groupname)&(ViewQuoteDf['Name'].str.contains(".*"+searchstring+".*")),['PriorityFlag']]="1"   
            else:
               if "groupfilters" in OrgDict['Agrola']:
                    groupfilters = OrgDict['Agrola']['groupfilters']
                    for groupname in groupfilters.keys():
                        searchstring = ".*".join([bominputfilter[x] for x in OrgDict['Agrola']['groupfilters'][groupname] if x in bominputfilter]) # Regex structure to allow string searches
                        if searchstring:
                            ViewQuoteDf.loc[(ViewQuoteDf['Group']==groupname)&(ViewQuoteDf['Name'].str.contains(".*"+searchstring+".*")),['PriorityFlag']]="1" """

            ViewQuoteDf.loc[ViewQuoteDf['Quantity'] > 0, 'PriorityFlag'] = "1"
            # Add Lookup for System-Accessory Pairs here, set those to PriorityFlag

            # System: Inverter
            # Remove those values above the kW                           
            ########## Drop Inverter Sizes 80% Above the selected kW ############
            kW = entity['Demand'].get('ACpower', 0) / 0.8
            ViewQuoteDf.loc[~ViewQuoteDf['PerfModelKey'].isnull(), 'PerfModelKey'] = ViewQuoteDf.loc[
                ~ViewQuoteDf['PerfModelKey'].isnull(), 'PerfModelKey'].apply(lambda x: x.replace('\ufeff', '')).values
            inverterlist = ViewQuoteDf.loc[
                ViewQuoteDf['PerfModelKey'].apply(lambda x: x in INVDF.columns), 'PerfModelKey'].values
            if len(inverterlist) > 0:
                for invname in inverterlist:
                    if INVDF[invname].Paco / 1000 > kW:
                        ViewQuoteDf.drop(labels=ViewQuoteDf[ViewQuoteDf['PerfModelKey'] == invname].index, inplace=True)
            ##########################################################

            ViewQuoteDf = CDB.BoMExcludeFlags(ViewQuoteDf, OrgBomInput=OrgBomInput)

            OutputDic.update(json.loads(json.dumps(ViewQuoteDf.loc[(ViewQuoteDf['Quantity'] != 0),
                                                                   ["ProjectID", "Name", "DisplayName", "Description",
                                                                    "Group", "Currency", "Cost", "Unit", "Id",
                                                                    "Quantity", "TotalCost", "PriorityFlag",
                                                                    "PriceAdjustment",
                                                                    "ExcludeFromCAPEX", "IncludeforBC"]].to_dict())))
            entity.update({"status": "Components"})
    elif userinput['action'] == 'updatebom':
        ProjectID = userinput['project_id']
        if 'NewRevID' in params:
            QuoteID = params['QuoteID']
            RevID = params['RevID']
            if entity.get('ActiveQuote'):
                entity['ActiveQuote'].update({"QuoteID": QuoteID})
                entity['ActiveQuote'].update({"RevID": RevID})
            else:
                entity.update({'ActiveQuote': {"QuoteID": QuoteID, "RevID": RevID}})

            ViewQuoteDf = CDB.StoreRevtoBoM(ProjectID, QuoteID, RevID, userinput['email'], params['NewRevID'])
            ViewQuoteDf.fillna(np.nan, inplace=True)
            ViewQuoteDf.replace([np.nan], [None], inplace=True)
            ViewQuoteDf = CDB.BoMExcludeFlags(ViewQuoteDf, OrgBomInput=OrgBomInput)
            OutputDic.update(json.loads(json.dumps(ViewQuoteDf.loc[(ViewQuoteDf['Quantity'] != 0),
                                                                   ["ProjectID", "QuoteID", "RevID", "Name",
                                                                    "DisplayName", "Description",
                                                                    "Group", "Currency", "Cost", "Unit", "ID", "Id",
                                                                    "Quantity", "TotalCost", "UnitCost", "TotalCost",
                                                                    "TotalPrice", "BasePrice",
                                                                    "PriceAdjustment", "NetPrice", "TotalBasePrice",
                                                                    "ExcludeFromCAPEX", "IncludeforBC"]].to_dict())))
            entity['ActiveQuote'].update({"RevID": params['NewRevID']})
            entity.update({"status": "Cost & Price Overview"})
        elif 'quoteparams' in userinput:
            # Fetch existing BoM
            QuoteID = userinput['quoteparams']['QuoteID']
            RevID = userinput['quoteparams']['RevID']
            if entity.get('ActiveQuote'):
                entity['ActiveQuote'].update({"QuoteID": QuoteID})
                entity['ActiveQuote'].update({"RevID": RevID})
            else:
                entity.update({'ActiveQuote': {"QuoteID": QuoteID, "RevID": RevID}})
            ViewCurrBoMDf = CDB.GetBoMQuote(ProjectID, QuoteID, RevID)

            bomquotedic = params
            # Make bomquote into parameterdf['ProjectID','QuoteID','RevID','Type','CreatedBy','Group','TypeID']
            parameterdf = pd.DataFrame(
                columns=['ProjectID', 'QuoteID', 'RevID', 'Type', 'CreatedBy', 'Group', 'TypeID'])
            # parameterdf.loc[range(np.sum([len(i) for i in bomquotedic.values())]),
            #                 ["ProjectID","QuoteID","RevID","CreatedBy"]] = [ProjectID,QuoteID,RevID,userinput['email']]

            i = 0
            for groups in bomquotedic:
                for entries in bomquotedic[groups]:
                    parameterdf.loc[i, ["ProjectID", "QuoteID", "RevID", "CreatedBy"]] = [ProjectID, QuoteID, RevID,
                                                                                          userinput['email']]
                    parameterdf.loc[i, ['Group', 'Type', 'TypeID']] = [groups, entries["tablename"], int(entries['Id'])]

                    if 'PriceAdjustment' in entries:  # Remove condition later
                        parameterdf.loc[i, ['PriceAdjustment']] = entries['PriceAdjustment']

                    i += 1
            parameterdf.fillna(np.nan, inplace=True)
            parameterdf.replace([np.nan], [None], inplace=True)
            parameterdf = parameterdf.loc[~parameterdf.duplicated(subset=['TypeID'])]

            IDinBoM = list(set(parameterdf['TypeID'].values))

            IDsToDelete = [int(ViewCurrBoMDf.loc[index, 'Id']) for index in ViewCurrBoMDf.index if
                           ViewCurrBoMDf.loc[index, 'ID'] not in IDinBoM]

            if len(IDsToDelete) > 0:
                DeleteIds = [{'_Id': _Id} for _Id in IDsToDelete]
                CDB.DeleteFromBoM(DeleteIds)

            # Delete Ids not in UpdateList

            CDB.MakeBoMQuote(parameterdf)
            # for Up in UpdateList:
            #    Up['_Id'] = Up.pop('Id')
            #    if 'Quantity' in Up:
            #        if Up['Quantity']!=None:
            #            Up['Quantity']= float(Up['Quantity'])
            #    BoM2Update.append(Up)
            # if BoM2Update:   
            #     CDB.UpdateBoM(BoM2Update)
            entity.update({"status": "Components"})
        else:  # An update of the full Cost and price Overview
            BoM2Update = []
            QuoteID = params['QuoteID']
            RevID = params['RevID']
            UpdateList = userinput['params']['data']
            for Up in UpdateList:
                Up['_Id'] = Up.pop('Id')
                if 'Quantity' in Up:
                    if Up['Quantity'] != None:
                        Up['Quantity'] = float(Up['Quantity'])
                BoM2Update.append(Up)
            if BoM2Update:
                CDB.UpdateBoM(BoM2Update)

    elif userinput['action'] == "gettypesof":
        tablename = params['tablename']
        columnname = params['columnname']
        querytext = "SELECT DISTINCT {} FROM {} WHERE ActiveFlag=1 ORDER BY {} ASC;".format(columnname, tablename,
                                                                                            columnname)
        res = CDB.RunRawSQLQuery(querytext)
        OutputDic.update({"Typesof": [r[0] for r in res]})
    elif userinput['action'] == "getquoterev":
        if userinput['params'] == {}:
            querytext = "SELECT `QuoteID`,`RevID` FROM BOM_QUOTE WHERE `ProjectID`='{}' \
                    ORDER BY `CreatedDate` DESC     \
                     LIMIT 1;".format(userinput['project_id'])
            res = CDB.RunRawSQLQuery(querytext)
            if len(res) > 0:
                OutputDic.update({"QuoteID": res[0][0], "RevID": res[0][1]})
            else:
                OutputDic.update({})
        else:
            QuoteID = params['QuoteID']
            querytext = "SELECT DISTINCT RevID FROM BOM_QUOTE WHERE `ProjectID`='{}' and `QuoteID`='{}' \
                     ;".format(userinput['project_id'], QuoteID)

            res = CDB.RunRawSQLQuery(querytext)
            if len(res) > 0:
                OutputDic.update({"RevID": [l[0] for l in res]})
            else:
                OutputDic.update({})
    ds.put(entity)
    return jsonify(OutputDic), 200


@app.route('/techselect-v2', methods=['POST'])
@jwt_authenticated
def techselectv2():
    OutputDict = {}
    ds = datastore.Client()
    gcs = storage.Client()
    bucket = gcs.get_bucket(BUCKET_NAME)
    req_json = request.get_json()['Components']
    userinput = req_json
    userinput['email'] = request.email
    orgname = request.org
    parent_key = ds.key('users', userinput['email'])
    message = {}
    entity = ds.get(key=ds.key('Project', userinput['project_id'], parent=parent_key))
    if not entity:
        message.update({"error": "No project with username {} and project {} exists".format(userinput['email'],
                                                                                            userinput['project_id'])})
        return jsonify(message), 400
    entchecklist = ['Location', 'Weather', 'Demand']
    for ec in entchecklist:
        if ec not in entity:
            return jsonify({"error": "{} not set in datastore".format(ec)}), 400

    DsLocation = entity['Location']
    DsWeather = entity['Weather']

    # Use the Correct DB based on Organisation            
    # qur = ds.query(kind='organisation')
    # qur.add_filter('users','=',userinput['email'])
    # Sol=list(qur.fetch())
    # if Sol != []:
    #     if os.environ['DB_NAME']!=Sol[0]['compdb']:
    #         os.environ['DB_NAME']= Sol[0]['compdb']
    #     orgname =  Sol[0].key.name    
    # else:        
    #     os.environ['DB_NAME'] = 'refdata' 
    #     orgname = 'default'
    orgentity = ds.get(key=ds.key('organisation', orgname))
    os.environ['DB_NAME'] = orgentity.get("compdb", "refdata")
    importlib.reload(CDB)
    # OrgInfo = orgentity['OrgInfo']

    OrgInfo = json.loads(bucket.get_blob(AUXFILES + "organisation_info.json").download_as_string())
    OrgDict = OrgInfo.get(orgname, OrgInfo['Agrola'])
    OrgBomInput = OrgDict.get('bominput', {})

    # TODO: Adapt for multiple areas till then just use 1st area
    # TODO : Put in error message when tilt is missing in Location
    if (('tilt' not in DsLocation) or ('azimuth' not in DsLocation)):
        return jsonify({"error": "tilt or azimuth information missing in Location"})
    if (('latitude' not in DsLocation) or ('longitude' not in DsLocation)):
        return jsonify({"error": "latitude or longitude information missing in Location"})
    if 'finalweatherfile' not in DsWeather:
        return jsonify({"error": "finalweatherfile information missing in Weather"})
    elecDesign = ELPARM
    default_costparams = FINPARM['cost']
    default_finparams = FINPARM['financial']

    tilt = {}
    azimuth = {}
    SurfArea = {}
    try:
        for AreaLabel in DsLocation['tilt']:
            tilt[AreaLabel] = DsLocation['tilt'][AreaLabel]
            azimuth[AreaLabel] = DsLocation['azimuth'][AreaLabel]

            if 'SurfArea' in DsLocation:
                SurfArea[AreaLabel] = DsLocation['SurfArea'][AreaLabel]
            else:
                numAreas = len(DsLocation['tilt'])
                SurfArea[AreaLabel] = DsLocation['TotalPanelArea'] / numAreas
    except:
        tilt['Area1'] = [i for i in DsLocation['tilt'].values()][0]
        azimuth['Area1'] = [i for i in DsLocation['azimuth'].values()][0]
        SurfArea['Area1'] = DsLocation['TotalPanelArea']

    # if 'SpacingParams' in DsLocation:
    #     SpacingParams = DsLocation['SpacingParams'] # {Area1:{Params}, "Area2":{Params}}
    # else:
    #     SpacingParams = elecDesign['systemparams']['SpacingParams'] 
    if 'AreaSpacingParams' in DsLocation:
        AreaSpacingParams = DsLocation['AreaSpacingParams']  # {Area1:{Params}, "Area2":{Params}}
    else:
        AreaSpacingParams = {AreaLabel: elecDesign['systemparams']['SpacingParams'] for AreaLabel in tilt}

    weatherfile = DsWeather['finalweatherfile']
    location_data = Location(DsLocation['latitude'], DsLocation['longitude'], tz=DsWeather['TZ'])
    SolData = pickle.loads(bucket.get_blob(weatherfile).download_as_string())

    # LatPolyfromFile
    GeoCoordFile = entity['Location']['GeoCoords']
    LatPolyfromFile = json.loads(bucket.get_blob(GeoCoordFile).download_as_string())

    uniqueid = 'Uid_' + userinput['email']
    blobname_pvmod = USER_UPLOADS + uniqueid + '_moduledata.pkl'
    blobpv = bucket.get_blob(blobname_pvmod)
    if blobpv:
        USERPVDF = pickle.loads(blobpv.download_as_string())

    Power_req = entity['Demand']['ACpower'] * 1000  # convert to W
    if (Power_req == 0):
        Power_req = 10 * 1000

    # Get Model names from BoM for solar module and inverter     
    QuoteID = userinput['quoteparams']['QuoteID']
    RevID = userinput['quoteparams']['RevID']
    BoMDf = CDB.GetBoMQuote(userinput['project_id'], QuoteID, RevID)
    BoMDf.loc[~BoMDf['PerfModelKey'].isnull(), 'PerfModelKey'] = BoMDf.loc[
        ~BoMDf['PerfModelKey'].isnull(), 'PerfModelKey'].apply(lambda x: x.replace('\ufeff', '')).values
    BoMDf = BoMDf.loc[~BoMDf.duplicated(subset=['ID'])]

    if 'ActiveQuote' in entity:
        entity['ActiveQuote'].update({"QuoteID": QuoteID})
        entity['ActiveQuote'].update({"RevID": RevID})
    else:
        entity.update({"ActiveQuote": {"QuoteID": QuoteID, "RevID": RevID}})

    PVrawlst = [pvname for pvname in list(BoMDf[BoMDf['Type'] == 'Solarmodule']['PerfModelKey'].values)]
    Invrawlst = [invname for invname in list(BoMDf[BoMDf['Type'] == 'Inverter']['PerfModelKey'].values)]

    PVList = []
    InvList = []
    errmsg = " "
    # if ((len(PVrawlst)>3)or(len(Invrawlst)>3)):
    #     return jsonify({"error": "More than 3 components uploaded "}),400
    if len(set(PVrawlst)) != len(PVrawlst):
        return jsonify({"error": "Duplicate names in PV module list"}), 400
    if len(set(Invrawlst)) != len(Invrawlst):
        return jsonify({"error": "Duplicate names in Inverter name list"}), 400

    # Check for the module and inverter names in the  performance modules db
    for p in PVrawlst:
        if p in PVDF.columns:
            PVList.append(p)
        elif p in PVSYSDF.columns:
            PVList.append(p)
        elif blobpv:
            if p in USERPVDF.columns:
                PVList.append(p)
            else:
                return jsonify({"error": "PV module name {} not found in any database".format(p)}), 400
        else:
            return jsonify({"error": "PV module name {} not found in any database".format(p)}), 400

    for i in Invrawlst:
        if i in INVDF.columns:  # IF exact name then take it
            Invname = i
        else:
            score, Invname = \
            sorted({Levenshtein.jaro(x.replace('_', ' '), i): x for x in INVDF.columns}.items(), reverse=True)[
                0]  # If fuzzy then try to get match

            InvData = INVDF.loc[:, Invname]
            errmsg = errmsg + ' - Inverter selected {}\n'.format(InvData.name.replace("_", " "))
            Invname = InvData.name
        InvList.append(Invname)

    readstored = False
    # Deactivate for the time being
    # if ('Components' in entity) & (('refresh') not in userinput):
    #     InvStored = entity['Components'].get('InvName')
    #     PVStored = entity['Components'].get('PVName')
    #     if (set(InvStored) == set(InvList))&(set(PVStored)==set(PVList)):
    #         # skip all the calculations and just return 
    #         readstored = True
    #         errmsg +=  "Read stored values - Refresh to re-optimise and update"
    #         OutputDict.update({'PVName':PVList,'InvName':InvList,'status':errmsg})

    if not readstored:

        ###### Gather Financial Parameters into FinData
        if 'finparams' in entity:
            FinData = {}
            FinData = entity['finparams']
            fin2ent = {}
            for t in default_finparams:
                if t not in FinData:
                    fin2ent[t] = default_finparams[t]
                    FinData[t] = default_finparams[t]
                else:
                    fin2ent[t] = FinData[t]
            if 'Adjustments' in FinData:
                fin2ent['Adjustments'] = FinData['Adjustments']

        else:
            fin2ent = {}
            FinData = {}
            for t in default_finparams:
                fin2ent[t] = default_finparams[t]
                FinData[t] = default_finparams[t]
        entity.update({'finparams': fin2ent})
        #########--------------------------#############################
        # Get Tariff Details here  Tariff and Yearly time series of tariff

        default_tariffsummary = {"pvtariffAvg": 0, "pvtarifftotal": 0,
                                 "EpriceAvg": default_finparams['e_price'], "Epricetotal": default_finparams['e_price']}
        if 'EpriceTariffFile' in entity:
            # blob = bucket.get_blob(USER_UPLOADS +entity['EpriceTariffFile'])
            FinData.update({"PVTariffSummary": {"pvtariffAvg": 0, "pvtarifftotal": 0,
                                                "EpriceAvg": default_finparams['e_price'],
                                                "Epricetotal": default_finparams['e_price']}})
            blob = bucket.get_blob(entity['EpriceTariffFile'])

            if blob:

                TariffDic = pickle.loads(blob.download_as_string())
                if ('PVTariffSummary' in TariffDic):
                    for t in default_tariffsummary:
                        if t not in TariffDic['PVTariffSummary']:
                            TariffDic['PVTariffSummary'][t] = default_tariffsummary[t]
                    FinData.update({"PVTariffSummary": TariffDic['PVTariffSummary']})
                else:
                    FinData.update({"PVTariffSummary": {"PVTariffSummary": {"pvtariffAvg": 0, "pvtarifftotal": 0,
                                                                            "EpriceAvg": default_finparams['e_price'],
                                                                            "Epricetotal": default_finparams[
                                                                                'e_price']}}})

                if ('YearlyTariff' in TariffDic):
                    FinData.update({"YearlyTariff": TariffDic['YearlyTariff']})
                else:
                    year = "2021"
                    YearlyTariff = pd.DataFrame(index=pd.date_range(start='1/1/' + year, periods=8760, freq='1H'),
                                                columns=['PVTariff'])
                    YearlyTariff['PVTariff'] = 0
                    FinData.update({"YearlyTariff": YearlyTariff})

            else:
                # blob = bucket.blob(USER_UPLOADS +entity['EpriceTariffFile'])
                blob = bucket.blob(entity['EpriceTariffFile'])
                TariffDic = {"PVTariffSummary": {"pvtariffAvg": 0, "pvtarifftotal": 0,
                                                 "EpriceAvg": default_finparams['e_price'],
                                                 "Epricetotal": default_finparams['e_price']}}

                year = "2021"
                YearlyTariff = pd.DataFrame(index=pd.date_range(start='1/1/' + year, periods=8760, freq='1H'),
                                            columns=['PVTariff'])
                YearlyTariff['PVTariff'] = 0
                TariffDic.update({"YearlyTariff": YearlyTariff})
                blob.upload_from_string(pickle.dumps(TariffDic))
                FinData.update(TariffDic)
        else:
            FinData.update({"PVTariffSummary": {"pvtariffAvg": 0, "pvtarifftotal": 0,
                                                "EpriceAvg": default_finparams['e_price'],
                                                "Epricetotal": default_finparams['e_price']}})
            YearlyTariff = pd.DataFrame(index=pd.date_range(start='1/1/2020', periods=8760, freq='1H'),
                                        columns=['PVTariff'])

            # Add electrical price by hour if avaialble
            YearlyTariff['PVTariff'] = 0
            FinData.update({"YearlyTariff": YearlyTariff})
        # Add Demand to FinData as well
        if 'DemandTS' in entity['Demand']:
            DemandTS = pickle.loads(bucket.get_blob(entity['Demand']['DemandTS']).download_as_string())
            FinData.update({"DemandTS": DemandTS})

        #######################################   
        # TO ensure the DC to AC power is limited 
        DClimits = True
        if 'criteria' in entity:
            if entity['criteria'] == 'None':
                DClimits = False

        St = elecDesign['PVstring']
        CompInd = pd.MultiIndex.from_product([PVList, InvList], names=['Modules', 'Inv'])
        OptDf = pd.DataFrame()
        TSOptDf = pd.DataFrame()
        for combo in CompInd:
            if combo[0] in PVDF:
                PVModuleData = PVDF.loc[:, combo[0]]
            elif combo[0] in PVSYSDF:
                PVModuleData = PVSYSDF.loc[:, combo[0]]
            elif blobpv:
                if combo[0] in USERPVDF:
                    PVModuleData = USERPVDF.loc[:, combo[0]]

            InvData = INVDF.loc[:, combo[1]]
            PVName = PVModuleData.name
            InvName = InvData.name
            # if Power_req  is DC Power

            # NumBB = int(round(Power_req/elecDesign['BoP_Design']['DC2ACmax']/InvData.Paco,0))
            NumBB = int(round(Power_req / InvData.Paco, 0))
            if NumBB == 0:
                break

            # if entity['criteria']=='None':
            #     TotalPanelArea = entity['Location']['TotalPanelArea']
            #     MaxNumPanels = int(TotalPanelArea/PVModuleData.A_c)
            #     PanStringArr,Ptuples =Opt.FeasibleStringRange(PVModuleData,InvData,SolData['Temperature'])
            #     MSRANGE = [list(set([d[0] for d in Ptuples.values()]))[0], list(set([d[0] for d in Ptuples.values()]))[-1]  ]
            #     MPRANGE = [list(set([d[1] for d in Ptuples.values()]))[1], list(set([d[1] for d in Ptuples.values()]))[-1]  ]
            #     OutputDict.update({"RangeSeries":MSRANGE,"RangeParallel":MPRANGE, "MaxNumPanels":MaxNumPanels,               
            #                             "NumInverters":NumBB,"DC2ACRange":[min(Ptuples.keys())*PVModuleData.Wp/(NumBB*InvData.Paco),max(Ptuples.keys())*PVModuleData.Wp/(NumBB*InvData.Paco)],
            #                             "ACPower_Multiplier":InvData.Paco/1000, "DCPower_Multiplier":PVModuleData.Wp/1000})
            #     OutputDict.update({"StringArr":json.loads(json.dumps(PanStringArr)),"MsMpPairs":json.loads(json.dumps(Ptuples))})
            #     return jsonify(OutputDict),200

            PVString = PVsel.PVString(tilt, azimuth, St['Albedo'], St['Ms'], St['Mp'], NumBB)
            # CostData is a df of the BOM only with PerfModelKey == PVName or InvName

            CostDf = BoMDf[BoMDf['PerfModelKey'].isnull()]
            CostDf = pd.concat([CostDf, BoMDf[BoMDf['PerfModelKey'] == InvName]])

            # CostDf = CostDf.drop(index=CostDf[CostDf['NetPrice']<0].index)
            # Remove negative prices from adjustments and subsidies that distort the CAPEX
            CostDf = CDB.BoMExcludeFlags(CostDf, OrgBomInput=OrgBomInput)

            TSDf, AggDf = Opt.OptParamRuns(SolData, location_data, PVModuleData, InvData,
                                           PVString, elecDesign, CostDf, FinData, AreaSpacingParams, tilt, azimuth,
                                           LatPolyfromFile, DClimits)

            TEMPdf = pd.concat([AggDf], keys=[(PVModuleData.name, InvData.name)], names=['PVName', 'InvName'], axis=0)
            OptDf = pd.concat([OptDf, TEMPdf], axis=0)

            TEMP1df = pd.concat([TSDf], keys=[(PVModuleData.name, InvData.name)], names=['PVName', 'InvName'], axis=1)
            TSOptDf = pd.concat([TSOptDf, TEMP1df], axis=1)

        if len(OptDf) == 0:
            return jsonify({"error": "Inverter size too large for required power"}), 400

        OptDf = OptDf.sort_index()

        # if len(OptDf)==0:
        #     return jsonify({"error":"No electrically feasible combination exits"}),400
        TSOptDf = TSOptDf.sort_index(axis='columns')  # Lexicograpic sorting ensures better performance
        uniqueid = 'Uid_' + userinput['email'] + '_Pid_' + userinput['project_id']
        blobname_configs = RESULTS + uniqueid + '_Configs.pkl'
        bucket.blob(blobname_configs).upload_from_string(pickle.dumps(OptDf))

        blobname_ts = RESULTS + uniqueid + '_TSConfigs.pkl'
        bucket.blob(blobname_ts).upload_from_string(pickle.dumps(TSOptDf))
        BoMDf.fillna(np.nan, inplace=True)
        BoMDf.replace([np.nan], [None], inplace=True)

        BoMtoSave = BoMDf[['Group', 'ID', 'Name', 'Cost', 'Unit', 'Currency']]

        optmatrix = {'OptMatrix': {'OptConfigFile': blobname_configs, 'TSConfigFile': blobname_ts},
                     'Components': {"PVName": PVList, "InvName": InvList},
                     'Cost': json.loads(BoMtoSave.to_json(orient='index'))}

        entity.update(optmatrix)
        entity.update({"status": "Generate Design"})

        OutputDict = {'PVName': PVList, 'InvName': InvList, 'status': errmsg}

        ds.put(entity)

    # Only if no criteria is set 1 set of 
    if readstored:
        blobname = entity['OptMatrix']['OptConfigFile']
        OptDf = pickle.loads(bucket.get_blob(blobname).download_as_string())
        combo = OptDf.index[0]
        if combo[0] in PVDF:
            PVModuleData = PVDF.loc[:, combo[0]]
        elif combo[0] in PVSYSDF:
            PVModuleData = PVSYSDF.loc[:, combo[0]]
        elif blobpv:
            if combo[0] in USERPVDF:
                PVModuleData = USERPVDF.loc[:, combo[0]]
        InvData = INVDF.loc[:, combo[1]]

    if 'criteria' in entity:
        if entity['criteria'] == 'None':
            if ((len(PVList) == 1) and (len(InvList) == 1)):
                TotalPanelArea = entity['Location']['TotalPanelArea']
                MaxNumPanels = int(TotalPanelArea / PVModuleData.A_c)
                # TODO: Send back the string arrangement - CHANGE UI
                # PanStringArr,Ptuples =Opt.FeasibleStringRange(PVModuleData,InvData,SolData['Temperature'])

                try:
                    MSRANGE = [min(OptDf.index.get_level_values(2)), max(OptDf.index.get_level_values(2))]
                    MPRANGE = [min(OptDf.index.get_level_values(3)), max(OptDf.index.get_level_values(3))]
                    OutputDict.update({"RangeSeries": MSRANGE, "RangeParallel": MPRANGE, "MaxNumPanels": MaxNumPanels,
                                       "NumInverters": OptDf.iloc[0]['NumBB'],
                                       "DC2ACRange": [min(OptDf.iloc[:]['DC2AC'].values),
                                                      max(OptDf.iloc[:]['DC2AC'].values)],
                                       "ACPower_Multiplier": InvData.Paco / 1000,
                                       "DCPower_Multiplier": PVModuleData.Wp / 1000})
                except:
                    MSRANGE = [0]
                    MPRANGE = [0]
                    OutputDict.update({"RangeSeries": MSRANGE, "RangeParallel": MPRANGE, "MaxNumPanels": MaxNumPanels,
                                       "NumInverters": 0, "DC2ACRange": [0, 0],
                                       "ACPower_Multiplier": InvData.Paco / 1000,
                                       "DCPower_Multiplier": PVModuleData.Wp / 1000})
            else:
                return jsonify({"error": "Only 1 Solarmodule and 1 inverter should be selected in component list"}), 400

    ds.put(entity)
    return jsonify(OutputDict), 200


@app.route('/techselect', methods=['POST', 'GET'])  # 
@jwt_authenticated
def techselect():
    ds = datastore.Client()
    gcs = storage.Client()

    req_json = request.get_json()['Components']

    userinput = req_json
    userinput['email'] = request.email
    orgname = request.org
    parent_key = ds.key('users', userinput['email'])
    message = {}

    entity = ds.get(key=ds.key('Project', userinput['project_id'], parent=parent_key))
    if not entity:
        message.update({"error": "No project with username {} and project {} exists".format(userinput['email'],
                                                                                            userinput['project_id'])})
        return jsonify(message), 400

    entchecklist = ['Location', 'Weather', 'Demand']
    for ec in entchecklist:
        if ec not in entity:
            return jsonify({"error": "{} not set in datastore".format(ec)}), 400

    uschklist = ['PVName', 'InvName']
    for us in uschklist:
        if us not in userinput:
            return jsonify({"error": "{} not set in input request".format(us)}), 400

    DsLocation = entity['Location']
    DsWeather = entity['Weather']

    # TODO: Adapt for multiple areas till then just use 1st area
    # TODO : Put in error message when tilt is missing in Location
    if (('tilt' not in DsLocation) or ('azimuth' not in DsLocation)):
        return jsonify({"error": "tilt or azimuth information missing in Location"})
    if (('latitude' not in DsLocation) or ('longitude' not in DsLocation)):
        return jsonify({"error": "latitude or longitude information missing in Location"})
    if 'finalweatherfile' not in DsWeather:
        return jsonify({"error": "finalweatherfile information missing in Weather"})
    elecDesign = ELPARM

    tilt = {}
    azimuth = {}
    SurfArea = {}
    try:
        for AreaLabel in DsLocation['tilt']:
            tilt[AreaLabel] = DsLocation['tilt'][AreaLabel]
            azimuth[AreaLabel] = DsLocation['azimuth'][AreaLabel]

            if 'SurfArea' in DsLocation:
                SurfArea[AreaLabel] = DsLocation['SurfArea'][AreaLabel]
            else:
                numAreas = len(DsLocation['tilt'])
                SurfArea[AreaLabel] = DsLocation['TotalPanelArea'] / numAreas
    except:
        tilt['Area1'] = [i for i in DsLocation['tilt'].values()][0]
        azimuth['Area1'] = [i for i in DsLocation['azimuth'].values()][0]
        SurfArea['Area1'] = DsLocation['TotalPanelArea']

    if 'AreaSpacingParams' in DsLocation:
        AreaSpacingParams = DsLocation['AreaSpacingParams']
    else:
        AreaSpacingParams = {AreaLabel: elecDesign['systemparams']['SpacingParams'] for AreaLabel in tilt}
        # try:
        #     tilt = tilt+DsLocation['SpacingParams']['Ti_P']
        #     azimuth = DsLocation['SpacingParams']['Az_P']
        # except:
        #     pass

    weatherfile = DsWeather['finalweatherfile']
    location_data = Location(DsLocation['latitude'], DsLocation['longitude'], tz=DsWeather['TZ'])

    blobname = weatherfile
    bucket = gcs.get_bucket(BUCKET_NAME)
    blob = bucket.get_blob(blobname)
    s = blob.download_as_string()

    SolData = pickle.loads(s)

    uniqueid = 'Uid_' + userinput['email']

    blobname_pvmod = USER_UPLOADS + uniqueid + '_moduledata.pkl'
    blobpv = bucket.get_blob(blobname_pvmod)
    if blobpv:
        s = blobpv.download_as_string()
        USERPVDF = pickle.loads(s)

    GeoCoordFile = entity['Location']['GeoCoords']
    LatPolyfromFile = json.loads(bucket.get_blob(GeoCoordFile).download_as_string())

    Power_req = entity['Demand']['ACpower'] * 1000  # convert to W

    if Power_req == 0:
        Power_req = 10 * 1000

    PVrawlst = [pvname for pvname in userinput['PVName']]
    Invrawlst = [invname for invname in userinput['InvName']]
    default_costparams = FINPARM['cost']

    default_finparams = FINPARM['financial']

    if 'Cost' in entity:
        CostData = {}
        CostData.update(entity['Cost'])
    else:
        CostData = {}
        CostData.update(default_costparams)

    if 'PVCost' not in CostData:
        CostData['PVCost'] = {PVName: {'data': default_costparams['PVCost']['data'][0],
                                       'Unit': default_costparams['PVCost']['Unit']} for PVName in PVrawlst}

    if 'InvCost' not in CostData:
        CostData['InvCost'] = {InvName: {'data': default_costparams['InvCost']['data'],
                                         'Unit': default_costparams['PVCost']['Unit']} for InvName in Invrawlst}

    if 'finparams' in entity:
        FinData = {}
        FinData = entity['finparams']
        fin2ent = {}
        for t in default_finparams:
            if t not in FinData:
                fin2ent[t] = default_finparams[t]
                FinData[t] = default_finparams[t]
            else:
                fin2ent[t] = FinData[t]
        if 'Adjustments' in FinData:
            fin2ent['Adjustments'] = FinData['Adjustments']

    else:
        fin2ent = {}
        FinData = {}
        for t in default_finparams:
            fin2ent[t] = default_finparams[t]
            FinData[t] = default_finparams[t]

    # DELETE SOON 

    ##############

    PVList = []
    InvList = []
    errmsg = " "

    if ((len(PVrawlst) > 3) or (len(Invrawlst) > 3)):
        return jsonify({"error": "More than 3 components uploaded "}), 400

    if len(set(PVrawlst)) != len(PVrawlst):
        return jsonify({"error": "Duplicate names in PV module list"}), 400

    if len(set(Invrawlst)) != len(Invrawlst):
        return jsonify({"error": "Duplicate names in Inverter name list"}), 400

    for p in PVrawlst:
        if p in PVDF.columns:
            PVList.append(p)
        elif p in PVSYSDF.columns:
            PVList.append(p)
        elif blobpv:
            if p in USERPVDF.columns:
                PVList.append(p)
            else:
                return jsonify({"error": "PV module name {} not found in any database".format(p)}), 400
        else:
            return jsonify({"error": "PV module name {} not found in any database".format(p)}), 400

    #        score,PVname = sorted({Levenshtein.jaro(x.replace('_',' '),p): x for x in PVDF.columns}.items(),reverse=True)[0] 
    #        sysscore, PVSYSname = sorted({Levenshtein.jaro(x.replace('_',' '),p): x for x in PVSYSDF.columns}.items(),reverse=True)[0]         
    #        
    #        if score>=sysscore:
    #            PVList.append(PVname)
    #        else:
    #            PVList.append(PVSYSname)

    for i in Invrawlst:
        if i in INVDF.columns:  # IF exact name then take it
            Invname = i
        else:
            score, Invname = \
                sorted({Levenshtein.jaro(x.replace('_', ' '), i): x for x in INVDF.columns}.items(), reverse=True)[
                0]  # If fuzzy then try to get match

            InvData = INVDF.loc[:, Invname]
            # brandname = re.split('__',Invname)[0]

            #            if ( ((InvData['Paco']) < Power_req) or ( abs(((InvData['Paco']) - Power_req)/(InvData['Paco'])) > 0.2 ) ): # Find closest power if power mismatch more than 20%
            #                samebrandlist = [invname for invname in INVDF.columns if invname.startswith(brandname)]
            #                InvData = INVDF.loc[:,(abs(INVDF.loc['Paco',samebrandlist]-Power_req)).idxmin] # Find closest power match within the same brand
            #                Invname = InvData.name
            #                errmsg = errmsg+' - Inverter selected {} - due to mismatch with power required, closest matching model of same brand of inverter selected\n'.format(InvData.name.replace("_"," "))
            #            else:
            errmsg = errmsg + ' - Inverter selected {}\n'.format(InvData.name.replace("_", " "))
            Invname = InvData.name
        InvList.append(Invname)

    # Updating Inverter and Battery Costs by expanding cost and name lists

    # Update to required form only if not already done so

    if 'Unit' in CostData['PVCost']:
        CostData.update({"PVCost": {k: {'data': v, 'Unit': CostData['PVCost']['Unit']} for k, v in
                                    zip(PVList, CostData['PVCost']['data'])},
                         'InvCost': {k: {'data': v, 'Unit': CostData['InvCost']['Unit']} for k, v in
                                     zip(InvList, CostData['InvCost']['data'])}})

    # TODO : Make this CostData based on BoMQuote 

    """
    Structure:
       {'PVCost': {'PVName': {'data': 0.3, 'Unit': 'per Wp'}},
        'InvCost': {'InvName': {'data': 0.05, 'Unit': 'per Wac'}},
        'Construction': {'Unit': 'per plant', 'data': 0},
        'Bos': {'Unit': 'per plant', 'data': 0},
        'Others': {'Unit': 'per plant', 'data': 0},
        'Curr': 'USD'} 
    """

    elecDesign = ELPARM

    ################################################

    entity.update({'finparams': fin2ent})  # Save it here, as Dfs will be added to the finparams which is invalid for ds

    # Get Tariff Details here  Tariff and Yearly time series of tariff

    default_tariffsummary = {"pvtariffAvg": 0, "pvtarifftotal": 0,
                             "EpriceAvg": default_finparams['e_price'], "Epricetotal": default_finparams['e_price']}
    if 'EpriceTariffFile' in entity:
        # blob = bucket.get_blob(USER_UPLOADS +entity['EpriceTariffFile'])
        FinData.update({"PVTariffSummary": {"pvtariffAvg": 0, "pvtarifftotal": 0,
                                            "EpriceAvg": default_finparams['e_price'],
                                            "Epricetotal": default_finparams['e_price']}})
        blob = bucket.get_blob(entity['EpriceTariffFile'])

        if blob:

            TariffDic = pickle.loads(blob.download_as_string())
            if ('PVTariffSummary' in TariffDic):
                for t in default_tariffsummary:
                    if t not in TariffDic['PVTariffSummary']:
                        TariffDic['PVTariffSummary'][t] = default_tariffsummary[t]
                FinData.update({"PVTariffSummary": TariffDic['PVTariffSummary']})
            else:
                FinData.update({"PVTariffSummary": {"PVTariffSummary": {"pvtariffAvg": 0, "pvtarifftotal": 0,
                                                                        "EpriceAvg": default_finparams['e_price'],
                                                                        "Epricetotal": default_finparams['e_price']}}})

            if ('YearlyTariff' in TariffDic):
                FinData.update({"YearlyTariff": TariffDic['YearlyTariff']})
            else:
                year = "2020"
                YearlyTariff = pd.DataFrame(index=pd.date_range(start='1/1/' + year, periods=8760, freq='1H'),
                                            columns=['PVTariff'])
                YearlyTariff['PVTariff'] = 0
                FinData.update({"YearlyTariff": YearlyTariff})

        else:
            # blob = bucket.blob(USER_UPLOADS +entity['EpriceTariffFile'])
            blob = bucket.blob(entity['EpriceTariffFile'])
            TariffDic = {"PVTariffSummary": {"pvtariffAvg": 0, "pvtarifftotal": 0,
                                             "EpriceAvg": default_finparams['e_price'],
                                             "Epricetotal": default_finparams['e_price']}}

            year = "2020"
            YearlyTariff = pd.DataFrame(index=pd.date_range(start='1/1/' + year, periods=8760, freq='1H'),
                                        columns=['PVTariff'])
            YearlyTariff['PVTariff'] = 0
            TariffDic.update({"YearlyTariff": YearlyTariff})
            blob.upload_from_string(pickle.dumps(TariffDic))
            FinData.update(TariffDic)
    else:
        FinData.update({"PVTariffSummary": {"pvtariffAvg": 0, "pvtarifftotal": 0,
                                            "EpriceAvg": default_finparams['e_price'],
                                            "Epricetotal": default_finparams['e_price']}})
        YearlyTariff = pd.DataFrame(index=pd.date_range(start='1/1/2020', periods=8760, freq='1H'),
                                    columns=['PVTariff'])

        # Add electrical price by hour if avaialble
        YearlyTariff['PVTariff'] = 0
        FinData.update({"YearlyTariff": YearlyTariff})

    #######################################   
    DClimits = True
    if 'criteria' in entity:
        if entity['criteria'] == 'None':
            DClimits = False

    St = elecDesign['PVstring']

    CompInd = pd.MultiIndex.from_product([PVList, InvList], names=['Modules', 'Inv'])
    OptDf = pd.DataFrame()
    TSOptDf = pd.DataFrame()
    for combo in CompInd:
        if combo[0] in PVDF:
            PVModuleData = PVDF.loc[:, combo[0]]
        elif combo[0] in PVSYSDF:
            PVModuleData = PVSYSDF.loc[:, combo[0]]
        elif blobpv:
            if combo[0] in USERPVDF:
                PVModuleData = USERPVDF.loc[:, combo[0]]

        InvData = INVDF.loc[:, combo[1]]
        PVName = PVModuleData.name
        InvName = InvData.name
        NumBB = int(round(Power_req / InvData.Paco, 0))

        PVString = PVsel.PVString(tilt, azimuth, St['Albedo'], St['Ms'], St['Mp'], NumBB)

        # If CostData has been pulled from a stored version without the cost update and so cost values are meaningless anyhow
        if PVName not in CostData['PVCost']:
            CostData.update({"PVCost": {
                k: {'data': default_costparams['PVCost']['data'][0], 'Unit': default_costparams['PVCost']['Unit']} for k
                in PVList}})
        if InvName not in CostData['InvCost']:
            CostData.update({"InvCost": {
                k: {'data': default_costparams['InvCost']['data'][0], 'Unit': default_costparams['InvCost']['Unit']} for
                k in InvList}})

        CostSelected = {'PVCost': CostData['PVCost'][PVName],
                        'InvCost': CostData['InvCost'][InvName]}
        CostSelected.update({k: v for k, v in CostData.items() if ((k != 'PVCost') and (k != 'InvCost'))})

        TSDf, AggDf = Opt.OptParamRuns(SolData, location_data, PVModuleData, InvData,
                                       PVString, elecDesign, CostSelected, FinData, AreaSpacingParams, tilt, azimuth,
                                       LatPolyfromFile, DClimits)

        TEMPdf = pd.concat([AggDf], keys=[(PVModuleData.name, InvData.name)], names=['PVName', 'InvName'], axis=0)
        OptDf = pd.concat([OptDf, TEMPdf], axis=0)

        TEMP1df = pd.concat([TSDf], keys=[(PVModuleData.name, InvData.name)], names=['PVName', 'InvName'], axis=1)
        TSOptDf = pd.concat([TSOptDf, TEMP1df], axis=1)

    OptDf = OptDf.sort_index()
    if len(OptDf) == 0:
        return jsonify({"error": "No electrically feasible combination exits"}), 400
    TSOptDf = TSOptDf.sort_index(axis='columns')  # Lexicograpic sorting ensures better performance
    uniqueid = 'Uid_' + userinput['email'] + '_Pid_' + userinput['project_id']
    blobname_configs = RESULTS + uniqueid + '_Configs.pkl'
    blob = bucket.blob(blobname_configs)
    blob.upload_from_string(pickle.dumps(OptDf))

    blobname_ts = RESULTS + uniqueid + '_TSConfigs.pkl'
    blob = bucket.blob(blobname_ts)
    blob.upload_from_string(pickle.dumps(TSOptDf))

    optmatrix = {'OptMatrix': {'OptConfigFile': blobname_configs, 'TSConfigFile': blobname_ts},
                 'Components': {"PVName": PVList, "InvName": InvList},
                 'Cost': CostData}

    entity.update(optmatrix)
    entity.update({"status": "Generate Design"})

    outputdict = {'PVName': PVList, 'InvName': InvList, 'status': errmsg}

    ds.put(entity)

    # Only if no criteria is set
    if 'criteria' in entity:
        if entity['criteria'] == 'None':
            if ((len(PVList) == 1) and (len(InvList) == 1)):
                TotalPanelArea = entity['Location']['TotalPanelArea']
                MaxNumPanels = int(TotalPanelArea / PVModuleData.A_c)
                try:
                    MSRANGE = [min(OptDf.index.get_level_values(2)), max(OptDf.index.get_level_values(2))]
                    MPRANGE = [min(OptDf.index.get_level_values(3)), max(OptDf.index.get_level_values(3))]
                    outputdict.update({"RangeSeries": MSRANGE, "RangeParallel": MPRANGE, "MaxNumPanels": MaxNumPanels,
                                       "NumInverters": OptDf.iloc[0]['NumBB'],
                                       "DC2ACRange": [min(OptDf.iloc[:]['DC2AC'].values),
                                                      max(OptDf.iloc[:]['DC2AC'].values)],
                                       "ACPower_Multiplier": InvData.Paco / 1000,
                                       "DCPower_Multiplier": PVModuleData.Wp / 1000})
                except:
                    MSRANGE = [0]
                    MPRANGE = [0]
                    outputdict.update({"RangeSeries": MSRANGE, "RangeParallel": MPRANGE, "MaxNumPanels": MaxNumPanels,
                                       "NumInverters": 0, "DC2ACRange": [0, 0],
                                       "ACPower_Multiplier": InvData.Paco / 1000,
                                       "DCPower_Multiplier": PVModuleData.Wp / 1000})

    #    compselection = {'PV_name':PVModuleData.name,'Inverter_name':InvData.name,
    #                     'criteria':userinput['criteria'],'battery':userinput['battery'],'tiltvalue':userinput['tiltvalue'],
    #                     'fulfil_factor':userinput['fulfil_factor']}

    return jsonify(outputdict), 200


@app.route('/search-components', methods=['POST', 'GET'])  # search for components
@jwt_authenticated
def searchcomponents():
    ds = datastore.Client()
    gcs = storage.Client()

    message = {}
    userinput = request.get_json()
    userinput['email'] = request.email
    orgname = request.org
    parent_key = ds.key('users', userinput['email'])

    entity = ds.get(key=ds.key('Project', userinput['project_id'], parent=parent_key))
    if not entity:
        message.update({"error": "No project with username {} and project {} exists".format(userinput['email'],
                                                                                            userinput['project_id'])})
        return jsonify(message), 400

    uschklist = ['component']
    for us in uschklist:
        if us not in userinput:
            return jsonify({"error": "{} not set in input request".format(us)}), 400
    if 'name' not in userinput:
        name = ""
    else:
        name = userinput['name']
    if 'numresults' not in userinput:
        results2show = 10
    else:
        results2show = int(userinput['numresults'])
    component = userinput['component']  # "PVModule" or "Inverter"

    uniqueid = 'Uid_' + userinput['email']
    OutputDic = {}
    bucket = gcs.get_bucket(BUCKET_NAME)
    if 'Demand' in entity:
        ACPower = entity['Demand']['ACpower']  # in kW
    else:
        ACPower = 99999

    if 'power' in userinput:
        Power = userinput['power']  # in W
    #        minP = 0.8*Power
    #        maxP = 1.2*Power 
    else:
        Power = 0
    #        minP = 0.8*Power
    #        maxP = np.Inf

    if component == "PVModule":

        blobname_pvmod = USER_UPLOADS + uniqueid + '_moduledata.pkl'
        blobpv = bucket.get_blob(blobname_pvmod)
        if blobpv:
            s = blobpv.download_as_string()
            USERPVDF = pickle.loads(s)
            USERPVDF.loc['Wp'] = USERPVDF.loc['I_mp_ref'] * USERPVDF.loc['V_mp_ref']
            USERPVDF = USERPVDF.loc[:, ~USERPVDF.columns.duplicated()]
        else:
            USERPVDF = pd.DataFrame()

        ListOfDfs = [PVDF, PVSYSDF, USERPVDF]  # Add User DFs here
        sourcelist = ['CEC', 'PVSYST', 'UserDB']
        List_pv = pd.DataFrame()
        for DFs, source in zip(ListOfDfs, sourcelist):
            try:

                if Power == 0:
                    BL = DFs.columns
                else:
                    # BL= abs(DFs.loc['Wp',:]-Power).sort_values(ascending=True).index[:results2show] 
                    # BL = DFs.columns[(DFs.loc['Wp']>Power*0.8) & (DFs.loc['Wp']<Power*1.2)] # Select Power range 
                    BL = DFs.columns[(DFs.loc['Wp'] >= Power) & (DFs.loc['Wp'] < Power * 1.1)]
                Lname = sorted([(Levenshtein.jaro(x.replace('_', ' ').lower(), name.lower()), x) for x in DFs[BL]],
                               reverse=True)[:results2show]
                listDf = pd.DataFrame(Lname, columns=['score', 'name'])
                listDf['Wp'] = [np.round(DFs[x[1]].loc['Wp']) for x in Lname]
                listDf['source'] = source
                List_pv = pd.concat([List_pv, listDf])
            except:
                pass
        # List_pv.sort_values(by=['Wp'],ascending=False).reset_index(drop=True)[['name','Wp','source']].iloc[:results2show].to_dict()    

        OutputDic.update({"PVlist": List_pv.sort_values(by=['score'], ascending=False).reset_index(drop=True)[
                                        ['name', 'Wp', 'source']]
                         .iloc[:results2show].to_dict()})

    if component == "Inverter":
        blobname_inv = USER_UPLOADS + uniqueid + '_invdata.pkl'
        blobinv = bucket.get_blob(blobname_inv)
        if blobinv:
            s = blobinv.download_as_string()
            USERINVDF = pickle.loads(s)
        else:
            USERINVDF = pd.DataFrame()

        ListOfDfs = [INVDF, USERINVDF]  # Add User DFs here 

        List_inv = pd.DataFrame()
        for DFs in ListOfDfs:
            try:
                # BL = DFs.columns[(DFs.loc['Paco']>minP) & (DFs.loc['Paco']<maxP)] # Select Power range
                if Power == 0:

                    BL = DFs.columns[(DFs.loc['Paco'] <= ACPower * 1000)]
                else:
                    BL = DFs.columns[(DFs.loc['Paco'] > Power * 0.8 * 1000) & (DFs.loc['Paco'] < Power * 1.2 * 1000) & (
                                DFs.loc['Paco'] <= ACPower * 1000)]
                # BL= abs(DFs.loc['Paco',:]-Power).sort_values(ascending=True).index[:results2show] 
                Lname = sorted([(Levenshtein.jaro(x.replace('_', ' ').lower(), name.lower()), x) for x in DFs[BL]],
                               reverse=True)[:results2show]
                listDf = pd.DataFrame(Lname, columns=['score', 'name'])
                listDf['Pac'] = [np.round(DFs[x[1]].loc['Paco']) / 1000 for x in Lname]
                listDf['source'] = [DFs[x[1]].loc['Db'] if ('Db' in DFs.index) else 'custom' for x in Lname]
                List_inv = pd.concat([List_inv, listDf])
            except:
                pass
        List_inv.sort_values(by=['score'], ascending=False).reset_index(drop=True)[['name', 'Pac']].iloc[
        :results2show].to_dict()

        OutputDic.update({"Invlist": List_inv.sort_values(by=['score'], ascending=False).reset_index(drop=True)[
                                         ['name', 'Pac', 'source']].iloc[:results2show].to_dict()})

    return jsonify(OutputDic), 200


@app.route('/get-energy-elec-config', methods=['POST', 'GET'])  # Without storage /techselect_withstorage   
@jwt_authenticated
def elecdesign():
    # This one does almost everything....!
    ds = datastore.Client()
    gcs = storage.Client()
    bucket = gcs.get_bucket(BUCKET_NAME)
    errmsg = ' '
    message = {}
    req_json = request.get_json()['Energy']

    userinput = req_json
    userinput['email'] = request.email
    orgname = request.org
    uschklist = ['email', 'project_id', 'requesttype']
    for us in uschklist:
        if us not in userinput:
            return jsonify({"error": "{} not set in input request".format(us)}), 400

    parent_key = ds.key('users', userinput['email'])

    entity = ds.get(key=ds.key('Project', userinput['project_id'], parent=parent_key))
    if not entity:
        message.update({"error": "No project with username {} and project {} exists".format(userinput['email'],
                                                                                            userinput['project_id'])})
        return jsonify(message), 400

    ## USE DIFFERENT DB BASED ON ORG ###
    # qur = ds.query(kind='organisation')
    # qur.add_filter('users','=',userinput['email'])
    # Sol=list(qur.fetch())
    # if Sol != []:
    #     if os.environ['DB_NAME']!=Sol[0]['compdb']:
    #         os.environ['DB_NAME']= Sol[0]['compdb']
    #     orgname =  Sol[0].key.name      
    # else:        
    #     os.environ['DB_NAME'] = 'refdata'     
    orgentity = ds.get(key=ds.key('organisation', orgname))
    os.environ['DB_NAME'] = orgentity.get("compdb", "refdata")
    importlib.reload(CDB)
    # OrgInfo = orgentity['OrgInfo']
    uniqueid = 'Uid_' + userinput['email'] + '_Pid_' + userinput['project_id']
    OrgInfo = json.loads(bucket.get_blob(AUXFILES + "organisation_info.json").download_as_string())
    OrgDict = OrgInfo.get(orgname, OrgInfo['Agrola'])
    OrgBomInput = OrgDict.get('bominput', {})

    entchecklist = ['criteria', 'Components']
    for ec in entchecklist:
        if ec not in entity:
            return jsonify({"error": "{} not set in datastore".format(ec)}), 400

    criteria = entity['criteria']
    entity.update({"status": "Generate Design"})
    #    elecDesign = ELPARM
    #    St=elecDesign['PVstring']
    #    PVString=PVsel.PVString(St['Tilt'],St['Azimuth'],St['Albedo'],St['a'],St['b'],St['delT'],St['Ms'],St['Mp'],St['DC2inv'])
    #    
    #    BD= elecDesign['BoP_Design'] 
    #    BoP_Design = PVsel.BoP_Design(BD['Mismatch_loss'], BD['DC_loss'], BD['StringDiode_loss'], BD['AC_loss'], BD['InvClipping'], BD['DC2ACmax'])
    #    #Num_BB = elecDesign['systemparams']['NumBB']

    bucket = gcs.get_bucket(BUCKET_NAME)

    blobname_configs = RESULTS + uniqueid + '_Configs.pkl'
    blobOpt = bucket.get_blob(blobname_configs)
    if blobOpt:
        s = blobOpt.download_as_string()
        OptDf = pickle.loads(s)

        blobname_ts = RESULTS + uniqueid + '_TSConfigs.pkl'
        blobts = bucket.blob(blobname_ts)
        st = blobts.download_as_string()
        TSOptDf = pickle.loads(st)
        if (userinput['requesttype'] == 'Best'):
            if criteria == 'None':
                return jsonify({"error": "criteria is not defined for this request"}), 400
            BestConfigDict, BestConfig_OptDf, BestConfig_TSDf = Opt.SelectBestConfig(criteria, OptDf, TSOptDf)
            OutputDic = {"Config": BestConfigDict, "ConfigResultsSummary": json.loads(BestConfig_OptDf.to_json())}

            entity.update({"SelectedConfig": OutputDic})

        elif (userinput['requesttype'] == 'BestperCombo'):
            if criteria == 'None':
                return jsonify({"error": "criteria is not defined for this request"}), 400
            AllComboSummaryDf, BestComboNameslist, BestperComboList = Opt.SelectMatrixConfigs(criteria, OptDf, TSOptDf)
            BestConfigDict, BestConfig_OptDf, _ = Opt.SelectBestConfig(criteria, OptDf, TSOptDf)
            if BestConfigDict == {}:
                return jsonify(
                    {"error": "No feasible combination to match power requirements - choose another inverter"}), 400
            BestConfigDictList = [BestConfigDict['PVName'], BestConfigDict['InvName'], BestConfigDict['Ms'],
                                  BestConfigDict['Mp']]
            if 'SelectedConfig' in entity:
                SelConfigDict = entity['SelectedConfig']['Config']
                if SelConfigDict:
                    SelConfigList = [SelConfigDict['PVName'], SelConfigDict['InvName'], SelConfigDict['Ms'],
                                     SelConfigDict['Mp']]
                else:
                    SelConfigList = BestConfigDictList

            #            OutputDic = {"Config":BestComboNameslist,"ConfigResultsSummary":BestperComboList, "SelectedConfig":BestConfigDict,
            #                         "SelectedConfigResultsSummary":json.loads(BestConfig_OptDf.to_json())}
            #            
            ######### Make the requisite output response #########
            InvList = entity['Components']['InvName']
            PVList = entity['Components']['PVName']

            InvinBestCombo = list(set([L['InvName'] for L in BestComboNameslist]))
            PVinBestCombo = list(set([L['PVName'] for L in BestComboNameslist]))

            InvExtra = [Inv for Inv in InvList if Inv not in InvinBestCombo]
            PVExtra = [PV for PV in PVList if PV not in PVinBestCombo]
            if InvExtra:
                for i in InvExtra:
                    InvList.remove(i)
            if PVExtra:
                for p in PVExtra:
                    PVList.remove(p)

            OutputDic = {}
            InvID = [{"invid": InvList.index(Inv) + 1, "name": Inv} for Inv in InvList]
            AdditionalDf = pd.DataFrame(columns=AllComboSummaryDf.columns,
                                        index=['Ms', 'Mp', 'selected', 'recommended'])

            for s in AdditionalDf.columns:
                AdditionalDf[s].loc['Ms'] = int(s[2])
                AdditionalDf[s].loc['Mp'] = int(s[3])
                AdditionalDf[s].loc['selected'] = False
                AdditionalDf[s].loc['recommended'] = False

            AllComboSummaryDf = AllComboSummaryDf.append(AdditionalDf, ignore_index=False)

            if 'SelectedConfig' in entity:
                if tuple(SelConfigList) in AllComboSummaryDf.columns:
                    AllComboSummaryDf[tuple(SelConfigList)].loc['selected'] = True
                else:
                    AllComboSummaryDf[tuple(BestConfigDictList)].loc['selected'] = True
                    errmsg += " Selected Config {} not in Best combinations matrix but is still selected".format(
                        str(SelConfigList))
            else:
                AllComboSummaryDf[tuple(BestConfigDictList)].loc['selected'] = True
            AllComboSummaryDf[tuple(BestConfigDictList)].loc['recommended'] = True

            pvdata = []
            for PV in PVList:
                data = []
                element = {"pid": PVList.index(PV) + 1, "name": PV}
                for Inv in InvList:
                    data.append([AllComboSummaryDf[tuple(BestComboNameslist[i].values())].to_dict() for i in
                                 range(len(BestComboNameslist))
                                 if
                                 (BestComboNameslist[i]['InvName'] == Inv) and (BestComboNameslist[i]['PVName'] == PV)][
                                    0])

                    data = json.loads(json.dumps(data))
                    element.update({"solardata": data})

                pvdata.append(element)
            OutputDic = {"InvData": InvID, "PVdata": pvdata}

            #######################                             
            entity.update({"SelectedConfig": {"Config": BestConfigDict,
                                              "ConfigResultsSummary": json.loads(BestConfig_OptDf.to_json())}})
        elif (userinput['requesttype'] == 'SelectConfig'):
            if 'selectedconfig' not in userinput:
                configsel = entity['SelectedConfig']['Config']
                # return jsonify({"error": "Selected Config not in request"}),400
            else:
                configsel = userinput['selectedconfig']

                condchklist = ['PVName', 'InvName', 'Ms', 'Mp']
                for ec in condchklist:
                    if ec not in configsel:
                        return jsonify({"error": "{} not set in request".format(ec)}), 400

            pvname = configsel['PVName']
            invname = configsel['InvName']
            Ms = configsel['Ms']
            Mp = configsel['Mp']
            uniqueid = 'Uid_' + userinput['email']
            blobname_pvmod = USER_UPLOADS + uniqueid + '_moduledata.pkl'
            blobpv = bucket.get_blob(blobname_pvmod)
            if pvname in PVDF:
                PVModuleData = PVDF.loc[:, pvname]
            elif pvname in PVSYSDF:
                PVModuleData = PVSYSDF.loc[:, pvname]
            elif blobpv:
                USERPVDF = pickle.loads(blobpv.download_as_string())
                if pvname in USERPVDF:
                    PVModuleData = USERPVDF.loc[:, pvname]
                else:
                    return jsonify({"Error": "PV Module {} not found in any database".format(pvname)}), 400
            InvData = INVDF.loc[:, invname]

            Power_req = entity['Demand']['ACpower'] * 1000  # convert to W
            if Power_req == 0:
                Power_req = 10 * 1000
            NumBB = int(round(Power_req / InvData.Paco, 0))

            weatherblob = entity['Weather']['finalweatherfile']
            dataTMY = pickle.loads(bucket.get_blob(weatherblob).download_as_string())
            PanStringArr, Ptuples = Opt.FeasibleStringRange(PVModuleData, InvData, dataTMY['Temperature'])
            PaninStringKeys = np.array([x for x in PanStringArr.keys()])
            NuminString = int(PaninStringKeys[np.argmin(np.abs(PaninStringKeys - Ms * Mp))])

            # TotalPanels = NumBB*Ms*Mp
            TotalPanels = NumBB * NuminString
            ConfigSummTuple = (pvname, invname, Ms, Mp)
            try:
                ConfSummary = json.loads(OptDf.loc[ConfigSummTuple].to_json())
            except KeyError:
                return jsonify({"error": "{} not valid config stored in components ".format(str(ConfigSummTuple))}), 400

            if 'Adjustments' in entity['finparams']:
                Adjustments = entity['finparams']['Adjustments']
            else:
                Adjustments = {}
            if 'AreaFigures' in entity['Location']:
                AreaFigures = entity['Location']['AreaFigures']
            else:
                AreaFigures = {}
                # Get Time series values
            # selconfig = (entity['SelectedConfig']['Config']['PVName'],entity['SelectedConfig']['Config']['InvName'],
            #                         entity['SelectedConfig']['Config']['Ms'],entity['SelectedConfig']['Config']['Mp'],'PAC_tot')
            # Timeseries has (pvname, invname, Ms, Mp, value=PAC) as indices
            SelConfig_TSDf = pd.DataFrame()

            try:
                SelConfig_TSDf = TSOptDf[ConfigSummTuple]
            except:
                return jsonify(
                    {"error": "Configuration {} not in Time series of {}".format(str(ConfigSummTuple), uniqueid)})
            monthlyenergy = pd.DataFrame()
            monthlyenergy['PAC_tot'] = (SelConfig_TSDf['PAC_tot'].groupby(pd.Grouper(freq='1M')).sum()) / 1000  # in kWh
            monthlyenergy['kWh/kWp'] = monthlyenergy['PAC_tot'] / ConfSummary['DCPower']
            monthlyenergy['month'] = [dt.datetime.strftime(d, '%b') for d in monthlyenergy.index]
            #    monthlyenergy['PR'] = monthlyenergy['PAC_tot'].div(np.array(self.W2D['IrrOnSurface'])*((PVModuleData['V_mp_ref']*PVModuleData['I_mp_ref'])/(1000*PVModuleData['A_c'])))/1000#PAC in kWh

            energydisplay = {"Energydisplay": {
                "kwhmonthly": {"PAC_tot": [*monthlyenergy['PAC_tot']], "kWh/kWp": [*monthlyenergy['kWh/kWp']],
                               "month": [*monthlyenergy['month']]},
                "kwhyearly": {"totalMwh": monthlyenergy['PAC_tot'].sum() / 1000,
                              "kWhkWp": monthlyenergy['kWh/kWp'].mean()}}}  # "PR":monthlyenergy['PR'].mean()}}}

            PVList = [pvname]
            InvList = [invname]
            InvID = [{"invid": InvList.index(Inv) + 1, "name": Inv} for Inv in InvList]
            pvdata = []

            for PV in PVList:
                data = []
                element = {"pid": PVList.index(PV) + 1, "name": PV}
                for Inv in InvList:
                    data.append(OptDf.loc[ConfigSummTuple].to_dict())
                    data[0].update({"Ms": Ms, "Mp": Mp, "selected": True, "recommended": True})

                    data = json.loads(json.dumps(data))
                    element.update({"solardata": data})

                pvdata.append(element)

                # displaydic={"InvData":InvID,"PVdata":pvdata} 

            OutputDic = {"SelectedConfig": {"Config": configsel, "ConfigResultsSummary": ConfSummary
                                            }}
            # Change Value in BoM for Panel and Inverter quantities in case they are filled
            if 'ActiveQuote' in entity:

                ActiveQuote = entity['ActiveQuote']
                # Dont Change Values of Quote if active Quote is Edit
                if 'Edit' in ActiveQuote['RevID']:
                    RevID2Update = ActiveQuote['RevID'].split('-')[0]
                else:
                    RevID2Update = ActiveQuote['RevID']
                VQDf = CDB.GetBoMQuote(userinput['project_id'], ActiveQuote['QuoteID'], RevID2Update)
                if not VQDf.empty:
                    # Set to 0 Modules and Inverters not selected
                    VQDf.loc[~VQDf['PerfModelKey'].isnull(), 'PerfModelKey'] = VQDf.loc[
                        ~VQDf['PerfModelKey'].isnull(), 'PerfModelKey'].apply(lambda x: x.replace('\ufeff', '')).values
                    CostDf = VQDf[VQDf['PerfModelKey'].isnull()]
                    CostDf = pd.concat([CostDf, VQDf[VQDf['PerfModelKey'] == pvname]])
                    CostDf = pd.concat([CostDf, VQDf[VQDf['PerfModelKey'] == invname]])
                    CostDf = pd.concat([CostDf, VQDf[VQDf['Group'] == 'Battery']])
                    CostDf.drop_duplicates(subset=['BusinessIdentifierCode'], inplace=True)

                    # Exclude Items from CAPEX or Business Case calculation
                    CostDf = CDB.BoMExcludeFlags(CostDf, OrgBomInput=OrgBomInput)

                    CAPEX, OPEX, ViewQuoteDf, AdjDict = fm.Cost_function_db(PVModuleData, InvData, CostDf, Ms, Mp,
                                                                            NumBB, TotalPanels,
                                                                            Adjustments=Adjustments,
                                                                            AreaFigures=AreaFigures)

                    ViewQuoteDf.set_index('Id', inplace=True)
                    ViewQuoteDf.fillna(np.nan, inplace=True)
                    ViewQuoteDf.replace([np.nan], [None], inplace=True)
                    # ADD BASE PRICE and CFACTOR CALCULATION HERE

                    BoM2Update = [{'_Id': int(idBom), 'Quantity': float(ViewQuoteDf.loc[idBom, 'Quantity']),
                                   'TotalCost': float(ViewQuoteDf.loc[idBom, 'TotalCost']),
                                   'NetPrice': float(ViewQuoteDf.loc[idBom, 'NetPrice']),
                                   'PriceAdjustment': float(ViewQuoteDf.loc[idBom, 'PriceAdjustment']),
                                   'TotalPrice': float(ViewQuoteDf.loc[idBom, 'TotalPrice']),
                                   'TotalBasePrice': float(ViewQuoteDf.loc[idBom, 'TotalBasePrice'])} \
                                  for idBom in ViewQuoteDf.index]
                    CDB.UpdateBoM(BoM2Update)

                    # Remove Items that are not selected in SelectConfig from BoMQuote
                    ToDeleteDf = VQDf[((~VQDf['PerfModelKey'].isnull()) & (VQDf['PerfModelKey'] != pvname) & (
                                VQDf['PerfModelKey'] != invname) & (VQDf['Group'] != 'Battery'))]
                    ToDeleteDf.set_index('Id', inplace=True)
                    ToDeleteDf.fillna(np.nan, inplace=True)
                    ToDeleteDf.replace([np.nan], [None], inplace=True)
                    # Make Quantity = 0 for those items to remove
                    if len(ToDeleteDf) > 0:
                        DelBoM = [{'_Id': int(idBom), 'Quantity': None, 'TotalCost': None} for idBom in
                                  ToDeleteDf.index]
                        # 'NetPrice':float(0),
                        # 'PriceAdjustment':float(ToDeleteDf.loc[idBom,'PriceAdjustment']),
                        # 'TotalPrice':float(0),
                        # 'TotalBasePrice':float(0)} for idBom in ToDeleteDf.index] 
                        if len(DelBoM) > 0:
                            CDB.UpdateBoM(DelBoM)

            entity.update(OutputDic)
            entity.update({"status": "Generate Design"})

            OutputDic.update({"InvData": InvID, "PVdata": pvdata})

            ds.put(entity)
            OutputDic.update(energydisplay)
            return jsonify(OutputDic)


        elif (userinput['requesttype'] == 'byname'):
            pvname = userinput['PVName']
            invname = userinput['InvName']
            if 'Ms' or 'Mp' not in userinput:
                SweepConfigDf, SweepList = Opt.SelectDc2ACSweepCombo(criteria, OptDf, pvname, invname)
                OutputDic = {"Config": " PV: {}, Inv:{}".format(pvname, invname), "ConfigResultsSummary": SweepList}
            else:
                try:
                    Ms = userinput['Ms']
                    Mp = userinput['Mp']

                    ConfigSummTuple = (pvname, invname, Ms, Mp)
                    ConfSummary = json.loads(OptDf.loc[ConfigSummTuple].to_json())
                    OutputDic = {
                        "SelectedConfig": {"Config": {"PVName": pvname, "InvName": invname, "Ms": Ms, "Mp": Mp},
                                           "ConfigResultsSummary": ConfSummary}}
                    entity.update(OutputDic)
                except:
                    return jsonify(
                        {"error": "selected configuration missing Ms or Mp fields or combination not found"}), 400





    else:
        errmsg = errmsg + 'Configuration file {} not available'.format(blobname_configs)
        OutputDic = {}

    entity.update({"status": "Generate Design"})
    ds.put(entity)

    OutputDic.update({"messages": errmsg})

    return jsonify(OutputDic), 200


@app.route('/financial_output', methods=['POST'])  # Without storage /techselect_withstorage   
@jwt_authenticated
def financeout():
    ds = datastore.Client()
    gcs = storage.Client()
    message = {}
    # Update finparm from user inputs
    req_json = request.get_json()['Financial']
    userinput = req_json
    userinput['email'] = request.email
    orgname = request.org
    parent_key = ds.key('users', userinput['email'])

    entity = ds.get(key=ds.key('Project', userinput['project_id'], parent=parent_key))
    if not entity:
        message.update({"error": "No project with username {} and project {} exists".format(userinput['email'],
                                                                                            userinput['project_id'])})
        return jsonify(message), 400

    #    entityNone=ds.get(key=ds.key('users','None','Project',userinput['project_id']))
    #   
    #    if entity is None : # None doesnt exist and neither does the email - so just make a new one. Ideally - atleast None should exist from previous step            
    #        entity=datastore.Entity(key=ds.key('users',userinput['email'],
    #                                               'Project',userinput['project_id']))
    #    
    #    if entityNone is not None:
    #        entity.update(entityNone)
    #        ds.delete(key=ds.key('users','None','Project',userinput['project_id']))

    entity.update({"status": "Results Summary"})

    uniqueid = 'Uid_' + userinput['email'] + '_Pid_' + userinput['project_id']

    entchecklist = ['OptMatrix', 'SelectedConfig']
    for ec in entchecklist:
        if ec not in entity:
            return jsonify({"error": "{} not set in datastore".format(ec)}), 400

    #    uschklist = ['selected]
    #    for us in uschklist:
    #        if us not in userinput:
    #            return jsonify({"error": "{} not set in input request".format(us)}),400 

    bucket = gcs.get_bucket(BUCKET_NAME)

    if 'finaloutput' in userinput:
        if 'FinalSummary' in entity:
            if type(entity['FinalSummary']['CashFlow']) == str:
                blobname = entity['FinalSummary']['CashFlow']
                CashFlowDf = pickle.loads(bucket.get_blob(blobname).download_as_string())
                cf2dispdf = CashFlowDf.groupby(pd.Grouper(freq='1Y')).sum()
                cf2dispdf['OPEX+Interest'] = -(
                            cf2dispdf['NetBenefit'] - cf2dispdf['SavingsOnly'])  # +cf2dispdf['interests'])
                OvSummary = entity['FinalSummary']['OverallSummary']
                cf2dispdf['CAPEX'] = OvSummary.get('EffCAPEX', OvSummary['CAPEX'])
                cf2dispdf['CAPEX'].iloc[1:] = 0
                cf2dispdf['Net Cash Flow'] = cf2dispdf['ProjectCashFlow']
                cf2dispdf['Net Cash Flow'].iloc[0] = cf2dispdf['Net Cash Flow'].iloc[0] - \
                                                     entity['FinalSummary']['OverallSummary']['CAPEX']

                cf2dispdf['CumCashFlow'] = cf2dispdf['Net Cash Flow'].cumsum()
                OutputDic = {"AnnualCashFlow": cf2dispdf.to_dict(orient='list'),
                             'Years': [dt.datetime.strftime(l, '%Y') for l in cf2dispdf.index]}

                OverallSummary = entity['FinalSummary']["OverallSummary"]
                FinKPIs = {"Project_payback": OverallSummary['PaybackTime'], "Project_IRR": OverallSummary['IRR']}
                if entity.get('Financial'):
                    entity['Financial'].update({"CashFlow": blobname, "FinKPIs": FinKPIs})
                else:
                    entity.update({"Financial": {"CashFlow": blobname, "FinKPIs": FinKPIs}})

                ds.put(entity)
            else:
                OutputDic = {"AnnualCashFlow": dict(entity['FinalSummary']['CashFlow'])}

            return jsonify(json.loads(json.dumps(OutputDic))), 200

    blobname_configs = RESULTS + uniqueid + '_Configs.pkl'
    blobOpt = bucket.get_blob(blobname_configs)
    s = blobOpt.download_as_string()
    OptDf = pickle.loads(s)

    blobname_ts = RESULTS + uniqueid + '_TSConfigs.pkl'
    blobts = bucket.blob(blobname_ts)
    st = blobts.download_as_string()
    TSOptDf = pickle.loads(st)

    default_finparams = FINPARM['financial']
    FinData = {}
    # LOAD FROM DATASTORE - Taking care to perform a deep copy, as FinData will be updated with DataFrame
    if 'finparams' in entity:
        for t in entity['finparams']:
            FinData[t] = entity['finparams'][t]
    else:
        FinData = FINPARM['financial']  # Load from datastore if modified by user

    if 'EpriceTariffFile' in entity:
        # TariffDic = pickle.loads(bucket.get_blob(USER_UPLOADS +entity['EpriceTariffFile'] ).download_as_string())
        TariffDic = pickle.loads(bucket.get_blob(entity['EpriceTariffFile']).download_as_string())
        FinData.update({"PVTariffSummary": TariffDic['PVTariffSummary']})
        FinData.update({"YearlyTariff": TariffDic['YearlyTariff']})
    else:
        FinData.update({"PVTariffSummary": {"pvtariffAvg": 0, "pvtarifftotal": 0,
                                            "EpriceAvg": default_finparams['e_price'],
                                            "Epricetotal": default_finparams['e_price']}})
        YearlyTariff = pd.DataFrame(index=pd.date_range(start='1/1/2020', periods=8760, freq='1H'),
                                    columns=['PVTariff'])

        # Add electrical price by hour if avaialble
        YearlyTariff['PVTariff'] = 0
        FinData.update({"YearlyTariff": YearlyTariff})

    if 'selectedconfig' not in userinput:
        configsel = entity['SelectedConfig']['Config']
    else:
        configsel = userinput['selectedconfig']

    #    InvData = INVDF.loc[:,entity['Inverter_name']]
    #    PVModuleData = PVDF.loc[:,entity['PV_name']]
    confchklist = ['PVName', 'InvName', 'Ms', 'Mp']
    for ec in confchklist:
        if ec not in configsel:
            return jsonify({"error": "{} not set in config".format(ec)}), 400

    pvname = configsel['PVName']
    invname = configsel['InvName']
    Ms = configsel['Ms']
    Mp = configsel['Mp']
    location_data = Location(entity['Location']['latitude'], entity['Location']['longitude'],
                             tz=entity['Weather']['TZ'])

    ConfigTSTuple = (pvname, invname, Ms, Mp, 'PAC_tot')  # Extract TS of total AC power
    ConfigSummTuple = (pvname, invname, Ms, Mp)
    TSDfsel = TSOptDf[ConfigTSTuple]

    ACOut = pd.Series(np.roll(TSDfsel.values, int(location_data.tz), axis=0), index=TSDfsel.index)
    # ACOut = TSOptDf[ConfigTSTuple]
    ConfSummary = OptDf.loc[ConfigSummTuple]

    # Start cashflow calc from next month of the Ref_year - taken care of in CashFlowCalc already

    year = int(FinData['Ref_year'])
    nextmonth = dt.datetime.now().month + 1
    if 'month' in FinData:
        month = FinData['month']
    else:
        month = nextmonth

    SelFinData = fm.Project_fin_data(FinData, ConfSummary['CAPEX'], ConfSummary['OPEX'])

    [CashFlowDf, Fin_KPIs, error_msg_fin, _, _] = fm.CashFlowCalc(SelFinData, ACOut,
                                                                  startdate='1/{month}/{year}'.format(month=month,
                                                                                                      year=year),
                                                                  detailed=True)

    # Cash flow data to display in yearly resolutions
    cf2dispdf = CashFlowDf.groupby(pd.Grouper(freq='1Y')).sum()
    cf2dispdf['OPEX+Interest'] = -(cf2dispdf['NetBenefit'] - cf2dispdf['SavingsOnly'])  # +cf2dispdf['interests'])
    cf2dispdf['CAPEX'] = ConfSummary['CAPEX']
    cf2dispdf['CAPEX'].iloc[1:] = 0
    cf2dispdf['Net Cash Flow'] = cf2dispdf['FreeCashFlow']
    cf2dispdf['Net Cash Flow'].iloc[0] = cf2dispdf['Net Cash Flow'].iloc[0] - ConfSummary['CAPEX']

    blobname_ts = RESULTS + uniqueid + '_CashFlow.pkl'
    blob = bucket.blob(blobname_ts)
    blob.upload_from_string(pickle.dumps(CashFlowDf))

    entity.update({"status": "Results Summary"})
    if entity.get('Financial'):
        entity['Financial'].update({"CashFlow": blobname_ts, "FinKPIs": Fin_KPIs})
    else:
        entity.update({"Financial": {"CashFlow": blobname_ts, "FinKPIs": Fin_KPIs}})

    ds.put(entity)
    OutputDic = {"FinKPIs": Fin_KPIs, "CashFlow": CashFlowDf.to_dict(orient='list'),
                 'Timestamps': list(CashFlowDf.index.to_native_types())}

    OutputDic.update({"AnnualCashFlow": cf2dispdf.to_dict(orient='list'),
                      'Years': [dt.datetime.strftime(l, '%Y') for l in cf2dispdf.index]})
    return jsonify(OutputDic), 200


@app.route('/physical-layout', methods=['POST'])
@jwt_authenticated
def physicalLayout():
    ds = datastore.Client()
    gcs = storage.Client()
    bucket = gcs.bucket(BUCKET_NAME)
    req_json = request.get_json()['LandConst']
    userinput = req_json
    message = {}

    userinput['email'] = request.email
    orgname = request.org
    parent_key = ds.key('users', userinput['email'])
    entity = ds.get(key=ds.key('Project', userinput['project_id'], parent=parent_key))

    if not entity:
        message.update({"error": "No project with username {} and project {} exists".format(userinput['email'],
                                                                                            userinput['project_id'])})
        return jsonify(message), 400

    uniqueid = 'Uid_' + userinput['email'] + '_Pid_' + userinput['project_id']

    entchecklist = ['SelectedConfig', 'Location', 'OptMatrix']
    for ec in entchecklist:
        if ec not in entity:
            return jsonify({"error": "{} not set in datastore".format(ec)}), 400

    #    uschklist = []
    #    for us in uschklist:
    #        if us not in userinput:
    #            return jsonify({"error": "{} not set in input request".format(us)}),400 

    if "action" in userinput:
        if 'PanelCoords' in entity['Location']:
            blobname = entity['Location']['PanelCoords']
            if blobname:
                try:
                    PolyFile = json.loads(bucket.get_blob(blobname).download_as_string())
                    GeoPanCoords = PolyFile['GeoPanCoords']
                    NumP2Layout = PolyFile["NumPanels2Layout"]
                except:
                    GeoPanCoords = {}
                    NumP2Layout = 0
            else:
                GeoPanCoords = {}
                NumP2Layout = 0
        else:
            GeoPanCoords = {}
            NumP2Layout = 0
        return jsonify({"GeoPanCoords": GeoPanCoords, "NumPanels2Layout": NumP2Layout}), 200

    if 'selectedconfig' not in userinput:
        try:
            configsel = entity['SelectedConfig']['Config']
        except:
            message.update(
                {"error": " project {} does not have a selected configuration".format(userinput['project_id'])})
            return jsonify(message), 400
    else:
        configsel = userinput['selectedconfig']

    confchklist = ['PVName', 'InvName', 'Ms', 'Mp']
    for ec in confchklist:
        if ec not in configsel:
            return jsonify({"error": "{} not set in config".format(ec)}), 400

    pvname = configsel['PVName']
    invname = configsel['InvName']
    Ms = configsel['Ms']
    Mp = configsel['Mp']
    ConfigSummTuple = (pvname, invname, Ms, Mp)

    bucket = gcs.get_bucket(BUCKET_NAME)

    blobname_configs = RESULTS + uniqueid + '_Configs.pkl'
    blobOpt = bucket.get_blob(blobname_configs)
    s = blobOpt.download_as_string()
    OptDf = pickle.loads(s)

    uniqueid = 'Uid_' + userinput['email']

    blobname_pvmod = USER_UPLOADS + uniqueid + '_moduledata.pkl'
    blobpv = bucket.get_blob(blobname_pvmod)
    if blobpv:
        s = blobpv.download_as_string()
        USERPVDF = pickle.loads(s)

    ConfSummary = OptDf.loc[ConfigSummTuple]
    # Power_req = entity['ACpower']*1000
    NumBB = ConfSummary['NumBB']
    # InvData = INVDF.loc[:,invname]
    if pvname in PVDF:
        PVModuleData = PVDF.loc[:, pvname]
    elif pvname in PVSYSDF:
        PVModuleData = PVSYSDF.loc[:, pvname]
    elif blobpv:
        if pvname in USERPVDF:
            PVModuleData = USERPVDF.loc[:, pvname]
    else:
        return jsonify({"Error": "PV Module {} not found in any database".format(pvname)}), 400
    InvData = INVDF.loc[:, invname]

    # in some databases, PV module units are in mm 
    # convert all to m 
    if PVModuleData['LongSide'] / 1000 > 0.1:
        PVModuleData['LongSide'] = PVModuleData['LongSide'] / 1000
    if PVModuleData['ShortSide'] / 1000 > 0.1:
        PVModuleData['ShortSide'] = PVModuleData['ShortSide'] / 1000
    if np.isnan(PVModuleData['LongSide']):
        PVModuleData['LongSide'] = PVModuleData['A_c'] / 0.992
        PVModuleData['ShortSide'] = 0.992

    weatherblob = entity['Weather']['finalweatherfile']
    dataTMY = pickle.loads(bucket.get_blob(weatherblob).download_as_string())

    # PanStringArr,Ptuples =Opt.FeasibleStringRange(PVModuleData,InvData,dataTMY['Temperature'])
    # PaninStringKeys = np.array([x for x in PanStringArr.keys()]) 
    # NuminString = int(PaninStringKeys[np.argmin(np.abs(PaninStringKeys-Ms*Mp))])        
    # TODO : Show Distribution of panels on each area
    # NumP2Layout = Ms*Mp*NumBB       
    NumP2Layout = ConfSummary['NumPanels']  # NuminString*NumBB
    ####################

    GeoCoordFile = entity['Location']['GeoCoords']
    blobname = GeoCoordFile
    bucket = gcs.get_bucket(BUCKET_NAME)
    blob = bucket.get_blob(blobname)
    s = blob.download_as_string()
    LatPolyfromFile = json.loads(s)

    # if ('SpacingParams' not in userinput.keys()):    
    #     SpacingParams = entity['Location']['SpacingParams']
    # else:
    #     SpacingParams = userinput['SpacingParams']
    #     for vals in entity['Location']['SpacingParams']:
    #         if vals not in SpacingParams.keys():
    #             SpacingParams[vals] = entity['Location']['SpacingParams'][vals]
    # try:
    if 'AreaSpacingParams' in userinput:
        AreaSpacingParams = userinput['AreaSpacingParams']
        if AreaSpacingParams == {}:
            AreaSpacingParams = {AreaLabel: entity['Location']['SpacingParams'] for AreaLabel in LatPolyfromFile}
    elif 'AreaSpacingParams' in entity['Location']:
        AreaSpacingParams = entity['Location']['AreaSpacingParams']
    else:
        AreaSpacingParams = {AreaLabel: entity['Location']['SpacingParams'] for AreaLabel in LatPolyfromFile}

    Areas = dict()
    GeoPanOut = dict()
    RealPanOut = dict()
    NumPanels = dict()
    AreaFigures = {"Areawise": {}}
    NumPanels = dict()

    if entity['Location'].get('latitude', 1) > 0:
        # Align to Which Hemisphere ? - Align towards south if >0 , align towards north if <0
        Hemisphere = 'South'
    else:
        Hemisphere = 'North'

    for AreaLabel in LatPolyfromFile:
        Areas[AreaLabel] = Geo2M.Geo2M3D(LatPolyfromFile[AreaLabel])
        AreaAz = GridM.AzofEdges(Areas[AreaLabel])
        SpacingParams = AreaSpacingParams[AreaLabel]

        # if LatPolyfromFile[AreaLabel]['Ext'][0][0]>0:
        #      #RefVector = np.array([0,-1,0]) # Vector facing south
        #      #Hemisphere = 'South'
        #      if SpacingParams['Ti_P']!=0:
        #          SpacingParams['Ti_P']=abs(SpacingParams['Ti_P'])
        # else:
        #      #RefVector = np.array([0,1,0]) # Vector facing north
        #      #Hemisphere = 'North'
        #      if SpacingParams['Ti_P']!=0:
        #          SpacingParams['Ti_P']=abs(SpacingParams['Ti_P']) # Rotae the default panels in the opposite direction

        InstallPlanePoints = Areas[AreaLabel].Real2InPlane(
            Areas[AreaLabel].Coords)  # Project entire shape to installation surface in 2D
        OuterPoly = polygon.orient(Polygon(InstallPlanePoints))

        # Check OuterPoly for orientation            

        # OuterPolyWKeepOut = LinearRing(OuterPoly).parallel_offset(SpacingParams['setback'],"left",join_style=2) # Demarcate keepout areas all along the boundaries
        # OuterPolyWKeepOut = Polygon(OuterPoly).buffer(-SpacingParams['setback']) 
        # OuterPoly should always be anticlockwise oriented
        ProjHoles = [Areas[AreaLabel].Real2InPlane(HoleCoords) for HoleCoords in
                     Areas[AreaLabel].Holes]  # Project the holes also in installation plane
        OuterPolyWKeepOut = polygon.orient(Polygon(OuterPoly).buffer(-SpacingParams['setback']))
        zcoord = OuterPoly.exterior.coords[:][0][2]
        IntersectedHoles = []
        for holes in ProjHoles:
            if Polygon(holes).is_valid:
                IntersectedHoles.append(Polygon(holes).intersection(OuterPolyWKeepOut))
            else:
                IntersectedHoles.append(Polygon(holes).buffer(0).intersection(OuterPolyWKeepOut))

        if not OuterPolyWKeepOut.has_z:
            OuterPolyWKeepOut = Polygon(
                [(coord[0], coord[1], zcoord) for coord in OuterPolyWKeepOut.exterior.coords[:]])
            IntersectedHoles = []
            for holes in ProjHoles:
                IntersectedHoles.append(Polygon([(p[0], p[1], zcoord) for p in holes]).intersection(OuterPolyWKeepOut))
                ######

        S = Polygon(Polygon(OuterPolyWKeepOut).exterior.coords,
                    [Polygon(inner).exterior.coords for inner in (IntersectedHoles) if inner]).buffer(0)

        if type(S) == MultiPolygon:
            # S = Polygon(Polygon([s.exterior.coords[:] for s in S][0]).exterior.coords,[Polygon(inner).exterior.coords for inner in (IntersectedHoles) if inner]).buffer(0.01)
            # S = Polygon(Polygon(OuterPolyWKeepOut).exterior.coords,[Polygon(inner).exterior.coords for inner in (IntersectedHoles) if inner]).buffer(0.01)
            OuterPolyWKeepOut = Polygon(
                [(coord[0], coord[1], zcoord) for coord in OuterPolyWKeepOut.exterior.coords[:]])
            IntersectedHoles = []
            for holes in ProjHoles:
                if Polygon(holes).is_valid:
                    IntersectedHoles.append(
                        Polygon([(p[0], p[1], zcoord) for p in holes]).intersection(OuterPolyWKeepOut))
                else:
                    IntersectedHoles.append(
                        Polygon([(p[0], p[1], zcoord) for p in holes]).buffer(0).intersection(OuterPolyWKeepOut))
            S = Polygon(Polygon(OuterPolyWKeepOut).exterior.coords,
                        [Polygon(inner).exterior.coords for inner in (IntersectedHoles) if inner]).buffer(0.01)

        if not S.has_z:
            S = Polygon([(ex[0], ex[1], zcoord) for ex in S.exterior.coords[:]],
                        [LinearRing([(iN[0], iN[1], zcoord) for iN in p.coords[:]]) for p in S.interiors])

        # S = Polygon(Polygon(OuterPolyWKeepOut).exterior.coords,[Polygon(inner).exterior.coords for inner in (ProjHoles)]) # Prepare input polygon

        # AreaFigures for Cost calculations
        AreaFigures["Areawise"].update({AreaLabel: {"area": S.area,
                                                    "convexhullperimeter": S.convex_hull.length,
                                                    "netperimeter": S.length,
                                                    "tilt": Areas[AreaLabel].tilt,
                                                    "azimuth": Areas[AreaLabel].azimuth,
                                                    "height": max([z[2] for z in InstallPlanePoints])
                                                    }})

        if 'layout' not in SpacingParams:
            SpacingParams['layout'] = 'flat'  # flat, v or butterfly    --, v, /\
        # RefVector = np.array([0,-1,0]) # Southeward pointing vector
        if Areas[AreaLabel].tilt == 0:
            # Flat roof
            if SpacingParams['layout'] == 'flat':
                MultiPanelPolygons, SpacingParams = GridM.FillFlatAreaWithPanels(S, PVModuleData, SpacingParams,
                                                                                 Hemisphere)
                # MultiPanelPolygons = GridM.FillAreaWithPanels(S,PVModuleData,SpacingParams)
                AreaSpacingParams[AreaLabel] = SpacingParams
            else:
                MultiPanelPolygons, SpacingParams = GridM.FillFlatAreaWithVPanels(S, PVModuleData, SpacingParams,
                                                                                  Hemisphere)
                # MultiPanelPolygons = GridM.FillAreaWithPanels(S,PVModuleData,SpacingParams)
                AreaSpacingParams[AreaLabel] = SpacingParams
        else:
            # MultiPanelPolygons = GridM.FillAreaWithPanels_V2(S,PVModuleData,SpacingParams)  # Fill entire area with panels avoiding obstructions and respecting spacing params
            MultiPanelPolygons = GridM.FillAreaWithPanels_V3(S, PVModuleData, SpacingParams, AreaAz=AreaAz)
        NumPanels[AreaLabel] = len(MultiPanelPolygons)
        # GET PANELS REAL COORDINATES (m)
        RealPanList = []
        NumPanels[AreaLabel] = len(MultiPanelPolygons)
        for Pan in MultiPanelPolygons:
            RealPanList.append(Polygon(Areas[AreaLabel].InPlane2Real(
                Pan.exterior.coords[:])))  # Transform all panels to Real coordinate systems
        # RealPanOut[AreaLabel] = GridM.ShapelytoJson(RealPanList)
        # A = GridM.SurfAndPanelsPlot(Areas[AreaLabel].Coords,RealPanList)  

        # GET PANELS GEO COORDINATES (lat,lon)
        GeoPanList = []
        for realpan in RealPanList:
            GeoPanList.append(Polygon(Areas[AreaLabel].m2Geo(
                realpan.exterior.coords[:])))  # Convert values to geo location according to appropriate Area projection
        GeoPanOut[AreaLabel] = GridM.ShapelytoJson(GeoPanList)  # Create Json friendly objects for subsequent transfer

    emsg = "Successful fill"
    # AreaFigures.update({"TotalDCInOut":5}) # 5 m cable by default

    # except:
    #     emsg = "error in  shapes uploaded, files or other inputs"
    #     return jsonify({"error": emsg, "Shape stored in Db": LatPolyfromFile}),400

    #        nameofpicfile = 'solution_1.png'
    #        public_url = 'https://storage.googleapis.com/ecobucket/Results/solution_1.png'
    #       
    # ReturnDict ={"Errmsg": emsg ,"File2Display":str(RESULTS+nameofpicfile+'.png'),
    #            "PicPublicUrl":public_url}

    uniqueid = 'Uid_' + userinput['email'] + '_Pid_' + userinput['project_id']
    blobname = USER_UPLOADS + uniqueid + '_physicallayout.json'

    entity['Location'].update({"PanelCoords": blobname})
    # entity['Location'].update({"SpacingParams":SpacingParams})
    entity['Location'].update({"AreaSpacingParams": AreaSpacingParams})
    entity['Location'].update({"AreaFigures": AreaFigures})
    entity.update({"status": "Physical Layout"})
    ReturnDict = {"Errmsg": emsg, "GeoPanCoords": GeoPanOut, "RealPanCoords": RealPanOut,
                  "NumPanels2Layout": NumP2Layout,
                  "PowerPerPanelkW": PVModuleData['Wp'] / 1000}

    AreaDistribution = entity.get('SelectedConfig_mu', {}).get('AreaDistribution', {})
    if 'SelectedConfig_mu' in entity:
        for AreaLabel in AreaDistribution:
            AreaDistribution[AreaLabel] = min(AreaDistribution[AreaLabel], NumPanels[AreaLabel])
        NumP2Layout = int(np.sum([nump for nump in AreaDistribution.values()]))
        for AreaLabel in AreaDistribution:
            AreaDistribution[AreaLabel] = min(AreaDistribution[AreaLabel], NumPanels[AreaLabel])
            NumP2Layout = int(np.sum([nump for nump in AreaDistribution.values()]))
    ReturnDict.update({"AreaDistribution": AreaDistribution, "NumPanels2Layout": NumP2Layout})
    blob = bucket.blob(blobname).upload_from_string(json.dumps({"GeoPanCoords": GeoPanOut, "RealPanCoords": RealPanOut,
                                                                "NumPanels2Layout": NumP2Layout}),
                                                    content_type='application/json')

    ReturnDict.update({"AreaDistribution": AreaDistribution, "NumPanels2Layout": NumP2Layout})
    ds.put(entity)

    return jsonify(ReturnDict), 200


@app.route('/updatefrompanels', methods=['POST'])
@jwt_authenticated
def updatepanels():
    ds = datastore.Client()
    gcs = storage.Client()
    bucket = gcs.bucket(BUCKET_NAME)

    req_json = request.get_json()['Panels']
    userinput = req_json
    OutputDic = {}
    message = {}
    userinput['email'] = request.email
    orgname = request.org
    # {"PanelsPerArea":{"Area1":34,"Area2":324,"Area3":23}
    parent_key = ds.key('users', userinput['email'])
    entity = ds.get(key=ds.key('Project', userinput['project_id'], parent=parent_key))

    if not entity:
        message.update({"error": "No project with username {} and project {} exists".format(userinput['email'],
                                                                                            userinput['project_id'])})
        return jsonify(message), 400

    entchecklist = ['Location', 'Weather', 'SelectedConfig']
    for ec in entchecklist:
        if ec not in entity:
            return jsonify({"error": "{} not set in datastore".format(ec)}), 400

    uniqueid = 'Uid_' + userinput['email'] + '_Pid_' + userinput['project_id']

    ## USE DIFFERENT DB BASED ON ORG ###
    # qur = ds.query(kind='organisation')
    # qur.add_filter('users','=',userinput['email'])
    # Sol=list(qur.fetch())
    # if Sol != []:
    #     if os.environ['DB_NAME']!=Sol[0]['compdb']:
    #         os.environ['DB_NAME']= Sol[0]['compdb']
    #     orgname =  Sol[0].key.name    
    # else:        
    #     os.environ['DB_NAME'] = 'refdata'  
    orgentity = ds.get(key=ds.key('organisation', orgname))
    os.environ['DB_NAME'] = orgentity.get("compdb", "refdata")
    importlib.reload(CDB)
    # 
    OrgInfo = json.loads(bucket.get_blob(AUXFILES + "organisation_info.json").download_as_string())
    OrgDict = OrgInfo.get(orgname, OrgInfo['Agrola'])
    OrgBomInput = OrgDict.get('bominput', {})

    # Check if FinalSummary is already there 
    blobname = USER_UPLOADS + uniqueid + '_physicallayout.json'

    NumPanel = {}
    TotFromCoords = 0
    TotalPanels = 0
    if userinput.get('PanelsPerArea', {}) == {}:
        if 'NumPanelsPerArea' in entity['Location']:
            NumPanel.update(entity['Location']['NumPanelsPerArea'])
            TotalPanels = sum([Num for _, Num in NumPanel.items()])
        elif blobname:
            PanelCoords = json.loads(bucket.get_blob(blobname).download_as_string())
            TotalPanels = PanelCoords['NumPanels2Layout']
            for AreaLabel in PanelCoords['GeoPanCoords'].keys():
                NumPanel[AreaLabel] = len(PanelCoords['GeoPanCoords'][AreaLabel]['PANELS GEO'])
                TotFromCoords += len(PanelCoords['GeoPanCoords'][AreaLabel]['PANELS GEO'])
            if TotalPanels < TotFromCoords:
                Num2remove = TotFromCoords - TotalPanels
                for AreaLabel in PanelCoords['GeoPanCoords'].keys():  # for AreaLabel in ranked list of areas
                    PrevNum = NumPanel[AreaLabel]
                    NumPanel[AreaLabel] = max(NumPanel[AreaLabel] - Num2remove, 0)
                    Num2remove = max(Num2remove - (PrevNum - NumPanel[AreaLabel]), 0)
            else:
                TotalPanels = TotFromCoords
            entity['Location']['NumPanelsPerArea'] = NumPanel

    else:
        for AreaLabel in userinput['PanelsPerArea']:
            NumPanel[AreaLabel] = userinput['PanelsPerArea'][AreaLabel]
            TotalPanels += userinput['PanelsPerArea'][AreaLabel]
        entity['Location']['NumPanelsPerArea'] = NumPanel

    elecDesign = ELPARM
    tiltArea = entity['Location']['tilt']
    azimuthArea = entity['Location']['azimuth']
    if 'AreaSpacingParams' in entity['Location']:
        AreaSpacingParams = entity['Location']['AreaSpacingParams']
    else:
        AreaSpacingParams = {AreaLabel: elecDesign['systemparams']['SpacingParams'] for AreaLabel in tiltArea}
    if 'AreaFigures' in entity['Location']:
        AreaFigures = entity['Location']['AreaFigures']
    else:
        AreaFigures = {}

    CostData = entity['Cost']
    weatherblob = entity['Weather']['finalweatherfile']
    SelectedConfig = entity['SelectedConfig']
    St = ELPARM['PVstring']
    elecDesign = ELPARM

    dataTMY = pickle.loads(bucket.get_blob(weatherblob).download_as_string())

    location_data = Location(entity['Location']['latitude'], entity['Location']['longitude'],
                             tz=entity['Weather']['TZ'])

    PVName = SelectedConfig['Config']['PVName']
    InvName = SelectedConfig['Config']['InvName']
    Ms = SelectedConfig['Config']['Ms']
    Mp = SelectedConfig['Config']['Mp']
    TotalNumBB = int(SelectedConfig['ConfigResultsSummary']['NumBB'])

    emailuniqueid = 'Uid_' + userinput['email']
    blobname_pvmod = USER_UPLOADS + emailuniqueid + '_moduledata.pkl'
    blobpv = bucket.get_blob(blobname_pvmod)
    if blobpv:
        s = blobpv.download_as_string()
        USERPVDF = pickle.loads(s)

    # Get Inverter and Panel Data
    if PVName in PVDF:
        PVModuleData = PVDF.loc[:, PVName]
    elif PVName in PVSYSDF:
        PVModuleData = PVSYSDF.loc[:, PVName]
    elif blobpv:
        if PVName in USERPVDF:
            PVModuleData = USERPVDF.loc[:, PVName]

    InvData = INVDF.loc[:, InvName]

    blobname = entity['OptMatrix']['OptConfigFile']
    OptDf = pickle.loads(bucket.get_blob(blobname).download_as_string())

    blobname_ts = entity['OptMatrix']['TSConfigFile']
    ALLTSDf = pickle.loads(bucket.get_blob(blobname_ts).download_as_string())

    ######################## REPLACE WITH OPTIMISATION AND SMART SELECTION ########### ################
    ##################### IF ACTUAL PANELS IN PHYSICAL LAYOUT ARE DIFFERENT THAN ELECTRICAL ###########
    ################################################################################## ################

    SurfArea = entity['Location']['SurfArea']
    MaxSurfArea = np.sum([v for v in SurfArea.values()])
    MaxNumPanelsFromArea = int(MaxSurfArea / (PVModuleData.A_c))

    # SubArrayConfig = [(Ms,Mp) for _ in range(int(TotalNumBB))]
    MsMpConfigs = OptDf.loc[PVName, InvName].index
    DetailedArrayConfig = {}
    # Selected Config

    configsel = entity['SelectedConfig']['Config']
    pvname = configsel['PVName']
    invname = configsel['InvName']
    Ms = configsel['Ms']
    Mp = configsel['Mp']
    ConfigSummTuple = (pvname, invname, Ms, Mp)
    ConfigSummary = OptDf.loc[ConfigSummTuple]

    # SHould be same as in OptParamsRuns
    DC2ACmax = elecDesign['BoP_Design']['DC2ACmax']
    DC2ACmin = elecDesign['BoP_Design']['DC2ACmin']

    PanStringArr, Ptuples = Opt.FeasibleStringRange(PVModuleData, InvData, dataTMY['Temperature'], TotalNumPanels=0,
                                                    DCMinMax=[DC2ACmin, DC2ACmax])
    PaninStringKeys = np.array([x for x in PanStringArr.keys()])
    # NuminString = int(PaninStringKeys[np.argmin(np.abs(PaninStringKeys-Ms*Mp))])
    # TODO: #(which value comes closest to Ms*Mp label) - change index in future to num of panels in string for OptDf
    # RecNumPanels = NuminString*TotalNumBB    
    RecNumPanels = int(ConfigSummary['NumPanels'])
    # NuminString must be closest value   
    NuminString = int(PaninStringKeys[np.argmin(np.abs(PaninStringKeys - int(RecNumPanels / TotalNumBB)))])

    errorDic = {}
    if (RecNumPanels == TotalPanels):
        DetailedArrayConfig = {i: PanStringArr[NuminString] for i in range(1, TotalNumBB + 1)}
        TotalNumBB = int(max(TotalNumBB, 1))
        TotalPanels = int(max(TotalPanels, 1))
    elif TotalPanels in PaninStringKeys:
        TotalNumBB = 1  # int(max(TotalNumBB,1))
        # TotalPanels = int(max(TotalPanels,1))
        DetailedArrayConfig = {i: PanStringArr[TotalPanels] for i in range(1, TotalNumBB + 1)}
    else:
        if TotalPanels % (NuminString) == 0:
            TotalNumBB = int(max(TotalPanels / (NuminString), 1))  # Exact number of building blocks just lesser
            TotalPanels = int(max(TotalPanels, 1))
        else:
            # First try to adjust the difference in the last inverter
            # Change number of building blocks and therefore inverters based on number of panels
            NewNumBB = int(TotalPanels / (NuminString))
            PanelsinLastBB = TotalPanels - (NuminString) * (NewNumBB)
            TotalNumBB = int(max(NewNumBB + 1, 1))

            TotalPanels = int(max(TotalPanels, 1))
            # Ensure NumBBs dont change outside limits
            if (TotalPanels * PVModuleData.Wp) / (TotalNumBB * InvData.Paco) < DC2ACmin:
                # Just Make best Combo irrespective of 'best combo'
                TotalNumBB = int(np.ceil((TotalPanels * PVModuleData.Wp) / (
                            DC2ACmin * InvData.Paco)))  # max(int((TotalPanels*PVModuleData.Wp)/(InvData.Paco*DC2ACmin)),1) #max(NewNumBB,1)


            elif (TotalPanels * PVModuleData.Wp) / (TotalNumBB * InvData.Paco) > DC2ACmax:

                TotalNumBB = int(np.ceil((TotalPanels * PVModuleData.Wp) / (DC2ACmax * InvData.Paco)))

            if PanelsinLastBB in PanStringArr.keys():
                DetailedArrayConfig = {i: PanStringArr[NuminString] for i in range(1, TotalNumBB)}
                DetailedArrayConfig.update({TotalNumBB: PanStringArr[PanelsinLastBB]})

            else:
                # Error message to provide change recommendations
                errorDic = {"error": "Number of Panels Mismatch"}
                arr = (np.array([X - PanelsinLastBB for X in PanStringArr.keys()]))

                if len(arr[arr > 0]) > 0:  # Add some more panels
                    AddPanels = int(min(arr[arr > 0]))
                    errorDic.update({"Add": AddPanels, "Total if Add": TotalPanels + AddPanels})
                else:  # No possibility to add more panels exist
                    errorDic.update({"Add": TotalNumBB * (NuminString) + min(PanStringArr.keys()) - TotalPanels,
                                     "Total if Add": TotalNumBB * (NuminString) + min(PanStringArr.keys())})
                if len(arr[arr < 0]) > 0:  # Panel Removal option exists
                    RemovePanels = int(min(-arr[arr < 0]))
                    errorDic.update({"Remove": RemovePanels, "Total if Remove": TotalPanels - RemovePanels})
                else:  # No possibility to remove panels exist
                    errorDic.update({"Remove": PanelsinLastBB, "Total if Remove": NewNumBB * (NuminString)})
                errorDic.update({"Num Inverters": TotalNumBB})

                # Make a default PanelsinLastBB - which is the nearest

                DetailedArrayConfig = {i: PanStringArr[NuminString] for i in range(1, TotalNumBB)}
                DetailedArrayConfig.update({TotalNumBB: PanStringArr[arr[np.argmin(abs(arr))] + PanelsinLastBB]})
                ArrayPanels1 = arr[np.argmin(abs(arr))] + PanelsinLastBB + NuminString * (TotalNumBB - 1)
                err1 = abs(ArrayPanels1 - TotalPanels)
                # Alternative way without provided NuminString
                # Free Sizing               
                RemPanels = TotalPanels
                PaninString = []  # [arrP[np.argmin(abs(arr))]+PanelsinString]
                for i in range(TotalNumBB, 0, -1):
                    PanelsinString = int(RemPanels / (i))
                    arrP = (np.array([X - PanelsinString for X in PanStringArr.keys()]))
                    RemPanels = RemPanels - (arrP[np.argmin(abs(arrP))] + PanelsinString)
                    PaninString.append(arrP[np.argmin(abs(arrP))] + PanelsinString)
                AltDetailedArrayConfig = {i: PanStringArr[j] for i, j in zip(range(1, TotalNumBB + 1), PaninString)}
                err2 = RemPanels
                if err2 < err1:
                    DetailedArrayConfig = AltDetailedArrayConfig
    # Array values can only be strings not numbers               
    # entity.update(json.loads(json.dumps({"StringArrayConfig":{str(k): np.array_str(np.array(v,dtype=object)) for k,v in DetailedArrayConfig.items()}})))      
    entity.update(
        json.loads(json.dumps({"StringArrayConfig": {str(k): str(v) for k, v in DetailedArrayConfig.items()}})))

    # return jsonify(errorDic),400    

    ######## ============== BREAK AND RETURN ENERGY =================            
    if 'PanelsPerArea' in userinput:
        # Only make a check for total energy and string design
        NumBB = {}
        AreaResults = {}
        ACEnergy = 0
        DCEnergy = 0
        for AreaLabel in tiltArea:

            SpacingParams = AreaSpacingParams[AreaLabel]
            NumBB[AreaLabel] = (TotalNumBB / TotalPanels) * NumPanel[AreaLabel]

            # Convert to PVLib convention, clockwise +ve from 0 to 360° 
            # PVlib Convention - East of North. (S=180,N=0,E=90,W=270)
            # 3D drawing Convention - West of South (S=0,N=180,E=90,W=270)
            #
            # if tiltArea[AreaLabel]==0:    
            #     azimuth = azimuthArea[AreaLabel]+SpacingParams['Az_P'] # Azimuth Panels only for flat roofs
            #     tilt = tiltArea[AreaLabel]+SpacingParams['Ti_P']
            # else:
            #     azimuth = azimuthArea[AreaLabel]
            #     tilt = tiltArea[AreaLabel]

            if tiltArea[AreaLabel] == 0:
                azimuth = (-azimuthArea[AreaLabel]) + 180 - SpacingParams['Az_P']  # Only for flat roofs
                tilt = tiltArea[AreaLabel] + SpacingParams['Ti_P']  # TODO: SpacingParams[AreaLabel][Ti_P]
            else:
                azimuth = -azimuthArea[AreaLabel]  # +180-SpacingParams[AreaLabel]['Az_P']
                tilt = tiltArea[AreaLabel]

            # azimuth = 180-azimuth # Convert from Eest of South to East of North inside the function    
            # if azimuth<0:
            #     azimuth = -azimuth
            # else:
            #     azimuth = 360-azimuth

            PVString = PVsel.PVString(tilt, azimuth, St['Albedo'], Ms, Mp, NumBB[AreaLabel])

            TS, Agg = Opt.SingleEnergyCalc(dataTMY, location_data, PVModuleData, InvData, PVString, elecDesign,
                                           NumPanel[AreaLabel], SpacingParams)
            ACEnergy += Agg['ACEnergy'].values[0]
            DCEnergy += Agg['DCEnergy'].values[0]

        OverallSummary = {"ACEnergy": ACEnergy,
                          "DCEnergy": DCEnergy,
                          "NumPanels": TotalPanels,
                          "NumBB": TotalNumBB,
                          "DCPower": PVModuleData.Wp * TotalPanels / 1000,
                          "ACPower": InvData.Paco * TotalNumBB / 1e3}
        OverallSummary['StringConf'] = json.loads(
            json.dumps({str(i): {"Ms": DetailedArrayConfig[i][0][0], "Mp": int(np.sum([len(DetailedArrayConfig[i][mppt])
                                                                                       for mppt in range(
                    len(DetailedArrayConfig[i]))])), "NumBB": 1} for i in DetailedArrayConfig}))

        # i is num of Building blocks
        OverallSummary['StringConfDetailed'] = json.loads(json.dumps(
            {str(i): {"NumMPPT": len(DetailedArrayConfig[i]), "NumStringsperMPPT": [len(DetailedArrayConfig[i][mppt])
                                                                                    for mppt in
                                                                                    range(len(DetailedArrayConfig[i]))],
                      "PanelsinSeriesperString": [DetailedArrayConfig[i][mppt][0] for mppt in
                                                  range(len(DetailedArrayConfig[i]))],
                      "TotalParallelStrings": int(np.sum([len(DetailedArrayConfig[i][mppt])
                                                          for mppt in range(len(DetailedArrayConfig[i]))]))} for i in
             DetailedArrayConfig}))

        OutputDic = {"OverallSummary": OverallSummary,
                     "warningmismatch": errorDic
                     }
        ds.put(entity)

        return jsonify(OutputDic), 200

    ######## ============== BREAK AND RETURN ENERGY =================   

    """   
    if (RecNumPanels > TotalPanels): # Less Panels placed
        # Diff = RecNumPanels - TotalPanels
        PanelsinLastBB = TotalPanels%()
        TotalNumBBUpDate = int((TotalPanels/(Ms*Mp)))


        if PanelsinLastBB in PanStringArr.keys(): # If it is part of feasible combinations
            DetailedArrayConfig[TotalNumBBUpDate+1]=PanStringArr[PanelsinLastBB] # Modify the last BB
            TotalNumPanels=PanelsinLastBB+TotalNumBBUpDate*(Ms*Mp)
            TotalNumBBUpDate+=1
        # elif PanelsinLastBB ==0: 


        #     TotalNumBBUpDate = int((TotalPanels/(Ms*Mp)))
        #     TotalNumPanels = TotalNumBBUpDate*(Ms*Mp)

        else:

          #  arr = (np.array([PanelsinLastBB-X for X in PanStringArr.keys() if X<PanelsinLastBB]))
            arr = (np.array([PanelsinLastBB-X for X in PanStringArr.keys()] )) # Allow to add or remove
            MinPanelsinLastBB = min(PanStringArr.keys()) #TotalNumBBUpDate*(Ms*Mp)
            MaxPanelsinLastBB = max(PanStringArr.keys())
            InverterChange = 0
            errorDic = {}
            if arr.size==0:

                if PanelsinLastBB < MinPanelsinLastBB:
                    AddPanels = MinPanelsinLastBB-PanelsinLastBB
                    TotalNumPanels = TotalNumBBUpDate*(Ms*Mp)+MinPanelsinLastBB

                    return jsonify({"error":"Panel Number Mismatch - Too few panels for last inverter ","Add":AddPanels,"Total if Add":TotalNumPanels,
                                    "Remove":-PanelsinLastBB, "Total if Remove":TotalNumBBUpDate*(Ms*Mp)}),400
                elif PanelsinLastBB > MaxPanelsinLastBB:
                    RemovePanels = PanelsinLastBB-MaxPanelsinLastBB
                    TotalNumPanels =  TotalNumBBUpDate*(Ms*Mp)+MaxPanelsinLastBB                   

                    return jsonify({"error":"Panel Number Mismatch - Too many panels for last inverter","Remove":-RemovePanels,
                                    "Total if Remove":TotalNumPanels, "Add":MinPanelsinLastBB-RemovePanels,
                                    "Total if Add":(TotalNumBBUpDate+1)*(Ms*Mp)+MinPanelsinLastBB}),400
                return jsonify(errorDic),400    
            else:

                if len(arr[arr<0])>0:
                    AddPanels = int(-np.amax(arr[arr<0]))

                else:
                    AddPanels= MinPanelsinLastBB-PanelsinLastBB
                    InverterChange = 1

                if len(arr[arr>0])>0:     
                    RemovePanels = int(np.amin(arr[arr>0]))
                else:
                    RemovePanels = PanelsinLastBB-MaxPanelsinLastBB
                    InverterChange = -1
                TotalNumPanelsifAdd = TotalNumBBUpDate*(Ms*Mp)+PanelsinLastBB+AddPanels
                TotalNumPanelsifRemove = TotalNumBBUpDate*(Ms*Mp)+PanelsinLastBB-RemovePanels

                if InverterChange == 0:
                    return jsonify({"error":"Panel Number Mismatch - Too many / too few panels for last inverter","Remove":RemovePanels,
                                "Total if Remove":int(TotalNumPanelsifRemove), "Add":int(AddPanels),
                                "Total if Add":int(TotalNumPanelsifAdd)}),400
                else:
                    return jsonify({"error":"Panel Number Mismatch - Too many / too few panels for last inverter","Remove":RemovePanels,
                                "Total if Remove":int(TotalNumPanelsifRemove), "Add":int(AddPanels),
                                "Total if Add":int(TotalNumPanelsifAdd),"Inverter Num Change by":InverterChange}),400

                    #NewPanelsinLastBB = PanelsinLastBB-np.amin(arr)

            # Don't automatically modify number of panels
            # else:                             
            #     NewPanelsinLastBB = PanelsinLastBB-np.amin(arr)
            #     DetailedArrayConfig[TotalNumBBUpDate+1]=PanStringArr[NewPanelsinLastBB]
            #     TotalNumPanels = NewPanelsinLastBB+TotalNumBBUpDate*(Ms*Mp)
            #     TotalNumBBUpDate+=1

        TotalNumBB = int(max(TotalNumBBUpDate,1))
        TotalPanels = int(max(TotalNumPanels,1))      
    elif (RecNumPanels<TotalPanels): # More Panels are placed
        PanelsinLastBB = TotalPanels-RecNumPanels
        TotalNumBBUpDate = int((TotalPanels/(Ms*Mp)))
        DetailedArrayConfig = {i:PanStringArr[NuminString] for i in range(1,TotalNumBBUpDate+1)}
        if PanelsinLastBB in PanStringArr.keys():
            DetailedArrayConfig[TotalNumBBUpDate+1]= PanStringArr[PanelsinLastBB]

            TotalNumPanels=PanelsinLastBB+TotalNumBBUpDate*(Ms*Mp)
            TotalNumBBUpDate+=1

        elif PanelsinLastBB ==0: # One inverter needs to be added
            TotalNumBBUpDate = int((TotalPanels/(Ms*Mp)))
            TotalNumPanels = TotalNumBBUpDate*(Ms*Mp)

        else:
            #arr = (np.array([PanelsinLastBB-X for X in PanStringArr.keys() if X<PanelsinLastBB]))
            arr = (np.array([PanelsinLastBB-X for X in PanStringArr.keys()] )) # Allow to add or remove
            if arr.size==0:
                paneladjustment=0
                MinPanelsinLastBB = min(PanStringArr.keys()) #TotalNumBBUpDate*(Ms*Mp)
                MaxPanelsinLastBB = max(PanStringArr.keys())
                if PanelsinLastBB < MinPanelsinLastBB:
                    AddPanels = MinPanelsinLastBB-PanelsinLastBB
                    TotalNumPanels = TotalNumBBUpDate*(Ms*Mp)+MinPanelsinLastBB
                    return jsonify({"error":"Panel Number Mismatch - Too few panels for last inverter ","Add":AddPanels,"Total if Add":TotalNumPanels,
                                    "Remove":-PanelsinLastBB, "Total if Remove":TotalNumBBUpDate*(Ms*Mp)}),400
                elif PanelsinLastBB > MaxPanelsinLastBB : 
                    RemovePanels = PanelsinLastBB-MaxPanelsinLastBB
                    TotalNumPanels =  TotalNumBBUpDate*(Ms*Mp)+MaxPanelsinLastBB                   
                    return jsonify({"error":"Panel Number Mismatch - Too many panels for last inverter","Remove":-RemovePanels,
                                    "Total if Remove":TotalNumPanels, "Add":MinPanelsinLastBB-RemovePanels,
                                    "Total if Add":(TotalNumBBUpDate+1)*(Ms*Mp)+MinPanelsinLastBB}),400
            else:  
                if len(arr[arr<0])>0:
                    AddPanels = int(-np.amax(arr[arr<0]))
                else:
                    AddPanels=int(0)
                if len(arr[arr>0])>0:     
                    RemovePanels = int(np.amin(arr[arr>0]))
                else:
                    RemovePanels = int(0)
                TotalNumPanelsifAdd = TotalNumBBUpDate*(Ms*Mp)+PanelsinLastBB+AddPanels
                TotalNumPanelsifRemove = TotalNumBBUpDate*(Ms*Mp)+PanelsinLastBB-RemovePanels
                return jsonify({"error":"Panel Number Mismatch - Too many / too few panels for last inverter","Remove":int(RemovePanels),
                                "Total if Remove":int(TotalNumPanelsifRemove), "Add":int(AddPanels),
                                "Total if Add":int(TotalNumPanelsifAdd)}),400

                # paneladjustment = np.amin(arr)
                # NewPanelsinLastBB = PanelsinLastBB-paneladjustment
                # DetailedArrayConfig[TotalNumBB]=PanStringArr[int(NewPanelsinLastBB)] 
                # TotalNumPanels=PanelsinLastBB+TotalNumBBUpDate*(Ms*Mp)
                # TotalNumBBUpDate+=1
        TotalNumBB = int(max(TotalNumBBUpDate,1))
        TotalPanels = int(max(TotalNumPanels,1))
    else:
        DetailedArrayConfig = {i:PanStringArr[NuminString] for i in range(1,TotalNumBB+1)}
        TotalNumBB = int(max(TotalNumBB,1))
        TotalPanels = int(max(TotalPanels,1))

    """

    # STORE TotalNumBBUpDate, DetaileArrayConfig and TotalNumPanels in entity

    # if (RecNumPanels > TotalPanels): # Less Panels placed
    #     Diff = RecNumPanels - TotalPanels

    #     if Diff < NuminString: # Only last BB needs to change
    #         PaninLastBB = TotalPanels%(Ms*Mp)            
    #         SubArrayConfig.pop()
    #         SearchList = [(abs(M[0]*M[1]-PaninLastBB),M) for M in MsMpConfigs]
    #         SearchList.sort(key=lambda x:x[0])  
    #         if SearchList[0][0]!=0:
    #             if PaninLastBB ==0:
    #                 SubArrayConfig.append((PaninLastBB,0))
    #             else:    
    #                 SubArrayConfig.append((PaninLastBB,1))  
    #         else:
    #             LastBB = SearchList[0][1]
    #             SubArrayConfig.append(LastBB)
    #     else:   # Remove NumBBs    
    #         TEMPNumBB = (int(np.floor(TotalPanels/(Ms*Mp))))            

    #         SubArrayConfig = [(Ms,Mp) for _ in range(TEMPNumBB)]
    #         PaninLastBB = TotalPanels%(Ms*Mp)
    #         SearchList = [(abs(M[0]*M[1]-PaninLastBB),M) for M in MsMpConfigs]
    #         SearchList.sort(key=lambda x:x[0])
    #         if SearchList[0][0]!=0:
    #             if PaninLastBB ==0:
    #                 SubArrayConfig.append((PaninLastBB,0))
    #             else:    
    #                 SubArrayConfig.append((PaninLastBB,1))    
    #         else:
    #             LastBB = SearchList[0][1]
    #             SubArrayConfig.append(LastBB)
    #         TotalNumBB = len(SubArrayConfig)
    # else:
    #     Diff = TotalPanels-RecNumPanels
    #     PP = [(abs(M[0]*M[1]),M) for M in MsMpConfigs]
    #     PP.sort(key=lambda x:x[0])

    #     if Diff in range(PP[0][0],PP[-1][0]): # Add a BB 
    #         PaninLastBB = Diff #TotalPanels%(Ms*Mp)+NuminString     

    #         SearchList = [(abs(M[0]*M[1]-PaninLastBB),M) for M in MsMpConfigs]
    #         SearchList.sort(key=lambda x:x[0])
    #         if SearchList[0][0]!=0:
    #             if PaninLastBB ==0:
    #                 SubArrayConfig.append((PaninLastBB,0))
    #             else:    
    #                 SubArrayConfig.append((PaninLastBB,1))   
    #         else:
    #             LastBB = SearchList[0][1]
    #             SubArrayConfig.append(LastBB)

    #         TotalNumBB = len(SubArrayConfig)
    #     elif Diff < PP[0][0]: # is less than minumum then adjust last BB
    #         PaninLastBB = TotalPanels%(Ms*Mp)            

    #         SearchList = [(abs(M[0]*M[1]-PaninLastBB),M) for M in MsMpConfigs]
    #         SearchList.sort(key=lambda x:x[0])
    #         if SearchList[0][0]!=0:
    #             if PaninLastBB ==0:
    #                 SubArrayConfig.append((PaninLastBB,0))
    #             else:    
    #                 SubArrayConfig.append((PaninLastBB,1))   
    #         else:
    #             LastBB = SearchList[0][1]
    #             SubArrayConfig.append(LastBB)

    #         TotalNumBB = len(SubArrayConfig)

    #     elif Diff > PP[-1][0]: # is more than the maximum then
    #         TEMPNumBB = (int(np.floor(TotalPanels/(Ms*Mp))))            

    #         SubArrayConfig = [(Ms,Mp) for _ in range(TEMPNumBB)]
    #         PaninLastBB = TotalPanels%(Ms*Mp)
    #         SearchList = [(abs(M[0]*M[1]-PaninLastBB),M) for M in MsMpConfigs]
    #         SearchList.sort(key=lambda x:x[0])
    #         if SearchList[0][0]!=0:
    #             if PaninLastBB ==0:
    #                 SubArrayConfig.append((PaninLastBB,0))
    #             else:    
    #                 SubArrayConfig.append((PaninLastBB,1))   
    #         else:
    #             LastBB = SearchList[0][1]
    #             SubArrayConfig.append(LastBB)

    #         TotalNumBB = len(SubArrayConfig)

    ######################## REPLACE WITH OPTIMISATION AND SMART SELECTION ###########
    ##################################################################################
    St = ELPARM['PVstring']
    elecDesign = ELPARM

    if 'ActiveQuote' in entity:
        if 'ActiveQuote' in userinput:
            ActiveQuote = userinput['ActiveQuote']
        else:
            ActiveQuote = entity['ActiveQuote']
        #########################
        ### Cost from Db here
        ### 
        ViewQuoteDf = CDB.GetBoMQuote(userinput['project_id'], ActiveQuote['QuoteID'], ActiveQuote['RevID'])

        ViewQuoteDf.loc[~ViewQuoteDf['PerfModelKey'].isnull(), 'PerfModelKey'] = ViewQuoteDf.loc[
            ~ViewQuoteDf['PerfModelKey'].isnull(), 'PerfModelKey'].apply(lambda x: x.replace('\ufeff', '')).values
        CostDf = ViewQuoteDf[ViewQuoteDf['PerfModelKey'].isnull()]
        CostDf = pd.concat([CostDf, ViewQuoteDf[ViewQuoteDf['PerfModelKey'] == PVName]])
        CostDf = pd.concat([CostDf, ViewQuoteDf[ViewQuoteDf['PerfModelKey'] == InvName]])
        CostDf = pd.concat([CostDf, ViewQuoteDf[ViewQuoteDf['Group'] == 'Battery']])
        CostDf.drop_duplicates(subset=['BusinessIdentifierCode'], inplace=True)

        # Exclude Items from CAPEX or Business Case calculation
        CostDf = CDB.BoMExcludeFlags(CostDf, OrgBomInput=OrgBomInput)

        CostSelected = CostDf

    else:
        CostSelected = {'PVCost': CostData['PVCost'][PVName],
                        'InvCost': CostData['InvCost'][InvName]}
        CostSelected.update({k: v for k, v in CostData.items() if ((k != 'PVCost') and (k != 'InvCost'))})
        Curr = CostData['Curr']

    #########################

    FinData = {}
    if 'finparams' in entity:
        for t in entity['finparams']:
            FinData[t] = entity['finparams'][t]

    else:
        FinData = FINPARM['financial']
    default_finparams = FINPARM['financial']

    if 'EpriceTariffFile' in entity:
        # TariffDic = pickle.loads(bucket.get_blob(USER_UPLOADS +entity['EpriceTariffFile'] ).download_as_string())

        try:
            TariffDic = pickle.loads(bucket.get_blob(entity['EpriceTariffFile']).download_as_string())
        except:
            TariffDic = pickle.loads(bucket.get_blob(USER_UPLOADS + entity['EpriceTariffFile']).download_as_string())

        FinData.update({"PVTariffSummary": TariffDic['PVTariffSummary']})
        FinData.update({"YearlyTariff": TariffDic['YearlyTariff']})
    else:
        FinData.update({"PVTariffSummary": {"pvtariffAvg": 0, "pvtarifftotal": 0,
                                            "EpriceAvg": default_finparams['e_price'],
                                            "Epricetotal": default_finparams['e_price']}})
        YearlyTariff = pd.DataFrame(index=pd.date_range(start='1/1/2020', periods=8760, freq='1H'),
                                    columns=['PVTariff'])

        # Add electrical price by hour if avaialble
        YearlyTariff['PVTariff'] = 0
        FinData.update({"YearlyTariff": YearlyTariff})
    if 'DemandTS' in entity['Demand']:
        DemandTS = pickle.loads(bucket.get_blob(entity['Demand']['DemandTS']).download_as_string())
        FinData.update({"DemandTS": DemandTS})

    OverallSummary = {}

    NumBB = {}
    AreaResults = {}
    TotalCAPEX = 0
    TotalOPEX = 0
    TotalIncRad = 0
    OptDf = pd.DataFrame()

    SysAttribs = ['Vmp', 'Imp', 'Pmp', 'PAC_inv', 'PAC_tot', 'PDC']
    TEOutindex = pd.MultiIndex.from_product([range(Ms, Ms + 1), range(Mp, Mp + 1), SysAttribs],
                                            names=['Ms', 'Mp', 'Attrib'])
    TotalEOut = pd.DataFrame(index=dataTMY.index, columns=TEOutindex)
    TotalEOut[Ms, Mp, 'PDC'] = 0
    TotalEOut[Ms, Mp, 'PAC_tot'] = 0
    TotalEOut[Ms, Mp, 'IncRad'] = 0
    AggCriteria = ['DCEnergy', 'ACEnergy', 'kWh/kWp', 'LCOE', 'IRR', 'PaybackTime', 'CAPEX', 'OPEX',
                   'NumPanels', 'TotalIncRad', 'PR', 'Tilt', 'Azimuth', 'PanelOrientation']
    # Ensuring same AggDf indexing as that in OptiModule, even though only 1 Ms and Mp combo is used
    Aggindex = pd.MultiIndex.from_tuples([(Ms, Mp)], names=['Ms', 'Mp'])
    # AggDf = pd.DataFrame(index=Aggindex, columns = AggCriteria)

    # Chose different if sql db used or not
    if 'ActiveQuote' in entity:

        BoMindex = pd.MultiIndex.from_product([[a for a in tiltArea], [k for k in CostSelected['Id']]], sortorder=0)

        BoMDf = pd.DataFrame(index=BoMindex, columns=['TotalCost', 'Num', 'Id', 'Name', 'Unit', 'CostperUnit', 'Curr'])
    else:
        BoMindex = pd.MultiIndex.from_product([[a for a in tiltArea], [k for k in CostData.keys() if k != 'Curr']],
                                              sortorder=0)
        BoMDf = pd.DataFrame(index=BoMindex, columns=['TotalCost', 'Num', 'CostperUnit', 'Unit', 'Name'])

    LossChain = {}
    LossDf = pd.DataFrame(columns=['%', 'kWh', 'cum %'])

    day = 21
    dispmonth = {"spring": 4, "summer": 7, "autumn": 9, "winter": 12}
    Trends = {}
    TotalTrends = {}
    TStoDisp = {}
    TZ = int(entity['Weather']['TZ'])

    for AreaLabel in tiltArea:

        SpacingParams = AreaSpacingParams[AreaLabel]
        NumBB[AreaLabel] = (TotalNumBB / TotalPanels) * NumPanel[AreaLabel]

        # Convert to PVLib convention, clockwise +ve from 0 to 360° 
        # tilt = 0 Area is defined
        if tiltArea[AreaLabel] == 0:
            azimuth = (-azimuthArea[AreaLabel]) + 180 - SpacingParams['Az_P']  # Only for flat roofs
            tilt = tiltArea[AreaLabel] + SpacingParams['Ti_P']  # TODO: SpacingParams[AreaLabel][Ti_P]
        else:
            azimuth = -azimuthArea[AreaLabel]  # +180-SpacingParams[AreaLabel]['Az_P']
            tilt = tiltArea[AreaLabel]
        # if azimuth<0:
        #     azimuth = -azimuth
        # else:
        #     azimuth = 360-azimuth

        PVString = PVsel.PVString(tilt, azimuth, St['Albedo'], Ms, Mp, NumBB[AreaLabel])

        TSDf, AggDf, _, BoM = Opt.SingleCalc(dataTMY, location_data, PVModuleData, InvData, PVString, elecDesign,
                                             CostSelected.copy(),
                                             FinData, NumPanel[AreaLabel], SpacingParams)

        # BOM Not used for SQL db anymore, check if used for noSQL and deprecate
        # BoM : pd.DataFrame, if sql db is used
        # BoM : dict if noSQL used

        # TotalRadCollected[AreaLabel] =  PVcalc.WeatherDatatoDisplay(dataTMY,location_data,tiltArea[AreaLabel],azimuthArea[AreaLabel])
        AggDf = AggDf.astype(float)
        AggDf.dropna(axis=0, inplace=True)
        AggDf.loc[(Ms, Mp), 'NumPanels'] = NumPanel[AreaLabel]
        AggDf.loc[(Ms, Mp), 'Tilt'] = PVString.Tilt
        AggDf.loc[(Ms, Mp), 'Azimuth'] = PVString.Azimuth
        # if 'layout' in SpacingParams:
        #     if SpacingParams['layout']!='flat':
        #         PO = 'E-W'
        #     else:
        #         PO = 'N-S'
        # else:
        PO = SpacingParams['orientation']
        AggDf.loc[(Ms, Mp), 'PanelOrientation'] = PO

        # =====DEPRECATE . Needded only for nSQL db ======
        TotalCAPEX += AggDf.loc[(Ms, Mp), 'CAPEX']
        TotalOPEX += AggDf.loc[(Ms, Mp), 'OPEX']
        # ====== DEPRECATE ==================
        TotalIncRad += AggDf.loc[(Ms, Mp), 'TotalIncRad']

        TotalEOut[Ms, Mp, 'PAC_tot'] = TotalEOut[Ms, Mp, 'PAC_tot'] + TSDf[Ms, Mp, 'PAC_tot']

        TotalEOut[Ms, Mp, 'PDC'] = TotalEOut[Ms, Mp, 'PDC'].add(TSDf[Ms, Mp, 'PDC'])
        TotalEOut[Ms, Mp, 'IncRad'] = TotalEOut[Ms, Mp, 'IncRad'].add(TSDf[Ms, Mp, 'IncRad'])

        AreaResults[AreaLabel] = json.loads(AggDf.to_json(orient='records'))[0]

        # CAPEX Price Adjustments

        # TODO: Save BoM directly to BoMQuote in Db and eliminate the use of BoMDf here 

        #############
        if 'ActiveQuote' in entity:

            for ind in BoM['Id']:
                BoMDf.loc[(AreaLabel, ind), ['TotalCost', 'Num', 'Id', 'Name', 'Unit', 'CostperUnit', 'Curr']] = \
                    BoM.loc[
                        BoM['Id'] == ind, ['TotalCost', 'Quantity', 'Id', 'Name', 'Unit', 'Cost', 'Currency']].values
        else:
            for costitems in CostData.keys():
                if costitems != 'Curr':
                    BoMDf.loc[(AreaLabel, costitems)] = BoM.loc[costitems]
            BoMDf['Curr'] = Curr

        ############

        VIEW = 0
        Trends = {}
        for month in dispmonth:
            VIEW = TSDf.shift(TZ).loc[(TSDf.index.day == day) & (TSDf.index.month == dispmonth[month])] / 1000  # To kWh

            Trends[month] = {"PAC_tot": json.loads(VIEW[Ms, Mp, 'PAC_tot'].to_json(orient='values'))}
            Trends[month].update({"IncRad": json.loads(VIEW[Ms, Mp, 'IncRad'].to_json(orient='values'))})
            # Trends[month].update({"Tot":TSDf[Ms,Mp,'IncRad'].sum()/1000})

        TStoDisp[AreaLabel] = Trends
        LossChain[AreaLabel], LossChainkWhDf = PVcalc.LossChain(dataTMY, location_data, PVModuleData, InvData, PVString,
                                                                elecDesign)
        if len(LossDf.index) == 0:
            LossDf = pd.DataFrame(columns=['Items', '%', 'kWh'], index=LossChain[AreaLabel].keys())
            LossDf['Items'] = LossChainkWhDf['Items']
            LossDf['kWh'] = LossChainkWhDf['kWh'] * NumPanel[AreaLabel] / (
                        PVString.Ms * PVString.Mp)  # Convert per string energy to overall
        else:
            LossDf['kWh'] = LossDf['kWh'] + LossChainkWhDf['kWh'] * NumPanel[AreaLabel] / (PVString.Ms * PVString.Mp)
    # ends AreaLabel Loop here

    # if 'getvalue' in userinput:
    #     if 'ACenergy' in userinput['getvalue']:    
    #         return jsonify({"ACEnergy":TotalEOut[Ms,Mp,'PAC_tot'].sum()/1000}),200 

    # Put TotalEOut from all areas together for totalised trends
    for month in dispmonth:
        VIEW = TotalEOut.shift(TZ).loc[
                   (TotalEOut.index.day == day) & (TotalEOut.index.month == dispmonth[month])] / 1000
        TotalTrends[month] = {"PAC_tot": json.loads(VIEW[Ms, Mp, 'PAC_tot'].to_json(orient='values'))}
        TotalTrends[month].update({"IncRad": json.loads(VIEW[Ms, Mp, 'IncRad'].to_json(orient='values'))})
        # TotalTrends[month].update({"Tot":TotalEOut[Ms,Mp,'IncRad'].sum()/1000})
    TStoDisp['Total'] = TotalTrends
    #   Add LossChain Values Together from all Areas
    TotalLossChainDf = PVcalc.LossChainPerCent(LossDf)

    TotalLossChainDf.index = TotalLossChainDf.index.astype(str)

    # Build up final BOM

    ConfFinData = fm.Project_fin_data(FinData, TotalCAPEX, TotalOPEX)
    year = int(FinData['Ref_year'])
    nextmonth = dt.datetime.now().month + 1
    if nextmonth > 12:
        nextmonth = 1
        year += 1
    # Jump to next year if current year is over    
    if 'month' in FinData:
        month = FinData['month']
    else:
        month = nextmonth

    # Get Consumption values
    if 'DemandTS' in entity['Demand']:
        DemandTS = pickle.loads(bucket.get_blob(entity['Demand']['DemandTS']).download_as_string())

    # Delete below    
    # elif 'Battery' in entity:
    #     BattTSblob=entity['Battery']['TS']
    #     FinalTS = pickle.loads(bucket.get_blob(BattTSblob).download_as_string())
    #     DemandTS = FinalTS['Demand']
    #     Demandblobname = USER_UPLOADS+uniqueid+'_demandTS.pkl'
    #     bucket.blob(Demandblobname).upload_from_string(pickle.dumps(DemandTS))
    #     entity['Demand']['DemandTS'] = Demandblobname
    #     # Refresh BattTS
    #     BatterySpec = entity['Battery']['BatterySpec']

    else:
        DemandTS = TotalEOut[
            Ms, Mp, 'PAC_tot']  # if no demand values avaialble from battery step, assume all PV is consumed 

    # Financial Values Get
    if 'Adjustments' in entity['finparams']:
        Adjustments = entity['finparams']['Adjustments']
    else:
        Adjustments = {}

    ############### CALCULATE CAPEX HERE ############
    ### CALCULATE SOLAR ONLY AND STORE IN CONFIGRESULTSSUMMARY ####
    if 'nm' in FinData['PVTariffSummary']:
        nm = FinData['PVTariffSummary']['nm']
    else:
        nm = 0

    LimitC = lambda x, y: y if x > y else x
    CombinedTS = pd.DataFrame(index=DemandTS.index, columns=['Solar', 'SolarToUse', 'Demand', 'SolarToNet'])
    TSDfsel = TotalEOut[Ms, Mp, 'PAC_tot']

    CombinedTS['Solar'] = pd.Series(np.roll(TSDfsel.values, int(location_data.tz), axis=0), index=DemandTS.index)
    CombinedTS['Demand'] = DemandTS

    CombinedTS['SolarToUse'] = CombinedTS.apply(lambda x: LimitC(x.Solar, x.Demand), axis=1)
    CombinedTS['SolarToNet'] = CombinedTS['Solar'] - CombinedTS['SolarToUse']
    AddBack = 0  # To Add Back BC CAPEX costs

    if 'ActiveQuote' in entity:
        # if Quote is already edited (contains 'Edit' in RevID - just use the financial numbers, else 
        # Calculate the new CAPEX 
        if 'ActiveQuote' in userinput:
            ActiveQuote = userinput['ActiveQuote']
        else:
            ActiveQuote = entity['ActiveQuote']

        if 'Edit' in ActiveQuote['RevID']:
            VQuoteDfQuant = CDB.GetBoMQuote(userinput['project_id'], ActiveQuote['QuoteID'], ActiveQuote['RevID'])
            VQuoteDfQuant.fillna(np.nan, inplace=True)
            VQuoteDfQuant.replace([np.nan], [0], inplace=True)
            VQuoteDfQuant.set_index('Id', inplace=True)
            # Ensure - if Value is removed, Quantity=0 the Prices are not counted
            VQuoteDfQuant.loc[
                VQuoteDfQuant['Quantity'] == 0, ['TotalCost', 'NetPrice', 'TotalPrice', 'TotalBasePrice']] = 0
            # CAPEX EXCLUSIONS 
            VQuoteDfQuant = CDB.BoMExcludeFlags(VQuoteDfQuant, OrgBomInput=OrgBomInput)

            if 'ExcludeFromCAPEX' in VQuoteDfQuant.columns:
                TotalCAPEX = VQuoteDfQuant[VQuoteDfQuant['ExcludeFromCAPEX'] == 0]['NetPrice'].sum()  # CAPEX EXCLUSIONS
            else:
                TotalCAPEX = \
                VQuoteDfQuant[(VQuoteDfQuant['Group'] != 'Service') | ((VQuoteDfQuant['Group'] != 'Option'))][
                    'NetPrice'].sum()

            TotalOPEX = 0.01 * TotalCAPEX
            TotalAdjustments = 0

            for key, val in Adjustments.items():

                if 'Total' not in Adjustments[key]:
                    TotalAdjustments += Adjustments[key]['data']
                else:
                    TotalAdjustments += Adjustments[key]['Total']
            TotalCAPEX += TotalAdjustments

            if ('ExcludeFromCAPEX' in VQuoteDfQuant.columns) and ('IncludeforBC' in VQuoteDfQuant.columns):
                AddBack = VQuoteDfQuant.loc[
                    (VQuoteDfQuant['ExcludeFromCAPEX'] == 1) & (VQuoteDfQuant['IncludeforBC'] == 1), 'NetPrice'].sum()
                ConfFinData = fm.Project_fin_data(FinData, TotalCAPEX + AddBack, TotalOPEX)
            else:
                ConfFinData = fm.Project_fin_data(FinData, TotalCAPEX, TotalOPEX)


        else:

            # BattSeries = BATTDF.get(CostSelected.loc[CostSelected['Group']=='Battery','PerfModelKey'])
            # if not BattSeries.empty:
            #     #BattFromBoM = CostSelected.loc[CostSelected['Group']=='Battery']
            #     BatterySpec = BattSeries.to_dict()[BattSeries.columns[0]]
            #     # BatterySpec['NumCabinets'] = BattFromBoM['Quantity'].values[0]
            #     # BatterySpec['Enom'] = BatterySpec['NumCabinets']*BatterySpec['Enom']

            #     #BatterySpec['PmaxD'] = BatterySpec['NumCabinets']*BatterySpec['PmaxD'] in usual cases....

            #     CAPEX,OPEX,VQuoteDfQuant,AdjDict=fm.Cost_function_db(PVModuleData, InvData, CostSelected, Ms, Mp, TotalNumBB, TotalPanels,
            #                                              BatterySpec=BatterySpec,Adjustments=Adjustments,AreaFigures=AreaFigures)
            #     entity['Battery'].update({"BatterySpec":BatterySpec})
            # else:

            CAPEX, OPEX, VQuoteDfQuant, AdjDict = fm.Cost_function_db(PVModuleData, InvData, CostSelected.copy(), Ms,
                                                                      Mp, TotalNumBB, TotalPanels,
                                                                      Adjustments=Adjustments, AreaFigures=AreaFigures)
            for key, val in Adjustments.items():
                Adjustments[key]['Total'] = AdjDict[key]

            VQuoteDfQuant.fillna(np.nan, inplace=True)
            VQuoteDfQuant.replace([np.nan], [0], inplace=True)
            VQuoteDfQuant.set_index('Id', inplace=True)
            # Transfer this BoM to BoM  quote 
            ### BOM['Quantity'] is filled above based on unit functions
            ### Save BoM['Quantity'] value to be retrieved for BoM page
            # Name of Id is special for bindparams
            # BoM2Update = [{'_Id':int(idBom),'Quantity':float(VQuoteDfQuant.loc[idBom,'Quantity']),'TotalCost':float(VQuoteDfQuant.loc[idBom,'TotalCost'])} 
            #                           for idBom in VQuoteDfQuant.index]   
            # CDB.UpdateBoM(BoM2Update)

            TotalCAPEX = CAPEX
            TotalOPEX = OPEX
            # Add BC Inclusions if subtracted
            if ('ExcludeFromCAPEX' in VQuoteDfQuant.columns) and ('IncludeforBC' in VQuoteDfQuant.columns):
                AddBack = VQuoteDfQuant.loc[
                    (VQuoteDfQuant['ExcludeFromCAPEX'] == 1) & (VQuoteDfQuant['IncludeforBC'] == 1), 'NetPrice'].sum()
                ConfFinData = fm.Project_fin_data(FinData, CAPEX + AddBack, OPEX)
            else:
                ConfFinData = fm.Project_fin_data(FinData, CAPEX, OPEX)

    else:
        ConfFinData = fm.Project_fin_data(FinData, TotalCAPEX,
                                          TotalOPEX)  # FinData, TotalCAPEX and TotalOPEX are calulcalutd by default by summing over areas

    CashFlowDf, Proj_Fin_KPI, _, YearlyBenefit = fm.CashFlowCalc(ConfFinData, CombinedTS['Solar'],
                                                                 ACtoUse=CombinedTS['SolarToUse'],
                                                                 DemandTS=DemandTS,
                                                                 startdate='01/{month}/{year}'.format(month=month,
                                                                                                      year=year),
                                                                 detailed=False)  # PAC_tot in  Wh 

    # else:     
    #     CashFlowDf,Proj_Fin_KPI,_ = fm.CashFlowCalc(ConfFinData,CombinedTS['SolarToNet'],ACtoUse=CombinedTS['SolarToUse'],
    #                                             Demand=DemandTS,startdate='01/{month}/{year}'.format(month=month,year=year),
    #                                         detailed=True) #  PAC_tot in  Wh 

    # CashFlowDf,Proj_Fin_KPI,_ = fm.CashFlowCalc(ConfFinData,TotalEOut[Ms,Mp,'PAC_tot'],
    #                                             Demand=DemandTS,startdate='1/{month}/{year}'.format(month=month,year=year),
    #                                         detailed=True) #  PAC_tot in  Wh 
    #  Add sepearately in case of no battery - Selected Config only for Battery
    SelectedConfig['ConfigResultsSummary']['CAPEX'] = TotalCAPEX
    SelectedConfig['ConfigResultsSummary']['OPEX'] = TotalOPEX
    SelectedConfig['ConfigResultsSummary']['TotalYield'] = CashFlowDf['NetBenefit'].sum()
    SelectedConfig['ConfigResultsSummary']['IRR'] = Proj_Fin_KPI['Project_IRR']
    SelectedConfig['ConfigResultsSummary']['PaybackTime'] = Proj_Fin_KPI['Project_payback']

    OverallSummary['IRR'] = Proj_Fin_KPI['Project_IRR']
    OverallSummary['PaybackTime'] = Proj_Fin_KPI['Project_payback']

    # Battery  ..HERE QUANTITY NOT UPDATING
    # if 'Battery' in entity:
    if not BATTDF.get(CostSelected.loc[CostSelected['Group'] == 'Battery', 'PerfModelKey']).empty:
        # BatterySpec = entity['Battery']['BatterySpec']    
        BattSeries = BATTDF.get(CostSelected.loc[CostSelected['Group'] == 'Battery', 'PerfModelKey'])
        BatterySpec = BattSeries.to_dict()[BattSeries.columns[0]]
        BatterySpec['NumCabinets'] = CostSelected.loc[CostSelected['Group'] == 'Battery', 'Quantity'].values[0]
        BatterySpec['Enom'] = BatterySpec['NumCabinets'] * BatterySpec['Enom']
        BatterySpec['Emax'] = BatterySpec['NumCabinets'] * BatterySpec['Enom']
        BatterySpec['Type'] = CostSelected.loc[CostSelected['Group'] == 'Battery', 'PerfModelKey'].values[0]
        # BatterySpec['PmaxD'] = BatterySpec['NumCabinets']*BatterySpec['PmaxD'] in usual cases....
        if type(BatterySpec) != str:  # Battery is used  or BatterySpec != 'None'
            # Refresh BatteryTS with new data generation data   

            Emax = BatterySpec['Emax']

            Pmax = BatterySpec['PmaxD']
            # BattName = CostSelected.loc[CostSelected['Group']=='Battery','PerfModelKey'].values[0] #BatterySpec['Type']
            NumCabinets = BatterySpec['NumCabinets']
            ##########################################################
            # Get BatteryMod from Battery Type - BatterySpec['Type']
            # if BattName in BATTDF:
            #     BatteryMod = BATTDF[BattName]
            # else:    
            #     BatteryMod ={"E2P":2.5, 
            #                   "ChargeEff":0.98,"DischargeEff":0.96} 
            BatteryMod = BatterySpec
            ########################################################3
            BattBuck = ST.BucketModel(BatteryMod)
            TSDfsel = TotalEOut[Ms, Mp, 'PAC_tot']

            SolarTS = pd.Series(np.roll(TSDfsel.values, int(location_data.tz), axis=0), index=TSDfsel.index)
            SolarTS.index = DemandTS.index

            BatteryTS, MaxBattReqSize, KPIs = BattBuck.getBattProfile(DemandTS, SolarTS, Pmax=Pmax * 1000,
                                                                      Emax=Emax * 1000)  #
            #########################################################    
            TotalToUse = BatteryTS['TotalToUse']
            SolarToUse = BatteryTS['SolarToUse']
            TotalToNet = SolarTS - SolarToUse
            FinalTS = pd.concat([SolarTS.rename('Solar'), DemandTS, BatteryTS['Power'].rename('Battery')], axis=1)
            Fulfilment = (TotalToUse.sum() / DemandTS.sum())  # Same as Independance
            SelfConsumption = TotalToUse.sum() / SolarTS.sum()

            entity['Battery']['KPI'].update(
                {'SelfConsumptionWBatt': SelfConsumption * 100, "FulfilmentWBatt": Fulfilment * 100})

            # FinalTS.rename(columns={"Power":"Battery"},inplace=True)
            blobname = USER_UPLOADS + uniqueid + '_BattTS.pkl'
            bucket.blob(blobname).upload_from_string(pickle.dumps(FinalTS))

            # bucket.blob(blobname).upload_from_string(pickle.dumps(BatteryTS)) 
            # TODO: Add Battery Quantity and entry to BOMQuote 
            if 'ActiveQuote' in entity:
                # if Quote is already edited (contains 'Edit' in RevID - just use the financial numbers, else 
                # Calculate the new CAPEXes- 
                if 'ActiveQuote' in userinput:
                    ActiveQuote = userinput['ActiveQuote']
                else:
                    ActiveQuote = entity['ActiveQuote']

                if 'Edit' in ActiveQuote['RevID']:
                    VQuoteDfQuant = CDB.GetBoMQuote(userinput['project_id'], ActiveQuote['QuoteID'],
                                                    ActiveQuote['RevID'])
                    VQuoteDfQuant.fillna(np.nan, inplace=True)
                    VQuoteDfQuant.replace([np.nan], [0], inplace=True)
                    # VQuoteDfQuant.set_index('Id',inplace=True) 

                    # CAPEX EXCLUSIONS 
                    VQuoteDfQuant = CDB.BoMExcludeFlags(VQuoteDfQuant, OrgBomInput=OrgBomInput)

                    if 'ExcludeFromCAPEX' in VQuoteDfQuant.columns:
                        TotalCAPEX = VQuoteDfQuant[VQuoteDfQuant['ExcludeFromCAPEX'] == 0][
                            'NetPrice'].sum()  # CAPEX EXCLUSIONS
                    else:
                        TotalCAPEX = \
                        VQuoteDfQuant[(VQuoteDfQuant['Group'] != 'Service') | ((VQuoteDfQuant['Group'] != 'Option'))][
                            'NetPrice'].sum()

                    TotalOPEX = 0.01 * TotalCAPEX
                    TotalAdjustments = 0
                    for key, val in Adjustments.items():
                        if 'Total' not in Adjustments[key]:
                            TotalAdjustments += Adjustments[key]['data']
                        else:
                            TotalAdjustments += Adjustments[key]['Total']

                    TotalCAPEX += TotalAdjustments

                    if ('ExcludeFromCAPEX' in VQuoteDfQuant.columns) and ('IncludeforBC' in VQuoteDfQuant.columns):
                        AddBack = VQuoteDfQuant.loc[(VQuoteDfQuant['ExcludeFromCAPEX'] == 1) & (
                                    VQuoteDfQuant['IncludeforBC'] == 1), 'NetPrice'].sum()
                        ConfFinData = fm.Project_fin_data(FinData, TotalCAPEX + AddBack, TotalOPEX)

                    else:
                        ConfFinData = fm.Project_fin_data(FinData, TotalCAPEX, TotalOPEX)



                else:
                    # Calculate the actual CAPEX from cost functions
                    # BatterySpec = BATTDF.get(CostSelected.loc[CostSelected['Group']=='Battery','PerfModelKey'])
                    CAPEX, OPEX, VQuoteDfQuant, AdjDict = fm.Cost_function_db(PVModuleData, InvData,
                                                                              CostSelected.copy(), Ms, Mp, TotalNumBB,
                                                                              TotalPanels,
                                                                              BatterySpec=BatterySpec,
                                                                              Adjustments=Adjustments,
                                                                              AreaFigures=AreaFigures)

                    for key, val in Adjustments.items():
                        Adjustments[key]['Total'] = AdjDict[key]

                    VQuoteDfQuant.fillna(np.nan, inplace=True)
                    VQuoteDfQuant.replace([np.nan], [0], inplace=True)
                    VQuoteDfQuant.set_index('Id', inplace=True)
                    # Transfer this BoM to BoM  quote 
                    ### BOM['Quantity'] is filled above based on unit functions
                    ### Save BoM['Quantity'] value to be retrieved for BoM page
                    # Name of Id is special for bindparams
                    BoM2Update = [{'_Id': int(idBom), 'Quantity': float(VQuoteDfQuant.loc[idBom, 'Quantity']),
                                   'TotalCost': float(VQuoteDfQuant.loc[idBom, 'TotalCost']),
                                   'NetPrice': float(VQuoteDfQuant.loc[idBom, 'NetPrice']),
                                   'PriceAdjustment': float(VQuoteDfQuant.loc[idBom, 'PriceAdjustment']),
                                   'TotalPrice': float(VQuoteDfQuant.loc[idBom, 'TotalPrice']),
                                   'TotalBasePrice': float(VQuoteDfQuant.loc[idBom, 'TotalBasePrice'])} \
                                  for idBom in VQuoteDfQuant.index]
                    CDB.UpdateBoM(BoM2Update)

                    TotalCAPEX = CAPEX
                    TotalOPEX = OPEX

                    if ('ExcludeFromCAPEX' in VQuoteDfQuant.columns) and ('IncludeforBC' in VQuoteDfQuant.columns):
                        AddBack = VQuoteDfQuant.loc[(VQuoteDfQuant['ExcludeFromCAPEX'] == 1) & (
                                    VQuoteDfQuant['IncludeforBC'] == 1), 'NetPrice'].sum()
                        ConfFinData = fm.Project_fin_data(FinData, TotalCAPEX + AddBack, TotalOPEX)
                    else:
                        ConfFinData = fm.Project_fin_data(FinData, TotalCAPEX, TotalOPEX)


            else:
                BoMDf.loc[('Area1', 'BatteryCost'), 'Num'] = BatterySpec['NumCabinets']
                BoMDf.loc[('Area1', 'BatteryCost'), 'CostperUnit'] = BatterySpec['Price']
                BoMDf.loc[('Area1', 'BatteryCost'), 'Unit'] = 'Unit'
                BoMDf.loc[('Area1', 'BatteryCost'), 'Name'] = BatterySpec['Type']
                BoMDf.loc[('Area1', 'BatteryCost'), 'TotalCost'] = BatterySpec['NumCabinets'] * BatterySpec['Price']
                TotalCAPEX += BoMDf.loc[('Area1', 'BatteryCost'), 'TotalCost']
                TotalOPEX += 0.01 * BoMDf.loc[('Area1', 'BatteryCost'), 'TotalCost']
                ConfFinData = fm.Project_fin_data(FinData, TotalCAPEX, TotalOPEX)

            # if 'ActiveQuote' in entity:
            #     CAPEX,OPEX,VQuoteDfQuant=fm.Cost_function_db(PVModuleData, InvData, ViewQuoteDf, Ms, Mp, TotalNumBB, TotalPanels,BatterySpec=BatterySpec,Adjustments=Adjustments)
            #     #TotalBoM = BoM
            #     #TotalBoM = BoMDf[['TotalCost','Num']].groupby(level=[1]).sum() # Sum up BoM values by arealabels
            #     #TotalBoM.rename(columns={'Num':'Quantity'},inplace=True)
            #     VQuoteDfQuant.fillna(np.nan,inplace=True)
            #     VQuoteDfQuant.replace([np.nan],[0],inplace=True)
            #     VQuoteDfQuant.set_index('Id',inplace=True)    
            #          # Transfer this BoM to BoM  quote 
            #         ### BOM['Quantity'] is filled above based on unit functions
            #         ### Save BoM['Quantity'] value to be retrieved for BoM page
            #         # Name of Id is special for bindparams
            #     BoM2Update = [{'_Id':int(idBom),'Quantity':float(VQuoteDfQuant.loc[idBom,'Quantity']),'TotalCost':float(VQuoteDfQuant.loc[idBom,'TotalCost'])} 
            #                               for idBom in VQuoteDfQuant.index]   
            #     CDB.UpdateBoM(BoM2Update)
            #         # BoM updated with quantities and Total Cost across all areas        
            #     #########    

            # Add battery capex

            # CashFlowDf,Proj_Fin_KPI,_,YearlyBenefit = fm.CashFlowCalc(ConfFinData,TotalToNet,ACtoUse=TotalToUse,DemandTS=DemandTS,
            #                                             startdate='01/{month}/{year}'.format(month=month,year=year),
            #                                         detailed=False)
            CashFlowDf, Proj_Fin_KPI, _, YearlyBenefit = fm.CashFlowCalc(ConfFinData, SolarTS, ACtoUse=TotalToUse,
                                                                         DemandTS=DemandTS,
                                                                         startdate='01/{month}/{year}'.format(
                                                                             month=month, year=year),
                                                                         detailed=False)

            # Add only if Battery is present
            OverallSummary['BatteryName'] = BatterySpec['Type']
            OverallSummary['BattkWh'] = BatterySpec['NumCabinets'] * BatterySpec['Emax']
            OverallSummary['IRR'] = Proj_Fin_KPI['Project_IRR']
            OverallSummary['PaybackTime'] = Proj_Fin_KPI['Project_payback']
            # Calculate No Battery Equivalents



        else:
            # Update BoM with Solar Only VQuoteDfQuant
            # VQuoteDfQuant.fillna(np.nan,inplace=True)
            # VQuoteDfQuant.replace([np.nan],[0],inplace=True)
            # VQuoteDfQuant.set_index('Id',inplace=True)   
            BoM2Update = [{'_Id': int(idBom), 'Quantity': float(VQuoteDfQuant.loc[idBom, 'Quantity']),
                           'TotalCost': float(VQuoteDfQuant.loc[idBom, 'TotalCost']),
                           'NetPrice': float(VQuoteDfQuant.loc[idBom, 'NetPrice']),
                           'PriceAdjustment': float(VQuoteDfQuant.loc[idBom, 'PriceAdjustment']), \
                           'TotalPrice': float(VQuoteDfQuant.loc[idBom, 'TotalPrice']),
                           'TotalBasePrice': float(VQuoteDfQuant.loc[idBom, 'TotalBasePrice'])} \
                          for idBom in VQuoteDfQuant.index]
            CDB.UpdateBoM(BoM2Update)

            OverallSummary['IRR'] = Proj_Fin_KPI['Project_IRR']
            OverallSummary['PaybackTime'] = Proj_Fin_KPI['Project_payback']

            CombinedTS['Solar'] = pd.Series(np.roll(TSDfsel.values, int(location_data.tz), axis=0),
                                            index=DemandTS.index)
            CombinedTS['Demand'] = DemandTS
            # Update with No Battery TS

            BatteryTS, _, KPIs = BattBuck.getBattProfile(CombinedTS['Demand'], CombinedTS['Solar'], Pmax=0, Emax=0)  #
            blobname = USER_UPLOADS + uniqueid + '_BattTS.pkl'
            FinalTS = pd.concat([SolarTS, DemandTS, BatteryTS['Power']], axis=1)
            FinalTS.rename(columns={"Power": "Battery"})
            blobname = USER_UPLOADS + uniqueid + '_BattTS.pkl'
            bucket.blob(blobname).upload_from_string(pickle.dumps(FinalTS))

            # in case of no battery - SolarOnly to Use
            # LimitC  = lambda x,y : y if x>y else x
            # CombinedTS = pd.DataFrame(index=DemandTS.index,columns=['Solar','SolarToUse','Demand','SolarToNet'])
            # CombinedTS['Solar'] = TotalEOut[Ms,Mp,'PAC_tot'].values
            # CombinedTS['Demand'] = DemandTS
            # CombinedTS['SolarToUse'] = CombinedTS.apply(lambda x:LimitC(x.Solar,x.Demand),axis=1)
            # CombinedTS['SolarToNet'] = CombinedTS['Solar'] - CombinedTS['SolarToUse']
            # if 'ActiveQuote' in entity:
            #     CAPEX,OPEX,VQuoteDfQuant=fm.Cost_function_db(PVModuleData, InvData, CostSelected, Ms, Mp, TotalNumBB, TotalPanels,
            #                                                  Adjustments=Adjustments,AreaFigures=AreaFigures)

            #     VQuoteDfQuant.fillna(np.nan,inplace=True)
            #     VQuoteDfQuant.replace([np.nan],[0],inplace=True)
            #     VQuoteDfQuant.set_index('Id',inplace=True)    
            #          # Transfer this BoM to BoM  quote 
            #         ### BOM['Quantity'] is filled above based on unit functions
            #         ### Save BoM['Quantity'] value to be retrieved for BoM page
            #         # Name of Id is special for bindparams
            #     BoM2Update = [{'_Id':int(idBom),'Quantity':float(VQuoteDfQuant.loc[idBom,'Quantity']),'TotalCost':float(VQuoteDfQuant.loc[idBom,'TotalCost'])} 
            #                               for idBom in VQuoteDfQuant.index]   
            #     CDB.UpdateBoM(BoM2Update)
            #     TotalCAPEX = CAPEX
            #     TotalOPEX = OPEX
            #     ConfFinData = fm.Project_fin_data(FinData,CAPEX,OPEX)

            # CashFlowDf,Proj_Fin_KPI,_ = fm.CashFlowCalc(ConfFinData,CombinedTS['SolarToNet'],ACtoUse=CombinedTS['SolarToUse'],
            #                                             Demand=DemandTS,startdate='1/{month}/{year}'.format(month=month,year=year),
            #                                         detailed=True) #  PAC_tot in  Wh 

            # #  Add sepearately in case of no battery - Selected Config only for Battery
            # SelectedConfig['ConfigResultsSummary']['CAPEX']=TotalCAPEX
            # SelectedConfig['ConfigResultsSummary']['IRR'] = Proj_Fin_KPI['Project_IRR'] 
            # SelectedConfig['ConfigResultsSummary']['PaybackTime'] = Proj_Fin_KPI['Project_payback']

    else:
        # Update BoM with Solar Only VQuoteDfQuant
        # VQuoteDfQuant.fillna(np.nan,inplace=True)
        # VQuoteDfQuant.replace([np.nan],[0],inplace=True)
        # VQuoteDfQuant.set_index('Id',inplace=True)  
        BoM2Update = [{'_Id': int(idBom), 'Quantity': float(VQuoteDfQuant.loc[idBom, 'Quantity']),
                       'TotalCost': float(VQuoteDfQuant.loc[idBom, 'TotalCost']),
                       'NetPrice': float(VQuoteDfQuant.loc[idBom, 'NetPrice']),
                       'PriceAdjustment': float(VQuoteDfQuant.loc[idBom, 'PriceAdjustment']),
                       'TotalPrice': float(VQuoteDfQuant.loc[idBom, 'TotalPrice']),
                       'TotalBasePrice': float(VQuoteDfQuant.loc[idBom, 'TotalBasePrice'])} \
                      for idBom in VQuoteDfQuant.index]
        CDB.UpdateBoM(BoM2Update)
        OverallSummary['IRR'] = Proj_Fin_KPI['Project_IRR']
        OverallSummary['PaybackTime'] = Proj_Fin_KPI['Project_payback']
        CombinedTS['Solar'] = pd.Series(np.roll(TSDfsel.values, int(location_data.tz), axis=0), index=DemandTS.index)
        CombinedTS['Demand'] = DemandTS
        # Update with No Battery TS
        CombinedTS['Battery'] = DemandTS * 0
        blobname = USER_UPLOADS + uniqueid + '_BattTS.pkl'
        bucket.blob(blobname).upload_from_string(pickle.dumps(CombinedTS))

    ALLTSDf[(PVName, InvName, Ms, Mp, 'PAC_tot')] = TotalEOut[Ms, Mp, 'PAC_tot'].values
    ALLTSDf[(PVName, InvName, Ms, Mp, 'PDC')] = TotalEOut[Ms, Mp, 'PDC'].values

    bucket.blob(blobname_ts).upload_from_string(pickle.dumps(ALLTSDf))
    BoMblobname = RESULTS + uniqueid + '_BOM.pkl'
    bucket.blob(BoMblobname).upload_from_string(pickle.dumps(BoMDf))

    blobname = RESULTS + uniqueid + '_YearlyBenefits.pkl'
    bucket.blob(blobname).upload_from_string(pickle.dumps(YearlyBenefit))

    # Update BatteryKPI values

    if entity.get('Financial'):
        entity['Financial'].update({'YearlyBenefit': blobname})
    else:
        entity.update({"Financial": {'YearlyBenefit': blobname}})

    ######### MONTHLY ENERGY FOR DISPLAY#############
    monthlyenergy = pd.DataFrame()
    monthlyenergy['PAC_tot'] = (ALLTSDf[(PVName, InvName, Ms, Mp, 'PAC_tot')].groupby(
        pd.Grouper(freq='1M')).sum()) / 1000  # in kWh
    monthlyenergy['kWh/kWp'] = monthlyenergy['PAC_tot'] / (PVModuleData.Wp * TotalPanels / 1000)
    monthlyenergy['month'] = [dt.datetime.strftime(d, '%b') for d in monthlyenergy.index]
    monthlyenergy['PDC'] = TotalEOut[Ms, Mp, 'PDC'].groupby(pd.Grouper(freq='1M')).sum() / 1000
    monthlyenergy['IncRad'] = TotalEOut[Ms, Mp, 'IncRad'].groupby(pd.Grouper(freq='1M')).sum() / 1000

    energydisplay = {"Energydisplay": {
        "monthlyenergy": {"TotalEnergy": [*monthlyenergy['PAC_tot']], "kWh/kWp": [*monthlyenergy['kWh/kWp']],
                          "month": [*monthlyenergy['month']],
                          "DCEnergy": [*monthlyenergy['PDC']],
                          "IncRad": [*monthlyenergy['IncRad']]}}}
    #################################################

    OverallSummary['ACEnergy'] = TotalEOut[Ms, Mp, 'PAC_tot'].sum() / 1000  # to kWh     
    OverallSummary['kWh/kWp'] = OverallSummary['ACEnergy'] * 1000 / (PVModuleData.Wp * TotalPanels)
    OverallSummary['DCEnergy'] = TotalEOut[Ms, Mp, 'PDC'].sum() / 1000

    OverallSummary['ACPower'] = InvData.Paco * TotalNumBB / 1e3
    OverallSummary['LCOE'] = fm.calc_LCOE(TotalCAPEX, TotalOPEX, OverallSummary['ACEnergy'])
    OverallSummary['DC2AC'] = (PVModuleData.Wp * TotalPanels) / (InvData.Paco * TotalNumBB)
    OverallSummary['DCPower'] = PVModuleData.Wp * TotalPanels / 1000

    OverallSummary['IRR'] = Proj_Fin_KPI['Project_IRR']
    OverallSummary['PaybackTime'] = Proj_Fin_KPI['Project_payback'] / 12  # in years
    OverallSummary['kWh/m2'] = (TotalEOut[Ms, Mp, 'PAC_tot'].sum() / 1000) / (TotalPanels * PVModuleData.A_c)

    OverallSummary['CAPEX'] = TotalCAPEX
    OverallSummary['OPEX'] = TotalOPEX
    OverallSummary['EffCAPEX'] = TotalCAPEX + AddBack

    OverallSummary['NumPanels'] = TotalPanels
    OverallSummary['NumBB'] = TotalNumBB
    ModEff = PVModuleData.Wp / (1000 * PVModuleData.A_c)
    OverallSummary['PR'] = TotalEOut[Ms, Mp, 'PAC_tot'].sum() / (TotalIncRad * ModEff * 1000)
    OverallSummary['TotalIncRad'] = TotalIncRad / 1000  # Convert to kWh
    OverallSummary['PVName'] = PVName.replace('_', ' ')
    OverallSummary['InvName'] = InvName.replace('_', ' ')

    SelectedConfig['ConfigResultsSummary']['LCOE'] = fm.calc_LCOE(SelectedConfig['ConfigResultsSummary']['CAPEX'],
                                                                  SelectedConfig['ConfigResultsSummary']['OPEX'],
                                                                  OverallSummary['ACEnergy'])
    # Solar only KPIs below
    SelectedConfig['ConfigResultsSummary']['DCPower'] = OverallSummary['DCPower']
    SelectedConfig['ConfigResultsSummary']['ACPower'] = OverallSummary['ACPower']
    SelectedConfig['ConfigResultsSummary']['ACEnergy'] = OverallSummary['ACEnergy']
    SelectedConfig['ConfigResultsSummary']['DC2AC'] = OverallSummary['DC2AC']

    SelectedConfig['ConfigResultsSummary']['DC2AC'] = OverallSummary['DC2AC']
    SelectedConfig['ConfigResultsSummary']['NumBB'] = OverallSummary['NumBB']
    SelectedConfig['ConfigResultsSummary']['NumPanels'] = OverallSummary['NumPanels']
    SelectedConfig['ConfigResultsSummary']['kWh/kWp'] = OverallSummary['kWh/kWp']
    SelectedConfig['ConfigResultsSummary']['kWh/m2'] = OverallSummary['kWh/m2']
    # OverallSummary['StringConf'] = json.loads( json.dumps({s:{"Ms":M[0],"Mp":M[1],"NumBB":SubArrayConfig.count(M)} for
    #                                  M,s in zip(set(SubArrayConfig),range(len(set(SubArrayConfig))))} ))
    OverallSummary['StringConf'] = json.loads(
        json.dumps({str(i): {"Ms": DetailedArrayConfig[i][0][0], "Mp": int(np.sum([len(DetailedArrayConfig[i][mppt])
                                                                                   for mppt in range(
                len(DetailedArrayConfig[i]))])), "NumBB": 1} for i in DetailedArrayConfig}))

    # i is num of Building blocks
    OverallSummary['StringConfDetailed'] = json.loads(json.dumps(
        {str(i): {"NumMPPT": len(DetailedArrayConfig[i]), "NumStringsperMPPT": [len(DetailedArrayConfig[i][mppt])
                                                                                for mppt in
                                                                                range(len(DetailedArrayConfig[i]))],
                  "PanelsinSeriesperString": [DetailedArrayConfig[i][mppt][0] for mppt in
                                              range(len(DetailedArrayConfig[i]))],
                  "TotalParallelStrings": int(np.sum([len(DetailedArrayConfig[i][mppt])
                                                      for mppt in range(len(DetailedArrayConfig[i]))]))} for i in
         DetailedArrayConfig}))

    # cf2dispdf = CashFlowDf.groupby(pd.Grouper(freq='1Y')).sum()
    # cf2dispdf['OPEX+Interest'] = -(cf2dispdf['NetBenefit']-cf2dispdf['SavingsOnly']+cf2dispdf['interests'])
    # cf2dispdf['CAPEX'] = ConfSummary['CAPEX']
    # cf2dispdf['CAPEX'].iloc[1:]=0
    # cf2dispdf['Net Cash Flow'] = cf2dispdf['FreeCashFlow']
    # cf2dispdf['Net Cash Flow'].iloc[0] = cf2dispdf['Net Cash Flow'].iloc[0]-ConfSummary['CAPEX']

    blobname_ts = RESULTS + uniqueid + '_CashFlow.pkl'
    blob = bucket.blob(blobname_ts)
    blob.upload_from_string(pickle.dumps(CashFlowDf))

    OutputDic.update({"AreaWise": AreaResults})
    OutputDic.update({"OverallSummary": OverallSummary})
    OutputDic.update({"CashFlow": blobname_ts})
    OutputDic.update({"Trends": TStoDisp})
    OutputDic.update({"Energydisplay": energydisplay['Energydisplay']})
    OutputDic.update({"BoM": BoMblobname})
    OutputDic.update({"TotalLossChain": TotalLossChainDf.to_dict()})
    OutputDic.update({"warningmismatch": errorDic})
    entity.update({"FinalSummary": OutputDic})
    entity.update({"status": "Results Summary"})
    ds.put(entity)
    # TODO: FORMAT LOSS CHAIN PROPERLY
    # OutputDic.update({"LossChain":LossChain})

    return jsonify(OutputDic), 200
    # return jsonify({"success":"Energy and financial values recalculated"}),200       


@app.route('/battery-v2', methods=['POST'])
@jwt_authenticated
def batteryv2():
    ds = datastore.Client()
    gcs = storage.Client()
    bucket = gcs.bucket(BUCKET_NAME)

    req_json = request.get_json()['Battery']
    userinput = req_json
    userinput['email'] = request.email
    orgname = request.org
    OutputDic = {}
    message = {}
    parent_key = ds.key('users', userinput['email'])
    entity = ds.get(key=ds.key('Project', userinput['project_id'], parent=parent_key))

    if not entity:
        message.update({"error": "No project with username {} and project {} exists".format(userinput['email'],
                                                                                            userinput['project_id'])})
        return jsonify(message), 400

    uniqueid = 'Uid_' + userinput['email'] + '_Pid_' + userinput['project_id']
    orgentity = ds.get(key=ds.key('organisation', orgname))
    os.environ['DB_NAME'] = orgentity.get("compdb", "refdata")
    importlib.reload(CDB)

    if 'SelectedConfig_mu' in entity:
        TS = pickle.loads(bucket.get_blob(USER_UPLOADS + uniqueid + "_BattTS.pkl").download_as_string())
        SolarTS = TS['Solar']
        DemandTS = TS['Demand']
    else:
        entchklist = ['OptMatrix', 'SelectedConfig', 'Demand', 'ActiveQuote']
        for ent in entchklist:
            if ent not in entity:
                return jsonify({"error": "{} not set in datastore".format(ent)}), 400

                # Get Solar Production Time Series
        blobname = entity['OptMatrix']['TSConfigFile']
        TSDf = pickle.loads(bucket.get_blob(blobname).download_as_string())
        configsel = entity['SelectedConfig']['Config']
        pvname = configsel['PVName']
        invname = configsel['InvName']
        Ms = configsel['Ms']
        Mp = configsel['Mp']
        ConfigSummTuple = (pvname, invname, Ms, Mp, 'PAC_tot')
        location_data = Location(entity['Location']['latitude'], entity['Location']['longitude'],
                                 tz=entity['Weather']['TZ'])

        # SolarTS = TSDf[ConfigSummTuple]

        TSDfsel = TSDf[ConfigSummTuple]
        SolarTS = pd.Series(np.roll(TSDfsel.values, int(location_data.tz), axis=0), index=TSDfsel.index)
        SolarTS.name = 'Solar'
        #########################
        #### Get Demand TS ############
        DemandTS = pickle.loads(bucket.get_blob(entity['Demand']['DemandTS']).download_as_string())

        # YearlyDemand = DemandTS.sum()/1000
        SolarTS.index = DemandTS.index
    # 
    ##################COMMON###########
    # Get Battery model values from PerfModelKey of selected Battery 
    # BattDf
    # BATTDF = pickle.loads(bucket.get_blob('BatteryModel-09-2020.pkl').download_as_string())
    # Get Active Quote Values
    quoteparams = entity['ActiveQuote']
    QuoteID = quoteparams['QuoteID']
    RevID = quoteparams['RevID']
    ProductName = entity['ActiveQuote'].get('ProductName')

    ViewQuoteDf = CDB.GetBoMQuote(userinput['project_id'], QuoteID, RevID)
    BattBomId = ViewQuoteDf.loc[ViewQuoteDf['Group'] == 'Battery', 'Id'].astype(int).values
    ViewQuoteDf.fillna(np.nan, inplace=True)
    ViewQuoteDf.replace([np.nan], [None], inplace=True)

    # Make PriceAdjustment same as that for product name
    PriceAdjustment = 0.15
    if ProductName:
        ProdQuote = CDB.GetQuotefromtemplate(ProductName)
        PriceAdjustmentVal = ProdQuote.loc[ProdQuote['GroupName'] == 'Battery', 'PriceAdjustment'].astype(float).values
        if len(PriceAdjustmentVal) > 0:
            PriceAdjustment = PriceAdjustmentVal[0]

    if entity['status'] != 'Lock':
        entity.update({"status": "Battery"})
    BBomName = ""
    if 'BatterySpec' not in userinput:

        BNameVals = ViewQuoteDf.loc[ViewQuoteDf['Group'] == 'Battery', 'PerfModelKey']

        if (len(BNameVals) > 0):
            if ViewQuoteDf.loc[ViewQuoteDf['Group'] == 'Battery', 'Quantity'].values[0] == 0:
                BattName = BNameVals.values[0]
            else:
                BattName = BNameVals.values[0]
            BBomName = ViewQuoteDf.loc[ViewQuoteDf['Group'] == 'Battery', 'Name'].values[0]
            if ViewQuoteDf.loc[ViewQuoteDf['Group'] == 'Battery', 'Quantity'].values[0] == None:
                ViewQuoteDf.loc[ViewQuoteDf['Group'] == 'Battery', 'Quantity'] = 1
            if BattName in BATTDF.columns:
                BattVar = {}
                BattData = BATTDF[BattName]
                # for ind in BATTDF.index:
                #     BattVar[ind]= BATTDF[BattName].loc[ind]

                # BattDbDf = CDB.GetEntry('SYSTEM','PerfModelKey',BattName) # Get Details From Db
                BattDbDf = ViewQuoteDf.loc[ViewQuoteDf['Group'] == 'Battery']
                Quantity = BattDbDf['Quantity'].values[0]
                CostFunc = BattDbDf['Cost'].values[0]
                if Quantity:
                    NumCabinets = Quantity
                else:
                    NumCabinets = 1
                # BattVar['Enom'] = BattVar['Enom']*NumCabinets*BattVar['Estep']
                # BattVar['Emax']= BattVar['Enom']
            else:
                BattName = 'None'
                BBomName = 'None'
        else:
            BattName = 'None'
            BBomName = 'None'
            # if 'Battery' in entity:
            #     return jsonify(entity['Battery']),200
            # else:              
            #     BattName= 'None'

        if BattName == 'None':

            BattVar = {}
            for ind in BATTDF.index:
                BattVar[ind] = BATTDF[BATTDF.columns[0]].loc[ind]
            BattVar['Enom'] = 0
            BattVar['PmaxD'] = 0
            NumCabinets = 0

            BatteryTS, _, KPIs = ST.battProfileModel(DemandTS, SolarTS, BattVar)
            BatteryP = pd.Series(BatteryTS['Power'], name='Battery')
            BatterySize = {"Enom": 0, "Emax": 0, "PmaxC": 0, "PmaxD": 0, 'NumCabinets': 0, "CycleLife": 0}

            FinalTS = pd.concat([SolarTS, DemandTS, BatteryP], axis=1)
            blobname = USER_UPLOADS + uniqueid + '_BattTS.pkl'
            bucket.blob(blobname).upload_from_string(pickle.dumps(FinalTS))

            # Remove from BoM

            TotalCost = 0  # float((BattDbDf['Cost']*Quantity).values[0])
            KPIs.update({"TotalCost": TotalCost})
            # if battery in BoM then update entry
            if (len(BattBomId) != 0) and ('Edit' not in RevID):
                _Id = BattBomId[0].item()
                Bat2Update = [{'_Id': _Id}]
                CDB.DeleteFromBoM(Bat2Update)
                # typeid = BattTypeId[0].item() # id in masterlist - not needed---
                # Bat2Update = [{'_Id':_Id,"Type":"SYSTEM","TypeID":typeid,"CreatedBy":userinput['email'],
                #               "Quantity":float(Quantity),"TotalCost":float(TotalCost)}]
                # CDB.UpdateBoM(Bat2Update)

                # else add battery

                # Bat2Insert = [{"Type":"SYSTEM","TypeID":int(BattDbDf['Id'].values[0].item()),"CreatedBy":userinput['email'],
                #                 "ProjectID":userinput['project_id'],"QuoteID":QuoteID,"RevID":RevID,"Quantity":Quantity,"TotalCost":float(TotalCost),
                #                 "Group":"Battery"}]
                # CDB.InserttoBoM(Bat2Insert)

            OutputDic.update({"Battery": {"BatterySpec": "None", "TS": blobname, "KPI": KPIs}})
            entity.update(OutputDic)
            ds.put(entity)

            return jsonify(OutputDic['Battery']), 200

        # if BatterySpec is None, remove battery from quote and Make TS with E=P=0

    else:
        if userinput['BatterySpec'] == 'None':
            BattVar = {}
            for ind in BATTDF.index:
                BattVar[ind] = BATTDF[BATTDF.columns[0]].loc[ind]

            BattVar['Enom'] = 0
            BattVar['PmaxD'] = 0
            NumCabinets = 0

            BatteryTS, _, KPIs = ST.battProfileModel(DemandTS, SolarTS, BattVar)
            BatteryP = pd.Series(BatteryTS['Power'], name='Battery')
            BatterySize = {"Enom": 0, "Emax": 0, "PmaxC": 0, "PmaxD": 0, 'NumCabinets': 0, "CycleLife": 0}

            FinalTS = pd.concat([SolarTS, DemandTS, BatteryP], axis=1)
            blobname = USER_UPLOADS + uniqueid + '_BattTS.pkl'
            bucket.blob(blobname).upload_from_string(pickle.dumps(FinalTS))

            if (len(BattBomId) != 0) and ('Edit' not in RevID):
                _Id = BattBomId[0].item()
                Bat2Update = [{'_Id': _Id}]
                CDB.DeleteFromBoM(Bat2Update)
            KPIs.update({"TotalCost": 0})
            OutputDic.update({"Battery": {"BatterySpec": "None", "TS": blobname, "KPI": KPIs}})
            entity.update(OutputDic)
            ds.put(entity)

            return jsonify(OutputDic['Battery']), 200
        else:
            # if 'None' then E and P values are 0

            if 'Name' in userinput['BatterySpec']:
                BBomName = userinput['BatterySpec']['Name']
                BattDbDf = CDB.GetEntry('SYSTEM', 'Name', BBomName)
                BattDbDf.rename(columns={"Id": "ID"}, inplace=True)
                CostFunc = BattDbDf['Cost'].values[0]
                BattName = BattDbDf['PerfModelKey'].values[0]
                if BattName in BATTDF.columns:
                    BattData = BATTDF[BattName]
                else:
                    return jsonify({"error": "Battery Name invalid {}".format(BattName)}), 400
            else:
                BattName = userinput['BatterySpec']['Type']
                if BattName in BATTDF.columns:
                    BattData = BATTDF[BattName]
                    BattDbDf = CDB.GetEntry('SYSTEM', 'PerfModelKey', BattName)  # Get Details From Db
                    # BattDbDf = CDB.GetEntry('SYSTEM','Name',BBomName)
                    if len(BattDbDf) > 0:
                        BBomName = BattDbDf['Name'].values[0]
                        BattDbDf.rename(columns={"Id": "ID"}, inplace=True)
                        CostFunc = BattDbDf['Cost'].values[0]
                    else:
                        BattDbDf = CDB.GetEntry('SYSTEM', 'PerfModelKey', 'ads-tec SRS2028')
                        BattDbDf.rename(columns={"Id": "ID"}, inplace=True)
                        BBomName = BattDbDf['Name'].values[0]
                        CostFunc = BattDbDf['Cost'].values[0]
                elif BattName == 'Custom':
                    BattData = BATTDF[BATTDF.columns[0]]
                    # TODO - Delete THIS AND GENERATE CUSTOM BATTERY DATA
                    BattDbDf = CDB.GetEntry('SYSTEM', 'PerfModelKey', 'ads-tec SRS2028')
                    BattDbDf.rename(columns={"Id": "ID"}, inplace=True)
                    ListOfColumns = [D for D in userinput['BatterySpec'] if D != 'Type']
                    for LC in ListOfColumns:
                        BattData[LC] = userinput['BatterySpec'][LC]
                    if 'Price' in userinput['BatterySpec']:
                        CostFunc = userinput['BatterySpec']['Price']
                        BattDbDf['Cost'] = CostFunc
                    else:
                        CostFunc = None
                else:
                    ds.put(entity)
                    return jsonify({"error": "Battery Name invalid"}), 400

                    # Calculate Size and Cost of Battery through optimisation options

    BattVar = {}
    for ind in BattData.index:
        BattVar[ind] = BattData[ind]
    EnPerCabinet = BattVar['Enom']
    if "optimise" in userinput:
        if 'constraint' in userinput['optimise']:
            constraint = userinput['optimise']['constraint']
        else:
            constraint = {}
        if 'target' in userinput['optimise']:
            target = userinput['optimise']['target']
        else:
            target = {}

        BattSize, BatteryTS, KPIs = ST.bestBattSizeWithModel(DemandTS, SolarTS, BattData=BattVar, target=target,
                                                             constraint=constraint, CostFunc=CostFunc)
        Emax = BattSize[1]
        Pmax = BattSize[0]
        NumCabinets = (Emax / EnPerCabinet)
        # BattVar = BattData.copy()
        BattVar['Enom'] = BattSize[1]
        BattVar['PmaxD'] = BattSize[0]
    # BatteryTS,_, KPIs=ST.battProfileModel(DemandTS,SolarTS,BattVar)

    else:

        # BattData.Enom *=NumCabinets
        if 'BatterySpec' in userinput:
            if 'NumCabinets' in userinput['BatterySpec']:
                NumCabinets = userinput['BatterySpec']['NumCabinets']
                BattVar['Enom'] = BattData.Enom * userinput['BatterySpec']['NumCabinets']
                BattVar['NumCabinets'] = NumCabinets
            else:
                BattVar['NumCabinets'] = 1
                NumCabinets = 1
        else:
            BattVar['Enom'] = NumCabinets * BattVar['Enom']
            BattVar['Emax'] = BattVar['Enom']  # Fetched before

        BatteryTS, _, KPIs = ST.battProfileModel(DemandTS, SolarTS, BattVar)
        BattSize = (BattVar['PmaxD'], BattVar['Enom'])
        Emax = BattSize[1]
        Pmax = BattSize[0]

    BatteryP = pd.Series(BatteryTS['Power'], name='Battery')

    BattVar['Emax'] = float(BattVar['Enom'])
    BattVar['NumCabinets'] = float(NumCabinets)
    BattVar['PmaxC'] = BattVar['PmaxD']
    BatterySize = BattVar

    #    BatterySize = {"Enom":Emax,"Emax":Emax,"PmaxC":Pmax,"PmaxD":Pmax,'NumCabinets':NumCabinets,"CycleLife":BattData.CycleLife}        

    # Add Battery Details to final quote - Size and Cost
    Quantity = float(NumCabinets)
    TotalCost = float((BattDbDf['Cost'] * Quantity).values[0])
    KPIs.update({"TotalCost": TotalCost})
    # if battery in BoM then update entry
    # Do not update or remove an 'Edit' labeled RevID
    if 'Edit' not in RevID:
        if len(BattBomId) != 0:
            _Id = BattBomId[0].item()
            ViewQuoteDf
            typeid = BattDbDf['ID'].values[0].item()
            Bat2Update = [{'_Id': _Id, "Type": "SYSTEM", "TypeID": typeid, "CreatedBy": userinput['email'],
                           "Quantity": float(Quantity), "TotalCost": float(TotalCost),
                           "PriceAdjustment": float(PriceAdjustment)
                           }]
            CDB.UpdateBoM(Bat2Update)
        else:
            # else add battery"PriceAdjustment":float(PriceAdjustment)

            Bat2Insert = [
                {"Type": "SYSTEM", "TypeID": int(BattDbDf['ID'].values[0].item()), "CreatedBy": userinput['email'],
                 "ProjectID": userinput['project_id'], "QuoteID": QuoteID, "RevID": RevID, "Quantity": float(Quantity),
                 "TotalCost": float(TotalCost), "PriceAdjustment": float(PriceAdjustment),
                 "Group": "Battery"}]
            CDB.InserttoBoM(Bat2Insert)

    # Save Battery TS 
    FinalTS = pd.concat([SolarTS, DemandTS, BatteryP], axis=1)
    blobname = USER_UPLOADS + uniqueid + '_BattTS.pkl'
    bucket.blob(blobname).upload_from_string(pickle.dumps(FinalTS))

    BatterySpec = {"Type": BattName}
    BatterySpec.update(BatterySize)

    BatterySpec.update({"BoMName": BBomName})
    OutputDic.update({"Battery": {"BatterySpec": BatterySpec, "TS": blobname, "KPI": KPIs}})

    entity.update(OutputDic)
    ds.put(entity)

    return jsonify({"KPI": KPIs, "BatterySpec": BatterySpec}), 200
    # return jsonify({"Emax":Pmax,"KPI":KPIs})


# Change this battery version
@app.route('/battery', methods=['POST'])
@jwt_authenticated
def battery():
    ds = datastore.Client()
    gcs = storage.Client()
    bucket = gcs.bucket(BUCKET_NAME)

    req_json = request.get_json()['Battery']
    userinput = req_json
    outputdic = {}
    message = {}
    userinput['email'] = request.email
    orgname = request.org
    parent_key = ds.key('users', userinput['email'])
    entity = ds.get(key=ds.key('Project', userinput['project_id'], parent=parent_key))

    if not entity:
        message.update({"error": "No project with username {} and project {} exists".format(userinput['email'],
                                                                                            userinput['project_id'])})
        return jsonify(message), 400

    uniqueid = 'Uid_' + userinput['email'] + '_Pid_' + userinput['project_id']

    entchklist = ['OptMatrix', 'SelectedConfig']

    uschklist = ['BatterySpec']
    for us in uschklist:
        if us not in userinput:
            return jsonify({"error": "{} not set in input request".format(us)}), 400
    for ent in entchklist:
        if ent not in entity:
            return jsonify({"error": "{} not set in datastore".format(ent)}), 400

            # Get SolarTS ##########
    blobname = entity['OptMatrix']['TSConfigFile']
    TSDf = pickle.loads(bucket.get_blob(blobname).download_as_string())

    configsel = entity['SelectedConfig']['Config']

    pvname = configsel['PVName']
    invname = configsel['InvName']
    Ms = configsel['Ms']
    Mp = configsel['Mp']
    ConfigSummTuple = (pvname, invname, Ms, Mp, 'PAC_tot')
    location_data = Location(entity['Location']['latitude'], entity['Location']['longitude'],
                             tz=entity['Weather']['TZ'])
    # SolarTS = TSDf[ConfigSummTuple]
    TSDfsel = TSDf[ConfigSummTuple]
    SolarTS = pd.Series(np.roll(TSDfsel.values, int(location_data.tz), axis=0), index=TSDfsel.index)
    SolarTS.name = 'Solar'

    ###### Get Demand TS ################
    if 'Demand' in entity:
        if 'DemandTS' in entity['Demand']:
            DemandTS = pickle.loads(bucket.get_blob(entity['Demand']['DemandTS']).download_as_string())
            YearlyDemand = DemandTS.sum() / 1000
        else:
            return jsonify({"error": "Demand data not entered"}), 400
    else:
        return jsonify({"error": "Demand not set in datastore"}), 400

    """
    #####GET PROFILE DATA###############
    country = "CHE"
    blobname = AUXFILES+'PriceAPICountry.json'
    CI  = json.loads(bucket.blob(blobname).download_as_string() )        
    blobnamePic = AUXFILES+CI['Data'][country]['PriceDb']    
   # path = os.getcwd()
   # e = bucket.blob(blobnamePic).download_to_filename(os.path.join(path,'PriceDb.pkl'))
    Data = pickle.loads(bucket.get_blob(blobnamePic).download_as_string())
    # with open(os.path.join(path,'PriceDb.pkl'),'rb') as f:
    #     Data = pickle.load(f)
    #####################################
    UpDic = {}
    if 'YearlyDemand' in userinput:
        TotalElec = userinput['YearlyDemand']
        DemandTS,E,H = Sup.MakeGenericDemand(Data,TotalElec=TotalElec)
        # if 'Customer Inputs' in entity:
        #     CIn = entity['Customer Inputs']
        Demandblobname = USER_UPLOADS+uniqueid+'_demandTS.pkl'
        bucket.blob(Demandblobname).upload_from_string(pickle.dumps(DemandTS))
        entity['Demand']['DemandTS'] = Demandblobname

        entity.update({'Customer Inputs':{"YearlyElecRequired":{"EleckWh":E,"Elec4HeatkWh":H,"TotalElec":E+H}}})

    elif 'DemandTS' in entity['Demand']:
        DemandTS = pickle.loads(bucket.get_blob(entity['Demand']['DemandTS']).download_as_string())
        E = DemandTS.sum()/1000
        H = 0
    elif 'Customer Inputs' in entity:
        CIn = entity['Customer Inputs']

        if 'YearlyElecRequired' in entity['Customer Inputs']:
            if (('Elec4HeatkWh' in entity['Customer Inputs']['YearlyElecRequired'])and('EleckWh' in entity['Customer Inputs']['YearlyElecRequired'])):
                TotalHeat = entity['Customer Inputs']['YearlyElecRequired']['Elec4HeatkWh']
                TotalElec = entity['Customer Inputs']['YearlyElecRequired']['EleckWh']
                DemandTS,E,H = Sup.MakeGenericDemand(Data,TotalElec=TotalElec,TotalHeat=TotalHeat)
            elif 'TotalElec' in entity['Customer Inputs']['YearlyElecRequired']:
                TotalElec = entity['Customer Inputs']['YearlyElecRequired']['TotalElec']
                DemandTS,E,H =  Sup.MakeGenericDemand(Data,TotalElec=TotalElec)        
            else:
                DemandTS,E,H = Sup.MakeGenericDemand(Data)        
        else:
            DemandTS,E,H = Sup.MakeGenericDemand(Data)

        UpDic.update({"YearlyElecRequired":{"EleckWh":E,"Elec4HeatkWh":H,"TotalElec":E+H}})
        CIn.update(UpDic)
        entity.update(CIn)
    else:
        DemandTS,E,H = Sup.MakeGenericDemand(Data)
        entity.update({'Customer Inputs':{"YearlyElecRequired":{"EleckWh":E,"Elec4HeatkWh":H,"TotalElec":E+H}}})

    YearlyDemand = (E+H)      
    """
    ##########################################################
    # Get BatteryMod from Battery Type

    BatteryMod = {"E2P": 2.5,
                  "ChargeEff": 0.98, "DischargeEff": 0.96}
    #########################################################
    SolarTS.index = DemandTS.index
    BatterySpec = userinput['BatterySpec']

    if (userinput['BatterySpec'] == 'None'):
        Emax = 0
        Pmax = 0
        NumCabinets = 0
        BattBuck = ST.BucketModel(BatteryMod)
        BatteryTS, MaxBattReqSize, KPIs = BattBuck.getBattProfile(DemandTS, SolarTS, Pmax=Pmax,
                                                                  Emax=Emax)  # E and P in W
        BatteryP = pd.Series(BatteryTS['Power'], name='Battery')

    else:
        if "optimise" in userinput:

            BatteryMod['E2P'] = userinput['BatterySpec']['Emax'] / userinput['BatterySpec']['PmaxD']
            if 'constraint' in userinput['optimise']:
                constraint = userinput['optimise']['constraint']
            else:
                constraint = None
            if 'target' in userinput['optimise']:
                target = userinput['optimise']['target']
            else:
                target = 0.8
            try:
                if 'Price' in userinput['BatterySpec']:
                    price = userinput['BatterySpec']['Price']
                    CostFunc = price / userinput['BatterySpec']['Emax']  # $/kWh
                else:
                    CostFunc = None
            except:
                CostFunc = None

            BattSize, BatteryTS, KPIs = ST.bestBattSizeWithConstraint(DemandTS, SolarTS, BattData=BatteryMod,
                                                                      CostFunc=CostFunc,
                                                                      Target=target,
                                                                      constraint=constraint)  # Optimises to target SelfConsumption    
            BatteryP = pd.Series(BatteryTS['Power'], name='Battery')
            Emax = BattSize[1]
            Pmax = BattSize[0]

            if ('Emax' in userinput['BatterySpec']):
                if (userinput['BatterySpec']['Emax']) != 0:
                    NumCabinets = BattSize[1] / (userinput['BatterySpec']['Emax'])
                else:
                    NumCabinets = 1
            else:
                NumCabinets = 1
        else:
            try:
                EmaxW = userinput['BatterySpec']['Emax'] * userinput['BatterySpec']['NumCabinets'] * 1000
                PmaxW = userinput['BatterySpec']['PmaxD'] * userinput['BatterySpec']['NumCabinets'] * 1000
                NumCabinets = userinput['BatterySpec']['NumCabinets']
            except:
                PmaxW = SolarTS.max()
                EmaxW = PmaxW * BatteryMod['E2P']
                NumCabinets = 1
            target = 0.8
            constraint = None
            BattBuck = ST.BucketModel(BatteryMod)
            BatteryTS, MaxBattReqSize, KPIs = BattBuck.getBattProfile(DemandTS, SolarTS, Pmax=PmaxW,
                                                                      Emax=EmaxW)  # E and P in W
            BatteryP = pd.Series(BatteryTS['Power'], name='Battery')
            Emax = EmaxW / 1000
            Pmax = PmaxW / 1000
    # Return Optimised Size

    FinalTS = pd.concat([SolarTS, DemandTS, BatteryP], axis=1)

    blobname = USER_UPLOADS + uniqueid + '_BattTS.pkl'
    bucket.blob(blobname).upload_from_string(pickle.dumps(FinalTS))
    CostData = entity['Cost']
    BatterySize = {"Enom": Emax, "Emax": Emax, "PmaxC": Pmax, "PmaxD": Pmax, 'NumCabinets': NumCabinets}

    if type(BatterySpec) != str:
        try:
            PriceperkWh = userinput['BatterySpec']['Price'] / userinput['BatterySpec']['Emax']
        except:
            PriceperkWh = 0
        BatterySpec.update(BatterySize)

        BatterySpec.update({"Price": PriceperkWh * Emax, "CostFunc": PriceperkWh, "target": target})
        if constraint:
            BatterySpec.update({"constraint": constraint})
        CostData.update({"BatteryCost": {"Name": userinput['BatterySpec']['Type'],
                                         "Unit": "Unit",
                                         "data": BatterySpec['Price']}})

    outputdic.update(
        {"Battery": {"BatterySpec": BatterySpec, "TS": blobname, "KPI": KPIs, "YearlyDemand": YearlyDemand}})

    entity.update(outputdic)
    ds.put(entity)

    return jsonify({"success": "Battery parameters added for project {}".format(userinput['project_id']),
                    "KPI": KPIs, "BatterySpec": BatterySpec}), 200


@app.route('/make-report', methods=['POST'])
@jwt_authenticated
def makereport():
    ds = datastore.Client()
    gcs = storage.Client()
    bucket = gcs.get_bucket(BUCKET_NAME)
    req_json = request.get_json()['Report']
    userinput = req_json
    message = {}

    userinput['email'] = request.email
    orgname = request.org
    uniqueid = 'Pid_' + userinput['project_id']
    reportname = uniqueid + "_finalreport.pdf"
    file2store = RESULTS + reportname

    parent_key = ds.key('users', userinput['email'])
    entity = ds.get(key=ds.key('Project', userinput['project_id'], parent=parent_key))

    if not entity:
        message.update({"error": "No project with username {} and project {} exists".format(userinput['email'],
                                                                                            userinput['project_id'])})
        return jsonify(message), 400

    uschklist = ['contents']
    for us in uschklist:
        if us not in userinput:
            return jsonify({"error": "{} not set in input request".format(us)}), 400
    if 'freetext' in userinput:
        freetext = userinput['freetext']
    else:
        freetext = {}

    orgentity = ds.get(key=ds.key('organisation', orgname))
    os.environ['DB_NAME'] = orgentity.get("compdb", "refdata")
    importlib.reload(CDB)
    # Organisation entity to provide name   

    if 'contents' in userinput:
        pagetype = userinput['contents']  # short, detailed
    else:
        pagetype = 'short'

    uniqueid = 'Uid_' + userinput['email']

    blobname_pvmod = USER_UPLOADS + uniqueid + '_moduledata.pkl'
    blobpv = bucket.get_blob(blobname_pvmod)
    if blobpv:
        s = blobpv.download_as_string()
        USERPVDF = pickle.loads(s)

    if 'SelectedConfig_mu' in entity:
        pvname = entity['SelectedConfig_mu']['Config']['PVName']
        invname = [n for n in entity['SelectedConfig_mu']['Config']['InvCombo'].keys()]
        InvData = {name: INVDF.loc[:, name].to_dict() for name in invname}

    else:
        entchecklist = ['SelectedConfig', 'Location', 'OptMatrix', 'Weather', 'Cost']
        for ec in entchecklist:
            if ec not in entity:
                return jsonify({"error": "{} not set in datastore".format(ec)}), 400

        pvname = entity['SelectedConfig']['Config']['PVName']
        invname = entity['SelectedConfig']['Config']['InvName']
        InvData = INVDF.loc[:, invname]

    # InvData = INVDF.loc[:,invname]
    if pvname in PVDF:
        PVModuleData = PVDF.loc[:, pvname]
    elif pvname in PVSYSDF:
        PVModuleData = PVSYSDF.loc[:, pvname]
    elif blobpv:
        if pvname in USERPVDF:
            PVModuleData = USERPVDF.loc[:, pvname]
    else:
        return jsonify({"Error": "PV Module {} not found in any database".format(pvname)}), 400

        # PVModuleData = PVDF.loc[:,entity['SelectedConfig']['Config']['PVName']]

    # contentlist =[cont for cont in userinput['contents'] if cont in templatepages.keys()]                                                                                   

    entity.update({"status": "Final Proposal"})
    # ds.put(entity)

    # Read settings from 
    # templatesettings=json.loads(bucket.get_blob(AUXFILES+"templatesettings.json").download_as_string())
    # Read templatesettings from orgentity
    templatesettings = orgentity['templatesettings']

    if "template" in userinput:
        templatekey = str(userinput['template'])
        if templatekey not in templatesettings:
            templatekey = [d for d in templatesettings.keys()][0]
    else:
        templatekey = [d for d in templatesettings.keys()][0]

    # lang = templatesettings[templatekey].get('lang','DE')
    if 'lang' in userinput:
        lang = userinput['lang']
    else:
        lang = templatesettings[templatekey].get('lang', 'DE')
    # if pagetype=='detailed': 
    #     page = 'pages-det'
    # else:
    #     page='pages'
    if 'pages' not in userinput:
        pages = templatesettings[templatekey]['pages']
    else:
        pages = []
        pageinput = userinput['pages']
        # Validate if input
        for page in pageinput:
            if page in orgentity.get('templatepages', {}):
                pages.append(page)

    if "addon2download" in templatesettings[templatekey]:
        addon2download = templatesettings[templatekey]['addon2download']
    else:
        addon2download = []

    # TODO: Read Below from orginfo or central settings
    tempfuncfolder = {"template-ag-agsolar-fix-kurz": {"func": "template_ag_agsolar_fix_kurz",
                                                       "folder": "template-ag-agsolar-fix-kurz"},
                      "template-ag-efh-fix-kurz": {"func": "template_ag_efh_fix_kurz",
                                                   "folder": "template-ag-efh-fix-kurz"},
                      "template-ag-efh-richt-kurz": {"func": "template_ag_efh_richt_kurz",
                                                     "folder": "template-ag-efh-richt-kurz"},
                      "template-ag-mfh-fix-kurz": {"func": "template_ag_mfh_fix_kurz",
                                                   "folder": "template-ag-mfh-fix-kurz"},
                      "template-ag-mfh-richt-kurz": {"func": "template_ag_mfh_richt_kurz",
                                                     "folder": "template-ag-mfh-richt-kurz"},
                      "fr-template-ag-mfh-fix-kurz": {"func": "template_ag_mfh_fix_kurz",
                                                      "folder": "fr-template-ag-mfh-fix-kurz"},
                      "fr-template-ag-efh-fix-kurz": {"func": "template_ag_efh_fix_kurz",
                                                      "folder": "fr-template-ag-efh-fix-kurz"},
                      "fr-template-ag-agsolar-fix-kurz": {"func": "template_ag_agsolar_fix_kurz",
                                                          "folder": "fr-template-ag-agsolar-fix-kurz"},
                      "template-ag-mfh-fix-lease": {"func": "template_ag_mfh_fix_kurz",
                                                    "folder": "template-ag-mfh-fix-kurz"},

                      "template-agnew-en": {"func": "template_agnew_en", "folder": "template-agnew-en"},
                      "template-agnew-blue-pt": {"func": "template_agnew_blue_pt", "folder": "template-agnew-blue-pt"},
                      "template-pt-short": {"func": "template_agnew_blue_pt", "folder": "template-pt-short"},
                      "template-tech-report": {"func": "template_tech_report", "folder": "template-tech-report"},
                      "template-br-homolog": {"func": "template_br_homolog", "folder": "template-br-homolog"},

                      "template-gen-v1": {"func": "template_gen_v1", "folder": "template-gen-v1-<lang>"},
                      "template-gen-v2": {"func": "template_gen_v2", "folder": "template-gen-v2-<lang>"},

                      }
    templatefolder = tempfuncfolder.get(templatekey, 'template-gen-v1').get('folder').replace('<lang>', lang)
    templatefunc = tempfuncfolder.get(templatekey, 'template-gen-v1').get('func')

    path2template = AUXFILES + templatefolder

    # if lang=='FR':
    #     templatename = templatename.split('fr-')[-1] #'fr-'+templatename
    R = RepM.Report(bucket, templatename=templatefunc, lang=lang, path2template=path2template)

    err = R.gettemplatefrombucket(bucket, path2template)
    if err == "ERROR":
        return jsonify({"error": R.errmsg})
    R.GenReportVars(entity, PVModuleData, InvData, orgentity)

    # Add user content to ReportContents
    # firstpage_paragraphs = freetext.get("firstpage", "").split("\n")
    lastpage_paragraphs = freetext.get("lastpage", "").split("\n")
    payment_mode = freetext.get("paymentmode", "emi")

    payments = []
    paymentschedule = []
    if payment_mode == "emi":
        payment_data = freetext.get("payment", {})
        if payment_data:
            payments.append([" ", "Upfront Amount = {}".format(payment_data.get("upfrontamount"))])
            payments.append(
                [" ", "EMI Amount = {} X {}".format(payment_data.get("totalemi"), payment_data.get("emiamount"))])
        else:
            payments.append(["", ""])
    else:
        payment_data = freetext.get("payment", {})
        if payment_data:
            for temp in payment_data:
                payments.append(
                    ["{} % {} - {}".format(temp.get("precentage"), temp.get("description"), temp.get("value")), ""])
                paymentschedule.append({"percentage": temp.get("precentage"), "description": temp.get("description")})
        else:
            payments.append(["", ""])
    if 'paymentschedule' in userinput:
        paymentschedule = userinput['paymentschedule']
    if 'ReportContentsStored' in entity:
        paymentschedule = entity['ReportContentsStored'].get('paymentschedule', [])
    R.ReportContents.update({
        "payments": payments,
        "freetext": freetext,
        # "firstpage_paragraphs": firstpage_paragraphs,
        "lastpage_paragraphs": lastpage_paragraphs,
        "payment_mode": payment_mode,
        "paymentschedule": paymentschedule
    })
    ################# STORE EDITABLES ##################
    ToSaveForEdits = R.ReportContents.get('ToSaveForEdits', [])
    if 'manualedit' in userinput:  # Use ReportContentsStored
        RC2Store = entity.get('ReportContentsStored', {})

        for keyvalues in userinput['manualedit']:
            if keyvalues in RC2Store:
                if keyvalues == 'BoMDisplay':
                    SavedBoMDisplay = pd.DataFrame(RC2Store['BoMDisplay'])
                    CurrentBoMDisplay = R.ReportContents['BoMDisplay']
                    for i in CurrentBoMDisplay.index:
                        Description = \
                        SavedBoMDisplay.loc[SavedBoMDisplay['DisplayName'] == CurrentBoMDisplay.loc[i]['DisplayName']][
                            'Description']
                        if len(Description) > 0:
                            CurrentBoMDisplay.loc[i, 'Description'] = Description.values[0]

                    R.ReportContents.update({'BoMDisplay': CurrentBoMDisplay})
                    # Only Update Descriptions   
                    # R.ReportContents.update({'BoMDisplay':pd.DataFrame(RC2Store[keyvalues])})
                else:
                    R.ReportContents.update({keyvalues: RC2Store[keyvalues]})

    elif 'overwrite_auto' in userinput:  # Use Automatic Values  for overwrite
        ToSaveForEdits = R.ReportContents.get('ToSaveForEdits', [])
        # ToSaveForEdits = ["BoMDisplay", "firstpage_paragraphs",
        #             "payment_mode","lastpage_paragraphs","tandc1","paymentschedule"]

        # Save Following Items of ReportContents for View and Edits
        RC2Store = {}
        for keyvalues in ToSaveForEdits:
            if keyvalues == 'BoMDisplay':
                RC2Store.update({keyvalues: R.ReportContents.get('BoMDisplay').to_dict('list')})
            else:
                RC2Store.update({keyvalues: R.ReportContents.get(keyvalues)})
            if 'ReportContentsStored' in entity:
                entity['ReportContentsStored'].update(RC2Store)
            else:
                entity.update({'ReportContentsStored': RC2Store})
        if userinput['overwrite_auto'] == []:  # first time
            ds.put(entity)
            return jsonify({"ReportContentsStored": RC2Store}), 200
    else:
        ToSaveForEdits = R.ReportContents.get('ToSaveForEdits', [])
        # Just Save the ToSaveForEdits keys in entity

        RCStored = dict(entity.get('ReportContentsStored', {})).copy()
        for keyvalues in ToSaveForEdits:
            if keyvalues not in RCStored:
                RCStored.update({keyvalues: R.ReportContents.get(keyvalues, {})})
        RCStored.update({'ToSaveForEdits': ToSaveForEdits})
        if 'ReportContentsStored' in entity:
            entity['ReportContentsStored'].update(RCStored)
        else:
            entity.update({'ReportContentsStored': RCStored})

        ds.put(entity)
        return jsonify({"ReportContentsStored": RCStored}), 200
    ds.put(entity)
    ################### STORE EDITABLES ##################    

    Path = R.rendertemplate(reportname, pages)

    R.uploadtobucket(Path, file2store)

    urltodownload = "https://www.googleapis.com/storage/v1/b/" + BUCKET_NAME + "/o/" + \
                    file2store.replace('/', '%2F') + "?alt=media"

    _ = R.localstoredfiles
    R.removetempfiles()
    # "debug":P
    arraydownload = [urltodownload]

    if addon2download:
        for filepack in addon2download:
            filepackname = "https://www.googleapis.com/storage/v1/b/" + BUCKET_NAME + "/o/" + \
                           filepack.replace('/', '%2F') + "?alt=media"
            arraydownload.append(filepackname)

    OutputDic = {"url2download": arraydownload}

    return jsonify(OutputDic), 200


@app.route('/leadtoproject', methods=['POST'])
@jwt_authenticated
def lead2project():
    ds = datastore.Client()
    gcs = storage.Client()
    bucket = gcs.get_bucket(BUCKET_NAME)

    # message = {}
    record = {}

    req_json = request.get_json()['LeadDetails']
    userinput = req_json
    userinput['email'] = request.email
    orgname = request.org
    # TODO: Authentication for certain proj ids to be able to access Lead Ids
    # and allocated to other proj_emails

    chklist = ['lead_email', 'lead_id', 'proj_email', 'projectname']  # Key Values that must be there
    # Still missing, "map address"
    for us in chklist:
        if us not in userinput:
            return jsonify({"error": "{} not set in input request".format(us)}), 400

    L_uniqueid = 'UID_' + userinput['lead_email'] + '_PID_' + userinput['lead_id']

    # Parent key for lead
    l_parent_key = ds.key('users', userinput['lead_email'])
    lead_id = userinput['lead_id']
    lead_email = userinput['lead_email']
    proj_email = userinput['proj_email']
    # entity for lead
    l_entity = ds.get(key=ds.key('Leads', lead_id, parent=l_parent_key))
    # Parent key for project
    p_parent_key = ds.key('users', proj_email)
    # entity for project
    p_entity = ds.get(key=p_parent_key)

    if not l_entity:
        return jsonify({"error": "No lead with username {} and lead id {} exists".format(lead_email, lead_id)})
    if not p_entity:
        return jsonify({"error": "No project with username {}  exists".format(proj_email)})

    ProjId = str(uuid.uuid1())
    entity = datastore.Entity(key=ds.key('users', userinput['proj_email'], 'Project', ProjId))
    p_uniqueid = 'Uid_' + userinput['proj_email'] + '_Pid_' + ProjId

    ListOfItems = ['Demand', 'Location', 'Weather', 'EpriceTariffFile', 'Orientation', 'Priority', 'Customer Inputs']
    for pitems in ListOfItems:
        if pitems not in l_entity:
            pass
        else:
            entity[pitems] = l_entity[pitems]
    customerinfo = {}
    entity['projectdetails'] = {}
    entity['projectdetails']['projectname'] = userinput['projectname']
    ### TRANSFER OF FILES IN BUCKETS #####
    if 'EpriceTariffFile' in l_entity:
        blobname = l_entity['EpriceTariffFile']
        p_blobname = USER_UPLOADS + p_uniqueid + '_epriceTariff.pkl'
        try:
            bucket.rename_blob(bucket.get_blob(blobname), p_blobname)
        except:
            pass
        entity['EpriceTariffFile'] = p_blobname

    ### TRANSFER OF MAP LAYOUT ###
    p_uniqueid = 'UID_' + userinput['proj_email'] + '_PID_' + ProjId
    d2layoutFname = USER_UPLOADS + L_uniqueid + '_2D_layout.json'
    d3layoutFname = USER_UPLOADS + p_uniqueid + '_3D_scene_layout.json'
    # try:
    #     bucket.rename_blob(bucket.get_blob(d2layoutFname),d3layoutFname)
    # except:
    #     pass
    #### RENAME 2d to 3d MAPS #####

    if 'customerinfo' in l_entity:
        if 'projectaddress' in l_entity['customerinfo']:
            entity['projectdetails']['projectaddress'] = "{}, {}, {}".format(l_entity['customerinfo']['projectaddress']
                                                                             , l_entity['customerinfo']['zip']
                                                                             , l_entity['customerinfo']['city'])
            entity['projectdetails']['customerinfo'] = l_entity['customerinfo']
            for keys in l_entity['customerinfo']:
                customerinfo[keys] = l_entity['customerinfo'][keys]

        else:
            return jsonify({"error": "projectaddress not set in customerinfo field"}), 400
    else:
        return jsonify({"error": "customerinfo not collected"}), 400

    entity['created'] = dt.datetime.utcnow()
    entity['status'] = 'From Lead'

    l_entity['status'] = 'Moved to Project'

    """
    Mapping of entities from Lead to Project

    Project:    Lead

    Demand : Demand

    [customerinfo] : [customerinfo]
    [projectdetails][projectaddress] = [customerinfo][projectaddress]
    Location: Location
    Customer Inputs: Customer Inputs
    Weather : Weather
    Orientation: Orientation

    UID_<email>_PID_<leadid>_3D_scene_layout.json <-: UID_<email>_PID_<projectid>_2D_scene_layout.json

    add attributes: ,“height”:6,“graphicID”:5,“roofAngle”:30,“turned”:1,“roof”:“gable”,
    Priority : Priority

    EpriceTariffFile : EpriceTariffFile

    """
    ds.put(entity)
    ds.put(l_entity)
    record.update({"Success": "Lead {lead_id} converted to project id {project_id} for user {proj_email}".format(
        lead_id=lead_id, project_id=ProjId, proj_email=proj_email)})
    record.update({"project_id": ProjId})
    record.update({"customerinfo": customerinfo})

    return jsonify(record), 200


@app.route('/cloneproject', methods=['POST'])
@jwt_authenticated
def cloneproject():
    ds = datastore.Client()
    gcs = storage.Client()
    bucket = gcs.get_bucket(BUCKET_NAME)

    req_json = request.get_json()
    userinput = req_json
    userinput['email'] = request.email
    orgname = request.org
    parent_key = ds.key('users', userinput['email'])
    entity = ds.get(key=ds.key('Project', userinput['project_id'], parent=parent_key))
    pid = userinput['project_id']
    message = {}
    if not entity:
        message.update({"error": "No project with username {} and project {} exists".format(userinput['email'],
                                                                                            userinput['project_id'])})
        return jsonify(message), 400

    # Allow transfer only within the organisation
    # TODO: Name Suggestion
    findusertotransfer = userinput.get('finduser')
    orgentity = ds.get(key=ds.key('organisation', orgname))
    if findusertotransfer:
        listofusers = orgentity.get('users', [])

        return jsonify({"userlist": listofusers}), 200
    thisprojectname = entity['projectdetails']['projectname']
    basenamefilter = '^(.*)_\D\d*$'
    try:
        baseprojectname = re.findall(basenamefilter, thisprojectname)[0]
    except IndexError:
        baseprojectname = thisprojectname

    if userinput.get('version'):

        query = ds.query(kind='Project', ancestor=parent_key)
        query.order = ['-created']
        # query.add_filter('status','=','Final Proposal')

        Q = query.fetch(limit=int(100))

        projList = []
        attribs = [{'ActiveQuote': ['QuoteID']}, {'projectdetails': ['projectname']}, 'status', 'created']

        projectnameCheck = baseprojectname
        for s in Q:
            if s.get('projectdetails'):
                if projectnameCheck in s['projectdetails']['projectname']:
                    entry = {"id": str(s.key.name)}
                    for att in attribs:
                        if type(att) != dict:

                            if att in s:
                                entry.update({att: s[att]})
                        elif type(att) == dict:
                            if list(att.keys())[0] in s:
                                mainatt = str(list(att.keys())[0])  # Main attribute and subattributes 
                                _ = [entry.update({subatt: s[mainatt][subatt]}) for subatt in list(att.values())[0] if
                                     subatt in list(s[mainatt].keys())]
                    projList.append(entry)

        vnumfilter = re.compile('^.*_\D(\d*)$')
        LatestVersion = max(
            [int(vnumfilter.findall(p['projectname'])[0]) for p in projList if vnumfilter.findall(p['projectname'])],
            default=0)

        newprojectname = baseprojectname + '_R' + str(LatestVersion + 1)
        try:
            if LatestVersion == 0:
                BaseQuoteID = [p['QuoteID'].split('_R')[0] for p in projList if
                               ((p['projectname'] == str(baseprojectname)) and ('QuoteID' in p))][0]
            else:
                BaseQuoteID = [p['QuoteID'].split('_R')[0] for p in projList if (
                            p['projectname'] == str(baseprojectname + '_R' + str(LatestVersion)) and ('QuoteID' in p))][
                    0]
        except:
            BaseQuoteID = entity.get('ActiveQuote', {}).get('QuoteID', '1')
        NewQuoteID = BaseQuoteID + '_R' + str(LatestVersion + 1)
        newuseremail = userinput.get('newusername', userinput['email'])
        entity.update({'status': 'Lock'})
    else:
        newprojectname = userinput.get('newprojectname', str(entity['projectdetails']['projectname']))
        newuseremail = userinput.get('newusername', userinput['email'])
        if 'ActiveQuote' in entity:
            # NewQuoteID = entity['ActiveQuote']['QuoteID']
            NewQuoteID = int(orgentity['bominput'].get('QuoteAutoIncUID', '100000')) + 1
            orgentity['bominput'].update({'QuoteAutoIncUID': NewQuoteID})

    os.environ['DB_NAME'] = orgentity.get("compdb", "refdata")
    importlib.reload(CDB)

    # newprojectname = userinput.get('newprojectname','new_'+str(entity['projectdetails']['projectname']))
    # newuseremail = userinput.get('newusername',userinput['email']) 

    # Check if user exeists
    newuserkey = ds.key('users', newuseremail)
    newentity = ds.get(key=newuserkey)
    if not newentity:
        return jsonify({"error": "user {} does not exist ".format(newuseremail)})

    ProjId = str(uuid.uuid1())
    NEWentity = datastore.Entity(key=ds.key('Project', ProjId, parent=newuserkey))

    outp = copy.deepcopy(entity)
    NEWentity.update(outp)

    projdetails = entity['projectdetails']
    projupdate = {k: v if k != 'projectname' else newprojectname for k, v in projdetails.items()}
    NEWentity.update({"projectdetails": projupdate})
    if 'ActiveQuote' in entity:
        NEWentity.update({'ActiveQuote': {"QuoteID": NewQuoteID, "RevID": entity['ActiveQuote']['RevID'],
                                          "ProductName": entity['ActiveQuote'].get('ProductName', '')}})

    NEWentity.update({'created': dt.datetime.now()})
    if entity['status'] == 'Lock':
        NEWentity.update({'status': 'Final Proposal'})

    ## Datastore replicated
    uniqueid = "Uid_" + userinput['email'] + "_Pid_" + userinput['project_id']
    sourcebloblists = ["UserUploads/" + uniqueid + "_physicallayout.json",
                       "UserUploads/" + uniqueid + "_layoutarea.json",
                       "Results/" + uniqueid + "_Configs.pkl", "Results/" + uniqueid + "_TSConfigs.pkl",
                       "UserUploads/" + uniqueid + "_epriceTariff.pkl", "UserUploads/" + uniqueid + "_demandTS.pkl",
                       "UserUploads/" + uniqueid + "_BattTS.pkl",
                       "FetchedWeather/" + uniqueid + "_weather.pkl",
                       "Results/" + uniqueid + "_YearlyBenefits.pkl",
                       "Results/" + uniqueid + "_CashFlow.pkl"]  # "UserUploads/UID_"+userinput['email']+"_PID_"+userinput['project_id']+"_3D_scene_layout.json",
    # Replicate bucket contents
    # find all 3d_scene_layouts
    for blob in gcs.list_blobs(BUCKET_NAME, prefix="UserUploads/UID_" + userinput['email'] + "_PID_" + userinput[
        'project_id'] + "_3D_scene_layout"):
        sourcebloblists.append(str(blob.name))

    # OptDf, TSDf, layoutarea.json, 3dscene_layout.json, EpriceTariffFile, physicallayout, BatteryTS
    # Change all the file names
    def getpath(nesteddict, value, prepath=()):
        for k, v in nesteddict.items():
            path = prepath + (k,)
            if value == str(v):
                return path
            elif hasattr(v, 'items'):
                p = getpath(v, value, path)  # recursive call
                if p is not None:
                    return p

    def getval(dic, paths, val=None):

        if val:
            Dic = dic
            for pp in paths[:-1]:
                Dic = Dic[pp]
            Dic.update({paths[-1]: val})
        else:
            for pp in paths:
                dic = dic[pp]
        return dic

        # Replicate blob names in entities    

    # for blobnames in sourcebloblists:
    #     path2val = getpath(entity,blobnames)
    #     if path2val:
    #         Oldvalue = getval(entity,path2val)
    #         Newvalue = Oldvalue.replace(pid, ProjId).replace(userinput['email'],newuseremail)
    #         #print("OldV  {} --  Newvalue  {}".format(Oldvalue,Newvalue))

    #         getval(NEWentity,path2val,Newvalue)

    # Replicate blobs in cloud storage        
    newbloblist = []

    for oldblob_name in sourcebloblists:
        newblob_name = oldblob_name.replace(pid, ProjId).replace(userinput['email'], newuseremail)
        if bucket.get_blob(oldblob_name):
            newblob = bucket.copy_blob(bucket.get_blob(oldblob_name), bucket, newblob_name)
            newbloblist.append(newblob.name)

    # COPY THEM ALL

    # Datastore and Buckets copied
    # Validate Entities are pointing to the same place
    Newuniqueid = uniqueid.replace(pid, ProjId).replace(userinput['email'], newuseremail)
    if 'GeoCoords' in NEWentity.get('Location', {}):
        NEWentity['Location']['GeoCoords'] = "UserUploads/" + Newuniqueid + "_layoutarea.json"
    if 'PanelCoords' in NEWentity.get('Location', {}):
        NEWentity['Location']['PanelCoords'] = "UserUploads/" + Newuniqueid + "_physicallayout.json"
    if 'DemandTS' in NEWentity.get('Demand', {}):
        NEWentity['Demand']['DemandTS'] = "UserUploads/" + Newuniqueid + "_demandTS.pkl"
    if NEWentity.get('EpriceTariffFile'):
        NEWentity['EpriceTariffFile'] = "UserUploads/" + Newuniqueid + "_epriceTariff.pkl"
    if 'finalweatherfile' in NEWentity.get('Weather', {}):
        NEWentity['Weather']['finalweatherfile'] = "FetchedWeather/" + Newuniqueid + "_weather.pkl"
    if 'TS' in NEWentity.get('Battery', {}):
        NEWentity['Battery']['TS'] = "UserUploads/" + Newuniqueid + "_BattTS.pkl"
    if 'OptConfigFile' in NEWentity.get('OptMatrix', {}):
        NEWentity['OptMatrix']['OptConfigFile'] = "Results/" + Newuniqueid + "_Configs.pkl"
    if 'TSConfigFile' in NEWentity.get('OptMatrix', {}):
        NEWentity['OptMatrix']['TSConfigFile'] = "Results/" + Newuniqueid + "_TSConfigs.pkl"
    if 'YearlyBenefit' in NEWentity.get('Financial', {}):
        if type(entity['Financial']['YearlyBenefit']) == str:
            NEWentity['Financial']['YearlyBenefit'] = "Results/" + Newuniqueid + "_YearlyBenefits.pkl"
    if 'CashFlow' in NEWentity.get('Financial', {}):
        if type(entity['Financial']['CashFlow']) == str:
            NEWentity['Financial']['CashFlow'] = "Results/" + Newuniqueid + "_CashFlow.pkl"

    # Cloud Sql ActiveQuote  - Only Last Revision of ComponentDb replicated
    # Replicate- last t
    if 'ActiveQuote' in entity:

        QuoteID = entity['ActiveQuote']['QuoteID']
        RevID = entity['ActiveQuote']['RevID']
        # Get all RevIds 
        querytext = "SELECT DISTINCT RevID FROM BOM_QUOTE WHERE `ProjectID`='{}' and `QuoteID`='{}' \
                     ;".format(userinput['project_id'], QuoteID)

        res = CDB.RunRawSQLQuery(querytext)
        if len(res) > 0:
            RevIDlist = [l[0] for l in res]
        else:
            RevIDlist = [RevID]

            # RevFormat=re.compile('^(R|V)\d*$') # Check for format 'R#' only, exclude R5-Edit, R2-Edit etc
        # if not re.match(RevFormat,RevID):
        #     RevIDlist.append(re.search('^(R|V)\d*',RevID).group())
        for RID in RevIDlist:
            ViewQuoteDf = CDB.GetBoMQuote(pid, QuoteID, RID)
            ViewQuoteDf['ProjectID'] = ProjId
            ViewQuoteDf['QuoteID'] = NewQuoteID
            ViewQuoteDf['TypeID'] = ViewQuoteDf['ID']
            ViewQuoteDf['Type'] = 'SYSTEM'
            ViewQuoteDf.fillna(np.nan, inplace=True)
            ViewQuoteDf.replace([np.nan], [None], inplace=True)
            # Make BOM
            ViewQuoteDf.drop_duplicates(subset=['ID'], inplace=True)
            Proxy = CDB.RunRawSQLQuery("SHOW COLUMNS FROM BOM_QUOTE")
            # Columns = [l._row[0] for l in Proxy if l._row[0] in ViewQuoteDf.columns and l._row[0]!='Id' ] # Get all Columns in BoMQuote
            Columns = [l[0] for l in Proxy if l[0] in ViewQuoteDf.columns and l[0] != 'Id']
            VQDict = ViewQuoteDf[Columns].to_dict(orient='records')
            try:
                CDB.InserttoBoM(VQDict, tablename='BOM_QUOTE')
            except:
                pass

    ds.put(NEWentity)
    ds.put(entity)
    ds.put(orgentity)
    return jsonify({"project_id": ProjId, "projectname": newprojectname, "user": newuseremail, "bloblist": newbloblist,
                    "quote": NewQuoteID}), 200


@app.route('/storedblobs', methods=['POST', 'GET'])
@jwt_authenticated
def getblobs():
    ds = datastore.Client()
    gcs = storage.Client()
    bucket = gcs.get_bucket(BUCKET_NAME)

    # req_json=request.get_json()
    pid = request.args.get('project_id', '')
    userinput = {}
    userinput['project_id'] = pid  # req_json['project_id']
    userinput['email'] = request.email
    orgname = request.org
    parent_key = ds.key('users', userinput['email'])
    entity = ds.get(key=ds.key('Project', userinput['project_id'], parent=parent_key))
    pid = userinput['project_id']
    itemstofetch = request.args.get('items', {})
    message = {}
    if not entity:
        message.update({"error": "No project with username {} and project {} exists".format(userinput['email'],
                                                                                            userinput['project_id'])})
        return jsonify(message), 400

    itemslist = [r.strip() for r in re.split(',', itemstofetch)]
    key2display = {"3dscene": "Layout", "pdfreport": "Offer", "sld": "SLD"}

    # ['3dscene', 'pdfreport', 'sld']
    # args : {blobs = }
    blobinfo = {}
    uniqueid = "Uid_" + userinput['email'] + "_Pid_" + userinput['project_id']
    uniqueprefix = RESULTS + uniqueid

    for key in itemslist:
        archiveblob = []
        displayname = key2display.get(key, key)
        if key == '3dscene':
            blobprefix = USER_UPLOADS + "UID_" + userinput['email'] + "_PID_" + userinput[
                'project_id'] + "_3D_scene_layout"
            baseurl = "https://www.googleapis.com/storage/v1/b/" + BUCKET_NAME + "/o/" + \
                      (blobprefix + '.json').replace('/', '%2F') + "?alt=media"
            baseblob = bucket.get_blob(blobprefix + '.json')

            r = re.compile('.*_[v]{1}\d.json$')
            vspattern = '.*_([v]{1}\d).json$'
        elif key == 'pdfreport':
            blobprefix = RESULTS + "Pid_" + userinput['project_id'] + "_finalreport"
            baseurl = "https://www.googleapis.com/storage/v1/b/" + BUCKET_NAME + "/o/" + \
                      (blobprefix + '.pdf').replace('/', '%2F') + "?alt=media"
            baseblob = bucket.get_blob(blobprefix + '.pdf')
            r = re.compile('.*_[v]{1}\d.pdf$')
            vspattern = '.*_([v]{1}\d).pdf$'
        else:  # key should be the entire suffix
            blobprefix = RESULTS + "Pid_" + userinput['project_id'] + key
            baseurl = "https://www.googleapis.com/storage/v1/b/" + BUCKET_NAME + "/o/" + \
                      (blobprefix).replace('/', '%2F') + "?alt=media"
            baseblob = bucket.get_blob(blobprefix)
            r = re.compile('.*_[v]{1}\d\..*$')
            vspattern = '.*_([v]{1}\d)\..*$'

        if baseblob:
            baseblobitem = {"url2download": baseurl, "Updated": dt.datetime.strftime(baseblob.updated, "%d/%m/%y %T"),
                            "displayname": f"{displayname} current", "version": "0"}
            archiveblob.append(baseblobitem)
            ####### SAVE A VERSION###########
        if request.method == 'POST':
            params = request.get_json()
            action = params.get('action')
            version = params.get('version', '1')
            try:

                if 9 < int(version) < 1:
                    version = '1'
            except ValueError:
                version = '1'

            if action == 'save':
                oldblob_name = baseblob.name
                suffix = re.findall('.*\.(.{3,4})$', oldblob_name)[0]

                newblob_name = oldblob_name.replace(f'.{suffix}', f'_v{version}.{suffix}')

                bucket.copy_blob(bucket.get_blob(oldblob_name), bucket, newblob_name)

            elif action == 'current':
                suffix = re.findall('.*\.(.{3,4})$', baseblob.name)[0]
                oldblob_name = baseblob.name.replace(f'.{suffix}', f'_v{version}.{suffix}')
                newblob_name = baseblob.name
                try:
                    bucket.copy_blob(bucket.get_blob(oldblob_name), bucket, newblob_name)
                except AttributeError:
                    pass
            elif action == 'delete':
                suffix = re.findall('.*\.(.{3,4})$', baseblob.name)[0]
                todeleteblobname = baseblob.name.replace(f'.{suffix}', f'_v{version}.{suffix}')
                try:
                    bl = bucket.get_blob(todeleteblobname)
                    if bl:
                        bl.delete()
                except AttributeError:
                    pass

        ############################ 
        bloblist = gcs.list_blobs(BUCKET_NAME, prefix=blobprefix)

        for blob in bloblist:
            if r.match(blob.name):
                file2store = r.match(blob.name).group()
                url2download = "https://www.googleapis.com/storage/v1/b/" + BUCKET_NAME + "/o/" + \
                               file2store.replace('/', '%2F') + "?alt=media"
                vs = re.findall(vspattern, file2store)[0]
                archiveblob.append({"url2download": url2download,
                                    "Updated": dt.datetime.strftime(blob.updated, "%d/%m/%y %T"),
                                    "displayname": f"{displayname} {vs}",
                                    "version": vs.split('v')[-1]})

        blobinfo.update({key: archiveblob})

    return jsonify(blobinfo), 200


@app.route('/makevizlink', methods=['POST'])
@jwt_authenticated
def makevizlink():
    ds = datastore.Client()
    gcs = storage.Client()
    bucket = gcs.bucket(BUCKET_NAME)
    userinput = request.get_json()
    userinput['email'] = request.email
    orgname = request.org
    parent_key = ds.key('users', userinput['email'])
    entity = ds.get(key=ds.key('Project', userinput['project_id'], parent=parent_key))
    orgentity = ds.get(key=ds.key('organisation', orgname))
    Status = ["Results Summary", "Final Proposal", "Lock"]
    if entity.get('status', '') not in Status:
        return jsonify({"error": "project incomplete at {} state ".format(entity.get('status', ''))}), 400

    # orgentity['viz3doptions']= {"tabs2toshow":"1,2,3,4,5"} # 1,2,3,4
    tabstoshow = None
    if 'viz3doptions' in orgentity:
        tabstoshow = orgentity['viz3doptions'].get('tabstoshow')

    VzL = VzLink.VizLink(entity, bucket)
    addedlink = VzL.collectAndSaveJson()
    excellink = VzL.TSforExport()
    status = {}
    status.update(addedlink)
    if 'delete' in userinput:
        deletedlink = VzL.deleteprojectfolder()
        status.update(deletedlink)
    day2expire = userinput.get('expirydays', 60)
    stringtime = dt.datetime.now() + dt.timedelta(days=int(day2expire))

    linkparameter_raw = userinput['project_id'] + "_" + dt.datetime.strftime(stringtime, "%Y%m%d")
    # tabs in orgentity to steer the viz3d according to org 
    if tabstoshow:
        linkparameter_raw += "_tabs_" + tabstoshow
    linkparameter_encoded = base64.b64encode(linkparameter_raw.encode('utf-8'))
    linkurl = "https://vizview.solextron.com/?projectId={}".format(linkparameter_encoded.decode('utf-8'))
    entity.update({"vizlink": linkurl})
    ds.put(entity)

    return jsonify(
        {"success": "added link", "linkforviz": linkurl, "expires": dt.datetime.strftime(stringtime, "%Y-%m-%d"),
         "excellink": excellink}), 200


@app.errorhandler(500)
def server_error(e):
    logging.exception('An error occurred during a request.')
    return """
    An internal error occurred: <pre>{}</pre>
    See logs for full stacktrace.
    """.format(getattr(e, "original_exception", None)), 500


if __name__ == '__main__':

    Envvar = {"Environ":
                  {"BUCKET_NAME": "solbucket",
                   "PV_DB_FILENAME": "CECModules2020-06.pkl",
                   "PVS_DB_FILENAME": "PvSystModules2020-06.pkl",
                   "INV_DB_FILENAME": "BigInvCEC04-2021.pkl",
                   "NASA_22YR_FILENAME": "22yr_nasa_monthly.pkl",
                   "USER_UPLOADS_FOLDER": "UserUploads/",
                   "FETCHED_WEATHER_FOLDER": "FetchedWeather/",
                   "AUXFILES_FOLDER": "AuxFiles/",
                   "RESULTS_FOLDER": "Results/",
                   "ELECPARAMS": "elec_design_params.json",
                   "FINPARAMS": "finparameter_inputs.json",
                   "DB_NAME": "refdata"}}

    for var in Envvar['Environ']:
        os.environ[var] = Envvar['Environ'][var]
    # os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = os.path.join("/Users","vipluv.aga","Code","auth",
    #                                                             "gcloudauthkeys","solex-mvp-2-e18a10831f31.json")
    credential_path = r"C:\Users\dell\Downloads\application_default_credentials.json"
    # project=r"solex-mvp-2"
    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = credential_path
    os.environ['SQLALCHEMY_LOCALDB_URI'] = "mysql+pymysql://root:default123@127.0.0.1:3306/refdata"
    os.environ['DB_HOST'] = '127.0.0.1'

    os.environ['FLASK_ENV'] = 'development'

    app.run(host='127.0.0.1', port=8080, use_reloader=True)

# !/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 31 23:38:49 2018

@author: vaga
"""

import json, re
import logging
import os
import datetime as dt
import numpy as np
import pandas as pd
import uuid
import pickle
import math
import importlib
import base64

from middlewareauth import jwt_authenticated
from flask import Flask, request, jsonify
from google.cloud import storage, datastore

import time
import copy

if __name__ == '__main__':
    Envvar = {"Environ":
                  {"BUCKET_NAME": "solbucket",
                   "PV_DB_FILENAME": "CECModules11-2021.pkl",
                   "PVS_DB_FILENAME": "PvSystModules2021-11.pkl",
                   "BATT_DB_FILENAME": "BatteryModel-08-2021.pkl",
                   "INV_DB_FILENAME": "BigInvCEC08-2021.pkl",
                   "NASA_22YR_FILENAME": "22yr_nasa_monthly.pkl",
                   "MANUAL_PV_DB_FILENAME": "ManualPVModules2020-11.pkl",
                   "USER_UPLOADS_FOLDER": "UserUploads/",
                   "FETCHED_WEATHER_FOLDER": "FetchedWeather/",
                   "AUXFILES_FOLDER": "AuxFiles/",
                   "RESULTS_FOLDER": "Results/",
                   "BOM_FOLDER": "BoM/",
                   "ELECPARAMS": "elec_design_params.json",
                   "FINPARAMS": "finparameter_inputs.json",
                   "SOLARCALCLEADS": "CalcLeads/",
                   "GOOGLE_CLOUD_PROJECT": "solex-mvp-2",
                   "SQLALCHEMY_DATABASE_URI": "mysql+pymysql://root:default123@/refdata?unix_socket=/cloudsql/solex-mvp-2:europe-west6:component-cost",
                   "DB_NAME": "ref_agrola"
                   }}
    for var in Envvar['Environ']:
        os.environ[var] = Envvar['Environ'][var]

# import pdb;
# pdb.set_trace()
# from addonModules import weatherGet as WG
from addonModules import ComponentDB as CDB
# from addonModules import SaveJsonOutputs as sjson
from addonModules import PVSYS_el_class as PVsel
from addonModules import PvSystemData_Calculation as PVcalc
from addonModules import Opti_Module as Opt
from addonModules import Finance_model as fm
# import addonModules.GeoCoordtoMetersCy as Geo2M # ensure cython extension
import addonModules.GeoCoordtoMeters as Geo2M
# import addonModules.GridMakerCy as GridM
import addonModules.GridMaker as GridM
from pvlib.location import Location

import Levenshtein
from addonModules import VizLinkMaker as VzLink

# import addonModules
import addonModules.ReportMaker as RepM
import addonModules.MakeSDparam as SD
from webcalc.webcalcMods import Supplier as Sup
from webcalc.webcalcMods import Storage as ST

from shapely.geometry import polygon, Polygon, MultiPolygon, LinearRing
from shapely.affinity import translate

import sentry_sdk
from sentry_sdk.integrations.flask import FlaskIntegration
from sentry_sdk.integrations.sqlalchemy import SqlalchemyIntegration

sentry_sdk.init(
    dsn="https://508d4d25eaa447c38445f1f0bd8ac634@o434693.ingest.sentry.io/5392060",
    integrations=[FlaskIntegration(), SqlalchemyIntegration()],
    traces_sample_rate=1.0
)

# from opencensus.common.transports.async_ import AsyncTransport
# from opencensus.ext.stackdriver import trace_exporter as stackdriver_exporter
# import opencensus.trace.tracer
# def initialize_tracer(project_id):
#     exporter = stackdriver_exporter.StackdriverExporter(
#         project_id=project_id, transport = AsyncTransport
#     )
#     tracer = opencensus.trace.tracer.Tracer(
#         exporter=exporter,
#         sampler=opencensus.trace.tracer.samplers.AlwaysOnSampler()
#     )

#     return tracer

BUCKET_NAME = os.environ['BUCKET_NAME']
PV_DB = os.environ['PV_DB_FILENAME']
PVS_DB = os.environ['PVS_DB_FILENAME']
INV_DB = os.environ['INV_DB_FILENAME']
BATT_DB = os.environ['BATT_DB_FILENAME']
ELECPARAMS = os.environ['ELECPARAMS']
FINPARAMS = os.environ['FINPARAMS']
ALLOWED_EXTENSIONS = {'txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif', 'csv'}
BOM = os.environ['BOM_FOLDER']
USER_UPLOADS = os.environ['USER_UPLOADS_FOLDER']
FETCHED_WEATHER = os.environ['FETCHED_WEATHER_FOLDER']
AUXFILES = os.environ['AUXFILES_FOLDER']
RESULTS = os.environ['RESULTS_FOLDER']
MANUALPV_DB = os.environ['MANUAL_PV_DB_FILENAME']
DBNAME = os.environ['DB_NAME']

# os.environ['DB_NAME'] = 'refdata'

PVDB = None

# from webcalc import webcalc
# from multiopt import multiopt

app = Flask(__name__)


# app.register_blueprint(webcalc.webcalc,url_prefix='/calc')
#
# app.register_blueprint(multiopt.multiopt,url_prefix='/multiop')


# @app.before_first_request
# def _load_db():
#     global PVDF, INVDF, ELPARM, FINPARM,PVSYSDF,BATTDF
# #    Electrical parameters
#     client = storage.Client()
#     bucket = client.get_bucket(BUCKET_NAME)
#     blob = bucket.get_blob(AUXFILES+ELECPARAMS)
#     e = blob.download_as_string()
#     ELPARM = json.loads(e)
#
# #   Financial parameters
#     blob = bucket.get_blob(AUXFILES+FINPARAMS)
#     f = blob.download_as_string()
#     FINPARM = json.loads(f)
#
#
# #    Full CEC Inverter Dataframe
#     blob = bucket.get_blob(INV_DB)
#     inv = blob.download_as_string()
#     INVDF = pickle.loads(inv)
#     INVDF=INVDF.loc[:,~INVDF.columns.duplicated()]
#
# #   Full CEC PV Dataframe
#     blob = bucket.get_blob(PV_DB)
#     p = blob.download_as_string()
#     PVDF = pickle.loads(p)
#     #PVDF.loc['Wp'] = PVDF.loc['I_mp_ref']*PVDF.loc['V_mp_ref']
#     #PVDF=PVDF.loc[:,~PVDF.columns.duplicated()]
#     # UPLOAD THE MANUAL DESOTO MODELS CREATED
#     # MANUALPVDF = pickle.loads(bucket.get_blob(MANUALPV_DB).download_as_string())
#     # PVDF=PVDF.join(MANUALPVDF,how='left')
#     PVDF = PVDF[~PVDF.index.duplicated()]
#     PVDF=PVDF.loc[:,~PVDF.columns.duplicated()]
#
#
#
#     blob = bucket.get_blob(PVS_DB)
#     pvs = blob.download_as_string()
#     PVSYSDF = pickle.loads(pvs)
#     #PVSYSDF.loc['Wp'] = PVSYSDF.loc['I_mp_ref']*PVSYSDF.loc['V_mp_ref']
#     PVSYSDF=PVSYSDF.loc[:,~PVSYSDF.columns.duplicated()]
#     ## Battery Load
#
#     BATTDF = pickle.loads(bucket.get_blob(BATT_DB).download_as_string())
#
#     #SQLALCHEMY_DATABASE_URI="mysql+pymysql://root:default123@/refdata?unix_socket=/tmp/cloudsql/solex-mvp-2:europe-west6:component-cost"
#
#     #tracer = initialize_tracer(os.environ['GOOGLE_CLOUD_PROJECT'])


@app.after_request
def add_corsheader(response):
    whitelistpatterns = [re.compile(".*solextron.*\.herokuapp\.com\/?.*"),
                         re.compile(".*solex-mvp-2.*\.appspot\.com\/?.*"),
                         re.compile(".*localhost.*"),
                         re.compile(".*solextron\.com\/?.*"),
                         re.compile(".*\.run\.*app\/?.*")]

    weborigin = request.environ.get('HTTP_ORIGIN')
    referer = request.environ.get('HTTP_REFERER')

    if len([weborigin for Wlist in whitelistpatterns if Wlist.match(str(weborigin))]) > 0:
        response.headers["Access-Control-Allow-Origin"] = weborigin
    elif len([referer for Wlist in whitelistpatterns if Wlist.match(str(referer))]) > 0:
        response.headers["Access-Control-Allow-Origin"] = referer
    else:
        response.headers["Access-Control-Allow-Origin"] = "http://localhost:4200"
    response.headers["Access-Control-Allow-Credentials"] = "true"
    response.headers["Access-Control-Allow-Headers"] = "authorization, content-type"
    response.headers["Access-Control-Allow-Methods"] = "GET,POST,OPTIONS"

    return response


@app.route('/user-projects', methods=['POST'])
@jwt_authenticated
def userprojects():
    # import pdb
    # pdb.set_trace()
    ds = datastore.Client()
    gcs = storage.Client()
    bucket = gcs.get_bucket(BUCKET_NAME)
    message = {}
    record = {}
    print("ririee", request)

    req_json = request.get_json()['userdata']

    user_details = req_json  # json.loads(req_json)
    # user_ip = '.'.join(user_ip.split('.')[:2])
    #    query = ds.query(kind='users')
    #    query.add_filter('email','=',user_details['email'])
    user_details['email'] = request.email
    orgname = request.org
    # Checklist of userinputs  - check for the required ones
    uschklist = ['crudstate', 'email']
    for us in uschklist:
        if us not in user_details:
            return jsonify({"error": "{} not set in input request".format(us)}), 400

    crudstate = user_details['crudstate']  # create, update, delete 

    # Creating new projects
    entity = ds.get(key=ds.key('users', user_details['email']))
    parent_key = ds.key('users', user_details['email'])
    if not entity:
        # entity = datastore.Entity(key=ds.key('users',user_details['email']))
        # entity.update(record)
        # ds.put(entity)
        message.update({"error": f"No user with username {user_details['email']} exists"})
        return jsonify(message), 400

    if (crudstate == "create"):
        ProjId = str(uuid.uuid1())
        entity = datastore.Entity(key=ds.key('users', user_details['email'], 'Project', ProjId))

        message.update(
            {"Success": "User {} has created a new project with project id: {}".format(user_details['email'], ProjId),
             "project_id": ProjId})
        record.update({'created': dt.datetime.utcnow(), 'status': 'Project Overview'})
        if 'projectdetails' in user_details:
            if 'projecttype' not in user_details['projectdetails']:
                user_details['projectdetails'].update({'projecttype': 'default'})
            if 'projectaddress' not in user_details['projectdetails']:
                user_details['projectdetails'].update({'projectaddress': ' '})
            if 'owner' not in user_details['projectdetails']:
                owner = user_details['email']
                user_details['projectdetails'].update({'owner': 'default'})
            record.update({"projectdetails": user_details['projectdetails']})

        else:
            projectdetails = {"projectname": "New Project", "projecttype": "default", "owner": user_details['email']}
            record.update({"projectdetails": projectdetails})
        if 'projectaddress' in record['projectdetails']:
            if 'Location' not in entity:
                entity['Location'] = {}
                entity['Location']['map address'] = record['projectdetails']['projectaddress']
            else:
                entity['Location']['map address'] = record['projectdetails']['projectaddress']

        entity.update(record)
        ds.put(entity)

        return jsonify(message), 200

    elif (crudstate == 'update project'):
        ProjId = user_details['project_id']
        entity = ds.get(key=ds.key('Project', ProjId, parent=parent_key))
        if not entity:
            message.update({"error": "Project id {} not found".format(ProjId)})
            return jsonify(message), 400
        if 'projectdetails' in user_details:
            record.update({"projectdetails": user_details['projectdetails']})

        if "status" in user_details:
            record.update({"status": user_details['status']})

        if 'projectdetails' in record:
            if 'projectaddress' in record['projectdetails']:
                if 'Location' not in entity:
                    entity['Location'] = {}
                    entity['Location']['map address'] = record['projectdetails']['projectaddress']
                else:
                    entity['Location']['map address'] = record['projectdetails']['projectaddress']

        entity.update(record)
        ds.put(entity)
        message.update({"Success": 'Updated project details of {} for user {}'.format(ProjId, user_details['email'])})
        return jsonify(message), 200

    elif (crudstate == 'update user'):
        entity = ds.get(key=ds.key('users', user_details['email']))
        if not entity:
            message.update({"error": "User email {} not found".format(user_details['email'])})
            return jsonify(message), 400
        record.update({"userdetails": user_details['userdetails']})
        entity.update(record)
        ds.put(entity)
        message.update({"Success": 'Updated details for user {}'.format(user_details['email'])})
        return jsonify(message), 200
    elif (crudstate == 'addtoorg'):

        if orgname:
            orgentity = ds.get(key=ds.key('organisation', orgname))
            if not orgentity:
                orgentity = datastore.Entity(key=ds.key('organisation', orgname))
                orgentity.update({"users": [user_details['email']]})
            else:
                users = orgentity.get('users')
                users.append(user_details['email'])
        else:
            return jsonify({"error": "organame not found in request"}), 400
        ds.put(entity)
        ds.put(orgentity)
        return jsonify({"success": "Added user {} to org {}".format(user_details['email'], orgname)}), 200
    elif (crudstate == 'removefromorg'):

        if orgname:
            orgentity = ds.get(key=ds.key('organisation', orgname))
            if not orgentity:
                return jsonify({"error": "orgname {} not found".format(orgname)}), 400
            else:
                users = orgentity.get('users')
                users.remove(user_details['email'])
        else:
            return jsonify({"error": "orgname not found in request"}), 400
        ds.put(entity)
        ds.put(orgentity)
        return jsonify({"success": "removed user {} from org {}".format(user_details['email'], orgname)}), 200
    elif (crudstate == 'upsertorg'):
        # qur = ds.query(kind='organisation')
        # qur.add_filter('users','=',user_details['email'])
        # Sol=list(qur.fetch())
        # if Sol != []:   
        #     orgname =  Sol[0].key.name 
        # else:           
        #     return jsonify({"error":"user {} doesnt belong to any org"}.format(user_details['email']))
        orgentity = ds.get(key=ds.key('organisation', orgname))
        orgtoupdate = user_details.get('orgsettings', {})
        for key, val in orgtoupdate.items():
            if isinstance(orgentity.get(key), list):
                EntVal = orgentity.get(key)
                E = EntVal
                [E.append(x) for x in val if val not in EntVal]
                orgentity[key] = list(set(E))

            elif isinstance(orgentity.get(key), dict):  # Just catches two levels of nesting not more
                EntDict = orgentity.get(key)
                EntDict.update(
                    {k: ({vk: v for vk, v in v1.items()} if isinstance(v1, dict) else v1) for k, v1 in val.items()})
                orgentity.update({key: EntDict})

            else:
                orgentity.update({key: val})
        ds.put(orgentity)
        return jsonify({"success": "updated org {} by user {}".format(orgname, user_details['email'])}), 200
    elif crudstate == 'displaypermissions':
        orgentity = ds.get(key=ds.key('organisation', orgname))
        output = {}
        if 'displaypermissions' in orgentity:
            if user_details['email'] in orgentity['admins']:
                displaypermissions = orgentity['displaypermissions'].get('admins', {})
            elif user_details['email'] in orgentity['users']:
                displaypermissions = orgentity['displaypermissions'].get('users', {})
            else:
                displaypermissions = {}
            output.update({"displaypermissions": displaypermissions})
        return jsonify(output), 200

    elif (crudstate == 'read user'):
        if 'userdetails' not in entity:
            return jsonify({"error": "No user details stored for this user {}".format(user_details['email'])})
        # qur = ds.query(kind='organisation')
        # qur.add_filter('users','=',user_details['email'])
        # Sol=list(qur.fetch())
        # if Sol != []:

        #     orgname =  Sol[0].key.name 
        # else:        

        #     orgname = 'default'
        orgentity = ds.get(key=ds.key('organisation', orgname))
        outdic = {"userdetails": entity['userdetails']}
        if orgentity:
            outdic.update({"organisation": orgentity, "orgname": orgname})
        output = json.loads(json.dumps(outdic))  # full entity read as json
        return jsonify(output), 200

    elif (crudstate == 'read project'):
        ProjId = user_details['project_id']
        entity = ds.get(key=ds.key('Project', ProjId, parent=parent_key))
        if not entity:
            message.update({"error": "Project id {} not found".format(ProjId)})
            return jsonify(message), 400

        return jsonify(entity), 200

    elif (crudstate == 'delete'):
        entity = ds.get(key=ds.key('Project', user_details['project_id'], parent=parent_key))
        if not entity:
            message.update({"error": "Project ID {} not found for user {} ".format(user_details['project_id'],
                                                                                   user_details['email'])})

        ds.delete(key=ds.key('users', user_details['email'], 'Project', user_details['project_id']))
        # datastore deleted 
        # delete storage values
        uniqueid = "Uid_" + entity.key.parent.name + "_Pid_" + entity.key.name
        usrblobs = bucket.list_blobs(prefix=USER_UPLOADS + uniqueid)  # Get list of files in User_Uploads
        resblobs = bucket.list_blobs(prefix=RESULTS + uniqueid)
        UIDids = "UID_" + entity.key.parent.name + "_PID_" + entity.key.name
        layoutblobs = bucket.list_blobs(prefix=USER_UPLOADS + UIDids)
        try:
            bucket.delete_blobs(resblobs)
            bucket.delete_blobs(usrblobs)
            bucket.delete_blobs(layoutblobs)
        except:
            message.update({"error": "couldnt delete bucket contents"})
        message.update(
            {"Success": "deleted project {} from user {} ".format(user_details['project_id'], user_details['email'])})
        # ds.put(entity)

        return jsonify(message), 200


#

@app.route('/project-details', methods=['GET', 'POST'])
@jwt_authenticated
def readproject():
    ds = datastore.Client()

    req_json = request.get_json()['readProject']
    userinput = req_json
    #### AUTENTICATED EMAIL
    userinput['email'] = request.email
    orgname = request.org
    parent_key = ds.key('users', userinput['email'])
    # entity=ds.get(key=ds.key('Project',userinput['project_id'],parent=parent_key))
    userentity = ds.get(key=ds.key('users', userinput['email']))
    OutputDic = {}
    uschklist = ['email', 'attribute']
    for us in uschklist:
        if us not in userinput:
            return jsonify({"error": "{} not set in input request".format(us)}), 400
    if not userentity:
        return jsonify({"error": "Email -- {} -- not found".format(userinput['email'])}), 400

    def filternonjson(obj):
        if isinstance(obj, dict):
            obj = {k: filternonjson(v) for k, v in obj.items()}
        elif isinstance(obj, list):
            obj = [filternonjson(it) for it in obj]
        elif isinstance(obj, float):
            if (np.isnan(obj) or (obj == np.inf) or (obj == -np.inf)):
                obj = 0
        return obj

    with ds.transaction(read_only=True):
        att = userinput['attribute']
        searchvalue = userinput.get('searchvalue', '')
        if 'numrecords' in userinput:
            numr = userinput['numrecords']
        else:
            numr = 20

        query = ds.query(kind='Project', ancestor=parent_key)
        query.order = ['-created']
        Q = query.fetch(limit=int(numr))

        # TODO: Use pagination and cursors to get to next set of results
        # Improve this list
        # att : 'Location', 'Weather', 'Components', 'Cost' , 'Financial', 'OptMatrix','SelectedConfig',
        #     : 'Demand"  

        if att == 'projectnums':
            # TODO: 

            projList = []
            attribs = [{'Location': ['locationName', 'map address']},
                       {'projectdetails': ['projectname', 'projecttype', 'owner']},
                       {'Demand': ['ACpower']}, 'status', 'created']

            for s in Q:
                entry = {"id": str(s.key.name)}
                for att in attribs:
                    if type(att) != dict:
                        if att in s:
                            entry.update({att: s[att]})
                    elif type(att) == dict:
                        if list(att.keys())[0] in s:
                            mainatt = str(list(att.keys())[0])  # Main attribute and subattributes

                            # _ = [entry.update({subatt : s[mainatt][subatt]}) for subatt in list(att.values())[0] if subatt in list(s[mainatt].keys())]
                            _ = [
                                entry.update({subatt: s[mainatt][subatt]}) if (s[mainatt].get(subatt)) else {subatt: ''}
                                for subatt in list(att.values())[0]]
                entry = filternonjson(entry)
                projList.append(entry)

                # OutputDic.update({"data":projList})    
            output = jsonify({"message": "Project List", "data": projList})
        elif att == 'QuoteID':
            projList = []
            attribs = [{'ActiveQuote': ['QuoteID']}, {'projectdetails': ['projectname', 'owner']},
                       {"FinalSummary": ["OverallSummary"]}, 'status', 'created']
            for s in Q:
                if s.get('ActiveQuote'):
                    quotestored = s['ActiveQuote'].get('QuoteID', '')
                    if quotestored == searchvalue:
                        entry = {"id": str(s.key.name)}
                        for att in attribs:
                            if type(att) != dict:
                                if att in s:
                                    entry.update({att: s[att]})
                            elif type(att) == dict:
                                if list(att.keys())[0] in s:
                                    mainatt = str(list(att.keys())[0])  # Main attribute and subattributes 
                                    _ = [entry.update({subatt: s[mainatt][subatt]}) for subatt in list(att.values())[0]
                                         if subatt in list(s[mainatt].keys())]
                        entry = filternonjson(entry)
                        projList.append(entry)
            # OutputDic.update({"data":projList}) 
            output = jsonify({"message": "Project List", "data": projList})
        else:
            entity = ds.get(key=ds.key('Project', userinput['project_id'], parent=parent_key))
            if not entity:
                return jsonify({"error": "No record for project_id {}".format(userinput['project_id'])}), 400
            if att in entity:
                output = (jsonify(entity[att]))
                # OutputDic.update(dict(entity[att]))
            else:
                return jsonify(
                    {"error": " No record for {} in project_id {}".format(att, userinput['project_id'])}), 400

    return output, 200


@app.route('/demand', methods=['POST'])
@jwt_authenticated
def demandread():
    ds = datastore.Client()
    gcs = storage.Client()
    bucket = gcs.get_bucket(BUCKET_NAME)

    req_json = request.get_json()['Demand']
    userinput = req_json  # json.loads(req_json)      
    userinput['email'] = request.email
    orgname = request.org
    parent_key = ds.key('users', userinput['email'])
    entity = ds.get(key=ds.key('Project', userinput['project_id'], parent=parent_key))
    # uniqueid='Uid_'+userinput['id']+'_Pid_'+userinput['project_id']
    #    if entity is None:
    #        entity=datastore.Entity(key=ds.key('users',userinput['email'],'Project',userinput['project_id']))
    #     
    message = {}
    if not entity:
        # entity = datastore.Entity(key=ds.key('users',user_details['email']))
        # entity.update(record)
        # ds.put(entity)
        message.update({"error": "No user with username {} exists".format(userinput['email'])})
        return jsonify(message), 400

    # Get Customer Input Consumption per year

    uniqueid = 'Uid_' + userinput['email'] + '_Pid_' + userinput['project_id']
    ACPowerFromYearlyDemand = 0
    YearlyDemand = 0
    DailyProfile2Display = {}
    Profile = {}
    ACPowerStored = 0
    ACPowerFromArea = [0, 0]
    if 'Location' in entity:
        TotalPanelArea = entity['Location'].get('MaxPanelArea', entity['Location'].get('TotalPanelArea', 0))
        # if 'TotalPanelArea' in entity['Location']:
        #     TotalPanelArea = entity['Location']['TotalPanelArea']
        Maxm2perkW = 5.5  # ELPARM['systemparams']['Maxm2perkW']*1.3 
        Minm2perkW = 4.5  # ELPARM['systemparams']['Minm2perkW']*1.3
        ACPowerFromArea = [TotalPanelArea / Maxm2perkW, TotalPanelArea / Minm2perkW]
    else:
        ACPowerFromArea = [0, 0]

    if entity['status'] != 'Lock':
        entity.update({"status": "Demand"})
    if 'readstored' in userinput:
        if 'Location' in entity:
            TotalPanelArea = entity['Location'].get('MaxPanelArea', entity['Location'].get('TotalPanelArea', 0))
            # if 'TotalPanelArea' in entity['Location']:
            #     TotalPanelArea = entity['Location']['TotalPanelArea']
            Maxm2perkW = 5.5  # ELPARM['systemparams']['Maxm2perkW']
            Minm2perkW = 4.5  # ELPARM['systemparams']['Minm2perkW']
            ACPowerFromArea = [TotalPanelArea / Maxm2perkW, TotalPanelArea / Minm2perkW]
        else:
            ACPowerFromArea = [0, 0]
        if 'Demand' in entity:
            if 'ACpower' in entity['Demand']:
                ACPowerStored = entity['Demand']['ACpower']
            else:
                ACPowerStored = 0
            if 'YearlyDemand' in entity['Demand']:
                YearlyDemand = entity['Demand']['YearlyDemand']

            elif 'Customer Inputs' in entity:
                if 'YearlyElecRequired' in entity['Customer Inputs']:
                    YearlyDemand = entity['Customer Inputs']['YearlyElecRequired']['TotalElec']
                else:
                    YearlyDemand = 0
            else:
                YearlyDemand = 0
            if 'Profile' in entity['Demand']:
                Profile = entity['Demand']['Profile']
                country = Profile['country']
                profiletype = Profile['profiletype']

                if profiletype == 'UserDefined':
                    DemandTS = pickle.loads(bucket.get_blob(entity['Demand']['DemandTS']).download_as_string())
                    DailyProfile2Display = {}
                    monthnames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
                    YearlyDemand = DemandTS.sum() / 1000
                    for month, i in zip(monthnames, range(1, 13)):
                        DailyProfile2Display[month] = {
                            str(j): DemandTS[(DemandTS.index.month == i) & (DemandTS.index.hour == j)].mean() / 1000 for
                            j in range(0, 24)}
                else:
                    # Fetch the dataset for daily profile

                    blobname = AUXFILES + 'PriceAPICountry.json'
                    CI = json.loads(bucket.blob(blobname).download_as_string())
                    if country in CI['Data']:
                        blobnamePic = AUXFILES + CI['Data'][country]['PriceDb']
                    else:
                        blobnamePic = AUXFILES + CI['Data']['CHE']['PriceDb']
                    Data = pickle.loads(bucket.get_blob(blobnamePic).download_as_string())
                    if profiletype in Data['ConsumptionProfile']:
                        DemDailyDf_E = Data['ConsumptionProfile'][profiletype]
                    else:
                        firstkey = next(iter(Data['ConsumptionProfile']))
                        DemDailyDf_E = Data['ConsumptionProfile'][firstkey]
                    DemDailyDf_E = pd.DataFrame(DemDailyDf_E)

                    DailyProfile2Display = DemDailyDf_E[DemDailyDf_E.columns[0:]].to_dict()
            else:
                DailyProfile2Display = {}
                Profile = {}

        ACPowerFromYearlyDemand = np.ceil(YearlyDemand / 1200)  # Assume a kWh/kWp of 1200

        return jsonify({"ACPowerFromArea": ACPowerFromArea, "ACPowerStore": ACPowerStored,
                        "YearlyDemand": YearlyDemand, "ACPowerFromConsumption": ACPowerFromYearlyDemand,
                        "DailyProfile2Display": DailyProfile2Display, "Profile": Profile}), 200

    if 'ACPower' in userinput:
        demand = {"Demand": {'ACpower': float(userinput['ACPower'])}}
        if 'Demand' in entity:
            entity['Demand']['ACpower'] = float(userinput['ACPower'])
        else:
            entity.update(demand)

    if 'ACpower' in userinput:
        demand = {"Demand": {'ACpower': float(userinput['ACpower'])}}
        if 'Demand' in entity:
            entity['Demand']['ACpower'] = float(userinput['ACpower'])
        else:
            entity.update(demand)
        # ds.put(entity)
        # return jsonify({'success':'ACPower {} updated'.format(userinput['ACpower'])}),200 
    # try:    
    #    demand={"Demand":{'ACpower':float(userinput['ACpower'])}}#,
    #    #     'dailyvalues':userinput['dailyvalues']}
    #    if 'Demand' in entity:
    #        entity['Demand']['ACpower'] = float(userinput['ACpower'])
    #    else:                                           
    #        entity.update(demand)
    #    ds.put(entity)
    # except:
    #     return jsonify({"error": "No value for ACpower given"}),400

    # Upload Detailed DemandTS here
    ####

    ##### Daily Profile
    country = 'Default'
    profiletype = 'H4'
    if 'Profile' in userinput:
        if type(userinput['Profile']) == dict:
            country = userinput['Profile']['country']
            profiletype = userinput['Profile']['profiletype']
        else:
            country = 'CHE'
            profiletype = 'H4'
    else:
        country = 'Default'
        profiletype = 'H4'
    ###########
    Profile = {"country": country, "profiletype": profiletype}
    ### Yearly Demand
    if 'Customer Inputs' in entity:
        YearlyDemand = entity['Customer Inputs']['YearlyElecRequired']['TotalElec']

    ##
    if Profile['profiletype'] != 'UserDefined':  # if user defined then demand is already known   
        blobname = AUXFILES + 'PriceAPICountry.json'
        CI = json.loads(bucket.blob(blobname).download_as_string())
        if country in CI['Data']:
            blobnamePic = AUXFILES + CI['Data'][country]['PriceDb']
        else:
            blobnamePic = AUXFILES + CI['Data']['CHE']['PriceDb']
        Data = pickle.loads(bucket.get_blob(blobnamePic).download_as_string())

        if 'YearlyDemand' in userinput:
            YearlyDemand = userinput['YearlyDemand']
        else:
            YearlyDemand = 11000

            # Every consumption profile will be a different type of df
        if (country == 'CHE') or (country == 'Default'):

            if profiletype in Data['ConsumptionProfile']:
                DemDailyDf_E = Data['ConsumptionProfile'][profiletype]
                DemDailyDf_H = Data['ConsumptionProfile']['HELECTRO']
            else:
                DemDailyDf_E = Data['ConsumptionProfile']['H4']
                DemDailyDf_H = Data['ConsumptionProfile']['HELECTRO']
            DemDailyDf_E = pd.DataFrame(DemDailyDf_E)
            DemDailyDf_H = pd.DataFrame(DemDailyDf_H)

            if 'YearlyDemand' in userinput:
                YearlyDemand = userinput['YearlyDemand']
            elif 'monthlydemand' in userinput:
                monthlydemand = userinput['monthlydemand']
                YearlyDemand = sum(monthlydemand)
                monthlydemand = [(YearlyDemand / 12)] * 12
            elif profiletype in Data['Electrical']['Kategorie'].values:
                YearlyDemand = Data['Electrical'].loc[Data['Electrical']['Kategorie'] == profiletype, 'YearkWh'].values[
                    0].astype(float)
            else:
                YearlyDemand = 11000

            s = time.time()
            Demand = Sup.CHDemandGetter(DemDailyDf_E, DemDailyDf_H, YearlyDemand, 0)  # TotalHeat=0
            print(time.time() - s)
            DemandTS = Demand['Total'].astype('float')
            DemandTS.name = 'Demand'

            DailyProfile2Display = DemDailyDf_E[DemDailyDf_E.columns[0:]].to_dict()

        else:
            if profiletype in Data['ConsumptionProfile']:
                DailyProfile = pd.DataFrame(Data['ConsumptionProfile'][profiletype])

            else:
                firstkey = next(iter(Data['ConsumptionProfile']))
                DailyProfile = pd.DataFrame(Data['ConsumptionProfile'][firstkey])

            if 'monthlydemand' in userinput:
                monthlydemand = userinput['monthlydemand']
                YearlyDemand = sum(monthlydemand)
            elif 'YearlyDemand' in userinput:
                YearlyDemand = userinput['YearlyDemand']
                monthlydemand = [(YearlyDemand / 12)] * 12
            else:
                monthlydemand = [(YearlyDemand / 12)] * 12

            Demand = Sup.DemandfromMonthly(monthlydemand, DailyProfile)
            DemandTS = Demand['Demand'].astype('float')
            # DailyProfile2Display = DailyProfile[DailyProfile.columns[1:]].to_dict()
            DailyProfile2Display = DailyProfile[DailyProfile.columns[0:]].to_dict()
        Demandblobname = USER_UPLOADS + uniqueid + '_demandTS.pkl'
        bucket.blob(Demandblobname).upload_from_string(pickle.dumps(DemandTS))
    else:
        Demandblobname = entity['Demand']['DemandTS']
        DemandTS = pickle.loads(bucket.get_blob(entity['Demand']['DemandTS']).download_as_string())
        DailyProfile2Display = {}
        monthnames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
        YearlyDemand = DemandTS.sum() / 1000
        for month, i in zip(monthnames, range(1, 13)):
            DailyProfile2Display[month] = {
                str(j): DemandTS[(DemandTS.index.month == i) & (DemandTS.index.hour == j)].mean() / 1000 for j in
                range(0, 24)}

    if 'fileupload' in userinput:
        fname = userinput['fileupload']

        try:
            bucket.get_blob(USER_UPLOADS + fname).download_to_filename(fname)
            if fname.endswith('.xlsx'):
                demandfromfileraw = pd.read_excel(fname, engine='openpyxl')
            elif fname.endswith('.csv'):
                demandfromfileraw = pd.read_csv(fname)
            else:
                demandfromfileraw = pd.read_csv(fname)

            if not (('kW' in demandfromfileraw.columns) or ('kWh' in demandfromfileraw.columns)):
                return jsonify({"error": "column containing load data should have label kW or kWh"}), 400
            # if 'kW' in demandfromfile.columns:  # convert to kWh 

            #     demandfromfile = (8760/len(demandfromfile))*demandfromfile
            #     demandfromfile.rename(columns={'kW':'kWh'},inplace=True)

            if 'time' in demandfromfileraw.columns:
                yearindex = pd.to_datetime(demandfromfileraw['time'], errors='coerce')
                kWconvert = ((yearindex[1] - yearindex[0]).seconds) / 3600  # fraction of an hour
                demandfromfileraw.set_index('time', inplace=True)

                if 'kW' in demandfromfileraw.columns:
                    demandfromfile = pd.DataFrame(demandfromfileraw.values * kWconvert, index=yearindex,
                                                  columns=['kWh'])
                else:
                    demandfromfile = pd.DataFrame(demandfromfileraw.values, index=yearindex,
                                                  columns=['kWh'])
                # Replicate if not enough data
                deltaTimeStep = ((demandfromfile.index[1] - demandfromfile.index[0]).seconds) / 3600  # Number of Hours
                timeres = int(((demandfromfile.index[1] - demandfromfile.index[0]).seconds) / 60)  # Number of minutes
                yearperiods = int(8760 / deltaTimeStep)
                Arr = np.array(demandfromfileraw.values)
                TOTARR = np.concatenate(
                    (np.tile(Arr, [int(yearperiods / len(Arr)), 1]), Arr[0:np.mod(yearperiods, len(Arr))]), axis=0)
                yearindex = pd.date_range(start='1/1/{}'.format(str(dt.datetime.now().year)), periods=yearperiods,
                                          freq=str(timeres) + 'T')
                demandfromfile = pd.DataFrame(TOTARR, index=yearindex, columns=['kWh'])

            else:  # use only the length of the file as a filter with 15 min steps
                if 'timeres' in userinput:
                    # 15, 60
                    timeres = int(userinput['timeres'])
                else:
                    timeres = 15
                Arr = np.array(pd.to_numeric(demandfromfileraw['kW'],
                                             errors='coerce').values) if 'kW' in demandfromfileraw else np.array(
                    pd.to_numeric(demandfromfileraw['kWh'], errors='coerce').values)
                # Arr = np.array(demandfromfileraw.values)
                if timeres == 60:
                    yearperiods = 8760
                else:
                    yearperiods = 8760 * 4

                # TOTARR = np.concatenate(np.ravel(np.tile(Arr,[int(yearperiods/len(Arr)),1])),np.ravel(Arr[0:np.mod(yearperiods,len(Arr))]),axis=0)
                TOTARR = np.hstack((np.ravel(np.tile(Arr, [int(yearperiods / len(Arr)), 1])),
                                    np.ravel(Arr[0:np.mod(yearperiods, len(Arr))])))
                yearindex = pd.date_range(start='1/1/{}'.format(str(dt.datetime.now().year)), periods=yearperiods,
                                          freq=str(timeres) + 'T')
                if 'kW' in demandfromfileraw.columns:
                    TOTARR = TOTARR * (8760 / yearperiods)
                demandfromfile = pd.DataFrame(TOTARR, index=yearindex, columns=['kWh'])

            os.remove(fname)
        except:
            return jsonify({"error": "file {} has invalid format or does not exist".format(USER_UPLOADS + fname)}), 400

        Profile = {"country": entity['projectdetails']['customerinfo']['country'], "profiletype": "UserDefined"}
        Demand, DailyProfile2Display = Sup.DemandFromFile(demandfromfile['kWh'])

        DemandTS = Demand['Demand'].astype('float')
        YearlyDemand = DemandTS.sum() / 1000
        Demandblobname = USER_UPLOADS + uniqueid + '_demandTS.pkl'
        bucket.blob(Demandblobname).upload_from_string(pickle.dumps(DemandTS))
        # DailyProfile2Display = {}
        # TODO : Return a profile

    # Generate  

    #######

    if 'Demand' in entity:
        entity['Demand']['DemandTS'] = Demandblobname
        entity['Demand']['YearlyDemand'] = YearlyDemand
    else:
        entity.update({"Demand": {"DemandTS": Demandblobname, "YearlyDemand": YearlyDemand}})
    if 'Profile' in entity['Demand']:
        entity['Demand']['Profile'].update(Profile)
    else:
        entity['Demand'].update({"Profile": Profile})
    if 'maxcurrent' in userinput:
        if 'maxcurrent' in entity['Demand']:
            entity['Demand']['maxcurrent'] = userinput['maxcurrent']
        else:
            entity['Demand'].update({"maxcurrent": userinput["maxcurrent"]})
    ds.put(entity)
    # Prepare Output

    MonthlyOutput = list(DemandTS.groupby([pd.Grouper(freq='1M')]).sum().values / 1000)  # Make List in kWh
    MonthlyPeak = list(DemandTS.groupby([pd.Grouper(freq='1M')]).max().values / 1000)  # Max List in kWh
    YearlyDemand = np.ceil(DemandTS.sum() / 1000)  # in kWh

    # Just to have adequate energy

    # RecommendedACPower = np.ceil(YearlyDemand/(8760*15/100)) # Assuming a 15% Capacity Factor
    RecommendedACPower = np.ceil(YearlyDemand / 1200)

    OutputDic = {"MonthlyOutput": MonthlyOutput, "MonthlyPeak": MonthlyPeak, "YearlyDemand": YearlyDemand,
                 "RecommendedACPower": RecommendedACPower, "DailyProfile2Display": DailyProfile2Display,
                 "Profile": Profile,
                 "ACPowerFromConsumption": RecommendedACPower}

    # OutputDic = {"MonthlyPeak":MonthlyPeak,"MonthlyOutput":MonthlyOutput,"YearlyDemand":YearlyDemand
    #               } 

    return jsonify(OutputDic), 200


@app.route('/locationlayout', methods=['POST'])
@jwt_authenticated
def location():
    gcs = storage.Client()
    ds = datastore.Client()

    req_json = request.get_json()['Location']
    userinput = req_json  # json.loads(req_json)
    userinput['email'] = request.email
    orgname = request.org
    parent_key = ds.key('users', userinput['email'])

    errmsg = ""
    entity = ds.get(key=ds.key('Project', userinput['project_id'], parent=parent_key))
    uniqueid = 'Uid_' + userinput['email'] + '_Pid_' + userinput['project_id']
    message = {}
    if not entity:
        # entity = datastore.Entity(key=ds.key('users',user_details['email']))
        # entity.update(record)
        # ds.put(entity)
        message.update({"error": "No user with username {}  and projectname {} exists".format(userinput['email'],
                                                                                              userinput['project_id'])})
        return jsonify(message), 400

    bucket = gcs.get_bucket(BUCKET_NAME)
    # TODO : Ideal Azimuth from Shading horizon

    if 'readstored' in userinput.keys():
        OutputDic = {}
        if 'Location' in entity:
            try:
                blobname = entity['Location'].get('GeoCoords')
                LatPolyfromfile = json.loads(bucket.get_blob(blobname).download_as_string())
                OutputDic.update({"GeoCoords": LatPolyfromfile})
                if entity['Location'].get('PanelCoords'):
                    PanelCoords = json.loads(bucket.get_blob(entity['Location']['PanelCoords']).download_as_string())
                    OutputDic.update({"PanelCoords": PanelCoords})
                for key, val in entity['Location'].items():
                    if key not in ['PanelCoords', 'GeoCoords']:
                        OutputDic.update({key: val})
                return jsonify(json.loads(json.dumps(OutputDic))), 200

            except:
                jsonify({"error": "Geocoord values not entered"}), 400
        else:
            return jsonify({"error": "Location values not entered"}), 400

    if ('landcoordGeofile' in userinput.keys()):
        AreaCoords = userinput['landcoordGeofile']
        GeoCoordFile = USER_UPLOADS + AreaCoords

        blobname = USER_UPLOADS + AreaCoords
        bucket = gcs.get_bucket(BUCKET_NAME)
        blob = bucket.get_blob(blobname)
        s = blob.download_as_string()

        LatPolyfromfile = json.loads(s)


    else:
        # try:
        if userinput.get('GeoCoords', {}) != {}:
            AreaCoords = userinput['GeoCoords']
            LatPolyfromfile = AreaCoords
            blobname = USER_UPLOADS + uniqueid + '_layoutarea.json'
            blob = bucket.blob(blobname)
            blob.upload_from_string(json.dumps(AreaCoords), content_type='application/json')
            GeoCoordFile = blobname

        else:
            # except:
            blobname = USER_UPLOADS + uniqueid + '_layoutarea.json'
            # blob=bucket.blob(blobname)
            # blob.upload_from_string(json.dumps(AreaCoords),content_type='application/json')
            GeoCoordFile = blobname
            if bucket.get_blob(blobname):
                LatPolyfromfile = json.loads(bucket.get_blob(blobname).download_as_string())
            else:
                return jsonify({"error": "coordinates not provided"}), 400
        # store the area coords as json in file
        # blobname=USER_UPLOADS+uniqueid+'_layoutarea.json'  
        # blob=bucket.blob(blobname)
        # blob.upload_from_string(json.dumps(AreaCoords),content_type='application/json')
        # GeoCoordFile = blobname

    # TODO: Ensure - uniqueness of uploaded file name    

    Areas = dict()
    DCfromArea = dict()
    SurfArea = dict()

    ModDefData = dict()

    GeoPanOut = dict()
    RealPanOut = dict()
    AreaFigures = {"Areawise": {}}
    #    if type(AreaCoords)==str:
    #    
    #        blobname=USER_UPLOADS+AreaCoords   
    #        bucket=gcs.get_bucket(BUCKET_NAME)
    #        blob=bucket.get_blob(blobname)
    #        s = blob.download_as_string()
    #         
    #        LatPolyfromfile = json.loads(s)
    #    
    #         
    #    elif type(AreaCoords)==dict:
    #        LatPolyfromfile = AreaCoords
    #        
    #    else:
    #         MaxPanelArea = -1
    #         LatPolyfromfile = {}

    elecDesign = ELPARM

    defaultSpacingParams = elecDesign['systemparams']['SpacingParams']
    # if ('SpacingParams' not in userinput.keys()):    
    #     SpacingParams = elecDesign['systemparams']['SpacingParams']

    # else:
    #     SpacingParams = userinput['SpacingParams']
    #     for vals in elecDesign['systemparams']['SpacingParams']:
    #          if vals not in SpacingParams.keys():
    #             SpacingParams[vals] = elecDesign['systemparams']['SpacingParams'][vals]

    defaultAreaSpacingParams = {AreaLabel: defaultSpacingParams for AreaLabel in LatPolyfromfile}
    if 'AreaSpacingParams' in userinput:
        AreaSpacingParams = userinput['AreaSpacingParams']
        for AreaLabel in AreaSpacingParams:
            SpacingParams = AreaSpacingParams[AreaLabel]
            for vals in elecDesign['systemparams']['SpacingParams']:
                if vals not in SpacingParams.keys():
                    SpacingParams[vals] = elecDesign['systemparams']['SpacingParams'][vals]
            AreaSpacingParams[AreaLabel] = SpacingParams
    elif 'Location' in entity:
        AreaSpacingParams = entity['Location'].get('AreaSpacingParams', defaultAreaSpacingParams)
    else:
        AreaSpacingParams = {AreaLabel: defaultSpacingParams for AreaLabel in LatPolyfromfile}

    ModDefData['LongSide'] = defaultSpacingParams['H_default']
    ModDefData['ShortSide'] = defaultSpacingParams['W_default']
    PVModuleData = PVSYSDF['JA_Solar_JAM54S30_405_MR']  # PVSYSDF['Longi_LR4_60_HIH_380_M_G2']

    Maxm2perkW = elecDesign['systemparams']['Maxm2perkW']
    Minm2perkW = elecDesign['systemparams']['Minm2perkW']
    # Read in Spacing Params from REST API

    for AreaLabel in AreaSpacingParams:
        # Sanitise inputs here only
        AreaSpacingParams[AreaLabel]['Ti_P'] = abs(AreaSpacingParams[AreaLabel]['Ti_P'])
        # if LatPolyfromfile[AreaLabel]['Ext'][0][0]>0:  # Northern Hemisphere 
        #     if AreaSpacingParams[AreaLabel]['layout']=='flat':
        #         AreaSpacingParams[AreaLabel]['Ti_P'] = abs(AreaSpacingParams[AreaLabel]['Ti_P']) # Should be negative to face south
        #     else:
        #         AreaSpacingParams[AreaLabel]['Ti_P']  = abs(AreaSpacingParams[AreaLabel]['Ti_P'])  # Should be positive for east-west  
        # else: # Southern Hemisphere
        #     if AreaSpacingParams[AreaLabel]['layout']=='flat':
        #         AreaSpacingParams[AreaLabel]['Ti_P'] = abs(AreaSpacingParams[AreaLabel]['Ti_P'])
        #     else:
        #         AreaSpacingParams[AreaLabel]['Ti_P'] = abs(AreaSpacingParams[AreaLabel]['Ti_P'])   

    TotalPanelArea = 0

    NumPanelsArea, AreaFigures, Areas = GridM.MakeGeoPanLayout(LatPolyfromfile, AreaSpacingParams, PVModuleData)

    for AreaLabel in NumPanelsArea:
        SurfArea[AreaLabel] = AreaFigures['Areawise'][AreaLabel]['area']

    # #TODO: Save Install Plane Points - or perform second rotation
    Maxm2perkW = ELPARM['systemparams']['Maxm2perkW']
    Minm2perkW = ELPARM['systemparams']['Minm2perkW']

    MaxNumPanels = np.sum([d for d in NumPanelsArea.values()])
    TotalDCfromArea = [(MaxNumPanels * PVModuleData.Wp / 1.11) / 1000, (MaxNumPanels * PVModuleData.Wp) / 1000]  # in kW
    TotalPanelArea = MaxNumPanels * PVModuleData.A_c

    # TotalDCfromArea = [TotalPanelArea/Maxm2perkW,TotalPanelArea/Minm2perkW]
    RecommendedAC = TotalDCfromArea  # [TotalPanelArea/Maxm2perkW,TotalPanelArea/Minm2perkW]
    # except:
    #     return jsonify({"error": "Error in geometry - not closed shapes, non in plane  or unusual distortions"}), 400
    AreaFigures.update({"TotalDCInOut": 5})

    location_entity = {'Location': {'latitude': userinput['latitude'], 'longitude': userinput['longitude'],
                                    'locationName': userinput['locationName'],
                                    'map address': userinput['map address'],
                                    'GeoCoords': GeoCoordFile, 'AreaSpacingParams': AreaSpacingParams,
                                    'tilt': dict([(AreaLabel, Areas[AreaLabel].tilt) for AreaLabel in Areas]),
                                    'azimuth': dict([(AreaLabel, Areas[AreaLabel].azimuth) for AreaLabel in Areas]),
                                    'SurfArea': dict([(AreaLabel, SurfArea[AreaLabel]) for AreaLabel in Areas]),
                                    'TotalPanelArea': TotalPanelArea, 'MaxPanelArea': TotalPanelArea,
                                    "AreaFigures": AreaFigures
                                    }}

    if 'CountryCode' in userinput:
        location_entity['Location'].update({"countrycode": userinput['CountryCode']})
        errmsg += " Country Code {} set ".format(userinput['CountryCode'])
    if 'Postal' in userinput:
        if userinput['Postal']:
            location_entity['Location'].update({"PLZ": userinput['Postal']})
            errmsg += " PLZ {} set".format(userinput['Postal'])
        else:
            try:
                ZIP = entity['projectdetails']['customerinfo']['zip']
                location_entity['Location'].update({"PLZ": ZIP})
                errmsg += " PLZ {} set".format(ZIP)
            except:
                location_entity['Location'].update({"PLZ": userinput['Postal']})
                errmsg += " PLZ {} set".format(userinput['Postal'])
    entity.update(location_entity)
    entity.update({"status": "Location"})
    ds.put(entity)

    outputdic = {"status": errmsg, "TotalDCfromArea": TotalDCfromArea, "RecommendedAC": RecommendedAC,
                 "GeoPanCoords": GeoPanOut, "RealPanCoords": RealPanOut}

    return jsonify(outputdic), 200


@app.route('/get-weather-service', methods=['POST', 'GET'])
@jwt_authenticated
def weatherservice():
    # TODO: Split this into 2 parts
    #   1.  Upload location and layout details worth recommended AC power 
    #   2.  Input weather service selection and get plots 
    #     
    gcs = storage.Client()
    ds = datastore.Client()
    # stor=storage.Client()
    bucket = gcs.get_bucket(BUCKET_NAME)

    if request.method == 'GET':
        project_id = request.args.get('project_id')
        uniqueid = 'Uid_' + request.email + '_Pid_' + project_id
        blobname = FETCHED_WEATHER + uniqueid + '_weather.pkl'
        if bucket.get_blob(blobname):

            SolData = pickle.loads(bucket.get_blob(blobname).download_as_string())
            SolData['Time'] = SolData.index.astype(str)

            parent_key = ds.key('users', request.email)
            entity = ds.get(key=ds.key('Project', project_id, parent=parent_key))
            lat = entity['Location']['latitude']
            lon = entity['Location']['longitude']
            if 'Weather' in entity:
                TZ = entity['Weather'].get('TZ', 0)
            else:
                TZ = 0

            loc = Location(lat, lon, tz=TZ)
            solpos = loc.get_solarposition(SolData['Time'])
            SolDataWSolPos = pd.concat([SolData, solpos], axis=1)
            return jsonify(SolDataWSolPos[['Time', 'DNI', 'GHI', 'DHI', 'Temperature', 'Wspd', 'zenith', 'elevation',
                                           'azimuth']].to_dict(orient='list')), 200
        else:
            return jsonify({"error": "No weather data found"}), 400
    req_json = request.get_json()['Weather']
    userinput = req_json  # json.loads(req_json)
    userinput['email'] = request.email
    orgname = request.org
    parent_key = ds.key('users', userinput['email'])

    entity = ds.get(key=ds.key('Project', userinput['project_id'], parent=parent_key))
    uniqueid = 'Uid_' + userinput['email'] + '_Pid_' + userinput['project_id']
    message = {}
    if not entity:
        message.update({"error": "No user with username {}  and projectname {} exists".format(userinput['email'],
                                                                                              userinput['project_id'])})
        return jsonify(message), 400

    # entityNone=ds.get(key=ds.key('users','None','Project',userinput['project_id']))

    if 'weathersource' in userinput:
        weathersource = userinput['weathersource']  # uniqueid+userinput['weathersource']
    else:
        weathersource = 'best'

    entchecklist = ['Location']
    for ec in entchecklist:
        if ec not in entity:
            return jsonify({"error": "{} not set in datastore".format(ec)}), 400

    lat = entity['Location']['latitude']
    lon = entity['Location']['longitude']
    tilt = entity['Location']['tilt']
    azimuth = entity['Location']['azimuth']
    errmsg = ""

    weatherfile = 'None'
    try:
        locationName = entity['Location']['locationName']
    except:
        locationName = 'default'
    #    if type(userinput['locationName'])==str:
    #        locationName = userinput['locationName']  
    #    else:
    #        locationName = 'None'

    # TODO: Read in downloaded weatherfile

    if (weathersource.endswith('.tm2') or (weathersource.endswith('.csv')) or (weathersource.endswith('.CSV'))):
        weatherfile = userinput['weathersource']
    elif (weathersource == 'none'):
        weatherfile = 'None'

    if (weathersource == 'best' or weathersource == 'Best'):
        if (lat > 8) & (lat < 35) & (lon > 68) & (lon < 96):  # India, South Asia 
            weathersource = 'NSRDB'
        elif (lat > -20) & (lat < 60) & (lon > -170) & (lon < -20):
            weathersource = 'NSRDB'  # Western Hemisphere    
        elif (lat > -60) & (lat < 60) & (lon > -63) & (lon < 63):
            # weathersource = 'CAMS' #Europe, Atlantic, ME, Africa
            weathersource = 'PVGIS'
        else:
            weathersource = 'PVGIS'
    #            weathersource = 'NASA'

    downloadNewWeatherFile = False

    if 'Weather' in entity:
        if entity['Weather'].get('SolmetaData'):
            SolmetaData = entity['Weather'].get('SolmetaData')
            if (lat != entity['Weather']['SolmetaData'].get('latitude', 0)) or (
                    lon != entity['Weather']['SolmetaData'].get('longitude', 0)):
                downloadNewWeatherFile = True
            if weathersource != entity['Weather'].get('weathersource', ''):
                downloadNewWeatherFile = True
        else:
            downloadNewWeatherFile = True
            SolmetaData = {}
    else:
        downloadNewWeatherFile = True
        SolmetaData = {}

    blobname = FETCHED_WEATHER + uniqueid + '_weather.pkl'
    if not bucket.get_blob(blobname):
        downloadNewWeatherFile = True

    if downloadNewWeatherFile:
        SolData, SolmetaData, errmsg = WG.retrieve_weather(weathersource, lat,
                                                           lon, locationName, weatherfile)

        blob = bucket.blob(blobname)
        blob.upload_from_string(pickle.dumps(SolData))
        finalweatherfile = FETCHED_WEATHER + uniqueid + '_weather.pkl'
    else:
        SolData = pickle.loads(bucket.get_blob(blobname).download_as_string())
        finalweatherfile = blobname
        errmsg = "Using stored file"

    try:
        location_data = Location(SolmetaData['latitude'], SolmetaData['longitude'], tz=SolmetaData['TZ'])
    except:
        return jsonify({"data for error": [SolmetaData],
                        "errmsg": errmsg}), 400

    # Get the bucket that the file will be uploaded to.

    # Create a new blob and upload the file's content          

    # Get Optimum Tilt
    opttilt = {}
    # for areas in tilt:
    #     opttilt[areas] = PVcalc.OptimumTilt(SolData,lat,lon,panelazimuth=azimuth[areas])

    #    tilt_temp = tilt
    #    
    #    try:
    #        tiltvalue = float(tilt_temp)
    #    except Valueerror:    
    #        tiltvalue = tilt_temp
    #    
    #    if (type(tiltvalue)==str):
    #            tilt = PVcalc.OptimumTilt(SolData,lat,lon,panelazimuth=azimuth,period=str(tiltvalue))
    #          
    #    elif (~np.isnan(tiltvalue)):
    #           tilt = tiltvalue

    # TODO : Ideal Azimuth from Shading horizon
    """
    if ('GeoCoords' in userinput.keys()):    
        AreaCoords = userinput['GeoCoords']

        #store the area coords as json in file
        blobname=USER_UPLOADS+uniqueid+'_layoutarea.json'  
        blob=bucket.blob(blobname)
        blob.upload_from_string(json.dumps(AreaCoords),content_type='application/json')
        GeoCoordFile = blobname

    else:      
        AreaCoords = userinput['landcoordGeofile']
        GeoCoordFile = USER_UPLOADS+AreaCoords
    #TODO: Ensure - uniqueness of uploaded file name    

    Areas = dict()
    DCfromArea = dict()

    ModDefData = dict()
    """
    Weather2Display = pd.DataFrame()
    """
    if type(AreaCoords)==str:

        blobname=USER_UPLOADS+AreaCoords   
        bucket=gcs.get_bucket(BUCKET_NAME)
        blob=bucket.get_blob(blobname)
        s = blob.download_as_string()

        LatPolyfromfile = json.loads(s)


    elif type(AreaCoords)==dict:
        LatPolyfromfile = AreaCoords

    else:
         MaxPanelArea = -1
         LatPolyfromfile = {}


    elecDesign = ELPARM

    if ('SpacingParams' not in userinput.keys()):    
        SpacingParams = elecDesign['systemparams']['SpacingParams']
    else:
        SpacingParams = userinput['SpacingParams']
        for vals in elecDesign['systemparams']['SpacingParams']:
             if vals not in SpacingParams.keys():
                SpacingParams[vals] = elecDesign['systemparams']['SpacingParams'][vals]



    ModDefData['LongSide'] = SpacingParams['H_default']
    ModDefData['ShortSide'] = SpacingParams['W_default']


    Maxm2perkW = elecDesign['systemparams']['Maxm2perkW'] 
    Minm2perkW = elecDesign['systemparams']['Minm2perkW']
    # Read in Spacing Params from REST API


    TotalPanelArea = 0
    for AreaLabel in LatPolyfromfile:

        Areas[AreaLabel] = Geo2M.Geo2M3D(LatPolyfromfile[AreaLabel])
        InstallPlanePoints = Areas[AreaLabel].Real2InPlane(Areas[AreaLabel].Coords) # Project entire shape to installation surface in 2D
        OuterPoly = InstallPlanePoints  
        OuterPolyWKeepOut = LinearRing(InstallPlanePoints).parallel_offset(SpacingParams['setback'],"right",join_style=2)

        OuterPolyWKeepOut = LinearRing(OuterPoly).parallel_offset(SpacingParams['setback'],"right",join_style=2) # Demarcate keepout areas all along the boundaries

        ProjHoles = [Areas[AreaLabel].Real2InPlane(HoleCoords) for HoleCoords in Areas[AreaLabel].Holes]

        S = Polygon(Polygon(OuterPolyWKeepOut).exterior.coords,[Polygon(inner).exterior.coords for inner in (ProjHoles)]) # Prepare input polygon

        MultiPanelPolygons = GridM.FillAreaWithPanels(S,ModDefData,SpacingParams)

        MaxPanelArea = np.sum([Pol.area for Pol in MultiPanelPolygons])
        TotalPanelArea += MaxPanelArea

        DCfromArea[AreaLabel] = [MaxPanelArea/Maxm2perkW,MaxPanelArea/Minm2perkW]

        tilt = Areas[AreaLabel].tilt
        azimuth = Areas[AreaLabel].azimuth
    """
    # To display, GHI and Total Incident Radiation - Current, Total Incident Radiation - if ideal 
    for AreaLabel in tilt:
        tdf = pd.concat(
            {AreaLabel: PVcalc.WeatherDatatoDisplay(SolData, location_data, tilt[AreaLabel], azimuth[AreaLabel])},
            names=['AreaLabels'])
        Weather2Display = pd.concat([Weather2Display, tdf])

    # TODO: Save Install Plane Points - or perform second rotation
    #    Maxm2perkW = ELPARM['systemparams']['Maxm2perkW'] 
    #    Minm2perkW = ELPARM['systemparams']['Minm2perkW']
    #     
    #    
    #    TotalDCfromArea = [TotalPanelArea/Maxm2perkW,TotalPanelArea/Minm2perkW]
    #    RecommendedAC = [TotalPanelArea/Maxm2perkW/1.3,TotalPanelArea/Minm2perkW/1.3]
    #    
    #        if MaxPanelArea>0:
    #            ACPowerfrmSpace = [MaxPanelArea/Maxm2perkW,MaxPanelArea/Minm2perkW] 
    #        else:
    #            ACPowerfrmSpace = 'None'
    #        

    Weather_entity = {
        'Weather': {'weathersource': weathersource, 'TZ': SolmetaData['TZ'],
                    'finalweatherfile': finalweatherfile,
                    'SolmetaData': SolmetaData
                    }
    }

    entity.update(Weather_entity)
    entity.update({"status": "Weather"})

    ds.put(entity)

    # Show weather for best Irradiated surface in monthly resolution

    outputdic = {"Weather2Display":
                     Weather2Display.loc[
                         pd.IndexSlice[Weather2Display.groupby(level=[0]).sum()['IrrOnSurface'].idxmax(), :]].groupby(
                         [pd.Grouper(freq='1M')]).mean()
                         .to_dict('list'), "IdealTilt": opttilt}

    outputdic.update({"Status": errmsg})

    return jsonify(outputdic), 200


@app.route('/update-user-params', methods=['POST', 'GET'])  # 
@jwt_authenticated
def userparams():
    ds = datastore.Client()
    gcs = storage.Client()
    bucket = gcs.get_bucket(BUCKET_NAME)

    req_json = request.get_json()['Params']
    userinput = req_json
    userinput['email'] = request.email
    orgname = request.org
    parent_key = ds.key('users', userinput['email'])

    message = {}
    outputdict = {}
    default_costparams = FINPARM['cost']
    OutputDic = {}
    uniqueid_Proj = 'Uid_' + userinput['email'] + '_Pid_' + userinput['project_id']

    if "uploadvals" not in userinput:
        message.update({"uploadvals": "No params key to update"})

    else:
        with ds.transaction():
            entity = ds.get(key=ds.key('Project', userinput['project_id'], parent=parent_key))
            if not entity:
                message.update({"error": "No project with username {} and project {} exists".format(userinput['email'],
                                                                                                    userinput[
                                                                                                        'project_id'])})
                return jsonify(message), 400

            if "criteria" in userinput['uploadvals']:
                entity.update({"criteria": userinput['uploadvals']['criteria'], "status": "Optimization Criteria"})

                message.update({"Success": "Criteria updated"})

            elif "finparams" in userinput['uploadvals']:
                entity.update({"status": "Financials"})
                if 'finparams' in entity:
                    Dic = entity['finparams']
                    for keys in userinput['uploadvals']['finparams']:
                        Dic[keys] = userinput['uploadvals']['finparams'][keys]
                    if 'Adjustments' in Dic:
                        Adjustments = userinput['uploadvals']['finparams']['Adjustments']
                        for k, v in Adjustments.items():
                            if 'data' in v:
                                if v['data'] == None:
                                    v['data'] = 0
                        Dic['Adjustments'] = Adjustments

                else:
                    Dic = userinput['uploadvals']['finparams']
                    Adjustments = userinput['uploadvals']['finparams']['Adjustments']
                    for k, v in Adjustments.items():
                        if v['data'] == None:
                            v['data'] = 0
                    Dic['Adjustments'] = Adjustments
                entity.update({"finparams": Dic})
                message.update({"Success": "financial params updated"})

            elif "pvmoduledata" in userinput["uploadvals"]:
                if 'name' not in userinput['uploadvals']['pvmoduledata']:
                    return jsonify({"error": "No component name"}), 400
                else:
                    name = userinput['uploadvals']['pvmoduledata']['name']
                    moddata = userinput['uploadvals']['pvmoduledata']['params']

                uniqueid = 'Uid_' + userinput['email']

                if type(moddata) == str:
                    if moddata.endswith('.csv'):
                        blobcname = USER_UPLOADS + moddata
                        cwd = os.getcwd()
                        try:
                            blobcsv = bucket.get_blob(blobcname)
                        except:
                            return jsonify({"error": "No file uploaded as {}".format(blobcname)}), 400
                        if blobcsv:
                            blobcsv.download_to_filename(os.path.join(cwd, moddata))
                            moddatadict_raw = pd.read_csv(moddata).set_index('params').to_dict()
                            stringkeys = ['BIPV', 'Date', 'celltype', 'Manufacturer', 'Name']
                            moddatadict = {key: float(value) if key not in stringkeys else value
                                           for key, value in moddatadict_raw['value'].items()}
                        else:
                            return jsonify({"error": "No file uploaded as {}".format(blobcname)}), 400

                        moddata = moddatadict
                    else:
                        return jsonify({"error": "wrong format of file "})

                blobname_pvmod = USER_UPLOADS + uniqueid + '_moduledata.pkl'
                blobpv = bucket.get_blob(blobname_pvmod)
                if blobpv:
                    s = blobpv.download_as_string()
                    USERPVDF = pickle.loads(s)
                    # USERPVDF[name] = [moddata[ind] for ind in USERPVDF.index]
                    if name in USERPVDF:
                        USERPVDF.drop(columns=name, inplace=True)
                    NewModDf, errmsg = SD.getModparamsfromDS(moddata, name)  # Only works for desoto model
                    USERPVDF = USERPVDF.join(NewModDf, how='outer').loc[
                        ~USERPVDF.join(NewModDf, how='outer').index.duplicated(keep='first')]

                else:
                    blobpv = bucket.blob(blobname_pvmod)
                    USERPVDF = pd.DataFrame()  # pd.DataFrame(moddata.values(),index=moddata.keys(),columns=[name])
                    NewModDf, errmsg = SD.getModparamsfromDS(moddata, name)  # Only works for desoto model
                    USERPVDF = USERPVDF.join(NewModDf, how='outer').loc[
                        ~USERPVDF.join(NewModDf, how='outer').index.duplicated(keep='first')]

                blobpv.upload_from_string(pickle.dumps(USERPVDF))
                message.update(
                    {"Component upload": "{} uploaded successfully for user {} ".format(name, userinput['email'])})

            elif "invdata" in userinput["uploadvals"]:
                if 'name' not in userinput['uploadvals']['invdata']:
                    return jsonify({"error": "No component name"})
                else:
                    name = userinput['uploadvals']['invdata']['name']
                moddata = userinput['uploadvals']['invdata']['params']
                uniqueid = 'Uid_' + userinput['email']

                if type(moddata) == str:
                    if moddata.endswith('.csv'):
                        blobcname = USER_UPLOADS + moddata
                        cwd = os.getcwd()
                        try:
                            blobcsv = bucket.get_blob(blobcname)
                        except:
                            return jsonify({"error": "No file uploaded as {}".format(blobcname)}), 400
                        if blobcsv:
                            blobcsv.download_to_filename(os.path.join(cwd, moddata))
                        else:
                            return jsonify({"error": "No file uploaded as {}".format(blobcname)}), 400
                        moddatadict = pd.read_csv(moddata).set_index('params').to_dict()
                        moddata = moddatadict['value']
                    else:
                        return jsonify({"error": "wrong format of file "})

                blobname_inv = USER_UPLOADS + uniqueid + '_invdata.pkl'
                blobinv = bucket.get_blob(blobname_inv)
                NEWINVDF = pd.DataFrame(moddata.values(), index=moddata.keys(), columns=[name])
                if blobinv:
                    s = blobinv.download_as_string()
                    USERINVDF = pickle.loads(s)

                    if name in USERINVDF:
                        USERINVDF.drop(columns=name, inplace=True)

                    USERINVDF = USERINVDF.join(NEWINVDF, how='outer').loc[
                        ~USERINVDF.join(NEWINVDF, how='outer').index.duplicated(keep='first')]
                    # USERINVDF[name] = [moddata[ind] for ind in USERINVDF.index]

                else:
                    blobinv = bucket.blob(blobname_inv)
                    USERINVDF = pd.DataFrame(moddata.values(), index=moddata.keys(), columns=[name])

                blobinv.upload_from_string(pickle.dumps(USERINVDF))
                message.update(
                    {"Component upload": "{} uploaded successfully for user {}".format(name, userinput['email'])})

            elif "components" in userinput['uploadvals']:
                UpDic = {}
                if 'Components' in entity:
                    UpDic = entity['Components']
                    UpDic.update(userinput['uploadvals']['components'])
                    entity.update({'Components': UpDic})

                else:
                    UpDic.update(userinput['uploadvals']['components'])
                    entity.update({"Components": UpDic})

                # Change the Cost values also to reflect the new components
                # TODO: Update Cost Params as well
                if "Cost" in entity:
                    if 'ActiveQuote' not in entity:
                        CDic = entity['Cost']
                        CDic['PVCost'] = {PVName: {'data': default_costparams['PVCost']['data'][0],
                                                   'Unit': default_costparams['PVCost']['Unit']} if PVName not in CDic[
                            'PVCost'] else
                        {'data': CDic['PVCost'][PVName]['data'],
                         'Unit': CDic['PVCost'][PVName]['Unit']} for PVName in UpDic['PVName']}

                        CDic['InvCost'] = {InvName: {'data': default_costparams['InvCost']['data'][0],
                                                     'Unit': default_costparams['InvCost']['Unit']} if InvName not in
                                                                                                       CDic[
                                                                                                           'InvCost'] else
                        {'data': CDic['InvCost'][InvName]['data'],
                         'Unit': CDic['InvCost'][InvName]['Unit']} for InvName in UpDic['InvName']}

                        entity.update({'Cost': CDic})
                    else:
                        CDic = {}

                else:
                    CDic = {}
                    CDic['PVCost'] = {PVName: {'data': default_costparams['PVCost']['data'][0],
                                               'Unit': default_costparams['PVCost']['Unit']} for PVName in
                                      UpDic['PVName']}
                    CDic['InvCost'] = {InvName: {'data': default_costparams['InvCost']['data'][0],
                                                 'Unit': default_costparams['InvCost']['Unit']} for InvName in
                                       UpDic['InvName']}

                    entity.update({'Cost': CDic})

                message.update({"Components upload": "{} uploaded successfully for user {}".format(
                    userinput['uploadvals']['components'], userinput['email'])})
                OutputDic.update({"Cost": CDic, "Components": UpDic})
                # message.update({"debug": [CDic,UpDic]})
            elif "cost" in userinput['uploadvals']:
                if 'Cost' in entity:
                    UpDic = userinput['uploadvals']['cost']
                    # UpDic = entity['Cost']
                    entity.update({"Cost": UpDic})
                    # for item in userinput['uploadvals']['cost']:
                    #     UpDic[item]=userinput['uploadvals']['cost'][item]
                else:
                    UpDic = userinput['uploadvals']['cost']
                    entity.update({"Cost": UpDic})
                message.update({"Cost update": "{} uploaded successfully for user {} ".format(
                    userinput['uploadvals']['cost'], userinput['email'])})

            elif "bominputfilter" in userinput['uploadvals']:
                UpDic = userinput['uploadvals']['bominputfilter']
                entity.update({"bominputfilter": UpDic})
                if 'cablelength' in UpDic:
                    TotalDCInOut = UpDic['cablelength']['inner'] + UpDic['cablelength']['outer']
                    if 'AreaFigures' in entity:
                        entity['AreaFigures'].update({"TotalDCInOut": TotalDCInOut})
                    else:
                        entity.update({"AreaFigures": {"TotalDCInOut": TotalDCInOut}})
                message.update({"bomfilter": "{} added for filtering for user {}".format(
                    userinput['uploadvals']['bominputfilter'], userinput['email'])})
            elif "status" in userinput['uploadvals']:
                entity.update({"status": userinput['uploadvals']['status']})
            elif "StringArrayConfig" in userinput['uploadvals']:
                entity.update({"StringArrayConfig": userinput['uploadvals']['StringArrayConfig']})
            elif "ReportContentsStored" in userinput['uploadvals']:
                entity.update({"ReportContentsStored": userinput['uploadvals']['ReportContentsStored']})

            ds.put(entity)

    if "defaultvals" not in userinput:
        message.update({"defaultvals": "No defaultvals requested"})

    else:
        if userinput['defaultvals'] == "cost":
            outputdict.update({"default costs": FINPARM["cost"]})
            message.update({"Success": "Default costs provided"})
        elif userinput['defaultvals'] == "financial":
            outputdict.update({"default financials": FINPARM["financial"]})
            message.update({"Success": "Default financial params provided"})

        elif userinput['defaultvals'] == "ACPower":

            #### Remove this entire thing #####

            entity = ds.get(key=ds.key('Project', userinput['project_id'], parent=parent_key))
            if not entity:
                message.update({"error": "No project with username {} and project {} exists".format(userinput['email'],
                                                                                                    userinput[
                                                                                                        'project_id'])})
                return jsonify(message), 400

            ##################GET DEMAND TS############
            country = "CHE"
            blobname = AUXFILES + 'PriceAPICountry.json'
            CI = json.loads(bucket.blob(blobname).download_as_string())
            blobnamePic = AUXFILES + CI['Data'][country]['PriceDb']
            path = os.getcwd()
            e = bucket.blob(blobnamePic).download_to_filename(os.path.join(path, 'PriceDb.pkl'))
            with open(os.path.join(path, 'PriceDb.pkl'), 'rb') as f:
                Data = pickle.load(f)
            UpDic = {}
            if 'Customer Inputs' in entity:
                CIn = entity['Customer Inputs']
                if 'YearlyElecRequired' in entity['Customer Inputs']:
                    if (('Elec4HeatkWh' in entity['Customer Inputs']['YearlyElecRequired']) and (
                            'EleckWh' in entity['Customer Inputs']['YearlyElecRequired'])):
                        TotalHeat = entity['Customer Inputs']['YearlyElecRequired']['Elec4HeatkWh']
                        TotalElec = entity['Customer Inputs']['YearlyElecRequired']['EleckWh']
                        DemandTS, E, H = Sup.MakeGenericDemand(Data, TotalElec=TotalElec, TotalHeat=TotalHeat)
                    elif 'TotalElec' in entity['Customer Inputs']['YearlyElecRequired']:
                        TotalElec = entity['Customer Inputs']['YearlyElecRequired']['TotalElec']
                        DemandTS, E, H = Sup.MakeGenericDemand(Data, TotalElec=TotalElec)
                    else:
                        DemandTS, E, H = Sup.MakeGenericDemand(Data)
                else:
                    DemandTS, E, H = Sup.MakeGenericDemand(Data)

                UpDic.update({"YearlyElecRequired": {"EleckWh": E, "Elec4HeatkWh": H, "TotalElec": E + H}})
                CIn.update(UpDic)
                entity.update(CIn)
            else:
                DemandTS, E, H = Sup.MakeGenericDemand(Data)
                entity.update(
                    {'Customer Inputs': {"YearlyElecRequired": {"EleckWh": E, "Elec4HeatkWh": H, "TotalElec": E + H}}})

            #  Pickle DemandTS        
            Demandblobname = USER_UPLOADS + uniqueid_Proj + '_demandTS.pkl'
            bucket.blob(Demandblobname).upload_from_string(pickle.dumps(DemandTS))

            TotalElecCons = E + H
            ACPowerFromConsRange = [TotalElecCons / 8760, (TotalElecCons / 8760) * 3]
            #################################################################

            if 'Location' in entity:
                TotalPanelArea = entity['Location'].get('MaxPanelArea', entity['Location'].get('TotalPanelArea', 0))
                # if 'TotalPanelArea' in entity['Location']:
                #     TotalPanelArea = entity['Location']['TotalPanelArea']
                Maxm2perkW = ELPARM['systemparams']['Maxm2perkW'] * 1.3
                Minm2perkW = ELPARM['systemparams']['Minm2perkW'] * 1.3
                ACPowerFromArea = [TotalPanelArea / Maxm2perkW, TotalPanelArea / Minm2perkW]

            else:
                ACPowerFromArea = [0, 0]
            #################################################################        

            if 'Demand' in entity:
                ACPowerStored = entity['Demand']['ACpower']
                if 'DemandTS' not in entity['Demand']:
                    entity['Demand']['DemandTS'] = Demandblobname
            else:
                ACPowerStored = 0
                entity.update({'Demand': {'DemandTS': Demandblobname, "ACpower": ACPowerStored}})
            outputdict.update({"ACPowerFromConsumption": ACPowerFromConsRange, "ACPowerFromArea": ACPowerFromArea,
                               "TotalElecCons": TotalElecCons, "ACPowerStore": ACPowerStored})
            message.update({"Success": "ACPower Estimates provided"})
            ds.put(entity)

    outputdict.update(message)
    outputdict.update(OutputDic)

    return jsonify(outputdict), 200


@app.route('/cost-financials', methods=['POST', 'GET'])  # Without storage /techselect_withstorage
@jwt_authenticated
def costfinancial():
    ds = datastore.Client()
    gcs = storage.Client()

    req_json = request.get_json()['Costs']

    userinput = req_json
    userinput['email'] = request.email
    orgname = request.org
    parent_key = ds.key('users', userinput['email'])
    message = {}
    OutputDic = {}

    entity = ds.get(key=ds.key('Project', userinput['project_id'], parent=parent_key))
    if not entity:
        message.update({"error": "No project with username {} and project {} exists".format(userinput['email'],
                                                                                            userinput['project_id'])})
        return jsonify(message), 400

    uschklist = ['cost', 'financial']
    for us in uschklist:
        if us not in userinput:
            return jsonify({"error": "{} not set in input request".format(us)}), 400

            # output.update(userinput['cost'])

    """
    Transform to such a structure

    {'BatteryCost': {'Unit': 'per kWh', 'data': 223},
     'PVCost': {'Unit': 'per Wp', 'data': [0.3,0.4]},
                },
     'InvCost': {'Unit': 'per Wac', 'data': [0.05,0.03]},
                 },
     'Construction': {'Unit': 'per plant', 'data': 0},
     'Bos': {'Unit': 'per plant', 'data': 0},
     'Curr':"USD"}
    """
    CostData = userinput['cost']

    # CostDic = {k:{num:{"Unit":v['Unit'],"data":d} for num,d in zip(range(len(v['data'])),v['data']) } for k,v in CostData.items() if k!='AddonCosts'}
    CostDic = {k: v for k, v in CostData.items() if k != 'AddonCosts'}

    if 'Curr' in CostData['PVCost']:
        Curr = CostData['PVCost']['Curr']
    else:
        Curr = 'CHF'
    if 'AddonCosts' in CostData:
        # TODO: Change these lines to just one if the structure is rationalised
        try:
            CostDic.update(
                {v['Name']: {"Unit": v['Units'], "data": float(v['data'])} for v in CostData['AddonCosts'].values() if
                 type(v) != str})
        except:
            CostDic.update(
                {v['Name']: {"Unit": v['Unit'], "data": float(v['data'])} for v in CostData['AddonCosts'].values() if
                 type(v) != str})

    CostDic.update({"Curr": Curr})
    # TODO: Data Model for Costing
    OutputDic.update({'cost': CostDic})

    entity.update({"Cost": CostDic})

    # Convert cost and financials in a different function
    FinanceData = userinput['financial']
    Curr = FinanceData['Adjustments']['Curr']
    FinanceData['Adjustments'] = {v['Name']: {'Unit': v['Units'], 'data': v['data']} for v in
                                  FinanceData['Adjustments'].values() if type(v) != str}
    FinanceData['Curr'] = Curr

    entity.update({'finparams': FinanceData})

    ds.put(entity)

    return jsonify({"success": "Cost and financial data uploaded for {}".format(userinput['project_id'])}), 200


@app.route('/tariff', methods=['POST', 'GET'])
@jwt_authenticated
def tariffprice():
    ds = datastore.Client()
    gcs = storage.Client()
    bucket = gcs.get_bucket(BUCKET_NAME)

    req_json = request.get_json()['Tariff']

    userinput = req_json
    userinput['email'] = request.email
    orgname = request.org
    parent_key = ds.key('users', userinput['email'])
    message = {}

    entity = ds.get(key=ds.key('Project', userinput['project_id'], parent=parent_key))
    if not entity:
        message.update({"error": "No project with username {} and project {} exists".format(userinput['email'],
                                                                                            userinput['project_id'])})
        return jsonify(message), 400
    uniqueid = 'Uid_' + userinput['email'] + '_Pid_' + userinput['project_id']
    entchklist = ['Location']
    for ent in entchklist:
        if ent not in entity:
            return jsonify({"error": "{} not set in datastore".format(ent)}), 400

    uschklist = ["action"]
    for us in uschklist:
        if us not in userinput:
            return jsonify({"error": "{} not set in input request".format(us)}), 400

    if 'Profile' in entity['Demand']:
        Profile = entity['Demand']['Profile']
        profiletype = Profile.get('profiletype')

    '''
    {"action:"getdata","update"
      if action==update, 
         {"pricetariff": {"eprice":234, pvtariffavg: 23, pvtariffHiWin:23, pvtariffLoWin .... }}
    '''
    OutputDic = {}
    action = userinput['action']
    if 'countrycode' in entity['Location']:
        country = entity['Location']['countrycode']
        blobname = AUXFILES + 'PriceAPICountry.json'
        blob = bucket.get_blob(blobname)
        e = blob.download_as_string()
        CI = json.loads(e)
    if country in CI['Data']:
        blobnamePic = AUXFILES + CI['Data'][country]['PriceDb']
        Data = pickle.loads(bucket.get_blob(blobnamePic).download_as_string())
        # Data = pickle.loads(bucket.get_blob(AUXFILES+"CH_Gemeinde_EPrice_2022.pkl").download_as_string()) 
    if entity['status'] != 'Lock':
        entity.update({"status": "Tariff Details"})
    if action == 'readstored':
        DataSaved = {}
        try:
            if 'EpriceTariffFile' in entity:
                blobname = entity['EpriceTariffFile']
                if blobname.startswith(USER_UPLOADS):
                    datastored = pickle.loads(bucket.get_blob(blobname).download_as_string())
                else:
                    datastored = pickle.loads(bucket.get_blob(USER_UPLOADS + blobname).download_as_string())

                valsin = ["state", "supplier", "ValsForGraph", "PVTariffSummary", "tariff", "city"]
                for keys in valsin:
                    if keys in datastored:
                        DataSaved.update({keys: datastored[keys]})
        except:
            pass
            # Populate keys for choices
        if 'countrycode' in entity['Location']:
            country = entity['Location']['countrycode']
        else:
            ds.put(entity)
            return jsonify(DataSaved), 200
        if country == 'CHE':
            if 'PLZ' in entity['Location']:
                PLZ = entity['Location']['PLZ']
                if not PLZ:
                    try:
                        if entity['projectdetails']['customerinfo'].get('zip'):
                            PLZ = entity['projectdetails']['customerinfo'].get('zip')
                    except:
                        PLZ = 8001
                else:
                    PLZ = 8001
            else:
                if entity['projectdetails']['customerinfo'].get('zip'):
                    PLZ = entity['projectdetails']['customerinfo'].get('zip')
                else:
                    PLZ = 8001

            # Get connection details to external API
            connection = CI['Data'][country]

            # Get stored files with Price DB information
            blobnamePic = AUXFILES + CI['Data'][country]['PriceDb']
            Data = pickle.loads(bucket.get_blob(blobnamePic).download_as_string())
            # Get Supplier and Tarifflist
            CT = Sup.CHTariffGetter(Data, connection, PLZ)
            allsuplist = CT.supplierlist()
            DataSaved.update({"supplierlist": list(allsuplist.keys()), "Curr": "CHF", "city": "", "state": "",
                              "tarifflist": ['H1', 'H2', 'H3', 'H4', 'H5', 'H6', \
                                             'H7', 'H8', 'C1', 'C2', 'C3', 'C4', 'C5', 'C6', 'C7'], "citylist": "",
                              "countrycode": country, "PLZ": PLZ})
        elif country == 'BRA':
            if 'PLZ' in entity['Location']:
                PLZ = entity['Location']['PLZ']
            else:
                PLZ = 100100

            BRAGet = Sup.BRATariffgetter(Data)
            if 'state' in DataSaved:
                citylist = BRAGet.cityfromstate(DataSaved['state'])
            else:
                citylist = []
            DataSaved.update({"supplierlist": BRAGet.supplierlist(), "tarifflist": BRAGet.tarifflist(),
                              "Curr": "BRL", "citylist": citylist, "statelist": BRAGet.statelist(),
                              "countrycode": country, "PLZ": PLZ})

        ds.put(entity)
        return jsonify(DataSaved), 200

    if action == 'auto getdata':

        if 'countrycode' in entity['Location']:
            country = entity['Location']['countrycode']
        #     blobname = AUXFILES+'PriceAPICountry.json'
        #     blob=bucket.get_blob(blobname)
        #     e = blob.download_as_string()
        #     CI = json.loads(e)

        else:
            return jsonify({"status": "no country information available - update in locationlayout",
                            "countrycode": "", "PLZ": ""}), 400
        if country == 'CHE':
            if 'PLZ' in entity['Location']:
                PLZ = entity['Location']['PLZ']
            else:
                PLZ = 8001
            # Get connection details to external API
            connection = CI['Data'][country]

            # Get stored files with Price DB information
            # blobnamePic = AUXFILES+CI['Data'][country]['PriceDb']
            # Data = pickle.loads(bucket.get_blob(blobnamePic).download_as_string()) 

            # Select Elec Supplier according to PLZ/ZIP
            CT = Sup.CHTariffGetter(Data, connection, PLZ)
            SupplierList = CT.suppliers()
            allsuplist = CT.supplierlist()
            OutputDic.update({"supplierlist": list(allsuplist.keys()), "Curr": "CHF", "city": "", "state": "",
                              "tarifflist": ['H1', 'H2', 'H3', 'H4', 'H5', 'H6', \
                                             'H7', 'H8', 'C1', 'C2', 'C3', 'C4', 'C5', 'C6', 'C7'], "citylist": ""})
            evuID_old = '0'
            try:
                if 'EpriceTariffFile' in entity:
                    blobname = entity['EpriceTariffFile']
                    if blobname.startswith(USER_UPLOADS):
                        epriceTariffFile = pickle.loads(bucket.get_blob(blobname).download_as_string())
                    else:
                        epriceTariffFile = pickle.loads(bucket.get_blob(USER_UPLOADS + blobname).download_as_string())

                    if 'evuID' in epriceTariffFile:
                        evuID_old = epriceTariffFile['evuID']
                        SupplierItemOld = {"Name": epriceTariffFile['Eprice']['Netzbetreiber'][0], 'nrElcom': evuID_old}
                        SupplierList.insert(0, SupplierItemOld)
            except:
                pass

            DailyTariff = 'None'
            i = 0
            while (type(DailyTariff) == str) & (i < len(SupplierList)):
                # Data corruption causes nrElcom string to get messed up
                try:
                    evuID = SupplierList[i]['nrElcom']
                except KeyError:
                    nrstring = [s for s in SupplierList[i].keys() if re.search('.*nrElcom', s)][0]
                    evuID = SupplierList[i][nrstring]
                supplier = SupplierList[i]['Name']
                DailyTariff, tariffsummary = CT.pvtariff(evuID)
                i += 1
            if not (type(DailyTariff) == str):
                DailyTariff.fillna(0, inplace=True)

                # for i in range(len(SupplierList)):
            #     if type(DayTariff) == str:
            #         if (SupplierList[i]!={}):
            #             evuID = SupplierList[i]['nrElcom']
            #             DailyTariff,tariffsummary=CT.pvtariff(evuID)
            #     else:
            #         break

            YearlyTariff = CT.makeyearly(DailyTariff)

            Eprice = CT.getEprice(evuID)
            if 'Profile' in entity['Demand']:
                profiletype = entity['Demand']['Profile']['profiletype']
            else:
                profiletype = ''
            if profiletype not in Eprice.index:
                tariffsummary.update({"EpriceAvg": Eprice.iloc[0]['Total'], "Epricetotal": Eprice.iloc[0]['Total'],
                                      "profiletype": Eprice.iloc[0].name})
            else:
                EpriceExtract = Eprice.loc[profiletype, 'Total'] if isinstance(Eprice.loc[profiletype, 'Total'],
                                                                               float) else \
                Eprice.loc[profiletype, 'Total'][0]
                tariffsummary.update(
                    {"EpriceAvg": EpriceExtract, "Epricetotal": EpriceExtract, "profiletype": profiletype})

            if (YearlyTariff['PVTariff'].mean() == 0):
                YearlyTariff['Eprice'] = tariffsummary['EpriceAvg']
                # ensure only single number
            else:
                YearlyTariff['Eprice'] = (YearlyTariff['PVTariff'] / (YearlyTariff['PVTariff'].mean())) * tariffsummary[
                    'EpriceAvg']
            YearlyTariff.fillna(0, inplace=True)

            # 'pvtariffAvg','pvtariffadjust','EpriceAvg','Epriceadjust'

            tariffsummary.update({"Epriceadjust": 0,
                                  "pvtariffadjust": 0, "pvtarifftotal": 0, "EpriceHi": YearlyTariff['Eprice'].max(),
                                  "EpriceLow": YearlyTariff['Eprice'].min()})  # Must match manual upload objects

            if type(DailyTariff) == str:
                ForGraph = {"hours": [x for x in range(24)], "pvtariffWin": [0 for x in range(24)],
                            "pvtariffSum": [0 for x in range(24)],
                            "Eprice": [p for p in YearlyTariff['Eprice'].iloc[:24].values]}
                #  "EpriceAvg":[Eprice.loc['H5','Total'] for x in range(24)]}
            else:
                ForGraph = {"hours": [x for x in range(24)],
                            "pvtariffWin": list(DailyTariff[('winter', 'pvtariff')].astype('float')),
                            "pvtariffSum": list(DailyTariff[('summer', 'pvtariff')].astype('float')),
                            "Eprice": [p for p in YearlyTariff['Eprice'].iloc[:24].values]}
                # "EpriceAvg":[Eprice.loc['H5','Total'] for x in range(24)]}
            # Extraneous - only to maintain same keys as in manual
            ForGraph.update({"pvtariffAvg": ""})

            # Convert Yearly Series into CHF/kWh
            YearlyTariff['Eprice'] = YearlyTariff['Eprice'] * 0.01
            YearlyTariff['PVTariff'] = YearlyTariff['PVTariff'] * 0.01

            Data2save = {"Eprice": Eprice, "YearlyTariff": YearlyTariff, "PVTariffSummary": tariffsummary,
                         "evuID": evuID, "supplier": supplier, "ValsForGraph": ForGraph}

            blobname = USER_UPLOADS + uniqueid + '_epriceTariff.pkl'
            blob = bucket.blob(blobname)
            blob.upload_from_string(pickle.dumps(Data2save))
            epriceTariffFile = blobname  # uniqueid+'_epriceTariff.pkl'
            entity.update({"EpriceTariffFile": epriceTariffFile})

            if "error" in tariffsummary:
                OutputDic.update({"error": tariffsummary['error']})
            OutputDic.update({"PVTariffSummary": tariffsummary,
                              "countrycode": country, "PLZ": PLZ})

            OutputDic.update({"ValsForGraph": ForGraph})
        elif country == 'BRA':
            # blobnamePic = AUXFILES+CI['Data'][country]['PriceDb']
            # Data = pickle.loads(bucket.get_blob(blobnamePic).download_as_string()) 

            BRAGet = Sup.BRATariffgetter(Data)
            OutputDic = {}
            statelist = BRAGet.statelist()
            OutputDic.update({"supplierlist": BRAGet.supplierlist(), "tarifflist": BRAGet.tarifflist(), "Curr": "BRL",
                              "countrycode": country, "statelist": statelist})
            if 'PLZ' in entity['Location']:
                ICMS = Data['StateTaxes']
                # if entity['Location']['PLZ']!=None:
                try:
                    PLZ = int(str(entity['Location']['PLZ'])[:5])  # take only 5 first values
                    STList = [state for state in ICMS.index if
                              ((ICMS.loc[state, 'PLZMax'] >= PLZ >= ICMS.loc[state, 'PLZMin']) or
                               (ICMS.loc[state, 'PLZMax1'] >= PLZ >= ICMS.loc[state, 'PLZMin1']))]
                except:
                    STList = []
                    PLZ = 100100
                if STList != []:
                    state = STList[0]
                else:
                    state = BRAGet.statelist()[0]

                if 'locationName' in entity['Location']:
                    Loccity = entity['Location']['locationName']
                    if STList != []:
                        city = BRAGet.cityfromstate(state, Loccity)[0]
                    else:
                        city = ""
                if (type(state) == str) & (city != ""):
                    supplier = BRAGet.citytoSup(state, city)[0]
                else:
                    supplier = BRAGet.supplierlist(state)[0]
                tariff = 'Residential - B1 - White'
                OutputDic.update({"tariff": 'Residential - B1 - White'})
                OutputDic.update({"state": state, "city": city, "supplier": supplier, "PLZ": PLZ, "tariff": tariff})
            else:
                PLZ = 100100
                state = BRAGet.statelist()[0]
                tariff = 'Residential - B1 - White'
                supplier = BRAGet.supplierlist(state)[0]
                city = ""
                OutputDic.update({"state": state, "city": city, "PLZ": PLZ, "supplier": supplier, "tariff": tariff})

            EpriceDay, pvfitsummary = BRAGet.DailyTariff(supplier, tariff=tariff)
            EpriceYearly = BRAGet.makeyearly(EpriceDay['EpriceWet'], supplier)
            YearlyTariff = pd.DataFrame(EpriceYearly, columns=['Eprice'])
            YearlyTariff.fillna(0, inplace=True)
            YearlyTariff['PVTariff'] = YearlyTariff['Eprice'] * 0
            statetax = BRAGet.Data['StateTaxes'].loc[state].values[0]
            pvfitsummary.update({"Epriceadjust": statetax,
                                 "pvtariffadjust": 0, "pvtarifftotal": 0})
            OutputDic.update({"PVTariffSummary": pvfitsummary})
            ForGraph = {"hours": [x for x in range(24)], "pvtariffWin": [0] * 24,
                        "pvtariffSum": [0] * 24}
            Etodisplay = {
                "Eprice": {"WetSeasonElecPrice": EpriceDay['EpriceWet'], "DrySeasonElecPrice": EpriceDay['EpriceDry']}}
            ForGraph.update(Etodisplay)
            OutputDic.update({"ValsForGraph": ForGraph})
            OutputDic['PVTariffSummary']['Epricetotal'] = EpriceYearly.mean().values[0]
            OutputDic['PVTariffSummary']['EpriceHi'] = EpriceYearly.max().values[0]
            OutputDic['PVTariffSummary']['EpriceLow'] = EpriceYearly.min().values[0]
            OutputDic['PVTariffSummary']['nm'] = 1  # Net metering is  default in Brazil

            Data2save = {"Eprice": EpriceYearly, "YearlyTariff": YearlyTariff, "PVTariffSummary": pvfitsummary,
                         "supplier": supplier, "tariff": tariff, "state": state, "ValsForGraph": ForGraph,
                         "countrycode": country, "PLZ": PLZ, "city": city}

            if 'EpriceTariffFile' in entity:
                blobname = entity['EpriceTariffFile']
                datastored = pickle.loads(bucket.get_blob(blobname).download_as_string())
                for vals in datastored:
                    if vals in Data2save:
                        datastored[vals] = Data2save[vals]
                for vals in Data2save:
                    if vals not in datastored:
                        datastored[vals] = Data2save[vals]
                bucket.get_blob(blobname).upload_from_string(pickle.dumps(datastored))
            else:
                blobname = USER_UPLOADS + uniqueid + '_epriceTariff.pkl'
                bucket.blob(blobname).upload_from_string(pickle.dumps(Data2save))
                entity.update({"EpriceTariffFile": blobname})



        else:
            if 'PLZ' in entity['Location']:
                PLZ = entity['Location']['PLZ']
            else:
                PLZ = 0
            return jsonify(
                {"status": "information for this country not avaialble - use manual input", "country": country,
                 "PLZ": PLZ}), 400
    elif "get info" in action:
        infoparams = action['get info']
        OutputDic = {}
        if country == 'BRA':
            BRAGet = Sup.BRATariffgetter(Data)
            OutputDic.update({"statelist": BRAGet.statelist(), "tarifflist": BRAGet.tarifflist()})
            if 'state' in infoparams:
                state = infoparams['state']
                OutputDic.update({"supplierlist": BRAGet.supplierlist(state), "state": state})
                if 'city' in infoparams:
                    city = infoparams['city']
                    supplier = BRAGet.citytoSup(state, city)
                    OutputDic.update({"supplier": supplier})
                    citylist = BRAGet.cityfromstate(state)
                    OutputDic.update({"citylist": citylist})
                    citysel = BRAGet.cityfromstate(state, searchcity=city)
                    if len(citysel) > 0:
                        OutputDic.update({"city": citysel[0]})

                else:
                    OutputDic.update({"citylist": BRAGet.cityfromstate(state)})
                # default supplier and tariff
                supplier = OutputDic['supplierlist'][0]
                OutputDic.update({"supplier": supplier})
                tariff = 'Residential - B1 - White'
                OutputDic.update({"tariff": 'Residential - B1 - White'})
                EpriceDay, pvfitsummary = BRAGet.DailyTariff(supplier, tariff=tariff)
                EpriceYearly = BRAGet.makeyearly(EpriceDay['EpriceWet'], supplier)
                YearlyTariff = pd.DataFrame(EpriceYearly, columns=['Eprice'])
                YearlyTariff['PVTariff'] = YearlyTariff['Eprice'] * 0
                statetax = BRAGet.Data['StateTaxes'].loc[state].values[0]
                pvfitsummary.update({"Epriceadjust": statetax,
                                     "pvtariffadjust": 0, "pvtarifftotal": 0})
                OutputDic.update({"PVTariffSummary": pvfitsummary})
                ForGraph = {"hours": [x for x in range(24)], "pvtariffWin": [0] * 24,
                            "pvtariffSum": [0] * 24}
                Etodisplay = {"Eprice": {"WetSeasonElecPrice": EpriceDay['EpriceWet'],
                                         "DrySeasonElecPrice": EpriceDay['EpriceDry']}}
                ForGraph.update(Etodisplay)
                OutputDic.update({"ValsForGraph": ForGraph})
                OutputDic['PVTariffSummary']['Epricetotal'] = EpriceYearly.mean().values[0]
                OutputDic['PVTariffSummary']['EpriceHi'] = EpriceYearly.max().values[0]
                OutputDic['PVTariffSummary']['EpriceLow'] = EpriceYearly.min().values[0]
                OutputDic['PVTariffSummary']['nm'] = 1  # Net metering is  default in Brazil
                OutputDic['PVTariffSummary']['profiletype'] = profiletype
                Data2save = {"Eprice": EpriceYearly, "YearlyTariff": YearlyTariff, "PVTariffSummary": pvfitsummary,
                             "supplier": supplier, "tariff": tariff, "state": state, "ValsForGraph": ForGraph,
                             "countrycode": country}
                if 'city' in OutputDic:
                    Data2save['city'] = OutputDic['city']

                if 'EpriceTariffFile' in entity:
                    blobname = entity['EpriceTariffFile']
                    datastored = pickle.loads(bucket.get_blob(blobname).download_as_string())
                    for vals in datastored:
                        if vals in Data2save:
                            datastored[vals] = Data2save[vals]
                    for vals in Data2save:
                        if vals not in datastored:
                            datastored[vals] = Data2save[vals]
                    bucket.get_blob(blobname).upload_from_string(pickle.dumps(datastored))
                else:
                    blobname = USER_UPLOADS + uniqueid + '_epriceTariff.pkl'
                    bucket.blob(blobname).upload_from_string(pickle.dumps(Data2save))
                    entity.update({"EpriceTariffFile": blobname})

                # blobname = USER_UPLOADS +uniqueid+'_epriceTariff.pkl'                   
                # blob=bucket.blob(blobname)
                # blob.upload_from_string(pickle.dumps(Data2save)) 
                # epriceTariffFile = blobname #uniqueid+'_epriceTariff.pkl'
                # entity.update({"EpriceTariffFile":epriceTariffFile})   


            elif 'supplier' in infoparams:
                supplier = infoparams['supplier']
                OutputDic.update({"supplier": supplier})
                if 'tariff' in infoparams:
                    if infoparams['tariff'] in BRAGet.tarifflist():
                        EpriceDay, pvfitsummary = BRAGet.DailyTariff(supplier, tariff=infoparams['tariff'])
                        OutputDic.update({"tariff": infoparams['tariff']})
                    else:
                        EpriceDay, pvfitsummary = BRAGet.DailyTariff(supplier)
                        OutputDic.update({"tariff": 'Residential - B1 - White'})
                else:
                    EpriceDay, pvfitsummary = BRAGet.DailyTariff(supplier)
                    OutputDic.update({"tariff": 'Residential - B1 - White'})
                state = BRAGet.suppliertostate(supplier)
                OutputDic.update({"state": state})
                statetax = BRAGet.Data['StateTaxes'].loc[state].values[0]

                pvfitsummary.update({"Epriceadjust": statetax,
                                     "pvtariffadjust": 0, "pvtarifftotal": 0})
                OutputDic.update({"PVTariffSummary": pvfitsummary})

                ForGraph = {"hours": [x for x in range(24)], "pvtariffWin": [0] * 24,
                            "pvtariffSum": [0] * 24}
                Etodisplay = {"Eprice": {"WetSeasonElecPrice": EpriceDay['EpriceWet'],
                                         "DrySeasonElecPrice": EpriceDay['EpriceDry']}}
                ForGraph.update(Etodisplay)
                OutputDic.update({"ValsForGraph": ForGraph})

                EpriceYearly = BRAGet.makeyearly(EpriceDay['EpriceWet'], supplier)
                OutputDic['PVTariffSummary']['Epricetotal'] = EpriceYearly.mean().values[0]
                OutputDic['PVTariffSummary']['EpriceHi'] = EpriceYearly.max().values[0]
                OutputDic['PVTariffSummary']['EpriceLow'] = EpriceYearly.min().values[0]
                OutputDic['PVTariffSummary']['nm'] = 1  # Net metering is  default in Brazil
                OutputDic['PVTariffSummary']['profiletype'] = profiletype
                YearlyTariff = pd.DataFrame(EpriceYearly, columns=['Eprice'])
                YearlyTariff['PVTariff'] = YearlyTariff['Eprice'] * 0
                # PVTariff = pd.DataFrame(EpriceYearly*0,columns=['PVTariff'])

                Data2save = {"Eprice": EpriceYearly, "YearlyTariff": YearlyTariff, "PVTariffSummary": pvfitsummary,
                             "supplier": supplier, "tariff": OutputDic['tariff'], "state": state,
                             "ValsForGraph": ForGraph,
                             "countrycode": country}

                if 'EpriceTariffFile' in entity:
                    blobname = entity['EpriceTariffFile']
                    datastored = pickle.loads(bucket.get_blob(blobname).download_as_string())
                    for vals in datastored:
                        if vals in Data2save:
                            datastored[vals] = Data2save[vals]
                    for vals in Data2save:
                        if vals not in datastored:
                            datastored[vals] = Data2save[vals]
                    bucket.get_blob(blobname).upload_from_string(pickle.dumps(datastored))
                else:
                    blobname = USER_UPLOADS + uniqueid + '_epriceTariff.pkl'
                    bucket.blob(blobname).upload_from_string(pickle.dumps(Data2save))
                    entity.update({"EpriceTariffFile": blobname})

                # blobname = USER_UPLOADS +uniqueid+'_epriceTariff.pkl'                   
                # blob=bucket.blob(blobname)
                # blob.upload_from_string(pickle.dumps(Data2save)) 
                # epriceTariffFile = blobname #uniqueid+'_epriceTariff.pkl'
                # entity.update({"EpriceTariffFile":epriceTariffFile})        
        elif country == 'CHE':
            if 'supplier' in infoparams:
                if 'PLZ' in entity['Location']:
                    PLZ = entity['Location']['PLZ']
                else:
                    PLZ = 8001
                # Get connection details to external API
                connection = CI['Data'][country]
                CT = Sup.CHTariffGetter(Data, connection, PLZ)
                OutputDic.update({'tarifflist': ['H1', 'H2', 'H3', 'H4', 'H5', 'H6', \
                                                 'H7', 'H8', 'C1', 'C2', 'C3', 'C4', 'C5', 'C6', 'C7'], "citylist": ""})
                if infoparams['supplier'] == 'all':
                    OutputDic.update({"supplierlist": CT.AllSuppliers})

                else:
                    supname = infoparams['supplier']  # search string
                    supdict = CT.supplierlist(supname)
                    if len(supdict) > 1:
                        OutputDic.update({'supplierlist': list(supdict.keys())})

                    elif len(supdict) != 0:
                        evuID = [*supdict.values()][0]
                        supplier = [*supdict.keys()][0]
                        DailyTariff, tariffsummary = CT.pvtariff(evuID)
                        YearlyTariff = CT.makeyearly(DailyTariff)
                        Eprice = CT.getEprice(evuID)

                        if 'tariff' in infoparams:
                            if infoparams['tariff'] in ['H1', 'H2', 'H3', 'H4', 'H5', 'H6', 'H7', 'H8', 'C1', 'C2',
                                                        'C3', 'C4', 'C5', 'C6', 'C7']:
                                profiletype = infoparams['tariff']
                            else:
                                profiletype = 'H5'

                            EpriceExtract = Eprice.loc[profiletype, 'Total'] if isinstance(
                                Eprice.loc[profiletype, 'Total'], float) else Eprice.loc[profiletype, 'Total'][0]
                            tariffsummary.update(
                                {"Epriceadjust": 0, "Epricetotal": EpriceExtract, "EpriceAvg": EpriceExtract,
                                 "pvtariffadjust": 0, "pvtarifftotal": 0})  # Must match manual upload objects
                            tariffsummary.update({"profiletype": infoparams['tariff']})
                        else:
                            profiletype = 'H5'
                            EpriceExtract = Eprice.loc[profiletype, 'Total'] if isinstance(
                                Eprice.loc[profiletype, 'Total'], float) else Eprice.loc[profiletype, 'Total'][0]
                            tariffsummary.update(
                                {"Epriceadjust": 0, "Epricetotal": EpriceExtract, "EpriceAvg": EpriceExtract,
                                 "pvtariffadjust": 0, "pvtarifftotal": 0})  # Must match manual upload objects
                            tariffsummary.update({"profiletype": infoparams['tariff']})
                        # DailyEPrice = (DailyTariff/DailyTariff.mean())*tariffsummary['EpriceAvg']
                        if (YearlyTariff['PVTariff'].mean() == 0):
                            YearlyTariff['Eprice'] = tariffsummary['EpriceAvg']
                        else:
                            YearlyTariff['Eprice'] = (YearlyTariff['PVTariff'] / (YearlyTariff['PVTariff'].mean())) * \
                                                     tariffsummary['EpriceAvg']
                        YearlyTariff.fillna(0, inplace=True)

                        tariffsummary.update(
                            {"EpriceHi": YearlyTariff['Eprice'].max(), "EpriceLow": YearlyTariff['Eprice'].min()})

                        if "error" in tariffsummary:
                            OutputDic.update({"error": tariffsummary['error']})

                        OutputDic.update({"PVTariffSummary": tariffsummary,
                                          "countrycode": country, "PLZ": PLZ, })
                        if type(DailyTariff) == str:
                            ForGraph = {"hours": [x for x in range(24)], "pvtariffWin": [0 for x in range(24)],
                                        "pvtariffSum": [0 for x in range(24)],
                                        "Eprice": [p for p in YearlyTariff['Eprice'].iloc[:24].values]}
                            # "EpriceAvg":[Eprice.loc['H5','Total'] for x in range(24)]}
                        else:
                            DailyTariff.fillna(0, inplace=True)
                            ForGraph = {"hours": [x for x in range(24)],
                                        "pvtariffWin": list(DailyTariff[('winter', 'pvtariff')].astype('float')),
                                        "pvtariffSum": list(DailyTariff[('summer', 'pvtariff')].astype('float')),
                                        "Eprice": [p for p in YearlyTariff['Eprice'].iloc[:24].values]}
                            #  "EpriceAvg":[Eprice.loc['H5','Total'] for x in range(24)]}
                        # Extraneous - only to maintain same keys as in manual
                        ForGraph.update({"pvtariffAvg": 0})
                        # Convert to CHF/kWh
                        YearlyTariff['Eprice'] = YearlyTariff['Eprice'] * 0.01
                        YearlyTariff['PVTariff'] = YearlyTariff['PVTariff'] * 0.01

                        Data2save = {"Eprice": Eprice, "YearlyTariff": YearlyTariff, "PVTariffSummary": tariffsummary,
                                     "evuID": evuID, "supplier": supplier, "ValsForGraph": ForGraph}
                        blobname = USER_UPLOADS + uniqueid + '_epriceTariff.pkl'
                        blob = bucket.blob(blobname)
                        blob.upload_from_string(pickle.dumps(Data2save))
                        epriceTariffFile = blobname  # uniqueid+'_epriceTariff.pkl'
                        entity.update({"EpriceTariffFile": epriceTariffFile})

                        OutputDic.update({"ValsForGraph": ForGraph})



    elif action == 'manual getdata':
        if 'countrycode' in entity['Location']:
            countrycode = entity['Location']['countrycode']
        else:
            countrycode = ''
        if 'PLZ' in entity['Location']:
            PLZ = entity['Location']['PLZ']
        else:
            PLZ = 0
        if 'EpriceTariffFile' in entity:
            # blobname=USER_UPLOADS +entity['EpriceTariffFile']                  
            blobname = entity['EpriceTariffFile']
            TariffDic = pickle.loads(bucket.get_blob(blobname).download_as_string())
            # blob=bucket.get_blob(blobname)
            # s = blob.download_as_string()
            # TariffDic = pickle.loads(s)

            OutputDic = TariffDic['PVTariffSummary']
            YearlyTariff = TariffDic['YearlyTariff']
            ForGraph = {"hours": [x for x in range(24)],
                        "pvtariff": [p for p in YearlyTariff['PVTariff'].iloc[:24].values],
                        "Eprice": [p for p in YearlyTariff['Eprice'].iloc[:24].values]}

            # if (('pvtariffadjust' in OutputDic) and ('Epriceadjust' in OutputDic)):
            #     ForGraph = {"hours":[x for x in range(24)],"pvtariff":[OutputDic['pvtariffAvg']*(1+OutputDic['pvtariffadjust']/100) for x in range(24)],
            #                      "EpriceAvg":[OutputDic['EpriceAvg']*(1+OutputDic['Epriceadjust']/100) for x in range(24)]}
            # else:
            #     ForGraph = {"hours":[x for x in range(24)],"pvtariff":[OutputDic['pvtariffAvg'] for x in range(24)],
            #                      "EpriceAvg":[OutputDic['EpriceAvg'] for x in range(24)]}
            # OutputDic.update({"pvtariffadjust":"","Epriceadjust":""})
            # Extraneous - only to maintain same keys as in auto
            ForGraph.update({"pvtariffWin": "", "pvtariffSum": ""})
            OutputDic.update({"ValsForGraph": ForGraph, "countrycode": countrycode, "PLZ": PLZ})

        else:
            return jsonify({"error": "no updated tariff and price data avaialble"}), 400

    elif action == 'manual upload':

        ManualPriceTariffKeys = ["pvtariffAvg", "pvtariffadjust", "pvtariffLoSum", "pvtariffHiSum",
                                 "pvtariffHiWin", "pvtariffLoWin", "pvtariffHi", "pvtariffLow",
                                 "EpriceAvg", "Epriceadjust", "EpriceHi", "EpriceLow",
                                 "nm", "capacitycharge"]
        NonNumericKeys = ["elecsupplier", "LinkToTariff", "profiletype"]
        if 'pricetariff' not in userinput:
            return jsonify({"error": "pricetariff not in request"})
        data = {}
        for mptkeys in ManualPriceTariffKeys:
            data[mptkeys] = userinput['pricetariff'].get(mptkeys, 0)
        for mptkeys in NonNumericKeys:
            data[mptkeys] = userinput['pricetariff'].get(mptkeys, '')

        """    
        if ('EpriceHi' in userinput['pricetariff']):
           EpriceHi = userinput['pricetariff']['EpriceHi']
        else:
           EpriceHi = data['EpriceAvg']
        if ('EpriceLow' in userinput['pricetariff']):
            EpriceLow = userinput['pricetariff']['EpriceLow']
        else:
            EpriceLow = data['EpriceAvg']
        highhours=[8,9,10,11,12,13,14,15,16,17,18,19,20,21]
        lowhours=[0,1,2,3,4,5,6,7,22,23]

        data['EpriceAvg'] = np.mean([EpriceHi,EpriceLow])

        if ('pvtariffHi' in userinput['pricetariff']):
           pvtariffHi = userinput['pricetariff']['pvtariffHi']
        else:
           pvtariffHi = data['pvtariffAvg']
        if ('pvtariffLow' in userinput['pricetariff']):
            pvtariffLow = userinput['pricetariff']['pvtariffLow']
        else:
            pvtariffLow = data['pvtariffAvg']

        EpriceHi = EpriceHi*(1+data['Epriceadjust']/100)
        EpriceLow = EpriceLow*(1+data['Epriceadjust']/100)   
        pvtariffHi = pvtariffHi*(1+data['pvtariffadjust']/100)
        pvtariffLow = pvtariffLow*(1+data['pvtariffadjust']/100)

        data['EpriceHi']=EpriceHi
        data['EpriceLow']=EpriceLow
        data['pvtarifftotal'] = data['pvtariffAvg']*(1+data['pvtariffadjust']/100)
        data['Epricetotal'] = data['EpriceAvg']*(1+data['Epriceadjust']/100)
        # Match automatic uploads
        AutoUploadkeys = ["pvtariffHiWin", "pvtariffHiSum","pvtariffLoWin", "pvtariffLoSum"]        

        for upk in AutoUploadkeys:
            if 'Hi' in upk:
                data[upk] =pvtariffHi
            if 'Lo' in upk:
                data[upk] = pvtariffLow   #data['pvtariffAvg']
        if 'LinkToTariff' in userinput['pricetariff']:
            data['LinkToTariff'] =   userinput['pricetariff']['LinkToTariff']
        else:
             data['LinkToTariff']=""
         """

        if 'EpriceTariffFile' in entity:
            # blobname=USER_UPLOADS+entity['EpriceTariffFile']
            blobname = entity['EpriceTariffFile']

            blob = bucket.get_blob(blobname)
            if blob:
                TariffDic = pickle.loads(blob.download_as_string())
            else:
                TariffDic = {"PVTariffSummary": {}}
                blob = bucket.blob(blobname)
        else:
            blobname = USER_UPLOADS + uniqueid + '_epriceTariff.pkl'
            blob = bucket.blob(blobname)
            TariffDic = {"PVTariffSummary": {}}

        for us in data:
            TariffDic['PVTariffSummary'][us] = data[us]

        year = entity['finparams'].get('Ref_year', str(dt.datetime.now().year))
        YearlyTariff = pd.DataFrame(index=pd.date_range(start='1/1/' + str(year), periods=8760, freq='1H'),
                                    columns=['PVTariff', 'Eprice'])
        # Make Yearly Data from High adn low
        country = entity['Location'].get('countrycode', 'CHE')
        if country == 'BRA':
            Multiplier = 1
        elif country == 'CHE':
            Multiplier = 0.01
            for k, v in data.items():
                if ((('Eprice' in k) or ('pvtariff' in k))):
                    data[k] = v * Multiplier
        else:
            Multiplier = 1

        YearlyTariff = Sup.CHTariffGetter.makeTariff(data, year=year)
        # All Yearly data in terms of Currency/kWh
        # Capacity Charge also in Currency/kW/month

        # YearlyTariff['PVTariff'] = data['pvtarifftotal']
        # for hihours in highhours:    
        #     YearlyTariff.loc[(YearlyTariff.index.hour == hihours),'Eprice'] =  EpriceHi*Multiplier
        #     YearlyTariff.loc[(YearlyTariff.index.hour == hihours),'PVTariff'] =  pvtariffHi*Multiplier
        # for lohours in lowhours:       
        #     YearlyTariff.loc[(YearlyTariff.index.hour == lohours),'Eprice']= EpriceLow*Multiplier
        #     YearlyTariff.loc[(YearlyTariff.index.hour == lohours),'PVTariff'] =  pvtariffLow*Multiplier

        TariffDic.update({"YearlyTariff": YearlyTariff})
        # ForGraph = {"hours":[x for x in range(24)],"pvtariff":[TariffDic['PVTariffSummary']['pvtariffAvg']*(1+TariffDic['PVTariffSummary']['pvtariffadjust']/100) for x in range(24)],
        #                      "EpriceAvg":[TariffDic['PVTariffSummary']['EpriceAvg']*(1+TariffDic['PVTariffSummary']['Epriceadjust']/100) for x in range(24)]}
        ForGraph = {"hours": [x for x in range(24)],
                    "pvtariff": [p for p in YearlyTariff['PVTariff'].iloc[:24].values / Multiplier],
                    "Eprice": [p for p in YearlyTariff['Eprice'].iloc[:24].values / Multiplier]}

        TariffDic.update({"ValsForGraph": ForGraph})

        blob = bucket.blob(blobname)
        blob.upload_from_string(pickle.dumps(TariffDic))
        entity.update({"EpriceTariffFile": blobname})
        # else:
        #     blobname=USER_UPLOADS + uniqueid+'_epriceTariff.pkl'
        #     blob = bucket.blob(blobname)
        #     TariffDic = {"PVTariffSummary":{}}

        #     for us in data:     
        #         TariffDic['PVTariffSummary'][us] = data[us]
        #     year = "2021"    
        #     YearlyTariff = pd.DataFrame(index=pd.date_range(start='1/1/'+year,periods=8760,freq='1H'),columns=['PVTariff'])
        #     YearlyTariff['PVTariff']=data['pvtarifftotal']
        #     TariffDic.update({"YearlyTariff":YearlyTariff})
        #     ForGraph = {"hours":[x for x in range(24)],"pvtariff":[TariffDic['PVTariffSummary']['pvtariffAvg']*(1+TariffDic['PVTariffSummary']['pvtariffadjust']/100) for x in range(24)],
        #                          "EpriceAvg":[TariffDic['PVTariffSummary']['EpriceAvg']*(1+TariffDic['PVTariffSummary']['Epriceadjust']/100) for x in range(24)]}

        #     TariffDic.update({"ValsForGraph":ForGraph})     

        #     blob.upload_from_string(pickle.dumps(TariffDic))
        #     #entity.update({"EpriceTariffFile":uniqueid+'_epriceTariff.pkl'})
        #     entity.update({"EpriceTariffFile":blobname})

        ds.put(entity)
        return jsonify({"success": "manual tariff values uploaded", "ValsForGraph": ForGraph}), 200

    entity.update({"status": "Tariff Details"})
    ds.put(entity)

    return jsonify(OutputDic), 200


@app.route('/bom', methods=['POST'])
@jwt_authenticated
def bom():
    OutputDic = {}
    # read entry

    ds = datastore.Client()
    gcs = storage.Client()
    bucket = gcs.get_bucket(BUCKET_NAME)
    req_json = request.get_json()['BoM']

    userinput = req_json
    userinput['email'] = request.email
    orgname = request.org
    parent_key = ds.key('users', userinput['email'])
    message = {}

    entity = ds.get(key=ds.key('Project', userinput['project_id'], parent=parent_key))
    if not entity:
        message.update({"error": "No project with username {} and project {} exists".format(userinput['email'],
                                                                                            userinput['project_id'])})
        return jsonify(message), 400
    ######## CHANGE USERS DB ###########

    # qur = ds.query(kind='organisation')
    # qur.add_filter('users','=',userinput['email'])
    # Sol=list(qur.fetch())
    # if Sol != []:
    #     if os.environ['DB_NAME']!=Sol[0]['compdb']:
    #         os.environ['DB_NAME']= Sol[0]['compdb']
    #     orgname =  Sol[0].key.name 
    # else:        
    #     os.environ['DB_NAME'] = 'refdata' 
    #     orgname = 'default'
    orgentity = ds.get(key=ds.key('organisation', orgname))
    os.environ['DB_NAME'] = orgentity.get("compdb", "refdata")
    # Organisation specific storage areas
    OrgUID = "OrgUID_" + orgname + "_"
    uniqueid = "Uid_" + userinput['email'] + "_" + "Pid" + "_" + userinput['project_id'] + "_"

    importlib.reload(CDB)
    OrgInfo = json.loads(bucket.get_blob(AUXFILES + "organisation_info.json").download_as_string())
    # OrgDict = OrgInfo.get(orgname,OrgInfo['agrola'])
    OrgDict = {"bominput": orgentity.get('bominput', OrgInfo['agrola'])}
    OrgBomInput = OrgDict.get('bominput', {})
    # orgentity = ds.get(key=ds.key('organisation',orgname))

    uschklist = ['action']
    for us in uschklist:
        if us not in userinput:
            return jsonify({"error": "{} not set in input request".format(us)}), 400
    entchklist = ['Demand', 'Location']
    for ent in entchklist:
        if ent not in entity:
            return jsonify({"error": "{} not set in datastore".format(ent)}), 400

    params = userinput['params']
    if userinput[
        'action'] == "getentry":  # Get values from component database based on searching the values of an attribute
        paramchk = ['tablename', 'attribute', 'search']
        for pars in params:
            if pars not in paramchk:
                return jsonify({"error": "{} not set in params".format(pars)}), 400
        Df = CDB.GetEntry(params['tablename'], params['attribute'],
                          params['search'])  # Get values from Component database

        ########## Drop Inverter Sizes Above the kW ############
        Df.loc[~Df['PerfModelKey'].isnull(), 'PerfModelKey'] = Df.loc[
            ~Df['PerfModelKey'].isnull(), 'PerfModelKey'].apply(lambda x: x.replace('\ufeff', '')).values
        kW = entity['Demand'].get('ACpower', 0)
        inverterlist = Df.loc[Df['PerfModelKey'].apply(lambda x: x in INVDF.columns), 'PerfModelKey'].values
        if len(inverterlist) > 0:
            for invname in inverterlist:
                if INVDF[invname].Paco / 1000 > kW:
                    Df.drop(labels=Df[Df['PerfModelKey'] == invname].index, inplace=True)
        ##### ONLY FOR INVERTER FILTERING HERE #########################3
        Df.fillna(np.nan, inplace=True)
        Df.replace([np.nan], [None], inplace=True)
        OutputDic.update(Df[['Name', 'Cost', 'Currency', 'PerfModelKey', 'Description', 'SystemType', 'Id']].to_dict())
    elif userinput['action'] == 'getentrytemplate':
        paramchk = ['ProductName', 'GroupName', 'search']
        for pars in params:
            if pars not in paramchk:
                return jsonify({"error": "{} not set in params".format(pars)}), 400
        productname = params['ProductName']
        groupname = params['GroupName']
        searchstr = params['search']
        TempDf = CDB.GetQuotefromtemplate(productname)

        ########## Drop Inverter Sizes Above the kW ############
        TempDf.loc[~TempDf['PerfModelKey'].isnull(), 'PerfModelKey'] = TempDf.loc[
            ~TempDf['PerfModelKey'].isnull(), 'PerfModelKey'].apply(lambda x: x.replace('\ufeff', '')).values
        kW = entity['Demand'].get('ACpower', 0)
        inverterlist = TempDf.loc[TempDf['PerfModelKey'].apply(lambda x: x in INVDF.columns), 'PerfModelKey'].values
        if len(inverterlist) > 0:
            for invname in inverterlist:
                if INVDF[invname].Paco / 1000 > kW:
                    TempDf.drop(labels=TempDf[TempDf['PerfModelKey'] == invname].index, inplace=True)
        ##### ONLY FOR INVERTER FILTERING HERE #########################3

        TempDf['ProjectID'] = userinput['project_id']
        TempDf.rename(columns={"GroupName": "Group"}, inplace=True)
        # make regex parameter

        searchstr = searchstr.replace("%", ".*")

        return jsonify(
            TempDf.loc[(TempDf['Group'] == groupname) & (TempDf['Name'].str.contains(searchstr, case=False)), ["Name",
                                                                                                               "Group",
                                                                                                               "Currency",
                                                                                                               "Cost",
                                                                                                               "Unit",
                                                                                                               "Id",
                                                                                                               "PriceAdjustment"]].to_dict()), 200

    elif userinput['action'] == "addentry":
        # if 'quoteparams' not in userinput:
        #     return jsonify({"error": "Quote and Rev Ids missing"}),400

        if 'filename' in params:
            if orgentity.get('admins'):
                if userinput['email'] not in orgentity['admins']:
                    return jsonify({"error": "Unauthorized - only admins may upload"}), 400
            fname = params['filename']
            bucket.get_blob(BOM + fname).download_to_filename(fname)
            if fname.endswith('xlsx'):
                entrydf = pd.read_excel(fname, engine='openpyxl')
            elif fname.endswith('csv'):
                entrydf = pd.read_csv(fname)
            elif fname.endswith('json'):
                entrydf = pd.read_json(fname, orient='split')
            else:
                return jsonify({"error": "filename {} format invalid".format(fname)}), 400
            os.remove(fname)
            entrydf.drop(labels=entrydf[entrydf['Name'].isnull()].index, axis=0, inplace=True)
            entrydf.fillna(np.nan, inplace=True)
            entrydf.replace([np.nan], [None], inplace=True)
        else:
            # Allow upload of raw json files
            entrydf = pd.DataFrame(params).transpose()
            entrydf.fillna(np.nan, inplace=True)
            entrydf.replace([np.nan], [None], inplace=True)
            # return jsonify({"error":"upload filename missing"}),400
        Id = CDB.AddEntry(entrydf)
        OutputDic.update({"Id": Id})
        # OutputDic.update(entrydf[['Name','Value','Currency','PerfModelKey','Description','Category']].to_dict())
    elif userinput['action'] == "addtotemplates":

        if orgentity.get('admins'):
            if userinput['email'] not in orgentity['admins']:
                return jsonify({"error": "Unauthorized - only admins may upload"}), 400
        if 'filename' in params:
            fname = params['filename']
            bucket.get_blob(BOM + fname).download_to_filename(fname)
            ProdDb = pd.DataFrame()
            BigDb = pd.DataFrame()

            if fname.endswith('xlsx'):
                Raw = pd.read_excel(fname, skiprows=1, engine='openpyxl')
                ProdNames = pd.read_excel(fname, nrows=0, engine='openpyxl')
                Names = [n for n in ProdNames.columns if 'Unnamed' not in n]
                for Name, colnum in zip(Names, range(6, len(Names) * 3 + 6, 3)):
                    ProdDb = Raw.iloc[:, :4]
                    ProdDb['ProductName'] = Name
                    ProdDb.loc[:, 'ActiveFlag'] = Raw.iloc[:, colnum]
                    ProdDb.loc[:, 'PriorityFlag'] = Raw.iloc[:, colnum + 2]
                    ProdDb.loc[:, 'PriceAdjustment'] = Raw.iloc[:, colnum + 1]
                    id2drop = ProdDb[ProdDb['BusinessIdentifierCode'].isnull()].index
                    ProdDb.drop(index=id2drop, inplace=True)
                    BigDb = pd.concat([BigDb, ProdDb], axis=0)
                BigDb.fillna(0, inplace=True)
            elif fname.endswith('csv'):
                entrydf = pd.read_csv(fname)
            # elif fname.endswith('json'):
            #     entrydf = pd.read_json(fname,orient='split')
            else:
                return jsonify({"error": "filename {} format invalid".format(fname)}), 400
            os.remove(fname)
            entrydf = BigDb

            entrydf.drop(labels=entrydf[entrydf['BusinessIdentifierCode'].isnull()].index, axis=0, inplace=True)
            entrydf.fillna(np.nan, inplace=True)
            entrydf.replace([np.nan], [None], inplace=True)
            # Add ActiveFlag for templates
            EntDict = entrydf[['ProductName', 'GroupName', 'BusinessIdentifierCode',
                               'PriceAdjustment', 'PriorityFlag', 'ActiveFlag']].to_dict(orient='records')
            CDB.InsertOrUpdate(EntDict)

        else:
            return jsonify({"error": "no filename in params"}), 400
    elif userinput["action"] == "getexcel":
        # Check if restricted organisation where downloads are also not allowed
        if orgentity.get('displaypermissions'):  # CHECK - if 'Cost and Price Overview' in user access rights
            if orgentity.get('admins'):
                if userinput['email'] not in orgentity['admins']:
                    return jsonify({"error": "Unauthorized - only admins may download"}), 400
        if 'report' in params:
            report = params['report']
        else:
            report = "SYSTEM"
        if report == "SYSTEM":
            # querytext = "SELECT * FROM SYSTEM WHERE ActiveFlag=1"
            querytext = "SELECT sy0.* FROM SYSTEM sy0 \
                            RIGHT JOIN  \
                        (SELECT MAX(s.Id) as MaxId, s.BusinessIdentifierCode as BC \
                         FROM SYSTEM s GROUP BY BC) as p \
                        ON p.MaxId = sy0.Id  \
                        WHERE p.BC is not null \
                        ORDER by sy0.BusinessIdentifierCode"

            Df2Excel = CDB.DfFromRawSQLQuery(querytext)
            # Df2Excel.sort_values(by='BusinessIdentifierCode',inplace=True)
            Df2Excel['ActiveFlag'] = Df2Excel['ActiveFlag'].apply(lambda x: int.from_bytes(x, byteorder='big'))
            # localfname = "MasterListExtract.xlsx"
            fnameorder = "MasterListExtract.xlsx"
            savefilename = "Uid_" + userinput['email'] + "_MasterListExtract.xlsx"
            blobfname = "BoM/ExcelExports/" + savefilename
            cwd = os.getcwd()

            try:
                with open(os.path.join(cwd, fnameorder), "wb") as f:
                    bucket.get_blob(blobfname.replace(savefilename, fnameorder)).download_to_file(f)
            except:
                return jsonify({"err": blobfname.replace(savefilename, fnameorder)}), 400
            # Organise columns in order from previous stored file
            # Organise columns in this order first
            oldfile = pd.read_excel(os.path.join(cwd, fnameorder), nrows=2, engine='openpyxl')
            columnorder = oldfile.columns
            Df2Excel1 = Df2Excel.reindex(columns=columnorder)
            # with open(os.path.join(cwd,localfname),"wb") as f:
            #     Df2Excel1.to_excel(f,index=False)  
            writer = pd.ExcelWriter(os.path.join(cwd, savefilename), engine='openpyxl')
            Df2Excel1.to_excel(writer, index=False)
            writer.save()
            with open(os.path.join(cwd, savefilename), "rb") as f:
                bucket.blob(blobfname).upload_from_file(f)
            if os.path.exists(os.path.join(cwd, savefilename)):
                os.remove(os.path.join(cwd, savefilename))
            urltodownload = "https://www.googleapis.com/storage/v1/b/" + BUCKET_NAME + "/o/" + \
                            blobfname.replace('/', '%2F') + "?alt=media"

        elif report == 'PRODUCT_GROUP':
            querytext = "SELECT \
                            pd.*, \
                            s.Name \
                            FROM  PRODUCT_GROUP pd \
                            INNER JOIN SYSTEM s	on pd.BusinessIdentifierCode = s.BusinessIdentifierCode \
                            AND s.ActiveFlag=1 AND pd.ActiveFlag=1"

            Df2Excel = CDB.DfFromRawSQLQuery(querytext)
            Df2Excel.sort_values(by=['ProductName', 'GroupName', 'Name'], inplace=True)
            # Df2Excel['ActiveFlag']=Df2Excel['ActiveFlag'].apply(lambda x:int.from_bytes(x,byteorder='big'))

            Df2Excel = Df2Excel[
                ['ProductName', 'GroupName', 'BusinessIdentifierCode', 'Name', 'PriceAdjustment', 'PriorityFlag',
                 'ActiveFlag']]
            productnames = set(Df2Excel['ProductName'].values)
            localfname = "{}_ProductListExtract.xlsx".format(uniqueid)

            blobfname = "BoM/ExcelExports/" + localfname
            cwd = os.getcwd()
            # Write the file
            writer = pd.ExcelWriter(os.path.join(cwd, localfname), engine='openpyxl')
            for prodnames in productnames:
                Df2Excel[Df2Excel['ProductName'] == prodnames].to_excel(writer, sheet_name=prodnames, index=False)
            writer.save()
            # Upload to bucket
            with open(os.path.join(cwd, localfname), "rb") as f:
                bucket.blob(blobfname).upload_from_file(f)
            if os.path.exists(os.path.join(cwd, localfname)):
                os.remove(os.path.join(cwd, localfname))
            urltodownload = "https://www.googleapis.com/storage/v1/b/" + BUCKET_NAME + "/o/" + \
                            blobfname.replace('/', '%2F') + "?alt=media"

        elif report == 'BoMQuote':
            QuoteID = params['QuoteID']
            RevID = params['RevID']
            ProjectID = userinput['project_id']
            ViewQuoteDf = CDB.GetBoMQuote(ProjectID, QuoteID, RevID)

            ViewQuoteDf.fillna(np.nan, inplace=True)
            ViewQuoteDf.replace([np.nan], [None], inplace=True)
            ViewQuoteDf['Quantity'] = ViewQuoteDf['Quantity'].apply(lambda x: float(x) if x else x)
            ViewQuoteDf['ActiveFlag'] = ViewQuoteDf['ActiveFlag'].apply(lambda x: int.from_bytes(x, byteorder='big'))
            ViewQuoteDf.fillna(np.nan, inplace=True)
            ViewQuoteDf.replace([np.nan], [None], inplace=True)
            ToExcel = ViewQuoteDf[(ViewQuoteDf['Quantity'] != 0) & (~ViewQuoteDf['Quantity'].isnull())]

            localfname = "{}_BoMQuoteExtract.xlsx".format(uniqueid)
            blobfname = "BoM/" + "Quotes/" + localfname
            cwd = os.getcwd()
            with open(os.path.join(cwd, localfname), "wb") as f:
                ToExcel.to_excel(f, index=False)
            with open(os.path.join(cwd, localfname), "rb") as f:
                bucket.blob(blobfname).upload_from_file(f)
            if os.path.exists(os.path.join(cwd, localfname)):
                os.remove(os.path.join(cwd, localfname))
            urltodownload = "https://www.googleapis.com/storage/v1/b/" + BUCKET_NAME + "/o/" + \
                            blobfname.replace('/', '%2F') + "?alt=media"
        elif report == 'custom':
            import addonModules.custommods.ExcelExports as CExl
            customreport = params['customreport']
            QuoteID = params['QuoteID']
            RevID = params['RevID']
            ProjectID = userinput['project_id']
            ExcelReport = CExl.ExcelReport(CDB)
            df = getattr(ExcelReport, customreport)(ProjectID, QuoteID, RevID)

            localfname = customreport + "_.xlsx"
            blobfname = "BoM/" + "Quotes/" + uniqueid + localfname
            urltodownload = ExcelReport.excel2download(df, bucket, blobfname)

        return jsonify({"urltodownload": urltodownload})



    elif userinput["action"] == "getinputfilters":
        # OrgDict=json.loads(bucket.get_blob(AUXFILES+"organisation_info.json").download_as_string())
        # s=re.compile("(?<=@)[^.]+(?=\.)")
        # userstring = s.findall(userinput['email'])
        # # TODO: Domain from org directory
        # if len(userstring)>0:
        #     domain = userstring[0]
        # else:
        #     domain=""
        with ds.transaction():
            orgentity = ds.get(key=ds.key('organisation', orgname))
            parent_key = ds.key('users', userinput['email'])
            entity = ds.get(key=ds.key('Project', userinput['project_id'], parent=parent_key))
            # TODO: TEMPORARY, CHANGE to OrgDict[domain]
            tiltareas = entity['Location']['tilt']
            flatcheck = [t for t in tiltareas.values() if t != 0]  # list of non-flat roofs
            if len(flatcheck) == 0:
                rooftilt = 'flat'
            else:
                rooftilt = 'nonflat'
            OrgDict = {"bominput": orgentity.get('bominput', OrgInfo['agrola'])}

            # if orgname in OrgDict:
            OutputDic = OrgDict['bominput']
            if 'roof' in OutputDic:
                OutputDic.update({"rooftilt": OutputDic['roof'][rooftilt]['rooftilt']})
                OutputDic.update({"rooftype": OutputDic['roof'][rooftilt]['rooftype']})

            if "groupfilters" in OrgDict:
                OutputDic.update({"groupfilters": OrgDict['groupfilters']})
            # else:
            #     OutputDic=OrgDict['agrola']['bominput']
            #     OutputDic.update({"rooftilt": OutputDic['roof'][rooftilt]['rooftilt']})
            #     OutputDic.update({"rooftype": OutputDic['roof'][rooftilt]['rooftype']})
            #     if "groupfilters" in OrgDict['agrola']:
            #         OutputDic.update({"groupfilters":OrgDict['Agrola']['groupfilters']})

            if 'ActiveQuote' in entity:
                if entity['ActiveQuote'].get('QuoteID'):
                    OutputDic.update({"QuoteID": entity['ActiveQuote'].get('QuoteID')})

                else:
                    NewQuoteID = int(orgentity['bominput'].get('QuoteAutoIncUID', '100000')) + 1
                    orgentity['bominput'].update({'QuoteAutoIncUID': NewQuoteID})

                    OutputDic.update({"QuoteID": NewQuoteID})
            else:
                NewQuoteID = int(orgentity['bominput'].get('QuoteAutoIncUID', '100000')) + 1
                orgentity['bominput'].update({'QuoteAutoIncUID': NewQuoteID})
                OutputDic.update({"QuoteID": NewQuoteID})
            ds.put(orgentity)

        return jsonify({"success": OutputDic}), 200




    elif userinput['action'] == "makebom":

        ProjectID = userinput['project_id']
        # TODO: Generate Quote and Revision ID
        QuoteID = userinput['quoteparams']['QuoteID']
        RevID = userinput['quoteparams']['RevID']
        if entity.get('ActiveQuote'):
            entity['ActiveQuote'].update({"QuoteID": QuoteID})
            entity['ActiveQuote'].update({"RevID": RevID})
        else:
            entity.update({'ActiveQuote': {"QuoteID": QuoteID, "RevID": RevID}})

        bomquotedic = params
        # Make bomquote into parameterdf['ProjectID','QuoteID','RevID','Type','CreatedBy','Group','TypeID']
        parameterdf = pd.DataFrame(columns=['ProjectID', 'QuoteID', 'RevID', 'Type', 'CreatedBy', 'Group', 'TypeID'])
        # parameterdf.loc[range(np.sum([len(i) for i in bomquotedic.values())]),
        #                 ["ProjectID","QuoteID","RevID","CreatedBy"]] = [ProjectID,QuoteID,RevID,userinput['email']]
        parameterdf = parameterdf.loc[~parameterdf.duplicated(subset=['TypeID'])]

        i = 0
        for groups in bomquotedic:
            for entries in bomquotedic[groups]:
                parameterdf.loc[i, ["ProjectID", "QuoteID", "RevID", "CreatedBy"]] = [ProjectID, QuoteID, RevID,
                                                                                      userinput['email']]
                parameterdf.loc[i, ['Group', 'Type', 'TypeID']] = [groups, entries["tablename"], int(entries['Id'])]
                if 'PriceAdjustment' in entries:  # Remove condition later
                    parameterdf.loc[i, ['PriceAdjustment']] = entries['PriceAdjustment']

                i += 1
        parameterdf.fillna(np.nan, inplace=True)
        parameterdf.replace([np.nan], [None], inplace=True)

        CDB.MakeBoMQuote(parameterdf)
        OutputDic.update((parameterdf.to_dict()))
        entity.update({"status": "Components"})

    elif userinput['action'] == "viewbom":
        ProjectID = userinput['project_id']
        if "QuoteID" in params:
            QuoteID = params['QuoteID']
            RevID = params['RevID']
            if entity.get('ActiveQuote'):
                entity['ActiveQuote'].update({"QuoteID": QuoteID})
                entity['ActiveQuote'].update({"RevID": RevID})

            ViewQuoteDf = CDB.GetBoMQuote(ProjectID, QuoteID, RevID)

            ViewQuoteDf.fillna(np.nan, inplace=True)
            ViewQuoteDf.replace([np.nan], [None], inplace=True)

            ViewQuoteDf['Quantity'] = ViewQuoteDf['Quantity'].apply(lambda x: float(x) if x else x)

            ViewQuoteDf.fillna(np.nan, inplace=True)
            ViewQuoteDf.replace([np.nan], [None], inplace=True)

            ViewQuoteDf = CDB.BoMExcludeFlags(ViewQuoteDf, OrgBomInput=OrgBomInput)

            OutputDic.update(json.loads(json.dumps(ViewQuoteDf.loc[(ViewQuoteDf['Quantity'] != 0),
                                                                   ["ProjectID", "QuoteID", "RevID", "Name",
                                                                    "DisplayName", "Description",
                                                                    "Group", "Currency", "Cost", "Unit", "ID", "Id",
                                                                    "Quantity", "TotalCost", "UnitCost", "TotalPrice",
                                                                    "PriceAdjustment", "NetPrice", "BasePrice",
                                                                    "TotalBasePrice",
                                                                    "ExcludeFromCAPEX", "IncludeforBC"]].to_dict())))


        elif "fromtemplate" in params:

            productname = params['fromtemplate']
            if 'ActiveQuote' in entity:
                entity['ActiveQuote'].update({"ProductName": productname})
            else:
                entity.update({"ActiveQuote": {"ProductName": productname}})
            ViewQuoteDf = CDB.GetQuotefromtemplate(productname)
            ViewQuoteDf['Quantity'] = None
            ViewQuoteDf['TotalCost'] = None
            ViewQuoteDf['ProjectID'] = userinput['project_id']
            ViewQuoteDf.rename(columns={"GroupName": "Group"}, inplace=True)
            ViewQuoteDf['Quantity'] = ViewQuoteDf['Quantity'].apply(lambda x: int(x) if x else x)
            # Eliminate items based on kW or segment selection 
            # Calculate Quantities if information is available
            unitvariable = 'UnitDependantVariable'
            unitfunction = 'UnitDependantVariableFunction'
            costfunction = 'CostFunction'
            costvariable = 'CostFunctionDependentVariable'
            if 'ACpower' in entity['Demand']:
                kW = entity['Demand']['ACpower']
            else:
                kW = 0
            # Roof Flat or Tilt
            tilted = 0
            for ar in entity['Location']['tilt']:
                if entity['Location']['tilt'][ar] != 0:
                    tilted = 1
            if 'AreaFigures' in entity['Location']:
                if 'TotalDCInOut' in entity['Location']['AreaFigures']:
                    TotalDCInOut = entity['Location']['AreaFigures']['TotalDCInOut']
                else:
                    TotalDCInOut = 5
            else:
                TotalDCInOut = 5
            DependantVar = {"kW": kW, "tilt": tilted, "TotalDCInOut": TotalDCInOut}
            ns = vars(math).copy()
            xvars = {}
            for row in ViewQuoteDf.index:
                if ViewQuoteDf.loc[row, unitvariable]:
                    UnitVariables = ViewQuoteDf.loc[row, unitvariable].split(",")
                    # an equation can be made of x, x1, x2.... etc
                    for Vars, num in zip(UnitVariables, range(len(UnitVariables))):
                        if num == 0:
                            if Vars.strip() in DependantVar:
                                xvars["x"] = DependantVar[Vars.strip()]
                            else:
                                xvars["x"] = np.nan
                                ViewQuoteDf.loc[row, unitfunction] = "x"
                        else:
                            if Vars.strip() in DependantVar:
                                xvars["x" + str(num)] = DependantVar[Vars.strip()]
                            else:
                                xvars["x" + str(num)] = np.nan
                                ViewQuoteDf.loc[row, unitfunction] = "x" + str(num)

                    evalstr = str(ViewQuoteDf.loc[row, unitfunction])

                    ViewQuoteDf.loc[row, 'Quantity'] = eval(evalstr, ns, xvars)
                    ViewQuoteDf.loc[row, 'TotalCost'] = ViewQuoteDf.loc[row, 'Quantity']

            ViewQuoteDf.fillna(np.nan, inplace=True)
            ViewQuoteDf.replace([np.nan], [None], inplace=True)

            # Get Filters

            # OrgDict=json.loads(bucket.get_blob(AUXFILES+"organisation_info.json").download_as_string())
            s = re.compile("(?<=@)[^.]+(?=\.)")
            userstring = s.findall(userinput['email'])

            if len(userstring) > 0:
                domain = userstring[0]
            else:
                domain = ""

            # TODO: TEMPORARY, CHANGE to OrgDict[domain]
            tiltareas = entity['Location']['tilt']
            flatcheck = [t for t in tiltareas.values() if t != 0]  # list of non-flat roofs
            if len(flatcheck) == 0:
                rooftilt = 'flat'
            else:
                rooftilt = 'nonflat'

            if 'bominputfilter' in entity:
                bominputfilter = entity['bominputfilter']
            else:
                bominputfilter = {}

            if 'groupfilters' in OrgDict['bominput']:
                groupfilters = OrgDict['bominput']['groupfilters']
                for groupname in groupfilters.keys():
                    searchstring = ".*".join(
                        [bominputfilter[x] for x in OrgDict['bominput']['groupfilters'][groupname] if
                         x in bominputfilter])  # Regex structure to allow string searches
                    if searchstring:
                        ViewQuoteDf.loc[(ViewQuoteDf['Group'] == groupname) & (
                            ViewQuoteDf['Name'].str.contains(".*" + searchstring + ".*")), ['PriorityFlag']] = "1"

            """ if domain in OrgDict:
                if "groupfilters" in OrgDict['Agrola']:
                    groupfilters = OrgDict['Agrola']['groupfilters']
                    for groupname in groupfilters.keys():
                        searchstring = ".*".join([bominputfilter[x] for x in OrgDict['Agrola']['groupfilters'][groupname] if x in bominputfilter]) # Regex structure to allow string searches
                        if searchstring:
                            ViewQuoteDf.loc[(ViewQuoteDf['Group']==groupname)&(ViewQuoteDf['Name'].str.contains(".*"+searchstring+".*")),['PriorityFlag']]="1"   
            else:
               if "groupfilters" in OrgDict['Agrola']:
                    groupfilters = OrgDict['Agrola']['groupfilters']
                    for groupname in groupfilters.keys():
                        searchstring = ".*".join([bominputfilter[x] for x in OrgDict['Agrola']['groupfilters'][groupname] if x in bominputfilter]) # Regex structure to allow string searches
                        if searchstring:
                            ViewQuoteDf.loc[(ViewQuoteDf['Group']==groupname)&(ViewQuoteDf['Name'].str.contains(".*"+searchstring+".*")),['PriorityFlag']]="1" """

            ViewQuoteDf.loc[ViewQuoteDf['Quantity'] > 0, 'PriorityFlag'] = "1"
            # Add Lookup for System-Accessory Pairs here, set those to PriorityFlag

            # System: Inverter
            # Remove those values above the kW                           
            ########## Drop Inverter Sizes 80% Above the selected kW ############
            kW = entity['Demand'].get('ACpower', 0) / 0.8
            ViewQuoteDf.loc[~ViewQuoteDf['PerfModelKey'].isnull(), 'PerfModelKey'] = ViewQuoteDf.loc[
                ~ViewQuoteDf['PerfModelKey'].isnull(), 'PerfModelKey'].apply(lambda x: x.replace('\ufeff', '')).values
            inverterlist = ViewQuoteDf.loc[
                ViewQuoteDf['PerfModelKey'].apply(lambda x: x in INVDF.columns), 'PerfModelKey'].values
            if len(inverterlist) > 0:
                for invname in inverterlist:
                    if INVDF[invname].Paco / 1000 > kW:
                        ViewQuoteDf.drop(labels=ViewQuoteDf[ViewQuoteDf['PerfModelKey'] == invname].index, inplace=True)
            ##########################################################

            ViewQuoteDf = CDB.BoMExcludeFlags(ViewQuoteDf, OrgBomInput=OrgBomInput)

            OutputDic.update(json.loads(json.dumps(ViewQuoteDf.loc[(ViewQuoteDf['Quantity'] != 0),
                                                                   ["ProjectID", "Name", "DisplayName", "Description",
                                                                    "Group", "Currency", "Cost", "Unit", "Id",
                                                                    "Quantity", "TotalCost", "PriorityFlag",
                                                                    "PriceAdjustment",
                                                                    "ExcludeFromCAPEX", "IncludeforBC"]].to_dict())))
            entity.update({"status": "Components"})
    elif userinput['action'] == 'updatebom':
        ProjectID = userinput['project_id']
        if 'NewRevID' in params:
            QuoteID = params['QuoteID']
            RevID = params['RevID']
            if entity.get('ActiveQuote'):
                entity['ActiveQuote'].update({"QuoteID": QuoteID})
                entity['ActiveQuote'].update({"RevID": RevID})
            else:
                entity.update({'ActiveQuote': {"QuoteID": QuoteID, "RevID": RevID}})

            ViewQuoteDf = CDB.StoreRevtoBoM(ProjectID, QuoteID, RevID, userinput['email'], params['NewRevID'])
            ViewQuoteDf.fillna(np.nan, inplace=True)
            ViewQuoteDf.replace([np.nan], [None], inplace=True)
            ViewQuoteDf = CDB.BoMExcludeFlags(ViewQuoteDf, OrgBomInput=OrgBomInput)
            OutputDic.update(json.loads(json.dumps(ViewQuoteDf.loc[(ViewQuoteDf['Quantity'] != 0),
                                                                   ["ProjectID", "QuoteID", "RevID", "Name",
                                                                    "DisplayName", "Description",
                                                                    "Group", "Currency", "Cost", "Unit", "ID", "Id",
                                                                    "Quantity", "TotalCost", "UnitCost", "TotalCost",
                                                                    "TotalPrice", "BasePrice",
                                                                    "PriceAdjustment", "NetPrice", "TotalBasePrice",
                                                                    "ExcludeFromCAPEX", "IncludeforBC"]].to_dict())))
            entity['ActiveQuote'].update({"RevID": params['NewRevID']})
            entity.update({"status": "Cost & Price Overview"})
        elif 'quoteparams' in userinput:
            # Fetch existing BoM
            QuoteID = userinput['quoteparams']['QuoteID']
            RevID = userinput['quoteparams']['RevID']
            if entity.get('ActiveQuote'):
                entity['ActiveQuote'].update({"QuoteID": QuoteID})
                entity['ActiveQuote'].update({"RevID": RevID})
            else:
                entity.update({'ActiveQuote': {"QuoteID": QuoteID, "RevID": RevID}})
            ViewCurrBoMDf = CDB.GetBoMQuote(ProjectID, QuoteID, RevID)

            bomquotedic = params
            # Make bomquote into parameterdf['ProjectID','QuoteID','RevID','Type','CreatedBy','Group','TypeID']
            parameterdf = pd.DataFrame(
                columns=['ProjectID', 'QuoteID', 'RevID', 'Type', 'CreatedBy', 'Group', 'TypeID'])
            # parameterdf.loc[range(np.sum([len(i) for i in bomquotedic.values())]),
            #                 ["ProjectID","QuoteID","RevID","CreatedBy"]] = [ProjectID,QuoteID,RevID,userinput['email']]

            i = 0
            for groups in bomquotedic:
                for entries in bomquotedic[groups]:
                    parameterdf.loc[i, ["ProjectID", "QuoteID", "RevID", "CreatedBy"]] = [ProjectID, QuoteID, RevID,
                                                                                          userinput['email']]
                    parameterdf.loc[i, ['Group', 'Type', 'TypeID']] = [groups, entries["tablename"], int(entries['Id'])]

                    if 'PriceAdjustment' in entries:  # Remove condition later
                        parameterdf.loc[i, ['PriceAdjustment']] = entries['PriceAdjustment']

                    i += 1
            parameterdf.fillna(np.nan, inplace=True)
            parameterdf.replace([np.nan], [None], inplace=True)
            parameterdf = parameterdf.loc[~parameterdf.duplicated(subset=['TypeID'])]

            IDinBoM = list(set(parameterdf['TypeID'].values))

            IDsToDelete = [int(ViewCurrBoMDf.loc[index, 'Id']) for index in ViewCurrBoMDf.index if
                           ViewCurrBoMDf.loc[index, 'ID'] not in IDinBoM]

            if len(IDsToDelete) > 0:
                DeleteIds = [{'_Id': _Id} for _Id in IDsToDelete]
                CDB.DeleteFromBoM(DeleteIds)

            # Delete Ids not in UpdateList

            CDB.MakeBoMQuote(parameterdf)
            # for Up in UpdateList:
            #    Up['_Id'] = Up.pop('Id')
            #    if 'Quantity' in Up:
            #        if Up['Quantity']!=None:
            #            Up['Quantity']= float(Up['Quantity'])
            #    BoM2Update.append(Up)
            # if BoM2Update:   
            #     CDB.UpdateBoM(BoM2Update)
            entity.update({"status": "Components"})
        else:  # An update of the full Cost and price Overview
            BoM2Update = []
            QuoteID = params['QuoteID']
            RevID = params['RevID']
            UpdateList = userinput['params']['data']
            for Up in UpdateList:
                Up['_Id'] = Up.pop('Id')
                if 'Quantity' in Up:
                    if Up['Quantity'] != None:
                        Up['Quantity'] = float(Up['Quantity'])
                BoM2Update.append(Up)
            if BoM2Update:
                CDB.UpdateBoM(BoM2Update)

    elif userinput['action'] == "gettypesof":
        tablename = params['tablename']
        columnname = params['columnname']
        querytext = "SELECT DISTINCT {} FROM {} WHERE ActiveFlag=1 ORDER BY {} ASC;".format(columnname, tablename,
                                                                                            columnname)
        res = CDB.RunRawSQLQuery(querytext)
        OutputDic.update({"Typesof": [r[0] for r in res]})
    elif userinput['action'] == "getquoterev":
        if userinput['params'] == {}:
            querytext = "SELECT `QuoteID`,`RevID` FROM BOM_QUOTE WHERE `ProjectID`='{}' \
                    ORDER BY `CreatedDate` DESC     \
                     LIMIT 1;".format(userinput['project_id'])
            res = CDB.RunRawSQLQuery(querytext)
            if len(res) > 0:
                OutputDic.update({"QuoteID": res[0][0], "RevID": res[0][1]})
            else:
                OutputDic.update({})
        else:
            QuoteID = params['QuoteID']
            querytext = "SELECT DISTINCT RevID FROM BOM_QUOTE WHERE `ProjectID`='{}' and `QuoteID`='{}' \
                     ;".format(userinput['project_id'], QuoteID)

            res = CDB.RunRawSQLQuery(querytext)
            if len(res) > 0:
                OutputDic.update({"RevID": [l[0] for l in res]})
            else:
                OutputDic.update({})
    ds.put(entity)
    return jsonify(OutputDic), 200


@app.route('/techselect-v2', methods=['POST'])
@jwt_authenticated
def techselectv2():
    OutputDict = {}
    ds = datastore.Client()
    gcs = storage.Client()
    bucket = gcs.get_bucket(BUCKET_NAME)
    req_json = request.get_json()['Components']
    userinput = req_json
    userinput['email'] = request.email
    orgname = request.org
    parent_key = ds.key('users', userinput['email'])
    message = {}
    entity = ds.get(key=ds.key('Project', userinput['project_id'], parent=parent_key))
    if not entity:
        message.update({"error": "No project with username {} and project {} exists".format(userinput['email'],
                                                                                            userinput['project_id'])})
        return jsonify(message), 400
    entchecklist = ['Location', 'Weather', 'Demand']
    for ec in entchecklist:
        if ec not in entity:
            return jsonify({"error": "{} not set in datastore".format(ec)}), 400

    DsLocation = entity['Location']
    DsWeather = entity['Weather']

    # Use the Correct DB based on Organisation            
    # qur = ds.query(kind='organisation')
    # qur.add_filter('users','=',userinput['email'])
    # Sol=list(qur.fetch())
    # if Sol != []:
    #     if os.environ['DB_NAME']!=Sol[0]['compdb']:
    #         os.environ['DB_NAME']= Sol[0]['compdb']
    #     orgname =  Sol[0].key.name    
    # else:        
    #     os.environ['DB_NAME'] = 'refdata' 
    #     orgname = 'default'
    orgentity = ds.get(key=ds.key('organisation', orgname))
    os.environ['DB_NAME'] = orgentity.get("compdb", "refdata")
    importlib.reload(CDB)
    # OrgInfo = orgentity['OrgInfo']

    OrgInfo = json.loads(bucket.get_blob(AUXFILES + "organisation_info.json").download_as_string())
    OrgDict = OrgInfo.get(orgname, OrgInfo['Agrola'])
    OrgBomInput = OrgDict.get('bominput', {})

    # TODO: Adapt for multiple areas till then just use 1st area
    # TODO : Put in error message when tilt is missing in Location
    if (('tilt' not in DsLocation) or ('azimuth' not in DsLocation)):
        return jsonify({"error": "tilt or azimuth information missing in Location"})
    if (('latitude' not in DsLocation) or ('longitude' not in DsLocation)):
        return jsonify({"error": "latitude or longitude information missing in Location"})
    if 'finalweatherfile' not in DsWeather:
        return jsonify({"error": "finalweatherfile information missing in Weather"})
    elecDesign = ELPARM
    default_costparams = FINPARM['cost']
    default_finparams = FINPARM['financial']

    tilt = {}
    azimuth = {}
    SurfArea = {}
    try:
        for AreaLabel in DsLocation['tilt']:
            tilt[AreaLabel] = DsLocation['tilt'][AreaLabel]
            azimuth[AreaLabel] = DsLocation['azimuth'][AreaLabel]

            if 'SurfArea' in DsLocation:
                SurfArea[AreaLabel] = DsLocation['SurfArea'][AreaLabel]
            else:
                numAreas = len(DsLocation['tilt'])
                SurfArea[AreaLabel] = DsLocation['TotalPanelArea'] / numAreas
    except:
        tilt['Area1'] = [i for i in DsLocation['tilt'].values()][0]
        azimuth['Area1'] = [i for i in DsLocation['azimuth'].values()][0]
        SurfArea['Area1'] = DsLocation['TotalPanelArea']

    # if 'SpacingParams' in DsLocation:
    #     SpacingParams = DsLocation['SpacingParams'] # {Area1:{Params}, "Area2":{Params}}
    # else:
    #     SpacingParams = elecDesign['systemparams']['SpacingParams'] 
    if 'AreaSpacingParams' in DsLocation:
        AreaSpacingParams = DsLocation['AreaSpacingParams']  # {Area1:{Params}, "Area2":{Params}}
    else:
        AreaSpacingParams = {AreaLabel: elecDesign['systemparams']['SpacingParams'] for AreaLabel in tilt}

    weatherfile = DsWeather['finalweatherfile']
    location_data = Location(DsLocation['latitude'], DsLocation['longitude'], tz=DsWeather['TZ'])
    SolData = pickle.loads(bucket.get_blob(weatherfile).download_as_string())

    # LatPolyfromFile
    GeoCoordFile = entity['Location']['GeoCoords']
    LatPolyfromFile = json.loads(bucket.get_blob(GeoCoordFile).download_as_string())

    uniqueid = 'Uid_' + userinput['email']
    blobname_pvmod = USER_UPLOADS + uniqueid + '_moduledata.pkl'
    blobpv = bucket.get_blob(blobname_pvmod)
    if blobpv:
        USERPVDF = pickle.loads(blobpv.download_as_string())

    Power_req = entity['Demand']['ACpower'] * 1000  # convert to W
    if (Power_req == 0):
        Power_req = 10 * 1000

    # Get Model names from BoM for solar module and inverter     
    QuoteID = userinput['quoteparams']['QuoteID']
    RevID = userinput['quoteparams']['RevID']
    BoMDf = CDB.GetBoMQuote(userinput['project_id'], QuoteID, RevID)
    BoMDf.loc[~BoMDf['PerfModelKey'].isnull(), 'PerfModelKey'] = BoMDf.loc[
        ~BoMDf['PerfModelKey'].isnull(), 'PerfModelKey'].apply(lambda x: x.replace('\ufeff', '')).values
    BoMDf = BoMDf.loc[~BoMDf.duplicated(subset=['ID'])]

    if 'ActiveQuote' in entity:
        entity['ActiveQuote'].update({"QuoteID": QuoteID})
        entity['ActiveQuote'].update({"RevID": RevID})
    else:
        entity.update({"ActiveQuote": {"QuoteID": QuoteID, "RevID": RevID}})

    PVrawlst = [pvname for pvname in list(BoMDf[BoMDf['Type'] == 'Solarmodule']['PerfModelKey'].values)]
    Invrawlst = [invname for invname in list(BoMDf[BoMDf['Type'] == 'Inverter']['PerfModelKey'].values)]

    PVList = []
    InvList = []
    errmsg = " "
    # if ((len(PVrawlst)>3)or(len(Invrawlst)>3)):
    #     return jsonify({"error": "More than 3 components uploaded "}),400
    if len(set(PVrawlst)) != len(PVrawlst):
        return jsonify({"error": "Duplicate names in PV module list"}), 400
    if len(set(Invrawlst)) != len(Invrawlst):
        return jsonify({"error": "Duplicate names in Inverter name list"}), 400

    # Check for the module and inverter names in the  performance modules db
    for p in PVrawlst:
        if p in PVDF.columns:
            PVList.append(p)
        elif p in PVSYSDF.columns:
            PVList.append(p)
        elif blobpv:
            if p in USERPVDF.columns:
                PVList.append(p)
            else:
                return jsonify({"error": "PV module name {} not found in any database".format(p)}), 400
        else:
            return jsonify({"error": "PV module name {} not found in any database".format(p)}), 400

    for i in Invrawlst:
        if i in INVDF.columns:  # IF exact name then take it
            Invname = i
        else:
            score, Invname = \
            sorted({Levenshtein.jaro(x.replace('_', ' '), i): x for x in INVDF.columns}.items(), reverse=True)[
                0]  # If fuzzy then try to get match

            InvData = INVDF.loc[:, Invname]
            errmsg = errmsg + ' - Inverter selected {}\n'.format(InvData.name.replace("_", " "))
            Invname = InvData.name
        InvList.append(Invname)

    readstored = False
    # Deactivate for the time being
    # if ('Components' in entity) & (('refresh') not in userinput):
    #     InvStored = entity['Components'].get('InvName')
    #     PVStored = entity['Components'].get('PVName')
    #     if (set(InvStored) == set(InvList))&(set(PVStored)==set(PVList)):
    #         # skip all the calculations and just return 
    #         readstored = True
    #         errmsg +=  "Read stored values - Refresh to re-optimise and update"
    #         OutputDict.update({'PVName':PVList,'InvName':InvList,'status':errmsg})

    if not readstored:

        ###### Gather Financial Parameters into FinData
        if 'finparams' in entity:
            FinData = {}
            FinData = entity['finparams']
            fin2ent = {}
            for t in default_finparams:
                if t not in FinData:
                    fin2ent[t] = default_finparams[t]
                    FinData[t] = default_finparams[t]
                else:
                    fin2ent[t] = FinData[t]
            if 'Adjustments' in FinData:
                fin2ent['Adjustments'] = FinData['Adjustments']

        else:
            fin2ent = {}
            FinData = {}
            for t in default_finparams:
                fin2ent[t] = default_finparams[t]
                FinData[t] = default_finparams[t]
        entity.update({'finparams': fin2ent})
        #########--------------------------#############################
        # Get Tariff Details here  Tariff and Yearly time series of tariff

        default_tariffsummary = {"pvtariffAvg": 0, "pvtarifftotal": 0,
                                 "EpriceAvg": default_finparams['e_price'], "Epricetotal": default_finparams['e_price']}
        if 'EpriceTariffFile' in entity:
            # blob = bucket.get_blob(USER_UPLOADS +entity['EpriceTariffFile'])
            FinData.update({"PVTariffSummary": {"pvtariffAvg": 0, "pvtarifftotal": 0,
                                                "EpriceAvg": default_finparams['e_price'],
                                                "Epricetotal": default_finparams['e_price']}})
            blob = bucket.get_blob(entity['EpriceTariffFile'])

            if blob:

                TariffDic = pickle.loads(blob.download_as_string())
                if ('PVTariffSummary' in TariffDic):
                    for t in default_tariffsummary:
                        if t not in TariffDic['PVTariffSummary']:
                            TariffDic['PVTariffSummary'][t] = default_tariffsummary[t]
                    FinData.update({"PVTariffSummary": TariffDic['PVTariffSummary']})
                else:
                    FinData.update({"PVTariffSummary": {"PVTariffSummary": {"pvtariffAvg": 0, "pvtarifftotal": 0,
                                                                            "EpriceAvg": default_finparams['e_price'],
                                                                            "Epricetotal": default_finparams[
                                                                                'e_price']}}})

                if ('YearlyTariff' in TariffDic):
                    FinData.update({"YearlyTariff": TariffDic['YearlyTariff']})
                else:
                    year = "2021"
                    YearlyTariff = pd.DataFrame(index=pd.date_range(start='1/1/' + year, periods=8760, freq='1H'),
                                                columns=['PVTariff'])
                    YearlyTariff['PVTariff'] = 0
                    FinData.update({"YearlyTariff": YearlyTariff})

            else:
                # blob = bucket.blob(USER_UPLOADS +entity['EpriceTariffFile'])
                blob = bucket.blob(entity['EpriceTariffFile'])
                TariffDic = {"PVTariffSummary": {"pvtariffAvg": 0, "pvtarifftotal": 0,
                                                 "EpriceAvg": default_finparams['e_price'],
                                                 "Epricetotal": default_finparams['e_price']}}

                year = "2021"
                YearlyTariff = pd.DataFrame(index=pd.date_range(start='1/1/' + year, periods=8760, freq='1H'),
                                            columns=['PVTariff'])
                YearlyTariff['PVTariff'] = 0
                TariffDic.update({"YearlyTariff": YearlyTariff})
                blob.upload_from_string(pickle.dumps(TariffDic))
                FinData.update(TariffDic)
        else:
            FinData.update({"PVTariffSummary": {"pvtariffAvg": 0, "pvtarifftotal": 0,
                                                "EpriceAvg": default_finparams['e_price'],
                                                "Epricetotal": default_finparams['e_price']}})
            YearlyTariff = pd.DataFrame(index=pd.date_range(start='1/1/2020', periods=8760, freq='1H'),
                                        columns=['PVTariff'])

            # Add electrical price by hour if avaialble
            YearlyTariff['PVTariff'] = 0
            FinData.update({"YearlyTariff": YearlyTariff})
        # Add Demand to FinData as well
        if 'DemandTS' in entity['Demand']:
            DemandTS = pickle.loads(bucket.get_blob(entity['Demand']['DemandTS']).download_as_string())
            FinData.update({"DemandTS": DemandTS})

        #######################################   
        # TO ensure the DC to AC power is limited 
        DClimits = True
        if 'criteria' in entity:
            if entity['criteria'] == 'None':
                DClimits = False

        St = elecDesign['PVstring']
        CompInd = pd.MultiIndex.from_product([PVList, InvList], names=['Modules', 'Inv'])
        OptDf = pd.DataFrame()
        TSOptDf = pd.DataFrame()
        for combo in CompInd:
            if combo[0] in PVDF:
                PVModuleData = PVDF.loc[:, combo[0]]
            elif combo[0] in PVSYSDF:
                PVModuleData = PVSYSDF.loc[:, combo[0]]
            elif blobpv:
                if combo[0] in USERPVDF:
                    PVModuleData = USERPVDF.loc[:, combo[0]]

            InvData = INVDF.loc[:, combo[1]]
            PVName = PVModuleData.name
            InvName = InvData.name
            # if Power_req  is DC Power

            # NumBB = int(round(Power_req/elecDesign['BoP_Design']['DC2ACmax']/InvData.Paco,0))
            NumBB = int(round(Power_req / InvData.Paco, 0))
            if NumBB == 0:
                break

            # if entity['criteria']=='None':
            #     TotalPanelArea = entity['Location']['TotalPanelArea']
            #     MaxNumPanels = int(TotalPanelArea/PVModuleData.A_c)
            #     PanStringArr,Ptuples =Opt.FeasibleStringRange(PVModuleData,InvData,SolData['Temperature'])
            #     MSRANGE = [list(set([d[0] for d in Ptuples.values()]))[0], list(set([d[0] for d in Ptuples.values()]))[-1]  ]
            #     MPRANGE = [list(set([d[1] for d in Ptuples.values()]))[1], list(set([d[1] for d in Ptuples.values()]))[-1]  ]
            #     OutputDict.update({"RangeSeries":MSRANGE,"RangeParallel":MPRANGE, "MaxNumPanels":MaxNumPanels,               
            #                             "NumInverters":NumBB,"DC2ACRange":[min(Ptuples.keys())*PVModuleData.Wp/(NumBB*InvData.Paco),max(Ptuples.keys())*PVModuleData.Wp/(NumBB*InvData.Paco)],
            #                             "ACPower_Multiplier":InvData.Paco/1000, "DCPower_Multiplier":PVModuleData.Wp/1000})
            #     OutputDict.update({"StringArr":json.loads(json.dumps(PanStringArr)),"MsMpPairs":json.loads(json.dumps(Ptuples))})
            #     return jsonify(OutputDict),200

            PVString = PVsel.PVString(tilt, azimuth, St['Albedo'], St['Ms'], St['Mp'], NumBB)
            # CostData is a df of the BOM only with PerfModelKey == PVName or InvName

            CostDf = BoMDf[BoMDf['PerfModelKey'].isnull()]
            CostDf = pd.concat([CostDf, BoMDf[BoMDf['PerfModelKey'] == InvName]])

            # CostDf = CostDf.drop(index=CostDf[CostDf['NetPrice']<0].index)
            # Remove negative prices from adjustments and subsidies that distort the CAPEX
            CostDf = CDB.BoMExcludeFlags(CostDf, OrgBomInput=OrgBomInput)

            TSDf, AggDf = Opt.OptParamRuns(SolData, location_data, PVModuleData, InvData,
                                           PVString, elecDesign, CostDf, FinData, AreaSpacingParams, tilt, azimuth,
                                           LatPolyfromFile, DClimits)

            TEMPdf = pd.concat([AggDf], keys=[(PVModuleData.name, InvData.name)], names=['PVName', 'InvName'], axis=0)
            OptDf = pd.concat([OptDf, TEMPdf], axis=0)

            TEMP1df = pd.concat([TSDf], keys=[(PVModuleData.name, InvData.name)], names=['PVName', 'InvName'], axis=1)
            TSOptDf = pd.concat([TSOptDf, TEMP1df], axis=1)

        if len(OptDf) == 0:
            return jsonify({"error": "Inverter size too large for required power"}), 400

        OptDf = OptDf.sort_index()

        # if len(OptDf)==0:
        #     return jsonify({"error":"No electrically feasible combination exits"}),400
        TSOptDf = TSOptDf.sort_index(axis='columns')  # Lexicograpic sorting ensures better performance
        uniqueid = 'Uid_' + userinput['email'] + '_Pid_' + userinput['project_id']
        blobname_configs = RESULTS + uniqueid + '_Configs.pkl'
        bucket.blob(blobname_configs).upload_from_string(pickle.dumps(OptDf))

        blobname_ts = RESULTS + uniqueid + '_TSConfigs.pkl'
        bucket.blob(blobname_ts).upload_from_string(pickle.dumps(TSOptDf))
        BoMDf.fillna(np.nan, inplace=True)
        BoMDf.replace([np.nan], [None], inplace=True)

        BoMtoSave = BoMDf[['Group', 'ID', 'Name', 'Cost', 'Unit', 'Currency']]

        optmatrix = {'OptMatrix': {'OptConfigFile': blobname_configs, 'TSConfigFile': blobname_ts},
                     'Components': {"PVName": PVList, "InvName": InvList},
                     'Cost': json.loads(BoMtoSave.to_json(orient='index'))}

        entity.update(optmatrix)
        entity.update({"status": "Generate Design"})

        OutputDict = {'PVName': PVList, 'InvName': InvList, 'status': errmsg}

        ds.put(entity)

    # Only if no criteria is set 1 set of 
    if readstored:
        blobname = entity['OptMatrix']['OptConfigFile']
        OptDf = pickle.loads(bucket.get_blob(blobname).download_as_string())
        combo = OptDf.index[0]
        if combo[0] in PVDF:
            PVModuleData = PVDF.loc[:, combo[0]]
        elif combo[0] in PVSYSDF:
            PVModuleData = PVSYSDF.loc[:, combo[0]]
        elif blobpv:
            if combo[0] in USERPVDF:
                PVModuleData = USERPVDF.loc[:, combo[0]]
        InvData = INVDF.loc[:, combo[1]]

    if 'criteria' in entity:
        if entity['criteria'] == 'None':
            if ((len(PVList) == 1) and (len(InvList) == 1)):
                TotalPanelArea = entity['Location']['TotalPanelArea']
                MaxNumPanels = int(TotalPanelArea / PVModuleData.A_c)
                # TODO: Send back the string arrangement - CHANGE UI
                # PanStringArr,Ptuples =Opt.FeasibleStringRange(PVModuleData,InvData,SolData['Temperature'])

                try:
                    MSRANGE = [min(OptDf.index.get_level_values(2)), max(OptDf.index.get_level_values(2))]
                    MPRANGE = [min(OptDf.index.get_level_values(3)), max(OptDf.index.get_level_values(3))]
                    OutputDict.update({"RangeSeries": MSRANGE, "RangeParallel": MPRANGE, "MaxNumPanels": MaxNumPanels,
                                       "NumInverters": OptDf.iloc[0]['NumBB'],
                                       "DC2ACRange": [min(OptDf.iloc[:]['DC2AC'].values),
                                                      max(OptDf.iloc[:]['DC2AC'].values)],
                                       "ACPower_Multiplier": InvData.Paco / 1000,
                                       "DCPower_Multiplier": PVModuleData.Wp / 1000})
                except:
                    MSRANGE = [0]
                    MPRANGE = [0]
                    OutputDict.update({"RangeSeries": MSRANGE, "RangeParallel": MPRANGE, "MaxNumPanels": MaxNumPanels,
                                       "NumInverters": 0, "DC2ACRange": [0, 0],
                                       "ACPower_Multiplier": InvData.Paco / 1000,
                                       "DCPower_Multiplier": PVModuleData.Wp / 1000})
            else:
                return jsonify({"error": "Only 1 Solarmodule and 1 inverter should be selected in component list"}), 400

    ds.put(entity)
    return jsonify(OutputDict), 200


@app.route('/techselect', methods=['POST', 'GET'])  # 
@jwt_authenticated
def techselect():
    ds = datastore.Client()
    gcs = storage.Client()

    req_json = request.get_json()['Components']

    userinput = req_json
    userinput['email'] = request.email
    orgname = request.org
    parent_key = ds.key('users', userinput['email'])
    message = {}

    entity = ds.get(key=ds.key('Project', userinput['project_id'], parent=parent_key))
    if not entity:
        message.update({"error": "No project with username {} and project {} exists".format(userinput['email'],
                                                                                            userinput['project_id'])})
        return jsonify(message), 400

    entchecklist = ['Location', 'Weather', 'Demand']
    for ec in entchecklist:
        if ec not in entity:
            return jsonify({"error": "{} not set in datastore".format(ec)}), 400

    uschklist = ['PVName', 'InvName']
    for us in uschklist:
        if us not in userinput:
            return jsonify({"error": "{} not set in input request".format(us)}), 400

    DsLocation = entity['Location']
    DsWeather = entity['Weather']

    # TODO: Adapt for multiple areas till then just use 1st area
    # TODO : Put in error message when tilt is missing in Location
    if (('tilt' not in DsLocation) or ('azimuth' not in DsLocation)):
        return jsonify({"error": "tilt or azimuth information missing in Location"})
    if (('latitude' not in DsLocation) or ('longitude' not in DsLocation)):
        return jsonify({"error": "latitude or longitude information missing in Location"})
    if 'finalweatherfile' not in DsWeather:
        return jsonify({"error": "finalweatherfile information missing in Weather"})
    elecDesign = ELPARM

    tilt = {}
    azimuth = {}
    SurfArea = {}
    try:
        for AreaLabel in DsLocation['tilt']:
            tilt[AreaLabel] = DsLocation['tilt'][AreaLabel]
            azimuth[AreaLabel] = DsLocation['azimuth'][AreaLabel]

            if 'SurfArea' in DsLocation:
                SurfArea[AreaLabel] = DsLocation['SurfArea'][AreaLabel]
            else:
                numAreas = len(DsLocation['tilt'])
                SurfArea[AreaLabel] = DsLocation['TotalPanelArea'] / numAreas
    except:
        tilt['Area1'] = [i for i in DsLocation['tilt'].values()][0]
        azimuth['Area1'] = [i for i in DsLocation['azimuth'].values()][0]
        SurfArea['Area1'] = DsLocation['TotalPanelArea']

    if 'AreaSpacingParams' in DsLocation:
        AreaSpacingParams = DsLocation['AreaSpacingParams']
    else:
        AreaSpacingParams = {AreaLabel: elecDesign['systemparams']['SpacingParams'] for AreaLabel in tilt}
        # try:
        #     tilt = tilt+DsLocation['SpacingParams']['Ti_P']
        #     azimuth = DsLocation['SpacingParams']['Az_P']
        # except:
        #     pass

    weatherfile = DsWeather['finalweatherfile']
    location_data = Location(DsLocation['latitude'], DsLocation['longitude'], tz=DsWeather['TZ'])

    blobname = weatherfile
    bucket = gcs.get_bucket(BUCKET_NAME)
    blob = bucket.get_blob(blobname)
    s = blob.download_as_string()

    SolData = pickle.loads(s)

    uniqueid = 'Uid_' + userinput['email']

    blobname_pvmod = USER_UPLOADS + uniqueid + '_moduledata.pkl'
    blobpv = bucket.get_blob(blobname_pvmod)
    if blobpv:
        s = blobpv.download_as_string()
        USERPVDF = pickle.loads(s)

    GeoCoordFile = entity['Location']['GeoCoords']
    LatPolyfromFile = json.loads(bucket.get_blob(GeoCoordFile).download_as_string())

    Power_req = entity['Demand']['ACpower'] * 1000  # convert to W

    if Power_req == 0:
        Power_req = 10 * 1000

    PVrawlst = [pvname for pvname in userinput['PVName']]
    Invrawlst = [invname for invname in userinput['InvName']]
    default_costparams = FINPARM['cost']

    default_finparams = FINPARM['financial']

    if 'Cost' in entity:
        CostData = {}
        CostData.update(entity['Cost'])
    else:
        CostData = {}
        CostData.update(default_costparams)

    if 'PVCost' not in CostData:
        CostData['PVCost'] = {PVName: {'data': default_costparams['PVCost']['data'][0],
                                       'Unit': default_costparams['PVCost']['Unit']} for PVName in PVrawlst}

    if 'InvCost' not in CostData:
        CostData['InvCost'] = {InvName: {'data': default_costparams['InvCost']['data'],
                                         'Unit': default_costparams['PVCost']['Unit']} for InvName in Invrawlst}

    if 'finparams' in entity:
        FinData = {}
        FinData = entity['finparams']
        fin2ent = {}
        for t in default_finparams:
            if t not in FinData:
                fin2ent[t] = default_finparams[t]
                FinData[t] = default_finparams[t]
            else:
                fin2ent[t] = FinData[t]
        if 'Adjustments' in FinData:
            fin2ent['Adjustments'] = FinData['Adjustments']

    else:
        fin2ent = {}
        FinData = {}
        for t in default_finparams:
            fin2ent[t] = default_finparams[t]
            FinData[t] = default_finparams[t]

    # DELETE SOON 

    ##############

    PVList = []
    InvList = []
    errmsg = " "

    if ((len(PVrawlst) > 3) or (len(Invrawlst) > 3)):
        return jsonify({"error": "More than 3 components uploaded "}), 400

    if len(set(PVrawlst)) != len(PVrawlst):
        return jsonify({"error": "Duplicate names in PV module list"}), 400

    if len(set(Invrawlst)) != len(Invrawlst):
        return jsonify({"error": "Duplicate names in Inverter name list"}), 400

    for p in PVrawlst:
        if p in PVDF.columns:
            PVList.append(p)
        elif p in PVSYSDF.columns:
            PVList.append(p)
        elif blobpv:
            if p in USERPVDF.columns:
                PVList.append(p)
            else:
                return jsonify({"error": "PV module name {} not found in any database".format(p)}), 400
        else:
            return jsonify({"error": "PV module name {} not found in any database".format(p)}), 400

    #        score,PVname = sorted({Levenshtein.jaro(x.replace('_',' '),p): x for x in PVDF.columns}.items(),reverse=True)[0] 
    #        sysscore, PVSYSname = sorted({Levenshtein.jaro(x.replace('_',' '),p): x for x in PVSYSDF.columns}.items(),reverse=True)[0]         
    #        
    #        if score>=sysscore:
    #            PVList.append(PVname)
    #        else:
    #            PVList.append(PVSYSname)

    for i in Invrawlst:
        if i in INVDF.columns:  # IF exact name then take it
            Invname = i
        else:
            score, Invname = \
                sorted({Levenshtein.jaro(x.replace('_', ' '), i): x for x in INVDF.columns}.items(), reverse=True)[
                0]  # If fuzzy then try to get match

            InvData = INVDF.loc[:, Invname]
            # brandname = re.split('__',Invname)[0]

            #            if ( ((InvData['Paco']) < Power_req) or ( abs(((InvData['Paco']) - Power_req)/(InvData['Paco'])) > 0.2 ) ): # Find closest power if power mismatch more than 20%
            #                samebrandlist = [invname for invname in INVDF.columns if invname.startswith(brandname)]
            #                InvData = INVDF.loc[:,(abs(INVDF.loc['Paco',samebrandlist]-Power_req)).idxmin] # Find closest power match within the same brand
            #                Invname = InvData.name
            #                errmsg = errmsg+' - Inverter selected {} - due to mismatch with power required, closest matching model of same brand of inverter selected\n'.format(InvData.name.replace("_"," "))
            #            else:
            errmsg = errmsg + ' - Inverter selected {}\n'.format(InvData.name.replace("_", " "))
            Invname = InvData.name
        InvList.append(Invname)

    # Updating Inverter and Battery Costs by expanding cost and name lists

    # Update to required form only if not already done so

    if 'Unit' in CostData['PVCost']:
        CostData.update({"PVCost": {k: {'data': v, 'Unit': CostData['PVCost']['Unit']} for k, v in
                                    zip(PVList, CostData['PVCost']['data'])},
                         'InvCost': {k: {'data': v, 'Unit': CostData['InvCost']['Unit']} for k, v in
                                     zip(InvList, CostData['InvCost']['data'])}})

    # TODO : Make this CostData based on BoMQuote 

    """
    Structure:
       {'PVCost': {'PVName': {'data': 0.3, 'Unit': 'per Wp'}},
        'InvCost': {'InvName': {'data': 0.05, 'Unit': 'per Wac'}},
        'Construction': {'Unit': 'per plant', 'data': 0},
        'Bos': {'Unit': 'per plant', 'data': 0},
        'Others': {'Unit': 'per plant', 'data': 0},
        'Curr': 'USD'} 
    """

    elecDesign = ELPARM

    ################################################

    entity.update({'finparams': fin2ent})  # Save it here, as Dfs will be added to the finparams which is invalid for ds

    # Get Tariff Details here  Tariff and Yearly time series of tariff

    default_tariffsummary = {"pvtariffAvg": 0, "pvtarifftotal": 0,
                             "EpriceAvg": default_finparams['e_price'], "Epricetotal": default_finparams['e_price']}
    if 'EpriceTariffFile' in entity:
        # blob = bucket.get_blob(USER_UPLOADS +entity['EpriceTariffFile'])
        FinData.update({"PVTariffSummary": {"pvtariffAvg": 0, "pvtarifftotal": 0,
                                            "EpriceAvg": default_finparams['e_price'],
                                            "Epricetotal": default_finparams['e_price']}})
        blob = bucket.get_blob(entity['EpriceTariffFile'])

        if blob:

            TariffDic = pickle.loads(blob.download_as_string())
            if ('PVTariffSummary' in TariffDic):
                for t in default_tariffsummary:
                    if t not in TariffDic['PVTariffSummary']:
                        TariffDic['PVTariffSummary'][t] = default_tariffsummary[t]
                FinData.update({"PVTariffSummary": TariffDic['PVTariffSummary']})
            else:
                FinData.update({"PVTariffSummary": {"PVTariffSummary": {"pvtariffAvg": 0, "pvtarifftotal": 0,
                                                                        "EpriceAvg": default_finparams['e_price'],
                                                                        "Epricetotal": default_finparams['e_price']}}})

            if ('YearlyTariff' in TariffDic):
                FinData.update({"YearlyTariff": TariffDic['YearlyTariff']})
            else:
                year = "2020"
                YearlyTariff = pd.DataFrame(index=pd.date_range(start='1/1/' + year, periods=8760, freq='1H'),
                                            columns=['PVTariff'])
                YearlyTariff['PVTariff'] = 0
                FinData.update({"YearlyTariff": YearlyTariff})

        else:
            # blob = bucket.blob(USER_UPLOADS +entity['EpriceTariffFile'])
            blob = bucket.blob(entity['EpriceTariffFile'])
            TariffDic = {"PVTariffSummary": {"pvtariffAvg": 0, "pvtarifftotal": 0,
                                             "EpriceAvg": default_finparams['e_price'],
                                             "Epricetotal": default_finparams['e_price']}}

            year = "2020"
            YearlyTariff = pd.DataFrame(index=pd.date_range(start='1/1/' + year, periods=8760, freq='1H'),
                                        columns=['PVTariff'])
            YearlyTariff['PVTariff'] = 0
            TariffDic.update({"YearlyTariff": YearlyTariff})
            blob.upload_from_string(pickle.dumps(TariffDic))
            FinData.update(TariffDic)
    else:
        FinData.update({"PVTariffSummary": {"pvtariffAvg": 0, "pvtarifftotal": 0,
                                            "EpriceAvg": default_finparams['e_price'],
                                            "Epricetotal": default_finparams['e_price']}})
        YearlyTariff = pd.DataFrame(index=pd.date_range(start='1/1/2020', periods=8760, freq='1H'),
                                    columns=['PVTariff'])

        # Add electrical price by hour if avaialble
        YearlyTariff['PVTariff'] = 0
        FinData.update({"YearlyTariff": YearlyTariff})

    #######################################   
    DClimits = True
    if 'criteria' in entity:
        if entity['criteria'] == 'None':
            DClimits = False

    St = elecDesign['PVstring']

    CompInd = pd.MultiIndex.from_product([PVList, InvList], names=['Modules', 'Inv'])
    OptDf = pd.DataFrame()
    TSOptDf = pd.DataFrame()
    for combo in CompInd:
        if combo[0] in PVDF:
            PVModuleData = PVDF.loc[:, combo[0]]
        elif combo[0] in PVSYSDF:
            PVModuleData = PVSYSDF.loc[:, combo[0]]
        elif blobpv:
            if combo[0] in USERPVDF:
                PVModuleData = USERPVDF.loc[:, combo[0]]

        InvData = INVDF.loc[:, combo[1]]
        PVName = PVModuleData.name
        InvName = InvData.name
        NumBB = int(round(Power_req / InvData.Paco, 0))

        PVString = PVsel.PVString(tilt, azimuth, St['Albedo'], St['Ms'], St['Mp'], NumBB)

        # If CostData has been pulled from a stored version without the cost update and so cost values are meaningless anyhow
        if PVName not in CostData['PVCost']:
            CostData.update({"PVCost": {
                k: {'data': default_costparams['PVCost']['data'][0], 'Unit': default_costparams['PVCost']['Unit']} for k
                in PVList}})
        if InvName not in CostData['InvCost']:
            CostData.update({"InvCost": {
                k: {'data': default_costparams['InvCost']['data'][0], 'Unit': default_costparams['InvCost']['Unit']} for
                k in InvList}})

        CostSelected = {'PVCost': CostData['PVCost'][PVName],
                        'InvCost': CostData['InvCost'][InvName]}
        CostSelected.update({k: v for k, v in CostData.items() if ((k != 'PVCost') and (k != 'InvCost'))})

        TSDf, AggDf = Opt.OptParamRuns(SolData, location_data, PVModuleData, InvData,
                                       PVString, elecDesign, CostSelected, FinData, AreaSpacingParams, tilt, azimuth,
                                       LatPolyfromFile, DClimits)

        TEMPdf = pd.concat([AggDf], keys=[(PVModuleData.name, InvData.name)], names=['PVName', 'InvName'], axis=0)
        OptDf = pd.concat([OptDf, TEMPdf], axis=0)

        TEMP1df = pd.concat([TSDf], keys=[(PVModuleData.name, InvData.name)], names=['PVName', 'InvName'], axis=1)
        TSOptDf = pd.concat([TSOptDf, TEMP1df], axis=1)

    OptDf = OptDf.sort_index()
    if len(OptDf) == 0:
        return jsonify({"error": "No electrically feasible combination exits"}), 400
    TSOptDf = TSOptDf.sort_index(axis='columns')  # Lexicograpic sorting ensures better performance
    uniqueid = 'Uid_' + userinput['email'] + '_Pid_' + userinput['project_id']
    blobname_configs = RESULTS + uniqueid + '_Configs.pkl'
    blob = bucket.blob(blobname_configs)
    blob.upload_from_string(pickle.dumps(OptDf))

    blobname_ts = RESULTS + uniqueid + '_TSConfigs.pkl'
    blob = bucket.blob(blobname_ts)
    blob.upload_from_string(pickle.dumps(TSOptDf))

    optmatrix = {'OptMatrix': {'OptConfigFile': blobname_configs, 'TSConfigFile': blobname_ts},
                 'Components': {"PVName": PVList, "InvName": InvList},
                 'Cost': CostData}

    entity.update(optmatrix)
    entity.update({"status": "Generate Design"})

    outputdict = {'PVName': PVList, 'InvName': InvList, 'status': errmsg}

    ds.put(entity)

    # Only if no criteria is set
    if 'criteria' in entity:
        if entity['criteria'] == 'None':
            if ((len(PVList) == 1) and (len(InvList) == 1)):
                TotalPanelArea = entity['Location']['TotalPanelArea']
                MaxNumPanels = int(TotalPanelArea / PVModuleData.A_c)
                try:
                    MSRANGE = [min(OptDf.index.get_level_values(2)), max(OptDf.index.get_level_values(2))]
                    MPRANGE = [min(OptDf.index.get_level_values(3)), max(OptDf.index.get_level_values(3))]
                    outputdict.update({"RangeSeries": MSRANGE, "RangeParallel": MPRANGE, "MaxNumPanels": MaxNumPanels,
                                       "NumInverters": OptDf.iloc[0]['NumBB'],
                                       "DC2ACRange": [min(OptDf.iloc[:]['DC2AC'].values),
                                                      max(OptDf.iloc[:]['DC2AC'].values)],
                                       "ACPower_Multiplier": InvData.Paco / 1000,
                                       "DCPower_Multiplier": PVModuleData.Wp / 1000})
                except:
                    MSRANGE = [0]
                    MPRANGE = [0]
                    outputdict.update({"RangeSeries": MSRANGE, "RangeParallel": MPRANGE, "MaxNumPanels": MaxNumPanels,
                                       "NumInverters": 0, "DC2ACRange": [0, 0],
                                       "ACPower_Multiplier": InvData.Paco / 1000,
                                       "DCPower_Multiplier": PVModuleData.Wp / 1000})

    #    compselection = {'PV_name':PVModuleData.name,'Inverter_name':InvData.name,
    #                     'criteria':userinput['criteria'],'battery':userinput['battery'],'tiltvalue':userinput['tiltvalue'],
    #                     'fulfil_factor':userinput['fulfil_factor']}

    return jsonify(outputdict), 200


@app.route('/search-components', methods=['POST', 'GET'])  # search for components
@jwt_authenticated
def searchcomponents():
    ds = datastore.Client()
    gcs = storage.Client()

    message = {}
    userinput = request.get_json()
    userinput['email'] = request.email
    orgname = request.org
    parent_key = ds.key('users', userinput['email'])

    entity = ds.get(key=ds.key('Project', userinput['project_id'], parent=parent_key))
    if not entity:
        message.update({"error": "No project with username {} and project {} exists".format(userinput['email'],
                                                                                            userinput['project_id'])})
        return jsonify(message), 400

    uschklist = ['component']
    for us in uschklist:
        if us not in userinput:
            return jsonify({"error": "{} not set in input request".format(us)}), 400
    if 'name' not in userinput:
        name = ""
    else:
        name = userinput['name']
    if 'numresults' not in userinput:
        results2show = 10
    else:
        results2show = int(userinput['numresults'])
    component = userinput['component']  # "PVModule" or "Inverter"

    uniqueid = 'Uid_' + userinput['email']
    OutputDic = {}
    bucket = gcs.get_bucket(BUCKET_NAME)
    if 'Demand' in entity:
        ACPower = entity['Demand']['ACpower']  # in kW
    else:
        ACPower = 99999

    if 'power' in userinput:
        Power = userinput['power']  # in W
    #        minP = 0.8*Power
    #        maxP = 1.2*Power 
    else:
        Power = 0
    #        minP = 0.8*Power
    #        maxP = np.Inf

    if component == "PVModule":

        blobname_pvmod = USER_UPLOADS + uniqueid + '_moduledata.pkl'
        blobpv = bucket.get_blob(blobname_pvmod)
        if blobpv:
            s = blobpv.download_as_string()
            USERPVDF = pickle.loads(s)
            USERPVDF.loc['Wp'] = USERPVDF.loc['I_mp_ref'] * USERPVDF.loc['V_mp_ref']
            USERPVDF = USERPVDF.loc[:, ~USERPVDF.columns.duplicated()]
        else:
            USERPVDF = pd.DataFrame()

        ListOfDfs = [PVDF, PVSYSDF, USERPVDF]  # Add User DFs here
        sourcelist = ['CEC', 'PVSYST', 'UserDB']
        List_pv = pd.DataFrame()
        for DFs, source in zip(ListOfDfs, sourcelist):
            try:

                if Power == 0:
                    BL = DFs.columns
                else:
                    # BL= abs(DFs.loc['Wp',:]-Power).sort_values(ascending=True).index[:results2show] 
                    # BL = DFs.columns[(DFs.loc['Wp']>Power*0.8) & (DFs.loc['Wp']<Power*1.2)] # Select Power range 
                    BL = DFs.columns[(DFs.loc['Wp'] >= Power) & (DFs.loc['Wp'] < Power * 1.1)]
                Lname = sorted([(Levenshtein.jaro(x.replace('_', ' ').lower(), name.lower()), x) for x in DFs[BL]],
                               reverse=True)[:results2show]
                listDf = pd.DataFrame(Lname, columns=['score', 'name'])
                listDf['Wp'] = [np.round(DFs[x[1]].loc['Wp']) for x in Lname]
                listDf['source'] = source
                List_pv = pd.concat([List_pv, listDf])
            except:
                pass
        # List_pv.sort_values(by=['Wp'],ascending=False).reset_index(drop=True)[['name','Wp','source']].iloc[:results2show].to_dict()    

        OutputDic.update({"PVlist": List_pv.sort_values(by=['score'], ascending=False).reset_index(drop=True)[
                                        ['name', 'Wp', 'source']]
                         .iloc[:results2show].to_dict()})

    if component == "Inverter":
        blobname_inv = USER_UPLOADS + uniqueid + '_invdata.pkl'
        blobinv = bucket.get_blob(blobname_inv)
        if blobinv:
            s = blobinv.download_as_string()
            USERINVDF = pickle.loads(s)
        else:
            USERINVDF = pd.DataFrame()

        ListOfDfs = [INVDF, USERINVDF]  # Add User DFs here 

        List_inv = pd.DataFrame()
        for DFs in ListOfDfs:
            try:
                # BL = DFs.columns[(DFs.loc['Paco']>minP) & (DFs.loc['Paco']<maxP)] # Select Power range
                if Power == 0:

                    BL = DFs.columns[(DFs.loc['Paco'] <= ACPower * 1000)]
                else:
                    BL = DFs.columns[(DFs.loc['Paco'] > Power * 0.8 * 1000) & (DFs.loc['Paco'] < Power * 1.2 * 1000) & (
                                DFs.loc['Paco'] <= ACPower * 1000)]
                # BL= abs(DFs.loc['Paco',:]-Power).sort_values(ascending=True).index[:results2show] 
                Lname = sorted([(Levenshtein.jaro(x.replace('_', ' ').lower(), name.lower()), x) for x in DFs[BL]],
                               reverse=True)[:results2show]
                listDf = pd.DataFrame(Lname, columns=['score', 'name'])
                listDf['Pac'] = [np.round(DFs[x[1]].loc['Paco']) / 1000 for x in Lname]
                listDf['source'] = [DFs[x[1]].loc['Db'] if ('Db' in DFs.index) else 'custom' for x in Lname]
                List_inv = pd.concat([List_inv, listDf])
            except:
                pass
        List_inv.sort_values(by=['score'], ascending=False).reset_index(drop=True)[['name', 'Pac']].iloc[
        :results2show].to_dict()

        OutputDic.update({"Invlist": List_inv.sort_values(by=['score'], ascending=False).reset_index(drop=True)[
                                         ['name', 'Pac', 'source']].iloc[:results2show].to_dict()})

    return jsonify(OutputDic), 200


@app.route('/get-energy-elec-config', methods=['POST', 'GET'])  # Without storage /techselect_withstorage   
@jwt_authenticated
def elecdesign():
    # This one does almost everything....!
    ds = datastore.Client()
    gcs = storage.Client()
    bucket = gcs.get_bucket(BUCKET_NAME)
    errmsg = ' '
    message = {}
    req_json = request.get_json()['Energy']

    userinput = req_json
    userinput['email'] = request.email
    orgname = request.org
    uschklist = ['email', 'project_id', 'requesttype']
    for us in uschklist:
        if us not in userinput:
            return jsonify({"error": "{} not set in input request".format(us)}), 400

    parent_key = ds.key('users', userinput['email'])

    entity = ds.get(key=ds.key('Project', userinput['project_id'], parent=parent_key))
    if not entity:
        message.update({"error": "No project with username {} and project {} exists".format(userinput['email'],
                                                                                            userinput['project_id'])})
        return jsonify(message), 400

    ## USE DIFFERENT DB BASED ON ORG ###
    # qur = ds.query(kind='organisation')
    # qur.add_filter('users','=',userinput['email'])
    # Sol=list(qur.fetch())
    # if Sol != []:
    #     if os.environ['DB_NAME']!=Sol[0]['compdb']:
    #         os.environ['DB_NAME']= Sol[0]['compdb']
    #     orgname =  Sol[0].key.name      
    # else:        
    #     os.environ['DB_NAME'] = 'refdata'     
    orgentity = ds.get(key=ds.key('organisation', orgname))
    os.environ['DB_NAME'] = orgentity.get("compdb", "refdata")
    importlib.reload(CDB)
    # OrgInfo = orgentity['OrgInfo']
    uniqueid = 'Uid_' + userinput['email'] + '_Pid_' + userinput['project_id']
    OrgInfo = json.loads(bucket.get_blob(AUXFILES + "organisation_info.json").download_as_string())
    OrgDict = OrgInfo.get(orgname, OrgInfo['Agrola'])
    OrgBomInput = OrgDict.get('bominput', {})

    entchecklist = ['criteria', 'Components']
    for ec in entchecklist:
        if ec not in entity:
            return jsonify({"error": "{} not set in datastore".format(ec)}), 400

    criteria = entity['criteria']
    entity.update({"status": "Generate Design"})
    #    elecDesign = ELPARM
    #    St=elecDesign['PVstring']
    #    PVString=PVsel.PVString(St['Tilt'],St['Azimuth'],St['Albedo'],St['a'],St['b'],St['delT'],St['Ms'],St['Mp'],St['DC2inv'])
    #    
    #    BD= elecDesign['BoP_Design'] 
    #    BoP_Design = PVsel.BoP_Design(BD['Mismatch_loss'], BD['DC_loss'], BD['StringDiode_loss'], BD['AC_loss'], BD['InvClipping'], BD['DC2ACmax'])
    #    #Num_BB = elecDesign['systemparams']['NumBB']

    bucket = gcs.get_bucket(BUCKET_NAME)

    blobname_configs = RESULTS + uniqueid + '_Configs.pkl'
    blobOpt = bucket.get_blob(blobname_configs)
    if blobOpt:
        s = blobOpt.download_as_string()
        OptDf = pickle.loads(s)

        blobname_ts = RESULTS + uniqueid + '_TSConfigs.pkl'
        blobts = bucket.blob(blobname_ts)
        st = blobts.download_as_string()
        TSOptDf = pickle.loads(st)
        if (userinput['requesttype'] == 'Best'):
            if criteria == 'None':
                return jsonify({"error": "criteria is not defined for this request"}), 400
            BestConfigDict, BestConfig_OptDf, BestConfig_TSDf = Opt.SelectBestConfig(criteria, OptDf, TSOptDf)
            OutputDic = {"Config": BestConfigDict, "ConfigResultsSummary": json.loads(BestConfig_OptDf.to_json())}

            entity.update({"SelectedConfig": OutputDic})

        elif (userinput['requesttype'] == 'BestperCombo'):
            if criteria == 'None':
                return jsonify({"error": "criteria is not defined for this request"}), 400
            AllComboSummaryDf, BestComboNameslist, BestperComboList = Opt.SelectMatrixConfigs(criteria, OptDf, TSOptDf)
            BestConfigDict, BestConfig_OptDf, _ = Opt.SelectBestConfig(criteria, OptDf, TSOptDf)
            if BestConfigDict == {}:
                return jsonify(
                    {"error": "No feasible combination to match power requirements - choose another inverter"}), 400
            BestConfigDictList = [BestConfigDict['PVName'], BestConfigDict['InvName'], BestConfigDict['Ms'],
                                  BestConfigDict['Mp']]
            if 'SelectedConfig' in entity:
                SelConfigDict = entity['SelectedConfig']['Config']
                if SelConfigDict:
                    SelConfigList = [SelConfigDict['PVName'], SelConfigDict['InvName'], SelConfigDict['Ms'],
                                     SelConfigDict['Mp']]
                else:
                    SelConfigList = BestConfigDictList

            #            OutputDic = {"Config":BestComboNameslist,"ConfigResultsSummary":BestperComboList, "SelectedConfig":BestConfigDict,
            #                         "SelectedConfigResultsSummary":json.loads(BestConfig_OptDf.to_json())}
            #            
            ######### Make the requisite output response #########
            InvList = entity['Components']['InvName']
            PVList = entity['Components']['PVName']

            InvinBestCombo = list(set([L['InvName'] for L in BestComboNameslist]))
            PVinBestCombo = list(set([L['PVName'] for L in BestComboNameslist]))

            InvExtra = [Inv for Inv in InvList if Inv not in InvinBestCombo]
            PVExtra = [PV for PV in PVList if PV not in PVinBestCombo]
            if InvExtra:
                for i in InvExtra:
                    InvList.remove(i)
            if PVExtra:
                for p in PVExtra:
                    PVList.remove(p)

            OutputDic = {}
            InvID = [{"invid": InvList.index(Inv) + 1, "name": Inv} for Inv in InvList]
            AdditionalDf = pd.DataFrame(columns=AllComboSummaryDf.columns,
                                        index=['Ms', 'Mp', 'selected', 'recommended'])

            for s in AdditionalDf.columns:
                AdditionalDf[s].loc['Ms'] = int(s[2])
                AdditionalDf[s].loc['Mp'] = int(s[3])
                AdditionalDf[s].loc['selected'] = False
                AdditionalDf[s].loc['recommended'] = False

            AllComboSummaryDf = AllComboSummaryDf.append(AdditionalDf, ignore_index=False)

            if 'SelectedConfig' in entity:
                if tuple(SelConfigList) in AllComboSummaryDf.columns:
                    AllComboSummaryDf[tuple(SelConfigList)].loc['selected'] = True
                else:
                    AllComboSummaryDf[tuple(BestConfigDictList)].loc['selected'] = True
                    errmsg += " Selected Config {} not in Best combinations matrix but is still selected".format(
                        str(SelConfigList))
            else:
                AllComboSummaryDf[tuple(BestConfigDictList)].loc['selected'] = True
            AllComboSummaryDf[tuple(BestConfigDictList)].loc['recommended'] = True

            pvdata = []
            for PV in PVList:
                data = []
                element = {"pid": PVList.index(PV) + 1, "name": PV}
                for Inv in InvList:
                    data.append([AllComboSummaryDf[tuple(BestComboNameslist[i].values())].to_dict() for i in
                                 range(len(BestComboNameslist))
                                 if
                                 (BestComboNameslist[i]['InvName'] == Inv) and (BestComboNameslist[i]['PVName'] == PV)][
                                    0])

                    data = json.loads(json.dumps(data))
                    element.update({"solardata": data})

                pvdata.append(element)
            OutputDic = {"InvData": InvID, "PVdata": pvdata}

            #######################                             
            entity.update({"SelectedConfig": {"Config": BestConfigDict,
                                              "ConfigResultsSummary": json.loads(BestConfig_OptDf.to_json())}})
        elif (userinput['requesttype'] == 'SelectConfig'):
            if 'selectedconfig' not in userinput:
                configsel = entity['SelectedConfig']['Config']
                # return jsonify({"error": "Selected Config not in request"}),400
            else:
                configsel = userinput['selectedconfig']

                condchklist = ['PVName', 'InvName', 'Ms', 'Mp']
                for ec in condchklist:
                    if ec not in configsel:
                        return jsonify({"error": "{} not set in request".format(ec)}), 400

            pvname = configsel['PVName']
            invname = configsel['InvName']
            Ms = configsel['Ms']
            Mp = configsel['Mp']
            uniqueid = 'Uid_' + userinput['email']
            blobname_pvmod = USER_UPLOADS + uniqueid + '_moduledata.pkl'
            blobpv = bucket.get_blob(blobname_pvmod)
            if pvname in PVDF:
                PVModuleData = PVDF.loc[:, pvname]
            elif pvname in PVSYSDF:
                PVModuleData = PVSYSDF.loc[:, pvname]
            elif blobpv:
                USERPVDF = pickle.loads(blobpv.download_as_string())
                if pvname in USERPVDF:
                    PVModuleData = USERPVDF.loc[:, pvname]
                else:
                    return jsonify({"Error": "PV Module {} not found in any database".format(pvname)}), 400
            InvData = INVDF.loc[:, invname]

            Power_req = entity['Demand']['ACpower'] * 1000  # convert to W
            if Power_req == 0:
                Power_req = 10 * 1000
            NumBB = int(round(Power_req / InvData.Paco, 0))

            weatherblob = entity['Weather']['finalweatherfile']
            dataTMY = pickle.loads(bucket.get_blob(weatherblob).download_as_string())
            PanStringArr, Ptuples = Opt.FeasibleStringRange(PVModuleData, InvData, dataTMY['Temperature'])
            PaninStringKeys = np.array([x for x in PanStringArr.keys()])
            NuminString = int(PaninStringKeys[np.argmin(np.abs(PaninStringKeys - Ms * Mp))])

            # TotalPanels = NumBB*Ms*Mp
            TotalPanels = NumBB * NuminString
            ConfigSummTuple = (pvname, invname, Ms, Mp)
            try:
                ConfSummary = json.loads(OptDf.loc[ConfigSummTuple].to_json())
            except KeyError:
                return jsonify({"error": "{} not valid config stored in components ".format(str(ConfigSummTuple))}), 400

            if 'Adjustments' in entity['finparams']:
                Adjustments = entity['finparams']['Adjustments']
            else:
                Adjustments = {}
            if 'AreaFigures' in entity['Location']:
                AreaFigures = entity['Location']['AreaFigures']
            else:
                AreaFigures = {}
                # Get Time series values
            # selconfig = (entity['SelectedConfig']['Config']['PVName'],entity['SelectedConfig']['Config']['InvName'],
            #                         entity['SelectedConfig']['Config']['Ms'],entity['SelectedConfig']['Config']['Mp'],'PAC_tot')
            # Timeseries has (pvname, invname, Ms, Mp, value=PAC) as indices
            SelConfig_TSDf = pd.DataFrame()

            try:
                SelConfig_TSDf = TSOptDf[ConfigSummTuple]
            except:
                return jsonify(
                    {"error": "Configuration {} not in Time series of {}".format(str(ConfigSummTuple), uniqueid)})
            monthlyenergy = pd.DataFrame()
            monthlyenergy['PAC_tot'] = (SelConfig_TSDf['PAC_tot'].groupby(pd.Grouper(freq='1M')).sum()) / 1000  # in kWh
            monthlyenergy['kWh/kWp'] = monthlyenergy['PAC_tot'] / ConfSummary['DCPower']
            monthlyenergy['month'] = [dt.datetime.strftime(d, '%b') for d in monthlyenergy.index]
            #    monthlyenergy['PR'] = monthlyenergy['PAC_tot'].div(np.array(self.W2D['IrrOnSurface'])*((PVModuleData['V_mp_ref']*PVModuleData['I_mp_ref'])/(1000*PVModuleData['A_c'])))/1000#PAC in kWh

            energydisplay = {"Energydisplay": {
                "kwhmonthly": {"PAC_tot": [*monthlyenergy['PAC_tot']], "kWh/kWp": [*monthlyenergy['kWh/kWp']],
                               "month": [*monthlyenergy['month']]},
                "kwhyearly": {"totalMwh": monthlyenergy['PAC_tot'].sum() / 1000,
                              "kWhkWp": monthlyenergy['kWh/kWp'].mean()}}}  # "PR":monthlyenergy['PR'].mean()}}}

            PVList = [pvname]
            InvList = [invname]
            InvID = [{"invid": InvList.index(Inv) + 1, "name": Inv} for Inv in InvList]
            pvdata = []

            for PV in PVList:
                data = []
                element = {"pid": PVList.index(PV) + 1, "name": PV}
                for Inv in InvList:
                    data.append(OptDf.loc[ConfigSummTuple].to_dict())
                    data[0].update({"Ms": Ms, "Mp": Mp, "selected": True, "recommended": True})

                    data = json.loads(json.dumps(data))
                    element.update({"solardata": data})

                pvdata.append(element)

                # displaydic={"InvData":InvID,"PVdata":pvdata} 

            OutputDic = {"SelectedConfig": {"Config": configsel, "ConfigResultsSummary": ConfSummary
                                            }}
            # Change Value in BoM for Panel and Inverter quantities in case they are filled
            if 'ActiveQuote' in entity:

                ActiveQuote = entity['ActiveQuote']
                # Dont Change Values of Quote if active Quote is Edit
                if 'Edit' in ActiveQuote['RevID']:
                    RevID2Update = ActiveQuote['RevID'].split('-')[0]
                else:
                    RevID2Update = ActiveQuote['RevID']
                VQDf = CDB.GetBoMQuote(userinput['project_id'], ActiveQuote['QuoteID'], RevID2Update)
                if not VQDf.empty:
                    # Set to 0 Modules and Inverters not selected
                    VQDf.loc[~VQDf['PerfModelKey'].isnull(), 'PerfModelKey'] = VQDf.loc[
                        ~VQDf['PerfModelKey'].isnull(), 'PerfModelKey'].apply(lambda x: x.replace('\ufeff', '')).values
                    CostDf = VQDf[VQDf['PerfModelKey'].isnull()]
                    CostDf = pd.concat([CostDf, VQDf[VQDf['PerfModelKey'] == pvname]])
                    CostDf = pd.concat([CostDf, VQDf[VQDf['PerfModelKey'] == invname]])
                    CostDf = pd.concat([CostDf, VQDf[VQDf['Group'] == 'Battery']])
                    CostDf.drop_duplicates(subset=['BusinessIdentifierCode'], inplace=True)

                    # Exclude Items from CAPEX or Business Case calculation
                    CostDf = CDB.BoMExcludeFlags(CostDf, OrgBomInput=OrgBomInput)

                    CAPEX, OPEX, ViewQuoteDf, AdjDict = fm.Cost_function_db(PVModuleData, InvData, CostDf, Ms, Mp,
                                                                            NumBB, TotalPanels,
                                                                            Adjustments=Adjustments,
                                                                            AreaFigures=AreaFigures)

                    ViewQuoteDf.set_index('Id', inplace=True)
                    ViewQuoteDf.fillna(np.nan, inplace=True)
                    ViewQuoteDf.replace([np.nan], [None], inplace=True)
                    # ADD BASE PRICE and CFACTOR CALCULATION HERE

                    BoM2Update = [{'_Id': int(idBom), 'Quantity': float(ViewQuoteDf.loc[idBom, 'Quantity']),
                                   'TotalCost': float(ViewQuoteDf.loc[idBom, 'TotalCost']),
                                   'NetPrice': float(ViewQuoteDf.loc[idBom, 'NetPrice']),
                                   'PriceAdjustment': float(ViewQuoteDf.loc[idBom, 'PriceAdjustment']),
                                   'TotalPrice': float(ViewQuoteDf.loc[idBom, 'TotalPrice']),
                                   'TotalBasePrice': float(ViewQuoteDf.loc[idBom, 'TotalBasePrice'])} \
                                  for idBom in ViewQuoteDf.index]
                    CDB.UpdateBoM(BoM2Update)

                    # Remove Items that are not selected in SelectConfig from BoMQuote
                    ToDeleteDf = VQDf[((~VQDf['PerfModelKey'].isnull()) & (VQDf['PerfModelKey'] != pvname) & (
                                VQDf['PerfModelKey'] != invname) & (VQDf['Group'] != 'Battery'))]
                    ToDeleteDf.set_index('Id', inplace=True)
                    ToDeleteDf.fillna(np.nan, inplace=True)
                    ToDeleteDf.replace([np.nan], [None], inplace=True)
                    # Make Quantity = 0 for those items to remove
                    if len(ToDeleteDf) > 0:
                        DelBoM = [{'_Id': int(idBom), 'Quantity': None, 'TotalCost': None} for idBom in
                                  ToDeleteDf.index]
                        # 'NetPrice':float(0),
                        # 'PriceAdjustment':float(ToDeleteDf.loc[idBom,'PriceAdjustment']),
                        # 'TotalPrice':float(0),
                        # 'TotalBasePrice':float(0)} for idBom in ToDeleteDf.index] 
                        if len(DelBoM) > 0:
                            CDB.UpdateBoM(DelBoM)

            entity.update(OutputDic)
            entity.update({"status": "Generate Design"})

            OutputDic.update({"InvData": InvID, "PVdata": pvdata})

            ds.put(entity)
            OutputDic.update(energydisplay)
            return jsonify(OutputDic)


        elif (userinput['requesttype'] == 'byname'):
            pvname = userinput['PVName']
            invname = userinput['InvName']
            if 'Ms' or 'Mp' not in userinput:
                SweepConfigDf, SweepList = Opt.SelectDc2ACSweepCombo(criteria, OptDf, pvname, invname)
                OutputDic = {"Config": " PV: {}, Inv:{}".format(pvname, invname), "ConfigResultsSummary": SweepList}
            else:
                try:
                    Ms = userinput['Ms']
                    Mp = userinput['Mp']

                    ConfigSummTuple = (pvname, invname, Ms, Mp)
                    ConfSummary = json.loads(OptDf.loc[ConfigSummTuple].to_json())
                    OutputDic = {
                        "SelectedConfig": {"Config": {"PVName": pvname, "InvName": invname, "Ms": Ms, "Mp": Mp},
                                           "ConfigResultsSummary": ConfSummary}}
                    entity.update(OutputDic)
                except:
                    return jsonify(
                        {"error": "selected configuration missing Ms or Mp fields or combination not found"}), 400





    else:
        errmsg = errmsg + 'Configuration file {} not available'.format(blobname_configs)
        OutputDic = {}

    entity.update({"status": "Generate Design"})
    ds.put(entity)

    OutputDic.update({"messages": errmsg})

    return jsonify(OutputDic), 200


@app.route('/financial_output', methods=['POST'])  # Without storage /techselect_withstorage   
@jwt_authenticated
def financeout():
    ds = datastore.Client()
    gcs = storage.Client()
    message = {}
    # Update finparm from user inputs
    req_json = request.get_json()['Financial']
    userinput = req_json
    userinput['email'] = request.email
    orgname = request.org
    parent_key = ds.key('users', userinput['email'])

    entity = ds.get(key=ds.key('Project', userinput['project_id'], parent=parent_key))
    if not entity:
        message.update({"error": "No project with username {} and project {} exists".format(userinput['email'],
                                                                                            userinput['project_id'])})
        return jsonify(message), 400

    #    entityNone=ds.get(key=ds.key('users','None','Project',userinput['project_id']))
    #   
    #    if entity is None : # None doesnt exist and neither does the email - so just make a new one. Ideally - atleast None should exist from previous step            
    #        entity=datastore.Entity(key=ds.key('users',userinput['email'],
    #                                               'Project',userinput['project_id']))
    #    
    #    if entityNone is not None:
    #        entity.update(entityNone)
    #        ds.delete(key=ds.key('users','None','Project',userinput['project_id']))

    entity.update({"status": "Results Summary"})

    uniqueid = 'Uid_' + userinput['email'] + '_Pid_' + userinput['project_id']

    entchecklist = ['OptMatrix', 'SelectedConfig']
    for ec in entchecklist:
        if ec not in entity:
            return jsonify({"error": "{} not set in datastore".format(ec)}), 400

    #    uschklist = ['selected]
    #    for us in uschklist:
    #        if us not in userinput:
    #            return jsonify({"error": "{} not set in input request".format(us)}),400 

    bucket = gcs.get_bucket(BUCKET_NAME)

    if 'finaloutput' in userinput:
        if 'FinalSummary' in entity:
            if type(entity['FinalSummary']['CashFlow']) == str:
                blobname = entity['FinalSummary']['CashFlow']
                CashFlowDf = pickle.loads(bucket.get_blob(blobname).download_as_string())
                cf2dispdf = CashFlowDf.groupby(pd.Grouper(freq='1Y')).sum()
                cf2dispdf['OPEX+Interest'] = -(
                            cf2dispdf['NetBenefit'] - cf2dispdf['SavingsOnly'])  # +cf2dispdf['interests'])
                OvSummary = entity['FinalSummary']['OverallSummary']
                cf2dispdf['CAPEX'] = OvSummary.get('EffCAPEX', OvSummary['CAPEX'])
                cf2dispdf['CAPEX'].iloc[1:] = 0
                cf2dispdf['Net Cash Flow'] = cf2dispdf['ProjectCashFlow']
                cf2dispdf['Net Cash Flow'].iloc[0] = cf2dispdf['Net Cash Flow'].iloc[0] - \
                                                     entity['FinalSummary']['OverallSummary']['CAPEX']

                cf2dispdf['CumCashFlow'] = cf2dispdf['Net Cash Flow'].cumsum()
                OutputDic = {"AnnualCashFlow": cf2dispdf.to_dict(orient='list'),
                             'Years': [dt.datetime.strftime(l, '%Y') for l in cf2dispdf.index]}

                OverallSummary = entity['FinalSummary']["OverallSummary"]
                FinKPIs = {"Project_payback": OverallSummary['PaybackTime'], "Project_IRR": OverallSummary['IRR']}
                if entity.get('Financial'):
                    entity['Financial'].update({"CashFlow": blobname, "FinKPIs": FinKPIs})
                else:
                    entity.update({"Financial": {"CashFlow": blobname, "FinKPIs": FinKPIs}})

                ds.put(entity)
            else:
                OutputDic = {"AnnualCashFlow": dict(entity['FinalSummary']['CashFlow'])}

            return jsonify(json.loads(json.dumps(OutputDic))), 200

    blobname_configs = RESULTS + uniqueid + '_Configs.pkl'
    blobOpt = bucket.get_blob(blobname_configs)
    s = blobOpt.download_as_string()
    OptDf = pickle.loads(s)

    blobname_ts = RESULTS + uniqueid + '_TSConfigs.pkl'
    blobts = bucket.blob(blobname_ts)
    st = blobts.download_as_string()
    TSOptDf = pickle.loads(st)

    default_finparams = FINPARM['financial']
    FinData = {}
    # LOAD FROM DATASTORE - Taking care to perform a deep copy, as FinData will be updated with DataFrame
    if 'finparams' in entity:
        for t in entity['finparams']:
            FinData[t] = entity['finparams'][t]
    else:
        FinData = FINPARM['financial']  # Load from datastore if modified by user

    if 'EpriceTariffFile' in entity:
        # TariffDic = pickle.loads(bucket.get_blob(USER_UPLOADS +entity['EpriceTariffFile'] ).download_as_string())
        TariffDic = pickle.loads(bucket.get_blob(entity['EpriceTariffFile']).download_as_string())
        FinData.update({"PVTariffSummary": TariffDic['PVTariffSummary']})
        FinData.update({"YearlyTariff": TariffDic['YearlyTariff']})
    else:
        FinData.update({"PVTariffSummary": {"pvtariffAvg": 0, "pvtarifftotal": 0,
                                            "EpriceAvg": default_finparams['e_price'],
                                            "Epricetotal": default_finparams['e_price']}})
        YearlyTariff = pd.DataFrame(index=pd.date_range(start='1/1/2020', periods=8760, freq='1H'),
                                    columns=['PVTariff'])

        # Add electrical price by hour if avaialble
        YearlyTariff['PVTariff'] = 0
        FinData.update({"YearlyTariff": YearlyTariff})

    if 'selectedconfig' not in userinput:
        configsel = entity['SelectedConfig']['Config']
    else:
        configsel = userinput['selectedconfig']

    #    InvData = INVDF.loc[:,entity['Inverter_name']]
    #    PVModuleData = PVDF.loc[:,entity['PV_name']]
    confchklist = ['PVName', 'InvName', 'Ms', 'Mp']
    for ec in confchklist:
        if ec not in configsel:
            return jsonify({"error": "{} not set in config".format(ec)}), 400

    pvname = configsel['PVName']
    invname = configsel['InvName']
    Ms = configsel['Ms']
    Mp = configsel['Mp']
    location_data = Location(entity['Location']['latitude'], entity['Location']['longitude'],
                             tz=entity['Weather']['TZ'])

    ConfigTSTuple = (pvname, invname, Ms, Mp, 'PAC_tot')  # Extract TS of total AC power
    ConfigSummTuple = (pvname, invname, Ms, Mp)
    TSDfsel = TSOptDf[ConfigTSTuple]

    ACOut = pd.Series(np.roll(TSDfsel.values, int(location_data.tz), axis=0), index=TSDfsel.index)
    # ACOut = TSOptDf[ConfigTSTuple]
    ConfSummary = OptDf.loc[ConfigSummTuple]

    # Start cashflow calc from next month of the Ref_year - taken care of in CashFlowCalc already

    year = int(FinData['Ref_year'])
    nextmonth = dt.datetime.now().month + 1
    if 'month' in FinData:
        month = FinData['month']
    else:
        month = nextmonth

    SelFinData = fm.Project_fin_data(FinData, ConfSummary['CAPEX'], ConfSummary['OPEX'])

    [CashFlowDf, Fin_KPIs, error_msg_fin, _, _] = fm.CashFlowCalc(SelFinData, ACOut,
                                                                  startdate='1/{month}/{year}'.format(month=month,
                                                                                                      year=year),
                                                                  detailed=True)

    # Cash flow data to display in yearly resolutions
    cf2dispdf = CashFlowDf.groupby(pd.Grouper(freq='1Y')).sum()
    cf2dispdf['OPEX+Interest'] = -(cf2dispdf['NetBenefit'] - cf2dispdf['SavingsOnly'])  # +cf2dispdf['interests'])
    cf2dispdf['CAPEX'] = ConfSummary['CAPEX']
    cf2dispdf['CAPEX'].iloc[1:] = 0
    cf2dispdf['Net Cash Flow'] = cf2dispdf['FreeCashFlow']
    cf2dispdf['Net Cash Flow'].iloc[0] = cf2dispdf['Net Cash Flow'].iloc[0] - ConfSummary['CAPEX']

    blobname_ts = RESULTS + uniqueid + '_CashFlow.pkl'
    blob = bucket.blob(blobname_ts)
    blob.upload_from_string(pickle.dumps(CashFlowDf))

    entity.update({"status": "Results Summary"})
    if entity.get('Financial'):
        entity['Financial'].update({"CashFlow": blobname_ts, "FinKPIs": Fin_KPIs})
    else:
        entity.update({"Financial": {"CashFlow": blobname_ts, "FinKPIs": Fin_KPIs}})

    ds.put(entity)
    OutputDic = {"FinKPIs": Fin_KPIs, "CashFlow": CashFlowDf.to_dict(orient='list'),
                 'Timestamps': list(CashFlowDf.index.to_native_types())}

    OutputDic.update({"AnnualCashFlow": cf2dispdf.to_dict(orient='list'),
                      'Years': [dt.datetime.strftime(l, '%Y') for l in cf2dispdf.index]})
    return jsonify(OutputDic), 200


@app.route('/physical-layout', methods=['POST'])
@jwt_authenticated
def physicalLayout():
    ds = datastore.Client()
    gcs = storage.Client()
    bucket = gcs.bucket(BUCKET_NAME)
    req_json = request.get_json()['LandConst']
    userinput = req_json
    message = {}

    userinput['email'] = request.email
    orgname = request.org
    parent_key = ds.key('users', userinput['email'])
    entity = ds.get(key=ds.key('Project', userinput['project_id'], parent=parent_key))

    if not entity:
        message.update({"error": "No project with username {} and project {} exists".format(userinput['email'],
                                                                                            userinput['project_id'])})
        return jsonify(message), 400

    uniqueid = 'Uid_' + userinput['email'] + '_Pid_' + userinput['project_id']

    entchecklist = ['SelectedConfig', 'Location', 'OptMatrix']
    for ec in entchecklist:
        if ec not in entity:
            return jsonify({"error": "{} not set in datastore".format(ec)}), 400

    #    uschklist = []
    #    for us in uschklist:
    #        if us not in userinput:
    #            return jsonify({"error": "{} not set in input request".format(us)}),400 

    if "action" in userinput:
        if 'PanelCoords' in entity['Location']:
            blobname = entity['Location']['PanelCoords']
            if blobname:
                try:
                    PolyFile = json.loads(bucket.get_blob(blobname).download_as_string())
                    GeoPanCoords = PolyFile['GeoPanCoords']
                    NumP2Layout = PolyFile["NumPanels2Layout"]
                except:
                    GeoPanCoords = {}
                    NumP2Layout = 0
            else:
                GeoPanCoords = {}
                NumP2Layout = 0
        else:
            GeoPanCoords = {}
            NumP2Layout = 0
        return jsonify({"GeoPanCoords": GeoPanCoords, "NumPanels2Layout": NumP2Layout}), 200

    if 'selectedconfig' not in userinput:
        try:
            configsel = entity['SelectedConfig']['Config']
        except:
            message.update(
                {"error": " project {} does not have a selected configuration".format(userinput['project_id'])})
            return jsonify(message), 400
    else:
        configsel = userinput['selectedconfig']

    confchklist = ['PVName', 'InvName', 'Ms', 'Mp']
    for ec in confchklist:
        if ec not in configsel:
            return jsonify({"error": "{} not set in config".format(ec)}), 400

    pvname = configsel['PVName']
    invname = configsel['InvName']
    Ms = configsel['Ms']
    Mp = configsel['Mp']
    ConfigSummTuple = (pvname, invname, Ms, Mp)

    bucket = gcs.get_bucket(BUCKET_NAME)

    blobname_configs = RESULTS + uniqueid + '_Configs.pkl'
    blobOpt = bucket.get_blob(blobname_configs)
    s = blobOpt.download_as_string()
    OptDf = pickle.loads(s)

    uniqueid = 'Uid_' + userinput['email']

    blobname_pvmod = USER_UPLOADS + uniqueid + '_moduledata.pkl'
    blobpv = bucket.get_blob(blobname_pvmod)
    if blobpv:
        s = blobpv.download_as_string()
        USERPVDF = pickle.loads(s)

    ConfSummary = OptDf.loc[ConfigSummTuple]
    # Power_req = entity['ACpower']*1000
    NumBB = ConfSummary['NumBB']
    # InvData = INVDF.loc[:,invname]
    if pvname in PVDF:
        PVModuleData = PVDF.loc[:, pvname]
    elif pvname in PVSYSDF:
        PVModuleData = PVSYSDF.loc[:, pvname]
    elif blobpv:
        if pvname in USERPVDF:
            PVModuleData = USERPVDF.loc[:, pvname]
    else:
        return jsonify({"Error": "PV Module {} not found in any database".format(pvname)}), 400
    InvData = INVDF.loc[:, invname]

    # in some databases, PV module units are in mm 
    # convert all to m 
    if PVModuleData['LongSide'] / 1000 > 0.1:
        PVModuleData['LongSide'] = PVModuleData['LongSide'] / 1000
    if PVModuleData['ShortSide'] / 1000 > 0.1:
        PVModuleData['ShortSide'] = PVModuleData['ShortSide'] / 1000
    if np.isnan(PVModuleData['LongSide']):
        PVModuleData['LongSide'] = PVModuleData['A_c'] / 0.992
        PVModuleData['ShortSide'] = 0.992

    weatherblob = entity['Weather']['finalweatherfile']
    dataTMY = pickle.loads(bucket.get_blob(weatherblob).download_as_string())

    # PanStringArr,Ptuples =Opt.FeasibleStringRange(PVModuleData,InvData,dataTMY['Temperature'])
    # PaninStringKeys = np.array([x for x in PanStringArr.keys()]) 
    # NuminString = int(PaninStringKeys[np.argmin(np.abs(PaninStringKeys-Ms*Mp))])        
    # TODO : Show Distribution of panels on each area
    # NumP2Layout = Ms*Mp*NumBB       
    NumP2Layout = ConfSummary['NumPanels']  # NuminString*NumBB
    ####################

    GeoCoordFile = entity['Location']['GeoCoords']
    blobname = GeoCoordFile
    bucket = gcs.get_bucket(BUCKET_NAME)
    blob = bucket.get_blob(blobname)
    s = blob.download_as_string()
    LatPolyfromFile = json.loads(s)

    # if ('SpacingParams' not in userinput.keys()):    
    #     SpacingParams = entity['Location']['SpacingParams']
    # else:
    #     SpacingParams = userinput['SpacingParams']
    #     for vals in entity['Location']['SpacingParams']:
    #         if vals not in SpacingParams.keys():
    #             SpacingParams[vals] = entity['Location']['SpacingParams'][vals]
    # try:
    if 'AreaSpacingParams' in userinput:
        AreaSpacingParams = userinput['AreaSpacingParams']
        if AreaSpacingParams == {}:
            AreaSpacingParams = {AreaLabel: entity['Location']['SpacingParams'] for AreaLabel in LatPolyfromFile}
    elif 'AreaSpacingParams' in entity['Location']:
        AreaSpacingParams = entity['Location']['AreaSpacingParams']
    else:
        AreaSpacingParams = {AreaLabel: entity['Location']['SpacingParams'] for AreaLabel in LatPolyfromFile}

    Areas = dict()
    GeoPanOut = dict()
    RealPanOut = dict()
    NumPanels = dict()
    AreaFigures = {"Areawise": {}}
    NumPanels = dict()

    if entity['Location'].get('latitude', 1) > 0:
        # Align to Which Hemisphere ? - Align towards south if >0 , align towards north if <0
        Hemisphere = 'South'
    else:
        Hemisphere = 'North'

    for AreaLabel in LatPolyfromFile:
        Areas[AreaLabel] = Geo2M.Geo2M3D(LatPolyfromFile[AreaLabel])
        AreaAz = GridM.AzofEdges(Areas[AreaLabel])
        SpacingParams = AreaSpacingParams[AreaLabel]

        # if LatPolyfromFile[AreaLabel]['Ext'][0][0]>0:
        #      #RefVector = np.array([0,-1,0]) # Vector facing south
        #      #Hemisphere = 'South'
        #      if SpacingParams['Ti_P']!=0:
        #          SpacingParams['Ti_P']=abs(SpacingParams['Ti_P'])
        # else:
        #      #RefVector = np.array([0,1,0]) # Vector facing north
        #      #Hemisphere = 'North'
        #      if SpacingParams['Ti_P']!=0:
        #          SpacingParams['Ti_P']=abs(SpacingParams['Ti_P']) # Rotae the default panels in the opposite direction

        InstallPlanePoints = Areas[AreaLabel].Real2InPlane(
            Areas[AreaLabel].Coords)  # Project entire shape to installation surface in 2D
        OuterPoly = polygon.orient(Polygon(InstallPlanePoints))

        # Check OuterPoly for orientation            

        # OuterPolyWKeepOut = LinearRing(OuterPoly).parallel_offset(SpacingParams['setback'],"left",join_style=2) # Demarcate keepout areas all along the boundaries
        # OuterPolyWKeepOut = Polygon(OuterPoly).buffer(-SpacingParams['setback']) 
        # OuterPoly should always be anticlockwise oriented
        ProjHoles = [Areas[AreaLabel].Real2InPlane(HoleCoords) for HoleCoords in
                     Areas[AreaLabel].Holes]  # Project the holes also in installation plane
        OuterPolyWKeepOut = polygon.orient(Polygon(OuterPoly).buffer(-SpacingParams['setback']))
        zcoord = OuterPoly.exterior.coords[:][0][2]
        IntersectedHoles = []
        for holes in ProjHoles:
            if Polygon(holes).is_valid:
                IntersectedHoles.append(Polygon(holes).intersection(OuterPolyWKeepOut))
            else:
                IntersectedHoles.append(Polygon(holes).buffer(0).intersection(OuterPolyWKeepOut))

        if not OuterPolyWKeepOut.has_z:
            OuterPolyWKeepOut = Polygon(
                [(coord[0], coord[1], zcoord) for coord in OuterPolyWKeepOut.exterior.coords[:]])
            IntersectedHoles = []
            for holes in ProjHoles:
                IntersectedHoles.append(Polygon([(p[0], p[1], zcoord) for p in holes]).intersection(OuterPolyWKeepOut))
                ######

        S = Polygon(Polygon(OuterPolyWKeepOut).exterior.coords,
                    [Polygon(inner).exterior.coords for inner in (IntersectedHoles) if inner]).buffer(0)

        if type(S) == MultiPolygon:
            # S = Polygon(Polygon([s.exterior.coords[:] for s in S][0]).exterior.coords,[Polygon(inner).exterior.coords for inner in (IntersectedHoles) if inner]).buffer(0.01)
            # S = Polygon(Polygon(OuterPolyWKeepOut).exterior.coords,[Polygon(inner).exterior.coords for inner in (IntersectedHoles) if inner]).buffer(0.01)
            OuterPolyWKeepOut = Polygon(
                [(coord[0], coord[1], zcoord) for coord in OuterPolyWKeepOut.exterior.coords[:]])
            IntersectedHoles = []
            for holes in ProjHoles:
                if Polygon(holes).is_valid:
                    IntersectedHoles.append(
                        Polygon([(p[0], p[1], zcoord) for p in holes]).intersection(OuterPolyWKeepOut))
                else:
                    IntersectedHoles.append(
                        Polygon([(p[0], p[1], zcoord) for p in holes]).buffer(0).intersection(OuterPolyWKeepOut))
            S = Polygon(Polygon(OuterPolyWKeepOut).exterior.coords,
                        [Polygon(inner).exterior.coords for inner in (IntersectedHoles) if inner]).buffer(0.01)

        if not S.has_z:
            S = Polygon([(ex[0], ex[1], zcoord) for ex in S.exterior.coords[:]],
                        [LinearRing([(iN[0], iN[1], zcoord) for iN in p.coords[:]]) for p in S.interiors])

        # S = Polygon(Polygon(OuterPolyWKeepOut).exterior.coords,[Polygon(inner).exterior.coords for inner in (ProjHoles)]) # Prepare input polygon

        # AreaFigures for Cost calculations
        AreaFigures["Areawise"].update({AreaLabel: {"area": S.area,
                                                    "convexhullperimeter": S.convex_hull.length,
                                                    "netperimeter": S.length,
                                                    "tilt": Areas[AreaLabel].tilt,
                                                    "azimuth": Areas[AreaLabel].azimuth,
                                                    "height": max([z[2] for z in InstallPlanePoints])
                                                    }})

        if 'layout' not in SpacingParams:
            SpacingParams['layout'] = 'flat'  # flat, v or butterfly    --, v, /\
        # RefVector = np.array([0,-1,0]) # Southeward pointing vector
        if Areas[AreaLabel].tilt == 0:
            # Flat roof
            if SpacingParams['layout'] == 'flat':
                MultiPanelPolygons, SpacingParams = GridM.FillFlatAreaWithPanels(S, PVModuleData, SpacingParams,
                                                                                 Hemisphere)
                # MultiPanelPolygons = GridM.FillAreaWithPanels(S,PVModuleData,SpacingParams)
                AreaSpacingParams[AreaLabel] = SpacingParams
            else:
                MultiPanelPolygons, SpacingParams = GridM.FillFlatAreaWithVPanels(S, PVModuleData, SpacingParams,
                                                                                  Hemisphere)
                # MultiPanelPolygons = GridM.FillAreaWithPanels(S,PVModuleData,SpacingParams)
                AreaSpacingParams[AreaLabel] = SpacingParams
        else:
            # MultiPanelPolygons = GridM.FillAreaWithPanels_V2(S,PVModuleData,SpacingParams)  # Fill entire area with panels avoiding obstructions and respecting spacing params
            MultiPanelPolygons = GridM.FillAreaWithPanels_V3(S, PVModuleData, SpacingParams, AreaAz=AreaAz)
        NumPanels[AreaLabel] = len(MultiPanelPolygons)
        # GET PANELS REAL COORDINATES (m)
        RealPanList = []
        NumPanels[AreaLabel] = len(MultiPanelPolygons)
        for Pan in MultiPanelPolygons:
            RealPanList.append(Polygon(Areas[AreaLabel].InPlane2Real(
                Pan.exterior.coords[:])))  # Transform all panels to Real coordinate systems
        # RealPanOut[AreaLabel] = GridM.ShapelytoJson(RealPanList)
        # A = GridM.SurfAndPanelsPlot(Areas[AreaLabel].Coords,RealPanList)  

        # GET PANELS GEO COORDINATES (lat,lon)
        GeoPanList = []
        for realpan in RealPanList:
            GeoPanList.append(Polygon(Areas[AreaLabel].m2Geo(
                realpan.exterior.coords[:])))  # Convert values to geo location according to appropriate Area projection
        GeoPanOut[AreaLabel] = GridM.ShapelytoJson(GeoPanList)  # Create Json friendly objects for subsequent transfer

    emsg = "Successful fill"
    # AreaFigures.update({"TotalDCInOut":5}) # 5 m cable by default

    # except:
    #     emsg = "error in  shapes uploaded, files or other inputs"
    #     return jsonify({"error": emsg, "Shape stored in Db": LatPolyfromFile}),400

    #        nameofpicfile = 'solution_1.png'
    #        public_url = 'https://storage.googleapis.com/ecobucket/Results/solution_1.png'
    #       
    # ReturnDict ={"Errmsg": emsg ,"File2Display":str(RESULTS+nameofpicfile+'.png'),
    #            "PicPublicUrl":public_url}

    uniqueid = 'Uid_' + userinput['email'] + '_Pid_' + userinput['project_id']
    blobname = USER_UPLOADS + uniqueid + '_physicallayout.json'

    entity['Location'].update({"PanelCoords": blobname})
    # entity['Location'].update({"SpacingParams":SpacingParams})
    entity['Location'].update({"AreaSpacingParams": AreaSpacingParams})
    entity['Location'].update({"AreaFigures": AreaFigures})
    entity.update({"status": "Physical Layout"})
    ReturnDict = {"Errmsg": emsg, "GeoPanCoords": GeoPanOut, "RealPanCoords": RealPanOut,
                  "NumPanels2Layout": NumP2Layout,
                  "PowerPerPanelkW": PVModuleData['Wp'] / 1000}

    AreaDistribution = entity.get('SelectedConfig_mu', {}).get('AreaDistribution', {})
    if 'SelectedConfig_mu' in entity:
        for AreaLabel in AreaDistribution:
            AreaDistribution[AreaLabel] = min(AreaDistribution[AreaLabel], NumPanels[AreaLabel])
        NumP2Layout = int(np.sum([nump for nump in AreaDistribution.values()]))
        for AreaLabel in AreaDistribution:
            AreaDistribution[AreaLabel] = min(AreaDistribution[AreaLabel], NumPanels[AreaLabel])
            NumP2Layout = int(np.sum([nump for nump in AreaDistribution.values()]))
    ReturnDict.update({"AreaDistribution": AreaDistribution, "NumPanels2Layout": NumP2Layout})
    blob = bucket.blob(blobname).upload_from_string(json.dumps({"GeoPanCoords": GeoPanOut, "RealPanCoords": RealPanOut,
                                                                "NumPanels2Layout": NumP2Layout}),
                                                    content_type='application/json')

    ReturnDict.update({"AreaDistribution": AreaDistribution, "NumPanels2Layout": NumP2Layout})
    ds.put(entity)

    return jsonify(ReturnDict), 200


@app.route('/updatefrompanels', methods=['POST'])
@jwt_authenticated
def updatepanels():
    ds = datastore.Client()
    gcs = storage.Client()
    bucket = gcs.bucket(BUCKET_NAME)

    req_json = request.get_json()['Panels']
    userinput = req_json
    OutputDic = {}
    message = {}
    userinput['email'] = request.email
    orgname = request.org
    # {"PanelsPerArea":{"Area1":34,"Area2":324,"Area3":23}
    parent_key = ds.key('users', userinput['email'])
    entity = ds.get(key=ds.key('Project', userinput['project_id'], parent=parent_key))

    if not entity:
        message.update({"error": "No project with username {} and project {} exists".format(userinput['email'],
                                                                                            userinput['project_id'])})
        return jsonify(message), 400

    entchecklist = ['Location', 'Weather', 'SelectedConfig']
    for ec in entchecklist:
        if ec not in entity:
            return jsonify({"error": "{} not set in datastore".format(ec)}), 400

    uniqueid = 'Uid_' + userinput['email'] + '_Pid_' + userinput['project_id']

    ## USE DIFFERENT DB BASED ON ORG ###
    # qur = ds.query(kind='organisation')
    # qur.add_filter('users','=',userinput['email'])
    # Sol=list(qur.fetch())
    # if Sol != []:
    #     if os.environ['DB_NAME']!=Sol[0]['compdb']:
    #         os.environ['DB_NAME']= Sol[0]['compdb']
    #     orgname =  Sol[0].key.name    
    # else:        
    #     os.environ['DB_NAME'] = 'refdata'  
    orgentity = ds.get(key=ds.key('organisation', orgname))
    os.environ['DB_NAME'] = orgentity.get("compdb", "refdata")
    importlib.reload(CDB)
    # 
    OrgInfo = json.loads(bucket.get_blob(AUXFILES + "organisation_info.json").download_as_string())
    OrgDict = OrgInfo.get(orgname, OrgInfo['Agrola'])
    OrgBomInput = OrgDict.get('bominput', {})

    # Check if FinalSummary is already there 
    blobname = USER_UPLOADS + uniqueid + '_physicallayout.json'

    NumPanel = {}
    TotFromCoords = 0
    TotalPanels = 0
    if userinput.get('PanelsPerArea', {}) == {}:
        if 'NumPanelsPerArea' in entity['Location']:
            NumPanel.update(entity['Location']['NumPanelsPerArea'])
            TotalPanels = sum([Num for _, Num in NumPanel.items()])
        elif blobname:
            PanelCoords = json.loads(bucket.get_blob(blobname).download_as_string())
            TotalPanels = PanelCoords['NumPanels2Layout']
            for AreaLabel in PanelCoords['GeoPanCoords'].keys():
                NumPanel[AreaLabel] = len(PanelCoords['GeoPanCoords'][AreaLabel]['PANELS GEO'])
                TotFromCoords += len(PanelCoords['GeoPanCoords'][AreaLabel]['PANELS GEO'])
            if TotalPanels < TotFromCoords:
                Num2remove = TotFromCoords - TotalPanels
                for AreaLabel in PanelCoords['GeoPanCoords'].keys():  # for AreaLabel in ranked list of areas
                    PrevNum = NumPanel[AreaLabel]
                    NumPanel[AreaLabel] = max(NumPanel[AreaLabel] - Num2remove, 0)
                    Num2remove = max(Num2remove - (PrevNum - NumPanel[AreaLabel]), 0)
            else:
                TotalPanels = TotFromCoords
            entity['Location']['NumPanelsPerArea'] = NumPanel

    else:
        for AreaLabel in userinput['PanelsPerArea']:
            NumPanel[AreaLabel] = userinput['PanelsPerArea'][AreaLabel]
            TotalPanels += userinput['PanelsPerArea'][AreaLabel]
        entity['Location']['NumPanelsPerArea'] = NumPanel

    elecDesign = ELPARM
    tiltArea = entity['Location']['tilt']
    azimuthArea = entity['Location']['azimuth']
    if 'AreaSpacingParams' in entity['Location']:
        AreaSpacingParams = entity['Location']['AreaSpacingParams']
    else:
        AreaSpacingParams = {AreaLabel: elecDesign['systemparams']['SpacingParams'] for AreaLabel in tiltArea}
    if 'AreaFigures' in entity['Location']:
        AreaFigures = entity['Location']['AreaFigures']
    else:
        AreaFigures = {}

    CostData = entity['Cost']
    weatherblob = entity['Weather']['finalweatherfile']
    SelectedConfig = entity['SelectedConfig']
    St = ELPARM['PVstring']
    elecDesign = ELPARM

    dataTMY = pickle.loads(bucket.get_blob(weatherblob).download_as_string())

    location_data = Location(entity['Location']['latitude'], entity['Location']['longitude'],
                             tz=entity['Weather']['TZ'])

    PVName = SelectedConfig['Config']['PVName']
    InvName = SelectedConfig['Config']['InvName']
    Ms = SelectedConfig['Config']['Ms']
    Mp = SelectedConfig['Config']['Mp']
    TotalNumBB = int(SelectedConfig['ConfigResultsSummary']['NumBB'])

    emailuniqueid = 'Uid_' + userinput['email']
    blobname_pvmod = USER_UPLOADS + emailuniqueid + '_moduledata.pkl'
    blobpv = bucket.get_blob(blobname_pvmod)
    if blobpv:
        s = blobpv.download_as_string()
        USERPVDF = pickle.loads(s)

    # Get Inverter and Panel Data
    if PVName in PVDF:
        PVModuleData = PVDF.loc[:, PVName]
    elif PVName in PVSYSDF:
        PVModuleData = PVSYSDF.loc[:, PVName]
    elif blobpv:
        if PVName in USERPVDF:
            PVModuleData = USERPVDF.loc[:, PVName]

    InvData = INVDF.loc[:, InvName]

    blobname = entity['OptMatrix']['OptConfigFile']
    OptDf = pickle.loads(bucket.get_blob(blobname).download_as_string())

    blobname_ts = entity['OptMatrix']['TSConfigFile']
    ALLTSDf = pickle.loads(bucket.get_blob(blobname_ts).download_as_string())

    ######################## REPLACE WITH OPTIMISATION AND SMART SELECTION ########### ################
    ##################### IF ACTUAL PANELS IN PHYSICAL LAYOUT ARE DIFFERENT THAN ELECTRICAL ###########
    ################################################################################## ################

    SurfArea = entity['Location']['SurfArea']
    MaxSurfArea = np.sum([v for v in SurfArea.values()])
    MaxNumPanelsFromArea = int(MaxSurfArea / (PVModuleData.A_c))

    # SubArrayConfig = [(Ms,Mp) for _ in range(int(TotalNumBB))]
    MsMpConfigs = OptDf.loc[PVName, InvName].index
    DetailedArrayConfig = {}
    # Selected Config

    configsel = entity['SelectedConfig']['Config']
    pvname = configsel['PVName']
    invname = configsel['InvName']
    Ms = configsel['Ms']
    Mp = configsel['Mp']
    ConfigSummTuple = (pvname, invname, Ms, Mp)
    ConfigSummary = OptDf.loc[ConfigSummTuple]

    # SHould be same as in OptParamsRuns
    DC2ACmax = elecDesign['BoP_Design']['DC2ACmax']
    DC2ACmin = elecDesign['BoP_Design']['DC2ACmin']

    PanStringArr, Ptuples = Opt.FeasibleStringRange(PVModuleData, InvData, dataTMY['Temperature'], TotalNumPanels=0,
                                                    DCMinMax=[DC2ACmin, DC2ACmax])
    PaninStringKeys = np.array([x for x in PanStringArr.keys()])
    # NuminString = int(PaninStringKeys[np.argmin(np.abs(PaninStringKeys-Ms*Mp))])
    # TODO: #(which value comes closest to Ms*Mp label) - change index in future to num of panels in string for OptDf
    # RecNumPanels = NuminString*TotalNumBB    
    RecNumPanels = int(ConfigSummary['NumPanels'])
    # NuminString must be closest value   
    NuminString = int(PaninStringKeys[np.argmin(np.abs(PaninStringKeys - int(RecNumPanels / TotalNumBB)))])

    errorDic = {}
    if (RecNumPanels == TotalPanels):
        DetailedArrayConfig = {i: PanStringArr[NuminString] for i in range(1, TotalNumBB + 1)}
        TotalNumBB = int(max(TotalNumBB, 1))
        TotalPanels = int(max(TotalPanels, 1))
    elif TotalPanels in PaninStringKeys:
        TotalNumBB = 1  # int(max(TotalNumBB,1))
        # TotalPanels = int(max(TotalPanels,1))
        DetailedArrayConfig = {i: PanStringArr[TotalPanels] for i in range(1, TotalNumBB + 1)}
    else:
        if TotalPanels % (NuminString) == 0:
            TotalNumBB = int(max(TotalPanels / (NuminString), 1))  # Exact number of building blocks just lesser
            TotalPanels = int(max(TotalPanels, 1))
        else:
            # First try to adjust the difference in the last inverter
            # Change number of building blocks and therefore inverters based on number of panels
            NewNumBB = int(TotalPanels / (NuminString))
            PanelsinLastBB = TotalPanels - (NuminString) * (NewNumBB)
            TotalNumBB = int(max(NewNumBB + 1, 1))

            TotalPanels = int(max(TotalPanels, 1))
            # Ensure NumBBs dont change outside limits
            if (TotalPanels * PVModuleData.Wp) / (TotalNumBB * InvData.Paco) < DC2ACmin:
                # Just Make best Combo irrespective of 'best combo'
                TotalNumBB = int(np.ceil((TotalPanels * PVModuleData.Wp) / (
                            DC2ACmin * InvData.Paco)))  # max(int((TotalPanels*PVModuleData.Wp)/(InvData.Paco*DC2ACmin)),1) #max(NewNumBB,1)


            elif (TotalPanels * PVModuleData.Wp) / (TotalNumBB * InvData.Paco) > DC2ACmax:

                TotalNumBB = int(np.ceil((TotalPanels * PVModuleData.Wp) / (DC2ACmax * InvData.Paco)))

            if PanelsinLastBB in PanStringArr.keys():
                DetailedArrayConfig = {i: PanStringArr[NuminString] for i in range(1, TotalNumBB)}
                DetailedArrayConfig.update({TotalNumBB: PanStringArr[PanelsinLastBB]})

            else:
                # Error message to provide change recommendations
                errorDic = {"error": "Number of Panels Mismatch"}
                arr = (np.array([X - PanelsinLastBB for X in PanStringArr.keys()]))

                if len(arr[arr > 0]) > 0:  # Add some more panels
                    AddPanels = int(min(arr[arr > 0]))
                    errorDic.update({"Add": AddPanels, "Total if Add": TotalPanels + AddPanels})
                else:  # No possibility to add more panels exist
                    errorDic.update({"Add": TotalNumBB * (NuminString) + min(PanStringArr.keys()) - TotalPanels,
                                     "Total if Add": TotalNumBB * (NuminString) + min(PanStringArr.keys())})
                if len(arr[arr < 0]) > 0:  # Panel Removal option exists
                    RemovePanels = int(min(-arr[arr < 0]))
                    errorDic.update({"Remove": RemovePanels, "Total if Remove": TotalPanels - RemovePanels})
                else:  # No possibility to remove panels exist
                    errorDic.update({"Remove": PanelsinLastBB, "Total if Remove": NewNumBB * (NuminString)})
                errorDic.update({"Num Inverters": TotalNumBB})

                # Make a default PanelsinLastBB - which is the nearest

                DetailedArrayConfig = {i: PanStringArr[NuminString] for i in range(1, TotalNumBB)}
                DetailedArrayConfig.update({TotalNumBB: PanStringArr[arr[np.argmin(abs(arr))] + PanelsinLastBB]})
                ArrayPanels1 = arr[np.argmin(abs(arr))] + PanelsinLastBB + NuminString * (TotalNumBB - 1)
                err1 = abs(ArrayPanels1 - TotalPanels)
                # Alternative way without provided NuminString
                # Free Sizing               
                RemPanels = TotalPanels
                PaninString = []  # [arrP[np.argmin(abs(arr))]+PanelsinString]
                for i in range(TotalNumBB, 0, -1):
                    PanelsinString = int(RemPanels / (i))
                    arrP = (np.array([X - PanelsinString for X in PanStringArr.keys()]))
                    RemPanels = RemPanels - (arrP[np.argmin(abs(arrP))] + PanelsinString)
                    PaninString.append(arrP[np.argmin(abs(arrP))] + PanelsinString)
                AltDetailedArrayConfig = {i: PanStringArr[j] for i, j in zip(range(1, TotalNumBB + 1), PaninString)}
                err2 = RemPanels
                if err2 < err1:
                    DetailedArrayConfig = AltDetailedArrayConfig
    # Array values can only be strings not numbers               
    # entity.update(json.loads(json.dumps({"StringArrayConfig":{str(k): np.array_str(np.array(v,dtype=object)) for k,v in DetailedArrayConfig.items()}})))      
    entity.update(
        json.loads(json.dumps({"StringArrayConfig": {str(k): str(v) for k, v in DetailedArrayConfig.items()}})))

    # return jsonify(errorDic),400    

    ######## ============== BREAK AND RETURN ENERGY =================            
    if 'PanelsPerArea' in userinput:
        # Only make a check for total energy and string design
        NumBB = {}
        AreaResults = {}
        ACEnergy = 0
        DCEnergy = 0
        for AreaLabel in tiltArea:

            SpacingParams = AreaSpacingParams[AreaLabel]
            NumBB[AreaLabel] = (TotalNumBB / TotalPanels) * NumPanel[AreaLabel]

            # Convert to PVLib convention, clockwise +ve from 0 to 360° 
            # PVlib Convention - East of North. (S=180,N=0,E=90,W=270)
            # 3D drawing Convention - West of South (S=0,N=180,E=90,W=270)
            #
            # if tiltArea[AreaLabel]==0:    
            #     azimuth = azimuthArea[AreaLabel]+SpacingParams['Az_P'] # Azimuth Panels only for flat roofs
            #     tilt = tiltArea[AreaLabel]+SpacingParams['Ti_P']
            # else:
            #     azimuth = azimuthArea[AreaLabel]
            #     tilt = tiltArea[AreaLabel]

            if tiltArea[AreaLabel] == 0:
                azimuth = (-azimuthArea[AreaLabel]) + 180 - SpacingParams['Az_P']  # Only for flat roofs
                tilt = tiltArea[AreaLabel] + SpacingParams['Ti_P']  # TODO: SpacingParams[AreaLabel][Ti_P]
            else:
                azimuth = -azimuthArea[AreaLabel]  # +180-SpacingParams[AreaLabel]['Az_P']
                tilt = tiltArea[AreaLabel]

            # azimuth = 180-azimuth # Convert from Eest of South to East of North inside the function    
            # if azimuth<0:
            #     azimuth = -azimuth
            # else:
            #     azimuth = 360-azimuth

            PVString = PVsel.PVString(tilt, azimuth, St['Albedo'], Ms, Mp, NumBB[AreaLabel])

            TS, Agg = Opt.SingleEnergyCalc(dataTMY, location_data, PVModuleData, InvData, PVString, elecDesign,
                                           NumPanel[AreaLabel], SpacingParams)
            ACEnergy += Agg['ACEnergy'].values[0]
            DCEnergy += Agg['DCEnergy'].values[0]

        OverallSummary = {"ACEnergy": ACEnergy,
                          "DCEnergy": DCEnergy,
                          "NumPanels": TotalPanels,
                          "NumBB": TotalNumBB,
                          "DCPower": PVModuleData.Wp * TotalPanels / 1000,
                          "ACPower": InvData.Paco * TotalNumBB / 1e3}
        OverallSummary['StringConf'] = json.loads(
            json.dumps({str(i): {"Ms": DetailedArrayConfig[i][0][0], "Mp": int(np.sum([len(DetailedArrayConfig[i][mppt])
                                                                                       for mppt in range(
                    len(DetailedArrayConfig[i]))])), "NumBB": 1} for i in DetailedArrayConfig}))

        # i is num of Building blocks
        OverallSummary['StringConfDetailed'] = json.loads(json.dumps(
            {str(i): {"NumMPPT": len(DetailedArrayConfig[i]), "NumStringsperMPPT": [len(DetailedArrayConfig[i][mppt])
                                                                                    for mppt in
                                                                                    range(len(DetailedArrayConfig[i]))],
                      "PanelsinSeriesperString": [DetailedArrayConfig[i][mppt][0] for mppt in
                                                  range(len(DetailedArrayConfig[i]))],
                      "TotalParallelStrings": int(np.sum([len(DetailedArrayConfig[i][mppt])
                                                          for mppt in range(len(DetailedArrayConfig[i]))]))} for i in
             DetailedArrayConfig}))

        OutputDic = {"OverallSummary": OverallSummary,
                     "warningmismatch": errorDic
                     }
        ds.put(entity)

        return jsonify(OutputDic), 200

    ######## ============== BREAK AND RETURN ENERGY =================   

    """   
    if (RecNumPanels > TotalPanels): # Less Panels placed
        # Diff = RecNumPanels - TotalPanels
        PanelsinLastBB = TotalPanels%()
        TotalNumBBUpDate = int((TotalPanels/(Ms*Mp)))


        if PanelsinLastBB in PanStringArr.keys(): # If it is part of feasible combinations
            DetailedArrayConfig[TotalNumBBUpDate+1]=PanStringArr[PanelsinLastBB] # Modify the last BB
            TotalNumPanels=PanelsinLastBB+TotalNumBBUpDate*(Ms*Mp)
            TotalNumBBUpDate+=1
        # elif PanelsinLastBB ==0: 


        #     TotalNumBBUpDate = int((TotalPanels/(Ms*Mp)))
        #     TotalNumPanels = TotalNumBBUpDate*(Ms*Mp)

        else:

          #  arr = (np.array([PanelsinLastBB-X for X in PanStringArr.keys() if X<PanelsinLastBB]))
            arr = (np.array([PanelsinLastBB-X for X in PanStringArr.keys()] )) # Allow to add or remove
            MinPanelsinLastBB = min(PanStringArr.keys()) #TotalNumBBUpDate*(Ms*Mp)
            MaxPanelsinLastBB = max(PanStringArr.keys())
            InverterChange = 0
            errorDic = {}
            if arr.size==0:

                if PanelsinLastBB < MinPanelsinLastBB:
                    AddPanels = MinPanelsinLastBB-PanelsinLastBB
                    TotalNumPanels = TotalNumBBUpDate*(Ms*Mp)+MinPanelsinLastBB

                    return jsonify({"error":"Panel Number Mismatch - Too few panels for last inverter ","Add":AddPanels,"Total if Add":TotalNumPanels,
                                    "Remove":-PanelsinLastBB, "Total if Remove":TotalNumBBUpDate*(Ms*Mp)}),400
                elif PanelsinLastBB > MaxPanelsinLastBB:
                    RemovePanels = PanelsinLastBB-MaxPanelsinLastBB
                    TotalNumPanels =  TotalNumBBUpDate*(Ms*Mp)+MaxPanelsinLastBB                   

                    return jsonify({"error":"Panel Number Mismatch - Too many panels for last inverter","Remove":-RemovePanels,
                                    "Total if Remove":TotalNumPanels, "Add":MinPanelsinLastBB-RemovePanels,
                                    "Total if Add":(TotalNumBBUpDate+1)*(Ms*Mp)+MinPanelsinLastBB}),400
                return jsonify(errorDic),400    
            else:

                if len(arr[arr<0])>0:
                    AddPanels = int(-np.amax(arr[arr<0]))

                else:
                    AddPanels= MinPanelsinLastBB-PanelsinLastBB
                    InverterChange = 1

                if len(arr[arr>0])>0:     
                    RemovePanels = int(np.amin(arr[arr>0]))
                else:
                    RemovePanels = PanelsinLastBB-MaxPanelsinLastBB
                    InverterChange = -1
                TotalNumPanelsifAdd = TotalNumBBUpDate*(Ms*Mp)+PanelsinLastBB+AddPanels
                TotalNumPanelsifRemove = TotalNumBBUpDate*(Ms*Mp)+PanelsinLastBB-RemovePanels

                if InverterChange == 0:
                    return jsonify({"error":"Panel Number Mismatch - Too many / too few panels for last inverter","Remove":RemovePanels,
                                "Total if Remove":int(TotalNumPanelsifRemove), "Add":int(AddPanels),
                                "Total if Add":int(TotalNumPanelsifAdd)}),400
                else:
                    return jsonify({"error":"Panel Number Mismatch - Too many / too few panels for last inverter","Remove":RemovePanels,
                                "Total if Remove":int(TotalNumPanelsifRemove), "Add":int(AddPanels),
                                "Total if Add":int(TotalNumPanelsifAdd),"Inverter Num Change by":InverterChange}),400

                    #NewPanelsinLastBB = PanelsinLastBB-np.amin(arr)

            # Don't automatically modify number of panels
            # else:                             
            #     NewPanelsinLastBB = PanelsinLastBB-np.amin(arr)
            #     DetailedArrayConfig[TotalNumBBUpDate+1]=PanStringArr[NewPanelsinLastBB]
            #     TotalNumPanels = NewPanelsinLastBB+TotalNumBBUpDate*(Ms*Mp)
            #     TotalNumBBUpDate+=1

        TotalNumBB = int(max(TotalNumBBUpDate,1))
        TotalPanels = int(max(TotalNumPanels,1))      
    elif (RecNumPanels<TotalPanels): # More Panels are placed
        PanelsinLastBB = TotalPanels-RecNumPanels
        TotalNumBBUpDate = int((TotalPanels/(Ms*Mp)))
        DetailedArrayConfig = {i:PanStringArr[NuminString] for i in range(1,TotalNumBBUpDate+1)}
        if PanelsinLastBB in PanStringArr.keys():
            DetailedArrayConfig[TotalNumBBUpDate+1]= PanStringArr[PanelsinLastBB]

            TotalNumPanels=PanelsinLastBB+TotalNumBBUpDate*(Ms*Mp)
            TotalNumBBUpDate+=1

        elif PanelsinLastBB ==0: # One inverter needs to be added
            TotalNumBBUpDate = int((TotalPanels/(Ms*Mp)))
            TotalNumPanels = TotalNumBBUpDate*(Ms*Mp)

        else:
            #arr = (np.array([PanelsinLastBB-X for X in PanStringArr.keys() if X<PanelsinLastBB]))
            arr = (np.array([PanelsinLastBB-X for X in PanStringArr.keys()] )) # Allow to add or remove
            if arr.size==0:
                paneladjustment=0
                MinPanelsinLastBB = min(PanStringArr.keys()) #TotalNumBBUpDate*(Ms*Mp)
                MaxPanelsinLastBB = max(PanStringArr.keys())
                if PanelsinLastBB < MinPanelsinLastBB:
                    AddPanels = MinPanelsinLastBB-PanelsinLastBB
                    TotalNumPanels = TotalNumBBUpDate*(Ms*Mp)+MinPanelsinLastBB
                    return jsonify({"error":"Panel Number Mismatch - Too few panels for last inverter ","Add":AddPanels,"Total if Add":TotalNumPanels,
                                    "Remove":-PanelsinLastBB, "Total if Remove":TotalNumBBUpDate*(Ms*Mp)}),400
                elif PanelsinLastBB > MaxPanelsinLastBB : 
                    RemovePanels = PanelsinLastBB-MaxPanelsinLastBB
                    TotalNumPanels =  TotalNumBBUpDate*(Ms*Mp)+MaxPanelsinLastBB                   
                    return jsonify({"error":"Panel Number Mismatch - Too many panels for last inverter","Remove":-RemovePanels,
                                    "Total if Remove":TotalNumPanels, "Add":MinPanelsinLastBB-RemovePanels,
                                    "Total if Add":(TotalNumBBUpDate+1)*(Ms*Mp)+MinPanelsinLastBB}),400
            else:  
                if len(arr[arr<0])>0:
                    AddPanels = int(-np.amax(arr[arr<0]))
                else:
                    AddPanels=int(0)
                if len(arr[arr>0])>0:     
                    RemovePanels = int(np.amin(arr[arr>0]))
                else:
                    RemovePanels = int(0)
                TotalNumPanelsifAdd = TotalNumBBUpDate*(Ms*Mp)+PanelsinLastBB+AddPanels
                TotalNumPanelsifRemove = TotalNumBBUpDate*(Ms*Mp)+PanelsinLastBB-RemovePanels
                return jsonify({"error":"Panel Number Mismatch - Too many / too few panels for last inverter","Remove":int(RemovePanels),
                                "Total if Remove":int(TotalNumPanelsifRemove), "Add":int(AddPanels),
                                "Total if Add":int(TotalNumPanelsifAdd)}),400

                # paneladjustment = np.amin(arr)
                # NewPanelsinLastBB = PanelsinLastBB-paneladjustment
                # DetailedArrayConfig[TotalNumBB]=PanStringArr[int(NewPanelsinLastBB)] 
                # TotalNumPanels=PanelsinLastBB+TotalNumBBUpDate*(Ms*Mp)
                # TotalNumBBUpDate+=1
        TotalNumBB = int(max(TotalNumBBUpDate,1))
        TotalPanels = int(max(TotalNumPanels,1))
    else:
        DetailedArrayConfig = {i:PanStringArr[NuminString] for i in range(1,TotalNumBB+1)}
        TotalNumBB = int(max(TotalNumBB,1))
        TotalPanels = int(max(TotalPanels,1))

    """

    # STORE TotalNumBBUpDate, DetaileArrayConfig and TotalNumPanels in entity

    # if (RecNumPanels > TotalPanels): # Less Panels placed
    #     Diff = RecNumPanels - TotalPanels

    #     if Diff < NuminString: # Only last BB needs to change
    #         PaninLastBB = TotalPanels%(Ms*Mp)            
    #         SubArrayConfig.pop()
    #         SearchList = [(abs(M[0]*M[1]-PaninLastBB),M) for M in MsMpConfigs]
    #         SearchList.sort(key=lambda x:x[0])  
    #         if SearchList[0][0]!=0:
    #             if PaninLastBB ==0:
    #                 SubArrayConfig.append((PaninLastBB,0))
    #             else:    
    #                 SubArrayConfig.append((PaninLastBB,1))  
    #         else:
    #             LastBB = SearchList[0][1]
    #             SubArrayConfig.append(LastBB)
    #     else:   # Remove NumBBs    
    #         TEMPNumBB = (int(np.floor(TotalPanels/(Ms*Mp))))            

    #         SubArrayConfig = [(Ms,Mp) for _ in range(TEMPNumBB)]
    #         PaninLastBB = TotalPanels%(Ms*Mp)
    #         SearchList = [(abs(M[0]*M[1]-PaninLastBB),M) for M in MsMpConfigs]
    #         SearchList.sort(key=lambda x:x[0])
    #         if SearchList[0][0]!=0:
    #             if PaninLastBB ==0:
    #                 SubArrayConfig.append((PaninLastBB,0))
    #             else:    
    #                 SubArrayConfig.append((PaninLastBB,1))    
    #         else:
    #             LastBB = SearchList[0][1]
    #             SubArrayConfig.append(LastBB)
    #         TotalNumBB = len(SubArrayConfig)
    # else:
    #     Diff = TotalPanels-RecNumPanels
    #     PP = [(abs(M[0]*M[1]),M) for M in MsMpConfigs]
    #     PP.sort(key=lambda x:x[0])

    #     if Diff in range(PP[0][0],PP[-1][0]): # Add a BB 
    #         PaninLastBB = Diff #TotalPanels%(Ms*Mp)+NuminString     

    #         SearchList = [(abs(M[0]*M[1]-PaninLastBB),M) for M in MsMpConfigs]
    #         SearchList.sort(key=lambda x:x[0])
    #         if SearchList[0][0]!=0:
    #             if PaninLastBB ==0:
    #                 SubArrayConfig.append((PaninLastBB,0))
    #             else:    
    #                 SubArrayConfig.append((PaninLastBB,1))   
    #         else:
    #             LastBB = SearchList[0][1]
    #             SubArrayConfig.append(LastBB)

    #         TotalNumBB = len(SubArrayConfig)
    #     elif Diff < PP[0][0]: # is less than minumum then adjust last BB
    #         PaninLastBB = TotalPanels%(Ms*Mp)            

    #         SearchList = [(abs(M[0]*M[1]-PaninLastBB),M) for M in MsMpConfigs]
    #         SearchList.sort(key=lambda x:x[0])
    #         if SearchList[0][0]!=0:
    #             if PaninLastBB ==0:
    #                 SubArrayConfig.append((PaninLastBB,0))
    #             else:    
    #                 SubArrayConfig.append((PaninLastBB,1))   
    #         else:
    #             LastBB = SearchList[0][1]
    #             SubArrayConfig.append(LastBB)

    #         TotalNumBB = len(SubArrayConfig)

    #     elif Diff > PP[-1][0]: # is more than the maximum then
    #         TEMPNumBB = (int(np.floor(TotalPanels/(Ms*Mp))))            

    #         SubArrayConfig = [(Ms,Mp) for _ in range(TEMPNumBB)]
    #         PaninLastBB = TotalPanels%(Ms*Mp)
    #         SearchList = [(abs(M[0]*M[1]-PaninLastBB),M) for M in MsMpConfigs]
    #         SearchList.sort(key=lambda x:x[0])
    #         if SearchList[0][0]!=0:
    #             if PaninLastBB ==0:
    #                 SubArrayConfig.append((PaninLastBB,0))
    #             else:    
    #                 SubArrayConfig.append((PaninLastBB,1))   
    #         else:
    #             LastBB = SearchList[0][1]
    #             SubArrayConfig.append(LastBB)

    #         TotalNumBB = len(SubArrayConfig)

    ######################## REPLACE WITH OPTIMISATION AND SMART SELECTION ###########
    ##################################################################################
    St = ELPARM['PVstring']
    elecDesign = ELPARM

    if 'ActiveQuote' in entity:
        if 'ActiveQuote' in userinput:
            ActiveQuote = userinput['ActiveQuote']
        else:
            ActiveQuote = entity['ActiveQuote']
        #########################
        ### Cost from Db here
        ### 
        ViewQuoteDf = CDB.GetBoMQuote(userinput['project_id'], ActiveQuote['QuoteID'], ActiveQuote['RevID'])

        ViewQuoteDf.loc[~ViewQuoteDf['PerfModelKey'].isnull(), 'PerfModelKey'] = ViewQuoteDf.loc[
            ~ViewQuoteDf['PerfModelKey'].isnull(), 'PerfModelKey'].apply(lambda x: x.replace('\ufeff', '')).values
        CostDf = ViewQuoteDf[ViewQuoteDf['PerfModelKey'].isnull()]
        CostDf = pd.concat([CostDf, ViewQuoteDf[ViewQuoteDf['PerfModelKey'] == PVName]])
        CostDf = pd.concat([CostDf, ViewQuoteDf[ViewQuoteDf['PerfModelKey'] == InvName]])
        CostDf = pd.concat([CostDf, ViewQuoteDf[ViewQuoteDf['Group'] == 'Battery']])
        CostDf.drop_duplicates(subset=['BusinessIdentifierCode'], inplace=True)

        # Exclude Items from CAPEX or Business Case calculation
        CostDf = CDB.BoMExcludeFlags(CostDf, OrgBomInput=OrgBomInput)

        CostSelected = CostDf

    else:
        CostSelected = {'PVCost': CostData['PVCost'][PVName],
                        'InvCost': CostData['InvCost'][InvName]}
        CostSelected.update({k: v for k, v in CostData.items() if ((k != 'PVCost') and (k != 'InvCost'))})
        Curr = CostData['Curr']

    #########################

    FinData = {}
    if 'finparams' in entity:
        for t in entity['finparams']:
            FinData[t] = entity['finparams'][t]

    else:
        FinData = FINPARM['financial']
    default_finparams = FINPARM['financial']

    if 'EpriceTariffFile' in entity:
        # TariffDic = pickle.loads(bucket.get_blob(USER_UPLOADS +entity['EpriceTariffFile'] ).download_as_string())

        try:
            TariffDic = pickle.loads(bucket.get_blob(entity['EpriceTariffFile']).download_as_string())
        except:
            TariffDic = pickle.loads(bucket.get_blob(USER_UPLOADS + entity['EpriceTariffFile']).download_as_string())

        FinData.update({"PVTariffSummary": TariffDic['PVTariffSummary']})
        FinData.update({"YearlyTariff": TariffDic['YearlyTariff']})
    else:
        FinData.update({"PVTariffSummary": {"pvtariffAvg": 0, "pvtarifftotal": 0,
                                            "EpriceAvg": default_finparams['e_price'],
                                            "Epricetotal": default_finparams['e_price']}})
        YearlyTariff = pd.DataFrame(index=pd.date_range(start='1/1/2020', periods=8760, freq='1H'),
                                    columns=['PVTariff'])

        # Add electrical price by hour if avaialble
        YearlyTariff['PVTariff'] = 0
        FinData.update({"YearlyTariff": YearlyTariff})
    if 'DemandTS' in entity['Demand']:
        DemandTS = pickle.loads(bucket.get_blob(entity['Demand']['DemandTS']).download_as_string())
        FinData.update({"DemandTS": DemandTS})

    OverallSummary = {}

    NumBB = {}
    AreaResults = {}
    TotalCAPEX = 0
    TotalOPEX = 0
    TotalIncRad = 0
    OptDf = pd.DataFrame()

    SysAttribs = ['Vmp', 'Imp', 'Pmp', 'PAC_inv', 'PAC_tot', 'PDC']
    TEOutindex = pd.MultiIndex.from_product([range(Ms, Ms + 1), range(Mp, Mp + 1), SysAttribs],
                                            names=['Ms', 'Mp', 'Attrib'])
    TotalEOut = pd.DataFrame(index=dataTMY.index, columns=TEOutindex)
    TotalEOut[Ms, Mp, 'PDC'] = 0
    TotalEOut[Ms, Mp, 'PAC_tot'] = 0
    TotalEOut[Ms, Mp, 'IncRad'] = 0
    AggCriteria = ['DCEnergy', 'ACEnergy', 'kWh/kWp', 'LCOE', 'IRR', 'PaybackTime', 'CAPEX', 'OPEX',
                   'NumPanels', 'TotalIncRad', 'PR', 'Tilt', 'Azimuth', 'PanelOrientation']
    # Ensuring same AggDf indexing as that in OptiModule, even though only 1 Ms and Mp combo is used
    Aggindex = pd.MultiIndex.from_tuples([(Ms, Mp)], names=['Ms', 'Mp'])
    # AggDf = pd.DataFrame(index=Aggindex, columns = AggCriteria)

    # Chose different if sql db used or not
    if 'ActiveQuote' in entity:

        BoMindex = pd.MultiIndex.from_product([[a for a in tiltArea], [k for k in CostSelected['Id']]], sortorder=0)

        BoMDf = pd.DataFrame(index=BoMindex, columns=['TotalCost', 'Num', 'Id', 'Name', 'Unit', 'CostperUnit', 'Curr'])
    else:
        BoMindex = pd.MultiIndex.from_product([[a for a in tiltArea], [k for k in CostData.keys() if k != 'Curr']],
                                              sortorder=0)
        BoMDf = pd.DataFrame(index=BoMindex, columns=['TotalCost', 'Num', 'CostperUnit', 'Unit', 'Name'])

    LossChain = {}
    LossDf = pd.DataFrame(columns=['%', 'kWh', 'cum %'])

    day = 21
    dispmonth = {"spring": 4, "summer": 7, "autumn": 9, "winter": 12}
    Trends = {}
    TotalTrends = {}
    TStoDisp = {}
    TZ = int(entity['Weather']['TZ'])

    for AreaLabel in tiltArea:

        SpacingParams = AreaSpacingParams[AreaLabel]
        NumBB[AreaLabel] = (TotalNumBB / TotalPanels) * NumPanel[AreaLabel]

        # Convert to PVLib convention, clockwise +ve from 0 to 360° 
        # tilt = 0 Area is defined
        if tiltArea[AreaLabel] == 0:
            azimuth = (-azimuthArea[AreaLabel]) + 180 - SpacingParams['Az_P']  # Only for flat roofs
            tilt = tiltArea[AreaLabel] + SpacingParams['Ti_P']  # TODO: SpacingParams[AreaLabel][Ti_P]
        else:
            azimuth = -azimuthArea[AreaLabel]  # +180-SpacingParams[AreaLabel]['Az_P']
            tilt = tiltArea[AreaLabel]
        # if azimuth<0:
        #     azimuth = -azimuth
        # else:
        #     azimuth = 360-azimuth

        PVString = PVsel.PVString(tilt, azimuth, St['Albedo'], Ms, Mp, NumBB[AreaLabel])

        TSDf, AggDf, _, BoM = Opt.SingleCalc(dataTMY, location_data, PVModuleData, InvData, PVString, elecDesign,
                                             CostSelected.copy(),
                                             FinData, NumPanel[AreaLabel], SpacingParams)

        # BOM Not used for SQL db anymore, check if used for noSQL and deprecate
        # BoM : pd.DataFrame, if sql db is used
        # BoM : dict if noSQL used

        # TotalRadCollected[AreaLabel] =  PVcalc.WeatherDatatoDisplay(dataTMY,location_data,tiltArea[AreaLabel],azimuthArea[AreaLabel])
        AggDf = AggDf.astype(float)
        AggDf.dropna(axis=0, inplace=True)
        AggDf.loc[(Ms, Mp), 'NumPanels'] = NumPanel[AreaLabel]
        AggDf.loc[(Ms, Mp), 'Tilt'] = PVString.Tilt
        AggDf.loc[(Ms, Mp), 'Azimuth'] = PVString.Azimuth
        # if 'layout' in SpacingParams:
        #     if SpacingParams['layout']!='flat':
        #         PO = 'E-W'
        #     else:
        #         PO = 'N-S'
        # else:
        PO = SpacingParams['orientation']
        AggDf.loc[(Ms, Mp), 'PanelOrientation'] = PO

        # =====DEPRECATE . Needded only for nSQL db ======
        TotalCAPEX += AggDf.loc[(Ms, Mp), 'CAPEX']
        TotalOPEX += AggDf.loc[(Ms, Mp), 'OPEX']
        # ====== DEPRECATE ==================
        TotalIncRad += AggDf.loc[(Ms, Mp), 'TotalIncRad']

        TotalEOut[Ms, Mp, 'PAC_tot'] = TotalEOut[Ms, Mp, 'PAC_tot'] + TSDf[Ms, Mp, 'PAC_tot']

        TotalEOut[Ms, Mp, 'PDC'] = TotalEOut[Ms, Mp, 'PDC'].add(TSDf[Ms, Mp, 'PDC'])
        TotalEOut[Ms, Mp, 'IncRad'] = TotalEOut[Ms, Mp, 'IncRad'].add(TSDf[Ms, Mp, 'IncRad'])

        AreaResults[AreaLabel] = json.loads(AggDf.to_json(orient='records'))[0]

        # CAPEX Price Adjustments

        # TODO: Save BoM directly to BoMQuote in Db and eliminate the use of BoMDf here 

        #############
        if 'ActiveQuote' in entity:

            for ind in BoM['Id']:
                BoMDf.loc[(AreaLabel, ind), ['TotalCost', 'Num', 'Id', 'Name', 'Unit', 'CostperUnit', 'Curr']] = \
                    BoM.loc[
                        BoM['Id'] == ind, ['TotalCost', 'Quantity', 'Id', 'Name', 'Unit', 'Cost', 'Currency']].values
        else:
            for costitems in CostData.keys():
                if costitems != 'Curr':
                    BoMDf.loc[(AreaLabel, costitems)] = BoM.loc[costitems]
            BoMDf['Curr'] = Curr

        ############

        VIEW = 0
        Trends = {}
        for month in dispmonth:
            VIEW = TSDf.shift(TZ).loc[(TSDf.index.day == day) & (TSDf.index.month == dispmonth[month])] / 1000  # To kWh

            Trends[month] = {"PAC_tot": json.loads(VIEW[Ms, Mp, 'PAC_tot'].to_json(orient='values'))}
            Trends[month].update({"IncRad": json.loads(VIEW[Ms, Mp, 'IncRad'].to_json(orient='values'))})
            # Trends[month].update({"Tot":TSDf[Ms,Mp,'IncRad'].sum()/1000})

        TStoDisp[AreaLabel] = Trends
        LossChain[AreaLabel], LossChainkWhDf = PVcalc.LossChain(dataTMY, location_data, PVModuleData, InvData, PVString,
                                                                elecDesign)
        if len(LossDf.index) == 0:
            LossDf = pd.DataFrame(columns=['Items', '%', 'kWh'], index=LossChain[AreaLabel].keys())
            LossDf['Items'] = LossChainkWhDf['Items']
            LossDf['kWh'] = LossChainkWhDf['kWh'] * NumPanel[AreaLabel] / (
                        PVString.Ms * PVString.Mp)  # Convert per string energy to overall
        else:
            LossDf['kWh'] = LossDf['kWh'] + LossChainkWhDf['kWh'] * NumPanel[AreaLabel] / (PVString.Ms * PVString.Mp)
    # ends AreaLabel Loop here

    # if 'getvalue' in userinput:
    #     if 'ACenergy' in userinput['getvalue']:    
    #         return jsonify({"ACEnergy":TotalEOut[Ms,Mp,'PAC_tot'].sum()/1000}),200 

    # Put TotalEOut from all areas together for totalised trends
    for month in dispmonth:
        VIEW = TotalEOut.shift(TZ).loc[
                   (TotalEOut.index.day == day) & (TotalEOut.index.month == dispmonth[month])] / 1000
        TotalTrends[month] = {"PAC_tot": json.loads(VIEW[Ms, Mp, 'PAC_tot'].to_json(orient='values'))}
        TotalTrends[month].update({"IncRad": json.loads(VIEW[Ms, Mp, 'IncRad'].to_json(orient='values'))})
        # TotalTrends[month].update({"Tot":TotalEOut[Ms,Mp,'IncRad'].sum()/1000})
    TStoDisp['Total'] = TotalTrends
    #   Add LossChain Values Together from all Areas
    TotalLossChainDf = PVcalc.LossChainPerCent(LossDf)

    TotalLossChainDf.index = TotalLossChainDf.index.astype(str)

    # Build up final BOM

    ConfFinData = fm.Project_fin_data(FinData, TotalCAPEX, TotalOPEX)
    year = int(FinData['Ref_year'])
    nextmonth = dt.datetime.now().month + 1
    if nextmonth > 12:
        nextmonth = 1
        year += 1
    # Jump to next year if current year is over    
    if 'month' in FinData:
        month = FinData['month']
    else:
        month = nextmonth

    # Get Consumption values
    if 'DemandTS' in entity['Demand']:
        DemandTS = pickle.loads(bucket.get_blob(entity['Demand']['DemandTS']).download_as_string())

    # Delete below    
    # elif 'Battery' in entity:
    #     BattTSblob=entity['Battery']['TS']
    #     FinalTS = pickle.loads(bucket.get_blob(BattTSblob).download_as_string())
    #     DemandTS = FinalTS['Demand']
    #     Demandblobname = USER_UPLOADS+uniqueid+'_demandTS.pkl'
    #     bucket.blob(Demandblobname).upload_from_string(pickle.dumps(DemandTS))
    #     entity['Demand']['DemandTS'] = Demandblobname
    #     # Refresh BattTS
    #     BatterySpec = entity['Battery']['BatterySpec']

    else:
        DemandTS = TotalEOut[
            Ms, Mp, 'PAC_tot']  # if no demand values avaialble from battery step, assume all PV is consumed 

    # Financial Values Get
    if 'Adjustments' in entity['finparams']:
        Adjustments = entity['finparams']['Adjustments']
    else:
        Adjustments = {}

    ############### CALCULATE CAPEX HERE ############
    ### CALCULATE SOLAR ONLY AND STORE IN CONFIGRESULTSSUMMARY ####
    if 'nm' in FinData['PVTariffSummary']:
        nm = FinData['PVTariffSummary']['nm']
    else:
        nm = 0

    LimitC = lambda x, y: y if x > y else x
    CombinedTS = pd.DataFrame(index=DemandTS.index, columns=['Solar', 'SolarToUse', 'Demand', 'SolarToNet'])
    TSDfsel = TotalEOut[Ms, Mp, 'PAC_tot']

    CombinedTS['Solar'] = pd.Series(np.roll(TSDfsel.values, int(location_data.tz), axis=0), index=DemandTS.index)
    CombinedTS['Demand'] = DemandTS

    CombinedTS['SolarToUse'] = CombinedTS.apply(lambda x: LimitC(x.Solar, x.Demand), axis=1)
    CombinedTS['SolarToNet'] = CombinedTS['Solar'] - CombinedTS['SolarToUse']
    AddBack = 0  # To Add Back BC CAPEX costs

    if 'ActiveQuote' in entity:
        # if Quote is already edited (contains 'Edit' in RevID - just use the financial numbers, else 
        # Calculate the new CAPEX 
        if 'ActiveQuote' in userinput:
            ActiveQuote = userinput['ActiveQuote']
        else:
            ActiveQuote = entity['ActiveQuote']

        if 'Edit' in ActiveQuote['RevID']:
            VQuoteDfQuant = CDB.GetBoMQuote(userinput['project_id'], ActiveQuote['QuoteID'], ActiveQuote['RevID'])
            VQuoteDfQuant.fillna(np.nan, inplace=True)
            VQuoteDfQuant.replace([np.nan], [0], inplace=True)
            VQuoteDfQuant.set_index('Id', inplace=True)
            # Ensure - if Value is removed, Quantity=0 the Prices are not counted
            VQuoteDfQuant.loc[
                VQuoteDfQuant['Quantity'] == 0, ['TotalCost', 'NetPrice', 'TotalPrice', 'TotalBasePrice']] = 0
            # CAPEX EXCLUSIONS 
            VQuoteDfQuant = CDB.BoMExcludeFlags(VQuoteDfQuant, OrgBomInput=OrgBomInput)

            if 'ExcludeFromCAPEX' in VQuoteDfQuant.columns:
                TotalCAPEX = VQuoteDfQuant[VQuoteDfQuant['ExcludeFromCAPEX'] == 0]['NetPrice'].sum()  # CAPEX EXCLUSIONS
            else:
                TotalCAPEX = \
                VQuoteDfQuant[(VQuoteDfQuant['Group'] != 'Service') | ((VQuoteDfQuant['Group'] != 'Option'))][
                    'NetPrice'].sum()

            TotalOPEX = 0.01 * TotalCAPEX
            TotalAdjustments = 0

            for key, val in Adjustments.items():

                if 'Total' not in Adjustments[key]:
                    TotalAdjustments += Adjustments[key]['data']
                else:
                    TotalAdjustments += Adjustments[key]['Total']
            TotalCAPEX += TotalAdjustments

            if ('ExcludeFromCAPEX' in VQuoteDfQuant.columns) and ('IncludeforBC' in VQuoteDfQuant.columns):
                AddBack = VQuoteDfQuant.loc[
                    (VQuoteDfQuant['ExcludeFromCAPEX'] == 1) & (VQuoteDfQuant['IncludeforBC'] == 1), 'NetPrice'].sum()
                ConfFinData = fm.Project_fin_data(FinData, TotalCAPEX + AddBack, TotalOPEX)
            else:
                ConfFinData = fm.Project_fin_data(FinData, TotalCAPEX, TotalOPEX)


        else:

            # BattSeries = BATTDF.get(CostSelected.loc[CostSelected['Group']=='Battery','PerfModelKey'])
            # if not BattSeries.empty:
            #     #BattFromBoM = CostSelected.loc[CostSelected['Group']=='Battery']
            #     BatterySpec = BattSeries.to_dict()[BattSeries.columns[0]]
            #     # BatterySpec['NumCabinets'] = BattFromBoM['Quantity'].values[0]
            #     # BatterySpec['Enom'] = BatterySpec['NumCabinets']*BatterySpec['Enom']

            #     #BatterySpec['PmaxD'] = BatterySpec['NumCabinets']*BatterySpec['PmaxD'] in usual cases....

            #     CAPEX,OPEX,VQuoteDfQuant,AdjDict=fm.Cost_function_db(PVModuleData, InvData, CostSelected, Ms, Mp, TotalNumBB, TotalPanels,
            #                                              BatterySpec=BatterySpec,Adjustments=Adjustments,AreaFigures=AreaFigures)
            #     entity['Battery'].update({"BatterySpec":BatterySpec})
            # else:

            CAPEX, OPEX, VQuoteDfQuant, AdjDict = fm.Cost_function_db(PVModuleData, InvData, CostSelected.copy(), Ms,
                                                                      Mp, TotalNumBB, TotalPanels,
                                                                      Adjustments=Adjustments, AreaFigures=AreaFigures)
            for key, val in Adjustments.items():
                Adjustments[key]['Total'] = AdjDict[key]

            VQuoteDfQuant.fillna(np.nan, inplace=True)
            VQuoteDfQuant.replace([np.nan], [0], inplace=True)
            VQuoteDfQuant.set_index('Id', inplace=True)
            # Transfer this BoM to BoM  quote 
            ### BOM['Quantity'] is filled above based on unit functions
            ### Save BoM['Quantity'] value to be retrieved for BoM page
            # Name of Id is special for bindparams
            # BoM2Update = [{'_Id':int(idBom),'Quantity':float(VQuoteDfQuant.loc[idBom,'Quantity']),'TotalCost':float(VQuoteDfQuant.loc[idBom,'TotalCost'])} 
            #                           for idBom in VQuoteDfQuant.index]   
            # CDB.UpdateBoM(BoM2Update)

            TotalCAPEX = CAPEX
            TotalOPEX = OPEX
            # Add BC Inclusions if subtracted
            if ('ExcludeFromCAPEX' in VQuoteDfQuant.columns) and ('IncludeforBC' in VQuoteDfQuant.columns):
                AddBack = VQuoteDfQuant.loc[
                    (VQuoteDfQuant['ExcludeFromCAPEX'] == 1) & (VQuoteDfQuant['IncludeforBC'] == 1), 'NetPrice'].sum()
                ConfFinData = fm.Project_fin_data(FinData, CAPEX + AddBack, OPEX)
            else:
                ConfFinData = fm.Project_fin_data(FinData, CAPEX, OPEX)

    else:
        ConfFinData = fm.Project_fin_data(FinData, TotalCAPEX,
                                          TotalOPEX)  # FinData, TotalCAPEX and TotalOPEX are calulcalutd by default by summing over areas

    CashFlowDf, Proj_Fin_KPI, _, YearlyBenefit = fm.CashFlowCalc(ConfFinData, CombinedTS['Solar'],
                                                                 ACtoUse=CombinedTS['SolarToUse'],
                                                                 DemandTS=DemandTS,
                                                                 startdate='01/{month}/{year}'.format(month=month,
                                                                                                      year=year),
                                                                 detailed=False)  # PAC_tot in  Wh 

    # else:     
    #     CashFlowDf,Proj_Fin_KPI,_ = fm.CashFlowCalc(ConfFinData,CombinedTS['SolarToNet'],ACtoUse=CombinedTS['SolarToUse'],
    #                                             Demand=DemandTS,startdate='01/{month}/{year}'.format(month=month,year=year),
    #                                         detailed=True) #  PAC_tot in  Wh 

    # CashFlowDf,Proj_Fin_KPI,_ = fm.CashFlowCalc(ConfFinData,TotalEOut[Ms,Mp,'PAC_tot'],
    #                                             Demand=DemandTS,startdate='1/{month}/{year}'.format(month=month,year=year),
    #                                         detailed=True) #  PAC_tot in  Wh 
    #  Add sepearately in case of no battery - Selected Config only for Battery
    SelectedConfig['ConfigResultsSummary']['CAPEX'] = TotalCAPEX
    SelectedConfig['ConfigResultsSummary']['OPEX'] = TotalOPEX
    SelectedConfig['ConfigResultsSummary']['TotalYield'] = CashFlowDf['NetBenefit'].sum()
    SelectedConfig['ConfigResultsSummary']['IRR'] = Proj_Fin_KPI['Project_IRR']
    SelectedConfig['ConfigResultsSummary']['PaybackTime'] = Proj_Fin_KPI['Project_payback']

    OverallSummary['IRR'] = Proj_Fin_KPI['Project_IRR']
    OverallSummary['PaybackTime'] = Proj_Fin_KPI['Project_payback']

    # Battery  ..HERE QUANTITY NOT UPDATING
    # if 'Battery' in entity:
    if not BATTDF.get(CostSelected.loc[CostSelected['Group'] == 'Battery', 'PerfModelKey']).empty:
        # BatterySpec = entity['Battery']['BatterySpec']    
        BattSeries = BATTDF.get(CostSelected.loc[CostSelected['Group'] == 'Battery', 'PerfModelKey'])
        BatterySpec = BattSeries.to_dict()[BattSeries.columns[0]]
        BatterySpec['NumCabinets'] = CostSelected.loc[CostSelected['Group'] == 'Battery', 'Quantity'].values[0]
        BatterySpec['Enom'] = BatterySpec['NumCabinets'] * BatterySpec['Enom']
        BatterySpec['Emax'] = BatterySpec['NumCabinets'] * BatterySpec['Enom']
        BatterySpec['Type'] = CostSelected.loc[CostSelected['Group'] == 'Battery', 'PerfModelKey'].values[0]
        # BatterySpec['PmaxD'] = BatterySpec['NumCabinets']*BatterySpec['PmaxD'] in usual cases....
        if type(BatterySpec) != str:  # Battery is used  or BatterySpec != 'None'
            # Refresh BatteryTS with new data generation data   

            Emax = BatterySpec['Emax']

            Pmax = BatterySpec['PmaxD']
            # BattName = CostSelected.loc[CostSelected['Group']=='Battery','PerfModelKey'].values[0] #BatterySpec['Type']
            NumCabinets = BatterySpec['NumCabinets']
            ##########################################################
            # Get BatteryMod from Battery Type - BatterySpec['Type']
            # if BattName in BATTDF:
            #     BatteryMod = BATTDF[BattName]
            # else:    
            #     BatteryMod ={"E2P":2.5, 
            #                   "ChargeEff":0.98,"DischargeEff":0.96} 
            BatteryMod = BatterySpec
            ########################################################3
            BattBuck = ST.BucketModel(BatteryMod)
            TSDfsel = TotalEOut[Ms, Mp, 'PAC_tot']

            SolarTS = pd.Series(np.roll(TSDfsel.values, int(location_data.tz), axis=0), index=TSDfsel.index)
            SolarTS.index = DemandTS.index

            BatteryTS, MaxBattReqSize, KPIs = BattBuck.getBattProfile(DemandTS, SolarTS, Pmax=Pmax * 1000,
                                                                      Emax=Emax * 1000)  #
            #########################################################    
            TotalToUse = BatteryTS['TotalToUse']
            SolarToUse = BatteryTS['SolarToUse']
            TotalToNet = SolarTS - SolarToUse
            FinalTS = pd.concat([SolarTS.rename('Solar'), DemandTS, BatteryTS['Power'].rename('Battery')], axis=1)
            Fulfilment = (TotalToUse.sum() / DemandTS.sum())  # Same as Independance
            SelfConsumption = TotalToUse.sum() / SolarTS.sum()

            entity['Battery']['KPI'].update(
                {'SelfConsumptionWBatt': SelfConsumption * 100, "FulfilmentWBatt": Fulfilment * 100})

            # FinalTS.rename(columns={"Power":"Battery"},inplace=True)
            blobname = USER_UPLOADS + uniqueid + '_BattTS.pkl'
            bucket.blob(blobname).upload_from_string(pickle.dumps(FinalTS))

            # bucket.blob(blobname).upload_from_string(pickle.dumps(BatteryTS)) 
            # TODO: Add Battery Quantity and entry to BOMQuote 
            if 'ActiveQuote' in entity:
                # if Quote is already edited (contains 'Edit' in RevID - just use the financial numbers, else 
                # Calculate the new CAPEXes- 
                if 'ActiveQuote' in userinput:
                    ActiveQuote = userinput['ActiveQuote']
                else:
                    ActiveQuote = entity['ActiveQuote']

                if 'Edit' in ActiveQuote['RevID']:
                    VQuoteDfQuant = CDB.GetBoMQuote(userinput['project_id'], ActiveQuote['QuoteID'],
                                                    ActiveQuote['RevID'])
                    VQuoteDfQuant.fillna(np.nan, inplace=True)
                    VQuoteDfQuant.replace([np.nan], [0], inplace=True)
                    # VQuoteDfQuant.set_index('Id',inplace=True) 

                    # CAPEX EXCLUSIONS 
                    VQuoteDfQuant = CDB.BoMExcludeFlags(VQuoteDfQuant, OrgBomInput=OrgBomInput)

                    if 'ExcludeFromCAPEX' in VQuoteDfQuant.columns:
                        TotalCAPEX = VQuoteDfQuant[VQuoteDfQuant['ExcludeFromCAPEX'] == 0][
                            'NetPrice'].sum()  # CAPEX EXCLUSIONS
                    else:
                        TotalCAPEX = \
                        VQuoteDfQuant[(VQuoteDfQuant['Group'] != 'Service') | ((VQuoteDfQuant['Group'] != 'Option'))][
                            'NetPrice'].sum()

                    TotalOPEX = 0.01 * TotalCAPEX
                    TotalAdjustments = 0
                    for key, val in Adjustments.items():
                        if 'Total' not in Adjustments[key]:
                            TotalAdjustments += Adjustments[key]['data']
                        else:
                            TotalAdjustments += Adjustments[key]['Total']

                    TotalCAPEX += TotalAdjustments

                    if ('ExcludeFromCAPEX' in VQuoteDfQuant.columns) and ('IncludeforBC' in VQuoteDfQuant.columns):
                        AddBack = VQuoteDfQuant.loc[(VQuoteDfQuant['ExcludeFromCAPEX'] == 1) & (
                                    VQuoteDfQuant['IncludeforBC'] == 1), 'NetPrice'].sum()
                        ConfFinData = fm.Project_fin_data(FinData, TotalCAPEX + AddBack, TotalOPEX)

                    else:
                        ConfFinData = fm.Project_fin_data(FinData, TotalCAPEX, TotalOPEX)



                else:
                    # Calculate the actual CAPEX from cost functions
                    # BatterySpec = BATTDF.get(CostSelected.loc[CostSelected['Group']=='Battery','PerfModelKey'])
                    CAPEX, OPEX, VQuoteDfQuant, AdjDict = fm.Cost_function_db(PVModuleData, InvData,
                                                                              CostSelected.copy(), Ms, Mp, TotalNumBB,
                                                                              TotalPanels,
                                                                              BatterySpec=BatterySpec,
                                                                              Adjustments=Adjustments,
                                                                              AreaFigures=AreaFigures)

                    for key, val in Adjustments.items():
                        Adjustments[key]['Total'] = AdjDict[key]

                    VQuoteDfQuant.fillna(np.nan, inplace=True)
                    VQuoteDfQuant.replace([np.nan], [0], inplace=True)
                    VQuoteDfQuant.set_index('Id', inplace=True)
                    # Transfer this BoM to BoM  quote 
                    ### BOM['Quantity'] is filled above based on unit functions
                    ### Save BoM['Quantity'] value to be retrieved for BoM page
                    # Name of Id is special for bindparams
                    BoM2Update = [{'_Id': int(idBom), 'Quantity': float(VQuoteDfQuant.loc[idBom, 'Quantity']),
                                   'TotalCost': float(VQuoteDfQuant.loc[idBom, 'TotalCost']),
                                   'NetPrice': float(VQuoteDfQuant.loc[idBom, 'NetPrice']),
                                   'PriceAdjustment': float(VQuoteDfQuant.loc[idBom, 'PriceAdjustment']),
                                   'TotalPrice': float(VQuoteDfQuant.loc[idBom, 'TotalPrice']),
                                   'TotalBasePrice': float(VQuoteDfQuant.loc[idBom, 'TotalBasePrice'])} \
                                  for idBom in VQuoteDfQuant.index]
                    CDB.UpdateBoM(BoM2Update)

                    TotalCAPEX = CAPEX
                    TotalOPEX = OPEX

                    if ('ExcludeFromCAPEX' in VQuoteDfQuant.columns) and ('IncludeforBC' in VQuoteDfQuant.columns):
                        AddBack = VQuoteDfQuant.loc[(VQuoteDfQuant['ExcludeFromCAPEX'] == 1) & (
                                    VQuoteDfQuant['IncludeforBC'] == 1), 'NetPrice'].sum()
                        ConfFinData = fm.Project_fin_data(FinData, TotalCAPEX + AddBack, TotalOPEX)
                    else:
                        ConfFinData = fm.Project_fin_data(FinData, TotalCAPEX, TotalOPEX)


            else:
                BoMDf.loc[('Area1', 'BatteryCost'), 'Num'] = BatterySpec['NumCabinets']
                BoMDf.loc[('Area1', 'BatteryCost'), 'CostperUnit'] = BatterySpec['Price']
                BoMDf.loc[('Area1', 'BatteryCost'), 'Unit'] = 'Unit'
                BoMDf.loc[('Area1', 'BatteryCost'), 'Name'] = BatterySpec['Type']
                BoMDf.loc[('Area1', 'BatteryCost'), 'TotalCost'] = BatterySpec['NumCabinets'] * BatterySpec['Price']
                TotalCAPEX += BoMDf.loc[('Area1', 'BatteryCost'), 'TotalCost']
                TotalOPEX += 0.01 * BoMDf.loc[('Area1', 'BatteryCost'), 'TotalCost']
                ConfFinData = fm.Project_fin_data(FinData, TotalCAPEX, TotalOPEX)

            # if 'ActiveQuote' in entity:
            #     CAPEX,OPEX,VQuoteDfQuant=fm.Cost_function_db(PVModuleData, InvData, ViewQuoteDf, Ms, Mp, TotalNumBB, TotalPanels,BatterySpec=BatterySpec,Adjustments=Adjustments)
            #     #TotalBoM = BoM
            #     #TotalBoM = BoMDf[['TotalCost','Num']].groupby(level=[1]).sum() # Sum up BoM values by arealabels
            #     #TotalBoM.rename(columns={'Num':'Quantity'},inplace=True)
            #     VQuoteDfQuant.fillna(np.nan,inplace=True)
            #     VQuoteDfQuant.replace([np.nan],[0],inplace=True)
            #     VQuoteDfQuant.set_index('Id',inplace=True)    
            #          # Transfer this BoM to BoM  quote 
            #         ### BOM['Quantity'] is filled above based on unit functions
            #         ### Save BoM['Quantity'] value to be retrieved for BoM page
            #         # Name of Id is special for bindparams
            #     BoM2Update = [{'_Id':int(idBom),'Quantity':float(VQuoteDfQuant.loc[idBom,'Quantity']),'TotalCost':float(VQuoteDfQuant.loc[idBom,'TotalCost'])} 
            #                               for idBom in VQuoteDfQuant.index]   
            #     CDB.UpdateBoM(BoM2Update)
            #         # BoM updated with quantities and Total Cost across all areas        
            #     #########    

            # Add battery capex

            # CashFlowDf,Proj_Fin_KPI,_,YearlyBenefit = fm.CashFlowCalc(ConfFinData,TotalToNet,ACtoUse=TotalToUse,DemandTS=DemandTS,
            #                                             startdate='01/{month}/{year}'.format(month=month,year=year),
            #                                         detailed=False)
            CashFlowDf, Proj_Fin_KPI, _, YearlyBenefit = fm.CashFlowCalc(ConfFinData, SolarTS, ACtoUse=TotalToUse,
                                                                         DemandTS=DemandTS,
                                                                         startdate='01/{month}/{year}'.format(
                                                                             month=month, year=year),
                                                                         detailed=False)

            # Add only if Battery is present
            OverallSummary['BatteryName'] = BatterySpec['Type']
            OverallSummary['BattkWh'] = BatterySpec['NumCabinets'] * BatterySpec['Emax']
            OverallSummary['IRR'] = Proj_Fin_KPI['Project_IRR']
            OverallSummary['PaybackTime'] = Proj_Fin_KPI['Project_payback']
            # Calculate No Battery Equivalents



        else:
            # Update BoM with Solar Only VQuoteDfQuant
            # VQuoteDfQuant.fillna(np.nan,inplace=True)
            # VQuoteDfQuant.replace([np.nan],[0],inplace=True)
            # VQuoteDfQuant.set_index('Id',inplace=True)   
            BoM2Update = [{'_Id': int(idBom), 'Quantity': float(VQuoteDfQuant.loc[idBom, 'Quantity']),
                           'TotalCost': float(VQuoteDfQuant.loc[idBom, 'TotalCost']),
                           'NetPrice': float(VQuoteDfQuant.loc[idBom, 'NetPrice']),
                           'PriceAdjustment': float(VQuoteDfQuant.loc[idBom, 'PriceAdjustment']), \
                           'TotalPrice': float(VQuoteDfQuant.loc[idBom, 'TotalPrice']),
                           'TotalBasePrice': float(VQuoteDfQuant.loc[idBom, 'TotalBasePrice'])} \
                          for idBom in VQuoteDfQuant.index]
            CDB.UpdateBoM(BoM2Update)

            OverallSummary['IRR'] = Proj_Fin_KPI['Project_IRR']
            OverallSummary['PaybackTime'] = Proj_Fin_KPI['Project_payback']

            CombinedTS['Solar'] = pd.Series(np.roll(TSDfsel.values, int(location_data.tz), axis=0),
                                            index=DemandTS.index)
            CombinedTS['Demand'] = DemandTS
            # Update with No Battery TS

            BatteryTS, _, KPIs = BattBuck.getBattProfile(CombinedTS['Demand'], CombinedTS['Solar'], Pmax=0, Emax=0)  #
            blobname = USER_UPLOADS + uniqueid + '_BattTS.pkl'
            FinalTS = pd.concat([SolarTS, DemandTS, BatteryTS['Power']], axis=1)
            FinalTS.rename(columns={"Power": "Battery"})
            blobname = USER_UPLOADS + uniqueid + '_BattTS.pkl'
            bucket.blob(blobname).upload_from_string(pickle.dumps(FinalTS))

            # in case of no battery - SolarOnly to Use
            # LimitC  = lambda x,y : y if x>y else x
            # CombinedTS = pd.DataFrame(index=DemandTS.index,columns=['Solar','SolarToUse','Demand','SolarToNet'])
            # CombinedTS['Solar'] = TotalEOut[Ms,Mp,'PAC_tot'].values
            # CombinedTS['Demand'] = DemandTS
            # CombinedTS['SolarToUse'] = CombinedTS.apply(lambda x:LimitC(x.Solar,x.Demand),axis=1)
            # CombinedTS['SolarToNet'] = CombinedTS['Solar'] - CombinedTS['SolarToUse']
            # if 'ActiveQuote' in entity:
            #     CAPEX,OPEX,VQuoteDfQuant=fm.Cost_function_db(PVModuleData, InvData, CostSelected, Ms, Mp, TotalNumBB, TotalPanels,
            #                                                  Adjustments=Adjustments,AreaFigures=AreaFigures)

            #     VQuoteDfQuant.fillna(np.nan,inplace=True)
            #     VQuoteDfQuant.replace([np.nan],[0],inplace=True)
            #     VQuoteDfQuant.set_index('Id',inplace=True)    
            #          # Transfer this BoM to BoM  quote 
            #         ### BOM['Quantity'] is filled above based on unit functions
            #         ### Save BoM['Quantity'] value to be retrieved for BoM page
            #         # Name of Id is special for bindparams
            #     BoM2Update = [{'_Id':int(idBom),'Quantity':float(VQuoteDfQuant.loc[idBom,'Quantity']),'TotalCost':float(VQuoteDfQuant.loc[idBom,'TotalCost'])} 
            #                               for idBom in VQuoteDfQuant.index]   
            #     CDB.UpdateBoM(BoM2Update)
            #     TotalCAPEX = CAPEX
            #     TotalOPEX = OPEX
            #     ConfFinData = fm.Project_fin_data(FinData,CAPEX,OPEX)

            # CashFlowDf,Proj_Fin_KPI,_ = fm.CashFlowCalc(ConfFinData,CombinedTS['SolarToNet'],ACtoUse=CombinedTS['SolarToUse'],
            #                                             Demand=DemandTS,startdate='1/{month}/{year}'.format(month=month,year=year),
            #                                         detailed=True) #  PAC_tot in  Wh 

            # #  Add sepearately in case of no battery - Selected Config only for Battery
            # SelectedConfig['ConfigResultsSummary']['CAPEX']=TotalCAPEX
            # SelectedConfig['ConfigResultsSummary']['IRR'] = Proj_Fin_KPI['Project_IRR'] 
            # SelectedConfig['ConfigResultsSummary']['PaybackTime'] = Proj_Fin_KPI['Project_payback']

    else:
        # Update BoM with Solar Only VQuoteDfQuant
        # VQuoteDfQuant.fillna(np.nan,inplace=True)
        # VQuoteDfQuant.replace([np.nan],[0],inplace=True)
        # VQuoteDfQuant.set_index('Id',inplace=True)  
        BoM2Update = [{'_Id': int(idBom), 'Quantity': float(VQuoteDfQuant.loc[idBom, 'Quantity']),
                       'TotalCost': float(VQuoteDfQuant.loc[idBom, 'TotalCost']),
                       'NetPrice': float(VQuoteDfQuant.loc[idBom, 'NetPrice']),
                       'PriceAdjustment': float(VQuoteDfQuant.loc[idBom, 'PriceAdjustment']),
                       'TotalPrice': float(VQuoteDfQuant.loc[idBom, 'TotalPrice']),
                       'TotalBasePrice': float(VQuoteDfQuant.loc[idBom, 'TotalBasePrice'])} \
                      for idBom in VQuoteDfQuant.index]
        CDB.UpdateBoM(BoM2Update)
        OverallSummary['IRR'] = Proj_Fin_KPI['Project_IRR']
        OverallSummary['PaybackTime'] = Proj_Fin_KPI['Project_payback']
        CombinedTS['Solar'] = pd.Series(np.roll(TSDfsel.values, int(location_data.tz), axis=0), index=DemandTS.index)
        CombinedTS['Demand'] = DemandTS
        # Update with No Battery TS
        CombinedTS['Battery'] = DemandTS * 0
        blobname = USER_UPLOADS + uniqueid + '_BattTS.pkl'
        bucket.blob(blobname).upload_from_string(pickle.dumps(CombinedTS))

    ALLTSDf[(PVName, InvName, Ms, Mp, 'PAC_tot')] = TotalEOut[Ms, Mp, 'PAC_tot'].values
    ALLTSDf[(PVName, InvName, Ms, Mp, 'PDC')] = TotalEOut[Ms, Mp, 'PDC'].values

    bucket.blob(blobname_ts).upload_from_string(pickle.dumps(ALLTSDf))
    BoMblobname = RESULTS + uniqueid + '_BOM.pkl'
    bucket.blob(BoMblobname).upload_from_string(pickle.dumps(BoMDf))

    blobname = RESULTS + uniqueid + '_YearlyBenefits.pkl'
    bucket.blob(blobname).upload_from_string(pickle.dumps(YearlyBenefit))

    # Update BatteryKPI values

    if entity.get('Financial'):
        entity['Financial'].update({'YearlyBenefit': blobname})
    else:
        entity.update({"Financial": {'YearlyBenefit': blobname}})

    ######### MONTHLY ENERGY FOR DISPLAY#############
    monthlyenergy = pd.DataFrame()
    monthlyenergy['PAC_tot'] = (ALLTSDf[(PVName, InvName, Ms, Mp, 'PAC_tot')].groupby(
        pd.Grouper(freq='1M')).sum()) / 1000  # in kWh
    monthlyenergy['kWh/kWp'] = monthlyenergy['PAC_tot'] / (PVModuleData.Wp * TotalPanels / 1000)
    monthlyenergy['month'] = [dt.datetime.strftime(d, '%b') for d in monthlyenergy.index]
    monthlyenergy['PDC'] = TotalEOut[Ms, Mp, 'PDC'].groupby(pd.Grouper(freq='1M')).sum() / 1000
    monthlyenergy['IncRad'] = TotalEOut[Ms, Mp, 'IncRad'].groupby(pd.Grouper(freq='1M')).sum() / 1000

    energydisplay = {"Energydisplay": {
        "monthlyenergy": {"TotalEnergy": [*monthlyenergy['PAC_tot']], "kWh/kWp": [*monthlyenergy['kWh/kWp']],
                          "month": [*monthlyenergy['month']],
                          "DCEnergy": [*monthlyenergy['PDC']],
                          "IncRad": [*monthlyenergy['IncRad']]}}}
    #################################################

    OverallSummary['ACEnergy'] = TotalEOut[Ms, Mp, 'PAC_tot'].sum() / 1000  # to kWh     
    OverallSummary['kWh/kWp'] = OverallSummary['ACEnergy'] * 1000 / (PVModuleData.Wp * TotalPanels)
    OverallSummary['DCEnergy'] = TotalEOut[Ms, Mp, 'PDC'].sum() / 1000

    OverallSummary['ACPower'] = InvData.Paco * TotalNumBB / 1e3
    OverallSummary['LCOE'] = fm.calc_LCOE(TotalCAPEX, TotalOPEX, OverallSummary['ACEnergy'])
    OverallSummary['DC2AC'] = (PVModuleData.Wp * TotalPanels) / (InvData.Paco * TotalNumBB)
    OverallSummary['DCPower'] = PVModuleData.Wp * TotalPanels / 1000

    OverallSummary['IRR'] = Proj_Fin_KPI['Project_IRR']
    OverallSummary['PaybackTime'] = Proj_Fin_KPI['Project_payback'] / 12  # in years
    OverallSummary['kWh/m2'] = (TotalEOut[Ms, Mp, 'PAC_tot'].sum() / 1000) / (TotalPanels * PVModuleData.A_c)

    OverallSummary['CAPEX'] = TotalCAPEX
    OverallSummary['OPEX'] = TotalOPEX
    OverallSummary['EffCAPEX'] = TotalCAPEX + AddBack

    OverallSummary['NumPanels'] = TotalPanels
    OverallSummary['NumBB'] = TotalNumBB
    ModEff = PVModuleData.Wp / (1000 * PVModuleData.A_c)
    OverallSummary['PR'] = TotalEOut[Ms, Mp, 'PAC_tot'].sum() / (TotalIncRad * ModEff * 1000)
    OverallSummary['TotalIncRad'] = TotalIncRad / 1000  # Convert to kWh
    OverallSummary['PVName'] = PVName.replace('_', ' ')
    OverallSummary['InvName'] = InvName.replace('_', ' ')

    SelectedConfig['ConfigResultsSummary']['LCOE'] = fm.calc_LCOE(SelectedConfig['ConfigResultsSummary']['CAPEX'],
                                                                  SelectedConfig['ConfigResultsSummary']['OPEX'],
                                                                  OverallSummary['ACEnergy'])
    # Solar only KPIs below
    SelectedConfig['ConfigResultsSummary']['DCPower'] = OverallSummary['DCPower']
    SelectedConfig['ConfigResultsSummary']['ACPower'] = OverallSummary['ACPower']
    SelectedConfig['ConfigResultsSummary']['ACEnergy'] = OverallSummary['ACEnergy']
    SelectedConfig['ConfigResultsSummary']['DC2AC'] = OverallSummary['DC2AC']

    SelectedConfig['ConfigResultsSummary']['DC2AC'] = OverallSummary['DC2AC']
    SelectedConfig['ConfigResultsSummary']['NumBB'] = OverallSummary['NumBB']
    SelectedConfig['ConfigResultsSummary']['NumPanels'] = OverallSummary['NumPanels']
    SelectedConfig['ConfigResultsSummary']['kWh/kWp'] = OverallSummary['kWh/kWp']
    SelectedConfig['ConfigResultsSummary']['kWh/m2'] = OverallSummary['kWh/m2']
    # OverallSummary['StringConf'] = json.loads( json.dumps({s:{"Ms":M[0],"Mp":M[1],"NumBB":SubArrayConfig.count(M)} for
    #                                  M,s in zip(set(SubArrayConfig),range(len(set(SubArrayConfig))))} ))
    OverallSummary['StringConf'] = json.loads(
        json.dumps({str(i): {"Ms": DetailedArrayConfig[i][0][0], "Mp": int(np.sum([len(DetailedArrayConfig[i][mppt])
                                                                                   for mppt in range(
                len(DetailedArrayConfig[i]))])), "NumBB": 1} for i in DetailedArrayConfig}))

    # i is num of Building blocks
    OverallSummary['StringConfDetailed'] = json.loads(json.dumps(
        {str(i): {"NumMPPT": len(DetailedArrayConfig[i]), "NumStringsperMPPT": [len(DetailedArrayConfig[i][mppt])
                                                                                for mppt in
                                                                                range(len(DetailedArrayConfig[i]))],
                  "PanelsinSeriesperString": [DetailedArrayConfig[i][mppt][0] for mppt in
                                              range(len(DetailedArrayConfig[i]))],
                  "TotalParallelStrings": int(np.sum([len(DetailedArrayConfig[i][mppt])
                                                      for mppt in range(len(DetailedArrayConfig[i]))]))} for i in
         DetailedArrayConfig}))

    # cf2dispdf = CashFlowDf.groupby(pd.Grouper(freq='1Y')).sum()
    # cf2dispdf['OPEX+Interest'] = -(cf2dispdf['NetBenefit']-cf2dispdf['SavingsOnly']+cf2dispdf['interests'])
    # cf2dispdf['CAPEX'] = ConfSummary['CAPEX']
    # cf2dispdf['CAPEX'].iloc[1:]=0
    # cf2dispdf['Net Cash Flow'] = cf2dispdf['FreeCashFlow']
    # cf2dispdf['Net Cash Flow'].iloc[0] = cf2dispdf['Net Cash Flow'].iloc[0]-ConfSummary['CAPEX']

    blobname_ts = RESULTS + uniqueid + '_CashFlow.pkl'
    blob = bucket.blob(blobname_ts)
    blob.upload_from_string(pickle.dumps(CashFlowDf))

    OutputDic.update({"AreaWise": AreaResults})
    OutputDic.update({"OverallSummary": OverallSummary})
    OutputDic.update({"CashFlow": blobname_ts})
    OutputDic.update({"Trends": TStoDisp})
    OutputDic.update({"Energydisplay": energydisplay['Energydisplay']})
    OutputDic.update({"BoM": BoMblobname})
    OutputDic.update({"TotalLossChain": TotalLossChainDf.to_dict()})
    OutputDic.update({"warningmismatch": errorDic})
    entity.update({"FinalSummary": OutputDic})
    entity.update({"status": "Results Summary"})
    ds.put(entity)
    # TODO: FORMAT LOSS CHAIN PROPERLY
    # OutputDic.update({"LossChain":LossChain})

    return jsonify(OutputDic), 200
    # return jsonify({"success":"Energy and financial values recalculated"}),200       


@app.route('/battery-v2', methods=['POST'])
@jwt_authenticated
def batteryv2():
    ds = datastore.Client()
    gcs = storage.Client()
    bucket = gcs.bucket(BUCKET_NAME)

    req_json = request.get_json()['Battery']
    userinput = req_json
    userinput['email'] = request.email
    orgname = request.org
    OutputDic = {}
    message = {}
    parent_key = ds.key('users', userinput['email'])
    entity = ds.get(key=ds.key('Project', userinput['project_id'], parent=parent_key))

    if not entity:
        message.update({"error": "No project with username {} and project {} exists".format(userinput['email'],
                                                                                            userinput['project_id'])})
        return jsonify(message), 400

    uniqueid = 'Uid_' + userinput['email'] + '_Pid_' + userinput['project_id']
    orgentity = ds.get(key=ds.key('organisation', orgname))
    os.environ['DB_NAME'] = orgentity.get("compdb", "refdata")
    importlib.reload(CDB)

    if 'SelectedConfig_mu' in entity:
        TS = pickle.loads(bucket.get_blob(USER_UPLOADS + uniqueid + "_BattTS.pkl").download_as_string())
        SolarTS = TS['Solar']
        DemandTS = TS['Demand']
    else:
        entchklist = ['OptMatrix', 'SelectedConfig', 'Demand', 'ActiveQuote']
        for ent in entchklist:
            if ent not in entity:
                return jsonify({"error": "{} not set in datastore".format(ent)}), 400

                # Get Solar Production Time Series
        blobname = entity['OptMatrix']['TSConfigFile']
        TSDf = pickle.loads(bucket.get_blob(blobname).download_as_string())
        configsel = entity['SelectedConfig']['Config']
        pvname = configsel['PVName']
        invname = configsel['InvName']
        Ms = configsel['Ms']
        Mp = configsel['Mp']
        ConfigSummTuple = (pvname, invname, Ms, Mp, 'PAC_tot')
        location_data = Location(entity['Location']['latitude'], entity['Location']['longitude'],
                                 tz=entity['Weather']['TZ'])

        # SolarTS = TSDf[ConfigSummTuple]

        TSDfsel = TSDf[ConfigSummTuple]
        SolarTS = pd.Series(np.roll(TSDfsel.values, int(location_data.tz), axis=0), index=TSDfsel.index)
        SolarTS.name = 'Solar'
        #########################
        #### Get Demand TS ############
        DemandTS = pickle.loads(bucket.get_blob(entity['Demand']['DemandTS']).download_as_string())

        # YearlyDemand = DemandTS.sum()/1000
        SolarTS.index = DemandTS.index
    # 
    ##################COMMON###########
    # Get Battery model values from PerfModelKey of selected Battery 
    # BattDf
    # BATTDF = pickle.loads(bucket.get_blob('BatteryModel-09-2020.pkl').download_as_string())
    # Get Active Quote Values
    quoteparams = entity['ActiveQuote']
    QuoteID = quoteparams['QuoteID']
    RevID = quoteparams['RevID']
    ProductName = entity['ActiveQuote'].get('ProductName')

    ViewQuoteDf = CDB.GetBoMQuote(userinput['project_id'], QuoteID, RevID)
    BattBomId = ViewQuoteDf.loc[ViewQuoteDf['Group'] == 'Battery', 'Id'].astype(int).values
    ViewQuoteDf.fillna(np.nan, inplace=True)
    ViewQuoteDf.replace([np.nan], [None], inplace=True)

    # Make PriceAdjustment same as that for product name
    PriceAdjustment = 0.15
    if ProductName:
        ProdQuote = CDB.GetQuotefromtemplate(ProductName)
        PriceAdjustmentVal = ProdQuote.loc[ProdQuote['GroupName'] == 'Battery', 'PriceAdjustment'].astype(float).values
        if len(PriceAdjustmentVal) > 0:
            PriceAdjustment = PriceAdjustmentVal[0]

    if entity['status'] != 'Lock':
        entity.update({"status": "Battery"})
    BBomName = ""
    if 'BatterySpec' not in userinput:

        BNameVals = ViewQuoteDf.loc[ViewQuoteDf['Group'] == 'Battery', 'PerfModelKey']

        if (len(BNameVals) > 0):
            if ViewQuoteDf.loc[ViewQuoteDf['Group'] == 'Battery', 'Quantity'].values[0] == 0:
                BattName = BNameVals.values[0]
            else:
                BattName = BNameVals.values[0]
            BBomName = ViewQuoteDf.loc[ViewQuoteDf['Group'] == 'Battery', 'Name'].values[0]
            if ViewQuoteDf.loc[ViewQuoteDf['Group'] == 'Battery', 'Quantity'].values[0] == None:
                ViewQuoteDf.loc[ViewQuoteDf['Group'] == 'Battery', 'Quantity'] = 1
            if BattName in BATTDF.columns:
                BattVar = {}
                BattData = BATTDF[BattName]
                # for ind in BATTDF.index:
                #     BattVar[ind]= BATTDF[BattName].loc[ind]

                # BattDbDf = CDB.GetEntry('SYSTEM','PerfModelKey',BattName) # Get Details From Db
                BattDbDf = ViewQuoteDf.loc[ViewQuoteDf['Group'] == 'Battery']
                Quantity = BattDbDf['Quantity'].values[0]
                CostFunc = BattDbDf['Cost'].values[0]
                if Quantity:
                    NumCabinets = Quantity
                else:
                    NumCabinets = 1
                # BattVar['Enom'] = BattVar['Enom']*NumCabinets*BattVar['Estep']
                # BattVar['Emax']= BattVar['Enom']
            else:
                BattName = 'None'
                BBomName = 'None'
        else:
            BattName = 'None'
            BBomName = 'None'
            # if 'Battery' in entity:
            #     return jsonify(entity['Battery']),200
            # else:              
            #     BattName= 'None'

        if BattName == 'None':

            BattVar = {}
            for ind in BATTDF.index:
                BattVar[ind] = BATTDF[BATTDF.columns[0]].loc[ind]
            BattVar['Enom'] = 0
            BattVar['PmaxD'] = 0
            NumCabinets = 0

            BatteryTS, _, KPIs = ST.battProfileModel(DemandTS, SolarTS, BattVar)
            BatteryP = pd.Series(BatteryTS['Power'], name='Battery')
            BatterySize = {"Enom": 0, "Emax": 0, "PmaxC": 0, "PmaxD": 0, 'NumCabinets': 0, "CycleLife": 0}

            FinalTS = pd.concat([SolarTS, DemandTS, BatteryP], axis=1)
            blobname = USER_UPLOADS + uniqueid + '_BattTS.pkl'
            bucket.blob(blobname).upload_from_string(pickle.dumps(FinalTS))

            # Remove from BoM

            TotalCost = 0  # float((BattDbDf['Cost']*Quantity).values[0])
            KPIs.update({"TotalCost": TotalCost})
            # if battery in BoM then update entry
            if (len(BattBomId) != 0) and ('Edit' not in RevID):
                _Id = BattBomId[0].item()
                Bat2Update = [{'_Id': _Id}]
                CDB.DeleteFromBoM(Bat2Update)
                # typeid = BattTypeId[0].item() # id in masterlist - not needed---
                # Bat2Update = [{'_Id':_Id,"Type":"SYSTEM","TypeID":typeid,"CreatedBy":userinput['email'],
                #               "Quantity":float(Quantity),"TotalCost":float(TotalCost)}]
                # CDB.UpdateBoM(Bat2Update)

                # else add battery

                # Bat2Insert = [{"Type":"SYSTEM","TypeID":int(BattDbDf['Id'].values[0].item()),"CreatedBy":userinput['email'],
                #                 "ProjectID":userinput['project_id'],"QuoteID":QuoteID,"RevID":RevID,"Quantity":Quantity,"TotalCost":float(TotalCost),
                #                 "Group":"Battery"}]
                # CDB.InserttoBoM(Bat2Insert)

            OutputDic.update({"Battery": {"BatterySpec": "None", "TS": blobname, "KPI": KPIs}})
            entity.update(OutputDic)
            ds.put(entity)

            return jsonify(OutputDic['Battery']), 200

        # if BatterySpec is None, remove battery from quote and Make TS with E=P=0

    else:
        if userinput['BatterySpec'] == 'None':
            BattVar = {}
            for ind in BATTDF.index:
                BattVar[ind] = BATTDF[BATTDF.columns[0]].loc[ind]

            BattVar['Enom'] = 0
            BattVar['PmaxD'] = 0
            NumCabinets = 0

            BatteryTS, _, KPIs = ST.battProfileModel(DemandTS, SolarTS, BattVar)
            BatteryP = pd.Series(BatteryTS['Power'], name='Battery')
            BatterySize = {"Enom": 0, "Emax": 0, "PmaxC": 0, "PmaxD": 0, 'NumCabinets': 0, "CycleLife": 0}

            FinalTS = pd.concat([SolarTS, DemandTS, BatteryP], axis=1)
            blobname = USER_UPLOADS + uniqueid + '_BattTS.pkl'
            bucket.blob(blobname).upload_from_string(pickle.dumps(FinalTS))

            if (len(BattBomId) != 0) and ('Edit' not in RevID):
                _Id = BattBomId[0].item()
                Bat2Update = [{'_Id': _Id}]
                CDB.DeleteFromBoM(Bat2Update)
            KPIs.update({"TotalCost": 0})
            OutputDic.update({"Battery": {"BatterySpec": "None", "TS": blobname, "KPI": KPIs}})
            entity.update(OutputDic)
            ds.put(entity)

            return jsonify(OutputDic['Battery']), 200
        else:
            # if 'None' then E and P values are 0

            if 'Name' in userinput['BatterySpec']:
                BBomName = userinput['BatterySpec']['Name']
                BattDbDf = CDB.GetEntry('SYSTEM', 'Name', BBomName)
                BattDbDf.rename(columns={"Id": "ID"}, inplace=True)
                CostFunc = BattDbDf['Cost'].values[0]
                BattName = BattDbDf['PerfModelKey'].values[0]
                if BattName in BATTDF.columns:
                    BattData = BATTDF[BattName]
                else:
                    return jsonify({"error": "Battery Name invalid {}".format(BattName)}), 400
            else:
                BattName = userinput['BatterySpec']['Type']
                if BattName in BATTDF.columns:
                    BattData = BATTDF[BattName]
                    BattDbDf = CDB.GetEntry('SYSTEM', 'PerfModelKey', BattName)  # Get Details From Db
                    # BattDbDf = CDB.GetEntry('SYSTEM','Name',BBomName)
                    if len(BattDbDf) > 0:
                        BBomName = BattDbDf['Name'].values[0]
                        BattDbDf.rename(columns={"Id": "ID"}, inplace=True)
                        CostFunc = BattDbDf['Cost'].values[0]
                    else:
                        BattDbDf = CDB.GetEntry('SYSTEM', 'PerfModelKey', 'ads-tec SRS2028')
                        BattDbDf.rename(columns={"Id": "ID"}, inplace=True)
                        BBomName = BattDbDf['Name'].values[0]
                        CostFunc = BattDbDf['Cost'].values[0]
                elif BattName == 'Custom':
                    BattData = BATTDF[BATTDF.columns[0]]
                    # TODO - Delete THIS AND GENERATE CUSTOM BATTERY DATA
                    BattDbDf = CDB.GetEntry('SYSTEM', 'PerfModelKey', 'ads-tec SRS2028')
                    BattDbDf.rename(columns={"Id": "ID"}, inplace=True)
                    ListOfColumns = [D for D in userinput['BatterySpec'] if D != 'Type']
                    for LC in ListOfColumns:
                        BattData[LC] = userinput['BatterySpec'][LC]
                    if 'Price' in userinput['BatterySpec']:
                        CostFunc = userinput['BatterySpec']['Price']
                        BattDbDf['Cost'] = CostFunc
                    else:
                        CostFunc = None
                else:
                    ds.put(entity)
                    return jsonify({"error": "Battery Name invalid"}), 400

                    # Calculate Size and Cost of Battery through optimisation options

    BattVar = {}
    for ind in BattData.index:
        BattVar[ind] = BattData[ind]
    EnPerCabinet = BattVar['Enom']
    if "optimise" in userinput:
        if 'constraint' in userinput['optimise']:
            constraint = userinput['optimise']['constraint']
        else:
            constraint = {}
        if 'target' in userinput['optimise']:
            target = userinput['optimise']['target']
        else:
            target = {}

        BattSize, BatteryTS, KPIs = ST.bestBattSizeWithModel(DemandTS, SolarTS, BattData=BattVar, target=target,
                                                             constraint=constraint, CostFunc=CostFunc)
        Emax = BattSize[1]
        Pmax = BattSize[0]
        NumCabinets = (Emax / EnPerCabinet)
        # BattVar = BattData.copy()
        BattVar['Enom'] = BattSize[1]
        BattVar['PmaxD'] = BattSize[0]
    # BatteryTS,_, KPIs=ST.battProfileModel(DemandTS,SolarTS,BattVar)

    else:

        # BattData.Enom *=NumCabinets
        if 'BatterySpec' in userinput:
            if 'NumCabinets' in userinput['BatterySpec']:
                NumCabinets = userinput['BatterySpec']['NumCabinets']
                BattVar['Enom'] = BattData.Enom * userinput['BatterySpec']['NumCabinets']
                BattVar['NumCabinets'] = NumCabinets
            else:
                BattVar['NumCabinets'] = 1
                NumCabinets = 1
        else:
            BattVar['Enom'] = NumCabinets * BattVar['Enom']
            BattVar['Emax'] = BattVar['Enom']  # Fetched before

        BatteryTS, _, KPIs = ST.battProfileModel(DemandTS, SolarTS, BattVar)
        BattSize = (BattVar['PmaxD'], BattVar['Enom'])
        Emax = BattSize[1]
        Pmax = BattSize[0]

    BatteryP = pd.Series(BatteryTS['Power'], name='Battery')

    BattVar['Emax'] = float(BattVar['Enom'])
    BattVar['NumCabinets'] = float(NumCabinets)
    BattVar['PmaxC'] = BattVar['PmaxD']
    BatterySize = BattVar

    #    BatterySize = {"Enom":Emax,"Emax":Emax,"PmaxC":Pmax,"PmaxD":Pmax,'NumCabinets':NumCabinets,"CycleLife":BattData.CycleLife}        

    # Add Battery Details to final quote - Size and Cost
    Quantity = float(NumCabinets)
    TotalCost = float((BattDbDf['Cost'] * Quantity).values[0])
    KPIs.update({"TotalCost": TotalCost})
    # if battery in BoM then update entry
    # Do not update or remove an 'Edit' labeled RevID
    if 'Edit' not in RevID:
        if len(BattBomId) != 0:
            _Id = BattBomId[0].item()
            ViewQuoteDf
            typeid = BattDbDf['ID'].values[0].item()
            Bat2Update = [{'_Id': _Id, "Type": "SYSTEM", "TypeID": typeid, "CreatedBy": userinput['email'],
                           "Quantity": float(Quantity), "TotalCost": float(TotalCost),
                           "PriceAdjustment": float(PriceAdjustment)
                           }]
            CDB.UpdateBoM(Bat2Update)
        else:
            # else add battery"PriceAdjustment":float(PriceAdjustment)

            Bat2Insert = [
                {"Type": "SYSTEM", "TypeID": int(BattDbDf['ID'].values[0].item()), "CreatedBy": userinput['email'],
                 "ProjectID": userinput['project_id'], "QuoteID": QuoteID, "RevID": RevID, "Quantity": float(Quantity),
                 "TotalCost": float(TotalCost), "PriceAdjustment": float(PriceAdjustment),
                 "Group": "Battery"}]
            CDB.InserttoBoM(Bat2Insert)

    # Save Battery TS 
    FinalTS = pd.concat([SolarTS, DemandTS, BatteryP], axis=1)
    blobname = USER_UPLOADS + uniqueid + '_BattTS.pkl'
    bucket.blob(blobname).upload_from_string(pickle.dumps(FinalTS))

    BatterySpec = {"Type": BattName}
    BatterySpec.update(BatterySize)

    BatterySpec.update({"BoMName": BBomName})
    OutputDic.update({"Battery": {"BatterySpec": BatterySpec, "TS": blobname, "KPI": KPIs}})

    entity.update(OutputDic)
    ds.put(entity)

    return jsonify({"KPI": KPIs, "BatterySpec": BatterySpec}), 200
    # return jsonify({"Emax":Pmax,"KPI":KPIs})


# Change this battery version
@app.route('/battery', methods=['POST'])
@jwt_authenticated
def battery():
    ds = datastore.Client()
    gcs = storage.Client()
    bucket = gcs.bucket(BUCKET_NAME)

    req_json = request.get_json()['Battery']
    userinput = req_json
    outputdic = {}
    message = {}
    userinput['email'] = request.email
    orgname = request.org
    parent_key = ds.key('users', userinput['email'])
    entity = ds.get(key=ds.key('Project', userinput['project_id'], parent=parent_key))

    if not entity:
        message.update({"error": "No project with username {} and project {} exists".format(userinput['email'],
                                                                                            userinput['project_id'])})
        return jsonify(message), 400

    uniqueid = 'Uid_' + userinput['email'] + '_Pid_' + userinput['project_id']

    entchklist = ['OptMatrix', 'SelectedConfig']

    uschklist = ['BatterySpec']
    for us in uschklist:
        if us not in userinput:
            return jsonify({"error": "{} not set in input request".format(us)}), 400
    for ent in entchklist:
        if ent not in entity:
            return jsonify({"error": "{} not set in datastore".format(ent)}), 400

            # Get SolarTS ##########
    blobname = entity['OptMatrix']['TSConfigFile']
    TSDf = pickle.loads(bucket.get_blob(blobname).download_as_string())

    configsel = entity['SelectedConfig']['Config']

    pvname = configsel['PVName']
    invname = configsel['InvName']
    Ms = configsel['Ms']
    Mp = configsel['Mp']
    ConfigSummTuple = (pvname, invname, Ms, Mp, 'PAC_tot')
    location_data = Location(entity['Location']['latitude'], entity['Location']['longitude'],
                             tz=entity['Weather']['TZ'])
    # SolarTS = TSDf[ConfigSummTuple]
    TSDfsel = TSDf[ConfigSummTuple]
    SolarTS = pd.Series(np.roll(TSDfsel.values, int(location_data.tz), axis=0), index=TSDfsel.index)
    SolarTS.name = 'Solar'

    ###### Get Demand TS ################
    if 'Demand' in entity:
        if 'DemandTS' in entity['Demand']:
            DemandTS = pickle.loads(bucket.get_blob(entity['Demand']['DemandTS']).download_as_string())
            YearlyDemand = DemandTS.sum() / 1000
        else:
            return jsonify({"error": "Demand data not entered"}), 400
    else:
        return jsonify({"error": "Demand not set in datastore"}), 400

    """
    #####GET PROFILE DATA###############
    country = "CHE"
    blobname = AUXFILES+'PriceAPICountry.json'
    CI  = json.loads(bucket.blob(blobname).download_as_string() )        
    blobnamePic = AUXFILES+CI['Data'][country]['PriceDb']    
   # path = os.getcwd()
   # e = bucket.blob(blobnamePic).download_to_filename(os.path.join(path,'PriceDb.pkl'))
    Data = pickle.loads(bucket.get_blob(blobnamePic).download_as_string())
    # with open(os.path.join(path,'PriceDb.pkl'),'rb') as f:
    #     Data = pickle.load(f)
    #####################################
    UpDic = {}
    if 'YearlyDemand' in userinput:
        TotalElec = userinput['YearlyDemand']
        DemandTS,E,H = Sup.MakeGenericDemand(Data,TotalElec=TotalElec)
        # if 'Customer Inputs' in entity:
        #     CIn = entity['Customer Inputs']
        Demandblobname = USER_UPLOADS+uniqueid+'_demandTS.pkl'
        bucket.blob(Demandblobname).upload_from_string(pickle.dumps(DemandTS))
        entity['Demand']['DemandTS'] = Demandblobname

        entity.update({'Customer Inputs':{"YearlyElecRequired":{"EleckWh":E,"Elec4HeatkWh":H,"TotalElec":E+H}}})

    elif 'DemandTS' in entity['Demand']:
        DemandTS = pickle.loads(bucket.get_blob(entity['Demand']['DemandTS']).download_as_string())
        E = DemandTS.sum()/1000
        H = 0
    elif 'Customer Inputs' in entity:
        CIn = entity['Customer Inputs']

        if 'YearlyElecRequired' in entity['Customer Inputs']:
            if (('Elec4HeatkWh' in entity['Customer Inputs']['YearlyElecRequired'])and('EleckWh' in entity['Customer Inputs']['YearlyElecRequired'])):
                TotalHeat = entity['Customer Inputs']['YearlyElecRequired']['Elec4HeatkWh']
                TotalElec = entity['Customer Inputs']['YearlyElecRequired']['EleckWh']
                DemandTS,E,H = Sup.MakeGenericDemand(Data,TotalElec=TotalElec,TotalHeat=TotalHeat)
            elif 'TotalElec' in entity['Customer Inputs']['YearlyElecRequired']:
                TotalElec = entity['Customer Inputs']['YearlyElecRequired']['TotalElec']
                DemandTS,E,H =  Sup.MakeGenericDemand(Data,TotalElec=TotalElec)        
            else:
                DemandTS,E,H = Sup.MakeGenericDemand(Data)        
        else:
            DemandTS,E,H = Sup.MakeGenericDemand(Data)

        UpDic.update({"YearlyElecRequired":{"EleckWh":E,"Elec4HeatkWh":H,"TotalElec":E+H}})
        CIn.update(UpDic)
        entity.update(CIn)
    else:
        DemandTS,E,H = Sup.MakeGenericDemand(Data)
        entity.update({'Customer Inputs':{"YearlyElecRequired":{"EleckWh":E,"Elec4HeatkWh":H,"TotalElec":E+H}}})

    YearlyDemand = (E+H)      
    """
    ##########################################################
    # Get BatteryMod from Battery Type

    BatteryMod = {"E2P": 2.5,
                  "ChargeEff": 0.98, "DischargeEff": 0.96}
    #########################################################
    SolarTS.index = DemandTS.index
    BatterySpec = userinput['BatterySpec']

    if (userinput['BatterySpec'] == 'None'):
        Emax = 0
        Pmax = 0
        NumCabinets = 0
        BattBuck = ST.BucketModel(BatteryMod)
        BatteryTS, MaxBattReqSize, KPIs = BattBuck.getBattProfile(DemandTS, SolarTS, Pmax=Pmax,
                                                                  Emax=Emax)  # E and P in W
        BatteryP = pd.Series(BatteryTS['Power'], name='Battery')

    else:
        if "optimise" in userinput:

            BatteryMod['E2P'] = userinput['BatterySpec']['Emax'] / userinput['BatterySpec']['PmaxD']
            if 'constraint' in userinput['optimise']:
                constraint = userinput['optimise']['constraint']
            else:
                constraint = None
            if 'target' in userinput['optimise']:
                target = userinput['optimise']['target']
            else:
                target = 0.8
            try:
                if 'Price' in userinput['BatterySpec']:
                    price = userinput['BatterySpec']['Price']
                    CostFunc = price / userinput['BatterySpec']['Emax']  # $/kWh
                else:
                    CostFunc = None
            except:
                CostFunc = None

            BattSize, BatteryTS, KPIs = ST.bestBattSizeWithConstraint(DemandTS, SolarTS, BattData=BatteryMod,
                                                                      CostFunc=CostFunc,
                                                                      Target=target,
                                                                      constraint=constraint)  # Optimises to target SelfConsumption    
            BatteryP = pd.Series(BatteryTS['Power'], name='Battery')
            Emax = BattSize[1]
            Pmax = BattSize[0]

            if ('Emax' in userinput['BatterySpec']):
                if (userinput['BatterySpec']['Emax']) != 0:
                    NumCabinets = BattSize[1] / (userinput['BatterySpec']['Emax'])
                else:
                    NumCabinets = 1
            else:
                NumCabinets = 1
        else:
            try:
                EmaxW = userinput['BatterySpec']['Emax'] * userinput['BatterySpec']['NumCabinets'] * 1000
                PmaxW = userinput['BatterySpec']['PmaxD'] * userinput['BatterySpec']['NumCabinets'] * 1000
                NumCabinets = userinput['BatterySpec']['NumCabinets']
            except:
                PmaxW = SolarTS.max()
                EmaxW = PmaxW * BatteryMod['E2P']
                NumCabinets = 1
            target = 0.8
            constraint = None
            BattBuck = ST.BucketModel(BatteryMod)
            BatteryTS, MaxBattReqSize, KPIs = BattBuck.getBattProfile(DemandTS, SolarTS, Pmax=PmaxW,
                                                                      Emax=EmaxW)  # E and P in W
            BatteryP = pd.Series(BatteryTS['Power'], name='Battery')
            Emax = EmaxW / 1000
            Pmax = PmaxW / 1000
    # Return Optimised Size

    FinalTS = pd.concat([SolarTS, DemandTS, BatteryP], axis=1)

    blobname = USER_UPLOADS + uniqueid + '_BattTS.pkl'
    bucket.blob(blobname).upload_from_string(pickle.dumps(FinalTS))
    CostData = entity['Cost']
    BatterySize = {"Enom": Emax, "Emax": Emax, "PmaxC": Pmax, "PmaxD": Pmax, 'NumCabinets': NumCabinets}

    if type(BatterySpec) != str:
        try:
            PriceperkWh = userinput['BatterySpec']['Price'] / userinput['BatterySpec']['Emax']
        except:
            PriceperkWh = 0
        BatterySpec.update(BatterySize)

        BatterySpec.update({"Price": PriceperkWh * Emax, "CostFunc": PriceperkWh, "target": target})
        if constraint:
            BatterySpec.update({"constraint": constraint})
        CostData.update({"BatteryCost": {"Name": userinput['BatterySpec']['Type'],
                                         "Unit": "Unit",
                                         "data": BatterySpec['Price']}})

    outputdic.update(
        {"Battery": {"BatterySpec": BatterySpec, "TS": blobname, "KPI": KPIs, "YearlyDemand": YearlyDemand}})

    entity.update(outputdic)
    ds.put(entity)

    return jsonify({"success": "Battery parameters added for project {}".format(userinput['project_id']),
                    "KPI": KPIs, "BatterySpec": BatterySpec}), 200


@app.route('/make-report', methods=['POST'])
@jwt_authenticated
def makereport():
    ds = datastore.Client()
    gcs = storage.Client()
    bucket = gcs.get_bucket(BUCKET_NAME)
    req_json = request.get_json()['Report']
    userinput = req_json
    message = {}

    userinput['email'] = request.email
    orgname = request.org
    uniqueid = 'Pid_' + userinput['project_id']
    reportname = uniqueid + "_finalreport.pdf"
    file2store = RESULTS + reportname

    parent_key = ds.key('users', userinput['email'])
    entity = ds.get(key=ds.key('Project', userinput['project_id'], parent=parent_key))

    if not entity:
        message.update({"error": "No project with username {} and project {} exists".format(userinput['email'],
                                                                                            userinput['project_id'])})
        return jsonify(message), 400

    uschklist = ['contents']
    for us in uschklist:
        if us not in userinput:
            return jsonify({"error": "{} not set in input request".format(us)}), 400
    if 'freetext' in userinput:
        freetext = userinput['freetext']
    else:
        freetext = {}

    orgentity = ds.get(key=ds.key('organisation', orgname))
    os.environ['DB_NAME'] = orgentity.get("compdb", "refdata")
    importlib.reload(CDB)
    # Organisation entity to provide name   

    if 'contents' in userinput:
        pagetype = userinput['contents']  # short, detailed
    else:
        pagetype = 'short'

    uniqueid = 'Uid_' + userinput['email']

    blobname_pvmod = USER_UPLOADS + uniqueid + '_moduledata.pkl'
    blobpv = bucket.get_blob(blobname_pvmod)
    if blobpv:
        s = blobpv.download_as_string()
        USERPVDF = pickle.loads(s)

    if 'SelectedConfig_mu' in entity:
        pvname = entity['SelectedConfig_mu']['Config']['PVName']
        invname = [n for n in entity['SelectedConfig_mu']['Config']['InvCombo'].keys()]
        InvData = {name: INVDF.loc[:, name].to_dict() for name in invname}

    else:
        entchecklist = ['SelectedConfig', 'Location', 'OptMatrix', 'Weather', 'Cost']
        for ec in entchecklist:
            if ec not in entity:
                return jsonify({"error": "{} not set in datastore".format(ec)}), 400

        pvname = entity['SelectedConfig']['Config']['PVName']
        invname = entity['SelectedConfig']['Config']['InvName']
        InvData = INVDF.loc[:, invname]

    # InvData = INVDF.loc[:,invname]
    if pvname in PVDF:
        PVModuleData = PVDF.loc[:, pvname]
    elif pvname in PVSYSDF:
        PVModuleData = PVSYSDF.loc[:, pvname]
    elif blobpv:
        if pvname in USERPVDF:
            PVModuleData = USERPVDF.loc[:, pvname]
    else:
        return jsonify({"Error": "PV Module {} not found in any database".format(pvname)}), 400

        # PVModuleData = PVDF.loc[:,entity['SelectedConfig']['Config']['PVName']]

    # contentlist =[cont for cont in userinput['contents'] if cont in templatepages.keys()]                                                                                   

    entity.update({"status": "Final Proposal"})
    # ds.put(entity)

    # Read settings from 
    # templatesettings=json.loads(bucket.get_blob(AUXFILES+"templatesettings.json").download_as_string())
    # Read templatesettings from orgentity
    templatesettings = orgentity['templatesettings']

    if "template" in userinput:
        templatekey = str(userinput['template'])
        if templatekey not in templatesettings:
            templatekey = [d for d in templatesettings.keys()][0]
    else:
        templatekey = [d for d in templatesettings.keys()][0]

    # lang = templatesettings[templatekey].get('lang','DE')
    if 'lang' in userinput:
        lang = userinput['lang']
    else:
        lang = templatesettings[templatekey].get('lang', 'DE')
    # if pagetype=='detailed': 
    #     page = 'pages-det'
    # else:
    #     page='pages'
    if 'pages' not in userinput:
        pages = templatesettings[templatekey]['pages']
    else:
        pages = []
        pageinput = userinput['pages']
        # Validate if input
        for page in pageinput:
            if page in orgentity.get('templatepages', {}):
                pages.append(page)

    if "addon2download" in templatesettings[templatekey]:
        addon2download = templatesettings[templatekey]['addon2download']
    else:
        addon2download = []

    # TODO: Read Below from orginfo or central settings
    tempfuncfolder = {"template-ag-agsolar-fix-kurz": {"func": "template_ag_agsolar_fix_kurz",
                                                       "folder": "template-ag-agsolar-fix-kurz"},
                      "template-ag-efh-fix-kurz": {"func": "template_ag_efh_fix_kurz",
                                                   "folder": "template-ag-efh-fix-kurz"},
                      "template-ag-efh-richt-kurz": {"func": "template_ag_efh_richt_kurz",
                                                     "folder": "template-ag-efh-richt-kurz"},
                      "template-ag-mfh-fix-kurz": {"func": "template_ag_mfh_fix_kurz",
                                                   "folder": "template-ag-mfh-fix-kurz"},
                      "template-ag-mfh-richt-kurz": {"func": "template_ag_mfh_richt_kurz",
                                                     "folder": "template-ag-mfh-richt-kurz"},
                      "fr-template-ag-mfh-fix-kurz": {"func": "template_ag_mfh_fix_kurz",
                                                      "folder": "fr-template-ag-mfh-fix-kurz"},
                      "fr-template-ag-efh-fix-kurz": {"func": "template_ag_efh_fix_kurz",
                                                      "folder": "fr-template-ag-efh-fix-kurz"},
                      "fr-template-ag-agsolar-fix-kurz": {"func": "template_ag_agsolar_fix_kurz",
                                                          "folder": "fr-template-ag-agsolar-fix-kurz"},
                      "template-ag-mfh-fix-lease": {"func": "template_ag_mfh_fix_kurz",
                                                    "folder": "template-ag-mfh-fix-kurz"},

                      "template-agnew-en": {"func": "template_agnew_en", "folder": "template-agnew-en"},
                      "template-agnew-blue-pt": {"func": "template_agnew_blue_pt", "folder": "template-agnew-blue-pt"},
                      "template-pt-short": {"func": "template_agnew_blue_pt", "folder": "template-pt-short"},
                      "template-tech-report": {"func": "template_tech_report", "folder": "template-tech-report"},
                      "template-br-homolog": {"func": "template_br_homolog", "folder": "template-br-homolog"},

                      "template-gen-v1": {"func": "template_gen_v1", "folder": "template-gen-v1-<lang>"},
                      "template-gen-v2": {"func": "template_gen_v2", "folder": "template-gen-v2-<lang>"},

                      }
    templatefolder = tempfuncfolder.get(templatekey, 'template-gen-v1').get('folder').replace('<lang>', lang)
    templatefunc = tempfuncfolder.get(templatekey, 'template-gen-v1').get('func')

    path2template = AUXFILES + templatefolder

    # if lang=='FR':
    #     templatename = templatename.split('fr-')[-1] #'fr-'+templatename
    R = RepM.Report(bucket, templatename=templatefunc, lang=lang, path2template=path2template)

    err = R.gettemplatefrombucket(bucket, path2template)
    if err == "ERROR":
        return jsonify({"error": R.errmsg})
    R.GenReportVars(entity, PVModuleData, InvData, orgentity)

    # Add user content to ReportContents
    # firstpage_paragraphs = freetext.get("firstpage", "").split("\n")
    lastpage_paragraphs = freetext.get("lastpage", "").split("\n")
    payment_mode = freetext.get("paymentmode", "emi")

    payments = []
    paymentschedule = []
    if payment_mode == "emi":
        payment_data = freetext.get("payment", {})
        if payment_data:
            payments.append([" ", "Upfront Amount = {}".format(payment_data.get("upfrontamount"))])
            payments.append(
                [" ", "EMI Amount = {} X {}".format(payment_data.get("totalemi"), payment_data.get("emiamount"))])
        else:
            payments.append(["", ""])
    else:
        payment_data = freetext.get("payment", {})
        if payment_data:
            for temp in payment_data:
                payments.append(
                    ["{} % {} - {}".format(temp.get("precentage"), temp.get("description"), temp.get("value")), ""])
                paymentschedule.append({"percentage": temp.get("precentage"), "description": temp.get("description")})
        else:
            payments.append(["", ""])
    if 'paymentschedule' in userinput:
        paymentschedule = userinput['paymentschedule']
    if 'ReportContentsStored' in entity:
        paymentschedule = entity['ReportContentsStored'].get('paymentschedule', [])
    R.ReportContents.update({
        "payments": payments,
        "freetext": freetext,
        # "firstpage_paragraphs": firstpage_paragraphs,
        "lastpage_paragraphs": lastpage_paragraphs,
        "payment_mode": payment_mode,
        "paymentschedule": paymentschedule
    })
    ################# STORE EDITABLES ##################
    ToSaveForEdits = R.ReportContents.get('ToSaveForEdits', [])
    if 'manualedit' in userinput:  # Use ReportContentsStored
        RC2Store = entity.get('ReportContentsStored', {})

        for keyvalues in userinput['manualedit']:
            if keyvalues in RC2Store:
                if keyvalues == 'BoMDisplay':
                    SavedBoMDisplay = pd.DataFrame(RC2Store['BoMDisplay'])
                    CurrentBoMDisplay = R.ReportContents['BoMDisplay']
                    for i in CurrentBoMDisplay.index:
                        Description = \
                        SavedBoMDisplay.loc[SavedBoMDisplay['DisplayName'] == CurrentBoMDisplay.loc[i]['DisplayName']][
                            'Description']
                        if len(Description) > 0:
                            CurrentBoMDisplay.loc[i, 'Description'] = Description.values[0]

                    R.ReportContents.update({'BoMDisplay': CurrentBoMDisplay})
                    # Only Update Descriptions   
                    # R.ReportContents.update({'BoMDisplay':pd.DataFrame(RC2Store[keyvalues])})
                else:
                    R.ReportContents.update({keyvalues: RC2Store[keyvalues]})

    elif 'overwrite_auto' in userinput:  # Use Automatic Values  for overwrite
        ToSaveForEdits = R.ReportContents.get('ToSaveForEdits', [])
        # ToSaveForEdits = ["BoMDisplay", "firstpage_paragraphs",
        #             "payment_mode","lastpage_paragraphs","tandc1","paymentschedule"]

        # Save Following Items of ReportContents for View and Edits
        RC2Store = {}
        for keyvalues in ToSaveForEdits:
            if keyvalues == 'BoMDisplay':
                RC2Store.update({keyvalues: R.ReportContents.get('BoMDisplay').to_dict('list')})
            else:
                RC2Store.update({keyvalues: R.ReportContents.get(keyvalues)})
            if 'ReportContentsStored' in entity:
                entity['ReportContentsStored'].update(RC2Store)
            else:
                entity.update({'ReportContentsStored': RC2Store})
        if userinput['overwrite_auto'] == []:  # first time
            ds.put(entity)
            return jsonify({"ReportContentsStored": RC2Store}), 200
    else:
        ToSaveForEdits = R.ReportContents.get('ToSaveForEdits', [])
        # Just Save the ToSaveForEdits keys in entity

        RCStored = dict(entity.get('ReportContentsStored', {})).copy()
        for keyvalues in ToSaveForEdits:
            if keyvalues not in RCStored:
                RCStored.update({keyvalues: R.ReportContents.get(keyvalues, {})})
        RCStored.update({'ToSaveForEdits': ToSaveForEdits})
        if 'ReportContentsStored' in entity:
            entity['ReportContentsStored'].update(RCStored)
        else:
            entity.update({'ReportContentsStored': RCStored})

        ds.put(entity)
        return jsonify({"ReportContentsStored": RCStored}), 200
    ds.put(entity)
    ################### STORE EDITABLES ##################    

    Path = R.rendertemplate(reportname, pages)

    R.uploadtobucket(Path, file2store)

    urltodownload = "https://www.googleapis.com/storage/v1/b/" + BUCKET_NAME + "/o/" + \
                    file2store.replace('/', '%2F') + "?alt=media"

    _ = R.localstoredfiles
    R.removetempfiles()
    # "debug":P
    arraydownload = [urltodownload]

    if addon2download:
        for filepack in addon2download:
            filepackname = "https://www.googleapis.com/storage/v1/b/" + BUCKET_NAME + "/o/" + \
                           filepack.replace('/', '%2F') + "?alt=media"
            arraydownload.append(filepackname)

    OutputDic = {"url2download": arraydownload}

    return jsonify(OutputDic), 200


@app.route('/leadtoproject', methods=['POST'])
@jwt_authenticated
def lead2project():
    ds = datastore.Client()
    gcs = storage.Client()
    bucket = gcs.get_bucket(BUCKET_NAME)

    # message = {}
    record = {}

    req_json = request.get_json()['LeadDetails']
    userinput = req_json
    userinput['email'] = request.email
    orgname = request.org
    # TODO: Authentication for certain proj ids to be able to access Lead Ids
    # and allocated to other proj_emails

    chklist = ['lead_email', 'lead_id', 'proj_email', 'projectname']  # Key Values that must be there
    # Still missing, "map address"
    for us in chklist:
        if us not in userinput:
            return jsonify({"error": "{} not set in input request".format(us)}), 400

    L_uniqueid = 'UID_' + userinput['lead_email'] + '_PID_' + userinput['lead_id']

    # Parent key for lead
    l_parent_key = ds.key('users', userinput['lead_email'])
    lead_id = userinput['lead_id']
    lead_email = userinput['lead_email']
    proj_email = userinput['proj_email']
    # entity for lead
    l_entity = ds.get(key=ds.key('Leads', lead_id, parent=l_parent_key))
    # Parent key for project
    p_parent_key = ds.key('users', proj_email)
    # entity for project
    p_entity = ds.get(key=p_parent_key)

    if not l_entity:
        return jsonify({"error": "No lead with username {} and lead id {} exists".format(lead_email, lead_id)})
    if not p_entity:
        return jsonify({"error": "No project with username {}  exists".format(proj_email)})

    ProjId = str(uuid.uuid1())
    entity = datastore.Entity(key=ds.key('users', userinput['proj_email'], 'Project', ProjId))
    p_uniqueid = 'Uid_' + userinput['proj_email'] + '_Pid_' + ProjId

    ListOfItems = ['Demand', 'Location', 'Weather', 'EpriceTariffFile', 'Orientation', 'Priority', 'Customer Inputs']
    for pitems in ListOfItems:
        if pitems not in l_entity:
            pass
        else:
            entity[pitems] = l_entity[pitems]
    customerinfo = {}
    entity['projectdetails'] = {}
    entity['projectdetails']['projectname'] = userinput['projectname']
    ### TRANSFER OF FILES IN BUCKETS #####
    if 'EpriceTariffFile' in l_entity:
        blobname = l_entity['EpriceTariffFile']
        p_blobname = USER_UPLOADS + p_uniqueid + '_epriceTariff.pkl'
        try:
            bucket.rename_blob(bucket.get_blob(blobname), p_blobname)
        except:
            pass
        entity['EpriceTariffFile'] = p_blobname

    ### TRANSFER OF MAP LAYOUT ###
    p_uniqueid = 'UID_' + userinput['proj_email'] + '_PID_' + ProjId
    d2layoutFname = USER_UPLOADS + L_uniqueid + '_2D_layout.json'
    d3layoutFname = USER_UPLOADS + p_uniqueid + '_3D_scene_layout.json'
    # try:
    #     bucket.rename_blob(bucket.get_blob(d2layoutFname),d3layoutFname)
    # except:
    #     pass
    #### RENAME 2d to 3d MAPS #####

    if 'customerinfo' in l_entity:
        if 'projectaddress' in l_entity['customerinfo']:
            entity['projectdetails']['projectaddress'] = "{}, {}, {}".format(l_entity['customerinfo']['projectaddress']
                                                                             , l_entity['customerinfo']['zip']
                                                                             , l_entity['customerinfo']['city'])
            entity['projectdetails']['customerinfo'] = l_entity['customerinfo']
            for keys in l_entity['customerinfo']:
                customerinfo[keys] = l_entity['customerinfo'][keys]

        else:
            return jsonify({"error": "projectaddress not set in customerinfo field"}), 400
    else:
        return jsonify({"error": "customerinfo not collected"}), 400

    entity['created'] = dt.datetime.utcnow()
    entity['status'] = 'From Lead'

    l_entity['status'] = 'Moved to Project'

    """
    Mapping of entities from Lead to Project

    Project:    Lead

    Demand : Demand

    [customerinfo] : [customerinfo]
    [projectdetails][projectaddress] = [customerinfo][projectaddress]
    Location: Location
    Customer Inputs: Customer Inputs
    Weather : Weather
    Orientation: Orientation

    UID_<email>_PID_<leadid>_3D_scene_layout.json <-: UID_<email>_PID_<projectid>_2D_scene_layout.json

    add attributes: ,“height”:6,“graphicID”:5,“roofAngle”:30,“turned”:1,“roof”:“gable”,
    Priority : Priority

    EpriceTariffFile : EpriceTariffFile

    """
    ds.put(entity)
    ds.put(l_entity)
    record.update({"Success": "Lead {lead_id} converted to project id {project_id} for user {proj_email}".format(
        lead_id=lead_id, project_id=ProjId, proj_email=proj_email)})
    record.update({"project_id": ProjId})
    record.update({"customerinfo": customerinfo})

    return jsonify(record), 200


@app.route('/cloneproject', methods=['POST'])
@jwt_authenticated
def cloneproject():
    ds = datastore.Client()
    gcs = storage.Client()
    bucket = gcs.get_bucket(BUCKET_NAME)

    req_json = request.get_json()
    userinput = req_json
    userinput['email'] = request.email
    orgname = request.org
    parent_key = ds.key('users', userinput['email'])
    entity = ds.get(key=ds.key('Project', userinput['project_id'], parent=parent_key))
    pid = userinput['project_id']
    message = {}
    if not entity:
        message.update({"error": "No project with username {} and project {} exists".format(userinput['email'],
                                                                                            userinput['project_id'])})
        return jsonify(message), 400

    # Allow transfer only within the organisation
    # TODO: Name Suggestion
    findusertotransfer = userinput.get('finduser')
    orgentity = ds.get(key=ds.key('organisation', orgname))
    if findusertotransfer:
        listofusers = orgentity.get('users', [])

        return jsonify({"userlist": listofusers}), 200
    thisprojectname = entity['projectdetails']['projectname']
    basenamefilter = '^(.*)_\D\d*$'
    try:
        baseprojectname = re.findall(basenamefilter, thisprojectname)[0]
    except IndexError:
        baseprojectname = thisprojectname

    if userinput.get('version'):

        query = ds.query(kind='Project', ancestor=parent_key)
        query.order = ['-created']
        # query.add_filter('status','=','Final Proposal')

        Q = query.fetch(limit=int(100))

        projList = []
        attribs = [{'ActiveQuote': ['QuoteID']}, {'projectdetails': ['projectname']}, 'status', 'created']

        projectnameCheck = baseprojectname
        for s in Q:
            if s.get('projectdetails'):
                if projectnameCheck in s['projectdetails']['projectname']:
                    entry = {"id": str(s.key.name)}
                    for att in attribs:
                        if type(att) != dict:

                            if att in s:
                                entry.update({att: s[att]})
                        elif type(att) == dict:
                            if list(att.keys())[0] in s:
                                mainatt = str(list(att.keys())[0])  # Main attribute and subattributes 
                                _ = [entry.update({subatt: s[mainatt][subatt]}) for subatt in list(att.values())[0] if
                                     subatt in list(s[mainatt].keys())]
                    projList.append(entry)

        vnumfilter = re.compile('^.*_\D(\d*)$')
        LatestVersion = max(
            [int(vnumfilter.findall(p['projectname'])[0]) for p in projList if vnumfilter.findall(p['projectname'])],
            default=0)

        newprojectname = baseprojectname + '_R' + str(LatestVersion + 1)
        try:
            if LatestVersion == 0:
                BaseQuoteID = [p['QuoteID'].split('_R')[0] for p in projList if
                               ((p['projectname'] == str(baseprojectname)) and ('QuoteID' in p))][0]
            else:
                BaseQuoteID = [p['QuoteID'].split('_R')[0] for p in projList if (
                            p['projectname'] == str(baseprojectname + '_R' + str(LatestVersion)) and ('QuoteID' in p))][
                    0]
        except:
            BaseQuoteID = entity.get('ActiveQuote', {}).get('QuoteID', '1')
        NewQuoteID = BaseQuoteID + '_R' + str(LatestVersion + 1)
        newuseremail = userinput.get('newusername', userinput['email'])
        entity.update({'status': 'Lock'})
    else:
        newprojectname = userinput.get('newprojectname', str(entity['projectdetails']['projectname']))
        newuseremail = userinput.get('newusername', userinput['email'])
        if 'ActiveQuote' in entity:
            # NewQuoteID = entity['ActiveQuote']['QuoteID']
            NewQuoteID = int(orgentity['bominput'].get('QuoteAutoIncUID', '100000')) + 1
            orgentity['bominput'].update({'QuoteAutoIncUID': NewQuoteID})

    os.environ['DB_NAME'] = orgentity.get("compdb", "refdata")
    importlib.reload(CDB)

    # newprojectname = userinput.get('newprojectname','new_'+str(entity['projectdetails']['projectname']))
    # newuseremail = userinput.get('newusername',userinput['email']) 

    # Check if user exeists
    newuserkey = ds.key('users', newuseremail)
    newentity = ds.get(key=newuserkey)
    if not newentity:
        return jsonify({"error": "user {} does not exist ".format(newuseremail)})

    ProjId = str(uuid.uuid1())
    NEWentity = datastore.Entity(key=ds.key('Project', ProjId, parent=newuserkey))

    outp = copy.deepcopy(entity)
    NEWentity.update(outp)

    projdetails = entity['projectdetails']
    projupdate = {k: v if k != 'projectname' else newprojectname for k, v in projdetails.items()}
    NEWentity.update({"projectdetails": projupdate})
    if 'ActiveQuote' in entity:
        NEWentity.update({'ActiveQuote': {"QuoteID": NewQuoteID, "RevID": entity['ActiveQuote']['RevID'],
                                          "ProductName": entity['ActiveQuote'].get('ProductName', '')}})

    NEWentity.update({'created': dt.datetime.now()})
    if entity['status'] == 'Lock':
        NEWentity.update({'status': 'Final Proposal'})

    ## Datastore replicated
    uniqueid = "Uid_" + userinput['email'] + "_Pid_" + userinput['project_id']
    sourcebloblists = ["UserUploads/" + uniqueid + "_physicallayout.json",
                       "UserUploads/" + uniqueid + "_layoutarea.json",
                       "Results/" + uniqueid + "_Configs.pkl", "Results/" + uniqueid + "_TSConfigs.pkl",
                       "UserUploads/" + uniqueid + "_epriceTariff.pkl", "UserUploads/" + uniqueid + "_demandTS.pkl",
                       "UserUploads/" + uniqueid + "_BattTS.pkl",
                       "FetchedWeather/" + uniqueid + "_weather.pkl",
                       "Results/" + uniqueid + "_YearlyBenefits.pkl",
                       "Results/" + uniqueid + "_CashFlow.pkl"]  # "UserUploads/UID_"+userinput['email']+"_PID_"+userinput['project_id']+"_3D_scene_layout.json",
    # Replicate bucket contents
    # find all 3d_scene_layouts
    for blob in gcs.list_blobs(BUCKET_NAME, prefix="UserUploads/UID_" + userinput['email'] + "_PID_" + userinput[
        'project_id'] + "_3D_scene_layout"):
        sourcebloblists.append(str(blob.name))

    # OptDf, TSDf, layoutarea.json, 3dscene_layout.json, EpriceTariffFile, physicallayout, BatteryTS
    # Change all the file names
    def getpath(nesteddict, value, prepath=()):
        for k, v in nesteddict.items():
            path = prepath + (k,)
            if value == str(v):
                return path
            elif hasattr(v, 'items'):
                p = getpath(v, value, path)  # recursive call
                if p is not None:
                    return p

    def getval(dic, paths, val=None):

        if val:
            Dic = dic
            for pp in paths[:-1]:
                Dic = Dic[pp]
            Dic.update({paths[-1]: val})
        else:
            for pp in paths:
                dic = dic[pp]
        return dic

        # Replicate blob names in entities    

    # for blobnames in sourcebloblists:
    #     path2val = getpath(entity,blobnames)
    #     if path2val:
    #         Oldvalue = getval(entity,path2val)
    #         Newvalue = Oldvalue.replace(pid, ProjId).replace(userinput['email'],newuseremail)
    #         #print("OldV  {} --  Newvalue  {}".format(Oldvalue,Newvalue))

    #         getval(NEWentity,path2val,Newvalue)

    # Replicate blobs in cloud storage        
    newbloblist = []

    for oldblob_name in sourcebloblists:
        newblob_name = oldblob_name.replace(pid, ProjId).replace(userinput['email'], newuseremail)
        if bucket.get_blob(oldblob_name):
            newblob = bucket.copy_blob(bucket.get_blob(oldblob_name), bucket, newblob_name)
            newbloblist.append(newblob.name)

    # COPY THEM ALL

    # Datastore and Buckets copied
    # Validate Entities are pointing to the same place
    Newuniqueid = uniqueid.replace(pid, ProjId).replace(userinput['email'], newuseremail)
    if 'GeoCoords' in NEWentity.get('Location', {}):
        NEWentity['Location']['GeoCoords'] = "UserUploads/" + Newuniqueid + "_layoutarea.json"
    if 'PanelCoords' in NEWentity.get('Location', {}):
        NEWentity['Location']['PanelCoords'] = "UserUploads/" + Newuniqueid + "_physicallayout.json"
    if 'DemandTS' in NEWentity.get('Demand', {}):
        NEWentity['Demand']['DemandTS'] = "UserUploads/" + Newuniqueid + "_demandTS.pkl"
    if NEWentity.get('EpriceTariffFile'):
        NEWentity['EpriceTariffFile'] = "UserUploads/" + Newuniqueid + "_epriceTariff.pkl"
    if 'finalweatherfile' in NEWentity.get('Weather', {}):
        NEWentity['Weather']['finalweatherfile'] = "FetchedWeather/" + Newuniqueid + "_weather.pkl"
    if 'TS' in NEWentity.get('Battery', {}):
        NEWentity['Battery']['TS'] = "UserUploads/" + Newuniqueid + "_BattTS.pkl"
    if 'OptConfigFile' in NEWentity.get('OptMatrix', {}):
        NEWentity['OptMatrix']['OptConfigFile'] = "Results/" + Newuniqueid + "_Configs.pkl"
    if 'TSConfigFile' in NEWentity.get('OptMatrix', {}):
        NEWentity['OptMatrix']['TSConfigFile'] = "Results/" + Newuniqueid + "_TSConfigs.pkl"
    if 'YearlyBenefit' in NEWentity.get('Financial', {}):
        if type(entity['Financial']['YearlyBenefit']) == str:
            NEWentity['Financial']['YearlyBenefit'] = "Results/" + Newuniqueid + "_YearlyBenefits.pkl"
    if 'CashFlow' in NEWentity.get('Financial', {}):
        if type(entity['Financial']['CashFlow']) == str:
            NEWentity['Financial']['CashFlow'] = "Results/" + Newuniqueid + "_CashFlow.pkl"

    # Cloud Sql ActiveQuote  - Only Last Revision of ComponentDb replicated
    # Replicate- last t
    if 'ActiveQuote' in entity:

        QuoteID = entity['ActiveQuote']['QuoteID']
        RevID = entity['ActiveQuote']['RevID']
        # Get all RevIds 
        querytext = "SELECT DISTINCT RevID FROM BOM_QUOTE WHERE `ProjectID`='{}' and `QuoteID`='{}' \
                     ;".format(userinput['project_id'], QuoteID)

        res = CDB.RunRawSQLQuery(querytext)
        if len(res) > 0:
            RevIDlist = [l[0] for l in res]
        else:
            RevIDlist = [RevID]

            # RevFormat=re.compile('^(R|V)\d*$') # Check for format 'R#' only, exclude R5-Edit, R2-Edit etc
        # if not re.match(RevFormat,RevID):
        #     RevIDlist.append(re.search('^(R|V)\d*',RevID).group())
        for RID in RevIDlist:
            ViewQuoteDf = CDB.GetBoMQuote(pid, QuoteID, RID)
            ViewQuoteDf['ProjectID'] = ProjId
            ViewQuoteDf['QuoteID'] = NewQuoteID
            ViewQuoteDf['TypeID'] = ViewQuoteDf['ID']
            ViewQuoteDf['Type'] = 'SYSTEM'
            ViewQuoteDf.fillna(np.nan, inplace=True)
            ViewQuoteDf.replace([np.nan], [None], inplace=True)
            # Make BOM
            ViewQuoteDf.drop_duplicates(subset=['ID'], inplace=True)
            Proxy = CDB.RunRawSQLQuery("SHOW COLUMNS FROM BOM_QUOTE")
            # Columns = [l._row[0] for l in Proxy if l._row[0] in ViewQuoteDf.columns and l._row[0]!='Id' ] # Get all Columns in BoMQuote
            Columns = [l[0] for l in Proxy if l[0] in ViewQuoteDf.columns and l[0] != 'Id']
            VQDict = ViewQuoteDf[Columns].to_dict(orient='records')
            try:
                CDB.InserttoBoM(VQDict, tablename='BOM_QUOTE')
            except:
                pass

    ds.put(NEWentity)
    ds.put(entity)
    ds.put(orgentity)
    return jsonify({"project_id": ProjId, "projectname": newprojectname, "user": newuseremail, "bloblist": newbloblist,
                    "quote": NewQuoteID}), 200


@app.route('/storedblobs', methods=['POST', 'GET'])
@jwt_authenticated
def getblobs():
    ds = datastore.Client()
    gcs = storage.Client()
    bucket = gcs.get_bucket(BUCKET_NAME)

    # req_json=request.get_json()
    pid = request.args.get('project_id', '')
    userinput = {}
    userinput['project_id'] = pid  # req_json['project_id']
    userinput['email'] = request.email
    orgname = request.org
    parent_key = ds.key('users', userinput['email'])
    entity = ds.get(key=ds.key('Project', userinput['project_id'], parent=parent_key))
    pid = userinput['project_id']
    itemstofetch = request.args.get('items', {})
    message = {}
    if not entity:
        message.update({"error": "No project with username {} and project {} exists".format(userinput['email'],
                                                                                            userinput['project_id'])})
        return jsonify(message), 400

    itemslist = [r.strip() for r in re.split(',', itemstofetch)]
    key2display = {"3dscene": "Layout", "pdfreport": "Offer", "sld": "SLD"}

    # ['3dscene', 'pdfreport', 'sld']
    # args : {blobs = }
    blobinfo = {}
    uniqueid = "Uid_" + userinput['email'] + "_Pid_" + userinput['project_id']
    uniqueprefix = RESULTS + uniqueid

    for key in itemslist:
        archiveblob = []
        displayname = key2display.get(key, key)
        if key == '3dscene':
            blobprefix = USER_UPLOADS + "UID_" + userinput['email'] + "_PID_" + userinput[
                'project_id'] + "_3D_scene_layout"
            baseurl = "https://www.googleapis.com/storage/v1/b/" + BUCKET_NAME + "/o/" + \
                      (blobprefix + '.json').replace('/', '%2F') + "?alt=media"
            baseblob = bucket.get_blob(blobprefix + '.json')

            r = re.compile('.*_[v]{1}\d.json$')
            vspattern = '.*_([v]{1}\d).json$'
        elif key == 'pdfreport':
            blobprefix = RESULTS + "Pid_" + userinput['project_id'] + "_finalreport"
            baseurl = "https://www.googleapis.com/storage/v1/b/" + BUCKET_NAME + "/o/" + \
                      (blobprefix + '.pdf').replace('/', '%2F') + "?alt=media"
            baseblob = bucket.get_blob(blobprefix + '.pdf')
            r = re.compile('.*_[v]{1}\d.pdf$')
            vspattern = '.*_([v]{1}\d).pdf$'
        else:  # key should be the entire suffix
            blobprefix = RESULTS + "Pid_" + userinput['project_id'] + key
            baseurl = "https://www.googleapis.com/storage/v1/b/" + BUCKET_NAME + "/o/" + \
                      (blobprefix).replace('/', '%2F') + "?alt=media"
            baseblob = bucket.get_blob(blobprefix)
            r = re.compile('.*_[v]{1}\d\..*$')
            vspattern = '.*_([v]{1}\d)\..*$'

        if baseblob:
            baseblobitem = {"url2download": baseurl, "Updated": dt.datetime.strftime(baseblob.updated, "%d/%m/%y %T"),
                            "displayname": f"{displayname} current", "version": "0"}
            archiveblob.append(baseblobitem)
            ####### SAVE A VERSION###########
        if request.method == 'POST':
            params = request.get_json()
            action = params.get('action')
            version = params.get('version', '1')
            try:

                if 9 < int(version) < 1:
                    version = '1'
            except ValueError:
                version = '1'

            if action == 'save':
                oldblob_name = baseblob.name
                suffix = re.findall('.*\.(.{3,4})$', oldblob_name)[0]

                newblob_name = oldblob_name.replace(f'.{suffix}', f'_v{version}.{suffix}')

                bucket.copy_blob(bucket.get_blob(oldblob_name), bucket, newblob_name)

            elif action == 'current':
                suffix = re.findall('.*\.(.{3,4})$', baseblob.name)[0]
                oldblob_name = baseblob.name.replace(f'.{suffix}', f'_v{version}.{suffix}')
                newblob_name = baseblob.name
                try:
                    bucket.copy_blob(bucket.get_blob(oldblob_name), bucket, newblob_name)
                except AttributeError:
                    pass
            elif action == 'delete':
                suffix = re.findall('.*\.(.{3,4})$', baseblob.name)[0]
                todeleteblobname = baseblob.name.replace(f'.{suffix}', f'_v{version}.{suffix}')
                try:
                    bl = bucket.get_blob(todeleteblobname)
                    if bl:
                        bl.delete()
                except AttributeError:
                    pass

        ############################ 
        bloblist = gcs.list_blobs(BUCKET_NAME, prefix=blobprefix)

        for blob in bloblist:
            if r.match(blob.name):
                file2store = r.match(blob.name).group()
                url2download = "https://www.googleapis.com/storage/v1/b/" + BUCKET_NAME + "/o/" + \
                               file2store.replace('/', '%2F') + "?alt=media"
                vs = re.findall(vspattern, file2store)[0]
                archiveblob.append({"url2download": url2download,
                                    "Updated": dt.datetime.strftime(blob.updated, "%d/%m/%y %T"),
                                    "displayname": f"{displayname} {vs}",
                                    "version": vs.split('v')[-1]})

        blobinfo.update({key: archiveblob})

    return jsonify(blobinfo), 200


@app.route('/makevizlink', methods=['POST'])
@jwt_authenticated
def makevizlink():
    ds = datastore.Client()
    gcs = storage.Client()
    bucket = gcs.bucket(BUCKET_NAME)
    userinput = request.get_json()
    userinput['email'] = request.email
    orgname = request.org
    parent_key = ds.key('users', userinput['email'])
    entity = ds.get(key=ds.key('Project', userinput['project_id'], parent=parent_key))
    orgentity = ds.get(key=ds.key('organisation', orgname))
    Status = ["Results Summary", "Final Proposal", "Lock"]
    if entity.get('status', '') not in Status:
        return jsonify({"error": "project incomplete at {} state ".format(entity.get('status', ''))}), 400

    # orgentity['viz3doptions']= {"tabs2toshow":"1,2,3,4,5"} # 1,2,3,4
    tabstoshow = None
    if 'viz3doptions' in orgentity:
        tabstoshow = orgentity['viz3doptions'].get('tabstoshow')

    VzL = VzLink.VizLink(entity, bucket)
    addedlink = VzL.collectAndSaveJson()
    excellink = VzL.TSforExport()
    status = {}
    status.update(addedlink)
    if 'delete' in userinput:
        deletedlink = VzL.deleteprojectfolder()
        status.update(deletedlink)
    day2expire = userinput.get('expirydays', 60)
    stringtime = dt.datetime.now() + dt.timedelta(days=int(day2expire))

    linkparameter_raw = userinput['project_id'] + "_" + dt.datetime.strftime(stringtime, "%Y%m%d")
    # tabs in orgentity to steer the viz3d according to org 
    if tabstoshow:
        linkparameter_raw += "_tabs_" + tabstoshow
    linkparameter_encoded = base64.b64encode(linkparameter_raw.encode('utf-8'))
    linkurl = "https://vizview.solextron.com/?projectId={}".format(linkparameter_encoded.decode('utf-8'))
    entity.update({"vizlink": linkurl})
    ds.put(entity)

    return jsonify(
        {"success": "added link", "linkforviz": linkurl, "expires": dt.datetime.strftime(stringtime, "%Y-%m-%d"),
         "excellink": excellink}), 200


@app.errorhandler(500)
def server_error(e):
    logging.exception('An error occurred during a request.')
    return """
    An internal error occurred: <pre>{}</pre>
    See logs for full stacktrace.
    """.format(getattr(e, "original_exception", None)), 500


if __name__ == '__main__':

    Envvar = {"Environ":
                  {"BUCKET_NAME": "solbucket",
                   "PV_DB_FILENAME": "CECModules2020-06.pkl",
                   "PVS_DB_FILENAME": "PvSystModules2020-06.pkl",
                   "INV_DB_FILENAME": "BigInvCEC04-2021.pkl",
                   "NASA_22YR_FILENAME": "22yr_nasa_monthly.pkl",
                   "USER_UPLOADS_FOLDER": "UserUploads/",
                   "FETCHED_WEATHER_FOLDER": "FetchedWeather/",
                   "AUXFILES_FOLDER": "AuxFiles/",
                   "RESULTS_FOLDER": "Results/",
                   "ELECPARAMS": "elec_design_params.json",
                   "FINPARAMS": "finparameter_inputs.json",
                   "DB_NAME": "refdata"}}

    for var in Envvar['Environ']:
        os.environ[var] = Envvar['Environ'][var]
    # os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = os.path.join("/Users","vipluv.aga","Code","auth",
    #                                                             "gcloudauthkeys","solex-mvp-2-e18a10831f31.json")
    credential_path = r"C:\Users\dell\Downloads\application_default_credentials.json"
    # project=r"solex-mvp-2"
    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = credential_path
    os.environ['SQLALCHEMY_LOCALDB_URI'] = "mysql+pymysql://root:default123@127.0.0.1:3306/refdata"
    os.environ['DB_HOST'] = '127.0.0.1'

    os.environ['FLASK_ENV'] = 'development'

    app.run(host='127.0.0.1', port=8080, use_reloader=True)


